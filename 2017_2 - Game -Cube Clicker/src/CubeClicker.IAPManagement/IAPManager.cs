﻿namespace CubeClicker.IAPManagement
{
    using Events;
    using System.Collections.Generic;
    using UnityEngine;

    public class IAPManager
    {
        public IAPManager() {}

        public static int productsCount { get { return products.Count; } }

        private static BaseIAPController mIAPController;
        private static List<IProduct> products = new List<IProduct>();

        public static void Initialize()
        {
            RepairController();

            if (mIAPController.IsInitialized())
            {
                return;
            }

            mIAPController.onInitializedSuccess -= RequestProductPrices;
            mIAPController.onInitializedSuccess += RequestProductPrices;
            mIAPController.Initialize();
        }

        ////////////////////////////////////////////////////////////
        public static void AddProduct(IProduct product)
        {
            RepairController();
            if (mIAPController.AddProduct(product.ProductID, product.ProductType))
            {
                products.Add(product);
            }
        }

        ////////////////////////////////////////////////////////////
        public static void AddProducts(IProduct[] products)
        {
            foreach (var product in products)
            {
                AddProduct(product);
            }
        }

        ////////////////////////////////////////////////////////////
        public static void BuyProduct(IProduct product, ProductEventHandler eventHandler)
        {
            Initialize();
            if (!products.Contains(product))
            {
                Debug.LogFormat("Can't buy broduct. Product {0} is not registered", product.ProductID);
                if (eventHandler != null)
                {
                    eventHandler(new ProductEventData() { product = product, status = PurchaseStatus.Fail });
                }
                return;
            }

            mIAPController.BuyProduct(product, eventHandler);
        }

        ////////////////////////////////////////////////////////////
        public static void RestorePurchases()
        {
            Initialize();
            mIAPController.RestorePurchases();
        }

        ////////////////////////////////////////////////////////////
        public static IProduct GetProduct(int index)
        {
            return products[index];
        }

        ////////////////////////////////////////////////////////////
        public static IProduct GetProduct(string productId)
        {
            return products.Find(p => { return p.ProductID == productId; });
        }

        ////////////////////////////////////////////////////////////
        public static IProduct[] GetProducts()
        {
            return products.ToArray();
        }

        ////////////////////////////////////////////////////////////
        private static void RepairController()
        {
            if (mIAPController == null)
            {
                mIAPController = IAPController.Instantiate();
            }
        }

        ////////////////////////////////////////////////////////////
        private static void RequestProductPrices()
        {
            foreach (var product in products)
            {
                product.price = mIAPController.GetProductPrice(product.ProductID);
            }
        }
    }
}
