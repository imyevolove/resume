﻿namespace CubeClicker.IAPManagement
{
    using Events;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Purchasing;

    public class IAPController : BaseIAPController, IStoreListener
    {
        public IAPController() {}

        // Unity IAP 
        private static IStoreController     m_StoreController;
        private static IExtensionProvider   m_StoreExtensionProvider;

        // Builder
        private ConfigurationBuilder configurationBuilder;

        //
        private Dictionary<string, ProductEventHandler> events = new Dictionary<string, ProductEventHandler>();

        public static IAPController Instantiate()
        {
            var go = new GameObject("IAPController", typeof(IAPController));
            return go.GetComponent<IAPController>();
        }

        ////////////////////////////////////////////////////////////
        public override void Initialize()
        {
            if (IsInitialized()) return;
            UnityPurchasing.Initialize(this, configurationBuilder);
            Debug.Log("UnityPurchasing initialize");
        }

        ////////////////////////////////////////////////////////////
        public override bool IsInitialized()
        {
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        ////////////////////////////////////////////////////////////
        public override bool AddProduct(string productId, ProductType type)
        {
            // Already initialized, can't add new product
            if (IsInitialized())
            {
                Debug.Log("Can't add new product UnityPurchasing already intialized");
                return false;
            }

            configurationBuilder.AddProduct(productId, type);
            return true;
        }

        ////////////////////////////////////////////////////////////
        public override string GetProductPrice(string productId)
        {
            if (!IsInitialized())
            {
                return string.Empty;
            }

            Product product = m_StoreController.products.WithID(productId);
            if (product == null)
            {
                return string.Empty;
            }

            return product.metadata.localizedPriceString;
        }

        ////////////////////////////////////////////////////////////
        public override void BuyProduct(IProduct product, ProductEventHandler eventHandler)
        {
            if (!IsInitialized())
            {
                Debug.Log("BuyProductID FAIL. Not initialized.");
                eventHandler(new ProductEventData() { product = product, status = PurchaseStatus.Fail });
                return;
            }

            Product iapProduct = m_StoreController.products.WithID(product.ProductID);

            /// Product not found
            if (iapProduct == null)
            {
                Debug.LogFormat("BuyProductID FAIL. Product [{0}] not found.", product.ProductID);
                eventHandler(new ProductEventData() { product = product, status = PurchaseStatus.Fail });
                return;
            }

            /// Product already purchased
            if (iapProduct.hasReceipt)
            {
                Debug.Log("BuyProductID ABORT. Product has receipt.");
                eventHandler(new ProductEventData() { product = product, status = PurchaseStatus.Purchased });
                return;
            }

            /// Product not available
            if (!iapProduct.availableToPurchase)
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found");
                eventHandler(new ProductEventData() { product = product, status = PurchaseStatus.Fail });
                return;
            }

            Debug.Log(string.Format("Purchasing product asychronously: '{0}'", iapProduct.definition.id));

            events.Remove(iapProduct.definition.id);
            events.Add(iapProduct.definition.id, eventHandler);

            m_StoreController.InitiatePurchase(iapProduct);
        }

        ////////////////////////////////////////////////////////////
        public override void RestorePurchases()
        {
            if (!IsInitialized())
            {
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                Debug.Log("RestorePurchases started ...");

                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions((result) => {
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            else
            {
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }

        ////////////////////////////////////////////////////////////
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }

        ////////////////////////////////////////////////////////////
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            Debug.LogFormat("Product purchase successfylly {0}", args.purchasedProduct.definition.id);
            TryCallProductEventCallback(args.purchasedProduct.definition.id, PurchaseStatus.Success);
            return PurchaseProcessingResult.Complete;
        }

        ////////////////////////////////////////////////////////////
        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.LogFormat("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason);
            TryCallProductEventCallback(product.definition.id, PurchaseStatus.Fail);
        }

        ////////////////////////////////////////////////////////////
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("OnInitialized: PASS");

            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;

            CallInitializedSuccessEvent();
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            if(configurationBuilder == null)
                configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        }

        ////////////////////////////////////////////////////////////
        private void TryCallProductEventCallback(string productId, PurchaseStatus status)
        {
            if (events.ContainsKey(productId))
            {
                if (events[productId] != null)
                {
                    events[productId](new ProductEventData() { product = null, status = status });
                    events.Remove(productId);
                }
            }
        }
    }
}
