﻿namespace CubeClicker.IAPManagement.Events
{
    public delegate void ProductPurchaseAction(IProduct product);
}
