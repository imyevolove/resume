﻿using CubeClicker.Application;
using CubeClicker.Managers;
using UnityEngine;

namespace CubeClicker
{
    public class Startup : MonoBehaviour
    {
        private void Awake()
        {
            GameManager.InitializeGame<CubeApplication>();
        }
    }
}