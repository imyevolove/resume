﻿namespace CubeClicker.Advertisement
{
    using UnityEngine.Events;

    public enum AD_POSITION
    {
        TOP,
        BOTTOM
    }

    public class AdsManager
    {
        public static event UnityAction onChange;

        public static bool canShowAds { get; protected set; }
        private static IAdsProvider m_Provider;

        ////////////////////////////////////////////////////////////
        public static void Setup(IAdsProvider provider)
        {
            m_Provider = provider;
            if (m_Provider != null)
            {
                if (!m_Provider.IsInitialized())
                {
                    EnableAds();
                    m_Provider.Initialize();
                    CallOnChangeEvent();
                }
            }
        }

        ////////////////////////////////////////////////////////////
        public static void ShowBanner()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!canShowAds) return;
            m_Provider.ShowBanner();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowBanner(AD_POSITION position)
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!canShowAds) return;
            m_Provider.ShowBanner(position);
        }

        ////////////////////////////////////////////////////////////
        public static void HideBanner()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            m_Provider.HideBanner();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowInterstitial()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!canShowAds) return;
            m_Provider.ShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public static bool CanShowInterstitial()
        {
            if (m_Provider == null || !m_Provider.IsInitialized() || !canShowAds) return false;
            return m_Provider.CanShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowVideo()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!canShowAds) return;
            m_Provider.ShowVideo();
        }

        ////////////////////////////////////////////////////////////
        public static bool CanShowVideo()
        {
            if (m_Provider == null || !m_Provider.IsInitialized() || !canShowAds) return false;
            return m_Provider.CanShowVideo();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowAnyFullAd()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!canShowAds) return;

            if (m_Provider.CanShowVideo())
            {
                m_Provider.ShowVideo();
                return;
            }

            if (m_Provider.CanShowInterstitial())
            {
                m_Provider.ShowInterstitial();
                return;
            }
        }

        ////////////////////////////////////////////////////////////
        public static bool CanShowAnyFullAd()
        {
            if (m_Provider == null || !m_Provider.IsInitialized() || !canShowAds) return false;
            return m_Provider.CanShowVideo() || m_Provider.CanShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public static void DisableAds()
        {
            if (canShowAds == false) return;

            canShowAds = false;
            HideBanner(); /// Hide banner if showing now
            CallOnChangeEvent();
        }

        ////////////////////////////////////////////////////////////
        public static void EnableAds()
        {
            if (canShowAds == true) return;

            canShowAds = true;
            CallOnChangeEvent();
        }

        ////////////////////////////////////////////////////////////
        protected static void CallOnChangeEvent()
        {
            if (onChange == null) return;
            onChange();
        }
    }
}
