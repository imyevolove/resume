﻿namespace CubeClicker.Advertisement
{
    public interface IAdsProvider
    {
        void Initialize();
        bool IsInitialized();

        void ShowBanner();
        void ShowBanner(AD_POSITION position);
        void HideBanner();

        void LoadInterstitial();
        void ShowInterstitial();
        bool CanShowInterstitial();

        void LoadVideo();
        void ShowVideo();
        bool CanShowVideo();
    }
}
