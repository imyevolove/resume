﻿namespace CubeClicker.Crypto
{
    using System;
    using System.IO;
    using System.Text;

    public static class EncryptionHelper
    {
        public static string Encrypt(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Decrypt(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
#if NETFX_CORE
            return Encoding.UTF8.GetString(base64EncodedBytes, 0, base64EncodedBytes.Length);
#else
            return Encoding.UTF8.GetString(base64EncodedBytes);
#endif
        }
    }
}
