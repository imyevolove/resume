﻿using CubeClicker.Personal;
using CubeClicker.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.DamageSystem
{
    [Serializable]
    public class PlayerSimpleDamageableBehaviour : DamageableBehaviour
    {
        [SerializeField]
        protected ParticleSystem particles;

        [SerializeField]
        protected int damageFactor;

        [SerializeField]
        protected int maxDamageParticlesPerHit;
        
        protected override void OnDamage(DamageEventData eventData)
        {
            int dmgParticlesCount = Mathf.CeilToInt(Mathf.Clamp01(eventData.damage / damageFactor) * maxDamageParticlesPerHit);

            particles.transform.position = eventData.point;
            particles.transform.rotation = Quaternion.LookRotation(eventData.normal);
            particles.Emit(dmgParticlesCount);

            PlayerManager.mine.Money.Value += eventData.damage * (Profile.current.vipStatus ? 2 : 1);

            base.OnDamage(eventData);
        }
    }
}
