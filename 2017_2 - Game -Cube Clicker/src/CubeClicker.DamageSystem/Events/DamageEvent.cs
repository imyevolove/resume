﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;

namespace CubeClicker.DamageSystem
{
    public class DamageEvent : UnityEvent<DamageEventData>
    {
    }
}
