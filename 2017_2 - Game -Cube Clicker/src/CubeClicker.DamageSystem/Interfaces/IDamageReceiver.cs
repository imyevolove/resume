﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.DamageSystem
{
    public interface IDamageReceiver
    {
        DamageEvent onDamage { get; }
        void ReceiveDamage(DamageEventData eventData);
    }
}
