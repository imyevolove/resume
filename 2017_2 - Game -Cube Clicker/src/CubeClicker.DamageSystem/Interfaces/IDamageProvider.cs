﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.DamageSystem
{
    public interface IDamageProvider
    {
        void Damage(DamageEventData eventData);
    }
}
