﻿using CubeClicker.Business.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CubeClicker.DamageSystem
{
    public struct DamageEventData
    {
        public IDamageProvider provider;
        public IDamageReceiver receiver;
        public float damage;
        public Vector3 point;
        public Vector3 normal;
        public TeamCode senderTeamCode;
    }
}
