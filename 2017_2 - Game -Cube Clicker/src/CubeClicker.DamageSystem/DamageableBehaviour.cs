﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.DamageSystem
{
    [DisallowMultipleComponent]
    public class DamageableBehaviour : MonoBehaviour, IDamageReceiver
    {
        [NonSerialized] private DamageEvent m_OnDamage = new DamageEvent();
        public DamageEvent onDamage => m_OnDamage;

        public void ReceiveDamage(DamageEventData eventData)
        {
            DispatchOnDamageEvent(eventData);
        }

        protected virtual void OnDamage(DamageEventData eventData)
        {
        }

        private void DispatchOnDamageEvent(DamageEventData eventData)
        {
            OnDamage(eventData);
            onDamage.Invoke(eventData);
        }
    }
}
