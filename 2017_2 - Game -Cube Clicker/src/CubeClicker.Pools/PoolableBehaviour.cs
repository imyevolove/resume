﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Pools
{
    [DisallowMultipleComponent]
    public class PoolableBehaviour : MonoBehaviour, IPoolable
    {
        public bool IsActivated => gameObject.activeSelf;

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }
    }
}
