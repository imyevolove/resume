﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.Pools
{
    public interface IPoolable
    {
        bool IsActivated { get; }
        void Activate();
        void Deactivate();
    }
}
