﻿namespace CubeClicker.Pools
{
    public interface IPool
    {
        IPoolable Get();
    }
}
