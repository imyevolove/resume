﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.Pools
{
    public abstract class Pool : IPool
    {
        protected List<IPoolable> objects = new List<IPoolable>();

        ////////////////////////////////////////////////////////////
        public IPoolable Get()
        {
            var free = objects.FirstOrDefault(o => !o.IsActivated);

            if (free == null)
            {
                free = CreateInstance();

                if (free == null)
                    throw new NullReferenceException("Poolable element instance can't be a null");

                objects.Add(free);
            }

            free.Activate();
            return free;
        }

        ////////////////////////////////////////////////////////////
        protected abstract IPoolable CreateInstance();
    }
}
