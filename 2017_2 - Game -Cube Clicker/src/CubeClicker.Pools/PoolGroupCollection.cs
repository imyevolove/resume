﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Pools
{
    public class PoolGroupManagerInstance : MonoBehaviour
    {
        private Dictionary<IPoolable, IPool> m_Pools = new Dictionary<IPoolable, IPool>();

        public IPool GetPool(IPoolable source, Func<IPool> poolFactory)
        {
            IPool pool = null;

            if (!m_Pools.TryGetValue(source, out pool))
            {
                pool = poolFactory.Invoke();
                m_Pools.Add(source, pool);
            }

            return pool;
        }
    }

    public sealed class PoolGroupManager<T> : PoolGroupManagerInstance
        where T : IPoolable
    {
        private static PoolGroupManagerInstance m_Instance;
        public static PoolGroupManagerInstance Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = new GameObject(nameof(PoolGroupManager<T>), typeof(PoolGroupManagerInstance)).GetComponent<PoolGroupManagerInstance>();
                return m_Instance;
            }
        }
    }
}
