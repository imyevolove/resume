﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Pools
{
    public class MonoBehaviourPool<T> : Pool
        where T : MonoBehaviour
    {
        private T m_Source;

        public MonoBehaviourPool(T source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var poolableComponent = (source as IPoolable) ?? source.gameObject.GetComponent<IPoolable>();
            if (poolableComponent == null)
                throw new ArgumentNullException("Source prefab doesnt have a poolable components");

            m_Source = source;
        }

        protected override IPoolable CreateInstance()
        {
            return GameObject.Instantiate<T>(m_Source).GetComponent<IPoolable>();
        }
    }
}
