﻿using CubeClicker.Behaviours.Weapons.Abilities;
using System;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    [Serializable]
    public class StandartNegativeWeaponAbilityModificator : WeaponAbilityModificator
    {
        ////////////////////////////////////////////////////////////
        public StandartNegativeWeaponAbilityModificator()
        {
        }

        ////////////////////////////////////////////////////////////
        public StandartNegativeWeaponAbilityModificator(float modifier)
        {
            Modifier = modifier;
        }

        ////////////////////////////////////////////////////////////
        public override void Modificate(WeaponAbility ability)
        {
            ability.SetValue(ability.BaseValue - Modifier * ability.Level);
        }
    }
}
