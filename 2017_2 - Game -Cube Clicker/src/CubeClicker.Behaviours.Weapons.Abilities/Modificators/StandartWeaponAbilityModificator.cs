﻿using CubeClicker.Behaviours.Weapons.Abilities;
using System;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    [Serializable]
    public class StandartWeaponAbilityModificator : WeaponAbilityModificator
    {
        public StandartWeaponAbilityModificator()
        {
        }

        ////////////////////////////////////////////////////////////
        public StandartWeaponAbilityModificator(float modifier)
        {
           Modifier = modifier;
        }

        public override void Modificate(WeaponAbility ability)
        {
            ability.SetValue(ability.BaseValue + Modifier * ability.Level);
        }
    }
}
