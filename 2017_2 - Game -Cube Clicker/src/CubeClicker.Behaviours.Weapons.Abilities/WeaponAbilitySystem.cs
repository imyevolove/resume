﻿using CubeClicker.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public class WeaponAbilitySystem : IWeaponAbilitySystem
    {
        public int Count { get { return abilities.Count; } }

        private Dictionary <WeaponAbilityCode, WeaponAbility> abilities
           = new Dictionary<WeaponAbilityCode, WeaponAbility>();

        ////////////////////////////////////////////////////////////
        public bool Contains(WeaponAbilityCode code)
        {
            return abilities.ContainsKey(code);
        }

        ////////////////////////////////////////////////////////////
        public void AddAbility(WeaponAbility ability)
        {
            if (ability == null)
                throw new ArgumentNullException(nameof(ability));

            if (abilities.ContainsKey(ability.Code))
            {
                Debug.LogFormat($"Ability with code ({ability.Code}) already defined");
                return;
            }

            abilities.Add(ability.Code, ability);
        }

        ////////////////////////////////////////////////////////////
        public WeaponAbility GetAbility(int index)
        {
            return abilities.ElementAt(index).Value;
        }

        ////////////////////////////////////////////////////////////
        public WeaponAbility GetAbility(WeaponAbilityCode code)
        {
            WeaponAbility ability = null;

            if (abilities.TryGetValue(code, out ability))
                return ability;

            Debug.LogWarning($"Object with code ({code}) not found in container");
            return null;
        }
        
        ////////////////////////////////////////////////////////////
        public bool RemoveAbility(WeaponAbilityCode code)
        {
            return abilities.Remove(code);
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();
            List<StorageDataContainer> absList = new List<StorageDataContainer>();

            foreach (var abs in abilities)
            {
                if (!(abs.Value is IStorable)) continue;

                var absData = new StorageDataContainer();
                absData["system_inject_ability_key"] = abs.Key.ToString();
                absData["system_inject_ability_data"] = abs.Value.GetStorageData();

                absList.Add(absData);
            }

            data["abilities_list"] = absList;

            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<List<StorageDataContainer>>("abilities_list"))
            {
                var list = (List<StorageDataContainer>)storageData["abilities_list"];

                foreach (var abilityData in list)
                {
                    /// Ability type full name not found
                    if (!abilityData.KeyValueIs<string>("system_inject_ability_key")) continue;
                    var codeValue = (string)abilityData["system_inject_ability_key"];

                    WeaponAbilityCode code;
                    if (!Enum.TryParse(codeValue, out code)) continue;

                    WeaponAbility ability = null;
                    if (!abilities.TryGetValue(code, out ability)) continue;

                    /// Ability is not storable
                    if (!(ability is IStorable))
                    {
                        Debug.LogWarning($"Ability '{ability.GetType().FullName}' with code {code} is not a IStorable");
                        continue;
                    }

                    /// Set storage data
                    if (abilityData.KeyValueIs<StorageDataContainer>("system_inject_ability_data"))
                    {
                        (ability as IStorable).SetStorageData((StorageDataContainer)abilityData["system_inject_ability_data"]);
                    }
                }
            }
        }
    }
}
