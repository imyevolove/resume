﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public enum WeaponAbilityCode
    {
        [WeaponAbilityOptions(typeof(StandartWeaponAbilityModificator), typeof(StandartWeaponAbilityPriceCalculator))]
        Damage,

        [WeaponAbilityOptions(typeof(StandartNegativeWeaponAbilityModificator), typeof(StandartWeaponAbilityPriceCalculator))]
        Reload,

        [WeaponAbilityOptions(typeof(StandartWeaponAbilityModificator), typeof(StandartWeaponAbilityPriceCalculator))]
        FireRate,

        [WeaponAbilityOptions(typeof(StandartWeaponAbilityModificator), typeof(StandartWeaponAbilityPriceCalculator))]
        Ammo
    }
}
