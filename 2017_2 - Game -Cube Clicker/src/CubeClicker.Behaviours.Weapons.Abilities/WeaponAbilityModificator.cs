﻿using CubeClicker.Abilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public abstract class WeaponAbilityModificator : AbilityModificator<WeaponAbility>, IWeaponAbilityModificator
    {
        public float Modifier { get; set; }
    }
}
