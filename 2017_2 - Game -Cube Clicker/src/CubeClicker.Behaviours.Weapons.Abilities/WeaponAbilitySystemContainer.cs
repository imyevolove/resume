﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CubeClicker.Storage;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    [Serializable]
    public class WeaponAbilitySystemContainer : MonoBehaviour, IWeaponAbilitySystem
    {
        public IWeaponAbilitySystem m_Source;
        public int Count => m_Source.Count;

        public void AddAbility(WeaponAbility ability)
        {
            m_Source.AddAbility(ability);
        }

        public bool Contains(WeaponAbilityCode code)
        {
            return m_Source.Contains(code);
        }

        public WeaponAbility GetAbility(int index)
        {
            return m_Source.GetAbility(index);
        }

        public WeaponAbility GetAbility(WeaponAbilityCode code)
        {
            return m_Source.GetAbility(code);
        }

        public bool RemoveAbility(WeaponAbilityCode code)
        {
            return m_Source.RemoveAbility(code);
        }

        public StorageDataContainer GetStorageData()
        {
            return m_Source.GetStorageData();
        }

        public void SetStorageData(StorageDataContainer storageData)
        {
            m_Source.SetStorageData(storageData);
        }
    }
}
