﻿using CubeClicker.Abilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public abstract class ScriptableWeaponAbilityModificator : ScriptableObject, IWeaponAbilityModificator
    {
        public float Modifier { get; set; }

        public abstract void Modificate(WeaponAbility ability);

        public void Modificate(IAbility ability)
        {
            Modificate(ability as WeaponAbility);
        }
    }
}
