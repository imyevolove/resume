﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CubeClicker.Abilities;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public class StandartWeaponAbilityPriceCalculator : WeaponAbilityModificator, IWeaponAbilityPriceCalculator
    {
        ////////////////////////////////////////////////////////////
        public StandartWeaponAbilityPriceCalculator()
        {
        }

        ////////////////////////////////////////////////////////////
        public StandartWeaponAbilityPriceCalculator(float modifier)
        {
           Modifier = modifier;
        }

        public override void Modificate(WeaponAbility ability)
        {
            ability.SetPrice(ability.BasePrice + ability.BasePrice * Mathf.Pow(ability.Level, Modifier) * Mathf.Pow(ability.BaseValue, Mathf.Log10(ability.Level + 1)));
        }
    }
}
