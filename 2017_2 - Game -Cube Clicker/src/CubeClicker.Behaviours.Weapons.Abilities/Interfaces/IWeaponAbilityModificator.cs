﻿using CubeClicker.Abilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public interface IWeaponAbilityModificator : IAbilityModificator<WeaponAbility>
    {
        float Modifier { get; set; }
    }
}
