﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public interface IWeaponAbilityBuilder : IDisposable
    {
        IWeaponAbilityBuilder SetCode(WeaponAbilityCode code);
        IWeaponAbilityBuilder SetPrice(float price);
        IWeaponAbilityBuilder SetValue(float value);
        IWeaponAbilityBuilder SetMaxLevel(int maxLevel);
        IWeaponAbilityBuilder SetModificatorFactor(float factor);
        IWeaponAbilityBuilder SetPriceCalculatorFactor(float factor);
        IWeaponAbilityBuilder SetPriceCalculator(IWeaponAbilityPriceCalculator calculator);
        IWeaponAbilityBuilder SetModificator(IWeaponAbilityModificator modificator);
        WeaponAbility Build();
    }
}
