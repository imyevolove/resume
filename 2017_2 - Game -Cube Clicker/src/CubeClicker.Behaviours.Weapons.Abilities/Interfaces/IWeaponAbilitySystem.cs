﻿namespace CubeClicker.Behaviours.Weapons.Abilities
{
    using CubeClicker.Storage;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IWeaponAbilitySystem : IStorable
    {
        int Count { get; }

        bool Contains(WeaponAbilityCode code);
        WeaponAbility GetAbility(int index);
        WeaponAbility GetAbility(WeaponAbilityCode code);
        void AddAbility(WeaponAbility ability);
        bool RemoveAbility(WeaponAbilityCode code);
    }
}
