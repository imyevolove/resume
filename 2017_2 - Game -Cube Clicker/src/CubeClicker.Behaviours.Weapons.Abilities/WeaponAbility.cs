﻿using CubeClicker.Abilities;
using CubeClicker.Economy;
using CubeClicker.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    [SerializeField]
    public class WeaponAbility : Ability, IPurchaseable<float>, IStorable
    {
        [SerializeField] public WeaponAbilityCode Code { get; }

        [SerializeField] private float m_BasePrice;
        public float BasePrice { get => m_BasePrice; private set => m_BasePrice = value; }

        public float Price { get; private set; }

        [SerializeField] private string m_Name;
        public string Name { get => m_Name; private set => m_Name = value; }

        private float m_BaseValue;
        public float BaseValue
        {
            get { return m_BaseValue; }
            set
            {
                m_BaseValue = value;
                Update();
            }
        }

        public float Value { get; private set; }

        private IWeaponAbilityPriceCalculator m_PriceCalculator;

        public WeaponAbility(
            WeaponAbilityCode code, 
            float basePrice,
            float baseValue, 
            int maxLevel,
            IWeaponAbilityModificator modificator,
            IWeaponAbilityPriceCalculator priceCalculator) 
            : base(maxLevel, modificator)
        {
            Name = code.ToString();
            m_PriceCalculator = priceCalculator;
            BasePrice = Price = basePrice;
            BaseValue = Value = baseValue;
            Code = code;

            SetLevel(0);
        }

        public void SetValue(float value)
        {
            Value = value;
        }

        public void SetPrice(float value)
        {
            Price = value;
        }

        ////////////////////////////////////////////////////////////
        public virtual StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["level"] = Level;

            return data;
        }

        ////////////////////////////////////////////////////////////
        public virtual void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<int>("level"))
            {
                SetLevel((int)storageData["level"]);
            }
        }

        public PurchaseReceipt Purchase(ICurrencyWallet<float> wallet)
        {
            if (wallet == null)
                throw new ArgumentNullException(nameof(wallet));

            if (wallet.Value < Price)
                return PurchaseReceipt.CreateRejectedReceipt(this, "Money not enought");

            wallet.Value -= Price;
            this.InrementLevel();

            return PurchaseReceipt.CreateOkReceipt(this);
        }

        public PurchaseReceipt Purchase(object requestData)
        {
            return Purchase(requestData as ICurrencyWallet<float>);
        }

        protected override void OnChanged()
        {
            base.OnChanged();
            Update();
        }

        private void Update()
        {
            m_PriceCalculator.Modificate(this);
            (Modificator as IWeaponAbilityModificator).Modificate(this);
        }
    }
}
