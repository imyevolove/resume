﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    public class WeaponAbilityBuilder : IWeaponAbilityBuilder
    {
        private WeaponAbilityCode m_Code;
        private IWeaponAbilityModificator m_Modificator;
        private IWeaponAbilityPriceCalculator m_PriceCalculator;
        private float m_PriceCalculatorFactor;
        private float m_ModificatorFactor;
        private float m_Value;
        private float m_Price;
        private int m_MaxLevel;

        public IWeaponAbilityBuilder SetCode(WeaponAbilityCode code)
        {
            m_Code = code;
            return this;
        }

        public IWeaponAbilityBuilder SetPrice(float price)
        {
            m_Price = price;
            return this;
        }

        public IWeaponAbilityBuilder SetValue(float value)
        {
            m_Value = value;
            return this;
        }

        public IWeaponAbilityBuilder SetMaxLevel(int maxLevel)
        {
            m_MaxLevel = maxLevel;
            return this;
        }

        public IWeaponAbilityBuilder SetModificator(IWeaponAbilityModificator modificator)
        {
            m_Modificator = modificator;
            return this;
        }

        public IWeaponAbilityBuilder SetModificatorFactor(float factor)
        {
            m_ModificatorFactor = factor;
            return this;
        }

        public IWeaponAbilityBuilder SetPriceCalculatorFactor(float factor)
        {
            m_PriceCalculatorFactor = factor;
            return this;
        }

        public IWeaponAbilityBuilder SetPriceCalculator(IWeaponAbilityPriceCalculator calculator)
        {
            m_PriceCalculator = calculator;
            return this;
        }

        public WeaponAbility Build()
        {
            if (m_Modificator == null || m_PriceCalculator == null)
            {
                var type = typeof(WeaponAbilityCode);
                var memInfo = type.GetMember(m_Code.ToString());
                var attributes = memInfo[0].GetCustomAttributes(typeof(WeaponAbilityOptionsAttribute), false);
                var abilityOptionsAttribute = (WeaponAbilityOptionsAttribute)attributes[0];

                if (m_Modificator == null)
                    m_Modificator = abilityOptionsAttribute.InstantiateModificator();

                if (m_PriceCalculator == null)
                    m_PriceCalculator = abilityOptionsAttribute.InstantiatePriceCalculator();
            }

            m_Modificator.Modifier = m_ModificatorFactor;
            m_PriceCalculator.Modifier = m_PriceCalculatorFactor;

            return new WeaponAbility(
                m_Code,
                m_Price,
                m_Value,
                m_MaxLevel,
                m_Modificator,
                m_PriceCalculator
                );
        }

        public void Dispose()
        {
            m_Modificator = null;
            m_PriceCalculator = null;
        }
    }
}
