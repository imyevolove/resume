﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Abilities
{
    [AttributeUsage(AttributeTargets.Field)]
    public class WeaponAbilityOptionsAttribute : Attribute
    {
        public Type ModificatorType;
        public Type PriceCalculatorType;

        public WeaponAbilityOptionsAttribute()
        {
        }

        public WeaponAbilityOptionsAttribute(Type modificatorType, Type priceCalculatorType)
        {
            ModificatorType = modificatorType;
            PriceCalculatorType = priceCalculatorType;
        }

        public IWeaponAbilityModificator InstantiateModificator()
        {
            return InstantiateModificator(ModificatorType);
        }

        public IWeaponAbilityPriceCalculator InstantiatePriceCalculator()
        {
            return InstantiatePriceCalculator(PriceCalculatorType);
        }

        public static IWeaponAbilityModificator InstantiateModificator(Type modificatorType)
        {
            return Instantiate<IWeaponAbilityModificator>(modificatorType);
        }

        public static IWeaponAbilityPriceCalculator InstantiatePriceCalculator(Type calculatorType)
        {
            return Instantiate<IWeaponAbilityPriceCalculator>(calculatorType);
        }

        private static T Instantiate<T>(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            if (!typeof(T).IsAssignableFrom(type))
                throw new TypeLoadException($"{nameof(T)} is not assignable from {nameof(type)}");

            return (T)Activator.CreateInstance(type);
        }
    }
}
