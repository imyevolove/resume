﻿namespace CubeClicker.Audio
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceRandomizer : MonoBehaviour
    {
        protected AudioSource audioSource;

        [SerializeField]
        protected List<AudioClip> audioClips;

        protected void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void PlayOneShot()
        {
            if (audioSource == null) return;
            if (audioClips.Count == 0) return;

            var audioClipIndex = UnityEngine.Random.Range(0, audioClips.Count);

            audioSource.PlayOneShot(audioClips[audioClipIndex]);
        }
    }
}
