﻿namespace CubeClicker.Animator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class BoolAnimatorStateHandler : IAnimatorStateHandler
    {
        protected UnityEngine.Animator animator;
        protected AnimatorState previousState;
        protected AnimatorState currentState;

        ////////////////////////////////////////////////////////////
        public BoolAnimatorStateHandler(UnityEngine.Animator animator)
        {
            this.animator = animator;
        }

        ////////////////////////////////////////////////////////////
        public void SetState(AnimatorState state, bool status = true)
        {
            if (state == null)
                throw new ArgumentNullException();

            previousState = currentState;
            currentState = state;

            if (previousState != null)
                ApplyState(previousState, false);

            if (currentState != null)
                ApplyState(currentState, status);
        }

        ////////////////////////////////////////////////////////////
        public void ApplyState(AnimatorState state, bool status)
        {
            animator.SetBool(state.Value, status);
        }
    }
}
