﻿namespace CubeClicker.Animator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IAnimatorStateHandler
    {
        void SetState(AnimatorState state, bool status);
    }
}
