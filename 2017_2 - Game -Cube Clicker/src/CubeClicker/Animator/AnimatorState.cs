﻿namespace CubeClicker.Animator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class AnimatorState
    {
        public readonly string  Value;

        ////////////////////////////////////////////////////////////
        public AnimatorState(string value)
        {
            Value = value;
        }
    }
}
