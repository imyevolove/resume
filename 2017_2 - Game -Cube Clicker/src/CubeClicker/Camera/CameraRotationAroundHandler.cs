﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker
{
    [Serializable]
    [RequireComponent(typeof(Camera))]
    public class CameraRotationAroundHandler : CameraRotationHandler
    {
        [SerializeField] public float movementDuration;
        [SerializeField] public float rotationSpeed;
        [SerializeField] public Transform target;

        private Camera m_Camera;
        public Camera Camera
        {
            get
            {
                if (m_Camera == null)
                    m_Camera = GetComponent<Camera>();
                return m_Camera;
            }
        }

        private Vector2 m_RotationVelocity;
        private Vector2 m_RotationVelocityDamp;

        private void Update()
        {
            RotateOrbit();
        }

        public override void Rotate(Vector2 direction)
        {
            Rotate(direction.x, direction.y);
        }

        public override void Rotate(float x, float y)
        {
            if (target == null)
            {
                Debug.LogWarning("Rotate around target not defined");
                return;
            }

            m_RotationVelocity.x += x;
            m_RotationVelocity.y += y;

        }

        private void RotateOrbit()
        {
            if (target == null) return;

            m_RotationVelocity = Vector2.SmoothDamp(m_RotationVelocity, Vector2.zero, ref m_RotationVelocityDamp, movementDuration);

            transform.RotateAround(target.position, transform.up, m_RotationVelocity.x * Time.deltaTime);
            transform.RotateAround(target.position, transform.right, m_RotationVelocity.y * Time.deltaTime);

            //var lookRotation = Quaternion.LookRotation(target.transform.position - transform.position);
            //transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
        }
    }
}
