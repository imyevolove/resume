﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker
{
    public abstract class CameraRotationHandler : MonoBehaviour
    {
        public abstract void Rotate(Vector2 direction);
        public abstract void Rotate(float x, float y);
    }
}
