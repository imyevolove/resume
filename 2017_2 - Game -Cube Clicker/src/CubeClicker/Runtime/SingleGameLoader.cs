﻿using CubeClicker.Managers;
using UnityEngine;

namespace CubeClicker.Runtime
{
    public class SingleGameLoader : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////
        public void Start()
        {
            PlayerManager.mine.UnitsManager.SpawnAllUnits();
        }
    }
}
