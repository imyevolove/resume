﻿using CubeClicker.Advertisement;
using UnityEngine;

namespace CubeClicker.Runtime
{
    public class AdsHandler : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////
        protected void Start()
        {
            AdsManager.ShowBanner();
        }
    }
}
