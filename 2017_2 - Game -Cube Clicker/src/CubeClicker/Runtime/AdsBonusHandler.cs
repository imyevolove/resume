﻿using CubeClicker.Advertisement;
using CubeClicker.Application.Commands;
using CubeClicker.Formatters;
using CubeClicker.Managers;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CubeClicker.Runtime
{
    [Serializable]
    public class AdsBonusHandler : MonoBehaviour
    {
        [SerializeField]
        protected RectTransform bannerTransform;

        [SerializeField]
        protected Button bannerButton;

        [SerializeField]
        protected Text bannerText;

        [Header("Settings")]

        [SerializeField]
        protected float gapBtwDisplayingMinutes;

        [SerializeField]
        protected int pauseUIVisibleSeconds;

        protected IMoneyFormatter moneyFormatter;
        protected long reward;

        ////////////////////////////////////////////////////////////
        protected void Start()
        {
            moneyFormatter = GameManager.Application.RevokeObject<IMoneyFormatter>();

            SetupEvents();
            StartWaitEnumerator();
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            bannerButton.onClick.AddListener(WatchAds);
        }

        ////////////////////////////////////////////////////////////
        protected void StartWaitEnumerator()
        {
            StopAllCoroutines();
            StartCoroutine("WaitForPauseTime");
        }

        ////////////////////////////////////////////////////////////
        protected void StartPauseUIEnumerator()
        {
            StopAllCoroutines();
            StartCoroutine("WaitForUIPauseTime");
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator WaitForPauseTime()
        {
            yield return new WaitForSeconds(gapBtwDisplayingMinutes * 60);

            if (CheckAdsAvailable())
            {
                GenerateReward();
                StartPauseUIEnumerator();
            }
            else
            {
                StartWaitEnumerator();
            }
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator WaitForUIPauseTime()
        {
            ShowUI();

            yield return new WaitForSeconds(pauseUIVisibleSeconds);

            HideUI();
            StartWaitEnumerator();
        }

        ////////////////////////////////////////////////////////////
        protected void GenerateReward()
        {
            reward = (long)(PlayerManager.mine.Money.Value * 2.5f);
        }

        ////////////////////////////////////////////////////////////
        protected bool CheckAdsAvailable()
        {
            return AdsManager.CanShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        protected void GiveReward()
        {
            PlayerManager.mine.Money.Value += reward;
            GameManager.Application.InvokeCommand<SavePlayerProfileDataCommand>();
        }

        ////////////////////////////////////////////////////////////
        protected void WatchAds()
        {
            GiveReward();

            AdsManager.ShowInterstitial();

            HideUI();
            StartWaitEnumerator();
        }

        ////////////////////////////////////////////////////////////
        protected void ShowUI()
        {
            bannerText.text = string.Format("GET FREE MONEY ({0})", moneyFormatter.Format(reward));
            bannerTransform.gameObject.SetActive(true);
        }

        ////////////////////////////////////////////////////////////
        protected void HideUI()
        {
            bannerTransform.gameObject.SetActive(false);
        }
    }
}
