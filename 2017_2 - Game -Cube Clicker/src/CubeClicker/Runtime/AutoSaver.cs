﻿using CubeClicker.Application.Commands;
using CubeClicker.Managers;
using System;
using System.Collections;
using UnityEngine;

namespace CubeClicker.Runtime
{
    [Serializable]
    public class AutoSaver : MonoBehaviour
    {
        [SerializeField]
        public float autoSaveTimePeriodic;

        ////////////////////////////////////////////////////////////
        protected void OnEnable()
        {
            StopCoroutine("AutoSaveEnumerator");
            StartCoroutine("AutoSaveEnumerator");
        }

        ////////////////////////////////////////////////////////////
        protected void Save()
        {
            GameManager.Application.InvokeCommand<SavePlayerProfileDataCommand>();
            Debug.Log("Auto Save");
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator AutoSaveEnumerator()
        {
            yield return new WaitForSeconds(Mathf.Abs(autoSaveTimePeriodic));

            Save();
            StartCoroutine("AutoSaveEnumerator");
        }
    }
}
