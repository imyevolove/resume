﻿using CubeClicker.DamageSystem;
using CubeClicker.Input.Player;
using System;
using UnityEngine;

namespace CubeClicker.GameControllers
{
    [Serializable, RequireComponent(typeof(IDamageReceiver))]
    public class PlayerInputHitReceiver : GameControllerBehaviour, IPlayerTouchEndHandler, IDamageProvider
    {
        ////////////////////////////////////////////////////////////
        public void Damage(DamageEventData eventData)
        {
            eventData.receiver.ReceiveDamage(eventData);
        }

        ////////////////////////////////////////////////////////////
        public void OnPlayerTouchEnd(PlayerTouchEventData eventData)
        {
            Damage(new DamageEventData() {
                receiver = GetComponent<IDamageReceiver>(),
                provider = this,
                damage = 1,
                normal = eventData.hit.normal,
                point = eventData.hit.point
            });
        }
    }
}
