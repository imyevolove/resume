﻿namespace CubeClicker.Generic.Mono
{
    using CubeClicker.Pools;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [Serializable, RequireComponent(typeof(IPoolable))]
    public class PoolElemetLifetime : MonoBehaviour
    {
        [SerializeField]
        protected float lifetime;

        protected void OnEnable()
        {
            StopCoroutine ("LifetimeEnumerator");
            StartCoroutine("LifetimeEnumerator");
        }

        protected IEnumerator LifetimeEnumerator()
        {
            yield return new WaitForSeconds(lifetime);
            GetComponent<IPoolable>().Deactivate();
        }
    }
}
