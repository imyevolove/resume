﻿namespace CubeClicker.Input.Player
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public struct PlayerTouchEventData
    {
        public Vector2 mousePosition;
        public RaycastHit hit;
    }
}
