﻿namespace CubeClicker.Input.Player
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IPlayerTouchStartHandler
    {
        void OnPlayerTouchStart(PlayerTouchEventData eventData);
    }
}
