﻿namespace CubeClicker.Input.Player
{
    using System;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UInput = UnityEngine.Input;

    [Serializable]
    public class PlayerInput3DTouchSystem : MonoBehaviour
    {
        [SerializeField]
        public LayerMask mask;

        [SerializeField]
        public float raycastLength;

        public void Update()
        {
            UpdateTouchEventListener();
        }

        protected void UpdateTouchEventListener()
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;

            if (UInput.GetMouseButtonUp(0))
            {
                var hit = Raycast<IPlayerTouchEndHandler>();
                if (hit.collider != null)
                {
                    BroadcastTouchEndEvent(
                        hit.collider.GetComponents<IPlayerTouchEndHandler>(), 
                        MakeEventData(hit));
                }
            }

            if (UInput.GetMouseButtonDown(0))
            {
                var hit = Raycast<IPlayerTouchStartHandler>();
                if (hit.collider != null)
                {
                    BroadcastTouchStartEvent(
                        hit.collider.GetComponents<IPlayerTouchStartHandler>(), 
                        MakeEventData(hit));
                }
            }

            if (UInput.GetMouseButton(0))
            {
                var hit = Raycast<IPlayerTouchHandler>();
                if (hit.collider != null)
                {
                    BroadcastTouchEvent(
                        hit.collider.GetComponents<IPlayerTouchHandler>(), 
                        MakeEventData(hit));
                }
            }
        }

        ////////////////////////////////////////////////////////////
        protected RaycastHit Raycast<TEventType>()
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(UInput.mousePosition);
            Physics.Raycast(ray, out hit, raycastLength, mask.value);

            return hit;
        }

        ////////////////////////////////////////////////////////////
        protected PlayerTouchEventData MakeEventData(RaycastHit hit) 
        {
            return new PlayerTouchEventData()
            {
                hit = hit,
                mousePosition = UInput.mousePosition
            };
        }

        ////////////////////////////////////////////////////////////
        protected void BroadcastTouchStartEvent(IPlayerTouchStartHandler[] listeners, PlayerTouchEventData eventData)
        {
            foreach (var listener in listeners)
            {
                listener.OnPlayerTouchStart(eventData);
            }
        }

        ////////////////////////////////////////////////////////////
        protected void BroadcastTouchEndEvent(IPlayerTouchEndHandler[] listeners, PlayerTouchEventData eventData)
        {
            foreach (var listener in listeners)
            {
                listener.OnPlayerTouchEnd(eventData);
            }
        }

        ////////////////////////////////////////////////////////////
        protected void BroadcastTouchEvent(IPlayerTouchHandler[] listeners, PlayerTouchEventData eventData)
        {
            foreach (var listener in listeners)
            {
                listener.OnPlayerTouch(eventData);
            }
        }
    }
}
