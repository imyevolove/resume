﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CubeClicker.Input.Player
{
    [Serializable]
    public class PlayerTouchCameraRotation : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] public float sesnsetivy = 1f;
        [SerializeField] public CameraRotationHandler rotationHandler;

        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        public void OnDrag(PointerEventData eventData)
        {
            rotationHandler.Rotate(eventData.delta * sesnsetivy);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
        }
    }
}
