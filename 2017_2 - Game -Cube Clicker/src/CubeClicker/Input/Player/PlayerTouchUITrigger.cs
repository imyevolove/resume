﻿using CubeClicker.DamageSystem;
using CubeClicker.Managers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CubeClicker.Input.Player
{
    [SerializeField]
    public class PlayerTouchUITrigger : MonoBehaviour, IDamageProvider, IPointerClickHandler
    {
        ////////////////////////////////////////////////////////////
        public void Damage(DamageEventData eventData)
        {
            eventData.receiver.ReceiveDamage(eventData);
        }

        ////////////////////////////////////////////////////////////
        public void OnPointerClick(PointerEventData eventData)
        {
            var cube = UnitUtility.GetCubeUnit();
            var mesh = cube.GetComponent<MeshFilter>().sharedMesh;

            var verticeIndex = UnityEngine.Random.Range(0, mesh.vertexCount);
            var point   = cube.transform.TransformPoint(mesh.vertices[verticeIndex]);
            var normal  = mesh.normals[verticeIndex];

            Damage(new DamageEventData() {
                damage = 1,
                normal = normal,
                point = point,
                provider = this,
                receiver = cube.DamageReceiver
            });
        }
    }
}
