﻿namespace CubeClicker.Input.Player
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IPlayerTouchHandler
    {
        void OnPlayerTouch(PlayerTouchEventData eventData);
    }
}
