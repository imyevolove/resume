﻿using CubeClicker.Behaviours.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.AI
{
    [Serializable]
    [RequireComponent(typeof(UnitBehaviour))]
    public abstract class UnitAI : MonoBehaviour
    {
        private UnitBehaviour m_Unit;
        public UnitBehaviour Unit
        {
            get
            {
                if (m_Unit == null)
                    m_Unit = GetComponent<UnitBehaviour>();
                return m_Unit;
            }
        }
    }
}
