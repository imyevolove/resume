﻿using CubeClicker.Behaviours.Units;
using CubeClicker.StateMachines;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.AI
{
    [Serializable]
    public class WarriorUnitAI : UnitAI
    {
        [SerializeField] public RotateAround rotator;

        private UnitBehaviour m_Target;
        public UnitBehaviour Target => m_Target;

        public bool HasTarget => Target != null && Target.isActiveAndEnabled;

        private Coroutine m_UpdateCoroutine;
        private bool m_IsAttacking;

        private readonly StateMachine<WarriorUnitAIState> m_StateMachine = new StateMachine<WarriorUnitAIState>();

        protected virtual void Start()
        {
            m_StateMachine.RegisterState(WarriorUnitAIState.FindTarget, WarriorUnitAIState.Attacking, TurnRotator);
            m_StateMachine.RegisterState(WarriorUnitAIState.Attacking, WarriorUnitAIState.FindTarget, TurnRotator);
            m_StateMachine.RegisterStateUpdate(WarriorUnitAIState.FindTarget, FindTarget);
            m_StateMachine.RegisterStateUpdate(WarriorUnitAIState.Attacking, AttackTarget);
            m_StateMachine.SetState(WarriorUnitAIState.FindTarget);
        }

        protected virtual void Update()
        {
            m_StateMachine.Update();
        }

        private void AttackTarget()
        {
            if (HasTarget)
            {
                if (!Unit.weapon.IsReady) return;
                Unit.weapon.Use();

                return;
            }

            m_StateMachine.SetState(WarriorUnitAIState.FindTarget);
        }

        private void FindTarget()
        {
            if (!HasTarget)
            {
                m_Target = UnitBehaviour.All.FirstOrDefault(u => u.team != Unit.team);
                return;
            }

            m_StateMachine.SetState(WarriorUnitAIState.Attacking);
        }

        private void TurnRotator()
        {
            rotator.rotTarget = m_Target?.transform;
        }
    }
}
