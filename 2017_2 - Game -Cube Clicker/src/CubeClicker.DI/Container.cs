﻿namespace CubeClicker.DI
{
    using Generic;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class Container : IContainer
    {
        protected Dictionary<Type, object> objects
           = new Dictionary<Type, object>();

        ////////////////////////////////////////////////////////////
        public void RegisterObject(Type type, object obj)
        {
            if (obj == null) return;
            if (!ReflectionHelper.IsAssignableFrom(type, obj.GetType())) return;

            if (objects.ContainsKey(type))
                objects[type] = obj;

            objects.Add(type, obj);
        }

        ////////////////////////////////////////////////////////////
        public void RegisterObject<TTypeKey>(TTypeKey obj) where TTypeKey : class
        {
            if (obj == null) return;

            Type type = typeof(TTypeKey);

            if (objects.ContainsKey(type))
            {
                objects[type] = obj;
            }

            objects.Add(type, obj);
        }

        ////////////////////////////////////////////////////////////
        public TTypeKey RevokeObject<TTypeKey>() where TTypeKey : class
        {
            Type type = typeof(TTypeKey);
            object obj = null;

            if (objects.TryGetValue(type, out obj))
            {
                return obj as TTypeKey;
            }

            Debug.LogFormat("Object with type ({0}) not found in container", type.Name);
            return null;
        }

        ////////////////////////////////////////////////////////////
        public TTypeKey[] RevokeObjects<TTypeKey>() where TTypeKey : class
        {
            List<TTypeKey> fObjects = new List<TTypeKey>();

            foreach (var key in objects.Keys)
            {
                if (key is TTypeKey)
                {
                    fObjects.Add(objects[key] as TTypeKey);
                }
            }

            return fObjects.ToArray();
        }
    }

    public class Container<T> : IContainer<T> where T : class
    {
        protected Dictionary<Type, object> objects
           = new Dictionary<Type, object>();

        ////////////////////////////////////////////////////////////
        public void RegisterObject(Type type, object obj)
        {
            if (obj == null) return;
            if (!ReflectionHelper.IsAssignableFrom(typeof(T), type)) return;
            if (!ReflectionHelper.IsAssignableFrom(type, obj.GetType())) return;
            
            if (objects.ContainsKey(type))
            {
                objects[type] = obj;
            }

            objects.Add(type, obj);
        }

        ////////////////////////////////////////////////////////////
        public void RegisterObject<TTypeKey>(TTypeKey obj) where TTypeKey : class, T
        {
            if (obj == null) return;

            Type type = typeof(TTypeKey);

            if (objects.ContainsKey(type))
            {
                objects[type] = obj;
            }

            objects.Add(type, obj);
        }

        ////////////////////////////////////////////////////////////
        public object RevokeObject(Type type)
        {
            object obj = null;

            if (objects.TryGetValue(type, out obj))
            {
                return obj;
            }

            Debug.LogFormat("Object with type ({0}) not found in container", type.Name);
            return null;
        }

        ////////////////////////////////////////////////////////////
        public TTypeKey RevokeObject<TTypeKey>() where TTypeKey : class, T
        {
            Type type = typeof(TTypeKey);
            object obj = null;

            if (objects.TryGetValue(type, out obj))
            {
                return obj as TTypeKey;
            }

            Debug.LogFormat("Object with type ({0}) not found in container", type.Name);
            return null;
        }

        ////////////////////////////////////////////////////////////
        public TType RevokeObject<TType>(Type type)
        {
            object obj = null;

            if (objects.TryGetValue(type, out obj))
            {
                return (TType)obj;
            }

            Debug.LogFormat("Object with type ({0}) not found in container", type.Name);
            return default(TType);
        }

        ////////////////////////////////////////////////////////////
        public TTypeKey[] RevokeObjects<TTypeKey>() where TTypeKey : class, T
        {
            var type = typeof(TTypeKey);
            List<TTypeKey> fObjects = new List<TTypeKey>();

            foreach (var key in objects.Keys)
            {
                if (ReflectionHelper.IsAssignableFrom(type, key))
                {
                    fObjects.Add(objects[key] as TTypeKey);
                }
            }

            return fObjects.ToArray();
        }
    }
}
