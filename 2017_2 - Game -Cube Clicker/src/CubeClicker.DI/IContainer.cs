﻿namespace CubeClicker.DI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IContainer
    {
        void RegisterObject<TTypeKey>(TTypeKey obj) where TTypeKey : class;
        void RegisterObject(Type type, object obj);
        TTypeKey RevokeObject<TTypeKey>() where TTypeKey : class;
    }

    public interface IContainer<T>
    {
        void RegisterObject<TTypeKey>(TTypeKey obj) where TTypeKey : class, T;
        void RegisterObject(Type type, object obj);
        TTypeKey RevokeObject<TTypeKey>() where TTypeKey : class, T;
        object RevokeObject(Type type);
        TType RevokeObject<TType>(Type type);
    }
}
