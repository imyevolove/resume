﻿using CubeClicker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Data
{
    [CreateAssetMenu(fileName = "UnitsDatabase", menuName = "Databases/Units")]
    public class ScriptableUnitDatabase : ScriptableDatabase<UnitData>, IUnitDatabase
    {
    }
}
