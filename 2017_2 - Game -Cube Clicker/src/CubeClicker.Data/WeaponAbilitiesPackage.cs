﻿using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Database;
using CubeClicker.Storage;
using UnityEngine;

namespace CubeClicker.Data
{
    [CreateAssetMenu(fileName = "WeaponAbilities", menuName = "Components/Weapon Abilities")]
    public class WeaponAbilitiesPackage : ScriptableDatabase<WeaponAbilityData>, IWeaponAbilityDatabase
    {
        public IWeaponAbilitySystem CreateAbilitySystem()
        {
            var system = new WeaponAbilitySystem();

            foreach (var data in GetAll())
            {
                using (var abilityBuilder = new WeaponAbilityBuilder())
                {
                    abilityBuilder.SetCode(data.code);
                    abilityBuilder.SetMaxLevel(data.maxLevel);
                    abilityBuilder.SetValue(data.value);
                    abilityBuilder.SetPrice(data.price);
                    abilityBuilder.SetModificatorFactor(data.modificatorFactor);
                    abilityBuilder.SetPriceCalculatorFactor(data.priceFactor);
                    system.AddAbility(abilityBuilder.Build());
                }
            }

            return system;
        }
    }
}
