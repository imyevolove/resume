﻿using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Data
{
    [Serializable]
    public class WeaponAbilityData : DatabaseEntity
    {
        [SerializeField] public WeaponAbilityCode code;
        [SerializeField] public int maxLevel;
        [SerializeField] public float value;
        [SerializeField] public float price;
        [SerializeField] public float modificatorFactor;
        [SerializeField] public float priceFactor;
    }
}
