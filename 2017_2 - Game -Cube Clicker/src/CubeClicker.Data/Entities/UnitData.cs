﻿using CubeClicker.Behaviours.Units;
using CubeClicker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Data
{
    [Serializable]
    public class UnitData : DatabaseEntity
    {
        [SerializeField] public UnitBehaviour original;
        [SerializeField] public WeaponAbilitiesPackage abilities;
    }
}
