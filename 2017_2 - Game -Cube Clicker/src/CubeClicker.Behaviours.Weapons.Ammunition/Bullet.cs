﻿using CubeClicker.DamageSystem;
using System;
using System.Collections;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Ammunition
{
    [Serializable, RequireComponent(typeof(MeshRenderer), typeof(Collider), typeof(MeshFilter))]
    public class Bullet : BaseShell
    {
        [SerializeField] protected float speed;

        private Vector3 m_MovementDirection;

        protected void OnCollisionEnter(Collision collision)
        {
            var receiver = collision.gameObject.GetComponent<IDamageReceiver>();

            if (receiver != null)
            {
                Damage(new DamageEventData()
                {
                    provider = this,
                    receiver = collision.gameObject.GetComponent<IDamageReceiver>(),
                    normal = collision.contacts[0].normal,
                    point = collision.contacts[0].point,
                    damage = DamageValue
                });
            }

            Deactivate();
        }

        protected void OnEnable()
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        protected virtual void Update()
        {
            transform.position += m_MovementDirection * Time.deltaTime * speed;
        }

        public void LaunchBullet(Vector3 direction, object sender, float damage)
        {
            DamageValue = damage;
            m_MovementDirection = direction;
        }
    }
}
