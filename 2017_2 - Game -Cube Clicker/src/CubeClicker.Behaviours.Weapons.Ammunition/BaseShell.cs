﻿using CubeClicker.Business.Teams;
using CubeClicker.DamageSystem;
using CubeClicker.Pools;
using System;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Ammunition
{
    [Serializable]
    public class BaseShell : PoolableBehaviour, IDamageProvider
    {
        public float DamageValue { get; set; }

        public void Damage(DamageEventData eventData)
        {
            eventData.receiver.ReceiveDamage(eventData);
        }
    }
}
