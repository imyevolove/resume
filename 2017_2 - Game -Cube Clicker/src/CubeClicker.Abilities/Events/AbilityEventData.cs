﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Abilities
{
    public class AbilityEventData
    {
        public readonly IAbility Ability;

        public AbilityEventData(IAbility ability)
        {
            Ability = ability;
        }
    }
}
