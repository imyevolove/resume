﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Abilities
{
    public interface IAbilityModificator<TAbility> : IAbilityModificator
        where TAbility : IAbility
    {
        void Modificate(TAbility ability);
    }

    public interface IAbilityModificator
    {
        void Modificate(IAbility ability);
    }
}
