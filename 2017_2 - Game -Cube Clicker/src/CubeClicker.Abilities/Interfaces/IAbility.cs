﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.Abilities
{
    public interface IAbility
    {
        AbilityEvent onChanged { get; }

        int Level { get; }
        int MaxLevel { get; }
        
        bool IsLevelMaximized { get; }

        int SetLevel(int level);
    }
}
