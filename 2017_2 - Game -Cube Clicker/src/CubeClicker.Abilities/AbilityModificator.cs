﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Abilities
{
    public abstract class AbilityModificator<TAbility> : IAbilityModificator<TAbility>
        where TAbility : class, IAbility
    {
        public abstract void Modificate(TAbility ability);

        public void Modificate(IAbility ability)
        {
            Modificate((TAbility)ability);
        }
    }
}
