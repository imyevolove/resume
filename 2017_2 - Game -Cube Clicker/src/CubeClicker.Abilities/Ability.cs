﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Abilities
{
    [Serializable]
    public class Ability : IAbility
    {
        private readonly AbilityEvent m_OnChanged = new AbilityEvent();
        public AbilityEvent onChanged => m_OnChanged;

        [SerializeField] private int m_Level;
        public int Level
        {
            get { return m_Level; }
            private set
            {
                m_Level = Mathf.Clamp(value, 0, MaxLevel);
                DispatchChangedEvent();
            }
        }

        [SerializeField] private int m_MaxLevel;
        public int MaxLevel
        {
            get { return m_MaxLevel; }
            private set
            {
                m_MaxLevel = value < 0 ? 0 : value;

                ClampLevel();
                DispatchChangedEvent();
            }
        }

        private IAbilityModificator m_Modificator;
        public IAbilityModificator Modificator => m_Modificator;

        public Ability(int maxLevel, IAbilityModificator modificator)
        {
            if (modificator == null)
                throw new ArgumentNullException(nameof(modificator));

            m_Modificator = modificator;
            m_MaxLevel = maxLevel;
        }

        public bool IsLevelMaximized => Level >= MaxLevel;

        public virtual int SetLevel(int level)
        {
            return Level = level;
        }

        protected virtual void OnChanged()
        {
        }

        private void DispatchChangedEvent()
        {
            OnChanged();
            onChanged.Invoke(new AbilityEventData(this));
        }

        private void ClampLevel()
        {
            if (m_Level <= MaxLevel) return;
            m_Level = m_Level < 0 ? 0 : m_Level;
            m_Level = MaxLevel;
        }
    }
}
