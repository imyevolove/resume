﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Abilities
{
    public static class AbilityLevelExtensions
    {
        public static int InrementLevel(this IAbility ability, int value)
        {
            ability.SetLevel(ability.Level + value);
            return ability.Level;
        }

        public static int InrementLevel(this IAbility ability)
        {
            return InrementLevel(ability, 1);
        }

        public static int DecrementLevel(this IAbility ability, int value)
        {
            ability.SetLevel(ability.Level - value);
            return ability.Level;
        }

        public static int DecrementLevel(this IAbility ability)
        {
            return InrementLevel(ability, 1);
        }
    }
}
