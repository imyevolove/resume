﻿namespace CubeClicker.Behaviours
{
    using System;
    using UnityEngine;

    [Serializable]
    public class RotateAround : RotatorBehaviour
    {
        [SerializeField]
        public bool lookToTarget;

        ////////////////////////////////////////////////////////////
        protected override void RotateAroundTarget()
        {
            base.RotateAroundTarget();
            LookAtTarget();
        }

        ////////////////////////////////////////////////////////////
        protected void LookAtTarget()
        {
            transform.LookAt(rotTarget);
        }
    }
}