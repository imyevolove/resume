﻿namespace CubeClicker.Behaviours
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [Serializable]
    public abstract class RotatorBehaviour : MonoBehaviour
    {
        [SerializeField]
        public Transform rotTarget;

        [SerializeField]
        public Vector3 axis;

        [SerializeField]
        public float angle;

        ////////////////////////////////////////////////////////////
        protected virtual void Start() { }

        ////////////////////////////////////////////////////////////
        protected virtual void Awake() { }

        ////////////////////////////////////////////////////////////
        protected virtual void Update()
        {
            if (rotTarget != null)
                RotateAroundTarget();
        }

        ////////////////////////////////////////////////////////////
        protected virtual void RotateAroundTarget()
        {
            transform.RotateAround(rotTarget.position, axis * Time.deltaTime, angle * Time.deltaTime);
        }
    }
}
