﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Economy
{
    public class PurchaseReceipt
    {
        public readonly PurchaseStatus Status;
        public readonly string RejectionMessage;
        public readonly IPurchaseable Target;

        public PurchaseReceipt(IPurchaseable target, PurchaseStatus status, string rejectionMessage)
        {
            Target = target;
            Status = status;
            RejectionMessage = rejectionMessage;
        }

        public static PurchaseReceipt CreateOkReceipt(IPurchaseable target)
        {
            return new PurchaseReceipt(target, PurchaseStatus.Successfully, "");
        }

        public static PurchaseReceipt CreateRejectedReceipt(IPurchaseable target, string message)
        {
            return new PurchaseReceipt(target, PurchaseStatus.Rejected, message);
        }
    }
}
