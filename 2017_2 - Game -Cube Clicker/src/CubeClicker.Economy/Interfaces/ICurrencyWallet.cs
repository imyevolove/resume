﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Economy
{
    public interface ICurrencyWallet : ICurrencyWallet<float>
    {
    }

    public interface ICurrencyWallet<TValue>
    {
        TValue Value { get; set; }
    }
}
