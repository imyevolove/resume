﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Economy
{
    public interface IPurchaseable
    {
        float Price { get; }
        PurchaseReceipt Purchase(object requestData);
    }

    public interface IPurchaseable<TCurrency> : IPurchaseable
    {
        PurchaseReceipt Purchase(ICurrencyWallet<TCurrency> wallet);
    }
}
