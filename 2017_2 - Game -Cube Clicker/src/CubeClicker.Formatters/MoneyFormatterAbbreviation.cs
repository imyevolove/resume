﻿namespace CubeClicker.Formatters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class MoneyFormatterAbbreviation : IMoneyFormatter
    {
        ////////////////////////////////////////////////////////////
        public static readonly string[] DEFAULT_ABBREVIATION = new string[21]
        {
            "K", "M", "B", "T", "q", "Q", "s",
            "S", "O", "N", "d", "U", "D", "!",
            "@", "#", "$", "%", "^", "&", "*"
        };

        private const int DIVIDER = 3;

        public readonly string[] abbreviation;
        
        ////////////////////////////////////////////////////////////
        public MoneyFormatterAbbreviation(string[] abbreviation)
        {
            this.abbreviation = abbreviation;
        }
        
        ////////////////////////////////////////////////////////////
        public string Format(float value)
        {
            if (value == 0) return "0";

            var index = Mathf.FloorToInt(Mathf.Log10(value) / DIVIDER);

            if (index <= 0) return value.ToString("#.###");

            var di = Mathf.Pow(10, index * DIVIDER);
            var result = value / di;
            var abbIndex = Mathf.Clamp(index - 1, 0, abbreviation.Length - 1);

            return result.ToString("#.###") + abbreviation[abbIndex];
        }


    }
}
