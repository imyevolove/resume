﻿namespace CubeClicker.Formatters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class MoneyFormatter : IMoneyFormatter
    {
        ////////////////////////////////////////////////////////////
        public string Format(float value)
        {
            return value.ToString("#.###");
        }
    }
}
