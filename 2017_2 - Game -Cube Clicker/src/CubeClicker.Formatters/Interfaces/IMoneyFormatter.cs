﻿namespace CubeClicker.Formatters
{
    using Personal;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IMoneyFormatter
    {
        string Format(float value);
    }
}
