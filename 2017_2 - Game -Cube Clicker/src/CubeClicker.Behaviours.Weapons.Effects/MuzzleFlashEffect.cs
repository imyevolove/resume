﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Effects
{
    [Serializable]
    public class MuzzleFlashEffect : EffectBehaviour
    {
        [SerializeField]
        protected Light mLight;

        [SerializeField]
        public float enterTime;

        [SerializeField]
        public float exitTime;

        [SerializeField]
        public float maxIntensity;

        ////////////////////////////////////////////////////////////
        public override void Play()
        {
            mLight.enabled = true;
            StartCoroutine("MuzzleFlashEnumerator");
        }

        ////////////////////////////////////////////////////////////
        public override void Stop()
        {
            mLight.enabled = false;
            StopCoroutine("MuzzleFlashEnumerator");
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator MuzzleFlashEnumerator()
        {
            /// Enter
            float time = enterTime;

            while (time > 0)
            {
                time -= Time.deltaTime;
                var t = 1 - time / enterTime;

                ApplyIntensity(maxIntensity * t);
                yield return true;
            }

            /// Exit
            time = exitTime;

            while (time > 0)
            {
                time -= Time.deltaTime;
                var t = time / exitTime;

                ApplyIntensity(maxIntensity * t);
                yield return true;
            }

            ApplyIntensity(0);
            Stop();
        }

        ////////////////////////////////////////////////////////////
        protected void ApplyIntensity(float intensity)
        {
            mLight.intensity = intensity;
        }
    }
}
