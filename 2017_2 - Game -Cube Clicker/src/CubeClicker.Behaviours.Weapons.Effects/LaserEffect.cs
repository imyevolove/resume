﻿using System;
using System.Collections;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Effects
{

    [Serializable]
    public class LaserEffect : EffectBehaviour
    {
        [SerializeField, Range(0, 0.5f)]
        protected float startTime;

        [SerializeField, Range(0, 0.5f)]
        protected float endTime;

        [SerializeField]
        protected float playTime;

        [Header("Line")]

        [SerializeField]
        protected LineRenderer mLineRenderer;

        [SerializeField]
        protected float maxLineWidth;

        [Header("Light")]

        [SerializeField]
        protected Light mLight;

        [SerializeField]
        protected float maxLightIntensity;

        ////////////////////////////////////////////////////////////
        public void Setup(float playTime)
        {
            this.playTime = playTime;
        }

        ////////////////////////////////////////////////////////////
        public void SetLineLength(float length)
        {
            mLineRenderer.SetPosition(1, new Vector3(0, 0, length));
        }

        ////////////////////////////////////////////////////////////
        public override void Play()
        {
            StopCoroutine("Animation");
            StartCoroutine("Animation");
        }

        ////////////////////////////////////////////////////////////
        public override void Stop()
        {
            StopCoroutine("Animation");
            mLineRenderer.widthMultiplier = 0;
            mLight.intensity = 0;
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator Animation()
        {
            var tStart    = playTime * startTime;
            var tEnd      = playTime * endTime;
            var tPlay     = playTime - tStart - tEnd;
            var time      = 0f;
            var mul       = 0f;

            time = tStart;
            while (time > 0)
            {
                time -= Time.deltaTime;
                mul = 1 - time / tStart;

                mLineRenderer.widthMultiplier = maxLineWidth * mul;
                mLight.intensity = maxLightIntensity * mul;

                yield return true;
            }

            time = tPlay;
            while (time > 0)
            {
                time -= Time.deltaTime;

                mLineRenderer.widthMultiplier = maxLineWidth;
                mLight.intensity = maxLightIntensity;

                yield return true;
            }

            time = tEnd;
            while (time > 0)
            {
                time -= Time.deltaTime;
                mul = time / tEnd;

                mLineRenderer.widthMultiplier = maxLineWidth * mul;
                mLight.intensity = maxLightIntensity * mul;

                yield return true;
            }
        }
    }
}
