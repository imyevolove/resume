﻿using System;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons.Effects
{
    [Serializable]
    public abstract class EffectBehaviour : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////
        public abstract void Play();
        
        ////////////////////////////////////////////////////////////
        public abstract void Stop();

        ////////////////////////////////////////////////////////////
        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        ////////////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            Stop();
        }
    }
}
