﻿using CubeClicker.Crypto;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CubeClicker.Storage
{
    public class Storage
    {
        public static string persistentDataPath { get { return UnityEngine.Application.persistentDataPath; } }

        public static string MakePath(params string[] parts)
        {
            return parts.Aggregate((x, y) => Path.Combine(x, y));
        }

        ////////////////////////////////////////////////////////////
        public static string LoadText(string filename)
        {
            try
            {
                var bytes = File.ReadAllText(filename);
                bytes = EncryptionHelper.Decrypt(bytes);
                return bytes;
            }
            catch (IOException e)
            {
                Debug.LogException(e);
                return "";
            }
        }

        ////////////////////////////////////////////////////////////
        public static void SaveText(string filename, string text)
        {
            try
            {
                CreateDirectories(filename);

                var bytes = text;
                bytes = EncryptionHelper.Encrypt(text);
                File.WriteAllText(filename, bytes);
            }
            catch (IOException e)
            {
                Debug.LogException(e);
            }
        }

        ////////////////////////////////////////////////////////////
        private static void CreateDirectories(string path)
        {
            path = Path.GetDirectoryName(path);
            Directory.CreateDirectory(path);
        }
    }
}
