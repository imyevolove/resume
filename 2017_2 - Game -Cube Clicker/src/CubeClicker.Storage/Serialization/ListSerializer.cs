﻿namespace CubeClicker.Storage.Serialization
{
    using Generic;
    using Serialization;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class ListSerializer : StorageSerializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(IList), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(SerializedStorageNode obj, StorageSerialization provider)
        {
            var collection = (IList)Activator.CreateInstance(Type.GetType(obj.type));

            foreach (var item in (IList)obj.value)
                collection.Add(provider.Deserialize(item));

            return collection;
        }

        ////////////////////////////////////////////////////////////
        public override SerializedStorageNode Serialize(string key, object obj, StorageSerialization provider)
        {
            var serialiedCollection = new List<SerializedStorageNode>();
            var collection = (IList)obj;

            foreach (var c in collection)
                serialiedCollection.Add(provider.Serialize("key", c));

            return new SerializedStorageNode()
            {
                key = key,
                type = collection.GetType().FullName,
                value = serialiedCollection
            };
        }
    }
}
