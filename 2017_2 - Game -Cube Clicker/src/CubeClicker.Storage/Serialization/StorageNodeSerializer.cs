﻿namespace CubeClicker.Storage.Serialization
{
    using Generic;
    using Serialization;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class StorageNodeSerializer : StorageSerializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(StorageNode), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(SerializedStorageNode obj, StorageSerialization provider)
        {
            return new StorageNode()
            {
                key = obj.key,
                value = provider.Deserialize(obj.value)
            };
        }

        ////////////////////////////////////////////////////////////
        public override SerializedStorageNode Serialize(string key, object obj, StorageSerialization provider)
        {
            var data = (StorageNode)obj;
            return new SerializedStorageNode()
            {
                key = key,
                value = provider.Serialize(data.key, data.value),
                type = data.GetType().FullName
            };
        }
    }
}
