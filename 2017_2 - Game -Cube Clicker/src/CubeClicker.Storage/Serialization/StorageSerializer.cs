﻿namespace CubeClicker.Storage.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public abstract class StorageSerializer
    {
        public abstract SerializedStorageNode Serialize(string key, object obj, StorageSerialization provider);
        public abstract object Deserialize(SerializedStorageNode obj, StorageSerialization provider);
        public abstract bool CanHandleType(Type type);
    }
}
