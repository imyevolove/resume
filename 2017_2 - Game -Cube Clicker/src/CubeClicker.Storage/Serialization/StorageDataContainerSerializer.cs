﻿namespace CubeClicker.Storage.Serialization
{
    using Generic;
    using Serialization;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class StorageDataContainerSerializer : StorageSerializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(IStorageContainer), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(SerializedStorageNode obj, StorageSerialization provider)
        {
            var container = new StorageDataContainer();

            foreach (var item in provider.Deserialize<IList>(obj.value))
            {
                var e = (SerializedStorageNode)item;
                container[e.key] = provider.Deserialize(e);
            }

            return container;
        }

        ////////////////////////////////////////////////////////////
        public override SerializedStorageNode Serialize(string key, object obj, StorageSerialization provider)
        {
            List<SerializedStorageNode> serializedCollecionData = new List<SerializedStorageNode>();
            var container = (StorageDataContainer)obj;

            foreach (var item in container.GetData())
                serializedCollecionData.Add(provider.Serialize(item.key, item.value));

            return new SerializedStorageNode()
            {
                key = key,
                type = typeof(StorageDataContainer).FullName,
                value = serializedCollecionData
            };
        }
    }
}
