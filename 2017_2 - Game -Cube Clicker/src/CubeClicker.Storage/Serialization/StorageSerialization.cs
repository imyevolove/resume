﻿namespace CubeClicker.Storage.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class StorageSerialization : ISerializationProvider<StorageSerializer>
    {
        protected HashSet<StorageSerializer> serializers = new HashSet<StorageSerializer>();
        
        ////////////////////////////////////////////////////////////
        public SerializedStorageNode Serialize(string key, object obj)
        {
            var type = obj.GetType();
            var serializer = FindSerializer(type);

            if (serializer == null)
            {
                return new SerializedStorageNode()
                {
                    key = key,
                    type = type.FullName,
                    value = obj
                };
            }

            return serializer.Serialize(key, obj, this);
        }

        ////////////////////////////////////////////////////////////
        public object Deserialize(object obj)
        {
            if (obj is SerializedStorageNode)
            {
                var serializedData = (SerializedStorageNode)obj;
                var type = Type.GetType(serializedData.type);

                if (type == null)
                    throw new Exception("Type is null");

                var serializer = FindSerializer(type);

                if (serializer == null)
                    return serializedData.value;

                return serializer.Deserialize((SerializedStorageNode)obj, this);
            }
            
            return obj;
        }

        ////////////////////////////////////////////////////////////
        public T Deserialize<T>(object obj)
        {
            var result = Deserialize(obj);

            if (result is T)
                return (T)result;

            return default(T);
        }

        ////////////////////////////////////////////////////////////
        protected StorageSerializer FindSerializer(Type type)
        {
            foreach (var serializer in serializers)
            {
                if (serializer.CanHandleType(type))
                    return serializer;
            }
            return null;
        }

        ////////////////////////////////////////////////////////////
        public void AddSerializer(StorageSerializer handler)
        {
            serializers.Add(handler);
        }

        ////////////////////////////////////////////////////////////
        public void RemoveSerializer(StorageSerializer handler)
        {
            serializers.Remove(handler);
        }
    }

    /*
     StorageSerialization serialization = new StorageSerialization();
     StorageSerialization.AddSerializer(new ListSerializer());
     StorageSerialization.AddSerializer(new StorageNode());
     StorageSerialization.AddSerializer(new StorageDataContainerSerializer());

     */
}
