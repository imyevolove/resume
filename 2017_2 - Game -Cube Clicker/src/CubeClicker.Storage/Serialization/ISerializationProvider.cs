﻿namespace CubeClicker.Storage.Serialization
{
    using Serialization;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface ISerializationProvider<TSerializer>
    {
        void AddSerializer(TSerializer handler);
        void RemoveSerializer(TSerializer handler);
    }
}
