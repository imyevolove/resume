﻿namespace CubeClicker.Storage
{
    using Serialization;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    using UnityEngine;

    [Serializable]
    public class StorageDataContainer : IStorageContainer
    {
        protected static readonly StorageSerialization StorageSerialization = new StorageSerialization();
        static StorageDataContainer()
        {
            StorageSerialization.AddSerializer(new StorageNodeSerializer());
            StorageSerialization.AddSerializer(new StorageDataContainerSerializer());
            StorageSerialization.AddSerializer(new ListSerializer());
        }

        protected List<StorageNode> data = new List<StorageNode>();

        [NonSerialized]
        protected readonly StorageNode defaultKVP = default(StorageNode);

        public int Count
        {
            get { return data.Count; }
        }

        public object this[string id]
        {
            get
            {
                return data.Find(pair => { return pair.key == id; }).value;
            }
            set
            {
                var result = data.Find(pair => { return pair.key == id; });
                if (!result.Equals(defaultKVP))
                {
                    data.Remove(result);
                    Debug.LogFormat("Data with id [{0}] was rewrited", id);
                }

                data.Add(new StorageNode() { key = id, value = value });
            }
        }

        ////////////////////////////////////////////////////////////
        public static StorageDataContainer CreateContainer()
        {
            return new StorageDataContainer();
        }

        ////////////////////////////////////////////////////////////
        public bool HasKey(string key)
        {
            var result = data.Find(pair => { return pair.key == key; });
            return !result.Equals(defaultKVP);
        }

        ////////////////////////////////////////////////////////////
        public bool KeyValueIs<T>(string key)
        {
            var result = data.Find(pair => { return pair.key == key; });
            return result.value is T;
        }

        ////////////////////////////////////////////////////////////
        public string ToXml()
        {
            Type[] extraTypes = {
                typeof(StorageNode),
                typeof(SerializedStorageNode),
                typeof(StorageDataContainer),
                typeof(List<SerializedStorageNode>),
                typeof(List<StorageNode>),
                typeof(List<StorageDataContainer>)
            };
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(SerializedStorageNode), extraTypes);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.NewLineHandling = NewLineHandling.None;
            settings.Indent = false;

            StringWriter stringWriter = new StringWriter();

            XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings);

            xmlSerializer.Serialize(xmlWriter, StorageSerialization.Serialize("root", this));

            return stringWriter.ToString();
        }

        ////////////////////////////////////////////////////////////
        public void DeserializeOverwrite(string xml)
        {
            try
            {
                Type[] extraTypes = {
                    typeof(StorageNode),
                    typeof(SerializedStorageNode),
                    typeof(StorageDataContainer),
                    typeof(List<SerializedStorageNode>),
                    typeof(List<StorageNode>),
                    typeof(List<StorageDataContainer>)
                };
                var serializer = new XmlSerializer(typeof(SerializedStorageNode), extraTypes);
                SerializedStorageNode root;

                using (TextReader reader = new StringReader(xml))
                {
                    root = (SerializedStorageNode)serializer.Deserialize(reader);
                }

                var container = StorageSerialization.Deserialize<StorageDataContainer>(root);
                data = container.GetData();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        ////////////////////////////////////////////////////////////
        public static StorageDataContainer Deserialize(string xml)
        {
            try
            {
                Type[] extraTypes = {
                    typeof(StorageNode),
                    typeof(SerializedStorageNode),
                    typeof(StorageDataContainer),
                    typeof(List<SerializedStorageNode>),
                    typeof(List<StorageNode>),
                    typeof(List<StorageDataContainer>)
                };
                var serializer = new XmlSerializer(typeof(SerializedStorageNode), extraTypes);
                SerializedStorageNode root;

                using (TextReader reader = new StringReader(xml))
                {
                    root = (SerializedStorageNode)serializer.Deserialize(reader);
                }

                var container = StorageSerialization.Deserialize<StorageDataContainer>(root);
                return container;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return null;
            }
        }

        ////////////////////////////////////////////////////////////
        public List<StorageNode> GetData()
        {
            return data;
        }
        
    }
}
