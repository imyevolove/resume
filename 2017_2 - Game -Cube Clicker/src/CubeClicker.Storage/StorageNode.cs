﻿namespace CubeClicker.Storage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    [Serializable]
    public struct StorageNode
    {
        public string key;
        public object value;
    }
}
