﻿namespace CubeClicker.Storage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IStorageContainer
    {
        int Count { get; }
        bool HasKey(string key);

        List<StorageNode> GetData();
        string ToXml();
    }
}
