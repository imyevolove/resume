﻿namespace CubeClicker.Storage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IStorageProvider<T>
    {
        T Load(string filename);
        void Save(T obj, string filename);
    }
}
