﻿using CubeClicker.Behaviours.Weapons;
using CubeClicker.Business.Teams;
using CubeClicker.DamageSystem;
using CubeClicker.Pools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CubeClicker.Behaviours.Units
{
    [Serializable]
    [RequireComponent(typeof(IDamageReceiver))]
    public class UnitBehaviour : PoolableBehaviour
    {
        private static List<UnitBehaviour> m_All = new List<UnitBehaviour>();
        private static IReadOnlyCollection<UnitBehaviour> m_AllReadonly;

        public static IReadOnlyCollection<UnitBehaviour> All
        {
            get
            {
                if (m_AllReadonly == null)
                    m_AllReadonly = m_All.AsReadOnly();
                return m_AllReadonly;
            }
        }

        [SerializeField] public TeamCode team;
        [SerializeField] public Sprite icon;
        [SerializeField] public Weapon weapon;

        [SerializeField] public float price;
        [SerializeField] public float priceFactor;

        [SerializeField] protected string m_UnitName;
        public string UnitName => m_UnitName;

        private IDamageReceiver m_DamageReceiver;
        public IDamageReceiver DamageReceiver
        {
            get
            {
                if (m_DamageReceiver == null)
                    m_DamageReceiver = GetComponent<IDamageReceiver>();
                return m_DamageReceiver;
            }
        }

        protected virtual void Awake()
        {
            m_All.Add(this);
        }

        protected virtual void OnDestroy()
        {
            m_All.Remove(this);
        }
    }
}
