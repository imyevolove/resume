﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.StateMachines
{
    public interface IStateTransition<T>
    {
        T           GetPreviousState();
        void        SetPreviousState(T state);

        T           GetNextState();
        void        SetNextState(T state);

        Callback    GetCallback();
        void        SetCallback(Callback callback);
        void        InvokeCallback();

        bool        Has(T previousState);
        bool        Has(T previousState, T nextState);
        bool        Has(T previousState, T nextState, Callback callback);
    }
}
