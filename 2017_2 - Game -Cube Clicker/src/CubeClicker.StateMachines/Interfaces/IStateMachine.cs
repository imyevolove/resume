﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.StateMachines
{
    public interface IStateMachine<T>
    {
        void RegisterState(T previousState, T nextState, Callback callback);
        void SetState(T state);
    }
}
