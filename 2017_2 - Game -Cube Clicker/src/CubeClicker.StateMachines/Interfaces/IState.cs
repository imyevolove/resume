﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.StateMachines
{
    public interface IState
    {
        Callback    GetCallback();
        void        SetCallback(Callback callback);

        void        InvokeCallback();
    }
}
