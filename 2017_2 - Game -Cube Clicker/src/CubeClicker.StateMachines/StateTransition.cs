﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.StateMachines
{
    public class StateTransition<T> : IStateTransition<T>
    {
        protected T previousState;
        protected T nextState;
        protected Callback callback;

        ////////////////////////////////////////////////////////////
        public StateTransition() {}

        ////////////////////////////////////////////////////////////
        public StateTransition(T previousState, T nextState, Callback callback)
        {
            this.previousState  = previousState;
            this.nextState      = nextState;
            this.callback = callback;
        }

        ////////////////////////////////////////////////////////////
        public T GetPreviousState()
        {
            return previousState;
        }

        ////////////////////////////////////////////////////////////
        public void SetPreviousState(T state)
        {
            previousState = state;
        }

        ////////////////////////////////////////////////////////////
        public T GetNextState()
        {
            return nextState;
        }

        ////////////////////////////////////////////////////////////
        public void SetNextState(T state)
        {
            nextState = state;
        }

        ////////////////////////////////////////////////////////////
        public Callback GetCallback()
        {
            return callback;
        }

        ////////////////////////////////////////////////////////////
        public void SetCallback(Callback callback)
        {
            this.callback = callback;
        }

        ////////////////////////////////////////////////////////////
        public void InvokeCallback()
        {
            callback();
        }

        ////////////////////////////////////////////////////////////
        public bool Has(T previousState)
        {
            return this.previousState   .Equals(previousState);
        }

        ////////////////////////////////////////////////////////////
        public bool Has(T previousState, T nextState)
        {
            return this.previousState   .Equals(previousState) 
                && this.nextState       .Equals(nextState);
        }

        ////////////////////////////////////////////////////////////
        public bool Has(T previousState, T nextState, Callback callback)
        {
            return this.previousState   .Equals(previousState)
                && this.nextState       .Equals(nextState)
                && this.callback        .Equals(callback);
        }
    }
}
