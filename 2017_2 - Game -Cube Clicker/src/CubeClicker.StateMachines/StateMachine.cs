﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CubeClicker.StateMachines
{
    public class StateMachine<T> : IStateMachine<T>
    {
        protected List<StateTransition<T>> transitions = new List<StateTransition<T>>();
        protected Dictionary<T, Callback> states = new Dictionary<T, Callback>();

        protected T currentState;
        protected Stack<T> stack = new Stack<T>();

        protected Callback[] m_UpdateCallbacks;

        private int m_UpdateCallbackIndex;

        public StateMachine()
        {
            UpdateUpdatableCallbacks();
        }

        ////////////////////////////////////////////////////////////
        public void RegisterStateUpdate(T state, Callback callback)
        {
            if (states.ContainsKey(state))
            {
                Debug.LogWarning($"State {state} already exists");
                return;
            }
            states.Add(state, callback);
            UpdateUpdatableCallbacks();
        }

        ////////////////////////////////////////////////////////////
        public void UnregisterStateUpdate(T state)
        {
            states.Remove(state);
        }
        
        ////////////////////////////////////////////////////////////
        public void Update()
        {
            for (m_UpdateCallbackIndex = 0; m_UpdateCallbackIndex < m_UpdateCallbacks.Length; m_UpdateCallbackIndex++)
                m_UpdateCallbacks[m_UpdateCallbackIndex]();
        }

        ////////////////////////////////////////////////////////////
        public void RegisterState(T previousState, T nextState, Callback callback)
        {
            transitions.Add(new StateTransition<T>(previousState, nextState, callback));
        }

        ////////////////////////////////////////////////////////////
        public bool UnregisterState(T previousState, T nextState, Callback callback)
        {
            return transitions.RemoveAll(transition => { return transition.Has(previousState, nextState, callback); }) > 0;
        }

        ////////////////////////////////////////////////////////////
        public void SelectPreviousState()
        {
            if (stack.Count == 0) return;

            var previous = currentState;
            currentState = stack.Pop();

            ApplyStateChanges(previous, currentState);

            return;
        }

        ////////////////////////////////////////////////////////////
        public void SetState(T state)
        {
            if (currentState.Equals(state)) return;

            stack.Push(currentState);
            currentState = state;

            UpdateUpdatableCallbacks();
            ApplyStateChanges();
        }

        ////////////////////////////////////////////////////////////
        public void Reset()
        {
            currentState = default(T);
            stack.Clear();
        }

        ////////////////////////////////////////////////////////////
        protected void ApplyStateChanges()
        {
            if (currentState != null && stack.Count != 0)
            {
                ApplyStateChanges(stack.Peek(), currentState);
            }
        }

        ////////////////////////////////////////////////////////////
        protected void ApplyStateChanges(T previousState, T nextState)
        {
            if (previousState != null && nextState != null)
            {
                foreach (var transition in transitions)
                {
                    if (!transition.Has(previousState, nextState)) continue;
                    transition.InvokeCallback();
                }
            }
        }

        protected void UpdateUpdatableCallbacks()
        {
            m_UpdateCallbacks = states.Where(s => s.Key.Equals(currentState)).Select(s => s.Value).ToArray();
        }
    }
}
