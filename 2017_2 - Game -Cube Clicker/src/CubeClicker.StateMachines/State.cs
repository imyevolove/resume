﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.StateMachines
{
    public class State : IState
    {
        protected Callback callback;

        ////////////////////////////////////////////////////////////
        public State() {}

        ////////////////////////////////////////////////////////////
        public State(Callback callback)
        {
            this.callback   = callback;
        }

        ////////////////////////////////////////////////////////////
        public Callback GetCallback()
        {
            return callback;
        }

        ////////////////////////////////////////////////////////////
        public void SetCallback(Callback callback)
        {
            this.callback = callback;
        }

        ////////////////////////////////////////////////////////////
        public void InvokeCallback()
        {
            callback();
        }
    }
}
