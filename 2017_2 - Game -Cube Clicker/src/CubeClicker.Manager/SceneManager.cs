﻿namespace CubeClicker.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.SceneManagement;
    using USceneManager = UnityEngine.SceneManagement.SceneManager;

    public class SceneManager
    {
        public const string SCENE_GAME     = "scn_Game";
        public const string SCENE_MENU     = "scn_Menu";
        public const string SCENE_STARTUP  = "scn_Startup";

        ////////////////////////////////////////////////////////////
        public static void LoadSceneAsync(string sceneName, UnityAction eventHandler)
        {
            UnityAction<Scene, LoadSceneMode> onLoadAction = null;
            onLoadAction = (scene, mode) => {
                if (scene.name == sceneName)
                {
                    USceneManager.sceneLoaded -= onLoadAction;
                    eventHandler();

                    Debug.Log("Scene Async was loaded successfully");
                }
            };

            USceneManager.sceneLoaded += onLoadAction;
            USceneManager.LoadSceneAsync(sceneName);
        }

        ////////////////////////////////////////////////////////////
        public static void LoadScene(string sceneName)
        {
            USceneManager.LoadScene(sceneName);
        }
    }
}
