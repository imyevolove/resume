﻿namespace CubeClicker.Managers
{
    using CubeClicker.Behaviours.Units;
    using CubeClicker.Manager;
    using System;
    using System.Linq;
    using UnityEngine;

    public class UnitUtility
    {
        public static float spawnDistanceMin = 5f;
        public static float spawnDistanceMax = 15f;

        public static float rotatorAngleMin = 5f;
        public static float rotatorAngleMax = 15f;

        private static CubeUnitBehaviour CubeUnit;

        ////////////////////////////////////////////////////////////
        public static UnitBehaviour InstantiateUnit(UnitsManager unitManager, UnitBehaviour original)
        {
            if (original == null)
                throw new ArgumentNullException(nameof(original));

            UnitBehaviour unit = GameObject.Instantiate(original);
            unit.transform.position = GetRandomSpawnPoint();

            return unit;
        }

        ////////////////////////////////////////////////////////////
        public static CubeUnitBehaviour GetCubeUnit()
        {
            if (CubeUnit == null)
                CubeUnit = Resources.FindObjectsOfTypeAll<CubeUnitBehaviour>().FirstOrDefault();
            return CubeUnit;
        }

        ////////////////////////////////////////////////////////////
        public static float GetRandomRotatorAngle()
        {
            return UnityEngine.Random.Range(rotatorAngleMin, rotatorAngleMax);
        }

        ////////////////////////////////////////////////////////////
        public static Vector3 GetRandomSpawnPoint()
        {
            CubeUnitBehaviour cube = GetCubeUnit();

            if (cube == null) return Vector3.zero;

            Vector3 distance = new Vector3(
                UnityEngine.Random.Range(spawnDistanceMin, spawnDistanceMax) * RandomPositiveNegative(),
                UnityEngine.Random.Range(spawnDistanceMin, spawnDistanceMax) * RandomPositiveNegative(),
                UnityEngine.Random.Range(spawnDistanceMin, spawnDistanceMax) * RandomPositiveNegative()
                );

            return distance + cube.transform.position;
        }

        ////////////////////////////////////////////////////////////
        private static int RandomPositiveNegative()
        {
            return UnityEngine.Random.Range(0, 2) * 2 - 1;
        }
    }
}
