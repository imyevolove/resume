﻿using CubeClicker.Behaviours.Units;
using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Pools;
using CubeClicker.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace CubeClicker.Manager
{
    public class UnitDescriptor
    {
        public readonly UnityEvent onChange = new UnityEvent();

        public readonly UnitsManager Manager;
        public readonly UnitBehaviour UnitSource;
        public readonly List<UnitBehaviour> UnitInstances = new List<UnitBehaviour>();
        public readonly IWeaponAbilitySystem WeaponAbilitySystem;

        private int m_UnitCount;
        public int UnitCount
        {
            get { return m_UnitCount; }
            set
            {
                m_UnitCount = value;
                onChange.Invoke();
            }
        }

        public float GetPrice()
        {
            return (UnitSource.price + UnitCount) * Mathf.Pow(UnitSource.priceFactor, UnitCount);
        }

        public readonly MonoBehaviourPool<UnitBehaviour> UnitPool;

        public UnitDescriptor(UnitsManager manager, UnitBehaviour unitSource, IWeaponAbilitySystem weaponAbilitySystem)
        {
            WeaponAbilitySystem = weaponAbilitySystem;
            Manager = manager;
            UnitSource = unitSource;
            UnitPool = new MonoBehaviourPool<UnitBehaviour>(unitSource);
        }
    }
}
