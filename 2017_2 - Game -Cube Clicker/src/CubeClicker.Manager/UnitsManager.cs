﻿using CubeClicker.Behaviours.Units;
using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Managers;
using CubeClicker.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CubeClicker.Manager
{
    public class UnitsManager : IStorable
    {
        protected List<UnitDescriptor> m_UnitDescriptors = new List<UnitDescriptor>();

        public struct UnitRegistrationInfo
        {
            public readonly UnitBehaviour Unit;
            public readonly IWeaponAbilitySystem WeaponAbilitySystem;

            public UnitRegistrationInfo(UnitBehaviour unit, IWeaponAbilitySystem weaponAbilitySystem)
            {
                Unit = unit;
                WeaponAbilitySystem = weaponAbilitySystem;
            }
        }

        ////////////////////////////////////////////////////////////
        public IEnumerable<UnitBehaviour> GetAllUnits()
        {
            return m_UnitDescriptors.Select(d => d.UnitSource);
        }

        ////////////////////////////////////////////////////////////
        public IEnumerable<UnitDescriptor> GetAllDescriptors()
        {
            return m_UnitDescriptors.AsEnumerable();
        }

        ////////////////////////////////////////////////////////////
        public bool PurchaseUnit(UnitBehaviour source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var descriptor = TryGetDescriptor(source);
            if (descriptor == null)
                throw new NullReferenceException(nameof(descriptor));

            var price = descriptor.GetPrice();
            if (PlayerManager.mine.Money.Value >= price)
            {
                PlayerManager.mine.Money.Value -= price;
                InstantiateUnit(source);

                return true;
            }

            return false;
        }

        ////////////////////////////////////////////////////////////
        public UnitBehaviour InstantiateUnit(int index)
        {
            return InstantiateUnit(m_UnitDescriptors[index].UnitSource);
        }

        ////////////////////////////////////////////////////////////
        public UnitBehaviour InstantiateUnit(UnitBehaviour source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var descriptor = TryGetDescriptor(source);
            if (descriptor == null)
                throw new NullReferenceException(nameof(descriptor));

            descriptor.UnitCount += 1;

            return InstantiateUnitWitoutCountRegisteration(descriptor);
        }

        ////////////////////////////////////////////////////////////
        public void AddUnitCount(UnitBehaviour source, int value)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var descriptor = TryGetDescriptor(source);
            if (descriptor == null)
                throw new NullReferenceException(nameof(descriptor));

            descriptor.UnitCount += Mathf.Abs(value);
        }

        ////////////////////////////////////////////////////////////
        public void SetUnitCount(UnitBehaviour source, int value)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var descriptor = TryGetDescriptor(source);
            if (descriptor == null)
                throw new NullReferenceException(nameof(descriptor));

            descriptor.UnitCount = Mathf.Abs(value);
        }
        
        ////////////////////////////////////////////////////////////
        public int GetUnitCount(UnitBehaviour source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var descriptor = TryGetDescriptor(source);
            if (descriptor == null)
                throw new NullReferenceException(nameof(descriptor));

            return descriptor.UnitCount;
        }

        ////////////////////////////////////////////////////////////
        public int GetSummaryUnitsCount() => m_UnitDescriptors.Sum(d => d.UnitCount);

        ////////////////////////////////////////////////////////////
        public void RegisterUnits(IEnumerable<UnitRegistrationInfo> unitSources)
        {
            if (unitSources == null)
                throw new ArgumentNullException(nameof(unitSources));

            foreach(var source in unitSources)
                RegisterDescriptorIfNotExists(source.Unit, source.WeaponAbilitySystem);
        }
        
        ////////////////////////////////////////////////////////////
        public void SpawnAllUnits()
        {
            foreach (var descriptor in m_UnitDescriptors)
                SpawnUnitsByDescriptor(descriptor);
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();
            List<StorageDataContainer> descriptorContainers = new List<StorageDataContainer>();

            foreach (var descriptor in m_UnitDescriptors)
            {
                var descriptorData = new StorageDataContainer();
                descriptorData["id"] = descriptor.UnitSource.UnitName;
                descriptorData["units_count"] = descriptor.UnitCount;
                descriptorData["abilities"] = descriptor.WeaponAbilitySystem.GetStorageData();
                descriptorContainers.Add(descriptorData);
            }

            data["descriptors"] = descriptorContainers;
            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<List<StorageDataContainer>>("descriptors"))
            {
                var descriptors = (List<StorageDataContainer>)storageData["descriptors"];
                UnitDescriptor tmpDescriptor;

                foreach (var descriptorData in descriptors)
                {
                    tmpDescriptor = null;

                    if (!descriptorData.KeyValueIs<string>("id")) continue;

                    tmpDescriptor = m_UnitDescriptors.FirstOrDefault(d => d.UnitSource.UnitName == (string)descriptorData["id"]);
                    if (tmpDescriptor == null) continue;

                    if (descriptorData.KeyValueIs<int>("units_count"))
                    {
                        tmpDescriptor.UnitCount = (int)descriptorData["units_count"];
                    }

                    if (descriptorData.KeyValueIs<StorageDataContainer>("abilities"))
                    {
                        tmpDescriptor.WeaponAbilitySystem.SetStorageData((StorageDataContainer)descriptorData["abilities"]);
                    }
                }
            }
        }

        private void SpawnUnitsByDescriptor(UnitDescriptor descriptor)
        {
            for (var i = 0; i < descriptor.UnitCount; i++)
                InstantiateUnitWitoutCountRegisteration(descriptor);
        }

        private UnitDescriptor GetOrCreateUnitDescriptor(UnitBehaviour unitSource, IWeaponAbilitySystem weaponAbilitySystem)
        {
            return RegisterDescriptorIfNotExists(unitSource, weaponAbilitySystem);
        }

        private UnitDescriptor TryGetDescriptor(UnitBehaviour unitSource)
        {
            return m_UnitDescriptors.FirstOrDefault(d => d.UnitSource == unitSource);
        }

        private UnitDescriptor RegisterDescriptorIfNotExists(UnitBehaviour unitSource, IWeaponAbilitySystem weaponAbilitySystem)
        {
            var descriptor = m_UnitDescriptors.FirstOrDefault(d => d.UnitSource == unitSource);

            if (descriptor == null)
            {
                descriptor = new UnitDescriptor(this, unitSource, weaponAbilitySystem);
                m_UnitDescriptors.Add(descriptor);
            }

            return descriptor;
        }

        private UnitBehaviour InstantiateUnitWitoutCountRegisteration(UnitDescriptor descriptor)
        {
            if (descriptor == null)
                throw new ArgumentNullException(nameof(descriptor));

            var unit = UnitUtility.InstantiateUnit(this, descriptor.UnitSource);
            unit.weapon.AbilitiesSystem = descriptor.WeaponAbilitySystem;
            unit.weapon.Initialize();

            return unit;
        }
    }
}
