﻿using CubeClicker.Personal;
using System.Collections.Generic;
using UnityEngine;

namespace CubeClicker.Managers
{
    public class PlayerManager
    {
        public static IPlayer mine { get; private set; }
        public static int Count { get { return players.Count; } }

        private static List<IPlayer> players = new List<IPlayer>();

        ////////////////////////////////////////////////////////////
        public static void RegisterPlayer(IPlayer player)
        {
            if (players.Contains(player))
            {
                Debug.Log("Player already added");
                return;
            }

            players.Add(player);
        }

        ////////////////////////////////////////////////////////////
        public static void RegisterMinePlayer(IPlayer player)
        {
            mine = player;

            if (players.Contains(player)) return;
            players.Add(player);
        }

        ////////////////////////////////////////////////////////////
        public static bool UnregisterPlayer(IPlayer player)
        {
            return players.Remove(player);
        }
    }
}
