﻿using CubeClicker.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.Managers
{
    public class GameManager
    {
        public static IApplication Application { get; private set; }

        ////////////////////////////////////////////////////////////
        public static void InitializeGame<TApp>() where TApp : class, IApplication, new()
        {
            Application = new TApp();
            Application.Initialize();
        }
    }
}
