﻿using CubeClicker.Storage;
using System;
using UnityEngine;

namespace CubeClicker.Personal.Settings
{
    public class ProfileSettings : IStorable
    {
        public Action onChange;

        ////////////////////////////////////////////////////////////
        public string qualitySettings
        {
            get
            {
                return QualitySettings.names[qualitySettingsIndex];
            }
            set
            {
                string[] names = QualitySettings.names;
                int index = Array.FindIndex(names, name => { return name == value; });

                if (index < 0) return;
                qualitySettingsIndex = index;
            }
        }

        ////////////////////////////////////////////////////////////
        public int qualitySettingsIndex
        {
            get
            {
                return Mathf.Clamp(mQualitySettingsIndex, 0, QualitySettings.names.Length);
            }
            set
            {
                mQualitySettingsIndex = Mathf.Clamp(value, 0, QualitySettings.names.Length);
                CallOnChangeEvent();
            }
        }

        private int mQualitySettingsIndex;

        ////////////////////////////////////////////////////////////
        public void Apply()
        {
            QualitySettings.SetQualityLevel(mQualitySettingsIndex);
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["quality_settings_index"] = mQualitySettingsIndex;

            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return;

            if (storageData.KeyValueIs<int>("quality_settings_index"))
            {
                mQualitySettingsIndex = (int)storageData["quality_settings_index"];
            }
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnChangeEvent()
        {
            if (onChange == null) return;
            onChange();
        }
    }
}
