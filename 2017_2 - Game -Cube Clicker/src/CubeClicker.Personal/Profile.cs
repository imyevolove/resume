﻿using CubeClicker.Personal.Settings;
using CubeClicker.Storage;
using System;

namespace CubeClicker.Personal
{
    public class Profile : IStorable
    {
        public static Profile current { get; protected set; }

        public string profileStorageFilePath
        {
            get
            {
                return Storage.Storage.MakePath(Storage.Storage.persistentDataPath, "QCSW", "usr_" + profileID + ".pfl");
            }
        }

        public readonly string profileID;
        public readonly ProfileSettings settings = new ProfileSettings();

        public event Action<bool> onVipStatusChange;

        public bool vipStatus {
            get { return mVipStatus; }
            set
            {
                mVipStatus = value;
                if (onVipStatusChange != null)
                    onVipStatusChange(vipStatus);
            }
        }
        private bool mVipStatus;

        ////////////////////////////////////////////////////////////
        public static void SelectProfile(Profile profile)
        {
            current = profile;
        }

        ////////////////////////////////////////////////////////////
        public Profile(string id)
        {
            profileID = id;
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["settings"] = settings.GetStorageData();
            data["vipStatus"] = vipStatus;

            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return;

            /// Restore settings
            if (storageData.KeyValueIs<StorageDataContainer>("settings"))
            {
                settings.SetStorageData((StorageDataContainer)storageData["settings"]);
            }

            /// Restore vip status
            if (storageData.KeyValueIs<bool>("vipStatus"))
            {
                vipStatus = (bool)storageData["vipStatus"];
            }
        }
    }
}
