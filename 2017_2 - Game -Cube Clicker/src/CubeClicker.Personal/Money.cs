﻿namespace CubeClicker.Personal
{
    using CubeClicker.Economy;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [Serializable]
    public class Money : ICurrencyWallet<float>
    {
        public event MoneyEventHandler  onChange;
        public event Action<float>       onChangeWithValue;

        public float Value
        {
            get
            {
                return mValue;
            }
            set
            {
                var difference = value - mValue;
                mValue = value;

                if (onChangeWithValue != null)
                    onChangeWithValue(difference);

                CallOnChangeEvent();
            }
        }

        [SerializeField]
        private float mValue;

        ////////////////////////////////////////////////////////////
        private void CallOnChangeEvent()
        {
            if (onChange == null) return;
            onChange(Value);
        }
    }
}
