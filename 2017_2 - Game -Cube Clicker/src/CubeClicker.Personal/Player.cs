﻿namespace CubeClicker.Personal
{
    using CubeClicker.Manager;
    using CubeClicker.Storage;
    using UnityEngine;

    public class Player : IPlayer
    {
        private string m_Id;

        public string PlayerStorageFilePath
        {
            get
            {
                return Storage.MakePath(Storage.persistentDataPath, "QCSW", "player_" + m_Id + ".pr");
            }
        }

        public Money            Money           { get; private set; }
        public UnitsManager     UnitsManager    { get; private set; }
        public PlayerStatistics Statistics      { get; private set; }

        ////////////////////////////////////////////////////////////
        public Player(string id, UnitsManager unitsManager)
        {
            m_Id = id;

            Money           = new Money();
            UnitsManager    = unitsManager;
            Statistics      = new PlayerStatistics(this);
        }

        ////////////////////////////////////////////////////////////
        public string GetPlayerID()
        {
            return m_Id;
        }

        ////////////////////////////////////////////////////////////
        public void SetPlayerID(string id)
        {
            m_Id = id;
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();
            data["player_money_value"] = Money.Value;
            data["player_units_data"] = UnitsManager.GetStorageData();
            data["player_stats_data"] = Statistics.GetStorageData();

            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<float>("player_money_value"))
            {
                Money.Value = (float)storageData["player_money_value"];
            }

            if (storageData.KeyValueIs<StorageDataContainer>("player_units_data"))
            {
                UnitsManager.SetStorageData(storageData["player_units_data"] as StorageDataContainer);
            }

            if (storageData.KeyValueIs<StorageDataContainer>("player_stats_data"))
            {
                Statistics.SetStorageData(storageData["player_stats_data"] as StorageDataContainer);
            }
        }
    }
}
