﻿namespace CubeClicker.Personal
{
    using Stats;
    using Storage;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PlayerStatistics : IStorable
    {
        public readonly StatisticalField<float> summaryMoney;

        public event Action onChange;

        ////////////////////////////////////////////////////////////
        public PlayerStatistics(IPlayer player)
        {
            summaryMoney = new StatisticalField<float>();
            summaryMoney.onChangeFree += CallOnChangeEvent;

            player.Money.onChangeWithValue += (value) => 
            {
                /// Decrement event. Break
                if (value < 0) return;

                /// Add earn money
                summaryMoney.Set(summaryMoney.Get() + value);
            };
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnChangeEvent()
        {
            if (onChange != null)
                onChange();
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["summary_money"] = summaryMoney.Get();

            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<float>("summary_money"))
            {
                summaryMoney.Set((float)storageData["summary_money"]);
            }
        }
    }
}
