﻿namespace CubeClicker.Personal
{
    using CubeClicker.Manager;
    using CubeClicker.Storage;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IPlayer : IStorable
    {
        string PlayerStorageFilePath { get; }

        string  GetPlayerID();
        void    SetPlayerID(string id);

        Money               Money           { get; }
        UnitsManager        UnitsManager    { get; }
        PlayerStatistics    Statistics      { get; }
    }
}
