﻿namespace CubeClicker.Personal.Stats
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class StatisticalField<TType> : IStatisticalField<TType>
    {
        protected TType mValue;

        public event Action<TType>  onChange;
        public event Action         onChangeFree;

        ////////////////////////////////////////////////////////////
        public virtual TType Get()
        {
            return mValue;
        }

        ////////////////////////////////////////////////////////////
        public virtual void Set(TType value)
        {
            mValue = value;

            CallOnChangeEvent();
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnChangeEvent()
        {
            if (onChange != null)
                onChange(mValue);

            if (onChangeFree != null)
                onChangeFree();
        }
    }
}
