﻿using System;

namespace CubeClicker.Personal.Stats
{
    public interface IStatisticalField<TType>
    {
        event Action<TType> onChange;
        event Action        onChangeFree;

        void    Set(TType value);
        TType   Get();
    }
}
