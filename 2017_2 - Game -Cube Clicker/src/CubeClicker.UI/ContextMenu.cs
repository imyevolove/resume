﻿namespace CubeClicker.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable, RequireComponent(typeof(VerticalLayoutGroup), typeof(ContentSizeFitter))]
    public class ContextMenu : MonoBehaviour
    {
        [SerializeField]
        public RectTransform contextMenuRoot;

        ////////////////////////////////////////////////////////////
        public void ShowContextMenu()
        {
            contextMenuRoot.gameObject.SetActive(true);
        }

        ////////////////////////////////////////////////////////////
        public void HideContextMenu()
        {
            contextMenuRoot.gameObject.SetActive(false);
        }

        ////////////////////////////////////////////////////////////
        public void NextContextMenuState()
        {
            contextMenuRoot.gameObject.SetActive(!contextMenuRoot.gameObject.activeSelf);
        }
    }
}
