﻿namespace CubeClicker.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable, RequireComponent(typeof(RectTransform)), ExecuteInEditMode]
    public class InheritSize : MonoBehaviour
    {
        [SerializeField]
        public RectTransform inheritedTransform;

        [SerializeField]
        public bool inheritWidth;

        [SerializeField]
        public bool inheritHeight;

        ////////////////////////////////////////////////////////////
        public void Recalculate()
        {
            if (inheritedTransform == null) return;
            if (!inheritHeight && !inheritWidth) return;

            var rt = transform as RectTransform;
            var size = rt.sizeDelta;

            if (inheritWidth)
                size.x = inheritedTransform.sizeDelta.x;

            if (inheritHeight)
                size.x = inheritedTransform.sizeDelta.y;

            rt.sizeDelta = size;
        }

        ////////////////////////////////////////////////////////////
        private void Update()
        {
            Recalculate();
        }
    }
}
