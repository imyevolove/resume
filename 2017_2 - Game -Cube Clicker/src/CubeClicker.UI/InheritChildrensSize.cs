﻿namespace CubeClicker.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    [Serializable, RequireComponent(typeof(RectTransform)), ExecuteInEditMode]
    public class InheritChildrensSize : MonoBehaviour
    {
        [SerializeField]
        public bool inheritWidth;

        [SerializeField]
        public bool inheritHeight;

        [SerializeField]
        LayoutType layoutType;

        public enum LayoutType
        {
            Vertical,
            Horizontal
        }

        ////////////////////////////////////////////////////////////
        public void Recalculate()
        {
            if (!inheritHeight && !inheritWidth) return;
            if (transform.childCount == 0) return;
            
            var rt = transform as RectTransform;
            Vector2 size = Vector2.zero;

            float tWMax = 0f;
            float tHMax = 0f;

            foreach (RectTransform child in transform as RectTransform)
            {
                if (inheritWidth)
                    tWMax = Mathf.Max(size.x, child.sizeDelta.x);

                if (inheritHeight)
                    tHMax = Mathf.Max(size.y, child.sizeDelta.y);

                switch (layoutType)
                {
                    case LayoutType.Horizontal:
                        size.x += tWMax;
                        size.y  = tHMax;
                        break;

                    case LayoutType.Vertical:
                        size.x  = tWMax;
                        size.y += tHMax;
                        break;
                }
            }

            rt.sizeDelta = size;
        }

        ////////////////////////////////////////////////////////////
        private void OnTransformChildrenChanged()
        {
            Recalculate();
        }

        ////////////////////////////////////////////////////////////
        protected void OnDisable()
        {
            Recalculate();
        }
        
        ////////////////////////////////////////////////////////////
        protected void OnEnable()
        {
            Recalculate();
        }

        ////////////////////////////////////////////////////////////
        protected void OnRectTransformDimensionsChange()
        {
            Recalculate();
        }

        ////////////////////////////////////////////////////////////
        protected void OnValidate()
        {
            Recalculate();
        }
    }
}
