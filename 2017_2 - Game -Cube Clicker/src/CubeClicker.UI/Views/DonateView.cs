﻿namespace CubeClicker.UI.Views
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class DonateView : BaseUIView
    {
        [SerializeField]
        public Text title;

        [Header("Controllers")]

        [SerializeField]
        public Button cancelButton;

        [SerializeField]
        public Button continueButton;

        [Header("Slider area")]

        [SerializeField]
        public Slider slider;

        [SerializeField]
        public TextField2 stateField;

        [SerializeField]
        public Text stateDescription;
    }
}
