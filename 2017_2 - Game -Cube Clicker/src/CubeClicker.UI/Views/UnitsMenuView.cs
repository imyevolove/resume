﻿namespace CubeClicker.UI.Views
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class UnitsMenuView : BaseUIView
    {
        [SerializeField]
        public Button closeButton;

        [SerializeField]
        public RectTransform itemsContainer;
    }
}
