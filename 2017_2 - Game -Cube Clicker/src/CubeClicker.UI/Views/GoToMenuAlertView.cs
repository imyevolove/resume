﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class GoToMenuAlertView : BaseUIView
    {
        [SerializeField]
        public Text message;

        [SerializeField]
        public Button continueButton;

        [SerializeField]
        public Button cancelButton;
    }
}
