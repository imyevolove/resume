﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class GameScreenView : BaseUIView
    {
        [SerializeField]
        public Text moneyText;

        [SerializeField]
        public RectTransform vipIndicator;

        [SerializeField]
        public Button unitsMenuButton;

        [SerializeField]
        public Button pauseMenuButton;
    }
}
