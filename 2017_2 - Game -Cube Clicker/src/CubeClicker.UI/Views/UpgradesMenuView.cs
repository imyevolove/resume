﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class UpgradesMenuView : BaseUIView
    {
        [SerializeField]
        public Button closeButton;

        [SerializeField]
        public Image icon;

        [SerializeField]
        public Text title;

        [SerializeField]
        public RectTransform upgradesContainer;
    }
}
