﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class MenuScreenView : BaseUIView
    {
        [SerializeField]
        public Button startGameButton;

        [Header("Quality Settings")]

        [SerializeField]
        public QualitySettingsButton originalQualitySettingsButton;

        [SerializeField]
        public QualitySettingsButton rootQualitySettingsButton;

        [SerializeField]
        public UI.ContextMenu qualitySettingsContextMenu;

        [Header("System monitor")]

        [SerializeField]
        public TextField2 originalSystemMonitorField;

        [SerializeField]
        public RectTransform systemMonitorFieldsContent;
    }
}
