﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class DonateBoardView : BaseUIView
    {
        [SerializeField]
        public Button donateButton;
    }
}
