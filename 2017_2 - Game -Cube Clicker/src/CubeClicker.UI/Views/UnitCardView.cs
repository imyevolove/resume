﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class UnitCardView : BaseUIView
    {
        [Header("Info content")]

        [SerializeField]
        public RectTransform infoTransformRoot;

        [SerializeField]
        public Image icon;

        [SerializeField]
        public Text title;

        [SerializeField]
        public Text price;

        [SerializeField]
        public Button purchaseButton;

        [SerializeField]
        public Button upgradeButton;

        [Header("Locker content")]

        [SerializeField]
        public RectTransform lockerTransformRoot;
    }
}
