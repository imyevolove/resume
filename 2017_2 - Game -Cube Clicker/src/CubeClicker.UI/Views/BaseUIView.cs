﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [Serializable]
    public abstract class BaseUIView : MonoBehaviour
    {
        [SerializeField]
        protected RectTransform rootContainer;
        public RectTransform Root { get { return rootContainer; } }

        ////////////////////////////////////////////////////////////
        public static TView FindUIView<TView>() where TView : BaseUIView
        {
            return FindObjectOfType<TView>();
        }

        ////////////////////////////////////////////////////////////
        public virtual void Hide()
        {
            rootContainer.gameObject.SetActive(false);
        }

        ////////////////////////////////////////////////////////////
        public virtual void Show()
        {
            rootContainer.gameObject.SetActive(true);
        }
    }
}
