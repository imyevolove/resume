﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class DonateResultView : BaseUIView
    {
        [SerializeField]
        public Button continueButton;

        [Header("Success Result")]

        [SerializeField]
        public RectTransform successResultRoot;

        [SerializeField]
        public Text successResultTitle;

        [SerializeField]
        public Text successResultDescription;

        [Header("Failure Result")]

        [SerializeField]
        public RectTransform failureResultRoot;

        [SerializeField]
        public Text failureResultTitle;
    }
}
