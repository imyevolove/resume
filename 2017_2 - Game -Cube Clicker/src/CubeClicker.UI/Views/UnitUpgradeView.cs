﻿namespace CubeClicker.UI.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class UnitUpgradeView : BaseUIView
    {
        [SerializeField]
        public Text title;

        [SerializeField]
        public Slider level;

        [SerializeField]
        public Text price;

        [SerializeField]
        public Button purchaseButton;
    }
}
