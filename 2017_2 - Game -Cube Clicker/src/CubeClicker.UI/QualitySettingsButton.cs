﻿namespace CubeClicker.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class QualitySettingsButton : MonoBehaviour
    {
        [SerializeField]
        public Button button;

        [SerializeField]
        public Text qualityText;
    }
}
