﻿namespace CubeClicker.UI
{
    using StateMachines;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class UIStateMachine : StateMachine<UIStateMachine.States>
    {
        public enum States
        {
            GAME_SCREEN_VIEW,
            UNITS_MENU_VIEW,
            UNIT_UPGRADE_VIEW,
            GOTO_MENU_ALERT
        }
    }
}
