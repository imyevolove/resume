﻿using CubeClicker.Application.Commands;
using CubeClicker.Managers;
using CubeClicker.UI.Views;
using System;
using UnityEngine;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class GoToMenuAlertController : BaseUIController
    {
        protected GoToMenuAlertView mGoToMenuAlertView;
        
        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mGoToMenuAlertView = (GoToMenuAlertView)View;
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mGoToMenuAlertView.cancelButton.onClick.AddListener(UIManager.gameScreenStateMachine.SelectPreviousState);
            mGoToMenuAlertView.continueButton.onClick.AddListener(() => {
                /// Save game
                GameManager.Application.InvokeCommand<SavePlayerProfileDataCommand>();

                /// Reset states
                UIManager.gameScreenStateMachine.Reset();

                /// Load Scene
                GameManager.Application.InvokeCommand<LoadMenuScreenCommand>();
            });
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected void OnEnable()
        {
            Time.timeScale = 0;
        }

        ////////////////////////////////////////////////////////////
        protected void OnDisable()
        {
            Time.timeScale = 1;
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();

            base.Awake();
        }
    }
}
