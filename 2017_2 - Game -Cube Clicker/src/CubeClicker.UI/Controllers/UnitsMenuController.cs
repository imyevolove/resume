﻿using CubeClicker.Animator;
using CubeClicker.Manager;
using CubeClicker.Managers;
using CubeClicker.UI.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class UnitsMenuController : BaseUIController
    {
        [SerializeField]
        protected UnitsMenuView mUnitsMenuView;

        [SerializeField]
        protected UnitCardController unitCardControllerOriginal;

        [SerializeField]
        protected UnityEngine.Animator animator;

        protected BoolAnimatorStateHandler animatorStateHandler;

        protected List<UnitCardController> cards = new List<UnitCardController>();

        ////////////////////////////////////////////////////////////
        public static class AnimatorStates
        {
            public static readonly AnimatorState IDLE           = new AnimatorState("idle");
            public static readonly AnimatorState NORMAL         = new AnimatorState("normal");
            public static readonly AnimatorState TRANSPARENT    = new AnimatorState("transparent");
        }

        ////////////////////////////////////////////////////////////
        public void SetAnimatorState(AnimatorState state)
        {
            animatorStateHandler.SetState(state);
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected void OnEnable()
        {
            UpdateCards();
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateCards()
        {
            float summaryMoney = PlayerManager.mine.Statistics.summaryMoney.Get();
            
            int index = 0;
            foreach (var card in cards)
            {
                var target = card.GetTarget();
                if (target == null) continue;

                if (index == 0)
                {
                    card.SetUnlockVisibility(true);
                    index++;

                    continue;
                }

                card.SetUnlockVisibility(summaryMoney > target.UnitSource.price);
            }
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mUnitsMenuView.closeButton.onClick.AddListener(UIManager.gameScreenStateMachine.SelectPreviousState);
        }

        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mUnitsMenuView = (UnitsMenuView)mView;
            animatorStateHandler = new BoolAnimatorStateHandler(animator);

            RemoveUnitItemsFromCanvas();
            InitializeUnitUIItems();
        }

        ////////////////////////////////////////////////////////////
        protected void InitializeUnitUIItems()
        {
            var unitsManager = PlayerManager.mine.UnitsManager;

            foreach (var descriptors in unitsManager.GetAllDescriptors())
                AddUnitMenuItem(descriptors);
        }

        ////////////////////////////////////////////////////////////
        protected void AddUnitMenuItem(UnitDescriptor unit)
        {
            UnitCardController controller = GameObject.Instantiate<UnitCardController>(unitCardControllerOriginal);
            controller.transform.SetParent(mUnitsMenuView.itemsContainer, false);
            controller.SetTarget(unit);
            controller.Show();

            cards.Add(controller);
        }

        ////////////////////////////////////////////////////////////
        protected void RemoveUnitItemsFromCanvas()
        {
            mUnitsMenuView.itemsContainer.gameObject.RemoveChildren();
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize ();
            SetupEvents();

            base.Awake();
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator WaitForUpdateCards(float time)
        {
            Debug.Log("Start");
            yield return new WaitForSeconds(time);
            Debug.Log("End");
        }
    }
}
