﻿using CubeClicker.Application.Commands;
using CubeClicker.IAPManagement;
using CubeClicker.Managers;
using CubeClicker.Personal;
using CubeClicker.UI.Views;
using System;
using UnityEngine;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class DonateController : BaseUIController
    {
        [SerializeField]
        public DonateResultController donateResultController;

        protected DonateView mDonateView;
        
        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mDonateView = (DonateView)mView;

            SetupSlider();
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mDonateView.cancelButton.onClick.AddListener(UIManager.menuScreenStateMachine.SelectPreviousState);
            mDonateView.continueButton.onClick.AddListener(TryDonate);
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            mDonateView.slider.onValueChanged.AddListener(SliderValueChangeEvent);
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            mDonateView.slider.onValueChanged.RemoveListener(SliderValueChangeEvent);
        }

        ////////////////////////////////////////////////////////////
        protected void SetupSlider()
        {
            mDonateView.slider.wholeNumbers = true;
            mDonateView.slider.minValue = 0;
            mDonateView.slider.maxValue = Mathf.Clamp(IAPManager.productsCount - 1, 0, int.MaxValue);
            mDonateView.slider.value = 0;

            UpdateSliderDetails();
        }

        ////////////////////////////////////////////////////////////
        protected void SliderValueChangeEvent(float vaule)
        {
            UpdateSliderDetails();
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateSliderDetails()
        {
            IProduct product = IAPManager.GetProduct((int)mDonateView.slider.value);
            if (product == null) return;

            mDonateView.stateField.title = product.title;
            mDonateView.stateField.value = product.price;
            mDonateView.stateDescription.text = product.description;
        }

        ////////////////////////////////////////////////////////////
        protected void TryDonate()
        {
            IProduct product = IAPManager.GetProduct((int)mDonateView.slider.value);
            if (product == null) return;

            product.Purchase((data) => {
                donateResultController.Show();

                switch (data.status)
                {
                    case PurchaseStatus.Success:
                        donateResultController.ApplyResultType(DonateResultController.ResultType.Success);

                        Profile.current.vipStatus = true;

                        GameManager.Application.InvokeCommand<ApplyVIPStatusCommand>();
                        GameManager.Application.InvokeCommand<SavePlayerProfileDataCommand>();

                        UIManager.menuScreenStateMachine.SelectPreviousState();

                        break;
                    default:
                        donateResultController.ApplyResultType(DonateResultController.ResultType.Failure);
                        break;
                }
            });
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();

            base.Awake();
        }
    }
}
