﻿using CubeClicker.Abilities;
using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Economy;
using CubeClicker.Formatters;
using CubeClicker.Managers;
using CubeClicker.UI.Views;
using System;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class UnitUpgradeController : BaseUIController
    {
        protected UnitUpgradeView mUnitUpgradeView;
        protected IMoneyFormatter mMoneyFormatter;

        protected WeaponAbility target;

        ////////////////////////////////////////////////////////////
        public void SetTarget(WeaponAbility target)
        {
            /// Disable auto control for old target
            DisableAutomaticControl();

            /// Change target
            this.target = target;

            /// Enable auto control again for new target
            EnableAutomaticControl();

            /// Immediate update interface
            UpdateUI();
        }

        ////////////////////////////////////////////////////////////
        public void Reset()
        {
            DisableAutomaticControl();
            target = null;
        }

        ////////////////////////////////////////////////////////////
        public void SetTitle(string title)
        {
            mUnitUpgradeView.title.text = title;
        }

        ////////////////////////////////////////////////////////////
        public void SetPrice(int price)
        {
            mUnitUpgradeView.price.text = price.ToString();
        }

        ////////////////////////////////////////////////////////////
        public void SetPrice(string price)
        {
            mUnitUpgradeView.price.text = price;
        }

        ////////////////////////////////////////////////////////////
        public void SetLevelVisibility(bool visibility)
        {
            mUnitUpgradeView.level.gameObject.SetActive(visibility);
        }

        ////////////////////////////////////////////////////////////
        public void SetLevelValue(float value, float maxValue)
        {
            mUnitUpgradeView.level.maxValue = maxValue;
            mUnitUpgradeView.level.value = value;
        }
        
        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mUnitUpgradeView.purchaseButton.onClick.AddListener(PurchaseTarget);
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            mUnitUpgradeView = (UnitUpgradeView)View;
            mMoneyFormatter = GameManager.Application.RevokeObject<IMoneyFormatter>();

            SetupEvents();
            base.Awake();
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            if (target == null) return;

            target.onChanged.AddListener(OnTargetChanged);
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            if (target == null) return;

            target.onChanged.RemoveListener(OnTargetChanged);
        }

        ////////////////////////////////////////////////////////////
        protected void PurchaseTarget()
        {
            if (target == null) return;

            var purchaseable = target as IPurchaseable;
            if (purchaseable == null) return;

            purchaseable.Purchase(PlayerManager.mine.Money);
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateUI()
        {
            if (target == null) return;

            var titleName = target.Name;
            var titleCount = string.Format("{0:0.##}", target.Value);
            var title = string.Format("{0} [{1}]", titleName, titleCount);
            var price = target.IsLevelMaximized ? "MAX" : mMoneyFormatter.Format(target.Price);

            SetTitle(title);
            SetPrice(price);

            if (target.MaxLevel == Int32.MaxValue)
            {
                SetLevelVisibility(false);
            }
            else
            {
                SetLevelVisibility(true);
                SetLevelValue(target.Level, target.MaxLevel);
            }
        }

        private void OnTargetChanged(AbilityEventData value)
        {
            UpdateUI();
        }
    }
}
