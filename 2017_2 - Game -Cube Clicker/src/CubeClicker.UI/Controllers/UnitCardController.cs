﻿using CubeClicker.Formatters;
using CubeClicker.Manager;
using CubeClicker.Managers;
using CubeClicker.UI.Views;
using System;
using UnityEngine;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class UnitCardController : BaseUIController
    {
        protected UnitCardView      mUnitCardView;
        protected IMoneyFormatter   mMoneyFormatter;

        protected UnitDescriptor target;

        ////////////////////////////////////////////////////////////
        public void SetUnlockVisibility(bool visibility)
        {
            if (mUnitCardView == null || mUnitCardView.infoTransformRoot == null || mUnitCardView.infoTransformRoot.gameObject == null)
                return;

            mUnitCardView.infoTransformRoot.gameObject.SetActive(visibility);
            mUnitCardView.lockerTransformRoot.gameObject.SetActive(!visibility);
        }

        ////////////////////////////////////////////////////////////
        public UnitDescriptor GetTarget()
        {
            return target;
        }

        ////////////////////////////////////////////////////////////
        public void SetTarget(UnitDescriptor unit)
        {
            DisableAutomaticControl();

            target = unit;

            EnableAutomaticControl();
            UpdateUI();
        }

        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mUnitCardView   = (UnitCardView)View;
            mMoneyFormatter = GameManager.Application.RevokeObject<IMoneyFormatter>();
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mUnitCardView.purchaseButton.onClick.AddListener(PurchaseTarget);
            mUnitCardView.upgradeButton.onClick.AddListener(UpgradeTarget);

            /// PlayerManager.mine.Statistics.summaryMoney.onChange += UpdateVisibility;
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateUI()
        {
            if (target == null) return;

            SetPrice(target.GetPrice());
            SetIcon(target.UnitSource.icon);
            SetTitle(target.UnitSource.UnitName);
        }

        ////////////////////////////////////////////////////////////
        public void SetTitle(string title)
        {
            mUnitCardView.title.text = title;
        }

        ////////////////////////////////////////////////////////////
        public void SetIcon(Sprite sprite)
        {
            mUnitCardView.icon.sprite = sprite;
        }

        ////////////////////////////////////////////////////////////
        public void SetPrice(float price)
        {
            mUnitCardView.price.text = mMoneyFormatter.Format(price);
        }

        ////////////////////////////////////////////////////////////
        protected void PurchaseTarget()
        {
            if (target == null) return;

            target.Manager.PurchaseUnit(target.UnitSource);
        }

        ////////////////////////////////////////////////////////////
        protected void UpgradeTarget()
        {
            if (target == null) return;

            UIManager.gameScreenStateMachine.SetState(UIStateMachine.States.UNIT_UPGRADE_VIEW);
            UIManager.FindController<UpgradesMenuController>().SetTarget(target);
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            if (target == null) return;
            target.onChange.AddListener(OnTargetChanged);
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            if (target == null) return;
            target.onChange.RemoveListener(OnTargetChanged);
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();

            base.Awake();
        }

        private void OnTargetChanged()
        {
            SetPrice(target.GetPrice());
        }
    }
}
