﻿using CubeClicker.UI.Views;
using System;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class DonateResultController : BaseUIController
    {
        public enum ResultType
        {
            Success,
            Failure
        }

        protected DonateResultView mDonateResultView;

        ////////////////////////////////////////////////////////////
        public void ApplyResultType(ResultType type)
        {
            switch (type)
            {
                case ResultType.Success:
                    mDonateResultView.successResultRoot.gameObject.SetActive(true);
                    mDonateResultView.failureResultRoot.gameObject.SetActive(false);
                    break;
                case ResultType.Failure:
                    mDonateResultView.successResultRoot.gameObject.SetActive(false);
                    mDonateResultView.failureResultRoot.gameObject.SetActive(true);
                    break;
            }
        }

        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mDonateResultView = (DonateResultView)View;
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mDonateResultView.continueButton.onClick.AddListener(mDonateResultView.Hide);
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();

            base.Awake();
        }
    }
}
