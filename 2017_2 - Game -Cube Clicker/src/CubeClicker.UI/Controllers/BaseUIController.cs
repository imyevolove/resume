﻿namespace CubeClicker.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using Views;

    [Serializable]
    public abstract class BaseUIController : MonoBehaviour
    {
        [SerializeField]
        protected   BaseUIView mView;
        public      BaseUIView View { get { return mView; } }

        ////////////////////////////////////////////////////////////
        public static TView FindUIController<TView>() where TView : BaseUIController
        {
            return FindObjectOfType<TView>();
        }

        ////////////////////////////////////////////////////////////
        public void Show()
        {
            mView.Show();
        }

        ////////////////////////////////////////////////////////////
        public void Hide()
        {
            mView.Hide();
        }

        ////////////////////////////////////////////////////////////
        protected abstract void EnableAutomaticControl();

        ////////////////////////////////////////////////////////////
        protected abstract void DisableAutomaticControl();
    
        ////////////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            DisableAutomaticControl();
            EnableAutomaticControl();
        }

        ////////////////////////////////////////////////////////////
        protected virtual void OnDestroy()
        {
            DisableAutomaticControl();
        }
    }
}
