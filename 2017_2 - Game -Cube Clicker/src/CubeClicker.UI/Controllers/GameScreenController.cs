﻿using CubeClicker.Formatters;
using CubeClicker.Managers;
using CubeClicker.Personal;
using CubeClicker.UI.Views;
using System;
using UnityEngine;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class GameScreenController : BaseUIController
    {
        [SerializeField]
        protected UnitsMenuController unitsMenuController;

        protected GameScreenView    m_GameScreenView;
        protected IMoneyFormatter   m_MoneyFormatter;

        ////////////////////////////////////////////////////////////
        public void UpdateMoney(float value)
        {
            m_GameScreenView.moneyText.text = m_MoneyFormatter.Format(value);
        }

        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            m_GameScreenView.unitsMenuButton.onClick.AddListener(() => {
                UIManager.gameScreenStateMachine.SetState(UIStateMachine.States.UNITS_MENU_VIEW);
            });

            m_GameScreenView.pauseMenuButton.onClick.AddListener(() => {
                UIManager.gameScreenStateMachine.SetState(UIStateMachine.States.GOTO_MENU_ALERT);
            });
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            PlayerManager.mine.Money.onChange += UpdateMoney;
            UpdateMoney(PlayerManager.mine.Money.Value);

            Profile.current.onVipStatusChange += UpdateVIPIndicator;
            UpdateVIPIndicator(Profile.current.vipStatus);
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            PlayerManager.mine.Money.onChange -= UpdateMoney;
            Profile.current.onVipStatusChange -= UpdateVIPIndicator;
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateVIPIndicator(bool status)
        {
            m_GameScreenView.vipIndicator.gameObject.SetActive(status);
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            m_GameScreenView = (GameScreenView)View;
            m_MoneyFormatter = new MoneyFormatterAbbreviation(MoneyFormatterAbbreviation.DEFAULT_ABBREVIATION);

            Initialize();
            SetupEvents();

            base.Awake();
        }

        ////////////////////////////////////////////////////////////
        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}
