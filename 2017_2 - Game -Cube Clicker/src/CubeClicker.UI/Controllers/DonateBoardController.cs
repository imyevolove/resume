﻿namespace CubeClicker.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using Views;

    [Serializable]
    public class DonateBoardController : BaseUIController
    {
        protected DonateBoardView mDonateBoardView;
        
        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mDonateBoardView = (DonateBoardView)mView;
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mDonateBoardView.donateButton.onClick.AddListener(() => {
                UIManager.menuScreenStateMachine.SetState(UIMenuScreenStateMachine.STATE.DONATE_VIEW);
            });
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {

        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {

        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();

            base.Awake();
        }
    }
}
