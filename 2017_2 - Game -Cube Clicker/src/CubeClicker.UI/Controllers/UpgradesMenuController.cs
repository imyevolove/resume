﻿using CubeClicker.Behaviours.Units;
using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Manager;
using CubeClicker.Managers;
using CubeClicker.UI.Views;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class UpgradesMenuController : BaseUIController
    {
        [SerializeField]
        protected UnitUpgradeController unitUpgradeControllerOriginal;
        protected List<UnitUpgradeController> upgradeObjects = new List<UnitUpgradeController>();

        protected UpgradesMenuView  mUpgradesMenuView;
        protected UnitDescriptor     target;

        ////////////////////////////////////////////////////////////
        public void SetTarget(UnitDescriptor unit)
        {
            target = unit;

            DeactivateAllUpgrades();
            ActivateTargetUpgrades();
            UpdateUI();
        }

        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mUpgradesMenuView = (UpgradesMenuView)View;
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mUpgradesMenuView.closeButton.onClick.AddListener(UIManager.gameScreenStateMachine.SelectPreviousState);
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl() {}

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl() {}

        ////////////////////////////////////////////////////////////
        protected void DeactivateAllUpgrades()
        {
            foreach (var upgrade in upgradeObjects)
            {
                upgrade.Reset();
                upgrade.Hide();
            }
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateUI()
        {
            if (target == null) return;

            mUpgradesMenuView.title.text    = target.UnitSource.UnitName;
            mUpgradesMenuView.icon.sprite   = target.UnitSource.icon;
        }

        ////////////////////////////////////////////////////////////
        protected void ActivateTargetUpgrades()
        {
            var abilitiesSystem = target.WeaponAbilitySystem;
            if (abilitiesSystem == null) return;

            for (var i = 0; i < abilitiesSystem.Count; i++)
            {
                AddUpgrade(abilitiesSystem.GetAbility(i));
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(mUpgradesMenuView.upgradesContainer);
        }

        ////////////////////////////////////////////////////////////
        protected void AddUpgrade(WeaponAbility ability)
        {
            if (ability == null) return;

            var controller = GetFreeUpgrade();
            controller.Show();
            controller.SetTarget(ability);
        }

        ////////////////////////////////////////////////////////////
        protected UnitUpgradeController GetFreeUpgrade()
        {
            UnitUpgradeController controller = upgradeObjects.Find(c => { return !c.gameObject.activeSelf; });

            if (controller != null) return controller;
            return CreateAndRegisterNewUpgrade();
        }

        ////////////////////////////////////////////////////////////
        protected UnitUpgradeController CreateAndRegisterNewUpgrade()
        {
            UnitUpgradeController controller = GameObject.Instantiate<UnitUpgradeController>(unitUpgradeControllerOriginal);
            controller.transform.SetParent(mUpgradesMenuView.upgradesContainer.transform, false);
            controller.Hide();

            upgradeObjects.Add(controller);
            return controller;
        }

        ////////////////////////////////////////////////////////////
        protected void ClearUpgrades()
        {
            DeactivateAllUpgrades();
            upgradeObjects.Clear();
            mUpgradesMenuView.upgradesContainer.gameObject.RemoveChildren();
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();
            ClearUpgrades();

            base.Awake();
        }

        ////////////////////////////////////////////////////////////
        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}
