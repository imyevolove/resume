﻿using CubeClicker.Application.Commands;
using CubeClicker.Formatters;
using CubeClicker.Managers;
using CubeClicker.Personal;
using CubeClicker.UI.Views;
using System;
using UnityEngine;

namespace CubeClicker.UI.Controllers
{
    [Serializable]
    public class MenuScreenController : BaseUIController
    {
        protected MenuScreenView mMenuScreenView;

        ////////////////////////////////////////////////////////////
        public void StartGame()
        {
            /// Reset states
            UIManager.gameScreenStateMachine.Reset();

            /// Load scene
            GameManager.Application.InvokeCommand<LoadGameScreenCommand>();
        }

        ////////////////////////////////////////////////////////////
        protected void Initialize()
        {
            mMenuScreenView = (MenuScreenView)mView;

            SetupQualitySettingsUI();
            SetupSystemMonitor();
        }

        ////////////////////////////////////////////////////////////
        protected void SetupEvents()
        {
            mMenuScreenView.startGameButton.onClick.AddListener(StartGame);
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableAutomaticControl()
        {
            Profile.current.settings.onChange += UpdateQualitySettingsButton;
        }

        ////////////////////////////////////////////////////////////
        protected override void DisableAutomaticControl()
        {
            Profile.current.settings.onChange -= UpdateQualitySettingsButton;
        }

        ////////////////////////////////////////////////////////////
        protected void SetupQualitySettingsUI()
        {
            mMenuScreenView.qualitySettingsContextMenu.gameObject.RemoveChildren();

            mMenuScreenView.rootQualitySettingsButton.button.onClick.AddListener(
                mMenuScreenView.qualitySettingsContextMenu.NextContextMenuState);

            var qualityIndex = 0;
            foreach (var qualityName in QualitySettings.names)
            {
                InstantiateQualitySettingsButton(qualityName, qualityIndex);
                qualityIndex++;
            }

            mMenuScreenView.qualitySettingsContextMenu.HideContextMenu();
            UpdateQualitySettingsButton();
        }

        ////////////////////////////////////////////////////////////
        protected void InstantiateQualitySettingsButton(string qualityName, int qualityIndex)
        {
            QualitySettingsButton button = GameObject.Instantiate<QualitySettingsButton>(mMenuScreenView.originalQualitySettingsButton);
            button.transform.SetParent(mMenuScreenView.qualitySettingsContextMenu.transform, false);
            button.qualityText.text = qualityName;
            button.button.onClick.AddListener(() => {
                Profile.current.settings.qualitySettingsIndex = qualityIndex;
                Profile.current.settings.Apply();

                mMenuScreenView.qualitySettingsContextMenu.HideContextMenu();
                GameManager.Application.InvokeCommand<SavePlayerProfileDataCommand>();
            });
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateQualitySettingsButton()
        {
            mMenuScreenView.rootQualitySettingsButton.qualityText.text 
                = string.Format("QUALITY: {0}", Profile.current.settings.qualitySettings);
        }

        ////////////////////////////////////////////////////////////
        protected void SetupSystemMonitor()
        {
            mMenuScreenView.systemMonitorFieldsContent.gameObject.RemoveChildren();

            var moneyFormatter = GameManager.Application.RevokeObject<IMoneyFormatter>();

            AddSystemMonitorField("money", moneyFormatter.Format(PlayerManager.mine.Money.Value));
            AddSystemMonitorField("units", PlayerManager.mine.UnitsManager.GetSummaryUnitsCount().ToString());
        }

        ////////////////////////////////////////////////////////////
        protected void AddSystemMonitorField(string title, string value)
        {
            TextField2 field = GameObject.Instantiate<TextField2>(mMenuScreenView.originalSystemMonitorField);
            field.transform.SetParent(mMenuScreenView.systemMonitorFieldsContent, false);
            field.title = title;
            field.value = value;
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            Initialize();
            SetupEvents();

            base.Awake();
        }
    }
}
