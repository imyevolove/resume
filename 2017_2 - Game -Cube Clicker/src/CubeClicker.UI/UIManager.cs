﻿namespace CubeClicker.UI
{
    using Controllers;
    using System.Linq;
    using UnityEngine;

    public class UIManager
    {
        ////////////////////////////////////////////////////////////
        public static TUIController FindController<TUIController>() where TUIController : BaseUIController
        {
            var objs = Resources.FindObjectsOfTypeAll<TUIController>();
            return objs.FirstOrDefault();
        }

        ////////////////////////////////////////////////////////////
        public readonly static UIStateMachine gameScreenStateMachine = new UIStateMachine();

        ////////////////////////////////////////////////////////////
        public readonly static UIMenuScreenStateMachine menuScreenStateMachine = new UIMenuScreenStateMachine();
    }
}
