﻿namespace CubeClicker.UI
{
    using StateMachines;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class UIMenuScreenStateMachine : StateMachine<UIMenuScreenStateMachine.STATE>
    {
        public enum STATE
        {
            MENU_VIEW,
            DONATE_VIEW
        }
    }
}
