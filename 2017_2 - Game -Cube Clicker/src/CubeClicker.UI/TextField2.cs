﻿namespace CubeClicker.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable]
    public class TextField2 : MonoBehaviour
    {
        [SerializeField]
        protected Text TitleField;

        [SerializeField]
        protected Text ValueField;

        public string title
        {
            get { return TitleField.text; }
            set { TitleField.text = value; }
        }

        public string value
        {
            get { return ValueField.text; }
            set { ValueField.text = value; }
        }
    }
}
