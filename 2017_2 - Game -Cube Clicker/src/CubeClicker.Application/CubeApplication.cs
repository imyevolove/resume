﻿using CubeClicker.Application.Commands;
using CubeClicker.DI;
using CubeClicker.Formatters;
using CubeClicker.Managers;
using CubeClicker.Personal;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CubeClicker.Application
{
    public class CubeApplication : IApplication
    {
        protected Container objectsContainer 
            = new Container();

        protected Dictionary<Type, ApplicationCommand> commandsRegister
            = new Dictionary<Type, ApplicationCommand>();

        ////////////////////////////////////////////////////////////
        public void Initialize()
        {
            ///RegisterObject<IMoneyFormatter>(new MoneyFormatterAbbreviation(MoneyFormatterAbbreviation.DEFAULT_ABBREVIATION));
            RegisterObject<IMoneyFormatter>(new MoneyFormatter());

            /// Active profile
            Profile profile = new Profile("super_player");
            Profile.SelectProfile(profile);

            RegisterCommand(new ConfigureGameDataCommand(this));
            RegisterCommand(new ConfigureIAPCommand(this));
            RegisterCommand(new ConfigureAdsCommand(this));
            RegisterCommand(new ConfigureGameUICommand(this));
            RegisterCommand(new ConfigureMenuUICommand(this));
            RegisterCommand(new LoadMenuScreenCommand(this));
            RegisterCommand(new LoadGameScreenCommand(this));
            RegisterCommand(new ApplyVIPStatusCommand(this));
            RegisterCommand(new ApplyProfileSettingsCommand(this));
            RegisterCommand(new SavePlayerProfileDataCommand(this));
            RegisterCommand(new LoadPlayerProfileDataCommand(this));

            InvokeCommand<ConfigureGameDataCommand>();
            InvokeCommand<ConfigureAdsCommand>();
            InvokeCommand<ConfigureGameUICommand>();
            InvokeCommand<ConfigureMenuUICommand>();
            InvokeCommand<ApplyProfileSettingsCommand>();
            InvokeCommand<LoadPlayerProfileDataCommand>();
            InvokeCommand<ConfigureIAPCommand>();
            InvokeCommand<ApplyVIPStatusCommand>();
            
            /// Check editor additive scene
            bool loadScene = true;

            for (var i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount; i++)
            {
                /// For dev game
                if (
                    UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name == SceneManager.SCENE_GAME
                    || UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name == SceneManager.SCENE_MENU)
                {
                    loadScene = false;
                }
            }

            if (loadScene)
                InvokeCommand<LoadMenuScreenCommand>();
        }

        ////////////////////////////////////////////////////////////
        public void InvokeCommand<TCommand>()
        {
            Type type = typeof(TCommand);
            ApplicationCommand command = null;

            if (!commandsRegister.TryGetValue(type, out command))
            {
                Debug.LogFormat("Command with type ({0}) not found", type.Name);
                return;
            }

            command.Execute();
        }

        ////////////////////////////////////////////////////////////
        public void RegisterCommand<TCommand>(TCommand command) where TCommand : ApplicationCommand
        {
            Type type = typeof(TCommand);

            if (commandsRegister.ContainsKey(type))
            {
                Debug.LogFormat("Object with type ({0}) already registered", type.Name);
                return;
            }

            commandsRegister.Add(type, command);
        }

        ////////////////////////////////////////////////////////////
        public void RegisterObject(Type type, object obj)
        {
            objectsContainer.RegisterObject(type, obj);
        }

        ////////////////////////////////////////////////////////////
        public void RegisterObject<TTypeKey>(TTypeKey obj) where TTypeKey : class
        {
            objectsContainer.RegisterObject<TTypeKey>(obj);
        }

        ////////////////////////////////////////////////////////////
        public TTypeKey RevokeObject<TTypeKey>() where TTypeKey : class
        {
            return objectsContainer.RevokeObject<TTypeKey>();
        }
    }
}
