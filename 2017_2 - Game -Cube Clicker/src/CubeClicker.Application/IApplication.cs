﻿using CubeClicker.Application.Commands;
using CubeClicker.DI;

namespace CubeClicker.Application
{

    public interface IApplication : IContainer
    {
        void Initialize();

        void RegisterCommand<TCommand>(TCommand command) where TCommand : ApplicationCommand;
        void InvokeCommand<TCommand>();
    }
}