﻿using CubeClicker.Advertisement;
using CubeClicker.UI;
using CubeClicker.UI.Controllers;
using UnityEngine;

namespace CubeClicker.Application.Commands
{
    public class ConfigureGameUICommand : ApplicationCommand
    {
        ////////////////////////////////////////////////////////////
        public ConfigureGameUICommand(IApplication application)
            : base(application)
        {
        }

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            /// SHOW UNITS MENU
            UIManager.gameScreenStateMachine.RegisterState(
                UIStateMachine.States.GAME_SCREEN_VIEW,
                UIStateMachine.States.UNITS_MENU_VIEW,
                () => {
                    AdsManager.ShowBanner(AD_POSITION.TOP);

                    UnitsMenuController unitsMenuController = UIManager.FindController<UnitsMenuController>();
                    unitsMenuController.Show();
                    unitsMenuController.SetAnimatorState(UnitsMenuController.AnimatorStates.NORMAL);
                    
                    Time.timeScale = 0;
                });

            /// HIDE UNITS MENU
            UIManager.gameScreenStateMachine.RegisterState(
                UIStateMachine.States.UNITS_MENU_VIEW, 
                UIStateMachine.States.GAME_SCREEN_VIEW,
                () => {
                    AdsManager.ShowBanner(AD_POSITION.BOTTOM);

                    UnitsMenuController unitsMenuController = UIManager.FindController<UnitsMenuController>();
                    unitsMenuController.SetAnimatorState(UnitsMenuController.AnimatorStates.IDLE);
                    unitsMenuController.Hide();

                    Time.timeScale = 1;
                });

            /// SHOW UPGRADES MENU
            UIManager.gameScreenStateMachine.RegisterState(
                UIStateMachine.States.UNITS_MENU_VIEW, 
                UIStateMachine.States.UNIT_UPGRADE_VIEW,
                () => {
                    UpgradesMenuController upgradesMenuController = UIManager.FindController<UpgradesMenuController>();
                    upgradesMenuController.Show();

                    UnitsMenuController unitsMenuController = UIManager.FindController<UnitsMenuController>();
                    unitsMenuController.SetAnimatorState(UnitsMenuController.AnimatorStates.TRANSPARENT);
                });

            /// HIDE UPGRADES MENU
            UIManager.gameScreenStateMachine.RegisterState(
                UIStateMachine.States.UNIT_UPGRADE_VIEW,
                UIStateMachine.States.UNITS_MENU_VIEW,
                () => {
                    UpgradesMenuController upgradesMenuController = UIManager.FindController<UpgradesMenuController>();
                    upgradesMenuController.Hide();

                    UnitsMenuController unitsMenuController = UIManager.FindController<UnitsMenuController>();
                    unitsMenuController.SetAnimatorState(UnitsMenuController.AnimatorStates.NORMAL);
                });

            /// SHOW GO TO MENU ALERT
            UIManager.gameScreenStateMachine.RegisterState(
                UIStateMachine.States.GAME_SCREEN_VIEW,
                UIStateMachine.States.GOTO_MENU_ALERT,
                () => {
                    GoToMenuAlertController goToMenuAlertController = UIManager.FindController<GoToMenuAlertController>();
                    goToMenuAlertController.Show();
                });

            /// HIDE GO TO MENU ALERT
            UIManager.gameScreenStateMachine.RegisterState(
                UIStateMachine.States.GOTO_MENU_ALERT,
                UIStateMachine.States.GAME_SCREEN_VIEW,
                () => {
                    GoToMenuAlertController goToMenuAlertController = UIManager.FindController<GoToMenuAlertController>();
                    goToMenuAlertController.Hide();
                });
        }
    }
}
