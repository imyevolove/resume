﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.Application.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
