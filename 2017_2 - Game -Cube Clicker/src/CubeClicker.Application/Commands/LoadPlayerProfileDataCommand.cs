﻿using CubeClicker.Managers;
using CubeClicker.Personal;
using CubeClicker.Storage;
using UnityEngine;

namespace CubeClicker.Application.Commands
{
    public class LoadPlayerProfileDataCommand : ApplicationCommand
    {
        ////////////////////////////////////////////////////////////
        public LoadPlayerProfileDataCommand(IApplication application) : base(application){}

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            string data = Storage.Storage.LoadText(Profile.current.profileStorageFilePath);
            if (!string.IsNullOrEmpty(data))
                Profile.current.SetStorageData(StorageDataContainer.Deserialize(data));

            string playerData = Storage.Storage.LoadText(PlayerManager.mine.PlayerStorageFilePath);
            if (!string.IsNullOrEmpty(playerData))
                PlayerManager.mine.SetStorageData(StorageDataContainer.Deserialize(playerData));
        }
    }
}
