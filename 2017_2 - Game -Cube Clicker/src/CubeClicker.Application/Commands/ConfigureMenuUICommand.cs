﻿using CubeClicker.UI;
using CubeClicker.UI.Controllers;

namespace CubeClicker.Application.Commands
{
    public class ConfigureMenuUICommand : ApplicationCommand
    {
        ////////////////////////////////////////////////////////////
        public ConfigureMenuUICommand(IApplication application)
            : base(application)
        {
        }

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            /// SHOW DONATE UI
            UIManager.menuScreenStateMachine.RegisterState(
                UIMenuScreenStateMachine.STATE.MENU_VIEW,
                UIMenuScreenStateMachine.STATE.DONATE_VIEW,
                () => {
                    DonateController donateController = UIManager.FindController<DonateController>();
                    donateController.Show();
                });

            /// HIDE DONATE UI / SHOW MENU UI
            UIManager.menuScreenStateMachine.RegisterState(
                UIMenuScreenStateMachine.STATE.DONATE_VIEW,
                UIMenuScreenStateMachine.STATE.MENU_VIEW,
                () => {
                    DonateController donateController = UIManager.FindController<DonateController>();
                    donateController.Hide();
                });
        }
    }
}
