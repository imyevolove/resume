﻿namespace CubeClicker.Application.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class SaveStorageDataCommand : ApplicationCommand
    {
        public SaveStorageDataCommand(IApplication application) 
            : base(application)
        {
        }

        public override void Execute()
        {
            Debug.Log("Save game data");
        }
    }
}
