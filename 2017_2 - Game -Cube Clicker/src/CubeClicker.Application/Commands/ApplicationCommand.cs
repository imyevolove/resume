﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CubeClicker.Application.Commands
{
    public abstract class ApplicationCommand : ICommand
    {
        protected IApplication targetApplication;

        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////CONSTRUCTOR
        public ApplicationCommand(IApplication application)
        {
            targetApplication = application;
        }

        ////////////////////////////////////////////////////////////
        public abstract void Execute();
    }
}
