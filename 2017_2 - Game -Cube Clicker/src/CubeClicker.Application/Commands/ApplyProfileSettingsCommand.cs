﻿namespace CubeClicker.Application.Commands
{
    using CubeClicker.Personal;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class ApplyProfileSettingsCommand : ApplicationCommand
    {
        public ApplyProfileSettingsCommand(IApplication application) : base(application)
        {
        }

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            if (Profile.current == null)
            {
                Debug.LogWarning("Can't apply profile settings, current profile not defined");
                return;
            }

            Profile.current.settings.Apply();
        }
    }
}
