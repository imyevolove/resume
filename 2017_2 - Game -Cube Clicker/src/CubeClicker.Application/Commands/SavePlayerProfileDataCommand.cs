﻿using CubeClicker.Managers;
using CubeClicker.Personal;

namespace CubeClicker.Application.Commands
{
    public class SavePlayerProfileDataCommand : ApplicationCommand
    {
        ////////////////////////////////////////////////////////////
        public SavePlayerProfileDataCommand(IApplication application) : base(application){}

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            string xml = Profile.current.GetStorageData().ToXml();
            Storage.Storage.SaveText(Profile.current.profileStorageFilePath, xml);

            string playerData = PlayerManager.mine.GetStorageData().ToXml();
            Storage.Storage.SaveText(PlayerManager.mine.PlayerStorageFilePath, playerData);
        }
    }
}
