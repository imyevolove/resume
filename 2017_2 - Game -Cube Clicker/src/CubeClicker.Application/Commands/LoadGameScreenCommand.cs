﻿using CubeClicker.Managers;

namespace CubeClicker.Application.Commands
{
    public class LoadGameScreenCommand : ApplicationCommand
    {
        public LoadGameScreenCommand(IApplication application) 
            : base(application)
        {
        }

        public override void Execute()
        {
            SceneManager.LoadSceneAsync(SceneManager.SCENE_GAME, () => {});
        }
    }
}
