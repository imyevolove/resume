﻿namespace CubeClicker.Application.Commands
{
    using CubeClicker.Advertisement;
    using Personal;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UI;
    using UI.Controllers;
    using UnityEngine;

    public class ConfigureAdsCommand : ApplicationCommand
    {
        ////////////////////////////////////////////////////////////
        public ConfigureAdsCommand(IApplication application)
            : base(application)
        {
        }

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            AdsManager.Setup(new AdmobProvider("ca-app-pub-1844390866925362/5318649537", "ca-app-pub-1844390866925362/6795382733", "ca-app-pub-1844390866925362/8272115933"));
        }
    }
}
