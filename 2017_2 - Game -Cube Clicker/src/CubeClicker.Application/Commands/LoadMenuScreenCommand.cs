﻿using CubeClicker.Advertisement;
using CubeClicker.Managers;

namespace CubeClicker.Application.Commands
{
    public class LoadMenuScreenCommand : ApplicationCommand
    {
        public LoadMenuScreenCommand(IApplication application) 
            : base(application)
        {
        }

        public override void Execute()
        {
            AdsManager.HideBanner();
            SceneManager.LoadSceneAsync(SceneManager.SCENE_MENU, () => { });
        }
    }
}
