﻿using CubeClicker.Advertisement;
using CubeClicker.Personal;
using UnityEngine;

namespace CubeClicker.Application.Commands
{

    public class ApplyVIPStatusCommand : ApplicationCommand
    {
        ////////////////////////////////////////////////////////////
        public ApplyVIPStatusCommand(IApplication application)
            : base(application)
        {
        }

        ////////////////////////////////////////////////////////////
        public override void Execute()
        {
            if (Profile.current.vipStatus)
            {
                AdsManager.DisableAds();
                Debug.Log("APPLY VIP STATUS: ADS DISABLE");
            }
        }
    }
}
