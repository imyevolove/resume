﻿using CubeClicker.Data;
using CubeClicker.Managers;
using CubeClicker.Personal;
using System.Linq;
using UnityEngine;

namespace CubeClicker.Application.Commands
{
    public class ConfigureGameDataCommand : ApplicationCommand
    {
        public ConfigureGameDataCommand(IApplication application) 
            : base(application)
        {
        }

        public override void Execute()
        {
            LoadUnits();
            ConfigurePlayer();
        }

        ////////////////////////////////////////////////////////////
        /////////////////////////////////////////// CONFIGURE PLAYER
        ////////////////////////////////////////////////////////////
        protected void ConfigurePlayer()
        {
            var unitsManager = new Manager.UnitsManager();

            var unitsDatabase = targetApplication.RevokeObject<IUnitDatabase>();
            unitsManager.RegisterUnits(unitsDatabase.GetAll().Select(d => new Manager.UnitsManager.UnitRegistrationInfo(d.original, d.abilities.CreateAbilitySystem())));

            PlayerManager.RegisterMinePlayer(new Player("Player", unitsManager));
        }

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////// LOAD AND BIND UNITS
        ////////////////////////////////////////////////////////////
        protected void LoadUnits()
        {
            targetApplication.RegisterObject<IUnitDatabase>(Resources.Load<ScriptableUnitDatabase>("Databases/UnitsDatabase"));
        }
    }
}
