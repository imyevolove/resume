﻿using CubeClicker.IAPManagement;
using UnityEngine.Purchasing;

namespace CubeClicker.Application.Commands
{
    public class ConfigureIAPCommand : ApplicationCommand
    {
        public ConfigureIAPCommand(IApplication application) 
            : base(application)
        {
        }

        public override void Execute()
        {
            IAPManager.AddProduct(new IAPProduct("donate_lvl_0", ProductType.Consumable, "Small Donation",      "Ads will be removed permanently. Also we activate vip status for you (x2 money). We will be so happy! Every dollar help develop this game."));
            IAPManager.AddProduct(new IAPProduct("donate_lvl_1", ProductType.Consumable, "Good Donation",       "Ads will be removed permanently. Also we activate vip status for you (x2 money). We will be so happy! Every dollar help develop this game."));
            IAPManager.AddProduct(new IAPProduct("donate_lvl_2", ProductType.Consumable, "Large Donation",      "Ads will be removed permanently. Also we activate vip status for you (x2 money). We will be so happy! Every dollar help develop this game."));
            IAPManager.AddProduct(new IAPProduct("donate_lvl_3", ProductType.Consumable, "Generous Donation",   "Ads will be removed permanently. Also we activate vip status for you (x2 money). We will be so happy! Every dollar help develop this game."));
            IAPManager.AddProduct(new IAPProduct("donate_lvl_4", ProductType.Consumable, "Fantastic Donation",  "Ads will be removed permanently. Also we activate vip status for you (x2 money). We will be so happy! Every dollar help develop this game."));
            IAPManager.Initialize();
        }
    }
}
