﻿using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.Behaviours.Weapons.Ammunition;
using CubeClicker.Pools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace CubeClicker.Behaviours.Weapons
{ 
    [Serializable]
    public abstract class ShootableWeapon : Weapon, IShootable
    {
        [SerializeField] private BaseShell m_ShellPrefab;

        protected int ammoCurrent;

        protected WeaponAbility ReloadAbility { get; private set; }
        protected WeaponAbility AmmoAbility { get; private set; }
        protected WeaponAbility FireRateAbility { get; private set; }

        protected MonoBehaviourPool<BaseShell> ShellPool { get; private set; }

        ////////////////////////////////////////////////////////////
        public void Shoot()
        {
            Use();
        }

        ////////////////////////////////////////////////////////////
        public void Reload()
        {
            StartTimeout(ReloadAbility.Value, ResetAmmo);
        }

        ////////////////////////////////////////////////////////////
        protected override bool UseCore()
        {
            if (IsReady && ammoCurrent <= 0)
            {
                Reload();
                return false;
            }

            ammoCurrent--;

            Fire();
            DispatchFireEnd();

            return true;
        }

        ////////////////////////////////////////////////////////////
        protected abstract void Fire();

        ////////////////////////////////////////////////////////////
        public override void Initialize()
        {
            if (ShellPool == null && m_ShellPrefab != null)
            {
                ShellPool = PoolGroupManager<BaseShell>.Instance.GetPool(m_ShellPrefab, () => new MonoBehaviourPool<BaseShell>(m_ShellPrefab)) as MonoBehaviourPool<BaseShell>;
            }

            base.Initialize();

            ReloadAbility = AbilitiesSystem.GetAbility(WeaponAbilityCode.Reload);
            AmmoAbility = AbilitiesSystem.GetAbility(WeaponAbilityCode.Ammo);
            FireRateAbility = AbilitiesSystem.GetAbility(WeaponAbilityCode.FireRate);

            ResetAmmo();
        }

        ////////////////////////////////////////////////////////////
        protected void ResetAmmo()
        {
            ammoCurrent = (AmmoAbility == null ? 1 : Mathf.CeilToInt(AmmoAbility.Value));
        }

        ////////////////////////////////////////////////////////////
        private void DispatchFireEnd()
        {
            if (ammoCurrent <= 0)
            {
                Reload();
            }
            else
            {
                var frt = (FireRateAbility == null ? 0 : 60 / FireRateAbility.Value);
                StartTimeout(frt);
            }
        }
    }
}
