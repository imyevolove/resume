﻿namespace CubeClicker.Behaviours.Weapons
{
    public interface IShootable
    {
        void Reload();
        void Shoot();
    }
}
