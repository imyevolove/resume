﻿using CubeClicker.DamageSystem;
using CubeClicker.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace CubeClicker.Behaviours.Weapons
{
    public interface IWeapon : IDamageProvider, IStorable
    {
        UnityEvent onReady { get; }
        bool IsReady { get; }
        void Use();
    }
}
