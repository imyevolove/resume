﻿using CubeClicker.Behaviours.Weapons.Ammunition;
using CubeClicker.Behaviours.Weapons.Effects;
using CubeClicker.DamageSystem;
using CubeClicker.Pools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Experimental.XR;

namespace CubeClicker.Behaviours.Weapons
{
    [Serializable]
    public class ShootableLaserWeapons : ShootableWeapon
    {
        [SerializeField, Range(0, 1)] protected float recoil;
        [SerializeField] protected float damageTime;
        [SerializeField] protected float raycastLength;
        [SerializeField] protected LaserEffect laserEffect;

        ////////////////////////////////////////////////////////////
        protected override void Fire()
        {
            StopCoroutine("FireEnumerator");
            StartCoroutine("FireEnumerator");
        }

        ////////////////////////////////////////////////////////////
        protected IEnumerator FireEnumerator()
        {
            var rayLength = raycastLength;
            IDamageReceiver damageReceiver = null;

            /// This logic is optimized
            RaycastHit hit;
            Ray ray = new Ray(transform.position, transform.forward);
            if (Physics.Raycast(ray, out hit, raycastLength))
            {
                damageReceiver = hit.collider.GetComponent<IDamageReceiver>();

                if (damageReceiver != null)
                    rayLength = hit.distance;
            }

            /// Start
            laserEffect.SetActive(true);
            laserEffect.Setup(damageTime);
            laserEffect.SetLineLength(rayLength);
            laserEffect.Play();

            /// Process
            var damageValue = GetDamageValue();

            var waitIterations = 2;
            var waitIteratorIndex = 0;

            var damageIteratorIndex = (int)Mathf.Floor(waitIterations / 2);
            var damageComplete = false;

            var timeWait = damageTime / waitIterations;

            while (waitIteratorIndex < waitIterations)
            {
                yield return new WaitForSeconds(timeWait);

                if (!damageComplete && damageIteratorIndex >= waitIteratorIndex)
                {
                    damageComplete = true;
                    DamageTo(damageReceiver, hit, damageValue);
                }

                waitIteratorIndex++;
            }

            /// End
            laserEffect.Stop();
            laserEffect.SetActive(false);
        }

        ////////////////////////////////////////////////////////////
        protected void DamageTo(IDamageReceiver receiver, RaycastHit hit, float damage)
        {
            if (receiver != null && hit.collider != null)
            {
                receiver.ReceiveDamage(new DamageEventData()
                {
                    provider = this,
                    receiver = receiver,
                    normal = hit.normal,
                    point = hit.point,
                    damage = damage
                });
            }
        }
    }
}
