﻿using CubeClicker.Abilities;
using CubeClicker.Behaviours.Weapons.Abilities;
using CubeClicker.DamageSystem;
using CubeClicker.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace CubeClicker.Behaviours.Weapons
{
    [Serializable]
    public abstract class Weapon : MonoBehaviour, IWeapon
    {
        [NonSerialized] private IWeaponAbilitySystem m_AbilitiesSystem;
        public IWeaponAbilitySystem AbilitiesSystem { get => m_AbilitiesSystem; set => m_AbilitiesSystem = value; }

        [SerializeField] private UnityEvent m_OnUse = new UnityEvent();
        public UnityEvent onUse => m_OnUse;

        [SerializeField] private UnityEvent m_OnReady = new UnityEvent();
        public UnityEvent onReady => m_OnReady;

        public bool IsReady { get; private set; } = true;

        protected WeaponAbility DamageAbility { get; private set; }

        private Coroutine m_UseTimeoutCoroutine;

        ////////////////////////////////////////////////////////////
        public void Use()
        {
            if (!IsReady || !UseCore()) return;
            DispatchUseEvent();
        }

        ////////////////////////////////////////////////////////////
        public void Damage(DamageEventData eventData)
        {
            eventData.receiver.ReceiveDamage(eventData);
        }

        ////////////////////////////////////////////////////////////
        public float GetDamageValue()
        {
            return DamageAbility.Value;
        }

        ////////////////////////////////////////////////////////////
        protected abstract bool UseCore();
        

        ////////////////////////////////////////////////////////////
        public virtual void Initialize()
        {
            DamageAbility = AbilitiesSystem.GetAbility(WeaponAbilityCode.Damage);
        }

        ////////////////////////////////////////////////////////////
        protected void StartTimeout(float time, Action callback = null)
        {
            if (m_UseTimeoutCoroutine != null)
            {
                StopCoroutine(m_UseTimeoutCoroutine);
                m_UseTimeoutCoroutine = null;
            }

            m_UseTimeoutCoroutine = StartCoroutine(UseTimeout(time, callback));
        }

        ////////////////////////////////////////////////////////////
        protected virtual void OnReady()
        {
        }
        
        ////////////////////////////////////////////////////////////
        protected virtual void OnUse()
        {
        }

        ////////////////////////////////////////////////////////////
        private void DispatchReadyEvent()
        {
            OnReady();
            onReady.Invoke();
        }
        
        ////////////////////////////////////////////////////////////
        private void DispatchUseEvent()
        {
            OnUse();
            onUse.Invoke();
        }

        ////////////////////////////////////////////////////////////
        private IEnumerator UseTimeout(float time, Action callback = null)
        {
            IsReady = false;
            yield return new WaitForSeconds(time);
            IsReady = true;

            if (callback != null)
                callback.Invoke();

            DispatchReadyEvent();
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();
            data["abilities_system"] = AbilitiesSystem.GetStorageData();

            return data;
        }

        ////////////////////////////////////////////////////////////
        public void SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<StorageDataContainer>("abilities_system"))
            {
                AbilitiesSystem.SetStorageData((StorageDataContainer)storageData["abilities_system"]);
            }
        }
    }
}
