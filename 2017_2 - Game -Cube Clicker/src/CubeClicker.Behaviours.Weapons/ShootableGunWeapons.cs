﻿using CubeClicker.Behaviours.Weapons.Ammunition;
using CubeClicker.Pools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Behaviours.Weapons
{
    [Serializable]
    public class ShootableGunWeapons : ShootableWeapon
    {
        [SerializeField, Range(0, 1)] protected float recoil;

        ////////////////////////////////////////////////////////////
        protected override void Fire()
        {
            if (ShellPool == null)
                throw new NullReferenceException("Shell pool is not defined");
            
            Bullet bullet = ShellPool.Get() as Bullet;
            bullet.transform.position = transform.position;

            Vector3 direction = transform.forward;

            float recoilX = UnityEngine.Random.Range(-recoil, recoil);
            float recoilY = UnityEngine.Random.Range(-recoil, recoil);

            /// Look at cube
            bullet.transform.rotation = Quaternion.LookRotation(direction);

            direction = bullet.transform.forward;
            direction = new Vector3(
                direction.x + recoilX,
                direction.y + recoilY,
                direction.z
                );
            direction.Normalize();

            /// Look at cube with recoil
            bullet.transform.rotation = Quaternion.LookRotation(direction);

            bullet.LaunchBullet(bullet.transform.forward, this, GetDamageValue());
        }
    }
}
