﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Database
{
    public abstract class DatabaseEntity
    {
        public string Id { get; private set; }

        public DatabaseEntity()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
