﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CubeClicker.Database
{
    [Serializable]
    public class Database<TEntity> : IDatabase<TEntity>
        where TEntity : DatabaseEntity
    {
        [SerializeField] private List<TEntity> m_Entities = new List<TEntity>();

        public TEntity this[int index] => m_Entities[index];
        
        public bool Contains(TEntity entity)
        {
            return m_Entities.Contains(entity);
        }

        public bool Add(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (Contains(entity))
                return false;

            m_Entities.Add(entity);

            return true;
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            foreach (var entity in entities)
                Add(entity);
        }

        public TEntity[] GetAll()
        {
            return m_Entities.ToArray();
        }

        public int IndexOf(TEntity entity)
        {
            return m_Entities.IndexOf(entity);
        }

        public bool Remove(TEntity entity)
        {
            return m_Entities.Remove(entity);
        }

        public void RemoveAt(int index)
        {
            m_Entities.RemoveAt(index);
        }
    }
}
