﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Database
{
    public interface IDatabase<TEntity>
        where TEntity : DatabaseEntity
    {
        TEntity this[int index] { get; }

        TEntity[] GetAll();

        bool Contains(TEntity entity);

        bool Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        bool Remove(TEntity entity);
        void RemoveAt(int index);

        int IndexOf(TEntity entity);
    }
}
