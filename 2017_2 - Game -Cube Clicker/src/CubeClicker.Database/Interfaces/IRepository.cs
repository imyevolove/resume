﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeClicker.Database
{
    public interface IRepository<TEntity>
    {
        TEntity[] GetAll();
    }
}
