﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Silentum.Interactions
{
    [Serializable]
    [DisallowMultipleComponent]
    public abstract class InteractableBehaviour : MonoBehaviour, IInteractable
    {
        public IRequirementProvider RequirementProvider { get; private set; }
        public IReactionProvider ReactionProvider { get; private set; }

        public IInteractable Interactable => this;

        public IEnumerable<InteractionProcess> ActiveInteractions => _processes;
        public int ActiveInteractionsCount => _processes.Count;

        private List<InteractionProcess> _processes = new List<InteractionProcess>();

        protected virtual void Awake()
        {
            RequirementProvider = new RequirementComponentProvider(gameObject);
            ReactionProvider = new ReactionComponentProvider(gameObject);
        }

        public void CompleteAllActiveInteractions() => InteractableExtensions.CompleteAllActiveInteractions(this);
        public void RejectAllActiveInteractions() => InteractableExtensions.RejectAllActiveInteractions(this);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        public void Interact(InteractionProcess process)
        {
            ValidateProcess(process);

            _processes.Add(process);

            var rpvd = ReactionProvider;
            process.Completed += (pcs) =>
            {
                _processes.Remove(pcs);

                rpvd?.ReactAll(pcs.Result);

                OnComplete(pcs.Result);
            };

            var requirementResult = RequirementProvider.ValidateAll(process.Context);
            if (!requirementResult.Status.IsPassed() || !VerifyInteraction(process.Context))
            {
                process.Complete(InteractionStatus.Rejected);
                return;
            }

            rpvd.ReactAll(new InteractionResult(process.Context, InteractionStatus.Started));
            HandleInteractionProcess(process);
        }

        public void Destroy()
        {
            Destroy(RequirementProvider);
            Destroy(ReactionProvider);
            Destroy(this);
        }

        protected abstract bool VerifyInteraction(InteractionContext context);
        protected abstract void HandleInteractionProcess(InteractionProcess process);

        protected virtual void OnComplete(InteractionResult result)
        {
        }

        private InteractionResult CreateResult(InteractionContext context, InteractionStatus status) => new InteractionResult(context, status);

        private void Destroy(IEnumerable<IDestroyable> collection)
        {
            if (collection == null) return;
            foreach (var item in collection)
            {
                item?.Destroy();
            }
        }

        private void ValidateProcess(InteractionProcess process)
        {
            if (process == null) throw new ArgumentNullException("Process is null");
            if (process.IsCompleted) throw new InteractionProcessException("Process is completed");
            if (_processes.Contains(process)) throw new Exception("Process already initiated");
        }
    }
}
