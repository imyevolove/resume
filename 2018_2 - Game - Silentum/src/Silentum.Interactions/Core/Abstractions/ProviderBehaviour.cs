﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public abstract class ProviderBehaviour<T> : MonoBehaviour, IProvider<T>
    {
        public T[] Items { get; private set; } = new T[0];
        public int Count => Items.Length;

        protected virtual void Awake()
        {
            Fetch();
        }

        public void Fetch() => Items = GetComponents<T>();

        public IEnumerator<T> GetEnumerator() => Items.AsEnumerable().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }
}
