﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    [Serializable]
    public abstract class ReactionBehaviour : PureReactionBehaviour
    {
        public InteractionStatus ReactionType { get => _reactionType; set => _reactionType = value; }
       
        [SerializeField] private InteractionStatus _reactionType;

        public sealed override void React(InteractionResult result)
        {
            if (!result.Status.Has(_reactionType)) return;
            ReactionHandler(result);
        }

        protected abstract void ReactionHandler(InteractionResult result);
    }
}
