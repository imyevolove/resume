﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    [Serializable]
    public abstract class InteractionHandlerBehaviour<T> : InteractionHandlerBehaviour
        where T : IInteractable
    {
        public override bool CanInteract(IInteractable interactable)
        {
            if (interactable == null) return false;
            return interactable.GetType().IsAssignableFrom(typeof(T));
        }

        protected abstract bool VerifyRequest(T interactable);
        protected override bool VerifyRequest(IInteractable interactable) => VerifyRequest((T)interactable);
    }

    [Serializable]
    [RequireComponent(typeof(InteractionBrain))]
    public abstract class InteractionHandlerBehaviour : MonoBehaviour, IInteractionHandler
    {
        public abstract bool CanInteract(IInteractable interactable);

        protected abstract bool VerifyRequest(IInteractable interactable);

        public InteractionProcess Interact(IInteractable interactable)
        {
            var process = new InteractionProcess(CreateContext(interactable));
            process.Completed += (p) => { OnInteractionCompleted(p); };

            /// Отклонение дальнейших операций взаимодействий по верификации.
            if (!CanInteract(interactable) || !VerifyRequest(interactable))
            {
                process.Complete(InteractionStatus.Rejected);
                return process;
            }

            interactable.Interact(process);

            if (!process.IsCompleted)
            {
                OnInteractionPassed(process);
            }

            return process;
        }

        protected virtual void OnInteractionPassed(InteractionProcess process) { }
        protected virtual void OnInteractionCompleted(InteractionProcess process) { }

        protected virtual InteractionContext CreateContext(IInteractable interactable) => new InteractionContext(interactable, this, gameObject, null);
    }
}
