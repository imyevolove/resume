﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    [Serializable]
    [RequireComponent(typeof(InteractableBehaviour))]
    public abstract class RequirementBehaviour : MonoBehaviour, IRequirement
    {
        public RequirementResult Validate(InteractionContext context)
        {
            return InternalValidate(context) 
                ? RequirementResult.CreatePassed()
                : RequirementResult.CreateRejected(this);
        }

        protected abstract bool InternalValidate(InteractionContext context);

        public void Destroy() => Destroy(this);
    }
}
