﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    [Serializable]
    [RequireComponent(typeof(InteractableBehaviour))]
    public abstract class PureReactionBehaviour : MonoBehaviour, IReaction
    {
        public IInteractable Interactable
        {
            get
            {
                if (_interactable == null)
                {
                    _interactable = GetComponent<IInteractable>();
                }

                return _interactable;
            }
        }

        [NonSerialized] private IInteractable _interactable;

        public abstract void React(InteractionResult result);
        public void Destroy() => Destroy(this);
    }
}
