﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    [Serializable]
    [DisallowMultipleComponent]
    public abstract class RaycasterBehaviour : MonoBehaviour, IRaycaster
    {
        [SerializeField] private LayerMask m_Layer;
        public LayerMask Layer { get => m_Layer; set => m_Layer = value; }

        [SerializeField] private float m_Distance;
        public float Distance { get => m_Distance; set => m_Distance = value; }

        public IInteractable Raycast(Ray ray) => Raycast(ray, Distance, Layer);
        public abstract IInteractable Raycast(Ray ray, float distance, LayerMask layer);
    }
}
