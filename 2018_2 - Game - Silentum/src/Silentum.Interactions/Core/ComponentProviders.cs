﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public class ComponentProvider<T> : IProvider<T>
    {
        public T[] Items { get; private set; } = new T[0];
        public int Count => Items.Length;

        public readonly GameObject Storage;

        public ComponentProvider(GameObject storage)
        {
            Storage = storage ?? throw new ArgumentNullException(nameof(storage));
            Fetch();
        }

        public void Fetch() => Items = Storage.GetComponents<T>();

        public IEnumerator<T> GetEnumerator() => Items.AsEnumerable().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }

    public sealed class InteractionHandlerComponentProvider : ComponentProvider<IInteractionHandler>, IInteractionHandlerProvider
    {
        public InteractionHandlerComponentProvider(GameObject storage) : base(storage)
        {
        }
    }

    public sealed class RequirementComponentProvider : ComponentProvider<IRequirement>, IRequirementProvider
    {
        public RequirementComponentProvider(GameObject storage) : base(storage)
        {
        }
    }

    public sealed class ReactionComponentProvider : ComponentProvider<IReaction>, IReactionProvider
    {
        public ReactionComponentProvider(GameObject storage) : base(storage)
        {
        }
    }
}
