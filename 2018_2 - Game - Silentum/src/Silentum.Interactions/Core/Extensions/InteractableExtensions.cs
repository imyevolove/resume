﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public static class InteractableExtensions
    {
        public static void CompleteAllActiveInteractions(this IInteractable interactable) => CompleteAllActiveInteractions(interactable, false);
        public static void RejectAllActiveInteractions(this IInteractable interactable) => CompleteAllActiveInteractions(interactable, true);

        public static void CompleteAllActiveInteractions(this IInteractable interactable, bool rejected)
        {
            if (interactable.ActiveInteractionsCount == 0) return;
            foreach (var process in interactable.ActiveInteractions.ToArray())
            {
                if (rejected)
                {
                    process.Reject();
                }
                else
                {
                    process.Complete();
                }
            }
        }
    }
}
