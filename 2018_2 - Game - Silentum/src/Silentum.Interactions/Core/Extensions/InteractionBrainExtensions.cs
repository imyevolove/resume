﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public static class InteractionBrainExtensions
    {
        /// <summary>
        /// Raycast from camera with Input.mousePosition
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        public static bool RaycastAndInteract(this InteractionBrain brain, Camera camera) => brain.RaycastAndInteract(camera, UnityEngine.Input.mousePosition);
        /// <summary>
        /// Raycast from main camera
        /// </summary>
        /// <param name="pointerPosition"></param>
        /// <returns></returns>
        public static bool RaycastAndInteract(this InteractionBrain brain, Vector2 pointerPosition) => brain.RaycastAndInteract(Camera.main, pointerPosition);
        /// <summary>
        /// Raycast from main camera with Input.mousePosition
        /// </summary>
        /// <returns></returns>
        public static bool RaycastAndInteract(this InteractionBrain brain) => brain.RaycastAndInteract(Camera.main);
    }
}
