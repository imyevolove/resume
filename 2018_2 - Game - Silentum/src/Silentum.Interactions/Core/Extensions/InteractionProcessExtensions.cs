﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public static class InteractionProcessExtensions
    {
        public static void Reject(this InteractionProcess process) => process.Complete(new InteractionResult(process.Context, InteractionStatus.Rejected));
        public static void Complete(this InteractionProcess process) => process.Complete(new InteractionResult(process.Context, InteractionStatus.Passed));
    }
}
