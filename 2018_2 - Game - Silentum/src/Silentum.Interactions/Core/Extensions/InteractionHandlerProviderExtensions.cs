﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public static class InteractionHandlerProviderExtensions
    {
        public static IInteractionHandler Find(this IInteractionHandlerProvider provider, IInteractable interactable)
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            return provider.FirstOrDefault(handler => handler.CanInteract(interactable));
        }
    }
}
