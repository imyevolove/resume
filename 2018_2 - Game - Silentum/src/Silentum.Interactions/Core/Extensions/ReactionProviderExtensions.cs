﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public static class ReactionProviderExtensions
    {
        public static void ReactAll(this IReactionProvider provider, InteractionResult result)
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (provider.Count == 0) return;

            for (var i = 0; i < provider.Count; i++)
            {
                provider.Items[i]?.React(result);
            }
        }
    }
}
