﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public static class RequirementProviderExtensions
    {
        public static RequirementResult ValidateAll(this IRequirementProvider provider, InteractionContext context)
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (provider.Count == 0) return RequirementResult.CreatePassed();

            IRequirement requirement;
            for (var i = 0; i < provider.Count; i++)
            {
                requirement = provider.Items[i];

                if (requirement == null) continue;

                var result = requirement.Validate(context);
                if (result.Status == InteractionStatus.Rejected) return result;
            }

            return RequirementResult.CreatePassed();
        }
    }
}
