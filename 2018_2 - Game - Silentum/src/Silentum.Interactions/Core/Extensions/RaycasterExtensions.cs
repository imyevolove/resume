﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public static class RaycasterExtensions
    {
        /// <summary>
        /// Raycast with camera and pointer.
        /// Will return interactable object or null.
        /// </summary>
        /// <param name="raycaster"></param>
        /// <param name="camera"></param>
        /// <param name="pointerPosition"></param>
        /// <returns></returns>
        public static IInteractable Raycast(this IRaycaster raycaster, Camera camera, Vector2 pointerPosition)
        {
            return raycaster.Raycast(camera.ScreenPointToRay(pointerPosition));
        }
    }
}
