﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public static class InteractionStatusExtensions
    {
        public static bool Has(this InteractionStatus source, InteractionStatus status) => (source & status) != 0;
        public static bool IsRejected(this InteractionStatus status) => Has(status, InteractionStatus.Rejected);
        public static bool IsPassed(this InteractionStatus status) => Has(status, InteractionStatus.Passed);
        public static bool IsCompleted(this InteractionStatus status) => Has(InteractionStatus.Completed, status);
        public static bool IsStarted(this InteractionStatus status) => Has(status, InteractionStatus.Started);
    }
}
