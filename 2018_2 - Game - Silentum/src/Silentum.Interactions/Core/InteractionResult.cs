﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public class InteractionResult
    {
        public readonly InteractionContext Context;
        public readonly InteractionStatus Status;

        public InteractionResult(InteractionContext context, InteractionStatus status)
        {
            Context = context;
            Status = status;
        }
    }
}
