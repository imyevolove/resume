﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public interface IRaycaster
    {
        LayerMask Layer { get; set; }
        float Distance { get; set; }

        IInteractable Raycast(Ray ray);
        IInteractable Raycast(Ray ray, float distance, LayerMask layer);
    }
}
