﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    /// <summary>
    /// IInteractionHandler - используется для инициации взаимодействия
    /// </summary>
    public interface IInteractionHandler
    {
        bool CanInteract(IInteractable interactable);
        InteractionProcess Interact(IInteractable interactable);
    }
}
