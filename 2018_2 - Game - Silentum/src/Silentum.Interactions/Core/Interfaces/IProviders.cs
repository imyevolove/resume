﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public interface IProvider<T> : IEnumerable<T>, IEnumerable
    {
        T[] Items { get; }
        int Count { get; }
    }

    public interface IRequirementProvider : IProvider<IRequirement>
    {
    }

    public interface IReactionProvider : IProvider<IReaction>
    {
    }

    public interface IInteractionHandlerProvider : IProvider<IInteractionHandler>
    {
    }
}
