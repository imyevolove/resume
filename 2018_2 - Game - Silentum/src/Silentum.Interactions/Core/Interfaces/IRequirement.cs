﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    /// <summary>
    /// IRequirement - используется для управления входящими запросами взаимодействия.
    /// </summary>
    public interface IRequirement : IDestroyable
    {
        RequirementResult Validate(InteractionContext context);
    }
}
