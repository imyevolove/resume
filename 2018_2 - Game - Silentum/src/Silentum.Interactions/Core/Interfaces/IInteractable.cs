﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;

namespace Silentum.Interactions
{
    public interface IInteractable : IRoute, IDestroyable
    {
        IEnumerable<InteractionProcess> ActiveInteractions { get; }
        int ActiveInteractionsCount { get; }

        void Interact(InteractionProcess process);
    }
}
