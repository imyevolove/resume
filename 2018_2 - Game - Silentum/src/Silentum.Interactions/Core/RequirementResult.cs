﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public struct RequirementResult
    {
        public readonly IRequirement RejectionRequirement;
        public readonly InteractionStatus Status;

        public RequirementResult(IRequirement rejectionRequirement, InteractionStatus status)
        {
            RejectionRequirement = rejectionRequirement;
            Status = status;
        }

        public static RequirementResult CreateRejected(IRequirement rejectionRequirement) => new RequirementResult(rejectionRequirement, InteractionStatus.Rejected);
        public static RequirementResult CreatePassed() => new RequirementResult(null, InteractionStatus.Passed);
    }
}
