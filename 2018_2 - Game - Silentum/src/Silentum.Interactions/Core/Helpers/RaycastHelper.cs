﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public static class RaycastHelper
    {
        public static T Raycast<T>(Ray ray, float distance, LayerMask layer)
        {
            RaycastHit raycastHit;
            Physics.Raycast(ray, out raycastHit, distance, layer);

            if (raycastHit.collider == null)
                return default;

            return raycastHit.collider.GetComponent<T>();
        }
    }
}
