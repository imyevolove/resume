﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public enum InteractionStatus
    {
        Started = 1 << 0,
        Passed = 1 << 1,
        Rejected = 1 << 2,
        Completed = Passed | Rejected,
    }
}
