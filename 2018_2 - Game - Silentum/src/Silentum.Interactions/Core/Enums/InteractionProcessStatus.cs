﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public enum InteractionProcessStatus
    {
        Performing,
        Completed
    }
}
