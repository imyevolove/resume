﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    public class InteractionContext
    {
        public readonly IInteractable Interactable;
        public readonly IInteractionHandler Handler;
        public readonly GameObject Interactor;
        public readonly object Data;

        public InteractionContext(IInteractable interactable, IInteractionHandler handler) : this(interactable, handler, null, null)
        {
        }
        public InteractionContext(IInteractable interactable, IInteractionHandler handler, object data) : this(interactable, handler, null, data)
        {
        }
        public InteractionContext(IInteractable interactable, IInteractionHandler handler, GameObject interactor) : this(interactable, handler, interactor, null)
        {
        }
        public InteractionContext(IInteractable interactable, IInteractionHandler handler, GameObject interactor, object data)
        {
            Interactable = interactable;
            Handler = handler;
            Interactor = interactor;
            Data = data;
        }
    }
}
