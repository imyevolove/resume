﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Interactions
{
    public sealed class InteractionProcess
    {
        public InteractionProcessCompletedEventDelegate Completed;

        public bool IsCompleted => ProcessStatus == InteractionProcessStatus.Completed;
        public InteractionProcessStatus ProcessStatus { get; private set; } = InteractionProcessStatus.Performing;
        public InteractionStatus InteractionStatus => IsCompleted ? Result?.Status ?? InteractionStatus.Completed : InteractionStatus.Started;
        public InteractionResult Result { get; private set; }
        public readonly InteractionContext Context;

        public InteractionProcess(InteractionContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Complete(InteractionResult result)
        {
            if (IsCompleted) return;

            ProcessStatus = InteractionProcessStatus.Completed;
            Result = result;

            Completed?.Invoke(this);
        }

        public void Complete(InteractionStatus status) => Complete(new InteractionResult(Context, status));
    }
}
