﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    /// <summary>
    /// InteractionBrain contains interaction handlers and provides methods for interacting with interactive objects of the world.
    /// </summary>
    [Serializable]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(IRaycaster))]
    public sealed class InteractionBrain : MonoBehaviour
    {
        public InteractionProcess ActiveInteractionProcess { get; private set; }

        /// <summary>
        /// Raycaster
        /// </summary>
        public IRaycaster Raycaster
        {
            get
            {
                if (m_Raycaster == null)
                    m_Raycaster = GetComponent<IRaycaster>();

                return m_Raycaster;
            }
        }

        private IRaycaster m_Raycaster;
        private IInteractionHandlerProvider _handlerProvider;

        private void Awake()
        {
            _handlerProvider = new InteractionHandlerComponentProvider(gameObject);
        }

        /// <summary>
        /// Interaction with the object using the available handlers.
        /// </summary>
        /// <param name="interactable"></param>
        /// <returns></returns>
        public bool Interact(IInteractable interactable)
        {
            RejectActiveInteractionProcess();
            
            var handler = FindHandler(interactable);

            if (handler == null)
            {
                Debug.LogWarning($"Interaction handler for interactable type '{interactable.GetType().Name}' not found");
                return false;
            }

            SetActiveInteractionProcess(handler.Interact(interactable));
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="pointerPosition"></param>
        /// <returns></returns>
        public bool RaycastAndInteract(Camera camera, Vector2 pointerPosition)
        {
            var interactable = Raycaster.Raycast(camera, pointerPosition);
            return interactable != null ? Interact(interactable) : false;
        }

        private IInteractionHandler FindHandler(IInteractable interactable) => _handlerProvider?.Find(interactable);
        private void SetActiveInteractionProcess(InteractionProcess process)
        {
            RejectActiveInteractionProcess();

            if (process == null || process.IsCompleted) return;

            ActiveInteractionProcess = process;
            ActiveInteractionProcess.Completed += (pcs) => 
            {
                if (ActiveInteractionProcess != pcs) return;
                RejectActiveInteractionProcess();
            };
        }
        private void RejectActiveInteractionProcess()
        {
            if (ActiveInteractionProcess == null) return;

            if (!ActiveInteractionProcess.IsCompleted)
            {
                ActiveInteractionProcess.Complete(InteractionStatus.Rejected);
            }

            ActiveInteractionProcess = null;
        }
    }
}
