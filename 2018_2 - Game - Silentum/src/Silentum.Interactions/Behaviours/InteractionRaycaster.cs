﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    /// <summary>
    /// This class not inheritable. 
    /// To define your own interaction raycaster inherits from <see cref="RaycasterBehaviour"/> or <see cref="IRaycaster"/>
    /// </summary>
    [Serializable]
    public sealed class InteractionRaycaster : RaycasterBehaviour
    {
        public override IInteractable Raycast(Ray ray, float distance, LayerMask layer)
        {
            return RaycastHelper.Raycast<IRoute>(ray, distance, layer)?.Interactable ?? null;
        }
    }
}
