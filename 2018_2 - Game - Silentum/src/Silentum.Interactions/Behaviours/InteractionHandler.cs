﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    /// <summary>
    /// This class not inheritable. 
    /// To define your own interaction handler inherits from <see cref="InteractionHandlerBehaviour"/> or <see cref="IInteractionHandler"/>
    /// </summary>
    [Serializable]
    public sealed class InteractionHandler : InteractionHandlerBehaviour<Interactable>
    {
        [SerializeField] public GameObject interactorOverride;

        protected override bool VerifyRequest(Interactable interactable) => true;

        protected override InteractionContext CreateContext(IInteractable interactable) => new InteractionContext(interactable, this, interactorOverride ?? gameObject, null);
    }
}
