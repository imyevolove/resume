﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    /// <summary>
    /// InteractionDestroyer destroys interactable at the end of the counter.
    /// </summary>
    [Serializable]
    public sealed class InteractionDestroyer : ReactionBehaviour
    {
        [SerializeField] public InteractionDestroyMode destroyMode;
        [SerializeField] public uint actionsToDestroy = 1;

        private uint m_Counter;

        public void ResetCounter()
        {
            m_Counter = 0;
        }

        private void CheckCompletition()
        {
            if (m_Counter < actionsToDestroy) return;

            DestroyInteraction();
            Destroy(this);
        }

        private void DestroyInteraction()
        {
            switch (destroyMode)
            {
                case InteractionDestroyMode.OnlyComponents:
                    Interactable.Destroy();
                    return;
                case InteractionDestroyMode.Object:
                    Destroy(gameObject);
                    return;

            }
        }

        protected override void ReactionHandler(InteractionResult result)
        {
            m_Counter++;
            CheckCompletition();
        }
    }
}
