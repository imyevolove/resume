﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    /// <summary>
    /// Provides basic interaction.
    /// This class not inheritable, 
    /// to define your own interactable object inherits from <see cref="InteractableBehaviour"/> or <see cref="IInteractable"/>
    /// </summary>
    [Serializable]
    public sealed class Interactable : InteractableBehaviour
    {
        protected override bool VerifyInteraction(InteractionContext context) => true;
        protected override void HandleInteractionProcess(InteractionProcess process) => process.Complete(new InteractionResult(process.Context, InteractionStatus.Passed));
    }
}
