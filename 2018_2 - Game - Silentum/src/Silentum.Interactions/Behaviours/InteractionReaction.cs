﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Silentum.Interactions
{
    [Serializable]
    public sealed class InteractionReaction : ReactionBehaviour
    {
        public InteractionEvent Interacted => _interacted;

        [SerializeField] private InteractionEvent _interacted = new InteractionEvent();

        protected override void ReactionHandler(InteractionResult result) => Interacted.Invoke(result);
    }
}
