﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Interactions
{
    /// <summary>
    /// The component allows to detect interactive objects.
    /// </summary>
    [Serializable]
    [RequireComponent(typeof(Collider))]
    public sealed class InteractionRedirector : MonoBehaviour, IRoute
    {
        /// <summary>
        /// Interactable target
        /// </summary>
        [SerializeField] public InteractableBehaviour interactable;
        public IInteractable Interactable => interactable;
    }
}
