﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Silentum.Interactions.Editor
{
    [CustomEditor(typeof(InteractableBehaviour), true)]
    public class InteractableBehaviourEditor : UnityEditor.Editor
    {
        public InteractableBehaviour Interactable => _interactable ?? (_interactable = target as InteractableBehaviour);
        private InteractableBehaviour _interactable;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DrawProcessesGUI();
        }

        private void DrawProcessesGUI()
        {
            if (Interactable.ActiveInteractionsCount == 0)
            {
                GUI.enabled = false;
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("No active interactions");
                EditorGUILayout.EndVertical();
                GUI.enabled = true;
                return;
            }

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            foreach (var interaction in Interactable.ActiveInteractions)
            {
                DrawInteractionGUI(interaction);
            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Complete all", EditorStyles.miniButtonLeft))
            {
                Interactable.CompleteAllActiveInteractions();
                GUIUtility.ExitGUI();
            }
            if (GUILayout.Button("Reject all", EditorStyles.miniButtonRight))
            {
                Interactable.RejectAllActiveInteractions();
                GUIUtility.ExitGUI();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawInteractionGUI(InteractionProcess process)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField(process.Context.Handler.ToString());
            EditorGUILayout.LabelField($"Process status: {process.ProcessStatus}, Interaction status: {process.InteractionStatus}");

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Complete", EditorStyles.miniButtonLeft))
            {
                process.Complete();
                GUIUtility.ExitGUI();
            }
            if (GUILayout.Button("Reject", EditorStyles.miniButtonRight))
            {
                process.Reject();
                GUIUtility.ExitGUI();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }
    }
}
