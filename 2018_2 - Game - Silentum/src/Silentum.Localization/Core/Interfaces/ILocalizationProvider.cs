﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public interface ILocalizationProvider<TValue> : IEnumerable<ILocalizedElement<TValue>>, IEnumerable
    {
        int Count { get; }

        bool HasValue(string key);
        TValue GetValue(string key);
        IEnumerable<ILocalizedElement<TValue>> GetAllElements();
    }
}
