﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Silentum.Localization
{
    public class LocalizationManager
    {
        private LocalizationManager()
        {
        }

        public readonly UnityEvent OnLocalizationChanged = new UnityEvent();

        public static SystemLanguage Language { get; private set; } = SystemLanguage.English;

        private static ConcurrentDictionary<Type, object> _providers = new ConcurrentDictionary<Type, object>();

        public static ILocalizationProvider<TValue> GetProvider<TValue>() => GetProviderProxy<TValue>();

        private static LocalizationProviderProxy<TValue> GetProviderProxy<TValue>()
        {
            LocalizationProviderProxy<TValue> provider = null;

            if (_providers.TryGetValue(typeof(TValue), out object boxedProvider))
            {
                provider = boxedProvider as LocalizationProviderProxy<TValue>;
            }

            if (provider == null)
            {
                provider = new LocalizationProviderProxy<TValue>(new LocalizationProvider<TValue>(null));
                _providers.AddOrUpdate(typeof(TValue), provider, (t, n) => provider);
            }

            return provider;
        }
    }
}
