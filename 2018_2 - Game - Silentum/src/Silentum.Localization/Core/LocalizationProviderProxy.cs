﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public sealed class LocalizationProviderProxy<TValue> : ILocalizationProvider<TValue>
    {
        public ILocalizationProvider<TValue> SourceProvider { get; private set; }
        public int Count => SourceProvider.Count;

        public LocalizationProviderProxy(ILocalizationProvider<TValue> provider)
        {
            SetProvider(provider);
        }

        public void SetProvider(ILocalizationProvider<TValue> provider) => SourceProvider = provider ?? throw new ArgumentNullException(nameof(provider));
        public IEnumerable<ILocalizedElement<TValue>> GetAllElements() => SourceProvider.GetAllElements();
        public TValue GetValue(string name) => SourceProvider.GetValue(name);
        public bool HasValue(string name) => SourceProvider.HasValue(name);
        public IEnumerator<ILocalizedElement<TValue>> GetEnumerator() => SourceProvider.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => SourceProvider.GetEnumerator();
    }
}
