﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public static class LocalizationProviderExtensions
    {
        public static string GetValue(this ILocalizationProvider<string> provider, string key, string[] args)
        {
            var value = provider.GetValue(key);
            return string.IsNullOrEmpty(value) ? value : string.Format(value, args);
        }
    }
}
