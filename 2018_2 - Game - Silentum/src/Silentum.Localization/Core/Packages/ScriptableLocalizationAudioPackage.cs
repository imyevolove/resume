﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [CreateAssetMenu(fileName = "LocalizationAudioPackage", menuName = "Silentum/Localization/Packages/Audio localization")]
    public class ScriptableLocalizationAudioPackage : ScriptableLocalizationPackageBase<LocalizedAudio, AudioClip>
    {
    }
}
