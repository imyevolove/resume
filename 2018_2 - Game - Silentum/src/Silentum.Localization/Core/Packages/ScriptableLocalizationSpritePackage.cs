﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [CreateAssetMenu(fileName = "LocalizationSpritePackage", menuName = "Silentum/Localization/Packages/Sprite localization")]
    public class ScriptableLocalizationSpritePackage : ScriptableLocalizationPackageBase<LocalizedSprite, Sprite>
    {
    }
}
