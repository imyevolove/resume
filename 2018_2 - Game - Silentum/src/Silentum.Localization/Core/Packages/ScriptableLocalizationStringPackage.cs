﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [CreateAssetMenu(fileName = "LocalizationStringPackage", menuName = "Silentum/Localization/Packages/String localization")]
    public class ScriptableLocalizationStringPackage : ScriptableLocalizationPackageBase<LocalizedString, string>
    {
    }
}
