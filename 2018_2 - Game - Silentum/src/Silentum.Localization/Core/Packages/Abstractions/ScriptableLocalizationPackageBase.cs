﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [Serializable]
    public abstract class ScriptableLocalizationPackageBase<TElement, TValue> : ScriptableObject, ILocalizationPackage<TValue>
        where TElement : class, ILocalizedElement<TValue>
    {
        public SystemLanguage Language => _language;
        public int Count => _elements.Count;

        [SerializeField] private SystemLanguage _language = SystemLanguage.English;
        [SerializeField] private List<TElement> _elements = new List<TElement>();

        public IEnumerable<ILocalizedElement<TValue>> GetAllElements() => _elements;
        public bool HasValue(string name) => _elements.Any(e => e != null && e.Name == name);
        public TValue GetValue(string name)
        {
            var element = _elements.FirstOrDefault(e => e != null && e.Name == name);
            return element == null || element.Value == null ? default : element.Value;
        }

        public IEnumerator<ILocalizedElement<TValue>> GetEnumerator() => _elements.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _elements.GetEnumerator();

    }
}
