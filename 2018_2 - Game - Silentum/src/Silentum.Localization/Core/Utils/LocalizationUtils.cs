﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public class LocalizationUtils
    {
        public static string GenerateGuid() => Guid.NewGuid().ToString();
    }
}
