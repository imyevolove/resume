﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [Serializable]
    public sealed class LocalizationSchemeElement
    {
        public string Key  { get => _key;  set => _key  = value; }
        public string Name { get => _name; set => _name = value; }

        [SerializeField] private string _key;
        [SerializeField] private string _name;

        public bool IsValid => !string.IsNullOrEmpty(_key) && !string.IsNullOrEmpty(_name);

        public LocalizationSchemeElement() : this(string.Empty)
        {
        }
        public LocalizationSchemeElement(string name) : this(name, LocalizationUtils.GenerateGuid())
        {
        }
        public LocalizationSchemeElement(string name, string key)
        {
            _name = name;
            _key = key;
        }
    }
}
