﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public interface ILocalizationScheme : IEnumerable<LocalizationSchemeElement>, IEnumerable
    {
        int Count { get; }
        LocalizationSchemeElement GetElementByName(string name);
        LocalizationSchemeElement GetElementByKey(string guid);
        IEnumerable<LocalizationSchemeElement> GetAllElements();
    }
}
