﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [CreateAssetMenu(fileName = "LocalizationScheme", menuName = "Silentum/Localization/Create localization scheme")]
    public sealed class LocalizationScheme : ScriptableObject, ILocalizationScheme, ISerializationCallbackReceiver
    {
        private static LocalizationSchemeElementEqualityComparer _comparer = new LocalizationSchemeElementEqualityComparer();

        public int Count => _elements.Count;

        [SerializeField] private List<LocalizationSchemeElement> _elements = new List<LocalizationSchemeElement>();

        public void InsertElement(LocalizationSchemeElement element) => _elements.Add(element);
        public bool RemoveElement(LocalizationSchemeElement element) => _elements.Remove(element);
        public void RemoveElement(int index) => _elements.RemoveAt(index);
        public void MoveElement(int from, int to)
        {
            if (!IndexInRange(from) || !IndexInRange(to) || from == to) return;

            var eFrom = _elements[from];
            var eTo = _elements[to];

            _elements[from] = eTo;
            _elements[to] = eFrom;
        }
        public void MoveElement(LocalizationSchemeElement element, int to) => MoveElement(_elements.IndexOf(element), to);
        public void MoveElementUp(LocalizationSchemeElement element) => MoveElementAt(element, -1);
        public void MoveElementDown(LocalizationSchemeElement element) => MoveElementAt(element, 1);

        public IEnumerable<LocalizationSchemeElement> GetAllElements() => _elements;
        public LocalizationSchemeElement GetElementByKey(string guid) => _elements.FirstOrDefault(e => e.Key == guid);
        public LocalizationSchemeElement GetElementByName(string name) => _elements.FirstOrDefault(e => e.Name == name);
        public IEnumerator<LocalizationSchemeElement> GetEnumerator() => _elements.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _elements.GetEnumerator();

        public void Cleanup() => _elements = _elements.Where(e => e != null && !string.IsNullOrEmpty(e.Key)).Distinct(_comparer).ToList();

        public void OnBeforeSerialize()
        {
        }
        public void OnAfterDeserialize()
        {
            Cleanup();
        }

        private bool IndexInRange(int index) => index >= 0 && index < _elements.Count;
        private void MoveElementAt(LocalizationSchemeElement element, int direction)
        {
            var index = _elements.IndexOf(element);
            MoveElement(index, index + direction);
        }
    }
}
