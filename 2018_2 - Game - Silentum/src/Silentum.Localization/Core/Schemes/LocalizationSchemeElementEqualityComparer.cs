﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public class LocalizationSchemeElementEqualityComparer : IEqualityComparer<LocalizationSchemeElement>
    {
        public bool Equals(LocalizationSchemeElement x, LocalizationSchemeElement y) => x.Key == y.Key;
        public int GetHashCode(LocalizationSchemeElement obj) => obj.GetHashCode();
    }
}
