﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public class LocalizationProvider<TValue> : ILocalizationProvider<TValue>
    {
        public int Count => _elements.Count;
        private List<ILocalizedElement<TValue>> _elements = new List<ILocalizedElement<TValue>>();

        public LocalizationProvider(IEnumerable<ILocalizedElement<TValue>> elements)
        {
            if (elements == null) return;
            foreach (var element in elements)
            {
                if (element == null) return;
                _elements.Add(element);
            }
        }

        public IEnumerable<ILocalizedElement<TValue>> GetAllElements() => _elements;
        public bool HasValue(string name) => _elements.Any(e => e != null && e.Name == name);
        public TValue GetValue(string name)
        {
            var element = _elements.FirstOrDefault(e => e != null && e.Name == name);
            return element == null || element.Value == null ? default : element.Value;
        }

        public IEnumerator<ILocalizedElement<TValue>> GetEnumerator() => _elements.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => _elements.GetEnumerator();
    }
}
