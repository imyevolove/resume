﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    /// <summary>
    /// Элемент локализации строки.
    /// </summary>
    [Serializable]
    public class LocalizedString : LocalizedElement<string>
    {
    }
    /// <summary>
    /// Элемент локализации аудио.
    /// </summary>
    [Serializable]
    public class LocalizedAudio : LocalizedElement<AudioClip>
    {
    }
    /// <summary>
    /// Элемент локализации спрайтов.
    /// </summary>
    [Serializable]
    public class LocalizedSprite : LocalizedElement<Sprite>
    {
    }
}
