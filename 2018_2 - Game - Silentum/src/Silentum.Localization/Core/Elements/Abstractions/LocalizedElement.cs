﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Silentum.Localization
{
    [Serializable]
    public abstract class LocalizedElement<TValue> : ILocalizedElement<TValue>
    {
        public string Name  => _name;
        public TValue Value => _value;

        [SerializeField] private string _name;
        [SerializeField] private TValue _value;

        public LocalizedElement()
        {
        }
        public LocalizedElement(string name) : this(name, default)
        {
        }
        public LocalizedElement(string name, TValue value)
        {
            _name  = name;
            _value = value;
        }
    }
}
