﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization
{
    public interface ILocalizedElement<TValue>
    {
        string  Name   { get; }
        TValue  Value { get; }
    }
}
