﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Silentum.Localization.Editor
{
    public class LocalizationSchemeEditorWindow : EditorWindow
    {
        [MenuItem("Window/Localization/Localization scheme window")]
        public static LocalizationSchemeEditorWindow ShowWindow()
        {
            var window = GetWindow(typeof(LocalizationSchemeEditorWindow)) as LocalizationSchemeEditorWindow;
            ConfigureWindow(window);
            return window;
        }

        public static LocalizationSchemeEditorWindow OpenWindow(LocalizationScheme scheme)
        {
            var window = ShowWindow();
            window.SetScheme(scheme);

            return window;
        }

        private static void ConfigureWindow(LocalizationSchemeEditorWindow window)
        {
            window.titleContent = new GUIContent("Scheme editor");
        }

        public LocalizationScheme Scheme { get; private set; }
        
        private Vector2 _scroll;
        private int _elementIndex = 0;
        private const string NameEmptyWarningMessage = "[NAME IS EMPTY]";
        private readonly LocalizationSchemeEditorSettings Settigns = new LocalizationSchemeEditorSettings();
        private readonly LocalizationSchemeEditContext EditContext = new LocalizationSchemeEditContext();

        public void SetScheme(LocalizationScheme scheme) => Scheme = scheme;

        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(300));
            RenderLGUI();
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.ExpandWidth(true));
            RenderRGUI();
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        private void RenderLGUI()
        {
            RenderSchemeSelectorGUI();
            RenderSettignsGUI();
            RenderSchemeMonitor();
        }
        private void RenderRGUI()
        {
            if (EditContext.IsConfigured)
            {
                RenderElementEditGUI();
            }
            else
            {
                RenderSchemeGUI();
            }
        }

        private void RenderSchemeSelectorGUI()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Selected scheme", EditorStyles.boldLabel);
            Scheme = EditorGUILayout.ObjectField(Scheme, typeof(LocalizationScheme), false) as LocalizationScheme;
            EditorGUILayout.EndVertical();
        }

        private void RenderSchemeMonitor()
        {
            if (Scheme == null) return;

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            #region Header
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(Scheme.name, EditorStyles.boldLabel);
            if (GUILayout.Button("Ping object", EditorStyles.miniButton))
            {
                EditorGUIUtility.PingObject(Scheme);
            }
            EditorGUILayout.EndHorizontal();
            #endregion
            EditorGUILayout.LabelField("Elements count: ", Scheme.Count.ToString());

            if (GUILayout.Button("Add element", EditorStyles.miniButton))
            {
                Scheme.InsertElement(new LocalizationSchemeElement());
            }

            EditorGUILayout.EndVertical();
        }

        private void RenderSettignsGUI()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);
            Settigns.DeleteImmediate = EditorGUILayout.Toggle("Delete immediate", Settigns.DeleteImmediate);
            EditorGUILayout.EndVertical();
        }

        private void RenderSchemeGUI()
        {
            if (Scheme == null)
            {
                EditorGUILayout.HelpBox("Scheme is not selected", MessageType.Warning);
                return;
            }

            _scroll = EditorGUILayout.BeginScrollView(_scroll);

            _elementIndex = 0;
            foreach (var element in Scheme)
            {
                RenderElementGUI(_elementIndex, element);
                _elementIndex++;
            }

            EditorGUILayout.EndScrollView();
        }

        private void RenderElementEditGUI()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Localization scheme element editor", EditorStyles.boldLabel);
            if (GUILayout.Button("Exit", EditorStyles.miniButton, GUILayout.Width(70)))
            {
                EditContext.Clear();
                Settigns.EditElementKey = false;
                GUI.FocusControl(null);
                GUIUtility.ExitGUI();
            }
            EditorGUILayout.EndHorizontal();
            EditContext.Element.Name = EditorGUILayout.TextField("Name", EditContext.Element.Name);
            GUI.enabled = Settigns.EditElementKey;
            EditContext.Element.Key = EditorGUILayout.TextField("Key", EditContext.Element.Key);
            GUI.enabled = true;
            RenderElementKeyEditControls(EditContext.Element);
            EditorGUILayout.EndVertical();
        }

        private void RenderElementKeyEditControls(LocalizationSchemeElement element)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Generate new key", EditorStyles.miniButtonLeft))
            {
                element.Key = LocalizationUtils.GenerateGuid();
            }
            if (GUILayout.Button("Edit key", EditorStyles.miniButtonRight))
            {
                Settigns.EditElementKey = !Settigns.EditElementKey;
            }
            EditorGUILayout.EndHorizontal();
        }

        private void RenderElementGUI(int index, LocalizationSchemeElement element)
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            EditorGUILayout.LabelField(string.IsNullOrEmpty(element.Name) ? NameEmptyWarningMessage : element.Name, EditorStyles.boldLabel);
            RenderElementMovementControls(element);

            if (GUILayout.Button("Edit", EditorStyles.miniButtonLeft, GUILayout.Width(70)))
            {
                EditContext.Configure(Scheme, element);
                GUIUtility.ExitGUI();
            }
            if (GUILayout.Button("Delete", EditorStyles.miniButtonRight, GUILayout.Width(70)))
            {
                RenderRemoveElementGUI(element);
                GUIUtility.ExitGUI();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void RenderElementMovementControls(LocalizationSchemeElement element)
        {
            if (GUILayout.Button("⤴", EditorStyles.miniButtonLeft, GUILayout.Width(30)))
            {
                Scheme.MoveElementUp(element);
                GUIUtility.ExitGUI();
            }
            if (GUILayout.Button("⤵", EditorStyles.miniButtonRight, GUILayout.Width(30)))
            {
                Scheme.MoveElementDown(element);
                GUIUtility.ExitGUI();
            }
        }

        private void RenderRemoveElementGUI(LocalizationSchemeElement element)
        {
            if (Settigns.DeleteImmediate)
            {
                RemoveElementImmediate(element);
            }
            else
            {
                RemoveElement(element);
            }
            GUIUtility.ExitGUI();
        }
        private void RemoveElement(LocalizationSchemeElement element)
        {
            if (EditorUtility.DisplayDialog("Localization element deleting", "Delete element?", "Yes", "No"))
            {
                RemoveElementImmediate(element);
            }
        }
        private void RemoveElementImmediate(LocalizationSchemeElement element) => Scheme.RemoveElement(element);
    }
}
