﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silentum.Localization.Editor
{
    public class LocalizationSchemeEditorSettings
    {
        public bool DeleteImmediate { get; set; }
        public bool EditElementKey { get; set; }
    }
}
