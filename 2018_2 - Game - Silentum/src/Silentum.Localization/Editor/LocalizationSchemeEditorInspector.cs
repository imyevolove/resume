﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Silentum.Localization.Editor
{
    [CustomEditor(typeof(LocalizationScheme))]
    public class LocalizationSchemeEditorInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Edit"))
            {
                LocalizationSchemeEditorWindow.OpenWindow(target as LocalizationScheme);
            }
        }
    }
}
