﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;

namespace Silentum.Localization.Editor
{
    public class LocalizationSchemeEditContext
    {
        public LocalizationScheme Scheme { get; private set; }
        public LocalizationSchemeElement Element { get; private set; }
        public bool IsConfigured => Scheme != null && Element != null;

        public void Configure(LocalizationScheme scheme, LocalizationSchemeElement element)
        {
            Scheme = scheme;
            Element = element;
        }

        public void Clear()
        {
            Scheme = null;
            Element = null;
        }
    }
}
