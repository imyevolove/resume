﻿using System.Linq;

namespace Generic.String
{
    public class StringFormat
    {
        public static string Format(string str, StringCase strCase)
        {
            switch (strCase)
            {
                case StringCase.Lower:
                    return str.ToLower();

                case StringCase.Upper:
                    return str.ToUpper();

                case StringCase.Capitalize:
                    return CapitalizeString(str);

                case StringCase.CapitalizeAll:
                    string[] ls = str.Split(' ');
                    ls = ls.Select(s => char.ToUpper(s[0]) + s.Substring(1).ToLower() ).ToArray();

                    return string.Join(" ", ls);

                case StringCase.None:
                default:
                    return str;
            }
        }

        protected static string CapitalizeString(string str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}
