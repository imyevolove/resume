﻿using IncEngine;
using IncEngine.UI;

namespace Wac
{
    public enum PopupId
    {
        CaseExplorer,
        DropExplorer,
        InventoryItemExplorer,
        ItemsDrop,
        DailyReward
    }

    public class PopupManager
    {
        private static Router<PopupId, PopupSingle> Router = new Router<PopupId, PopupSingle>();

        public static bool Add(PopupId id, PopupSingle obj)
        {
            /// For call awake
            obj.Show();
            obj.Hide();

            return Router.Add(id, obj);
        }

        public static bool Remove(PopupId id)
        {
            return Router.Remove(id);
        }

        public static PopupSingle Get(PopupId id)
        {
            return Router.Get(id);
        }

        public static T Get<T>(PopupId id) where T : PopupSingle
        {
            return Router.Get(id) as T;
        }
    }
}
