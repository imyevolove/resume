﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Services
{
    public interface IResourcesProvider
    {
        void Add<T>(T item) where T : class;
        void Remove<T>() where T : class;
        void Remove(Type type);
        T Get<T>() where T : class;
    }
}
