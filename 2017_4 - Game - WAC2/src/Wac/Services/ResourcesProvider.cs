﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Services
{
    public class ResourcesProvider : IResourcesProvider
    {
        private Dictionary<Type, object> _resources;

        public ResourcesProvider()
        {
            _resources = new Dictionary<Type, object>();
        }

        void IResourcesProvider.Add<T>(T resource)
        {
            _resources[typeof(T)] = resource;
        }

        void IResourcesProvider.Remove<T>()
        {
            _resources.Remove(typeof(T));
        }

        void IResourcesProvider.Remove(Type type)
        {
            _resources.Remove(type);
        }

        T IResourcesProvider.Get<T>()
        {
            return _resources[typeof(T)] as T;
        }
    }
}
