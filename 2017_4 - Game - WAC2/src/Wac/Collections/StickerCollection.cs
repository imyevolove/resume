﻿using IncEngine.Structures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Collections
{
    public class StickerCollection : List<Sticker>
    {
        public Sticker Find(string id)
        {
            return Find(sticker => sticker.Id == id);
        }
    }
}
