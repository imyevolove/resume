﻿using IncEngine.Structures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Collections
{
    public class CaseCollection : List<Case>
    {
        /// <summary>
        /// Find case by collection
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public Case Find(Collection collection)
        {
            return Find(cs => cs.Collection == collection);
        }

        /// <summary>
        /// Find case by skin
        /// </summary>
        /// <param name="skin"></param>
        /// <returns></returns>
        public Case FindCase(Skin skin)
        {
            return Find(cs => cs.Skins.Contains(skin));
        }

        /// <summary>
        /// Find case by collection and quality
        /// </summary>
        /// <param name="skin"></param>
        /// <returns></returns>
        public Case Find(Collection collection, Quality quality)
        {
            return Find(cs => cs.Collection == collection && cs.HasSkin(quality));
        }

        /// <summary>
        /// Find case by skin and quality
        /// </summary>
        /// <param name="skin"></param>
        /// <param name="quality"></param>
        /// <returns></returns>
        public Case Find(Skin skin, Quality quality)
        {
            return Find(cs => cs.HasSkin(skin) && cs.HasSkin(quality));
        }

        /// <summary>
        /// Find case by quality
        /// </summary>
        /// <param name="quality"></param>
        /// <returns></returns>
        public Case Find(Quality quality)
        {
            return Find(cs => cs.HasSkin(quality));
        }

        /// <summary>
        /// Find case by qualities
        /// </summary>
        /// <param name="qualities"></param>
        /// <returns></returns>
        public Case Find(IEnumerable<Quality> qualities)
        {
            Case result = null;

            foreach (var quality in qualities)
            {
                result = Find(quality);

                if (result != null) continue;
            }

            return result;
        }

        /// <summary>
        /// Find case by skin
        /// </summary>
        /// <param name="skin"></param>
        /// <returns></returns>
        public Case Find(IEnumerable<Collection> collections, Quality quality)
        {
            Case result = null;

            foreach (var collection in collections)
            {
                result = Find(collection, quality);

                if (result != null) continue;
            }

            return result;
        }
    }
}
