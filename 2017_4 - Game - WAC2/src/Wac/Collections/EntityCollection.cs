﻿using System.Collections.Generic;
using IncEngine.Structures;

namespace Wac.Collections
{
    public class EntityCollection
    {
        public readonly List<Entity>    Originals   = new List<Entity>();
        public readonly List<Skin>      Skins       = new List<Skin>();

        /// <summary>
        /// Retrun original exists status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasExistsOriginal(string id)
        {
            return Originals.Exists(o => o.Id == id);
        }

        /// <summary>
        /// Retrun the found original entity or default.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity GetOriginal(string id)
        {
            return Originals.Find(o => o.Id == id);
        }

        /// <summary>
        /// Retrun the found original entity or default.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity GetOriginal(string id, EntityType type)
        {
            return Originals.Find(o => o.Type == type && o.Id == id);
        }

        /// <summary>
        /// Retrun the found original entity or default.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Entity> GetAllOriginals(EntityType type)
        {
            return Originals.FindAll(o => o.Type == type);
        }

        /// <summary>
        /// Retrun the found skin entity or default.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Skin GetSkin(string id)
        {
            return Skins.Find(o => { return o.Id == id; });
        }

        /// <summary>
        /// Retrun the found skin entity or default.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Skin> GetAllSkins(EntityType type)
        {
            return Skins.FindAll(o => { return o.Type == type; });
        }

        /// <summary>
        /// Retrun the found skin entity or default.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Skin GetSkin(string id, EntityType type)
        {
            return Skins.Find(o => o.Type == type && o.Id == id );
        }

        /// <summary>
        /// Retrun skin exists status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasExistsSkin(string id)
        {
            return Skins.Exists(o => { return o.Id == id; });
        }
    }
}
