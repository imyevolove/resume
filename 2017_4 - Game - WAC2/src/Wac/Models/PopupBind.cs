﻿using IncEngine.UI;
using System;
using UnityEngine;

namespace Wac.Models
{
    [Serializable]
    public struct PopupBind
    {
        [SerializeField] public PopupId id;
        [SerializeField] public PopupSingle   popup;
    }
}
