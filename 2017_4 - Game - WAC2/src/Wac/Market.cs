﻿using IncEngine;
using IncEngine.Abilities;
using IncEngine.Collections;

namespace Wac
{
    public class Market : IMarket
    {
        public float Tax;

        public Market()
        {
        }

        public Market(float tax)
        {
            Tax = tax;
        }

        public ulong GetSalePrice(InventoryItem item)
        {
            if (item == null) return 0;

            var price = item.GetPrice();
            price += Game.Player.Abilities.GetAbility(AbilityType.SaleValueIncrease).ApplyToValue(price);
            price -= (ulong)(price * Tax);

            return price;
        }
        
    }
}
