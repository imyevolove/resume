﻿using IncEngine.UpdateManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Wac.Online
{
    public class TimeManager : BaseSynchronizer, ITimeManager
    {
        public event Action<DateTime> Event_TimeChange;

        private IServerResponseDateTimeParser _dateParser;
        
        private DateTime _currentDate = new DateTime(0);
        public DateTime CurrentDate
        {
            get { return _currentDate; }
            private set
            {
                if (_currentDate != value)
                {
                    _currentDate = value;

                    if (Event_TimeChange != null)
                    {
                        Event_TimeChange(_currentDate);
                    }
                }
            }
        }
        
        private string _suncServerUri;
        
        public TimeManager(string serverUri, IServerResponseDateTimeParser dateParser)
        {
            _suncServerUri = serverUri;
            _dateParser = dateParser;

            UpdateManager.StartRepeate(1, TimeTick);
        }
        
        protected override void SyncCore(SynchronizationEvent callback)
        {
            var request = UnityWebRequest.Get(_suncServerUri);
            SynchronizationUtility.SunchronizationApiUnit.Api_SendRequest(request, (result) => 
            {
                var state = ConvertResponseStatusToSynchronizationState(result);

                if (state == SynchronizationState.SynchronizationCompleted)
                {
                    try
                    {
                        CurrentDate = _dateParser.Parse(result.downloadHandler.text);
                    }
                    catch (Exception)
                    {
                        state = SynchronizationState.SynchronizationFailed;

                        Debug.LogError("Date time parse error");
                    }
                }

                State = state;
                callback(state);
            });
        }

        private void TimeTick()
        {
            if (State != SynchronizationState.SynchronizationCompleted) return;
            CurrentDate.AddSeconds(1);
        }

        private static SynchronizationState ConvertResponseStatusToSynchronizationState(UnityWebRequest request)
        {
            return request.isDone 
                ? SynchronizationState.SynchronizationCompleted
                : SynchronizationState.SynchronizationFailed;
        }
    }
}
