﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Online
{
    public class ForbitenServerResponseDateTimeParser : IServerResponseDateTimeParser
    {
        public DateTime Parse(string text)
        {
            var unixTimeOffset = long.Parse(text);
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(unixTimeOffset);
        }
    }
}
