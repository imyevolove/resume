﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Online
{
    public interface IServerResponseDateTimeParser
    {
        DateTime Parse(string text);
    }
}
