﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Networking;

namespace Wac.Online
{
    public class SynchronizationUtility
    {
        private static SunchronizationApiUnit _api;
        public static SunchronizationApiUnit SunchronizationApiUnit
        {
            get
            {
                if (_api == null || _api.gameObject == null)
                {
                    _api = SunchronizationApiUnit.Create();
                }

                return _api;
            }
        }
    }
}
