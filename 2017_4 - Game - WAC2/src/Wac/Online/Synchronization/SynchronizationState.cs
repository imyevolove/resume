﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Online
{
    public enum SynchronizationState
    {
        SynchronizationNeeded,
        SynchronizationCompleted,
        SynchronizationFailed
    }
}
