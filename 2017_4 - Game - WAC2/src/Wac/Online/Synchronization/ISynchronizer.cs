﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Online
{
    public interface ISynchronizer
    {
        event SynchronizationEvent Event_StateChange;

        bool IsSynchronized { get; }

        SynchronizationState State { get; }
        DateTime LastSynchronizationDate { get; }

        void Sync();
        void Sync(SynchronizationEvent callback);
    }
}
