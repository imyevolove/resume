﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Wac.Online
{
    public class SunchronizationApiUnit : MonoBehaviour, ISynchronizationApi
    {
        private static int _unitCounter = 0;

        /// <summary>
        /// Setup unit here
        /// </summary>
        private void Awake()
        {
            DontDestroyOnLoad(this);

            _unitCounter++;
        }

        protected virtual void OnDisable()
        {
            enabled = true;
            gameObject.SetActive(true);
        }

        public static SunchronizationApiUnit Create()
        {
            var container = new GameObject(GenerateUnitName(), typeof(SunchronizationApiUnit));
            container.hideFlags = HideFlags.HideAndDontSave | HideFlags.NotEditable;

            return container.GetComponent<SunchronizationApiUnit>();
        }

        private static string GenerateUnitName()
        {
            return string.Format("[{0}]_sync_api_unit", _unitCounter);
        }

        public void Api_SendRequest(UnityWebRequest request, Action<UnityWebRequest> callback)
        {
            if (request == null)
            {
                throw new ArgumentNullException("Request is null");
            }

            if (callback == null)
            {
                throw new ArgumentNullException("Callback is null");
            }

            StartCoroutine(SendRequestCoroutine(request, callback));
        }

        private IEnumerator SendRequestCoroutine(UnityWebRequest request, Action<UnityWebRequest> callback)
        {
            yield return request.Send();

            callback(request);
        }
    }
}
