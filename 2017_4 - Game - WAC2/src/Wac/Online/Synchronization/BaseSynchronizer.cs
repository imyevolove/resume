﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wac.Online
{
    public abstract class BaseSynchronizer : ISynchronizer
    {
        public event SynchronizationEvent Event_StateChange;

        private SynchronizationState _state = SynchronizationState.SynchronizationNeeded;
        public SynchronizationState State
        {
            get { return _state; }
            protected set
            {
                if (_state != value)
                {
                    _state = value;

                    if (Event_StateChange != null)
                    {
                        Event_StateChange(_state);
                    }
                }
            }
        }

        public DateTime LastSynchronizationDate { get; private set; }

        public bool IsSynchronized { get { return State == SynchronizationState.SynchronizationCompleted; } }

        public void Sync()
        {
            Sync((result) => { });
        }

        public void Sync(SynchronizationEvent callback)
        {
            SyncCore((result) => {
                LastSynchronizationDate = DateTime.Now;
                SyncCore(callback);
            });
        }

        protected abstract void SyncCore(SynchronizationEvent callback);
    }
}
