﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Networking;

namespace Wac.Online
{
    public interface ISynchronizationApi
    {
        void Api_SendRequest(UnityWebRequest request, Action<UnityWebRequest> callback);
    }
}
