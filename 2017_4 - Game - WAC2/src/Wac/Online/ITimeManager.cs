﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Online
{
    public interface ITimeManager : ISynchronizer
    {
        event Action<DateTime> Event_TimeChange;

        DateTime CurrentDate { get; }
    }
}
