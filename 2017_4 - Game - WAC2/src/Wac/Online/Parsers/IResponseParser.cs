﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Online.Parsers
{
    public interface IResponseParser<T>
    {
        T Parse(string response);
    }
}
