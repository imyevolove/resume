﻿using UnityEngine;

namespace Wac.Common
{
    [DisallowMultipleComponent]
    public abstract class AbstractSetuper : MonoBehaviour
    {
        protected abstract void Setup();

        void Start()
        {
            Setup();
        }
    }
}
