﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wac.Common
{
    [Serializable]
    public class PrefabSpawner : MonoBehaviour
    {
        [SerializeField] public Transform parent;
        [SerializeField] public GameObject prefab;

        protected virtual void Start()
        {
            SpawnPrefab();
            SelfDestroy();
        }

        protected virtual void SpawnPrefab()
        {
            if (prefab == null) return;

            var parentTransform = parent ?? transform.parent;

            Instantiate(prefab, parentTransform, false);
        }

        protected virtual void SelfDestroy()
        {
            Destroy(gameObject);
        }
    }
}
