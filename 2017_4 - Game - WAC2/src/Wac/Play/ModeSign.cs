﻿using IncEngine.Play;

namespace Wac.Play
{
    public static class ModeSignatures
    {
        public static readonly ModeSignature Normal = new ModeSignature("Waves");
        public static readonly ModeSignature Timer  = new ModeSignature("Time left");
    }
}
