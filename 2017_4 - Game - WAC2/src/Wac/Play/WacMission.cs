﻿using IncEngine;
using IncEngine.Play;
using IncEngine.Structures;
using System;

namespace Wac.Play
{
    public class WacMission : Mission
    {
        public event Action<Map> Event_MapChange;

        private Map m_Map;
        public Map Map
        {
            get { return m_Map; }
            protected set
            {
                m_Map = value;
                OnMapChange(m_Map);
            }
        }

        public WacMission(Stage stage)
            : base(stage)
        {
            UpdateMap();
        }

        protected void UpdateMap()
        {
            var index = UnityEngine.Random.Range(0, EntitiesManager.Maps.Count);
            Map = EntitiesManager.Maps[index];
        }

        protected override void OnLevelChange(ulong level)
        {
            base.OnLevelChange(level);
            UpdateMap();
            
            /*
            Debug.LogFormat("IS Mobs: {0}, Bomb: {1}, Hostage: {2}",
                Stage.Task.Compare(TaskSign.DestroyEnemies),
                Stage.Task.Compare(TaskSign.DefuseBomb),
                Stage.Task.Compare(TaskSign.SaveHostage));
                */
        }

        protected virtual void OnMapChange(Map map)
        {
            if (Event_MapChange != null)
            {
                Event_MapChange(map);
            }
        }
    }
}
