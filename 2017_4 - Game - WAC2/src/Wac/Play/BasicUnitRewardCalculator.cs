﻿using IncEngine.Play;
using UnityEngine;

namespace Wac.Play
{
    public class BasicUnitRewardCalculator : IUnitRewardCalculator
    {
        public float Multiplier = 1.82f; 
        public ulong Divider = 44;

        public BasicUnitRewardCalculator()
        {
        }

        public BasicUnitRewardCalculator(float multiplier, ulong divider)
        {
            Multiplier = multiplier;
            Divider = divider;
        }

        public ulong Calculate(IUnit unit, ulong index)
        {
            if (unit == null) return 0;
            if (index == 0) return 1;
            if (unit.MaxHealth < Divider) return index;

            return index + unit.MaxHealth / (ulong)(Divider * Multiplier);
        }
    }
}
