﻿using IncEngine.Play;
using UnityEngine;

namespace Wac.Play
{
    public class BasicUnitHealthCalculator : IUnitHealthCalculator
    {

        public ulong BaseHealth = 20;
        public float HealthMultiplier = 1.55f;
        public bool IsBoss = false;
        public ulong BossMultiplier = 5;

        public ulong Calculate(ulong index)
        {
            //return (IsBoss ? BossMultiplier : 1UL) * BaseHealth * (ulong)(Mathf.Pow(index, HealthMultiplier) * Mathf.Max(Mathf.Log(index, HealthMultiplier), 1));
            //return (IsBoss ? BossMultiplier : 1UL) * BaseHealth * (ulong)(Math.Pow(index + 1, HealthMultiplier) / (index + 1));
            return (IsBoss ? BossMultiplier : 1UL) * BaseHealth * index + (ulong)(Mathf.Pow(index * Mathf.Log10(index + 10UL), HealthMultiplier));
        }

        public void Calculate(IUnit unit, ulong index)
        {
            unit.Configure(Calculate(index));
        }
    }
}
