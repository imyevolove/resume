﻿using IncEngine.Collections;
using IncEngine.Localization;
using IncEngine.Personal;
using IncEngine.Structures;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Popups
{
    public enum DropExplorerSelectionType
    {
        Skin,
        Coins
    }

    [Serializable]
    public class DropExplorerPopup : PopupSingle
    {
        [SerializeField] public Animator    animator;
        [SerializeField] public Text        dropTitleText;
        [SerializeField] public Text        dropExteriorQualityText;
        [SerializeField] public Text        statsText;
        [SerializeField] public Image       gradientImage;
        [SerializeField] public Image       iconImage;
        [SerializeField] public Text        buttonSkinTitleText;
        [SerializeField] public Text        buttonCoinsTitleText;
        [SerializeField] public Text        buttonCoinsValueText;

        public event Action<DropExplorerSelectionType> Event_Select;

        public InventorySkinItem        TargetInventorySkinItem     { get; protected set; }
        public Player                   TargetPlayer                { get; protected set; }

        private ulong m_CachedTargetInventorySkinItemPrice;

        public void SelectionActionTakeSkin()
        {
            if (Validate())
            {
                Debug.Log("TAKE SKIN (" + TargetInventorySkinItem.Skin.FullName + ")");
                TargetPlayer.Inventory.Add(TargetInventorySkinItem);
            }

            CallEventSelect(DropExplorerSelectionType.Skin);
            Hide();
        }

        public void SelectionActionTakeCoins()
        {
            if (Validate())
            {
                Debug.Log("TAKE COINS (" + m_CachedTargetInventorySkinItemPrice + ")");
                TargetPlayer.Money.Increase(m_CachedTargetInventorySkinItemPrice);
            }

            CallEventSelect(DropExplorerSelectionType.Coins);
            Hide();
        }

        public void Explore(InventorySkinItem inventoryItem, Player player)
        {
            TargetInventorySkinItem = inventoryItem;
            TargetPlayer = player;

            UpdateDetails();
            Show();

            animator.SetBool("active", true);
            animator.Play("Active", -1, 0f);
        }

        public void Explore(InventorySkinItem inventoryItem, Player player, Action onResult)
        {
            Action<DropExplorerSelectionType> tDelegate = null;
            tDelegate = (t) =>
            {
                Event_Select -= tDelegate;
                onResult();
            };
            Event_Select += tDelegate;

            Explore(inventoryItem, player);
        }

        protected void UpdateDetails()
        {
            m_CachedTargetInventorySkinItemPrice = Game.Market.GetSalePrice(TargetInventorySkinItem);

            dropTitleText.text              = TargetInventorySkinItem.Skin.FullName.ToUpper();
            dropExteriorQualityText.text    = ExteriorQuality.GetLocalizationForExterior(TargetInventorySkinItem.Exterior).ToUpper();
            statsText.text                  = BuildStatsText(TargetInventorySkinItem.BaseDamage, TargetInventorySkinItem.BaseIncome);
            gradientImage.color             = TargetInventorySkinItem.Skin.Quality.Color;
            iconImage.sprite                = TargetInventorySkinItem.Skin.Icon;
            buttonCoinsValueText.text       = Game.NumberFormatter.Format(m_CachedTargetInventorySkinItemPrice);
        }

        protected bool Validate()
        {
            if (TargetInventorySkinItem == null)
            {
                Debug.Log("InventorySkinItem not defined");
                return false;
            }

            if (TargetPlayer == null)
            {
                Debug.Log("TargetPlayer not defined");
                return false;
            }

            return true;
        }

        protected void CallEventSelect(DropExplorerSelectionType type)
        {
            if (Event_Select != null)
            {
                Event_Select(type);
            }
        }

        protected string BuildStatsText(ulong damagePerClick, ulong damagePerSecond)
        {
            return string.Format(
                "{2} <color=#FFFA44>{0}</color> " +
                "{3} <color=#FFFA44>{1}</color>",
                Game.NumberFormatter.Format(damagePerClick), Game.NumberFormatter.Format(damagePerSecond),
                GetLocalization("dpc"), GetLocalization("dps"));
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
