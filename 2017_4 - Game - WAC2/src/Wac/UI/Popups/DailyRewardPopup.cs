﻿using IncEngine.UI;
using System;
using Wac.Gifts;
using Uni.DependencyInjection;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Popups
{
    [Serializable]
    public class DailyRewardPopup : PopupSingle
    {
        [SerializeField] public Text GiftNameText;
        [SerializeField] public Image GiftIconImage;

        private DailyReward _dailyReward;

        protected void Awake()
        {
            var application = Uni.ApplicationManagement.Application.Main;
            _dailyReward = application.ServiceProvider.GetService<DailyReward>();

            _dailyReward.Event_GiftGiven += ShowReward;
        }

        public void ShowReward(Gift gift)
        {
            GiftNameText.text = gift.Name;
            GiftIconImage.sprite = gift.Icon;

            Show();
        }
    }
}
