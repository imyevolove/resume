﻿using IncEngine;
using IncEngine.Activities;
using IncEngine.Dice;
using IncEngine.Localization;
using IncEngine.Personal;
using IncEngine.Structures;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Popups
{
    [Serializable]
    public class CaseExplorerPopup : PopupSingle
    {
        [Header("Prefabs")]
        [SerializeField] public SkinExplorer skinExplorerItemPrefab;

        [Header("Pointers")]
        [SerializeField] public int rollItemsCount;
        [SerializeField] public CaseRollSlider rollSlider;
        [SerializeField] public Activity rollingBlockPanel;
        [SerializeField] public Activity explorerContainer;
        [SerializeField] public Button closeButton;
        [SerializeField] public Button luckButton;
        [SerializeField] public Text rollingBlockPanelText;
        [SerializeField] public Text closeButtonText;
        [SerializeField] public Text priceText;

        private MonoEntityPool<SkinExplorer> m_ExplorerPoolItems;
        private List<SkinExplorer> m_ActivatedExplorerItems;

        public Case         TargetCase          { get; protected set; }
        public CaseOpener   TargetCaseOpener    { get; protected set; }
        public Player       TargetPlayer        { get; protected set; }

        public RollState State = RollState.Idle;
        public bool IsReadyToUse { get { return State == RollState.Idle; } }

        private CaseOpener m_DefaultCaseOpener;

        public enum RollState
        {
            Idle,
            Rolling
        }

        void Awake()
        {
            m_ExplorerPoolItems = new MonoEntityPool<SkinExplorer>(skinExplorerItemPrefab);
            m_ActivatedExplorerItems = new List<SkinExplorer>();
            m_DefaultCaseOpener = Game.CaseOpener;

            UnockActionsPanel();
        }

        protected virtual void Start()
        {
            LocalizationManager.onChange += RedrawUI;
        }

        /// <summary>
        /// Start rolling
        /// </summary>
        public void Roll()
        {
            if (!ValidateReadyToUse()) return;
            if (!IsReadyToUse) return;

            State = RollState.Rolling;

            LockPopup();
            LockActionsPanel();

            rollSlider.Roll(TargetCase, 20, TargetCaseOpener, OnRollEnd);
            TargetPlayer.Money.Decrease(TargetCase.Price);
        }

        /// <summary>
        /// Set active user
        /// </summary>
        /// <param name="player"></param>
        public void SetPlayer(Player player)
        {
            TargetPlayer = player;
        }

        /// <summary>
        /// Explore case
        /// </summary>
        /// <param name="cs"></param>
        /// <param name="opener"></param>
        public void Explore(Case cs, CaseOpener opener = null)
        {
            TargetCase = cs;
            TargetCaseOpener = opener;

            if (TargetCaseOpener == null)
            {
                TargetCaseOpener = m_DefaultCaseOpener;
            }
            
            DeactivateCurrentExploreItems();
            RebuildExplorer(TargetCase);
            RedrawUI();

            rollSlider.RebuildRollItems(TargetCase, rollItemsCount, TargetCaseOpener);
        }

        protected void RedrawUI()
        {
            RedrawPrice();
            RedrawCloseButton();
        }

        protected void RedrawPrice()
        {
            priceText.text = Game.NumberFormatter.Format(TargetCase.Price);
        }
        
        protected void RebuildExplorer(Case cs)
        {
            foreach (var skin in cs.Skins)
            {
                var explorerItem = GetFreeExplorerItem();
                explorerItem.SetTarget(skin);
                explorerItem.Transform.SetAsLastSibling();

                m_ActivatedExplorerItems.Add(explorerItem);
            }
        }

        protected void DeactivateCurrentExploreItems()
        {
            foreach (var explorer in m_ActivatedExplorerItems)
            {
                explorer.Deactivate();
            }

            m_ActivatedExplorerItems.Clear();
        }

        protected SkinExplorer GetFreeExplorerItem()
        {
            var item = m_ExplorerPoolItems.GetFree();
            item.transform.SetParent(explorerContainer.Transform, false);
            
            return item;
        }

        protected virtual void OnRollEnd(Skin skin)
        {
            State = RollState.Idle;

            UnlockPopup();
            UnockActionsPanel();

            var inventorySkinItem = TargetCaseOpener.GenerateInventoryItem(skin);
            var dropExplorer = PopupManager.Get<DropExplorerPopup>(PopupId.DropExplorer);

            dropExplorer.Explore(inventorySkinItem, TargetPlayer);
        }

        protected void LockPopup()
        {
            RedrawCloseButton();
            Lock();
        }

        protected void UnlockPopup()
        {
            RedrawCloseButton();
            Unlock();
        }

        protected void RedrawCloseButton()
        {
            closeButtonText.text = GetLocalization(State == RollState.Idle 
                ? "caseopener_cancel"
                : "caseopener_rolling");
        }

        protected void LockActionsPanel()
        {
            rollingBlockPanel.Show();
        }

        protected void UnockActionsPanel()
        {
            rollingBlockPanel.Hide();
        }

        protected bool ValidateReadyToUse()
        {
            if (!IsShown || !rollSlider.IsReady)
            {
                Debug.LogWarning("Can't use roll. Popup not shown or roll slider is not ready");
                return false;
            }

            if (TargetPlayer == null)
            {
                Debug.LogWarning("Player not found");
                return false;
            }

            if (TargetCase == null)
            {
                Debug.LogWarning("Case not found");
                return false;
            }

            if (TargetPlayer.Money.Value < TargetCase.Price)
            {
                Debug.LogWarning("Not enough money");
                return false;
            }

            if (TargetCaseOpener == null)
            {
                Debug.LogWarning("Case Opener not found");
                return false;
            }

            return true;
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
