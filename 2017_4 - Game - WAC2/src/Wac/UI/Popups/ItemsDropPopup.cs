﻿using IncEngine;
using IncEngine.Structures;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wac.UI.Popups
{
    [Serializable]
    public class ItemsDropPopup : PopupSingle
    {
        [Header("Prefabs")]
        [SerializeField] public SkinExplorer ItemExplorePrefab;

        [Header("Preferences")]
        [SerializeField] public Transform ItemsContainerTransform;

        private Skin[] m_TargetItems;

        private MonoEntityPool<SkinExplorer> m_ExplorerPoolItems;
        private List<SkinExplorer> m_ActivatedExplorerItems;

        private void Awake()
        {
            m_ExplorerPoolItems = new MonoEntityPool<SkinExplorer>(ItemExplorePrefab);
            m_ActivatedExplorerItems = new List<SkinExplorer>();
        }

        public void Activate(Skin[] skins)
        {
            m_TargetItems = skins;

            DeactivateCurrentExploreItems();
            RedrawExplorerItems();

            Show();
        }

        public void Accept()
        {
            Hide();
        }

        protected void RedrawExplorerItems()
        {
            if (m_TargetItems == null) return;

            foreach (var skin in m_TargetItems)
            {
                var explorerItem = GetFreeExplorerItem();
                explorerItem.SetTarget(skin);
                explorerItem.Transform.SetAsLastSibling();

                m_ActivatedExplorerItems.Add(explorerItem);
            }
        }

        protected SkinExplorer GetFreeExplorerItem()
        {
            var item = m_ExplorerPoolItems.GetFree();
            item.transform.SetParent(ItemsContainerTransform, false);

            return item;
        }

        protected void DeactivateCurrentExploreItems()
        {
            foreach (var explorer in m_ActivatedExplorerItems)
            {
                explorer.Deactivate();
            }

            m_ActivatedExplorerItems.Clear();
        }
    }
}
