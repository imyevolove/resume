﻿using IncEngine.UI;
using System;
using UnityEngine;
using IncEngine.Activities;
using UnityEngine.UI;
using IncEngine;
using IncEngine.Collections;
using IncEngine.Collections.Context;
using IncEngine.Localization;

namespace Wac.UI.Popups
{
    [Serializable]
    public class InventoryItemExplorerPopup : PopupSingle
    {
        [Header("Properties")]
        [SerializeField] public Text        ItemTitleText;
        [SerializeField] public Text        ItemTitleAdditionalText;
        [SerializeField] public Text        ItemStatisticsText;
        [SerializeField] public Image       ItemIconImage;
        [SerializeField] public Image       ItemGradientImage;
        [SerializeField] public Activity    ButtonsContainer;

        [Header("Prefabs")]
        [SerializeField] public ActionButton ActionButtonPrefab;

        public InventoryItem Target;

        private MonoEntityPool<ActionButton> m_ActionButtonPool;
        
        protected virtual void Awake()
        {
            m_ActionButtonPool = new MonoEntityPool<ActionButton>(ActionButtonPrefab);
        }

        protected virtual void Start()
        {
            LocalizationManager.onChange += Redraw;
        }

        public void Explore(InventoryItem item)
        {
            Target = item;
            RedrawDetails();
            RedrawActionButtons();

            Show();
        }

        public void Redraw()
        {
            RedrawActionButtons();
            RedrawDetails();
        }

        private void RedrawActionButtons()
        {
            m_ActionButtonPool.DeactivateAllActiveEntities();

            foreach (var contextMenuItem in Target.ContextMenu.Items)
            {
                SetUpActionButton(GetFreeActionButton(), contextMenuItem);
            }
        }

        private ActionButton GetFreeActionButton()
        {
            ActionButton button = m_ActionButtonPool.GetFree(ButtonsContainer.Transform);
            button.transform.SetAsLastSibling();
            button.onClick.RemoveAllListeners();

            return button;
        }

        private void SetUpActionButton(ActionButton button, ContextMenuItemData data)
        {
            button.AdditionalText.text = GetLocalization("let_" + data.Name).ToUpper();
            button.ButtonText.text = data.Command.AdditionalInformation;
            button.ButtonImage.color = data.Color;
            button.AddtionalImage.sprite = data.Sprite;
            button.AddtionalImage.gameObject.SetActive(data.Sprite != null);

            button.onClick.AddListener(() => {
                if (data.Command.Execute())
                {
                    Hide();
                }
            });
        }

        private void RedrawDetails()
        {
            ItemTitleText.text = Target.FullName.ToUpper();
            ItemTitleAdditionalText.text = Target.LocalizedCategoryName.ToUpper();
            ItemStatisticsText.text = BuildStatsString(Target.BaseDamage, Target.BaseIncome);
            ItemIconImage.sprite = Target.Icon;
            ItemGradientImage.color = Target.Color;
        }

        private string BuildStatsString(ulong damage, ulong income)
        {
            return string.Format(
                "{2} <color=#FFFA44>{0}</color> " +
                "{3} <color=#FFFA44>{1}</color>",
                Game.NumberFormatter.Format(damage), Game.NumberFormatter.Format(income),
                GetLocalization("dpc"), GetLocalization("dps"));
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
