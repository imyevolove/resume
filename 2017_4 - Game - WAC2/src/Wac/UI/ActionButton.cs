﻿using IncEngine;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Wac.UI
{
    [Serializable]
    public class ActionButton : MonoBehaviour, IPointerClickHandler, IPoolable
    {
        [SerializeField] public Image ButtonImage;
        [SerializeField] public Image AddtionalImage;
        [SerializeField] public Text  ButtonText;
        [SerializeField] public Text  AdditionalText;

        public bool Active
        {
            get { return gameObject.activeSelf; }
            set
            {
                if (Active == value) return;

                gameObject.SetActive(value);

                if (Event_ActiveChange != null)
                    Event_ActiveChange(this, value);
            }
        }

        public ButtonClickedEvent onClick = new ButtonClickedEvent();
        public event Action<IPoolable, bool> Event_ActiveChange;
        public event Action<IPoolable> Event_Activate;
        public event Action<IPoolable> Event_Deactivate;
        
        public class ButtonClickedEvent : UnityEvent
        {
            public ButtonClickedEvent()
            {
            }
        }

        /// <summary>
        /// Activate poolable element
        /// </summary>
        public void Activate()
        {
            if (Active) return;
            Active = true;

            OnActivate();
        }

        /// <summary>
        /// Deactivate poolable element
        /// </summary>
        public void Deactivate()
        {
            if (!Active) return;
            Active = false;

            OnDeactivate();
        }

        public UnityAction ListenOneClick(UnityAction action)
        {
            UnityAction gaction = null;

            gaction = () => {
                action();
                onClick.RemoveListener(gaction);
            };

            onClick.AddListener(action);

            return gaction;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick();
        }

        protected virtual void OnClick()
        {
            onClick.Invoke();
        }

        protected virtual void OnActivate()
        {
            if (Event_Activate != null)
            {
                Event_Activate(this);
            }
        }

        protected virtual void OnDeactivate()
        {
            if (Event_Deactivate != null)
            {
                Event_Deactivate(this);
            }
        }
    }
}
