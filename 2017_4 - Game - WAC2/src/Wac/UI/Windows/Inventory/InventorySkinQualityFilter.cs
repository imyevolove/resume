﻿using IncEngine.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Collections;
using IncEngine.Localization;

namespace Wac.UI.Windows.Inventory
{
    [Serializable, RequireComponent(typeof(Dropdown))]
    public class InventorySkinQualityFilter : DropdownFilter
    {
        [SerializeField] public Image qualityIndicatorImage;
        [SerializeField] public Color qualityAllColor;

        public Dropdown Dropdown    { get; private set; }
        public Quality  Quality     { get; set; }
        
        public override List<InventoryItem> Filter(ICollection<InventoryItem> items)
        {
            if (Quality == null)
            {
                return items.ToList();
            }

            return items.Where(item => item.Quality == Quality).ToList();
        }

        public override void Filter(ref ICollection<InventoryItem> items)
        {
            if (Quality == null) return;

            var result = items.Where(item => item.Quality == Quality).ToList();
            
            items.Clear();

            foreach (var item in result)
            {
                items.Add(item);
            }
        }

        protected virtual void Awake()
        {
            SetDropdown(GetComponent<Dropdown>());
            SetUpDropdownList();
        }

        protected virtual void Start()
        { 
            LocalizationManager.onChange += SetUpDropdownList;
        }

        protected void SetDropdown(Dropdown dropdown)
        {
            RemoveDropdownEventListeners(Dropdown);

            Dropdown = dropdown;

            AddDropdownEventListeners(Dropdown);
        }

        protected void SetUpDropdownList()
        {
            Dropdown.ClearOptions();

            var options = Quality.Qualities
                .Select(quality => (Dropdown.OptionData)(new SuperDropdownOptionData(
                    GetLocalizationForQuality(quality), quality)))
                .ToList();
            options.Insert(0, new SuperDropdownOptionData(GetLocalization("no_filter"), null));

            Dropdown.AddOptions(options);
        }

        protected void SelectQuality(Quality quality)
        {
            Quality = quality;

            OnChange();
            CallChangeEvent();
        }

        protected virtual void OnDropdownValueChange(int value)
        {
            var option = Dropdown.options[value] as SuperDropdownOptionData;

            if (option == null) return;

            SelectQuality(option.Bind as Quality);
        }

        protected virtual void AddDropdownEventListeners(Dropdown dropdown)
        {
            if (dropdown == null) return;

            dropdown.onValueChanged.AddListener(OnDropdownValueChange);
        }

        protected virtual void RemoveDropdownEventListeners(Dropdown dropdown)
        {
            if (dropdown == null) return;

            dropdown.onValueChanged.RemoveListener(OnDropdownValueChange);
        }

        protected virtual void OnChange()
        {
            qualityIndicatorImage.color = Quality == null 
                ? qualityAllColor 
                : Quality.Color;
        }

        protected void CallChangeEvent()
        {
            if (Event_Change != null)
            {
                Event_Change();
            }
        }
        
        private string GetLocalizationForQuality(Quality quality)
        {
            return GetLocalization("skin_quality_" + quality.Name.Replace(" ", "_").ToLower());
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
