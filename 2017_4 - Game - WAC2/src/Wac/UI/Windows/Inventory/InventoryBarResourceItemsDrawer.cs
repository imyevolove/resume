﻿using IncEngine;
using IncEngine.Collections;
using IncEngine.Personal;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Inventory
{
    [Serializable]
    public class InventoryBarResourceItemsDrawer : Drawer<IInventory>
    {
        [SerializeField] public Text resourceCountText;

        public void Redraw()
        {
            resourceCountText.text = Game.NumberFormatterSpace.Format(Target.Items.Count);
        }

        protected override void Awake()
        {
            base.Awake();

            SetTarget(GameSession.Current.GetSingleton<Player>().Inventory);
        }

        protected override void OnRegisterTarget(IInventory target)
        {
            target.Event_Change += Redraw;
            Redraw();
        }

        protected override void OnUnregisterTarget(IInventory target)
        {
            target.Event_Change -= Redraw;
        }
    }
}
