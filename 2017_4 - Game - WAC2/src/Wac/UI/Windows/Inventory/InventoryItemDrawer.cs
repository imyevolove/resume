﻿using IncEngine.Collections;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Wac.UI.Popups;
using Wac.UI.Windows.Inventory.Components;

namespace Wac.UI.Windows.Inventory
{
    [Serializable]
    public class InventoryItemDrawer : UIElement<InventoryItem>, IPointerClickHandler
    {
        [SerializeField] public Image   gradientImage;
        [SerializeField] public Image   productIcon;
        [SerializeField] public Text    titleText;

        private List<IIComponent> m_IIComponents = new List<IIComponent>();

        protected override void Awake()
        {
            base.Awake();
            InitializeIIComponents();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            var popup = PopupManager.Get<InventoryItemExplorerPopup>(PopupId.InventoryItemExplorer);
            popup.Explore(Target);
        }

        public void Redraw()
        {
            if (Target == null) return;

            gradientImage.color     = Target.Color;
            productIcon.sprite      = Target.Icon;
            titleText.text          = Target.FullName.Replace('|', '\n');

            ValidateIIComponents();
        }

        protected override void OnRegisterTarget(InventoryItem target)
        {
            Redraw();
        }

        protected override void OnUnregisterTarget(InventoryItem target)
        {
            
        }

        private void ValidateIIComponents()
        {
            foreach (var component in m_IIComponents)
            {
                component.Validate(Target);
                component.Redraw();
            }
        }

        private void InitializeIIComponents()
        {
            m_IIComponents.AddRange(GetComponentsInChildren<IIComponent>());
        }
    }
}
