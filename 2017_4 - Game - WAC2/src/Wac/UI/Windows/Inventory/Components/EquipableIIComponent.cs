﻿using System;
using IncEngine.Collections;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Collections.Context.Commands;
using IncEngine;
using IncEngine.Personal;

namespace Wac.UI.Windows.Inventory.Components
{
    [Serializable]
    public class EquipableIIComponent : IIComponent
    {
        [SerializeField] public Color ActiveColor;
        [SerializeField] public Color InactiveColor;
        [SerializeField] private Image Indicator;

        public bool IsValidated { get; protected set; }
        private bool Equipped;

        public override bool Validate(InventoryItem item)
        {
            IsValidated = item.ContextMenu.HasCommand<EquipCommand>();

            Equipped = !IsValidated ? false : ValidateEquipmentStatus(item);
            
            return IsValidated;
        }

        public override void Redraw()
        {
            gameObject.SetActive(IsValidated);
            Indicator.color = Equipped ? ActiveColor : InactiveColor;
        }

        protected bool ValidateEquipmentStatus(InventoryItem item)
        {
            if (item == null) return false;

            var entity = GameSession.Current.GetSingleton<Player>().Equipment
                    .FindEntity(item.Target);
            
            if (entity == null)
            {
                return false;
            }

            return item.Equals(entity.AttachedInventoryItem);
        }
    }
}
