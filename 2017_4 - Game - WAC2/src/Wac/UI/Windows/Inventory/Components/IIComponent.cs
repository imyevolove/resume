﻿using IncEngine.Collections;
using UnityEngine;

namespace Wac.UI.Windows.Inventory.Components
{
    public abstract class IIComponent : MonoBehaviour
    {
        public abstract bool Validate(InventoryItem item);
        public abstract void Redraw();
    }
}
