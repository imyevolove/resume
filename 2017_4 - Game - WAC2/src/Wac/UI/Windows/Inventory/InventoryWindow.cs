﻿using IncEngine;
using IncEngine.Activities;
using IncEngine.Collections;
using IncEngine.Personal;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using Wac.UI.Popups;

namespace Wac.UI.Windows.Inventory
{
    [Serializable]
    public class InventoryWindow : Window
    {
        [SerializeField] public InventoryPageLayout pageLayoutGroup;
        [SerializeField] public DropdownFiltersMaster filterMaster;
        [SerializeField] public InventoryItemDrawer itemDrawerPrefab;
        [SerializeField] public Activity content;
        
        private List<InventoryItem> m_InventoryItems;

        public IInventory Inventory { get; protected set; }
        
        protected virtual void Awake()
        {
            m_InventoryItems = new List<InventoryItem>();

            filterMaster.Event_Change += Redraw;

            SetInventory(GameSession.Current.GetSingleton<Player>().Inventory);

            PopupManager.Get<InventoryItemExplorerPopup>(PopupId.InventoryItemExplorer).Event_Hide += Redraw;
        }

        public void SetInventory(IInventory inventory)
        {
            Inventory = inventory;
        }

        public void Redraw()
        {
            FilterInventoryItems();
            DrawInventory();
        }
        
        protected override void OnShow()
        {
            base.OnShow();

            Redraw();
        }

        protected void FilterInventoryItems()
        {
            m_InventoryItems.Clear();
            m_InventoryItems.AddRange(filterMaster.Filter(Inventory.Items));
        }

        protected void DrawInventory()
        {
            var lastIndex = pageLayoutGroup.CurrentPageIndex;

            pageLayoutGroup.Clear();
            pageLayoutGroup.AddItems(m_InventoryItems);

            pageLayoutGroup.Select(lastIndex);
        }
    }
}