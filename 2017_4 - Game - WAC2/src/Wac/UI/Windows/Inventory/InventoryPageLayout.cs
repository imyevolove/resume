﻿using IncEngine.Collections;
using System;

namespace Wac.UI.Windows.Inventory
{
    [Serializable]
    public class InventoryPageLayout : PageLayoutGrid<InventoryItem, InventoryItemDrawer>
    {
        protected override void Swipe(SwipeDirection direction)
        {
            switch (direction)
            {
                case SwipeDirection.Left:
                    Back();
                    break;

                case SwipeDirection.Right:
                    Next();
                    break;
            }
        }
    }
}
