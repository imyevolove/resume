﻿using System;
using System.Collections.Generic;
using UnityEngine;
using IncEngine.Activities;
using IncEngine.Science;
using IncEngine.UI;

namespace Wac.UI.Windows.Science
{
    [Serializable]
    public class ScienceDrawer : Drawer<TechMapper>
    {
        [SerializeField]
        public TechDrawer techDrawerPrefab;

        [SerializeField]
        public Activity content;

        private List<TechDrawer> m_TechDrawers = new List<TechDrawer>();

        /// <summary>
        /// Return tech exists status
        /// </summary>
        /// <param name="tech"></param>
        /// <returns></returns>
        public bool TechObserverExists(Tech tech)
        {
            return m_TechDrawers.Exists(drawer => drawer.Target == tech);
        }

        /// <summary>
        /// Return tech drawer by tech
        /// </summary>
        /// <param name="tech"></param>
        /// <returns></returns>
        public TechDrawer GetTechDrawer(Tech tech)
        {
            return m_TechDrawers.Find(drawer => drawer.Target == tech);
        }

        /// <summary>
        /// Add observer for tech
        /// </summary>
        /// <param name="tech"></param>
        /// <returns></returns>
        public bool AddObserveForTech(Tech tech)
        {
            if (TechObserverExists(tech)) return false;

            var techDrawer = Instantiate(techDrawerPrefab, content.Transform, false);
            techDrawer.SetTarget(tech);

            m_TechDrawers.Add(techDrawer);
            return true;
        }

        public bool RemoveObserveForTech(Tech tech)
        {
            var drawer = GetTechDrawer(tech);
            
            if (drawer == null) return false;

            var removed = m_TechDrawers.Remove(drawer);

            if (removed) drawer.Destroy();

            return removed;
        }

        protected override void OnRegisterTarget(TechMapper target)
        {
            target.Event_AddTech    += OnAnyTechAdd;
            target.Event_RemoveTech += OnAnyTechRemove;

            foreach (var tech in target.Technologies)
                AddObserveForTech(tech);
        }

        protected override void OnUnregisterTarget(TechMapper target)
        {
            target.Event_AddTech    -= OnAnyTechAdd;
            target.Event_RemoveTech -= OnAnyTechRemove;

            RemoveAllObserves();
        }

        private void OnAnyTechAdd(Tech tech)
        {
            AddObserveForTech(tech);
        }

        private void OnAnyTechRemove(Tech tech)
        {
            RemoveObserveForTech(tech);
        }

        private void RemoveAllObserves()
        {
            foreach (var drawer in m_TechDrawers)
                drawer.Destroy();
            m_TechDrawers.Clear();
        }
    }
}
