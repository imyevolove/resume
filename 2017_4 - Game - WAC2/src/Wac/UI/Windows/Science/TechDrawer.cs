﻿using System;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Economy;
using IncEngine.Science;
using IncEngine.UI;
using IncEngine.Personal;
using IncEngine;
using IncEngine.Localization;
using IncEngine.Abilities;

namespace Wac.UI.Windows.Science
{
    [Serializable, DisallowMultipleComponent]
    public class TechDrawer : Drawer<Tech>
    {
        [Header("Elements")]
        [SerializeField]
        public Text TitleText;
        [SerializeField]
        public Image IconImage;
        [SerializeField]
        public Slider LevelIndicator;
        [SerializeField]
        public Image AbilityIconImage;
        [SerializeField]
        public Text AbilityTitleText;
        [SerializeField]
        public Text AbilityValueText;
        [SerializeField]
        public PurchaseButton PurchaseButton;

        public Player Player { get; protected set; }

        protected override void Awake()
        {
            base.Awake();

            Player = GameSession.Current.GetSingleton<Player>();

            PurchaseButton.Event_Click += PurchaseAction;
            Game.Player.Abilities.Event_AnyAbilityChange += (abl) =>
            {
                Redraw();
            };
        }

        protected virtual void Start()
        {
            LocalizationManager.onChange += Redraw;
        }

        public void Redraw()
        {
            if (!HasTarget) return;

            TitleText.text = GetLocalizationNameForTech(Target);
            IconImage.sprite = Target.Icon;
            
            LevelIndicator.maxValue = Target.MaxLevel;
            LevelIndicator.value = Target.Level;

            AbilityIconImage.sprite = Target.Ability.Icon;
            AbilityTitleText.text = GetLocalizationNameForAbility(Target.Ability);
            AbilityValueText.text = GetAbilityStatsText();
            
            var isMaximized = Target.IsMaximized;

            PurchaseButton.ResourceIconImage.gameObject.SetActive(!isMaximized);
            PurchaseButton.ResourceCountText.gameObject.SetActive(!isMaximized);

            PurchaseButton.ResourceIconImage.sprite = EconomicResources.ResourceMedal.Icon;
            PurchaseButton.ResourceCountText.text   = Target.GetPrice().ToString();
            PurchaseButton.TitleText.text           = GetLocalization(isMaximized ? "let_maximum" : "let_improve");
        }

        protected virtual void PurchaseAction()
        {
            if (!Target.CanImprove())
            {
                Debug.Log("Can't improve tech");
                return;
            }

            if (!Player.PurchaseForMedals(Target.GetPrice()))
            {
                Debug.Log("Not enough money");
                return;
            }
            
            var status = Target.Improve();
            Debug.Log(status ? "Improved" : "Improve failed");
        }

        protected override void OnRegisterTarget(Tech target)
        {
            target.Event_Improve += OnTechImprove;
            
            Redraw();
        }

        protected override void OnUnregisterTarget(Tech target)
        {
            target.Event_Improve -= OnTechImprove;
        }

        protected virtual void OnTechImprove(Tech tech, uint count)
        {
            Redraw();
        }

        private string GetAbilityStatsText()
        {
            var valueTypeChar = Target.Ability.valueType == IncEngine.Abilities.AbilityModValueType.Percentage ? "%" : "";
            return Target.IsMaximized
                ? Target.Ability.Value.ToString() + valueTypeChar
                : string.Format("{0}{2} > {1}{2}", Target.Ability.Value, Target.GetNextAbilityValue(), valueTypeChar);
        }

        private string GetLocalizationNameForTech(Tech tech)
        {
            return GetLocalization(string.Format("tech_{0}_name", tech.Ability.Name)).ToUpper();
        }

        private string GetLocalizationNameForAbility(Ability ability)
        {
            return GetLocalization(string.Format("ability_{0}_name", ability.Name)).ToUpper();
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}