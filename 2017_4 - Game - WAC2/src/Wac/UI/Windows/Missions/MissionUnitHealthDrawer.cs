﻿using IncEngine.Play;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Missions
{
    [Serializable]
    public class MissionUnitHealthDrawer : Drawer<Unit>
    {
        [SerializeField] public Text healthText;

        protected void Redraw(IUnit unit)
        {
            healthText.text = Game.NumberFormatterSpace.Format(unit.MaxHealth) + " / " + Game.NumberFormatterSpace.Format(unit.Health);
        }

        protected override void OnRegisterTarget(Unit target)
        {
            target.Event_HealthChange += Redraw;
        }

        protected override void OnUnregisterTarget(Unit target)
        {
            target.Event_HealthChange -= Redraw;
        }
    }
}
