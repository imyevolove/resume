﻿using System;
using System.Linq;
using IncEngine.Play;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Activities;
using IncEngine.Localization;

namespace Wac.UI.Windows.Missions
{
    [Serializable]
    public class MissionTaskMobsDrawer : MissionTaskDrawer
    {
        [SerializeField] public Text progressIndicatorText;
        [SerializeField] public MissionUnitIndicatorElement indicatorElementPrefab;
        [SerializeField] public Activity indicatorsContainer;

        private MissionUnitIndicatorManager indicatorManager;

        protected override void Awake()
        {
            base.Awake();

            indicatorManager = new MissionUnitIndicatorManager(
                indicatorElementPrefab, indicatorsContainer.Transform);
        }

        protected override void RedrawCore()
        {
            UpdateUnitIndicators();
            RedrawHealth();
            RedrawUnitGraphics();
        }
        
        protected virtual void RedrawHealth()
        {
            if (!DrawerEnabled) return;

            progressIndicatorText.text = string.Format("{0} {1} / {2}",
                LocalizationManager.GetLocalization("hp").ToUpper(),
                Game.NumberFormatter.Format(Target.Unit.Health),
                Game.NumberFormatter.Format(Target.Unit.MaxHealth)
                );
        }

        protected override void OnRegisterTarget(Mission target)
        {
            target.Stage.Event_LevelChange  += OnStageLevelChange;
            target.Event_WaveChange         += OnWaveChange;
            target.Unit .Event_HealthChange += OnUnitHealthChange;
            target.Stage.Event_UnitChange   += OnUnitChange;
        }

        protected override void OnUnregisterTarget(Mission target)
        {
            target.Stage.Event_LevelChange  -= OnStageLevelChange;
            target.Event_WaveChange         -= OnWaveChange;
            target.Unit .Event_HealthChange -= OnUnitHealthChange;
            target.Stage.Event_UnitChange   -= OnUnitChange;
        }

        protected void UpdateUnitIndicators()
        {
            if (!DrawerEnabled) return;

            indicatorManager.Configure(Target.Stage.GetWave().GetUnits().ToArray());
        }

        protected void RedrawUnitGraphics()
        {
            if (!DrawerEnabled) return;

            image.sprite = Target.Unit.Details.Image;
        }

        protected override void OnEnableDrawer()
        {
            Redraw();
        }

        protected virtual void OnStageLevelChange(ulong level)
        {
            UpdateUnitIndicators();
        }

        protected virtual void OnWaveChange()
        {
            RedrawUnitGraphics();
        }

        protected virtual void OnUnitChange(IUnit unit)
        {
            RedrawHealth();
        }

        protected virtual void OnUnitHealthChange(IUnit unit)
        {
            RedrawHealth();
        }
    }
}
