﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wac.Play;

namespace Wac.UI.Windows.Missions
{
    public class MissionTaskDrawerManager
    {
        private Dictionary<TaskSign, MissionTaskDrawer> m_Drawers = new Dictionary<TaskSign, MissionTaskDrawer>();
        
        public TaskSign                         ActiveSign          { get { return m_ActiveDrawer.Key; } }
        public MissionTaskDrawer                ActiveDrawer        { get { return m_ActiveDrawer.Value; } }

        public ICollection<TaskSign>            RegisteredSigns     { get { return m_Drawers.Keys; } }
        public ICollection<MissionTaskDrawer>   RegisteredDrawers   { get { return m_Drawers.Values; } }

        private KeyValuePair<TaskSign, MissionTaskDrawer> m_ActiveDrawer;
        
        public bool ActivateDrawer(TaskSign sign)
        {
            if (!Contains(sign)) return false;

            ActivateDrawerCore(m_Drawers[sign]);
            return true;
        }

        public bool Contains(TaskSign sign)
        {
            return m_Drawers.ContainsKey(sign);
        }

        public bool Contains(MissionTaskDrawer drawer)
        {
            return m_Drawers.ContainsValue(drawer);
        }

        public bool AddDrawer(TaskSign sign, MissionTaskDrawer drawer, bool updateIfExists = false)
        {
            if (drawer == null) return false;

            if (Contains(sign))
            {
                if (updateIfExists)
                {
                    m_Drawers[sign] = drawer;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                m_Drawers.Add(sign, drawer);
                drawer.DisableDrawer();

                /// Activate drawer id it is first drawer
                if (m_Drawers.Count == 1)
                {
                    ActivateDrawer(sign);
                }

                return true;
            }
        }

        public bool RemoveDrawer(TaskSign sign)
        {
            var removed = m_Drawers.Remove(sign);

            if (removed && ActiveSign == sign)
            {
                ActivateFirstDrawer();
            }

            return removed;
        }

        protected void ActivateFirstDrawer()
        {
            if (m_Drawers.Count == 0) return;

            ActivateDrawerCore(m_Drawers.ElementAt(0).Value);
        }

        protected virtual void ActivateDrawerCore(MissionTaskDrawer drawer)
        {
            if (drawer == null)
            {
                Debug.LogWarning("can't activate drawer. Drawer is null");
                return;
            }

            foreach (var kvp in m_Drawers)
            {
                if (kvp.Value == drawer)
                {
                    m_ActiveDrawer = kvp;
                    kvp.Value.EnableDrawer();
                }
                else
                {
                    kvp.Value.DisableDrawer();
                }
            }
        }
    }
}
