﻿using UnityEngine;
using IncEngine.UI;
using System;
using UnityEngine.UI;
using Wac.Play;
using IncEngine;
using IncEngine.Structures;
using UnityEngine.EventSystems;
using IncEngine.Personal;
using IncEngine.Play;
using Wac.Data;
using IncEngine.Activities;
using IncEngine.Localization;

namespace Wac.UI.Windows.Missions
{
    [Serializable]
    public class MissionWindow : Window
    {
        [SerializeField] public Image   mapIconImage;
        [SerializeField] public Image   mapCoverImage;
        [SerializeField] public Text    mapNameText;
        [SerializeField] public Text    levelText;
        [SerializeField] public Text    briefText;
        [SerializeField] public Text    missionDetailsTitleText;
        [SerializeField] public Text    missionDetailsDescriptionText;
        [SerializeField] public Slider  progressSlider;
        [SerializeField] public Button  retreatButton;
        [SerializeField] public MissionClickablePanel clickablePanel;
        [SerializeField] public Activity drawersContainer;

        [Header("Visualization")]
        [SerializeField] public RectTransform ParticlesContainer;
        [SerializeField] public FloatInteractiveResource FIRCoinRewardPrefab;

        [SerializeField] public MissionDrawerBind[] missionDrawerBinds;

        public WacMission   Mission { get; private set; }
        public Player       Player  { get; private set; }

        private MissionTaskDrawerManager m_DrawerManager = new MissionTaskDrawerManager();
        private UIParticleSystem m_RewardParticleSystem;

        protected void Awake()
        {
            m_RewardParticleSystem = new UIParticleSystem(FIRCoinRewardPrefab, ParticlesContainer);

            Mission = Game.Mission;
            Mission.Event_MapChange                 += OnMapChange;
            Mission.Stage.Event_LevelChange         += OnStageLevelChange;
            Mission.Stage.Event_TaskModeChangeTick  += OnTaskModeTick;
            Mission.Unit.Event_HealthChange         += OnUnitHealthChange;
            Mission.Event_UnitChange                += OnUnitChange;
            Mission.Event_GiveReward                += OnGiveReward;

            Player = GameSession.Current.GetSingleton<Player>();
            
            clickablePanel.Event_Click += OnClick;
            retreatButton.onClick.AddListener(Mission.LevelFallback);

            ///missionDrawer.SetTarget(Mission);
            RedrawAll();
            InitializeDrawers();
        }

        protected void RedrawAll()
        {
            RedrawMap(Mission.Map);
            RedrawLevel(Mission.Stage.Level);
            RedrawUnitHealth(Mission.Unit);
            RedrawMode();
            RedrawRetreatButton();
        }

        protected void RedrawMode()
        {
            missionDetailsTitleText.text = GetLocalization("mission_mode_" + Mission.Stage.CurrentTask.Mode.Sign.Name.ToLower().Replace(" ", ""));
            RedrawModeDesc();
        }

        protected void RedrawModeDesc()
        {
            missionDetailsDescriptionText.text = Mission.Stage.CurrentTask.Mode.GetDetailsString();
        }

        protected void RedrawLevel(ulong level)
        {
            levelText.text = (Mission.Stage.Level + 1).ToString();
            briefText.text = GetLocalization("mission_task_" + Mission.Stage.CurrentTask.Name);
        }

        protected void RedrawUnitHealth(IUnit unit)
        {
            progressSlider.minValue = 0;
            progressSlider.maxValue = unit.MaxHealth;
            progressSlider.value = unit.MaxHealth - unit.Health;
        }

        protected void RedrawMap(Map map)
        {
            mapCoverImage.sprite = map.Image;
            mapIconImage .sprite = map.Icon;
            mapNameText  .text   = map.Name.ToUpper();
        }

        protected void RedrawRetreatButton()
        {
            retreatButton.gameObject.SetActive(Mission.Stage.CurrentTask.Mode.Sign.Equals(ModeSignatures.Timer));
        }

        protected void RedrawMissionDrawer()
        {
            foreach (var sign in m_DrawerManager.RegisteredSigns)
            {
                if (Mission.Stage.CurrentTask.Compare(sign))
                {
                    m_DrawerManager.ActivateDrawer(sign);
                    return;
                }
            }
        }

        protected override void OnShow()
        {
            base.OnShow();
            RedrawAll();
            m_DrawerManager.ActiveDrawer.Redraw();
            Mission.Stage.Continue();
            Mission.EnablePlayerDamagePerSecond();
        }

        protected override void OnHide()
        {
            base.OnHide();
            Mission.Stage.Pause();
            Mission.DisablePlayerDamagePerSecond();
        }

        protected virtual void OnMapChange(Map map)
        {
            RedrawMap(map);
            RedrawMissionDrawer();
        }

        protected virtual void OnStageLevelChange(ulong level)
        {
            RedrawRetreatButton();
            RedrawLevel(level);
            RedrawMode();
        }

        protected virtual void OnUnitHealthChange(IUnit unit)
        {
            RedrawUnitHealth(unit);
        }

        protected virtual void OnTaskModeTick()
        {
            RedrawModeDesc();
        }

        protected virtual void OnUnitChange(IUnit unit)
        {
            RedrawUnitHealth(unit);
        }

        protected virtual void OnGiveReward(ulong reward)
        {
            var particle = m_RewardParticleSystem.Emit() as FloatInteractiveResource;
            particle.SetDetails(IconsManager.CoinIcon, Game.NumberFormatter.Format(reward));

            var hw  = m_RewardParticleSystem.Container.rect.width / 2 - particle.RectTransform.rect.width / 2;
            var hh = m_RewardParticleSystem.Container.rect.height / 2 - particle.RectTransform.rect.height / 2;

            particle.RectTransform.anchoredPosition = new Vector2(
                UnityEngine.Random.Range(-hw, hw),
                UnityEngine.Random.Range(-hh, hh)
                );
        }

        protected virtual void OnClick(PointerEventData eventData)
        {
            if (Mission.DamageUnit(Player.GetDamage()))
            {
                m_DrawerManager.ActiveDrawer.PlayHitSound();
            }
        }

        private void InitializeDrawers()
        {
            foreach (var bind in missionDrawerBinds)
            {
                var drawer = Instantiate(bind.drawerPrefab, drawersContainer.Transform, false);
                drawer.SetTarget(Mission);

                m_DrawerManager.AddDrawer(bind.sign, drawer, true);
            }
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
