﻿using IncEngine.Play;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Missions
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class MissionUnitIndicatorElement : UIElement<IUnit>
    {
        [SerializeField] public Image image;

        private Animator m_Animator;

        protected override void Awake()
        {
            base.Awake();

            m_Animator = GetComponent<Animator>();
        }

        protected void Redraw()
        {
            if (Target == null)
            {
                return;
            }
            
            image.sprite = Target.Details.Icon;

            m_Animator.SetBool("Active", !Target.Destroyed);
        }

        protected override void OnDeactivate()
        {
            base.OnDeactivate();
            
        }

        protected override void OnRegisterTarget(IUnit target)
        {
            AddEventListeners(Target);
            Redraw();
        }

        protected override void OnUnregisterTarget(IUnit target)
        {
            RemoveEventListeners(Target);
        }

        protected void AddEventListeners(IUnit target)
        {
            if (target == null) return;

            RemoveEventListeners(target);

            target.Event_Destroy += OnUnitDestroy;
        }

        protected void RemoveEventListeners(IUnit target)
        {
            if (target == null) return;
            
            target.Event_Destroy -= OnUnitDestroy;
        }

        protected void OnUnitDestroy(IUnit unit)
        {
            Redraw();
        }
    }
}
