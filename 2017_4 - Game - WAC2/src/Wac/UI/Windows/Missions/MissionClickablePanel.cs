﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Wac.UI.Windows.Missions
{
    [Serializable, RequireComponent(typeof(Image))]
    public class MissionClickablePanel : MonoBehaviour, IPointerClickHandler
    {
        public event Action<PointerEventData> Event_Click;

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick(eventData);
        }

        protected virtual void OnClick(PointerEventData eventData)
        {
            if (Event_Click != null)
            {
                Event_Click(eventData);
            }
        }
    }
}
