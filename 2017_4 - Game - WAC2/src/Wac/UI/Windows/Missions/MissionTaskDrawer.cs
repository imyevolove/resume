﻿using IncEngine;
using IncEngine.Play;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Missions
{
    [Serializable]
    public abstract class MissionTaskDrawer : Drawer<Mission>
    {
        [SerializeField] public Image   image;
        [SerializeField] public AudioPlayer HitAudioPlayer;

        public bool DrawerEnabled { get { return isActiveAndEnabled; } }

        public void PlayHitSound()
        {
            if (HitAudioPlayer == null) return;
            HitAudioPlayer.Play();
        }

        public void EnableDrawer()
        {
            gameObject.SetActive(true);
            OnEnableDrawer();
        }

        public void DisableDrawer()
        {
            gameObject.SetActive(false);
            OnDisableDrawer();
        }

        public void Redraw()
        {
            if (DrawerEnabled)
            {
                RedrawCore();
            }
        }

        protected abstract void RedrawCore();

        protected virtual void OnEnableDrawer()
        {

        }

        protected virtual void OnDisableDrawer()
        {

        }
    }
}
