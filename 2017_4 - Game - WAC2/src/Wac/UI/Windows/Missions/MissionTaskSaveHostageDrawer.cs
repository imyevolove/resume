﻿using System;
using IncEngine.Play;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Missions
{
    [Serializable]
    public class MissionTaskSaveHostageDrawer : MissionTaskDrawer
    {
        [SerializeField] public Slider slider;

        protected override void RedrawCore()
        {
            RedrawIcon();
            RedrawRedrawSlider();
        }

        protected override void OnRegisterTarget(Mission target)
        {
        }

        protected override void OnUnregisterTarget(Mission target)
        { 
        }

        protected override void OnEnableDrawer()
        {
            base.OnEnableDrawer();

            RegisterEventListeners(Target);
            Redraw();
        }

        protected override void OnDisableDrawer()
        {
            base.OnDisableDrawer();

            UnregisterEventListeners(Target);
        }

        protected void RedrawRedrawSlider()
        {
            slider.minValue = 0;
            slider.maxValue = Target.Unit.MaxHealth;
            slider.value = Target.Unit.Health;
        }

        protected void RedrawIcon()
        {
            if (!DrawerEnabled) return;

            image.sprite = Target.Unit.Details.Image;
        }

        protected virtual void OnUnitHealthChange(IUnit unit)
        {
            RedrawRedrawSlider();
        }

        protected void RegisterEventListeners(Mission target)
        {
            if (target == null) return;

            UnregisterEventListeners(target);

            target.Unit.Event_HealthChange += OnUnitHealthChange;
        }

        protected void UnregisterEventListeners(Mission target)
        {
            if (target == null) return;

            target.Unit.Event_HealthChange -= OnUnitHealthChange;
        }
    }
}
