﻿using System;
using IncEngine.Play;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Missions
{
    [Serializable]
    public class MissionTaskDefuseBombDrawer : MissionTaskDrawer
    {
        [SerializeField] public Text bombCodeText;
        [SerializeField] public string bombCodePattern;

        protected override void RedrawCore()
        {
            RedrawIcon();
            RedrawBombCode();
        }

        protected override void OnRegisterTarget(Mission target)
        {
        }

        protected override void OnUnregisterTarget(Mission target)
        { 
        }

        protected override void OnEnableDrawer()
        {
            base.OnEnableDrawer();

            RegisterEventListeners(Target);
            Redraw();
        }

        protected override void OnDisableDrawer()
        {
            base.OnDisableDrawer();

            UnregisterEventListeners(Target);
        }

        protected void RedrawBombCode()
        {
            var progress = 1f - (float)Target.Unit.Health / Target.Unit.MaxHealth;
           
            var code = bombCodePattern.Substring(0, (int)(bombCodePattern.Length * progress));
            code += new String('*', bombCodePattern.Length - code.Length);
            code = FormatBombCode(code);
            
            bombCodeText.text = code;
        }

        protected string FormatBombCode(string code)
        {
            var result = "";

            foreach(var ch in code)
            {
                result += ch + " ";
            }

            result.TrimEnd(' ');
            return result;
        }

        protected void RedrawIcon()
        {
            if (!DrawerEnabled) return;

            image.sprite = Target.Unit.Details.Image;
        }

        protected virtual void OnUnitHealthChange(IUnit unit)
        {
            RedrawBombCode();
        }

        protected void RegisterEventListeners(Mission target)
        {
            if (target == null) return;

            UnregisterEventListeners(target);

            target.Unit.Event_HealthChange += OnUnitHealthChange;
        }

        protected void UnregisterEventListeners(Mission target)
        {
            if (target == null) return;

            target.Unit.Event_HealthChange -= OnUnitHealthChange;
        }
    }
}
