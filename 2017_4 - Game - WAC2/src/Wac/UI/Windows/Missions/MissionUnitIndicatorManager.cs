﻿using IncEngine;
using IncEngine.Play;
using System.Collections.Generic;
using UnityEngine;

namespace Wac.UI.Windows.Missions
{
    public class MissionUnitIndicatorManager
    {
        public Transform Parent { get; protected set; }
        private MonoEntityPool<MissionUnitIndicatorElement> m_Pool;

        public MissionUnitIndicatorManager(MissionUnitIndicatorElement originalPrefab, Transform parent)
        {
            m_Pool = new MonoEntityPool<MissionUnitIndicatorElement>(originalPrefab);
            Parent = parent;
        }

        public void Configure(IEnumerable<IUnit> units)
        {
            DestroyIndicators();
            CreateIndicators(units);
        }

        protected void CreateIndicators(IEnumerable<IUnit> units)
        {
            foreach (var unit in units)
            {
                CreateIndicator(unit);
            }
        }

        protected void CreateIndicator(IUnit unit)
        {
            if (unit == null) return;

            var drawer = m_Pool.GetFree(Parent);
            drawer.RectTransform.SetAsLastSibling();
            drawer.SetTarget(unit);
        }

        protected void DestroyIndicators()
        {
            m_Pool.DeactivateAllActiveEntities();
        }
    }
}
