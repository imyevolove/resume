﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Wac.IAP;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class Carousel : MonoBehaviour
    {
        [SerializeField] public RectTransform SliderContainer;
        [SerializeField] public IAPSpecialSlide IAPSliderPrefab;

        private List<IAPSpecialSlide> m_Slides = new List<IAPSpecialSlide>();

        public bool AddSlide(IAPSpecial details)
        {
            var slide = m_Slides.Find(sld => sld.Target == details);
            if (slide != null) return false;

            m_Slides.Add(CreateSlide(details));

            return true;
        }

        public bool RemoveSlide(IAPSpecial details)
        {
            var slide = m_Slides.Find(sld => sld.Target == details);
            if (slide == null) return false;

            var removed = m_Slides.Remove(slide);

            if (removed)
            {
                Destroy(slide);
            }

            return removed;
        }

        public void RemoveSlide(int index)
        {
            m_Slides.RemoveAt(index);
        }

        private IAPSpecialSlide CreateSlide(IAPSpecial details)
        {
            var slide = Instantiate(IAPSliderPrefab);
            slide.RectTransform.SetParent(SliderContainer, false);
            slide.SetTarget(details);

            return slide;
        }
    }
}