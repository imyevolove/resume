﻿using IncEngine.Localization;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;
using Wac.IAP;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class IAPSpecialSlide : Drawer<IAPSpecial>
    {
        [SerializeField] public Image BackgroundImage;
        [SerializeField] public Image IconImage;
        [SerializeField] public Text  TitleText;
        [SerializeField] public Text  DescriptionText;
        [SerializeField] public SimpleButton PurchaseButton;
        [SerializeField] public RectTransform RewardContainer;
        [SerializeField] public IAPRewardItemDrawer RewardItemPrefab;

        protected virtual void Start()
        {
            LocalizationManager.onChange += Redraw;
        }

        protected void Redraw()
        {
            if (Target == null) return;
            
        }

        protected override void OnRegisterTarget(IAPSpecial target)
        {
            Redraw();
        }

        protected override void OnUnregisterTarget(IAPSpecial target)
        {
            
        }

        private string GetTextForReward(IAPReward reward)
        {
            return string.Format("<color=#F0CD33>{0}</color> {1}", reward.Count, GetLocalization(reward.Name));
        }

        private string GetTextForPurchaseButton()
        {
            return string.Format("{0} {1}", GetLocalization("let_buy"), 0);
        }

        private string GetLocalizationForPhrase(string phrase)
        {
            return GetLocalization(phrase.Replace(" ", "_")).ToUpper();
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
