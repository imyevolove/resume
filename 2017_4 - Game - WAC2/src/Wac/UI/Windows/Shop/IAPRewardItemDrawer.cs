﻿using IncEngine.Localization;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;
using Wac.IAP;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class IAPRewardItemDrawer : Drawer<IAPReward>
    {
        [SerializeField] public Image IconImage;
        [SerializeField] public Text  NameText;
        [SerializeField] public Text  CountText;

        protected virtual void Start()
        {
            LocalizationManager.onChange += Redraw;
        }

        protected void Redraw()
        {
            if (Target == null) return;

            NameText.text = GetLocalizationForPhrase("let_" + Target.Name);
            CountText.text = Target.Count.ToString();
            IconImage.sprite = Target.Icon;
        }

        protected override void OnRegisterTarget(IAPReward target)
        {
            Redraw();
        }

        protected override void OnUnregisterTarget(IAPReward target)
        {
            
        }

        private string GetLocalizationForPhrase(string phrase)
        {
            return GetLocalization(phrase.Replace(" ", "_")).ToUpper();
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
