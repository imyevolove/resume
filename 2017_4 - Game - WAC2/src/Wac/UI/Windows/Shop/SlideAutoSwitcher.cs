﻿using UnityEngine;

namespace Wac.UI.Windows.Shop
{
    [RequireComponent(typeof(Carousel))]
    public class SlideAutoSwitcher : MonoBehaviour
    {
        [HideInInspector] private Carousel m_Carousel;
        public Carousel Carousel
        {
            get
            {
                if (m_Carousel == null)
                {
                    m_Carousel = GetComponent<Carousel>();
                }

                return m_Carousel;
            }
        }
    }
}
