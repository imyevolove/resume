﻿using UnityEngine;

namespace Wac.UI.Windows.Shop
{
    public class AdBlockMessageHandler : MonoBehaviour
    {
        protected virtual void Awake()
        {
            Game.Profile.AdsState.Event_OnValueChange += OnAdsStateChange;
            OnAdsStateChange(Game.Profile.AdsState.Value);
        }

        private void OnAdsStateChange(bool state)
        {
            gameObject.SetActive(state);
        }
    }
}
