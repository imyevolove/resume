﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wac.IAP;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class ShopPanelDonate : MonoBehaviour
    {
        [SerializeField] public Carousel SpecialCarousel;
        [SerializeField] public IAPItemDrawer IAPItemPrefab;
        [SerializeField] public RectTransform IAPItemContainer;

        protected virtual void Start()
        {
            SetupSpecialIAPItems();
            SetupIAPItems();
        }

        private void SetupSpecialIAPItems()
        {
            //var items = GetIAPItems<IAPSpecial>();
        }

        private void SetupIAPItems()
        {
            var items = GetIAPItems<IAPSingle>();
            
            foreach (var item in items)
            {
                CreateIAPItem(item);
            }
        }

        private IEnumerable<T> GetIAPItems<T>() where T : IAPDetails
        {
            return Game.IAPCollection.Items.Select(item => item as T);
        }

        private void CreateIAPSpecialItem(IAPSpecial item)
        {
            SpecialCarousel.AddSlide(item);
        }

        private void CreateIAPItem(IAPSingle item)
        {
            var uiElement = Instantiate(IAPItemPrefab);
            uiElement.RectTransform.SetParent(IAPItemContainer, false);
            uiElement.SetTarget(item);
        }
    }
}
