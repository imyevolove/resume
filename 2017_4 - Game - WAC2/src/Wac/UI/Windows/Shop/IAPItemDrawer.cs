﻿using IncEngine.IAPManagement;
using IncEngine.Localization;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Wac.IAP;
using Wac.IAP.Rewards;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class IAPItemDrawer : Drawer<IAPSingle>, IPointerClickHandler
    {
        [SerializeField] public Image GradientImage;
        [SerializeField] public Image IconImage;
        [SerializeField] public Text  TitleText;
        [SerializeField] public Text  RewardText;
        [SerializeField] public Text PurchaseText;

        protected virtual void Start()
        {
            LocalizationManager.onChange += Redraw;
        }

        protected void Redraw()
        {
            if (Target == null) return;

            TitleText.text = GetNameForProduct(Target.Product.ProductID);
            IconImage.sprite = Target.Icon;
            RewardText.text = GetTextForReward(Target.Reward);
            PurchaseText.text = GetTextForPurchaseButton();
            GradientImage.color = Target.Color;
        }

        protected override void OnRegisterTarget(IAPSingle target)
        {
            Redraw();
        }

        protected override void OnUnregisterTarget(IAPSingle target)
        {
        }

        private void PurchaseProduct()
        {
            if (Target == null)
            {
                Debug.LogWarning("IAP product not defined");
                return;
            }

            var product = Target;
            IAPManager.BuyProduct(Target.Product, (eventData) => 
            {
                Debug.LogFormat("Purchase status: {0}", eventData.status);

                if (eventData.status == PurchaseStatus.Success)
                {
                    RewardsExecuteUtility.ExecuteReward(product.Reward);
                    Game.Save();
                }
            });
        }

        private string GetTextForReward(IAPReward reward)
        {
            //return string.Format("<color=#F0CD33>{0}</color> {1}", Game.NumberFormatterSpace.Format(reward.Count), GetLocalization(string.Format("let_{0}", reward.Name))).ToUpper();
            return string.Format("<color=#F0CD33>{0}</color>", Game.NumberFormatterSpace.Format(reward.Count)).ToUpper();
        }

        private string GetTextForPurchaseButton()
        {
            ///return string.Format("{0} {1}", GetLocalization("let_buy"), Target.Product.price);
            return GetLocalization("let_buy");
        }

        private string GetNameForProduct(string productID)
        {
            return GetLocalization(string.Format("iap_product_{0}_title", productID)).ToUpper();
        }

        private string GetLocalizationForPhrase(string phrase)
        {
            return GetLocalization(phrase.Replace(" ", "_")).ToUpper();
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            PurchaseProduct();
        }
    }
}
