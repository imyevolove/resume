﻿using IncEngine.Structures;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;
using Wac.UI.Popups;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class CaseItemDrawer : Drawer<Case>
    {
        [SerializeField] public Image iconImage;
        [SerializeField] public Text  nameText;
        [SerializeField] public Text  collectionText;
        [SerializeField] public Text  priceText;

        public void Redraw()
        {
            if (Target == null) return;

            nameText.text = Target.Name.ToUpper();
            priceText.text = Game.NumberFormatter.Format(Target.Price);
            iconImage.sprite = Target.Icon;
            collectionText.text = Target.Collection.Name.ToUpper();
        }

        public void Explore()
        {
            var caseExplorer = PopupManager.Get(PopupId.CaseExplorer) as CaseExplorerPopup;
            caseExplorer.Explore(Target);
            caseExplorer.Show();
        }

        protected override void OnRegisterTarget(Case target)
        {
            Redraw();
        }

        protected override void OnUnregisterTarget(Case target)
        {
            
        }
    }
}
