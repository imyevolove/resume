﻿using IncEngine;
using IncEngine.Abilities;
using IncEngine.Activities;
using IncEngine.Structures;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wac.UI.Windows.Shop
{
    [Serializable]
    public class ShopPanelCases : MonoBehaviour
    {
        [Header("Prefabs")]
        [SerializeField]
        public CaseItemDrawer caseHandlerPrefab;

        [Header("Options")]
        [SerializeField]
        public Activity content;

        private List<CaseItemDrawer> m_Drawers = new List<CaseItemDrawer>();

        void Start()
        {
            InstantiateCaseEntities();

            Game.Player.Abilities.Event_AnyAbilityChange += OnAnyAbilityChange;
        }

        private void InstantiateCaseEntities()
        {
            /// Sort cases by price
            var cases = new List<Case>(EntitiesManager.Cases);
            cases.Sort((cs, cs2) => cs.Price.CompareTo(cs2.Price));

            foreach (var entity in cases)
            {
                InstantiateCaseEntity(entity);
            }
        }

        private CaseItemDrawer InstantiateCaseEntity(Case entity)
        {
            var handler = Instantiate(caseHandlerPrefab, content.Transform, false);
            handler.SetTarget(entity);

            m_Drawers.Add(handler);

            return handler;
        }

        private void OnAnyAbilityChange(Ability ability)
        {
            RedrawDrawers();
        }

        private void RedrawDrawers()
        {
            foreach (var drawer in m_Drawers)
            {
                drawer.Redraw();
            }
        }
    }
}
