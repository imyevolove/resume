﻿using IncEngine.AdsManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Wac.UI.Windows.Earn
{
    [Serializable]
    public class AdditionalEarn : MonoBehaviour, IPointerClickHandler
    {
        private Dictionary<string, Action<int>> m_PrizeMiddlewarePipeline = new Dictionary<string, Action<int>>();
        private bool m_Initialized = false;
        
        protected virtual void Awake()
        {
            UseDefaultPrizePipeline();
        }

        protected virtual void Start()
        {
            if (!m_Initialized)
            {
                InitializeComponents();
            }
            else
            {
                Deactivate();
                SetupEventListeners();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (AdsManager.CanShowVideo())
            {
                Deactivate();

                AdsManager.ShowVideo();
                GivePrizeMedals(10);
            }
        }

        public void Activate()
        {
            SetActivePanel(true);
        }

        public void Deactivate()
        {
            SetActivePanel(false);
        }

        public void SetActivePanel(bool active)
        {
            gameObject.SetActive(active);
        }

        protected void GivePrizeMedals(int count)
        {
            Game.Player.Medals.Increase((ulong)Mathf.Abs(count));
        }

        protected void GivePrizeCoins(int count)
        {
            Game.Player.Money.Increase((ulong)Mathf.Abs(count));
        }

        protected void UseDefaultPrizePipeline()
        {
            m_PrizeMiddlewarePipeline.Clear();

            m_PrizeMiddlewarePipeline.Add("medals", GivePrizeMedals);
            m_PrizeMiddlewarePipeline.Add("coins", GivePrizeMedals);
        }

        protected virtual void GiveCurrency(string currencyName, int value)
        {
            foreach (var handler in m_PrizeMiddlewarePipeline)
            {
                if (handler.Key != currencyName.ToLower()) continue;

                handler.Value.Invoke(value);
                return;
            }

            Debug.LogWarningFormat("Handler for currency '{0}' not found", currencyName);
        }

        protected virtual void SetupEventListeners()
        {
            AdsManager.Event_VideoReady += HandleActivity;
        }

        protected virtual void InitializeComponents()
        {
            HandleActivity();
        }

        private void HandleActivity()
        {
            if (AdsManager.CanShowVideo())
            {
                Activate();
            }
            else
            {
                Deactivate();
            }
        }
    }
}
