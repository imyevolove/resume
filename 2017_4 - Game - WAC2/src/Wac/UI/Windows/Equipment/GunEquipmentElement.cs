﻿using IncEngine.Equipment.Entities;

namespace Wac.UI.Windows.Equipment
{
    public class GunEquipmentElement : BaseEquipmentElement<Gun>
    {
        protected override void OnRegisterTarget(Gun target)
        {
            base.OnRegisterTarget(target);
        }

        protected override void OnUnregisterTarget(Gun target)
        {
            base.OnUnregisterTarget(target);
        }
    }
}
