﻿using System;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Equipment.Entities;
using IncEngine.UI;
using IncEngine.Personal;
using IncEngine;
using IncEngine.Localization;
using IncEngine.Abilities;

namespace Wac.UI.Windows.Equipment
{
    [Serializable, DisallowMultipleComponent]
    public abstract class BaseEquipmentElement<T> : UIElement<T> where T : EquipmentEntity
    {
        [Header("Elements")]
        [SerializeField]
        public Image iconImage;
        [SerializeField]
        public Image gradientImage;
        [SerializeField]
        public Text entityName;
        [SerializeField]
        public Text entitySkinName;
        [SerializeField]
        public Text statsText;
        [SerializeField]
        public Text ownedCountText;
        [SerializeField]
        public PurchaseButton purchaseButton;

        [Header("Stylization")]
        [SerializeField]
        public Color statsColor;

        public Player Owner { get; private set; }

        public virtual void UpdateGraphicUI()
        {
            SetTitle(Target.EntityName, Target.SkinName);
            SetGradientColor(Target.QualityColor);
            SetIcon(Target.Icon);
        }

        public virtual void UpdateStatsUI()
        {
            UpdatePrice();
            SetStats(Target.GetDamage(), Target.GetIncome());
            SetOwnedCount(Target.UpgradeLevel);
        }

        protected override void Awake()
        {
            base.Awake();

            purchaseButton.Event_Click += PurchaseUpgrade;
        }

        protected virtual void Start()
        {
            Owner = GameSession.Current.GetSingleton<Player>();

            LocalizationManager.onChange += UpdateUI;
            Game.Player.Abilities.Event_AnyAbilityChange += OnAnyAbilityChange;
        }

        public virtual void SetTitle(string originalName, string skinName)
        {
            entityName.text = originalName.ToUpper();
            entitySkinName.text = skinName.ToUpper();
        }

        public virtual void SetStats(ulong damage, ulong income)
        {
            var color = ColorUtility.ToHtmlStringRGB(statsColor);
            statsText.text = string.Format("{3} <color=#{0}>{1}</color> {4} <color=#{0}>{2}</color>", 
                color,
                Game.NumberFormatter.Format(damage),
                Game.NumberFormatter.Format(income), 
                GetLocalization("dpc"), GetLocalization("dps"));
        }

        public virtual void SetOwnedCount(uint count)
        {
            ownedCountText.text = string.Format("{0}: {1}", GetLocalization("equipment_own"), Game.NumberFormatterSpace.Format(count));
        }

        public virtual void SetIcon(Sprite sprite)
        {
            iconImage.sprite = sprite;
        }

        public virtual void SetGradientColor(Color color)
        {
            gradientImage.color = color;
        }

        public virtual void SetPrice(ulong price)
        {
            purchaseButton.ResourceCountText.text = Game.NumberFormatter.Format(price);
        }

        public virtual void UpdateUI()
        {
            RedrawPurchaseButtonTitle();
            UpdateGraphicUI();
            UpdateStatsUI();
        }

        protected void RedrawPurchaseButtonTitle()
        {
            purchaseButton.TitleText.text = GetLocalization("let_buy");
        }

        protected virtual void PurchaseUpgrade()
        {
            var status = Owner.PurchaseForMoney(Target.GetUpgradePrice());

            if (status)
            {
                Target.Upgrade();
            }

            Debug.Log("Purchase was ended with status: " + status);
        }

        protected void UpdatePrice()
        {
            SetPrice(Target.GetUpgradePrice());
        }

        protected override void OnRegisterTarget(T target)
        {
            UpdateUI();

            target.Event_AnyChange += UpdateUI;
        }

        protected override void OnUnregisterTarget(T target)
        {
            target.Event_AnyChange -= UpdateUI;
        }

        private void OnAnyAbilityChange(Ability ability)
        {
            UpdatePrice();
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}