﻿using IncEngine.Equipment.Entities;

namespace Wac.UI.Windows.Equipment
{
    public class GloveEquipmentElement : BaseEquipmentElement<Glove>
    {
        protected override void OnRegisterTarget(Glove target)
        {
            base.OnRegisterTarget(target);
            target.Event_ActiveChange += OnActiveChange;
            OnActiveChange(target.Active);
        }

        protected override void OnUnregisterTarget(Glove target)
        {
            base.OnUnregisterTarget(target);
            Deactivate();
        }

        private void OnActiveChange(bool active)
        {
            Active = active;
        }
    }
}
