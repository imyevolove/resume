﻿using IncEngine.Equipment.Entities;

namespace Wac.UI.Windows.Equipment
{
    public class KnifeEquipmentElement : BaseEquipmentElement<Knife>
    {
        protected override void OnRegisterTarget(Knife target)
        {
            base.OnRegisterTarget(target);
            target.Event_ActiveChange += OnActiveChange;
            OnActiveChange(target.Active);
        }

        protected override void OnUnregisterTarget(Knife target)
        {
            base.OnUnregisterTarget(target);
            Deactivate();
        }

        private void OnActiveChange(bool active)
        {
            Active = active;
        }
    }
}
