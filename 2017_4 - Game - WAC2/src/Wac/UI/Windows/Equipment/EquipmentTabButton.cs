﻿using IncEngine.UI;
using UnityEngine.UI;

namespace Wac.UI.Windows.Equipment
{
    public class EquipmentTabButton : TabButton
    {
        public override void SetText(string text)
        {
            Text.text = text.ToUpper();
        }

        public void SetText(string text, int counter)
        {
            SetText(text + " <color=#FED442>" + counter + "</color>");
        }
    }
}
