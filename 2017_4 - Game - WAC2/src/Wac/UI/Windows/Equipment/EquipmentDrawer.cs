﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using IncEngine.Equipment;
using IncEngine.Equipment.Entities;
using IncEngine.UI;
using IncEngine.Activities;
using IncEngine;
using IncEngine.Personal;
using IncEngine.Localization;

/// <summary>
///  ТУТ БЫДЛОКОД!! ИСПРАВИТЬ!!
/// </summary>

namespace Wac.UI.Windows.Equipment
{
    [Serializable, DisallowMultipleComponent, RequireComponent(typeof(TabView))]
    public class EquipmentDrawer : Drawer<IEquipmentMapper>
    {
        [Header("Activities")]
        [SerializeField]
        public Activity contentContainer;
        [SerializeField]
        public Activity navigationContainer;

        [Header("Tab prefabs")]
        [SerializeField]
        public EquipmentTabButton tabButtonPrefab;
        [SerializeField]
        public Activity tabContentPrefab;

        [Header("Element prefabs")]
        [SerializeField]
        private GunEquipmentElement gunElementPrefab;
        [SerializeField]
        private KnifeEquipmentElement knifeElementPrefab;
        [SerializeField]
        private GloveEquipmentElement gloveElementPrefab;
        
        public TabView TabView { get; protected set; }

        public enum EquipmentTabId
        {
            Guns,
            Knives,
            Gloves
        }

        public IEquipmentLockSystem m_GunsLockSystem;

        private List<GunEquipmentElement>      m_GunElements   = new List<GunEquipmentElement>();
        private List<KnifeEquipmentElement>    m_KnifeElements = new List<KnifeEquipmentElement>();
        private List<GloveEquipmentElement>    m_GloveElements = new List<GloveEquipmentElement>();

        private Dictionary<EquipmentTabId, TabRoute> m_Routes = new Dictionary<EquipmentTabId, TabRoute>();

        protected override void Awake()
        {
            base.Awake();

            var player = GameSession.Current.GetSingleton<Player>();

            m_GunsLockSystem = new EquipmentStageLockSystem(
                player.Statistics.MoneyCollected, 
                new LockSystemStagePowCalcMethod(1.85f), 2);
            m_GunsLockSystem.Event_AnyChange += OnGunsLockSystemChange;

            TabView = GetComponent<TabView>();

            RegisterTabRoute(EquipmentTabId.Guns,   "Guns");
            RegisterTabRoute(EquipmentTabId.Knives, "Knives");
            RegisterTabRoute(EquipmentTabId.Gloves, "Gloves");
        }

        protected virtual void Start()
        {
            LocalizationManager.onChange += RedrawNavigation;
        }

        protected void RegisterTabRoute(EquipmentTabId id, string name)
        {
            var button = Instantiate(tabButtonPrefab, navigationContainer.Transform, false);
            button.SetText(name, 0);

            var content = Instantiate(tabContentPrefab, contentContainer.Transform, false);

            var route = new TabRoute();
            route.Button = button;
            route.Content = content;

            TabView.AddRoute(route);

            if (!m_Routes.ContainsKey(id)) m_Routes.Add(id, null);
            m_Routes[id] = route;
        }

        protected override void OnRegisterTarget(IEquipmentMapper target)
        {
            foreach (var gun in target.GetEntities<Gun>())
            {
                var element = Instantiate(gunElementPrefab, m_Routes[EquipmentTabId.Guns].Content.Transform, false);
                element.SetTarget(gun);

                m_GunsLockSystem.Add(gun);

                element.Event_ActiveChange += OnAnyElementActiveChange;

                m_GunElements.Add(element);
            }

            foreach (var knife in target.GetEntities<Knife>())
            {
                var element = Instantiate(knifeElementPrefab, m_Routes[EquipmentTabId.Knives].Content.Transform, false);
                element.SetTarget(knife);

                element.Event_ActiveChange += OnAnyElementActiveChange;

                m_KnifeElements.Add(element);
            }

            foreach (var glove in target.GetEntities<Glove>())
            {
                var element = Instantiate(gloveElementPrefab, m_Routes[EquipmentTabId.Gloves].Content.Transform, false);
                element.SetTarget(glove);

                element.Event_ActiveChange += OnAnyElementActiveChange;

                m_GloveElements.Add(element);
            }

            RedrawNavigation();
            RedrawGuns();
        }

        protected override void OnUnregisterTarget(IEquipmentMapper target)
        {
            foreach (var element in m_GunElements)
                element.Destroy();
            m_GunElements.Clear();

            foreach (var element in m_KnifeElements)
                element.Destroy();
            m_KnifeElements.Clear();

            foreach (var element in m_GloveElements)
                element.Destroy();
            m_GloveElements.Clear();

            RedrawNavigation();
        }

        protected void RedrawGuns()
        {
            var counter = 0;

            foreach (var element in m_GunElements)
            {
                element.Active = counter < m_GunsLockSystem.StageLockIndex;
                counter++;
            }
        }

        protected virtual void OnGunsLockSystemChange()
        {
            RedrawGuns();
        }

        protected virtual void OnAnyElementActiveChange(IPoolable element, bool active)
        {
            RedrawNavigation();
        }

        protected void RedrawNavigation()
        {
            (m_Routes[EquipmentTabId.Guns].Button as EquipmentTabButton)
                .SetText(GetLocalization("guns"), m_GunElements.Count((e) => { return e.Active; }));

            (m_Routes[EquipmentTabId.Knives].Button as EquipmentTabButton)
                .SetText(GetLocalization("knives"), m_KnifeElements.Count((e) => { return e.Active; }));

            (m_Routes[EquipmentTabId.Gloves].Button as EquipmentTabButton)
                .SetText(GetLocalization("gloves"), m_GloveElements.Count((e) => { return e.Active; }));
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}