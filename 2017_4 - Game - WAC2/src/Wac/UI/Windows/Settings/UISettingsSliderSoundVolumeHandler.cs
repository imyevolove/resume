﻿using System;

namespace Wac.UI.Windows.Settings
{
    [Serializable]
    public class UISettingsSliderSoundVolumeHandler : UISettingsSliderHandler
    {
        protected override void Awake()
        {
            base.Awake();

            SetTarget(Game.Profile.settings.VolumeSound);
        }
    }
}
