﻿using IncEngine;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Settings
{
    [Serializable]
    public abstract class UISettingsSliderHandler : Drawer<SmartValue<int>>
    {
        [SerializeField] public Slider Slider;

        protected virtual void Start()
        {
            base.Awake();

            Slider.wholeNumbers = true;
            Slider.minValue = 0;
            Slider.maxValue = 100;

            Slider.onValueChanged.AddListener(OnSliderValueChange);
            RedrawSlider(Target.Value);
        }

        protected override void OnRegisterTarget(SmartValue<int> target)
        {
            target.Event_OnValueChange += RedrawSlider;
        }

        protected override void OnUnregisterTarget(SmartValue<int> target)
        {
            target.Event_OnValueChange += RedrawSlider;
        }

        protected virtual void OnSliderValueChange(float value)
        {
            Target.Value = Mathf.FloorToInt(value);
        }

        protected virtual void RedrawSlider(int value)
        {
            if (Slider.value == value) return;
            Slider.value = value;
        }
    }
}
