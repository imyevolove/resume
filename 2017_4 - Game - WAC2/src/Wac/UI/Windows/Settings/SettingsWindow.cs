﻿using IncEngine.UI;

namespace Wac.UI.Windows.Settings
{
    public class SettingsWindow : Window
    {
        protected override void OnHide()
        {
            base.OnHide();
            Game.Save();
        }
    }
}
