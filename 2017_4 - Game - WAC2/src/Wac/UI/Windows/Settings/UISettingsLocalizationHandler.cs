﻿using IncEngine.Localization;
using IncEngine.UI;
using System;
using System.Linq;
using UnityEngine;

namespace Wac.UI.Windows.Settings
{
    [Serializable]
    public class UISettingsLocalizationHandler : MonoBehaviour
    {
        [SerializeField] public DropdownUI LocalizationDropdown;

        protected virtual void Start()
        {
            Redraw();
            LocalizationManager.onChange += Redraw;
            LocalizationDropdown.onSelectionChange.AddListener(OnSelectionChange);
        }

        private void Redraw()
        {
            var code = (object)LocalizationManager.CurrentLanguage;

            if (LocalizationDropdown.Selection != null && LocalizationDropdown.Selection.Target.Equals(code))
            {
                return;
            }

            LocalizationDropdown.RemoveOptions();

            var options = LocalizationManager.Languages.Select(ln => new DropdownUI.OptionData()
            {
                Name = LocalizationManager.GetLocalization("lang_" + ln).ToUpper(),
                Target = ln
            });

            LocalizationDropdown.AddOptions(options);
            LocalizationDropdown.SetSelection(code);
        }

        private void OnSelectionChange(DropdownUI.OptionData option)
        {
            if (option == null) return;

            var code = (string)option.Target;

            if (string.Equals(code, LocalizationManager.CurrentLanguage, StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            LocalizationManager.ApplyLocalizationLanguage(code);
            Game.Profile.settings.LanguageCode = code;
        }
    }
}
