﻿using System;

namespace Wac.UI.Windows.Settings
{
    [Serializable]
    public class UISettingsSliderMusicVolumeHandler : UISettingsSliderHandler
    {
        protected override void Awake()
        {
            base.Awake();

            SetTarget(Game.Profile.settings.VolumeMusic);
        }
    }
}
