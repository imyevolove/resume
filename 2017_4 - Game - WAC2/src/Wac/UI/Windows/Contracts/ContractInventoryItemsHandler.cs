﻿using IncEngine;
using IncEngine.Activities;
using IncEngine.Collections;
using IncEngine.Deals;
using IncEngine.Structures;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wac.UI.Windows.Contracts
{
    [Serializable]
    public class ContractInventoryItemsHandler : MonoBehaviour
    {
        public event Action Event_Change;
        public event Action<ContractInventoryItem> Event_AnyItemClick;

        [SerializeField] public ContractInventoryItem itemPrefab;
        [SerializeField] public Activity content;
        
        public Contract                 Contract        { get; protected set; }
        public List<InventorySkinItem>  InventoryItems  { get; protected set; }

        private MonoEntityPool<ContractInventoryItem> m_Pool;

        protected virtual void Awake()
        {
            m_Pool = new MonoEntityPool<ContractInventoryItem>(itemPrefab);
        }

        public void Configure(Contract contract, List<InventorySkinItem> items)
        {
            Contract = contract;
            InventoryItems = items;
            InventoryItems.Sort((a, b) => (a.Quality as Quality).Weight.CompareTo((b.Quality as Quality).Weight));
        }

        public void Rebuild()
        {
            if (Contract == null)
            {
                return;
            }

            Clear();

            foreach (var item in InventoryItems)
            {
                if (Contract.Validator.Validate(item) && !Contract.HasContractItem(item))
                {
                    AddItem(item);
                }
            }
        }

        /// <summary>
        /// Add selected item
        /// </summary>
        /// <param name="skin"></param>
        public void AddItem(InventorySkinItem inventorySkinItem)
        {
            var entity = GetFreeItem();
            entity.SetTarget(inventorySkinItem);
            entity.RectTransform.SetAsLastSibling();

            OnChange();
        }

        /// <summary>
        /// Remove selected item
        /// </summary>
        public bool RemoveItem(ContractInventoryItem item)
        {
            var removed = m_Pool.Deactivate(item);

            if (removed)
            {
                OnChange();
            }

            return removed;
        }

        /// <summary>
        /// Deactivate all selected items
        /// </summary>
        public void Clear()
        {
            m_Pool.DeactivateAllActiveEntities();
            OnChange();
        }

        protected ContractInventoryItem GetFreeItem()
        {
            var entity = m_Pool.GetFree(content.Transform, false);
            entity.Event_Click -= OnAnyEntityItemEventClick;
            entity.Event_Click += OnAnyEntityItemEventClick;

            return entity;
        }

        protected virtual void OnAnyEntityItemEventClick(ContractInventoryItem item)
        {
            if (Event_AnyItemClick != null)
            {
                Event_AnyItemClick(item);
            }
        }

        protected virtual void OnChange()
        {
            if (Event_Change != null)
            {
                Event_Change();
            }
        }
    }
}
