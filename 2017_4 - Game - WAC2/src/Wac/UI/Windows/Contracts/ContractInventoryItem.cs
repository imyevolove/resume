﻿using IncEngine.Collections;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Wac.UI.Windows.Contracts
{
    [Serializable]
    public class ContractInventoryItem : UIElement<InventorySkinItem>, IPointerClickHandler
    {
        [SerializeField] public Text  titleText;
        [SerializeField] public Text  skinText;
        [SerializeField] public Image imageGradient;
        [SerializeField] public Image imageIcon;

        public event Action<ContractInventoryItem> Event_Click;

        private List<InventorySkinItem> m_History = new List<InventorySkinItem>();

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (Event_Click != null)
            {
                Event_Click(this);
            }
        }

        protected override void OnRegisterTarget(InventorySkinItem target)
        {
            titleText.text      = target.Skin.Original.Name;
            skinText.text       = target.Skin.Name;
            imageGradient.color = target.Color;
            imageIcon.sprite    = target.Icon;

            m_History.Add(target);
        }

        protected override void OnUnregisterTarget(InventorySkinItem target)
        {

        }
    }
}
