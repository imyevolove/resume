﻿using IncEngine.Deals;
using IncEngine.Localization;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.Contracts
{
    [Serializable]
    public class ContractProcess : Drawer<Contract>
    {
        [SerializeField] public Slider  slider;
        [SerializeField] public Text    contractNumberText;
        [SerializeField] public Text    contractHashText;
        [SerializeField] public Button  approveButton;

        public event Action Event_Approve;
        
        protected virtual void Start()
        {
            slider.wholeNumbers = true;
            slider.minValue = 0;

            approveButton.onClick.AddListener(OnApproveAction);
            LocalizationManager.onChange += UpdateDetails;

            UpdateDetails();
        }

        public void SetProgress(int value)
        {
            slider.value = value;
        }

        protected override void OnUnregisterTarget(Contract target)
        {
            target.Event_Change -= OnContractChange;
            target.Event_TransactionChange -= OnContractTransactionChange;
        }

        protected override void OnRegisterTarget(Contract target)
        {
            slider.maxValue = target.maxContractItems;

            target.Event_Change += OnContractChange;
            target.Event_TransactionChange += OnContractTransactionChange;

            UpdateSlider();
            UpdateDetails();
            UpdateApproveButton();
        }

        protected virtual void UpdateSlider()
        {
            SetProgress(Target.ContractItemsCount);
        }

        protected virtual void UpdateDetails()
        {
            
            contractNumberText.text = (LocalizationManager.GetLocalization("contract") + " #" + Target.TransactionIndex).ToUpper();
            contractHashText.text = Target.TransactionSignature;
        }

        protected virtual void UpdateApproveButton()
        {
            approveButton.gameObject.SetActive(Target.IsFilled);
        }

        protected virtual void OnContractChange()
        {
            UpdateSlider();
            UpdateApproveButton();
        }

        protected virtual void OnContractTransactionChange()
        {
            UpdateDetails();
        }

        protected virtual void OnApproveAction()
        {
            if (Event_Approve != null)
            {
                Event_Approve();
            }
        }
    }
}