﻿using IncEngine;
using IncEngine.Activities;
using IncEngine.Collections;
using IncEngine.Deals;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wac.UI.Popups;

namespace Wac.UI.Windows.Contracts
{
    [Serializable]
    public class ContractWindow : Window
    {
        [Header("Referenses")]
        [SerializeField] public Animator animator;
        [SerializeField] public Activity itemsContent;
        [SerializeField] public ContractProcess contractProcess;
        [SerializeField] public ContractSelectedItemsHandler selectedItemsHandler;
        [SerializeField] public ContractInventoryItemsHandler inventoryItemsHandler;
        [SerializeField] public AudioSource audioSource;

        [Header("Sounds")]
        [SerializeField] public AudioClip audioAddItem;
        [SerializeField] public AudioClip audioRemoveItem;
        [SerializeField] public AudioClip audioFilled;

        public Contract Contract { get; protected set; }

        private List<InventorySkinItem> m_CahcedInventorySkinItems = new List<InventorySkinItem>();
        private bool WaitForApproveTransaction = false;

        protected virtual void Awake()
        {
            Contract = GameSession.Current.GetSingleton<Contract>();
            contractProcess.SetTarget(Contract);

            Contract.Event_Change += OnContractChange;
            Contract.Event_FirstAdd += OnContractFirstAdd;
            inventoryItemsHandler.Event_AnyItemClick += OnInventoryItemsHandlerAnyItemClick;
            selectedItemsHandler.Event_AnyItemClick += OnSelectedItemsHandlerAnyItemClick;
            contractProcess.Event_Approve += ApproveContractTransaction;
        }

        protected virtual void OnInventoryItemsHandlerAnyItemClick(ContractInventoryItem item)
        {
            if (Contract.Add(item.Target))
            {
                inventoryItemsHandler.RemoveItem(item);
                selectedItemsHandler.AddItem(item.Target);

                audioSource.PlayOneShot(audioAddItem);

                if (Contract.ContractItemsCount == 1)
                {
                    inventoryItemsHandler.Rebuild();
                }
            }
            else
            {
                audioSource.PlayOneShot(audioFilled);
            }
        }

        protected virtual void OnContractChange()
        {
            if (!IsShown)
            {
                return;
            }

            animator.SetBool("Filled", Contract.ContractItemsCount > 0);

            if (Contract.IsEmpty && !WaitForApproveTransaction)
            {
                inventoryItemsHandler.Rebuild();
            }
        }

        protected virtual void ApproveContractTransaction()
        {
            WaitForApproveTransaction = true;

            var result = Contract.ApproveTransaction(false);

            if (result != null)
            {
                var dropExplorer = PopupManager.Get<DropExplorerPopup>(PopupId.DropExplorer);
                dropExplorer.Explore(result, Contract.Owner, OnTransactionApproved);
            }
            else
            {
                WaitForApproveTransaction = false;
            }
        }

        protected virtual void OnSelectedItemsHandlerAnyItemClick(ContractSelectedItem item)
        {
            if (Contract.Remove(item.Target))
            {
                if (!Contract.IsEmpty)
                {
                    inventoryItemsHandler.AddItem(item.Target);
                }

                selectedItemsHandler.RemoveItem(item);

                audioSource.PlayOneShot(audioAddItem);
            }
        }

        protected virtual void OnContractFirstAdd()
        {

        }

        protected virtual void OnTransactionApproved()
        {
            ResetContract();
            WaitForApproveTransaction = false;
        }

        protected override void OnShow()
        {
            base.OnShow();
            ResetContract();
        }

        protected void ResetContract()
        {
            Contract.Reset();
            inventoryItemsHandler.Clear();
            selectedItemsHandler.Clear();

            UpdateCache();
            ConfigureHandlers();

            inventoryItemsHandler.Rebuild();
        }

        protected void ConfigureHandlers()
        {
            inventoryItemsHandler.Configure(Contract, m_CahcedInventorySkinItems);
        }

        protected void UpdateCache()
        {
            m_CahcedInventorySkinItems.Clear();
            m_CahcedInventorySkinItems = Contract.Owner.Inventory.Items.Cast<InventorySkinItem>().ToList();
        }
    }
}