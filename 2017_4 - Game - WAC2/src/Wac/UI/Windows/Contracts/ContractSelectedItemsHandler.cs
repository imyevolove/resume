﻿using IncEngine;
using IncEngine.Activities;
using IncEngine.Collections;
using System;
using UnityEngine;

namespace Wac.UI.Windows.Contracts
{
    [Serializable]
    public class ContractSelectedItemsHandler : MonoBehaviour
    {
        public event Action Event_Change;
        public event Action<ContractSelectedItem> Event_AnyItemClick;

        [SerializeField] public ContractSelectedItem selectedItemPrefab;
        [SerializeField] public Activity content;
        
        private MonoEntityPool<ContractSelectedItem> m_Pool;

        protected virtual void Awake()
        {
            m_Pool = new MonoEntityPool<ContractSelectedItem>(selectedItemPrefab);
            m_Pool.Prebuild(10, content.Transform, false);
        }

        /// <summary>
        /// Add selected item
        /// </summary>
        /// <param name="skin"></param>
        public void AddItem(InventorySkinItem inventorySkinItem)
        {
            var entity = GetFreeItem();
            entity.SetTarget(inventorySkinItem);
            entity.RectTransform.SetAsFirstSibling();
            
            OnChange();
        }

        /// <summary>
        /// Remove selected item
        /// </summary>
        public bool RemoveItem(ContractSelectedItem item)
        {
            var removed = m_Pool.Deactivate(item);
            
            if (removed)
            {
                OnChange();
            }

            return removed;
        }

        /// <summary>
        /// Deactivate all selected items
        /// </summary>
        public void Clear()
        {
            m_Pool.DeactivateAllActiveEntities();
            OnChange();
        }

        protected ContractSelectedItem GetFreeItem()
        {
            var entity = m_Pool.GetFree(content.Transform, false);
            entity.Event_Click -= OnAnyEntityItemEventClick;
            entity.Event_Click += OnAnyEntityItemEventClick;

            return entity;
        }

        protected virtual void OnAnyEntityItemEventClick(ContractSelectedItem item)
        {
            if (Event_AnyItemClick != null)
            {
                Event_AnyItemClick(item);
            }
        }

        protected virtual void OnChange()
        {
            if (Event_Change != null)
            {
                Event_Change();
            }
        }
    }
}
