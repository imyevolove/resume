﻿using IncEngine.Collections;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Wac.UI.Windows.Contracts
{
    [Serializable]
    public class ContractSelectedItem : UIElement<InventorySkinItem>, IPointerClickHandler
    {
        [SerializeField] public Text  titleText;
        [SerializeField] public Image imageGradient;
        [SerializeField] public Image imageIcon;

        public event Action<ContractSelectedItem> Event_Click;

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (Event_Click != null)
            {
                Event_Click(this);
            }
        }

        protected override void OnRegisterTarget(InventorySkinItem target)
        {
            titleText.text      = target.Skin.Original.Name + "\n" + target.Skin.Name;
            imageGradient.color = target.Quality.Color;
            imageIcon.sprite    = target.Icon;
        }

        protected override void OnUnregisterTarget(InventorySkinItem target)
        {
            
        }
    }
}
