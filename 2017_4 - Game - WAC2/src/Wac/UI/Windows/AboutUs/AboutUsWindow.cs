﻿using IncEngine.Localization;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI.Windows.AboutUs
{
    [Serializable]
    public class AboutUsWindow : Window
    {
        [SerializeField] public RectTransform InformationContainer;
        [SerializeField] public Text InformationLinePrefab;

        [HideInInspector] private Dictionary<string, Text> m_InfoLines = new Dictionary<string, Text>();

        protected virtual void Start()
        {
            Redraw();
            LocalizationManager.onChange += Redraw;
        }

        protected void Redraw()
        {
            GetLine("app_version").text = (string.Format("{0} - {1}",
                LocalizationManager.GetLocalization("version"), Application.version)).ToUpper();

            GetLine("system_max_value").text = (string.Format("{0}: \n<color=#FFCD4B>{1}</color>",
                LocalizationManager.GetLocalization("system_max_unsigned_value"), 
                Game.NumberFormatterSpace.Format(ulong.MaxValue))).ToUpper();
        }

        private Text GetLine(string id)
        {
            id = FormatInfoId(id);

            Text line = ContainsInfoLine(id) ? m_InfoLines[id] : CreateInformationLine(id);
            return line;
        }

        private bool ContainsInfoLine(string id)
        {
            id = FormatInfoId(id);
            return m_InfoLines.ContainsKey(id);
        }

        private Text CreateInformationLine(string id)
        {
            id = FormatInfoId(id);

            var line = Instantiate(InformationLinePrefab);
            line.rectTransform.SetParent(InformationContainer, false);

            AddInfoLine(id, line);

            return line;
        }

        private void AddInfoLine(string id, Text text)
        {
            if (m_InfoLines.ContainsKey(id))
            {
                RemoveInfoLine(id);
            }

            m_InfoLines[id] = text;
        }

        private bool RemoveInfoLine(string id)
        {
            if (!m_InfoLines.ContainsKey(id)) return false;

            return m_InfoLines.Remove(id);
        }

        private string FormatInfoId(string id)
        {
            return id.ToLower();
        }
    }
}
