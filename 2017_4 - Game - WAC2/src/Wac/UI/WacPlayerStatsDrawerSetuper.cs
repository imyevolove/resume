﻿using UnityEngine;
using Wac.Common;
using IncEngine.UI;

namespace Wac.UI
{
    [RequireComponent(typeof(PlayerStatsDrawer))]
    public class WacPlayerStatsDrawerSetuper : AbstractSetuper
    {
        protected override void Setup()
        {
            GetComponent<PlayerStatsDrawer>().SetTarget(Game.Player);
        }
    }
}
