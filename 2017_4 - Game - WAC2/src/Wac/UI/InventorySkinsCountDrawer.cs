﻿using UnityEngine;
using IncEngine.UI;
using IncEngine;
using IncEngine.Personal;
using System.Linq;
using IncEngine.Structures;
using IncEngine.Collections;
using UnityEngine.UI;

namespace Wac.UI
{
    public class InventorySkinsCountDrawer : Drawer<IInventory>
    {
        [SerializeField]
        public Text valueText;

        protected override void Awake()
        {
            base.Awake();

            SetTarget(GameSession.Current.GetSingleton<Player>().Inventory);
        }

        protected void Redraw()
        {
            valueText.text = Target.Items.Where(item => item.Target is Skin).Count().ToString();
        }

        protected override void OnRegisterTarget(IInventory target)
        {
            target.Event_Change += Redraw;
            Redraw();
        }

        protected override void OnUnregisterTarget(IInventory target)
        {
            target.Event_Change -= Redraw;
        }
    }
}
