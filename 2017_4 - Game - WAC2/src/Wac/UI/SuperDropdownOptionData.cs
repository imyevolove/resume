﻿using UnityEngine.UI;

namespace Wac.UI
{
    public class SuperDropdownOptionData : Dropdown.OptionData
    {
        public object Bind { get; set; }

        public SuperDropdownOptionData(string name, object bind)
        {
            text = name;
            Bind = bind;
        }
    }
}
