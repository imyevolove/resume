﻿using UnityEngine;
using Wac.Common;
using IncEngine.UI;

namespace Wac.UI
{
    [RequireComponent(typeof(ResourceValueDrawer))]
    public class WacResourceValueDrawerDiamondsSetuper : AbstractSetuper
    {
        protected override void Setup()
        {
            GetComponent<ResourceValueDrawer>().SetTarget(Game.Player.Medals);
        }
    }
}
