﻿using IncEngine;
using IncEngine.Collections;
using IncEngine.Personal;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI
{
    [Serializable]
    public class InventoryIndicator : Drawer<IInventory>
    {
        [SerializeField] public Text counterText;

        protected override void Awake()
        {
            base.Awake();

            SetTarget(GameSession.Current.GetSingleton<Player>().Inventory);
        }

        public void UpdateUI()
        {
            counterText.text = Target.Items.Count.ToString();
        }

        protected override void OnRegisterTarget(IInventory target)
        {
            target.Event_Change += UpdateUI;
            UpdateUI();
        }

        protected override void OnUnregisterTarget(IInventory target)
        {
            target.Event_Change -= UpdateUI;
        }
    }
}
