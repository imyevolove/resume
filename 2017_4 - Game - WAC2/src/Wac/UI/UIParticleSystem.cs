﻿using IncEngine;
using UnityEngine;

namespace Wac.UI
{
    public class UIParticleSystem
    {
        public readonly UIParticle ParticleOriginal;
        public RectTransform Container;
        private MonoEntityPool<UIParticle> m_Pool;

        public UIParticleSystem(UIParticle original, RectTransform container)
        {
            ParticleOriginal = original;
            m_Pool = new MonoEntityPool<UIParticle>(original);
            Container = container;
        }

        public void Emit(uint count)
        {
            while (count > 0)
            {
                Emit();
                count--;
            }
        }

        public UIParticle Emit()
        {
            return Spawn();
        }

        private UIParticle Spawn()
        {
            return m_Pool.GetFree(Container);
        }
    }
}
