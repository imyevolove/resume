﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class FloatInteractiveResource : UIParticle
    {
        [SerializeField] public Image Image;
        [SerializeField] public Text Text;

        public const string AnimatorKeyName = "active";

        private Animator m_Animator;

        protected virtual void Awake()
        {
            m_Animator = GetComponent<Animator>();
        }

        public void SetDetails(Sprite icon, string text)
        {
            Image.sprite = icon;
            Text.text = text;
        }

        protected override void OnDeactivate()
        {
            if (gameObject.activeInHierarchy)
            {
                m_Animator.SetBool(AnimatorKeyName, false);
            }

            base.OnDeactivate();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            m_Animator.SetBool(AnimatorKeyName, true);
        }
    }
}
