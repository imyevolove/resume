﻿using IncEngine.Personal;
using IncEngine.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI
{
    [Serializable]
    public class PlayerStatsIncomeDrawer : Drawer<Player>
    {
        [SerializeField] public Text ValueText;

        protected override void Awake()
        {
            base.Awake();

            SetTarget(Game.Player);
        }

        protected void Redraw()
        {
            ValueText.text = Game.NumberFormatter.Format(Target.GetIncomeWithoutRandomModifiers());
        }

        protected override void OnRegisterTarget(Player target)
        {
            target.Event_AnyChange += Redraw;
            Redraw();
        }

        protected override void OnUnregisterTarget(Player target)
        {
            target.Event_AnyChange -= Redraw;
        }
    }
}
