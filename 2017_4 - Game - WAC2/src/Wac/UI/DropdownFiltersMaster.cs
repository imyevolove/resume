﻿using IncEngine.Collections;
using IncEngine.Collections.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Wac.UI
{
    [Serializable]
    public class DropdownFiltersMaster : MonoBehaviour, IInventoryFilter
    {
        public Action Event_Change;

        [SerializeField] private List<DropdownFilter> m_Filters;
        
        protected virtual void Awake()
        {
            if (m_Filters == null)
            {
                m_Filters = new List<DropdownFilter>();
            }

            SetUpFilters(m_Filters);
        }

        public List<InventoryItem> Filter(ICollection<InventoryItem> items)
        {
            ICollection<InventoryItem> filteredItems = items.ToList();

            foreach (var filter in m_Filters)
            {
                filter.Filter(ref filteredItems);
            }

            return filteredItems.ToList();
        }

        public void Filter(ref ICollection<InventoryItem> items)
        {
            foreach (var filter in m_Filters)
            {
                filter.Filter(ref items);
            }
        }

        protected void SetUpFilters(IEnumerable<DropdownFilter> filters)
        {
            foreach (var filter in filters)
            {
                SetUpFilterEvents(filter);
            }
        }

        protected void SetUpFilterEvents(DropdownFilter filter)
        {
            filter.Event_Change -= OnAnyFilterEventChange;
            filter.Event_Change += OnAnyFilterEventChange;
        }

        protected virtual void OnAnyFilterEventChange()
        {
            CallEventChange();
        }

        protected void CallEventChange()
        {
            if (Event_Change != null)
            {
                Event_Change();
            }
        }
    }
}
