﻿using UnityEngine;
using Wac.Common;
using IncEngine.UI;

namespace Wac.UI
{
    [RequireComponent(typeof(AbstractRankDrawer))]
    public class WacRankDrawerSetuper : AbstractSetuper
    {
        protected override void Setup()
        {
            GetComponent<AbstractRankDrawer>().SetTarget(Game.Player.RankSystem);
        }
    }
}
