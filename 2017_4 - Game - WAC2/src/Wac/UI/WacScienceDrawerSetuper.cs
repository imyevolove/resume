﻿using UnityEngine;
using Wac.Common;
using Wac.UI.Windows.Science;

namespace Wac.UI
{
    [RequireComponent(typeof(ScienceDrawer))]
    public class WacScienceDrawerSetuper : AbstractSetuper
    {
        protected override void Setup()
        {
            GetComponent<ScienceDrawer>().SetTarget(Game.Player.Science);
        }
    }
}
