﻿using UnityEngine;
using Wac.Common;
using Wac.UI.Windows.Equipment;

namespace Wac.UI
{
    [RequireComponent(typeof(EquipmentDrawer))]
    public class WacEquipmentDrawerSetuper : AbstractSetuper
    {
        protected override void Setup()
        {
            GetComponent<EquipmentDrawer>().SetTarget(Game.Player.Equipment);
        }
    }
}
