﻿using IncEngine;
using IncEngine.Localization;
using IncEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Wac.UI
{
    public class QuitAppButton : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            var dialog = DialogManager.CreateDialog<ConfirmationDialog>();
            dialog.Configure(GetLocalization("dialog_quit_title"), GetLocalization("dialog_quit_message"));
            dialog.Execute(WaitForQuitConfirmationResult);
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }

        private void WaitForQuitConfirmationResult(ConfirmationDialogResult result)
        {
            if (result == ConfirmationDialogResult.Yes)
            {
                Game.Quit();
            }
        }
    }
}
