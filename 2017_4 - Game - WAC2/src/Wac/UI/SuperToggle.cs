﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.UI
{
    [Serializable]
    public class SuperToggle : Toggle
    {
        [SerializeField] public Sprite indicator;
    }
}
