﻿using IncEngine.Collections.Filters;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Collections;

namespace Wac.UI
{
    [Serializable, RequireComponent(typeof(Dropdown))]
    public abstract class DropdownFilter : MonoBehaviour, IInventoryFilter
    {
        public Action Event_Change;
        public abstract List<InventoryItem> Filter(ICollection<InventoryItem> items);
        public abstract void Filter(ref ICollection<InventoryItem> items);
    }
}
