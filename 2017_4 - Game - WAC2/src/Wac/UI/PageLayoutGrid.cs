﻿using IncEngine;
using IncEngine.Activities;
using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Wac.UI
{
    public enum SwipeDirection
    {
        Left,
        Right,
        Top,
        Bottom
    }

    [Serializable]
    public abstract class PageLayoutGrid<T, UIT> : PageLayoutGroup, IBeginDragHandler, IEndDragHandler
        where T : class
        where UIT : UIElement<T>
    {
        [SerializeField] public UIT ItemPrefab;
        [SerializeField] public uint ItemsPerPage;
        [SerializeField] public Activity Content;

        private List<T> m_Items = new List<T>();
        private MonoEntityPool<UIT> m_ElementsPool;

        protected override void OnValidate()
        {
            base.OnValidate();

            if (Application.isEditor)
            {
                RebuildPage();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            m_ElementsPool = new MonoEntityPool<UIT>(ItemPrefab);

            Rebuild();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            var dtX = (eventData.position.x - eventData.pressPosition.x);
            var dtY = (eventData.position.y - eventData.pressPosition.y);

            if (Math.Abs(dtX) > Math.Abs(dtY))
            {
                Swipe(GetSwipeHorizontalDirection(eventData.position.x, eventData.pressPosition.x));
            }
            else
            {
                Swipe(GetSwipeVerticalDirection(eventData.position.y, eventData.pressPosition.y));
            }
        }

        public void AddItems(ICollection<T> items)
        {
            m_Items.AddRange(items);
            OnItemsChange();
        }

        public void AddItem(T item)
        {
            m_Items.Add(item);
            OnItemsChange();
        }

        public bool Remove(T item)
        {
            var status = m_Items.Remove(item);

            if (status)
            {
                OnItemsChange();
            }

            return status;
        }

        public bool Remove(int index)
        {
            if (index < 0 || index >= m_Items.Count) return false;

            m_Items.RemoveAt(index);
            OnItemsChange();

            return true;
        }

        public void Clear()
        {
            m_Items.Clear();
            OnItemsChange();
        }

        public void RebuildPage()
        {
            var startIndex = CurrentPageIndex * ItemsPerPage;

            if (startIndex >= m_Items.Count)
            {
                if (m_ElementsPool != null)
                {
                    m_ElementsPool.DeactivateAllActiveEntities();
                }

                return;
            }

            RedrawItems(startIndex);
        }

        protected SwipeDirection GetSwipeHorizontalDirection(float startX, float endX)
        {
            var dir = startX - endX;
            return dir <= 0 ? SwipeDirection.Right : SwipeDirection.Left;
        }

        protected SwipeDirection GetSwipeVerticalDirection(float startX, float endX)
        {
            var dir = startX - endX;
            return dir <= 0 ? SwipeDirection.Bottom : SwipeDirection.Top;
        }

        protected void RedrawItems(uint startIndex)
        {
            var lastIndex = startIndex + ItemsPerPage;
            lastIndex = (uint)Mathf.Clamp(lastIndex, startIndex, m_Items.Count);
            
            m_ElementsPool.DeactivateAllActiveEntities();

            for (var i = (int)startIndex; i < lastIndex; i++)
            {
                var element = GetFreeUIElement();
                element.RectTransform.SetAsLastSibling();
                element.SetTarget(m_Items[i]);
            }
        }

        protected UIT GetFreeUIElement()
        {
            return m_ElementsPool.GetFree(Content.Transform);
        }

        protected void RecalculatePagesCount()
        {
            var count = (uint)(m_Items.Count / ItemsPerPage);
            var rest = m_Items.Count % ItemsPerPage;

            if (rest > 0)
            {
                count++;
            }

            SetPagesCount(count);
        }

        protected void Rebuild()
        {
            RecalculatePagesCount();
            RebuildPage();
        }

        protected override void OnCurrentPageChange(uint index)
        {
            base.OnCurrentPageChange(index);
            Rebuild();
        }

        protected virtual void OnItemsChange()
        {
            Rebuild();
        }

        protected abstract void Swipe(SwipeDirection direction);
    }
}
