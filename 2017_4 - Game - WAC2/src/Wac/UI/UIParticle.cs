﻿using System;
using IncEngine.UI;
using UnityEngine;
using System.Collections;

namespace Wac.UI
{
    [Serializable]
    public abstract class UIParticle : UIPoolableElement
    {
        [SerializeField] public float lifetime;

        private Coroutine m_LifetimeCoroutine;

        protected override void OnDeactivate()
        {
            base.OnDeactivate();

            StopLifetimeTimer();
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            StartLifetimeTimer();
        }

        private void StartLifetimeTimer()
        {
            StopLifetimeTimer();

            m_LifetimeCoroutine = StartCoroutine(Deactivator());
        }

        private void StopLifetimeTimer()
        {
            if (m_LifetimeCoroutine == null) return;

            StopCoroutine(m_LifetimeCoroutine);
            m_LifetimeCoroutine = null;
        }

        private IEnumerator Deactivator()
        {
            yield return new WaitForSeconds(lifetime);
            Deactivate();
        }
    }
}
