﻿using UnityEngine;
using IncEngine.UI;
using IncEngine;
using IncEngine.Personal;
using IncEngine.Collections;
using UnityEngine.UI;

namespace Wac.UI
{
    public class InventoryCountDrawer : Drawer<IInventory>
    {
        [SerializeField]
        public Text valueText;

        protected override void Awake()
        {
            base.Awake();

            SetTarget(GameSession.Current.GetSingleton<Player>().Inventory);
        }

        protected void Redraw()
        {
            valueText.text = Game.NumberFormatter.Format(Target.Items.Count);
        }

        protected override void OnRegisterTarget(IInventory target)
        {
            target.Event_Change += Redraw;
            Redraw();
        }

        protected override void OnUnregisterTarget(IInventory target)
        {
            target.Event_Change -= Redraw;
        }
    }
}
