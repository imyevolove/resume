﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.U2D;

namespace Wac.Resources
{
    public class AtlasChainSpriteProviderBuilder : IAtlasChainSpriteProviderBuilder
    {
        private AtlasChainSpriteProvider _currentProvider;
        private AtlasChainSpriteProvider _rootProvider;

        public AtlasChainSpriteProviderBuilder()
        {
        }

        public IAtlasChainSpriteProviderBuilder Add(SpriteAtlas atlas)
        {
            var provider = new AtlasChainSpriteProvider(atlas);

            if (_currentProvider != null)
            {
                _currentProvider.nextProvider = provider;
            }
            else
            {
                _rootProvider = provider;
            }

            _currentProvider = provider;

            return this;
        }

        public IAtlasChainSpriteProviderBuilder Add(IEnumerable<SpriteAtlas> atlases)
        {
            foreach (var atlas in atlases)
            {
                Add(atlas);
            }

            return this;
        }

        public IAtlasChainSpriteProviderBuilder Add(params SpriteAtlas[] atlases)
        {
            Add(atlases as IEnumerable<SpriteAtlas>);
            return this;
        }

        public AtlasChainSpriteProvider Build()
        {
            return _rootProvider;
        }
    }
}
