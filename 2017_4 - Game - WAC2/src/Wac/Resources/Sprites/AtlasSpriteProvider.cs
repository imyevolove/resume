﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.U2D;

namespace Wac.Resources
{
    public class AtlasSpriteProvider : ISpriteProvider
    {
        public SpriteAtlas Atlas { get; protected set; }

        public AtlasSpriteProvider(SpriteAtlas atlas)
        {
            Atlas = atlas;
        }

        public virtual Sprite Get(string name)
        {
            return Atlas.GetSprite(name);
        }
    }
}
