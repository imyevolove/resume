﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.U2D;

namespace Wac.Resources
{
    public class AtlasChainSpriteProvider : AtlasSpriteProvider
    {
        public AtlasSpriteProvider nextProvider;

        public int ChainLength
        {
            get
            {
                var chain = nextProvider as AtlasChainSpriteProvider;
                return 1 + (chain != null ? chain.ChainLength : (nextProvider == null ? 0 : 1));
            }
        }

        public AtlasChainSpriteProvider(SpriteAtlas atlas)
            : base(atlas)
        {
        }

        public AtlasChainSpriteProvider(SpriteAtlas atlas, AtlasSpriteProvider next)
            : base(atlas)
        {
            nextProvider = next;
        }

        public override Sprite Get(string name)
        {
            var sprite = base.Get(name);

            if (sprite)
            {
                return sprite;
            }

            if (nextProvider != null)
            {
                return nextProvider.Get(name);
            }

            return null;
        }
    }
}
