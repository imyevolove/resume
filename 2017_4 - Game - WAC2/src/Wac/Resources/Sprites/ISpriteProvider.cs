﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wac.Resources
{
    public interface ISpriteProvider
    {
        Sprite Get(string name);
    }
}
