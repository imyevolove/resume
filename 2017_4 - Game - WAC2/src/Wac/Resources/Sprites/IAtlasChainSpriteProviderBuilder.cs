﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.U2D;

namespace Wac.Resources
{
    public interface IAtlasChainSpriteProviderBuilder
    {
        IAtlasChainSpriteProviderBuilder Add(SpriteAtlas atlas);
        IAtlasChainSpriteProviderBuilder Add(IEnumerable<SpriteAtlas> atlases);
        IAtlasChainSpriteProviderBuilder Add(params SpriteAtlas[] atlases);
        AtlasChainSpriteProvider Build();
    }
}
