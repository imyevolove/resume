﻿using System;
using UnityEngine;
using Wac.Play;
using Wac.UI.Windows.Missions;

namespace Wac.Data
{
    [Serializable]
    public class MissionDrawerBind
    {
        [SerializeField] public TaskSign sign;
        [SerializeField] public MissionTaskDrawer drawerPrefab;
    }
}
