﻿using System;
using UnityEngine;
using Wac.IAP;

namespace Wac.Data
{
    [Serializable]
    public class IAPSingleProductBind
    {
        [Header("Game IAP details")]
        [SerializeField] public string          rewardName;
        [SerializeField] public Sprite          rewardIcon;
        [SerializeField] public Color           rewardColor;
        [SerializeField] public ulong           rewardCount;
        [SerializeField] public IAPRewardType   rewardType;

        [Header("Market service IAP details")]
        [SerializeField] public string          sku;
    }
}
