﻿using System;
using UnityEngine;
using IncEngine;
using IncEngine.Data;
using System.Collections.Generic;
using UnityEngine.U2D;

namespace Wac.Data
{
    [Serializable]
    public class GameDataProvider
    {
        [Header("Sprites")]
        [SerializeField] public SpriteAtlas[] atlases;

        [Header("Entities")]
        [SerializeField] public TextAsset   gunsDataTextAsset;
        [SerializeField] public TextAsset   gunsDefaultDataTextAsset;
        [SerializeField] public TextAsset   knivesDataTextAsset;
        [SerializeField] public TextAsset   knivesDefaultDataTextAsset;
        [SerializeField] public TextAsset   glovesDataTextAsset;
        [SerializeField] public TextAsset   glovesDefaultDataTextAsset;
        [SerializeField] public TextAsset   casesDataTextAsset;
        [SerializeField] public TextAsset   stickersDataTextAsset;
        [SerializeField] public TextAsset   collectionsDataTextAsset;
        [SerializeField] public TextAsset   playerDataTextAsset;
        [SerializeField] public TextAsset   mapsDataTextAsset;

        [Header("Ranks")]
        [SerializeField] public Rank[]      ranks;

        [Header("Technologies")]
        [SerializeField] public TechBind[]  techBinds;

        [Header("IAP Products")]
        [SerializeField] public IAPSingleProductBind[] iapSingleProducts;
        
        [Header("Mission")]
        [SerializeField] public MissionDataBind missionDataMobs;
        [SerializeField] public MissionDataBind missionDataBomb;
        [SerializeField] public MissionDataBind missionDataHostage;
    }
}