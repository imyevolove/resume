﻿using IncEngine.Play;
using System;
using UnityEngine;

namespace Wac.Data
{
    [Serializable]
    public class MissionDataBind
    {
        [SerializeField] public UnitDetails[] unitDetails;
        [SerializeField] public uint IterationsCount;
    }
}
