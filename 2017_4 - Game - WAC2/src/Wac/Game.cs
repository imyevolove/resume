﻿using IncEngine;
using IncEngine.Dice;
using IncEngine.Formatters;
using IncEngine.Personal;
using IncEngine.Storage;
using UnityEngine;
using Wac.Activities;
using Wac.IAP;
using Wac.Play;

namespace Wac
{
    public sealed class Game : GameSession
    {
        public static GameSession Session { get; private set; }
        
        public Game()
        {
            Session = this;
            IsNewGame = false;
        }

        public static bool IsNewGame { get; private set; }

        /// <summary>
        /// Game main app window
        /// </summary>
        public static WacAppWindow AppWindow { get; set; }

        public static readonly IAPCollection IAPCollection = new IAPCollection();

        public static NumberFormatterSpace NumberFormatterSpace
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<NumberFormatterSpace>(); }
        }

        public static INumberFormatter NumberFormatter
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<INumberFormatter>(); }
        }

        public static IMarket Market
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<IMarket>(); }
        }

        public static Profile Profile
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<Profile>(); }
        }

        public static WacMission Mission
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<WacMission>(); }
        }

        public static Player Player
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<Player>(); }
        }

        public static CaseOpener CaseOpener
        {
            set { Session.AddSingleton(value); }
            get { return Session.GetSingleton<CaseOpener>(); }
        }

        public static void Quit()
        {
            Save();
            Application.Quit();
        }

        /// <summary>
        /// Save game
        /// </summary>
        public static void Save()
        {
            string xml = Profile.GetStorageData().ToXml();
            Storage.SaveText(Profile.StorageFilePath, xml);

            Debug.Log("Game save");
        }

        /// <summary>
        /// Load game
        /// </summary>
        public static void Load()
        {
            string data = Storage.LoadText(Profile.StorageFilePath);
            IsNewGame = string.IsNullOrEmpty(data);

            if (!string.IsNullOrEmpty(data))
            {
                Profile.SetStorageData(StorageDataContainer.Deserialize(data));
            }
        }
    }
}
