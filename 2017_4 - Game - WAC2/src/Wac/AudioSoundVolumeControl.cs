﻿namespace Wac
{
    public class AudioSoundVolumeControl : AudioVolumeControl
    {
        protected virtual void Awake()
        {
            SetTarget(Game.Profile.settings.VolumeSound);
        }
    }
}
