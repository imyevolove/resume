﻿using UnityEngine;
using Wac.Common;

namespace Wac.MiniGames
{
    [RequireComponent(typeof(AbstractMiniGame))]
    public class WacMiniGamePlayerSetuper : AbstractSetuper
    {
        protected override void Setup()
        {
            GetComponent<AbstractMiniGame>().SetPlayer(Game.Player);
        }
    }
}
