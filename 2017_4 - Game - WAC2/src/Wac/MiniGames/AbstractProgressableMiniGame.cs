﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using IncEngine.Abilities;
using IncEngine.Common;
using IncEngine.Personal;
using IncEngine;

namespace Wac.MiniGames
{
    [Serializable]
    public abstract class AbstractProgressableMiniGame : AbstractMiniGame, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        [SerializeField] public MixedAudioPlayer ClickActionAudioPlayer;
        [SerializeField] public Slider progressSlider;
        [SerializeField] public RectTransform interactiveTransform;
        [SerializeField] public float pulseDuration;
        [SerializeField] public float pulsePeakNormal = 1f;
        [SerializeField] public float pulsePeakDown = 0.5f;
        [SerializeField] public AnimationCurve pulseCurve;

        private Coroutine   m_Helper_PulseAnimationCoroutine;
        private float       m_Helper_PulseAnimationTime;
        private float       m_Helper_PulseAnimationStep;
        private float       m_Helper_PulseAnimationStartScale;
        private Vector3     m_Helper_PulseAnimationScale = Vector3.one;

        protected Ability   m_PlayerExperienceValueAdditionAbility;

        public virtual void GiveExperience(ulong value)
        {
            Player.RankSystem.IncreaseExperience(value);
        }

        protected override void Awake()
        {
            base.Awake();
            UpdateProgressUI();
        }

        protected override void OnProgressTick(IncrementalProgress sender, ulong value)
        {
            base.OnProgressTick(sender, value);
            UpdateProgressUI();
        }

        protected override void OnSetPlayer(Player player)
        {
            base.OnSetPlayer(player);
            m_PlayerExperienceValueAdditionAbility = player.Abilities.GetAbility(AbilityType.ExperienceSpeedUpValue);
        }


        protected void UpdateProgressUI()
        {
            progressSlider.value = Progress.Percentage01;
        }

        /// <summary>
        /// Click event handler
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (Player == null) return;

            Action();
            ClickActionAudioPlayer.Play();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            RunPulseAnimation(pulsePeakDown);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            RunPulseAnimation(pulsePeakNormal);
        }

        protected void RunPulseAnimation(float size)
        {
            if (interactiveTransform == null) return;

            if(m_Helper_PulseAnimationCoroutine != null)
                StopCoroutine(m_Helper_PulseAnimationCoroutine);

            m_Helper_PulseAnimationStartScale = interactiveTransform.localScale.x;
            m_Helper_PulseAnimationCoroutine  = StartCoroutine(PulseAnimationTransformation(size));
        }

        protected void SetPulseScale(float scale)
        {
            m_Helper_PulseAnimationScale.Set(scale, scale, scale);
            interactiveTransform.localScale = m_Helper_PulseAnimationScale;
        }

        protected void SetPulseScale(Vector3 scale)
        {
            interactiveTransform.localScale = scale;
        }

        protected IEnumerator PulseAnimationTransformation(float scale)
        {
            m_Helper_PulseAnimationTime = 0;

            while (m_Helper_PulseAnimationTime < 1)
            {
                m_Helper_PulseAnimationStep = pulseCurve.Evaluate(m_Helper_PulseAnimationTime);
                m_Helper_PulseAnimationTime += Time.deltaTime / pulseDuration;

                SetPulseScale(Mathf.Lerp(m_Helper_PulseAnimationStartScale, scale, m_Helper_PulseAnimationStep));

                yield return false;
            }

            SetPulseScale(scale);
        }
    }
}
