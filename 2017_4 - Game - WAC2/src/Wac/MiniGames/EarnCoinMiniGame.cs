﻿using System;
using IncEngine.Personal;
using UnityEngine;
using IncEngine.UpdateManagement;
using UnityEngine.UI;
using IncEngine.Abilities;
using IncEngine.Localization;

namespace Wac.MiniGames
{
    [Serializable]
    public class EarnCoinMiniGame : AbstractProgressableMiniGame
    {
        [SerializeField] public Text statsDPCText;
        [SerializeField] public Text statsDPSText;
        [SerializeField] public Text revenueText;
        [SerializeField] public Text revenueMedalsText;

        [SerializeField] public float incomePeriod;
        [SerializeField] public ulong incomeValue;

        public override ulong PrizeValue
        {
            get
            {
                return m_AbilityIncreaseProfit != null ? m_AbilityIncreaseProfit.ApplyToValue(base.PrizeValue) : base.PrizeValue;
            }
        }

        private Ability m_AbilityFindMedalsValue;
        private Ability m_AbilityFindMedalsChance;
        private Ability m_AbilityJackpotChance;
        private Ability m_AbilityJackpotMultiplier;
        private Ability m_AbilityIncreaseProfit;

        private ulong m_CachedActionValue;

        protected override void Awake()
        {
            base.Awake();
            StartIncomeUpdate();
        }

        protected override void Start()
        {
            base.Start();

            m_AbilityFindMedalsValue = Game.Player.Abilities.GetAbility(AbilityType.EarnCoinsFindMedalValue);
            m_AbilityFindMedalsChance = Game.Player.Abilities.GetAbility(AbilityType.EarnCoinsFindMedalChance);
            m_AbilityJackpotChance = Game.Player.Abilities.GetAbility(AbilityType.EarnCoinsChance);
            m_AbilityJackpotMultiplier = Game.Player.Abilities.GetAbility(AbilityType.EarnCoinsChanceMultiplie);
            m_AbilityIncreaseProfit = Game.Player.Abilities.GetAbility(AbilityType.EarnCoinsIncreaseProfit);
            
            Game.Player.Event_AnyChange += UpdateAll;
            Game.Player.Abilities.Event_AnyAbilityChange += (abl) => { UpdateUI(); };

            LocalizationManager.onChange += UpdateAll;

            UpdateAll();
        }

        public override void GivePrize(ulong value)
        {
            GiveExperience(ExperiencePrizeValue);
            GiveLuckyMedal(m_AbilityFindMedalsValue.Value);
            GiveEarnMoney(value);
        }

        public override void Action()
        {
            Progress.Value += m_CachedActionValue;
        }

        protected override void UpdateUI()
        {
            revenueText.text = Game.NumberFormatterSpace.Format(PrizeValue);
            
            if (m_AbilityFindMedalsChance != null && m_AbilityFindMedalsValue != null)
            {
                revenueMedalsText.text = string.Format("{0} [{1} {2}%]",
                    Game.NumberFormatterSpace.Format(m_AbilityFindMedalsValue.Value),
                    GetLocalization("let_chance"),
                    m_AbilityFindMedalsChance.Value);
            }
        }

        protected void StartIncomeUpdate()
        {
            UpdateManager.StartRepeate(incomePeriod, IncomeAction);
        }

        protected virtual void IncomeAction()
        {
            Progress.Value += incomeValue;
        }

        protected void OnGiveMoney(ulong value)
        {
        }

        protected void OnGiveMedal(ulong value)
        {
        }

        private void GiveEarnMoney(ulong value)
        {
            value += TryGetJackpot(value);

            Player.Money.Increase(value);
            OnGiveMoney(value);
        }

        protected void UpdateAll()
        {
            RecalculateActionValue();
            UpdateUI();
        }

        private void OnAnyAbilityChange(Ability ability, ulong value)
        {
            UpdateAll();
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }

        private void RecalculateActionValue()
        {
            m_CachedActionValue = actionValue + (ulong)Math.Log(Game.Player.GetDamageWithoutRandomModifiers(), 3);
        }

        private ulong TryGetJackpot(ulong value)
        {
            if (!TryChance(m_AbilityJackpotChance)) return 0;
            return m_AbilityJackpotMultiplier.Value * value;
        }

        private void GiveLuckyMedal(ulong value)
        {
            if (TryChance(m_AbilityFindMedalsChance))
            {
                Player.Medals.Increase(value);
                OnGiveMedal(value);
            }
        }

        private bool TryChance(Ability ability)
        {
            var offset = ability.Value > 100
                ? ability.Value * 10
                : 100;

            var chance = (ulong)UnityEngine.Random.Range(0, offset);

            return ability.Value >= chance;
        }
    }
}
