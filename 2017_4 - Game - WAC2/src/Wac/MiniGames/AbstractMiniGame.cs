﻿using System;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Common;
using IncEngine.Personal;
using IncEngine.Localization;
using IncEngine;

namespace Wac.MiniGames
{
    [Serializable, DisallowMultipleComponent, RequireComponent(typeof(Image))]
    public abstract class AbstractMiniGame : MonoBehaviour
    {
        [SerializeField] private IncrementalProgress m_Progress;

        public IncrementalProgress Progress { get { return m_Progress; } }
        public Player Player { get; protected set; }

        [SerializeField] public ulong actionValue;
        [SerializeField] public ulong ExperiencePrizeValue;
        [SerializeField] protected ulong m_BasePrizeValue;
        public virtual ulong PrizeValue { get { return m_BasePrizeValue; } }
        
        protected virtual void Awake()
        {
            
        }

        protected virtual void Start()
        {
            StartListenProgressEvents();
            UpdateUI();

            LocalizationManager.onChange += UpdateUI;
        }
        
        /// <summary>
        /// Set new player for this game
        /// </summary>
        /// <param name="player"></param>
        public void SetPlayer(Player player)
        {
            /// Remove event listeners from current plaeyr
            if (Player != null) OnRemovePlayer(Player);

            /// Initialize new player
            Player = player;
            OnSetPlayer(Player);
        }

        /// <summary>
        /// Give prize to player
        /// </summary>
        public abstract void GivePrize(ulong value);

        /// <summary>
        /// Action for next state
        /// </summary>
        public virtual void Action()
        {
            Progress.Value += actionValue;
        }

        protected abstract void UpdateUI();

        /// <summary>
        /// Listen progress end stage
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="value"></param>
        protected void ProgressEnd(IncrementalProgress progress, ulong value)
        {
            if (Player == null) return;
            GivePrize(PrizeValue);
        }
        
        /// <summary>
        /// On progress change value event
        /// </summary>
        /// <param name="value"></param>
        protected virtual void OnProgressTick(IncrementalProgress sender, ulong value)
        {
        }

        protected virtual void OnSetPlayer(Player player)
        {
            
        }

        protected virtual void OnRemovePlayer(Player player)
        {
            
        }

        /// <summary>
        /// Start listen progress events
        /// </summary>
        private void StartListenProgressEvents()
        {
            Progress.End  += ProgressEnd;
            Progress.Tick += OnProgressTick;
        }
    }
}
