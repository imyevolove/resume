﻿using IncEngine;
using IncEngine.Abilities;
using IncEngine.Dice;
using UnityEngine;
using Wac.UI.Popups;

namespace Wac.MiniGames
{
    public class EarnCaseMiniGame : AbstractProgressableMiniGame
    {
        private Ability CaseFindAbility;
        private ulong m_CachedActionValue;

        public override void GivePrize(ulong value)
        {
            GiveExperience(ExperiencePrizeValue);

            var priceLimit = Game.Player.Statistics.MoneyCollected.Value;
            priceLimit /= (ulong)Mathf.Log(priceLimit);

            var item = GameSession.Current.GetSingleton<CaseOpener>().GetRandomSkinForInventory(
                EntitiesManager.Entities.Skins, priceLimit);
            
            var popup = PopupManager.Get<DropExplorerPopup>(PopupId.DropExplorer);
            popup.Explore(item, Player);
        }

        protected override void UpdateUI()
        {
            
        }

        protected override void Start()
        {
            base.Start();

            CaseFindAbility = Game.Player.Abilities.GetAbility(AbilityType.FindCaseSpeedUp);
            CaseFindAbility.Event_Modify += OnAbilityModify;

            RecalculateActionValue();
        }

        public override void Action()
        {
            Progress.Value += m_CachedActionValue;
        }

        private void OnAbilityModify(Ability ability, ulong value)
        {
            RecalculateActionValue();
        }

        private void RecalculateActionValue()
        {
            m_CachedActionValue = CaseFindAbility.ApplyToValue(actionValue);
            }
    }
}
