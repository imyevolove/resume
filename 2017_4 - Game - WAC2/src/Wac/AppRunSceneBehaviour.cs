﻿using IncEngine;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wac
{
    public class AppRunSceneBehaviour : MonoBehaviour
    {
        protected virtual void Start()
        {
            var activeScene = SceneManager.GetActiveScene();
            var loader = new SceneBundleLoader();
            
            loader.AddScene("scn_Startup", LoadSceneMode.Additive, () => 
            {
                Debug.Log("Startup scene loaded");
            });
            
            loader.Load(() => 
            {
                SceneManager.UnloadSceneAsync(activeScene);
            });
        }
    }
}
