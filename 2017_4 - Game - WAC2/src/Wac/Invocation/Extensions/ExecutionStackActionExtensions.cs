﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Invocation
{
    public static class ExecutionStackActionExtensions
    {
        public static Action<ExecutionNextAction> ToExecutionStackAction(this Action action)
        {
            return (next) => 
            {
                action();
                next();
            };
        }
    }
}
