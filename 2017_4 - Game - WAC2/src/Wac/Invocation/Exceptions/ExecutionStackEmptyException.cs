﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Invocation
{
    public class ExecutionStackEmptyException : Exception
    {
        public ExecutionStackEmptyException()
            : base("Execution stack empty")
        {
        }

        public ExecutionStackEmptyException(string message)
            : base(message)
        {
        }
    }
}
