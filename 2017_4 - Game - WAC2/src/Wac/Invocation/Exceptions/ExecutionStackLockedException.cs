﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Invocation
{
    public class ExecutionStackLockedException : Exception
    {
        public ExecutionStackLockedException()
            : base("Execution stack locked")
        {
        }

        public ExecutionStackLockedException(string message)
            : base(message)
        {
        }
    }
}
