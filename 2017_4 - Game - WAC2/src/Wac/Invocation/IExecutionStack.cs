﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Invocation
{
    public interface IExecutionStack
    {
        ExecutionStackState State { get; }

        IExecutionStack Next(Action<ExecutionNextAction> action);
        void Run(Action endCallback);
        void Clear();
    }
}
