﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wac.Invocation
{
    public class ExecutionStack : IExecutionStack
    {
        public ExecutionStackState State { get; private set; }

        public bool IsLocked { get { return State != ExecutionStackState.Idle; } }

        private List<Action<ExecutionNextAction>> _actions 
            = new List<Action<ExecutionNextAction>>();

        public ExecutionStack()
        {
            State = ExecutionStackState.Idle;
        }

        public void Run(Action endCallback)
        {
            if (endCallback == null)
            {
                throw new ArgumentNullException("End callback can't be null");
            }

            if (IsLocked)
            {
                throw new ExecutionStackLockedException();
            }

            if (_actions.Count == 0)
            {
                throw new ExecutionStackEmptyException();
            }

            State = ExecutionStackState.Running;

            ExecuteNextAction(0, endCallback);
        }

        public IExecutionStack Next(Action<ExecutionNextAction> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("Action can't be null");
            }

            _actions.Add(action);
            return this;
        }

        public void Clear()
        {
            _actions.Clear();
        }

        private void EndExecution()
        {
            State = ExecutionStackState.Idle;
        }

        private void ExecuteNextAction(int index, Action endCallback)
        {
            if (index >= _actions.Count)
            {
                endCallback();
                EndExecution();

                return;
            }

            _actions[index].Invoke(() => {
                ExecuteNextAction(++index, endCallback);
            });
        }
    }
}
