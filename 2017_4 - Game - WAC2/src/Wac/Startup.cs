﻿using UnityEngine;
using Wac;
using Wac.Data;
using IncEngine;
using IncEngine.Abilities;
using IncEngine.Collections;
using IncEngine.Data;
using IncEngine.Equipment;
using IncEngine.Equipment.Entities;
using IncEngine.Persers;
using IncEngine.Personal;
using IncEngine.Science;
using IncEngine.Structures;
using IncEngine.Dice;
using IncEngine.Dice.Selectors;
using IncEngine.Deals;
using Wac.Play;
using IncEngine.Play;
using System;
using IncEngine.Localization;
using IncEngine.UpdateManagement;
using Wac.IAP;
using IncEngine.Formatters;
using IncEngine.IAPManagement;
using System.Linq;
using Uni;
using Uni.DependencyInjection;
using Wac.Services;
using System.Reflection;
using Wac.Collections;
using Wac.Resources;
using UnityEngine.SceneManagement;
using Wac.Online;
using Wac.Gifts;
using Wac.Invocation;
using Wac.Social;

/// <summary>
/// Initialize game properties and etc. here
/// </summary>
[Serializable]
[StartupOptions(IsMain = true)]
public class Startup : Uni.Startup
{
    [SerializeField] public GameDataProvider gameDataProvider;
    
    protected override void ConfigureServices(IServiceCollection services)
    {
        /// Setup sprite atlases
        services.AddSingleton<ISpriteProvider>(provider => {
            return new AtlasChainSpriteProviderBuilder()
            .Add(gameDataProvider.atlases)
            .Build();
        });

        /// Setup game resources
        services.AddSingleton<IResourcesProvider, ResourcesProvider>();

        ///
        services.AddSingleton<ITimeManager>((provider) => {
            return new TimeManager(
                "https://forbiten.com/api/server.time", 
                new ForbitenServerResponseDateTimeParser());
        });
        
        services.AddSingleton<DailyReward>((provider) => {
            var service = new DailyReward(
                provider.GetService<ITimeManager>(), 
                Game.Profile.CustomData,
                new RandomSkinGift(Game.CaseOpener, EntitiesManager.Cases));

            return service;
        });

        GameSession.Start<Game>();

        SetUpUnity();
        SetupFormatters();
        LoadLocalization();
        CreateProfile();
        MakeGameData();
        SetupMarket();
        SortCases();
        MakePlayer();
        SetupPlayer();
        MakeEquipments();
        MakeCaseOpener();
        MakeContracts();
        MakeMissions();
        InitIAPProducts();
        LoadGame();
        StartAutosave();
        SetupLocalization();
        SetupNewGame();
        CreateImmediatlySaveListeners();
    }

    protected void Configure(
        ITimeManager timeManager,
        DailyReward dailyReward)
    {
        var initializationStack = new ExecutionStack();

        /// Load time
        initializationStack.Next((next) =>
        {
            timeManager.Sync((state) => 
            {
                next();
            });
        });

        /// Load main scene
        initializationStack.Next((next) =>
        {
            var loader = new SceneBundleLoader();
            loader.AddScene("scn_Main", LoadSceneMode.Additive);
            loader.Load(() =>
            {
                next();
            });
        });

        /// Give daily gift
        initializationStack.Next((next) =>
        {
            dailyReward.GiveGift(Game.Player);
            Game.Save();

            next();
        });

        /// Run
        initializationStack.Run(() => 
        {
            Debug.Log("Initialization stack was ended");
        });
    }
    private void SetUpUnity()
    {
        Application.targetFrameRate = 80;
    }
    
    private void SetupFormatters()
    {
        var numberFormatterSpace = new NumberFormatterSpace();
        Game.NumberFormatterSpace = numberFormatterSpace;

        var numberFormatterPrefix = new NumberFormatterPrefix(NumberFormatterPrefix.DEFAULT_ABBREVIATION);
        Game.Session.AddSingleton(numberFormatterPrefix);

        /// Set default formatter
        Game.NumberFormatter = numberFormatterPrefix;
    }

    private void SetupNewGame()
    {
        if (!Game.IsNewGame) return;

        Game.Player.Money.SetValue(20);
        Game.Player.Medals.SetValue(50);
    }

    private void SetupMarket()
    {
        Game.Market = new Market(0.1f);
    }

    private void StartAutosave()
    {
        UpdateManager.StartRepeate(60, Game.Save);
    }

    private void InitIAPProducts()
    {
        var iapCollectionBuilder = new IAPCollectionBuilder();
        
        foreach (var bind in gameDataProvider.iapSingleProducts)
        {
            IProduct product = new IAPProduct(bind.sku, UnityEngine.Purchasing.ProductType.Consumable);

            IAPManager.AddProduct(product);
            iapCollectionBuilder.Add(IAPSingleFactory.Create(
                product,
                bind.rewardName, 
                bind.rewardCount,
                bind.rewardType,
                bind.rewardIcon,
                bind.rewardColor));
        }

        Game.IAPCollection.AddRange(iapCollectionBuilder.Build());
        IAPManager.Initialize();
    }

    private void MakeGameData()
    {
        IGameDataBuilder gameDataBuilder = new GameDataBuilder(
                    gameDataProvider.gunsDataTextAsset.text,
                    gameDataProvider.gunsDefaultDataTextAsset.text,
                    gameDataProvider.knivesDataTextAsset.text,
                    gameDataProvider.knivesDefaultDataTextAsset.text,
                    gameDataProvider.glovesDataTextAsset.text,
                    gameDataProvider.glovesDefaultDataTextAsset.text,
                    gameDataProvider.stickersDataTextAsset.text,
                    gameDataProvider.collectionsDataTextAsset.text,
                    gameDataProvider.casesDataTextAsset.text,
                    gameDataProvider.mapsDataTextAsset.text
                    );

        EntitiesManager.FetchFromGameData(gameDataBuilder.Build());
    }

    private void CreateProfile()
    {
        var profile = new Profile("rs_acc");
        Game.Profile = profile;
    }

    private void LoadGame()
    {
        Game.Load();
    }

    private void LoadLocalization()
    {
        LocalizationManager.LoadConfigFile("lang/cfg");
        LocalizationManager.LoadLocalizations();
    }

    private void SetupLocalization()
    {
        LocalizationManager.ApplyLocalizationLanguage(Game.Profile.settings.LanguageCode);
    }

    private void CreateImmediatlySaveListeners()
    {
        var currentGame = Game.Session;
        ///currentGame.Player.Inventory.Event_Change += currentGame.Save;
        ///currentGame.Player.Equipment.Event_AnyChange += currentGame.Save;
    }

    private void SortCases()
    {
        /// Sorting skins in cases
        foreach (var cs in EntitiesManager.Cases)
        {
            EntitiesManager.SortSkinsByQuality(cs.Skins);
        }
    }

    private void MakePlayer()
    {
        /// Initialize player
        IInventory inventory = new IncEngine.Collections.Inventory();
        AbilityMapper abilityMapper = new StableAbilityMapper();
        RankSystem rankSystem = new RankSystem(gameDataProvider.ranks);
        IEquipmentMapper equipmentSystem = new EquipmentMapper();
        TechMapper techMapper = TechMapperFactory.CreateTechMapperFromBinds<TechMapper>(gameDataProvider.techBinds, abilityMapper);

        var player = new Player(inventory, abilityMapper, rankSystem, equipmentSystem, techMapper, true);
        Game.Player = player;
    }

    private void SetupPlayer()
    {
        /// Setup player
        PlayerSetupDataParser playerSetupDataParser = new PlayerSetupDataParser();
        PlayerSetupData setupData = playerSetupDataParser.Parse(gameDataProvider.playerDataTextAsset.text);

        Game.Player.BaseDamage = setupData.baseDamage;
        Game.Player.BaseIncome = setupData.baseIncome;
    }

    private void MakeEquipments()
    {
        /// Guns
        foreach (var entity in EntitiesManager.Entities.GetAllOriginals(EntityType.Gun))
            Game.Player.Equipment.AddEntity(new Gun(entity));

        /// Knives
        foreach (var entity in EntitiesManager.Entities.GetAllOriginals(EntityType.Knive))
            Game.Player.Equipment.AddEntity(new Knife(entity));

        /// Gloves
        foreach (var entity in EntitiesManager.Entities.GetAllOriginals(EntityType.Glove))
            Game.Player.Equipment.AddEntity(new Glove(entity));
    }

    private void MakeCaseOpener()
    {
        ISkinSelector skinSelector = new SkinSelector(
            new LevelWeight[]
            {
                new LevelWeight() { Weight = 5,     Level = 1f },
                new LevelWeight() { Weight = 10,     Level = 0.8f },
                new LevelWeight() { Weight = 15,    Level = 0.5f },
                new LevelWeight() { Weight = 20,    Level = 0.2f },
                new LevelWeight() { Weight = 50,    Level = 0.1f }
            },
            new EntityWeight[]
            {
                new EntityWeight { Weight = 20,  EntityType = EntityType.Knive },
                new EntityWeight { Weight = 100, EntityType = EntityType.Glove },
                new EntityWeight { Weight = 200, EntityType = EntityType.Gun }
            }
            );
        ISkinQualitySelector SkinQulitySelector = new SkinQualitySelector();
        IExteriorQualitySelector exteriaorQualitySelector = new ExteriorQualitySelector();
        ISkinSpecialSelector skinSpecialSelector = new SkinSpecialSelector();

        var caseOpener = new CaseOpener(
            skinSelector,
            SkinQulitySelector,
            exteriaorQualitySelector,
            skinSpecialSelector
            );

        Game.CaseOpener = caseOpener;
    }

    private void MakeContracts()
    {
        var contractValidator = new ContractValidatorChainBuilder()
            .AddValidator(new ContractCollectionValidator())
            .AddValidator(new ContractEntityTypeValidator(ContractValidatorActionType.Include, EntityType.Gun))
            .AddValidator(new ContractSpecialValidator(ContractValidatorActionType.Include, SkinSpecial.SpecialStattrak))
            .AddValidator(new ContractQualityValidator(ContractValidatorActionType.Exclude,
                Quality.QualityCovert,
                Quality.QualityExceedinglyRare,
                Quality.QualityExtraordinary,
                Quality.QualityContraband
                ))
            .Build();

        var contract = new Contract(contractValidator, Game.CaseOpener, Game.Player)
        {
            maxContractItems = 10
        };
        GameSession.Current.AddSingleton(contract);
    }

    private void MakeMissions()
    {
        var enemiesTask = new TaskSigned<TaskSign>(
            gameDataProvider.missionDataMobs.IterationsCount,
            new MissionModeWave(ModeSignatures.Normal),
            TaskSign.DestroyEnemies,
            new BasicUnitHealthCalculator(),
            new BasicUnitRewardCalculator(),
            gameDataProvider.missionDataMobs.unitDetails);

        var bossBombTask = new TaskSigned<TaskSign>(
            gameDataProvider.missionDataBomb.IterationsCount,
            new MissionModeTime(ModeSignatures.Timer, 30),
            TaskSign.DefuseBomb,
            new BasicUnitHealthCalculator() { IsBoss = true },
            new BasicUnitRewardCalculator(),
            gameDataProvider.missionDataBomb.unitDetails);

        var bossHostageTask = new TaskSigned<TaskSign>(
            gameDataProvider.missionDataHostage.IterationsCount,
            new MissionModeTime(ModeSignatures.Timer, 30),
            TaskSign.SaveHostage,
            new BasicUnitHealthCalculator() { IsBoss = true },
            new BasicUnitRewardCalculator(),
            gameDataProvider.missionDataHostage.unitDetails);

        var bossTask = new TaskRand(bossBombTask, bossHostageTask);

        var missionSchedule = new ScheduleBuilder()
            .AddScheduleUnit(enemiesTask, 9)
            .AddScheduleUnit(bossTask, 1)
            .Build();
        var missionStage = new Stage(missionSchedule);
        var mission = new WacMission(missionStage);

        Game.Mission = mission;
    }
}
