﻿namespace Wac
{
    public enum GameWindow
    {
        Equipments,
        EarnPlace,
        Missions,
        Inventory,
        Shop,
        Science,
        Jackpot,
        Contract,
        Achievements,
        Settings,
        AboutUs
    }
}
