﻿namespace Wac
{
    public class AudioMusicVolumeControl : AudioVolumeControl
    {
        protected virtual void Awake()
        {
            SetTarget(Game.Profile.settings.VolumeMusic);
        }
    }
}
