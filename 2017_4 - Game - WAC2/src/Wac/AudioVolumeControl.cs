﻿using IncEngine;
using System;
using UnityEngine;

namespace Wac
{
    [Serializable, RequireComponent(typeof(AudioSource))]
    public abstract class AudioVolumeControl : MonoBehaviour
    {
        [HideInInspector] private AudioSource m_AudioSource;
        public AudioSource AudioSource
        {
            get
            {
                if (m_AudioSource == null)
                {
                    m_AudioSource = GetComponent<AudioSource>();
                }

                return m_AudioSource;
            }
        }

        public Volume Target { get; private set; }

        public void SetTarget(Volume target)
        {
            UnregisterVolumeEventListeners(Target);
            RegisterVolumeEventListeners(target);

            Target = target;

            UpdateVolume();
        }

        public void UpdateVolume()
        {
            if (Target == null) return;
            AudioSource.volume = Target.Value * 0.01f;
        }

        private void RegisterVolumeEventListeners(Volume volume)
        {
            if (volume == null) return;
            volume.Event_OnValueChange += VolumeChange;
        }

        private void UnregisterVolumeEventListeners(Volume volume)
        {
            if (volume == null) return;
            volume.Event_OnValueChange -= VolumeChange;
        }


        protected virtual void VolumeChange(int value)
        {
            SetVolume(value * 0.01f);
        }

        private void SetVolume(float value)
        {
            AudioSource.volume = value;
        }
    }
}
