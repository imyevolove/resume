﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.SocialPlatforms;

namespace Wac.Social
{
    public class SocialManager : ISocialManager
    {
        public ILocalUser User { get { return UnityEngine.Social.localUser; } }

        public event Action<bool> Event_UserAuthenticationComplete;
        
        public SocialManager()
        {
            ///GooglePlayGames.PlayGamesPlatform.Activate();
        }

        public void Authenticate()
        {
            Authenticate((status) => { });
        }

        public void Authenticate(Action<bool> callback)
        {
            User.Authenticate((status) =>
            {
                InvokeAuthenticationEvent(status);
                callback(status);
            });
        }

        private void InvokeAuthenticationEvent(bool status)
        {
            if (Event_UserAuthenticationComplete == null) return;
            Event_UserAuthenticationComplete(status);
        }
    }
}
