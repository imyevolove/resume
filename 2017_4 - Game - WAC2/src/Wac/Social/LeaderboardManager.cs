﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.SocialPlatforms;

namespace Wac.Social
{
    public class LeaderboardManager : ILeaderboardManager
    {
        /// <summary>
        ///  Leaderboards dictionary [ID/Leaderboard]
        /// </summary>
        private List<ILeaderboard> _leaderboards 
            = new List<ILeaderboard>();

        public void Add(ILeaderboard leaderboard)
        {
            _leaderboards.Add(leaderboard);
        }

        public void Add(IEnumerable<ILeaderboard> leaderboards)
        {
            _leaderboards.AddRange(leaderboards);
        }

        public ILeaderboard Get(string id)
        {
            return _leaderboards.Find(leaderboard => leaderboard.id == id);
        }
    }
}
