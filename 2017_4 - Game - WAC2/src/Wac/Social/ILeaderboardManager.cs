﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.SocialPlatforms;

namespace Wac.Social
{
    public interface ILeaderboardManager
    {
        void Add(ILeaderboard leaderboard);
        void Add(IEnumerable<ILeaderboard> leaderboards);
        ILeaderboard Get(string id);
    }
}
