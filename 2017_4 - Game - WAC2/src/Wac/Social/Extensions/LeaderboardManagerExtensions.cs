﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Uni.DependencyInjection;
using UnityEngine.SocialPlatforms;

namespace Wac.Social
{
    public static class LeaderboardManagerExtensions
    {
        public static IServiceCollection UseLeaderboards(this IServiceCollection services)
        {
            services.AddSingleton<ILeaderboardManager, LeaderboardManager>();
            return services;
        }

        public static IServiceCollection UseLeaderboards<T>(this IServiceCollection services)
            where T : ILeaderboardManager
        {
            services.AddSingleton(typeof(ILeaderboardManager), typeof(T));
            return services;
        }

        public static IServiceCollection UseLeaderboards(this IServiceCollection services, ILeaderboardManager manager)
        {
            services.AddSingleton<ILeaderboardManager>((provider) => { return manager; });
            return services;
        }

        public static IServiceCollection UseLeaderboards(this IServiceCollection services, ILeaderboardManager manager, params ILeaderboard[] leaderboards)
        {
            manager.Add(leaderboards);
            services.AddSingleton<ILeaderboardManager>((provider) => { return manager; });
            return services;
        }
    }
}
