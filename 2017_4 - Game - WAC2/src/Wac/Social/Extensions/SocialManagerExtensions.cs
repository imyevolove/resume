﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Uni.DependencyInjection;
using UnityEngine.SocialPlatforms;

namespace Wac.Social
{
    public static class SocialManagerExtensions
    {
        public static IServiceCollection UseSocial(this IServiceCollection services)
        {
            services.AddSingleton<ISocialManager, SocialManager>();
            return services;
        }

        public static IServiceCollection UseSocial<T>(this IServiceCollection services)
            where T : ISocialManager
        {
            services.AddSingleton(typeof(ISocialManager), typeof(T));
            return services;
        }

        public static IServiceCollection UseSocial(this IServiceCollection services, ISocialManager manager)
        {
            services.AddSingleton<ISocialManager>((provider) => { return manager; });
            return services;
        }
    }
}
