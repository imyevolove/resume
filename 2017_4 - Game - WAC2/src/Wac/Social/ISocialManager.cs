﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Social
{
    public interface ISocialManager
    {
        UnityEngine.SocialPlatforms.ILocalUser User { get; }
        event Action<bool> Event_UserAuthenticationComplete;

        void Authenticate();
        void Authenticate(Action<bool> callback);
    }
}
