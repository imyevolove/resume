﻿using IncEngine.Personal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wac.Gifts
{
    public interface IGiftAction
    {
        event Action<Gift> Event_GiftGiven;
        void GiveGift(Player player);
    }
}
