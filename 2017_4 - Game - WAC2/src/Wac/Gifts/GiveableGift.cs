﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncEngine.Personal;
using UnityEngine;

namespace Wac.Gifts
{
    public abstract class GiveableGift : Gift
    {
        public abstract Gift Give(Player player);
    }
}
