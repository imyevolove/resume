﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncEngine.Personal;
using IncEngine.Dice;
using IncEngine.Structures;
using UnityEngine;

namespace Wac.Gifts
{
    public class RandomSkinGift : GiveableGift
    {
        public CaseOpener CaseOpener { get; set; }
        public List<Case> AvailableCases { get; set; }

        public RandomSkinGift(CaseOpener caseOpener, List<Case> availableCases)
        {
            CaseOpener = caseOpener;
            AvailableCases = availableCases;
        }

        public override Gift Give(Player player)
        {
            var priceLimit = player.Statistics.MoneyCollected.Value;
            priceLimit /= (ulong)Mathf.Log(priceLimit);
            priceLimit = (ulong)Mathf.Clamp(priceLimit, 1, 20000);

            var item = CaseOpener.GetRandomSkinForInventory(GetRandomCase().Skins, priceLimit);
            player.Inventory.Add(item);

            return new Gift()
            {
                Id = Id,
                Name = item.FullName,
                Icon = item.Icon
            };
        }

        private Case GetRandomCase()
        {
            return AvailableCases[UnityEngine.Random.Range(0, AvailableCases.Count)];
        }
    }
}
