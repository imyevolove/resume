﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wac.Gifts
{
    public class Gift
    {
        public string Id    { get; set; }
        public string Name  { get; set; }
        public Sprite Icon  { get; set; }
    }
}
