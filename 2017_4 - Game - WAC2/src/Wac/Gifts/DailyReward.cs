﻿using IncEngine.Personal;
using IncEngine.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Wac.Online;

namespace Wac.Gifts
{
    public class DailyReward : IGiftAction
    {
        private const string StorageTimeKey = "daily_reward_dfg_date";
        private const string StorageGiftKey = "daily_reward_dfg_index";

        public event Action<Gift> Event_GiftGiven;

        public readonly GiveableGift[] availableGifts;

        private int _rewardIndex;
        public int RewardIndex
        {
            get { return _rewardIndex % availableGifts.Length; }
            protected set { _rewardIndex = value; }
        }

        private ITimeManager _timeManager;
        private IStorageContainer _storage;

        public DailyReward(
            ITimeManager timeManager, 
            IStorageContainer storage, 
            params GiveableGift[] gifts)
        {
            _timeManager = timeManager;
            _storage = storage;
            availableGifts = gifts;

            RewardIndex = ParseIndex(_storage, StorageGiftKey);
        }

        public void GiveGift(Player player)
        {
            if (!CanGiveGift()) return;

            var gift = availableGifts[RewardIndex];
            var givenReward = gift.Give(player);

            RewardIndex++;

            WriteGivenDate();

            if (Event_GiftGiven != null)
            {
                Event_GiftGiven(givenReward);
            }
        }

        public bool CanGiveGift()
        {
            if (!_timeManager.IsSynchronized) return false;

            var lastDate = GetLastDateReward();
            
            if (!lastDate.HasValue) return true;
            if (!ReadyToGiveReward(_timeManager.CurrentDate, lastDate.Value)) return false;
            
            return true;
        }

        private bool ReadyToGiveReward(DateTime currentTime, DateTime lastReward)
        {
            return currentTime.Year     >= lastReward.Year
                && currentTime.Month    >= lastReward.Month
                && currentTime.Day      > lastReward.Day;
        }

        private DateTime? GetLastDateReward()
        {
            if (!_storage.KeyValueIs<long>(StorageTimeKey)) return null;

            try
            {
                var rawDate = (long)_storage[StorageTimeKey];
                return DateTime.FromFileTime(rawDate);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private int ParseIndex(IStorageContainer storage, string key)
        {
            if (!storage.KeyValueIs<int>(key)) return 0;
            return (int)storage[key];
        }

        private void WriteGivenDate()
        {
            _storage[StorageTimeKey] = _timeManager.CurrentDate.ToFileTime();
            _storage[StorageGiftKey] = RewardIndex;
        }
    }
}
