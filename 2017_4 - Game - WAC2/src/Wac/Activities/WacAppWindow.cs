﻿using System;
using System.Collections.Generic;
using UnityEngine;
using IncEngine.Activities;

namespace Wac.Activities
{
    [Serializable]
    public class WacAppWindow : AbstractAppWindow<GameWindow>
    {
        /// <summary>
        /// Binds for initialize windows at runtime
        /// </summary>
        [SerializeField]
        public List<WacActivityWindowBind> windowBinds;

        void Awake()
        {
            MenuActivity.Hide();

            Game.AppWindow = this;

            /// Initialize windows
            foreach (var bind in windowBinds)
            {
                RegisterWindow(bind);

                MenuActivity.AddButton(bind.image, "window_name_" + bind.Window.windowName.ToLower(), () => {
                    if (IsMenuTransition) return;
                    ShowWindow(bind.Id);
                    CloseMenu();
                });
            }

            Game.AppWindow.ShowWindow(GameWindow.Equipments);
        }
    }
}
