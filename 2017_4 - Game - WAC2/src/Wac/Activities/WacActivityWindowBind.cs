﻿using System;
using UnityEngine;
using IncEngine.Activities;

namespace Wac.Activities
{
    [Serializable]
    public class WacActivityWindowBind : ActivityWindowBind<GameWindow>
    {
        [SerializeField] public Sprite image;
    }
}
