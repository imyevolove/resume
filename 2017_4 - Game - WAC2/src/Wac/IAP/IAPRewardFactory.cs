﻿using IncEngine;
using Wac.IAP;

namespace Assets.Scripts.Wac.IAP
{
    public class IAPRewardFactory
    {
        public static IAPReward CreateCoinsReward(ulong count)
        {
            return new IAPReward()
            {
                Count = count,
                Name = "Coins",
                Icon = IconsManager.CoinIcon,
                Type = IAPRewardType.Coins
            };
        }

        public static IAPReward CreateMedalsReward(ulong count)
        {
            return new IAPReward()
            {
                Count = count,
                Name = "Medals",
                Icon = IconsManager.MedalIcon,
                Type = IAPRewardType.Medals
            };
        }

        public static IAPReward CreateCasesReward(ulong count)
        {
            return new IAPReward()
            {
                Count = count,
                Name = "Cases",
                Icon = IconsManager.CaseIcon,
                Type = IAPRewardType.Cases
            };
        }

        public static IAPReward CreateReward(ulong count, IAPRewardType type)
        {
            IAPReward reward = null;

            switch (type)
            {
                case IAPRewardType.Coins:
                    reward = CreateCoinsReward(count);
                    break;

                case IAPRewardType.Medals:
                    reward = CreateMedalsReward(count);
                    break;

                case IAPRewardType.Cases:
                    reward = CreateCasesReward(count);
                    break;
            }

            return reward;
        }
    }
}
