﻿using Assets.Scripts.Wac.IAP;
using IncEngine.IAPManagement;
using UnityEngine;

namespace Wac.IAP
{
    public class IAPSingleFactory
    {
        public static IAPSingle Create(IProduct product, string name, ulong count, IAPRewardType rewardType, Sprite icon, Color color)
        {
            return new IAPSingle()
            {
                Name = name,
                Product = product,
                Color = color,
                Icon = icon,
                Reward = IAPRewardFactory.CreateReward(count, rewardType)
            };
        }

        public static IAPSingle CreateForCases(IProduct product, string name, ulong count, Sprite icon, Color color)
        {
            return new IAPSingle()
            {
                Name = name,
                Product = product,
                Color = color,
                Icon = icon,
                Reward = IAPRewardFactory.CreateCasesReward(count)
            };
        }

        public static IAPSingle CreateForCoins(IProduct product, string name, ulong count, Sprite icon, Color color)
        {
            return new IAPSingle()
            {
                Name = name,
                Product = product,
                Color = color,
                Icon = icon,
                Reward = IAPRewardFactory.CreateCoinsReward(count)
            };
        }

        public static IAPSingle CreateForMedals(IProduct product, string name, ulong count, Sprite icon, Color color)
        {
            return new IAPSingle()
            {
                Name = name,
                Product = product,
                Color = color,
                Icon = icon,
                Reward = IAPRewardFactory.CreateMedalsReward(count)
            };
        }
    }
}
