﻿namespace Wac.IAP
{
    public enum IAPRewardType
    {
        Coins,
        Medals,
        Cases
    }
}
