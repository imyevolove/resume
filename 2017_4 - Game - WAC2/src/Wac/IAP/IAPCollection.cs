﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Wac.IAP
{
    public class IAPCollection : IEnumerable<IAPDetails>
    {
        private List<IAPDetails> m_Items = new List<IAPDetails>();

        public int Count { get { return m_Items.Count; } }
        public List<IAPDetails> Items { get { return m_Items; } }

        public bool Add(IAPDetails item)
        {
            if (m_Items.Contains(item)) return false;

            m_Items.Add(item);

            return true;
        }

        public void AddRange(IEnumerable<IAPDetails> items)
        {
            if (items == null) return;

            foreach (var item in items)
            {
                Add(item);
            }
        }

        public bool Remove(IAPDetails item)
        {
            if (item == null) return false;
            return m_Items.Remove(item);
        }

        public bool Remove(int index)
        {
            if (index < 0 || index >= m_Items.Count) return false;
            return Remove(m_Items[index]);
        }

        public IEnumerator<IAPDetails> GetEnumerator()
        {
            return m_Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
