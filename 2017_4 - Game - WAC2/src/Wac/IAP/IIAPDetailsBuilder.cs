﻿using IncEngine.IAPManagement;
using UnityEngine;

namespace Wac.IAP
{
    public interface IIAPDetailsBuilder<T> where T : IAPDetails
    {
        IIAPDetailsBuilder<T> SetName(string name);
        IIAPDetailsBuilder<T> SetColor(Color color);
        IIAPDetailsBuilder<T> SetProduct(IProduct product);
        IIAPDetailsBuilder<T> SetIcon(Sprite icon);
        IAPDetails Build();
    }
}
