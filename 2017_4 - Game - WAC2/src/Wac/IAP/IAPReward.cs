﻿using UnityEngine;

namespace Wac.IAP
{
    public class IAPReward
    {
        public Sprite           Icon        { get; set; }
        public string           Name        { get; set; }
        public ulong            Count       { get; set; } 
        public IAPRewardType    Type        { get; set; }
    }
}
