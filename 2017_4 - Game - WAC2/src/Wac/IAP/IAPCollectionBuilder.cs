﻿namespace Wac.IAP
{
    public class IAPCollectionBuilder
    {
        private IAPCollection m_Collection;

        public IAPCollectionBuilder()
        {
            m_Collection = new IAPCollection();
        }

        public IAPCollectionBuilder Add(IAPDetails details)
        {
            m_Collection.Add(details);
            return this;
        }

        public IAPCollection Build()
        {
            return m_Collection;
        }
    }
}
