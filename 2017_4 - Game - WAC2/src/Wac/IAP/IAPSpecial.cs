﻿using System.Collections.Generic;
using UnityEngine;

namespace Wac.IAP
{
    public class IAPSpecial : IAPDetails
    {
        public Sprite           BackgroundImage { get; set; }
        public List<IAPReward>  Rewards         { get; set; }
    }
}
