﻿using System.Collections.Generic;
using UnityEngine;

namespace Wac.IAP.Rewards
{
    public class RewardsExecuteUtility
    {
        private static Dictionary<IAPRewardType, IRewardExecutor> m_Handlers
            = new Dictionary<IAPRewardType, IRewardExecutor>()
            {
                { IAPRewardType.Coins,  new RewardExecutorCoins()   },
                { IAPRewardType.Medals, new RewardExecutorMedals()  },
                { IAPRewardType.Cases,  new RewardExecutorCases()   }
            };

        public static void SetExectorHandler(IAPRewardType type, IRewardExecutor executor)
        {
            if (executor == null) return;

            if (m_Handlers.ContainsKey(type))
            {
                m_Handlers.Remove(type);
                Debug.LogFormat("Reward executor '{0}' handler was redefined", type.ToString());
            }

            m_Handlers.Add(type, executor);
        }

        public static void ExecuteReward(IAPReward reward)
        {
            if (!m_Handlers.ContainsKey(reward.Type))
            {
                Debug.LogWarningFormat("Reward is not executed. Reward Executor for reward type '{0}' not found", reward.Type.ToString());
                return;
            }

            m_Handlers[reward.Type].Execute(reward);
        }
    }
}
