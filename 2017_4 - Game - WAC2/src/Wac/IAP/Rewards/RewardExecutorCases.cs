﻿using IncEngine;
using System;
using System.Linq;
using UnityEngine;
using Wac.UI.Popups;

namespace Wac.IAP.Rewards
{
    public class RewardExecutorCases : IRewardExecutor
    {
        public void Execute(IAPReward reward)
        {
            var priceLimit = Game.Player.Statistics.MoneyCollected.Value;
            priceLimit /= (ulong)Mathf.Log(priceLimit);

            var items = Game.CaseOpener.GetRandomSkinsForInventory(EntitiesManager.Entities.Skins, priceLimit, reward.Count);

            foreach (var item in items)
            {
                Game.Player.Inventory.Add(item);
            }

            Game.Profile.AdsState.Value = false;

            var popup = PopupManager.Get<ItemsDropPopup>(PopupId.ItemsDrop);
            popup.Activate(items.Select(inv => inv.Skin).ToArray());
        }
    }
}
