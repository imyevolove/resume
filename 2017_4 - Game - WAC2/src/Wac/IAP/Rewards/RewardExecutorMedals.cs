﻿namespace Wac.IAP.Rewards
{
    public class RewardExecutorMedals : IRewardExecutor
    {
        public void Execute(IAPReward reward)
        {
            Game.Player.Medals.Increase(reward.Count);
            Game.Profile.AdsState.Value = false;
        }
    }
}
