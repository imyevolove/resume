﻿namespace Wac.IAP.Rewards
{
    public interface IRewardExecutor
    {
        void Execute(IAPReward reward);
    }
}
