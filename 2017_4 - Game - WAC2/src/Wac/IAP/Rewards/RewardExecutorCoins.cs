﻿namespace Wac.IAP.Rewards
{
    public class RewardExecutorCoins : IRewardExecutor
    {
        public void Execute(IAPReward reward)
        {
            Game.Player.Money.Increase(reward.Count);
            Game.Profile.AdsState.Value = false;
        }
    }
}
