﻿using IncEngine.IAPManagement;
using System.Collections.Generic;
using UnityEngine;

namespace Wac.IAP
{
    public class IAPSpecialBuilder : IIAPDetailsBuilder<IAPSpecial>
    {
        private IAPSpecial m_IAPItem;

        public IAPSpecialBuilder()
        {
            m_IAPItem = new IAPSpecial();
        }

        public IAPDetails Build()
        {
            return m_IAPItem;
        }

        public IAPSpecialBuilder AddReward(IAPReward reward)
        {
            m_IAPItem.Rewards.Add(reward);
            return this;
        }

        public IAPSpecialBuilder AddRewards(IEnumerable<IAPReward> rewards)
        {
            m_IAPItem.Rewards.AddRange(rewards);
            return this;
        }

        public IIAPDetailsBuilder<IAPSpecial> SetColor(Color color)
        {
            m_IAPItem.Color = color;
            return this;
        }

        public IIAPDetailsBuilder<IAPSpecial> SetIcon(Sprite icon)
        {
            m_IAPItem.Icon = icon;
            return this;
        }

        public IIAPDetailsBuilder<IAPSpecial> SetName(string name)
        {
            m_IAPItem.Name = name;
            return this;
        }

        public IIAPDetailsBuilder<IAPSpecial> SetProduct(IProduct product)
        {
            m_IAPItem.Product = product;
            return this;
        }
    }
}
