﻿using IncEngine.IAPManagement;
using UnityEngine;

namespace Wac.IAP
{
    public class IAPSingleBuilder : IIAPDetailsBuilder<IAPSingle>
    {
        private IAPSingle m_IAPItem;

        public IAPSingleBuilder()
        {
            m_IAPItem = new IAPSingle();
        }

        public IAPDetails Build()
        {
            return m_IAPItem;
        }

        public IAPSingleBuilder SetReward(IAPReward reward)
        {
            m_IAPItem.Reward = reward;
            return this;
        }

        public IIAPDetailsBuilder<IAPSingle> SetColor(Color color)
        {
            m_IAPItem.Color = color;
            return this;
        }

        public IIAPDetailsBuilder<IAPSingle> SetIcon(Sprite icon)
        {
            m_IAPItem.Icon = icon;
            return this;
        }

        public IIAPDetailsBuilder<IAPSingle> SetName(string name)
        {
            m_IAPItem.Name = name;
            return this;
        }

        public IIAPDetailsBuilder<IAPSingle> SetProduct(IProduct product)
        {
            m_IAPItem.Product = product;
            return this;
        }
    }
}
