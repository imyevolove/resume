﻿using IncEngine.IAPManagement;
using UnityEngine;

namespace Wac.IAP
{
    public class IAPDetails
    {
        public IProduct Product     { get; set; }
        public string   Name        { get; set; }
        public Sprite   Icon        { get; set; }
        public Color    Color       { get; set; }
    }
}
