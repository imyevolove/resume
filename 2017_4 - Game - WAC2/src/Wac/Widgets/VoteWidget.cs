﻿using IncEngine;
using IncEngine.UpdateManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Wac.Widgets
{
    [Serializable]
    public class VoteWidget : MonoBehaviour
    {
        [SerializeField] public int timeout;
        [SerializeField] public ulong reward;
        [SerializeField] public Text rewardText;
        [SerializeField] public Button closeButton;
        [SerializeField] public Button voteButton;
        public bool IsActivated { get; private set; }

        private Coroutine m_ShowUpTimeoutCoroutine;
        private string VoteUrl { get; set; }

        private SmartValue<bool> m_StorageValue;

        protected virtual void Awake()
        {
            m_StorageValue = Game.Profile.VoteWidgetState;
            
            Configure();
            ConfigureActions();
            Hide();

            Activate();

            m_StorageValue.Event_OnValueChange += WidgetHandler;
            WidgetHandler(m_StorageValue.Value);
        }

        protected virtual void Start()
        {
            Redraw();
        }

        public void Activate()
        {
            IsActivated = true;

            RunShowUpTimeout();
        }

        public void Deactivate()
        {
            IsActivated = false;

            StopShowUpTimeout();
        }

        public void Show()
        {
            if (!IsActivated)
            {
                Debug.Log("Widget deactivated");
                return;
            }

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        protected void RunShowUpTimeout()
        {
            if (!IsActivated) return;

            StopShowUpTimeout();

            m_ShowUpTimeoutCoroutine = UpdateManager.DelayedExecution(ShowAndDeactivate, timeout);
        }

        protected void StopShowUpTimeout()
        {
            if (m_ShowUpTimeoutCoroutine == null) return;

            UpdateManager.StopCoroutine(m_ShowUpTimeoutCoroutine);

            m_ShowUpTimeoutCoroutine = null;
        }
        
        private void OpenVotePage()
        {
            Application.OpenURL(VoteUrl);
        }

        private void GiveReward()
        {
            Game.Player.Medals.Increase(reward);
        }

        private void VoteAction()
        {
            OpenVotePage();

            GiveReward();

            CloseAction();
        }

        private void CloseAction()
        {
            m_StorageValue.Value = false;

            Game.Save();

            HideAndDeactivate();
        }

        public virtual void Redraw()
        {
            rewardText.text = reward.ToString();
        }

        private void Configure()
        {
#if UNITY_ANDROID
            VoteUrl = "https://play.google.com/store/apps/details?id=com.Forbiten.WAC2X";
#elif NETFX_CORE
            VoteUrl = "https://www.microsoft.com/ru-ru/store/p/weapons-and-cases-2/9nqsr9fkpvs1";
#else
            VoteUrl = "https://vk.com/club104689708";
#endif
        }

        private void ConfigureActions()
        {
            closeButton.onClick.AddListener(CloseAction);
            voteButton.onClick.AddListener(VoteAction);
        }

        private void ShowAndDeactivate()
        {
            Show();
            Deactivate();
        }

        private void HideAndDeactivate()
        {
            Hide();
            Deactivate();
        }

        private void WidgetHandler(bool state)
        {
            if (!state)
            {
                Deactivate();
            }
        }
    }
}
