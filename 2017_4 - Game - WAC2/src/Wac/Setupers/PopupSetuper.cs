﻿using System;
using UnityEngine;
using Wac.Models;

namespace Wac.Setupers
{
    [Serializable]
    public class PopupSetuper : Setuper
    {
        [SerializeField]
        public Transform parentTransform;

        [SerializeField]
        public PopupBind[] binds;

        protected override void Setup()
        {
            foreach (var bind in binds)
            {
                var popup = Instantiate(bind.popup, parentTransform, false);
                    popup.Hide();

                PopupManager.Add(bind.id, popup);
            }
        }
    }
}
