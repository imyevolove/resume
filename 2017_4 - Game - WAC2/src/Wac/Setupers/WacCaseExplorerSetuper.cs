﻿using UnityEngine;
using Wac.UI.Popups;

namespace Wac.Setupers
{
    [RequireComponent(typeof(CaseExplorerPopup))]
    public class WacCaseExplorerSetuper : Setuper
    {
        protected override void Setup()
        {
            GetComponent<CaseExplorerPopup>().SetPlayer(Game.Player);
        }
    }
}
