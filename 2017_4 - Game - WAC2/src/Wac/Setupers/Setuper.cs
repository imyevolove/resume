﻿using UnityEngine;

namespace Wac.Setupers
{
    public abstract class Setuper : MonoBehaviour
    {
        protected abstract void Setup();

        private void Awake()
        {
            Setup();
        }
    }
}
