﻿namespace IncEngine
{
    public interface IPool<T> where T : IPoolable
    {
        T GetFree();
        T CreateEntity();
    }
}
