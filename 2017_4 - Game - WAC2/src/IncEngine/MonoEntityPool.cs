﻿using UnityEngine;

namespace IncEngine
{
    public class MonoEntityPool<T> : AbstractPool<T> where T : MonoBehaviour, IPoolable
    {
        private T m_Original;

        public MonoEntityPool(T original)
        {
            m_Original = original;
        }

        public void Prebuild(int count)
        {
            for (var i = 0; i < count; i++)
            {
                CreateAndRegisterEntity();
            }
        }

        public void Prebuild(int count, Transform parent, bool worldPositionStays = false)
        {
            for (var i = 0; i < count; i++)
            {
                var entity = CreateAndRegisterEntity();
                entity.transform.SetParent(parent, worldPositionStays);
            }
        }

        public T GetFree(Transform parent, bool worldPositionStays = false)
        {
            T free = GetFree();
            free.transform.SetParent(parent, worldPositionStays);
            return free;
        }

        public override T CreateEntity()
        {
            return UnityEngine.Object.Instantiate(m_Original);
        }
    }
}
