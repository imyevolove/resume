﻿using System;

namespace IncEngine
{
    public class SmartValue<T> where T : struct
    {
        public event Action<T> Event_OnValueChange;

        private T m_Value;
        public T Value
        {
            get { return m_Value; }
            set
            {
                m_Value = value;
                OnValueChange(value);
            }
        }

        public SmartValue()
        {
        }

        public SmartValue(T value)
        {
            Value = value;
        }

        private void OnValueChange(T value)
        {
            if (Event_OnValueChange != null)
            {
                Event_OnValueChange(value);
            }
        }
    }
}
