﻿using System;

namespace IncEngine
{
    public class StoreValue
    {
        public event Action<ulong> Event_Change;
        public event Action<ulong> Event_Increase;
        public event Action<ulong> Event_Decrease;

        public ulong Value { get; private set; }

        public void Increase(ulong value)
        {
            try
            {
                Value = checked(Value + value);
            }
            catch (Exception)
            {
                Value = ulong.MaxValue;
            }

            CallIncreaseEvent(value);
            CallChangeEvent(Value);
        }

        public void Decrease(ulong value)
        {
            try
            {
                Value = checked(Value - value);
            }
            catch (Exception)
            {
                Value = 0;
            }

            CallDecreaseEvent(value);
            CallChangeEvent(Value);
        }

        public void SetValue(ulong value)
        {
            Value = value;
            CallChangeEvent(Value);
        }

        protected void CallChangeEvent(ulong value)
        {
            if (Event_Change != null)
            {
                Event_Change(value);
            }
        }

        protected void CallIncreaseEvent(ulong value)
        {
            if (Event_Increase != null)
            {
                Event_Increase(value);
            }
        }

        protected void CallDecreaseEvent(ulong value)
        {
            if (Event_Decrease != null)
            {
                Event_Decrease(value);
            }
        }
    }
}
