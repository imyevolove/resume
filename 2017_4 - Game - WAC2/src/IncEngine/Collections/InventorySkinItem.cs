﻿using IncEngine.Collections.Context;
using IncEngine.Structures;
using UnityEngine;
using IncEngine.Collections.Context.Commands;
using IncEngine.Structures.Interfaces;
using IncEngine.Storage;

namespace IncEngine.Collections
{
    public class InventorySkinItem : InventoryItem, IInteractiveEntity
    {
        public ExteriorQuality      Exterior        { get; protected set; }
        public Skin                 Skin            { get; protected set; }
        public SkinSpecial          Special         { get; protected set; }

        public override string      FullName        { get { return Skin.FullName; } }
        public override string      CategoryName    { get { return Exterior.Name; } }
        public override string      LocalizedCategoryName { get { return ExteriorQuality.GetLocalizationForExterior(Exterior); } }
        public override string      Name            { get { return Skin.Name; } }
        public override Sprite      Icon            { get { return Skin.Icon; } }
        public override Color       Color           { get { return Skin.Quality.Color; } }
        public          EntityType  EntityType      { get { return Skin.Type; } }
        public override IQuality    Quality         { get { return Skin.Quality; } }

        public override AbstractEntityDetails Target { get { return Skin; } }

        public override ulong       BaseDamage { get { return Skin.GetBaseDamage(Exterior, Special); } }
        public override ulong       BaseIncome { get { return Skin.GetBaseIncome(Exterior, Special); } }

        public override InventoryContextMenu ContextMenu { get; protected set; }

        public InventorySkinItem()
        {
            CreateContextMenu();
        }

        public InventorySkinItem(Skin skin, ExteriorQuality exterior, SkinSpecial special)
        {
            Skin        = skin;
            Exterior    = exterior;
            Special     = special;

            CreateContextMenu();
        }

        public override ulong GetPrice()
        {
            return Skin.GetPrice(Exterior, Special);
        }

        public override InventoryItem Copy()
        {
            return new InventorySkinItem(Skin, Exterior, Special);
        }

        public override StorageDataContainer GetStorageData()
        {
            var storageData = base.GetStorageData();

            storageData["exterior"] = Exterior.Name;
            storageData["special"] = Special == null ? "" : Special.Name;

            return storageData;
        }

        public override bool SetStorageData(StorageDataContainer storageData)
        {
            if (
                storageData == null 
                || !storageData.KeyValueIs<string>("target_id")
                || !storageData.KeyValueIs<string>("exterior")
                || !storageData.KeyValueIs<string>("special")
                ) return false;

            var skin = EntitiesManager.Entities.GetSkin((string)storageData["target_id"]);
            var exterior = ExteriorQuality.Parse((string)storageData["exterior"]);
            var special = SkinSpecial.Parse((string)storageData["special"]);

            if (skin == null || exterior == null) return false;

            Skin = skin;
            Exterior = exterior;
            Special = special;

            return true;
        }

        public override InventoryItem Create(StorageDataContainer container)
        {
            InventorySkinItem item = null;

            if (
                container != null 
                && container.KeyValueIs<string>("target_id")
                && container.KeyValueIs<string>("exterior")
                && container.KeyValueIs<string>("special")
                )
            {
                var skin = EntitiesManager.Entities.GetSkin((string)container["target_id"]);
                var exterior = ExteriorQuality.Parse((string)container["exterior"]);
                var special = SkinSpecial.Parse((string)container["special"]);

                if (skin != null && exterior != null)
                {
                    item = new InventorySkinItem(skin, exterior, special);
                }
            }

            return item;
        }

        private void CreateContextMenu()
        {
            ContextMenu = new InventoryContextMenuBuilder()
                .AddContextMenuItemData(new ContextMenuItemData("equip",
                new EquipCommand(this), new Color32(0, 0, 0, 60)))
                .AddContextMenuItemData(new ContextMenuItemData("sell",
                new SellCommand(this), new Color32(255, 215, 68, 255), IconsManager.CoinIcon))
                .Build();
        }
    }
}