﻿using UnityEngine;

namespace IncEngine.Collections
{
    using IncEngine.Collections.Context;
    using Structures;
    using IncEngine.Storage;

    public class InventoryStickerItem : InventoryItem
    {
        public const string CATEGORY_NAME = "Sticker";

        public Sticker              Sticker         { get; protected set; }

        public override string      FullName        { get { return Sticker.Name; } }
        public override string      CategoryName    { get { return CATEGORY_NAME; } }
        public override string      LocalizedCategoryName  { get { return CategoryName; } }
        public override string      Name            { get { return Sticker.Name; } }
        public override Sprite      Icon            { get { return Sticker.Icon; } }
        public override Color       Color           { get { return Sticker.Quality.Color; } }
        public override IQuality    Quality         { get { return Sticker.Quality; } }

        public override ulong       BaseDamage      { get { return 0; } }
        public override ulong       BaseIncome      { get { return 0; } }

        public override AbstractEntityDetails Target { get { return Sticker; } }

        public override InventoryContextMenu ContextMenu { get; protected set; }

        public InventoryStickerItem()
        {

        }

        public InventoryStickerItem(Sticker sticker)
        {
            Sticker = sticker;
        }

        public override ulong GetPrice()
        {
            return Sticker.Price;
        }

        public override InventoryItem Copy()
        {
            return new InventoryStickerItem(Sticker);
        }

        public override InventoryItem Create(StorageDataContainer container)
        {
            InventoryStickerItem item = null;

            if (
                container != null
                && container.KeyValueIs<string>("target_id")
                && container.KeyValueIs<string>("exterior")
                && container.KeyValueIs<string>("special")
                )
            {
                var sticker = EntitiesManager.FindSticker((string)container["target_id"]);

                if (sticker != null)
                {
                    item = new InventoryStickerItem(sticker);
                }
            }

            return item;
        }

        public override bool SetStorageData(StorageDataContainer storageData)
        {
            if (
               storageData == null
               || !storageData.KeyValueIs<string>("target_id")
               ) return false;

            var sticker = EntitiesManager.FindSticker((string)storageData["target_id"]);

            if (sticker == null) return false;

            Sticker = sticker;

            return true;
        }
    }
}
