﻿using IncEngine.Storage;
using System;
using System.Collections.Generic;

namespace IncEngine.Collections
{
    public class Inventory : IInventory, IStorage
    {
        public event Action<InventoryItem> Event_ItemAdd;
        public event Action<InventoryItem> Event_ItemRemove;
        public event Action Event_Change;

        private List<InventoryItem> m_Items = new List<InventoryItem>();

        /// <summary>
        /// Warning. Events do not work when modify items.
        /// </summary>
        public List<InventoryItem> Items { get { return m_Items; } }


        public InventoryItem Find(int index)
        {
            if (index < 0 || index >= m_Items.Count) return null;
            return m_Items[index];
        }

        public string GetItemId(InventoryItem item)
        {
            if (!Contains(item)) return "undefined_id";
            return "i" + FindIndex(item);
        }

        /// <summary>
        /// Add item and return adding status
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Add(InventoryItem item)
        {
            /// Item already exists and can't added second time
            if (item.ParentInventory == this || Contains(item))
            {
                return false;
            }

            m_Items.Add(item);

            if (item.AttachTo(this))
            {
                OnAddItem(item);
                OnChangeItem();

                return true;
            }
            
            return false;
        }

        /// <summary>
        /// Return contains status
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(InventoryItem item)
        {
            return m_Items.Contains(item);
        }

        /// <summary>
        /// return item index in list 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int FindIndex(InventoryItem item)
        {
            return m_Items.FindIndex(i => i == item);
        }

        /// <summary>
        /// Remove item by index and return status
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool Remove(int index)
        {
            return Remove(m_Items[index]);
        }

        /// <summary>
        /// Remove item and return status
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool Remove(InventoryItem item)
        {
            var removed = m_Items.Remove(item);

            if (removed)
            {
                item.DeattachFromParentInventory();

                OnRemoveItem(item);
                OnChangeItem();
            }

            return removed;
        }

        /// <summary>
        /// Remove items range
        /// </summary>
        /// <param name="items"></param>
        public void RemoveRange(IEnumerable<InventoryItem> items)
        {
            foreach (var item in items)
            {
                Remove(item);
            }
        }

        protected virtual void OnAddItem(InventoryItem item)
        {
            if (Event_ItemAdd != null)
            {
                Event_ItemAdd(item);
            }
        }

        protected virtual void OnRemoveItem(InventoryItem item)
        {
            if (Event_ItemRemove != null)
            {
                Event_ItemRemove(item);
            }
        }

        protected virtual void OnChangeItem()
        {
            if (Event_Change != null)
            {
                Event_Change();
            }
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            List<StorageDataContainer> itemsData = new List<StorageDataContainer>();
            
            foreach (var item in Items)
            {
                var itemData = item.GetStorageData();
                itemsData.Add(itemData);
            }
            
            data["items"] = itemsData;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<List<StorageDataContainer>>("items"))
            {
                foreach (var itemData in ((ICollection<StorageDataContainer>)storageData["items"]))
                {
                    var item = InventoryItem.CreateItemFromStorageData(itemData);

                    if (item != null)
                    {
                        Add(item);
                    }
                }
            }

            return true;
        }
    }
}
