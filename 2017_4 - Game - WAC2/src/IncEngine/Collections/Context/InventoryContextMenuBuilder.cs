﻿namespace IncEngine.Collections.Context
{
    public class InventoryContextMenuBuilder : IInventoryContextMenuBuilder
    {
        InventoryContextMenu m_ContextMenu;

        public InventoryContextMenuBuilder()
        {
            m_ContextMenu = new InventoryContextMenu();
        }

        public IInventoryContextMenuBuilder AddContextMenuItemData(ContextMenuItemData data)
        {
            m_ContextMenu.AddItem(data);
            return this;
        }

        public InventoryContextMenu Build()
        {
            return m_ContextMenu;
        }
    }
}
