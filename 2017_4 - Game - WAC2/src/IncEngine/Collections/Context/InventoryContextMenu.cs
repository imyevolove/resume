﻿using IncEngine.Collections.Context.Commands;
using System.Collections.Generic;
using System.Linq;

namespace IncEngine.Collections.Context
{
    public class InventoryContextMenu
    {
        public  List<ContextMenuItemData>   Items { get { return m_Items.ToList(); } }
        private List<ContextMenuItemData>   m_Items = new List<ContextMenuItemData>();

        public InventoryContextMenu()
        {
            
        }

        public bool HasCommand<T>() where T : ContextCommand
        {
            return m_Items.Exists(item => item.Command.GetType() == typeof(T));
        }

        public void AddItem(ContextMenuItemData data)
        {
            m_Items.Add(data);
        }

        public void RemoveItem(int index)
        {
            m_Items.RemoveAt(index);
        }
    }
}
