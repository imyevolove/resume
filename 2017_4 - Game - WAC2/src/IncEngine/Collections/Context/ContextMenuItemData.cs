﻿using IncEngine.Collections.Context.Commands;
using UnityEngine;

namespace IncEngine.Collections.Context
{
    public class ContextMenuItemData
    {
        public Sprite           Sprite      { get; set; }
        public string           Name        { get; set; }

        public Color            Color       { get; set; }

        public ContextCommand   Command     { get; set; }
        
        public ContextMenuItemData()
        {
            Name = "undefined";
        }

        public ContextMenuItemData(string name, ContextCommand command)
        {
            Name = name;
            Command = command;
        }

        public ContextMenuItemData(string name, ContextCommand command, Color color)
        {
            Name = name;
            Command = command;
            Color = color;
        }

        public ContextMenuItemData(string name, ContextCommand command, Color color, Sprite sprite)
        {
            Name = name;
            Command = command;
            Color = color;
            Sprite = sprite;
        }

        public ContextMenuItemData(string name, ContextCommand command, string hexColor)
        {
            Name = name;
            Command = command;

            var color = Color.white;
            Color = color;

            ColorUtility.TryParseHtmlString(hexColor, out color);
        }

        public bool SaveExecuteCommand()
        {
            if (Command == null) return false;

            Command.Execute();

            return true;
        }
    }
}
