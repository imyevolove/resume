﻿namespace IncEngine.Collections.Context
{
    public interface IInventoryContextMenuBuilder
    {
        IInventoryContextMenuBuilder AddContextMenuItemData(ContextMenuItemData data);
        InventoryContextMenu Build();
    }
}
