﻿using IncEngine.Personal;
using UnityEngine;
using Wac;

namespace IncEngine.Collections.Context.Commands
{
    public class SellCommand : ContextCommand
    {
        public override string AdditionalInformation { get { return Game.NumberFormatter.Format(GetItemPrice()); } }
        public Player Player { get; protected set; }


        public SellCommand(InventoryItem inventoryItem) 
            : base(inventoryItem)
        {
            Player = GameSession.Current.GetSingleton<Player>();
        }

        public override bool Execute()
        {
            var status = Player.Inventory.Remove(InventoryItem);

            if (status)
            {
                Player.Money.Increase(GetItemPrice());
            }

            Debug.Log("Selling transaction was ended with status: " + status);

            return status;
        }

        private ulong GetItemPrice()
        {
            return Game.Market.GetSalePrice(InventoryItem);
        }
    }
}
