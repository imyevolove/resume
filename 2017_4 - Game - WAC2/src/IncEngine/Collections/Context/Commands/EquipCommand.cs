﻿using IncEngine.Personal;
using UnityEngine;

namespace IncEngine.Collections.Context.Commands
{
    public class EquipCommand : ContextCommand
    {
        public EquipCommand(InventoryItem inventoryItem) 
            : base(inventoryItem)
        {
        }

        public override bool Execute()
        {
            var item = InventoryItem as InventorySkinItem;

            if(item == null)
            {
                Debug.LogWarning("Item is not InventorySkinItem; EquipCommand");
                return false;
            }

            var entity = GameSession.Current.GetSingleton<Player>().Equipment.FindEntity(item.Skin.Original);

            if (entity == null)
            {
                Debug.LogWarning("Entity not found; EquipCommand");
                return false;
            }
            
            return entity.EquipSkin(item);
        }
    }
}