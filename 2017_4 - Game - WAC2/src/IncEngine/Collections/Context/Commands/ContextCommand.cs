﻿namespace IncEngine.Collections.Context.Commands
{
    public abstract class ContextCommand
    {
        public readonly InventoryItem InventoryItem;

        public virtual string AdditionalInformation { get { return ""; } }

        public ContextCommand(InventoryItem inventoryItem)
        {
            InventoryItem = inventoryItem;
        }

        public abstract bool Execute();
    }
}
