﻿namespace IncEngine.Collections
{
    public interface IWeighted
    {
        int Weight { get; }
    }
}
