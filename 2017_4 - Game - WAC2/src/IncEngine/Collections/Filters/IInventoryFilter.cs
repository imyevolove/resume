﻿using System.Collections.Generic;

namespace IncEngine.Collections.Filters
{
    public interface IInventoryFilter
    {
        List<InventoryItem> Filter(ICollection<InventoryItem> items);
        void Filter(ref ICollection<InventoryItem> items);
    }
}