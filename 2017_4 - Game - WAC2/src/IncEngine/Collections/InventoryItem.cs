﻿using IncEngine.Collections.Context;
using IncEngine.Storage;
using IncEngine.Structures;
using System;
using UnityEngine;

namespace IncEngine.Collections
{
    [SerializeField]
    public abstract class InventoryItem : IStorage
    {
        public virtual string               FullName        { get; protected set; }
        public virtual string               CategoryName    { get; protected set; }
        public virtual string               LocalizedCategoryName { get { return CategoryName; } }
        public virtual string               Name            { get; protected set; }
        public virtual Sprite               Icon            { get; protected set; }
        public virtual Color                Color           { get; protected set; }
        public virtual IQuality             Quality         { get; protected set; }
        public virtual InventoryContextMenu ContextMenu     { get; protected set; }

        public virtual ulong                BaseDamage      { get; protected set; }
        public virtual ulong                BaseIncome      { get; protected set; }

        public virtual AbstractEntityDetails Target          { get; protected set; }

        public IInventory ParentInventory { get; private set; }

        public string GetId()
        {
            if (ParentInventory == null) return "undefined_id";
            return ParentInventory.GetItemId(this);
        }

        public int GetIndex()
        {
            if (ParentInventory == null) return -1;
            return ParentInventory.FindIndex(this);
        }

        public bool AttachTo(IInventory inventory)
        {
            if (inventory == null) return false;

            if (ParentInventory != null)
            {
                if (ParentInventory != inventory)
                {
                    ParentInventory.Remove(this);
                }
            }

            if (!inventory.Contains(this))
            {
                return inventory.Add(this);
            }

            ParentInventory = inventory;
            return true;
        }

        public bool DeattachFromParentInventory()
        {
            if (ParentInventory == null) return false;

            if (!ParentInventory.Contains(this))
            {
                ParentInventory = null;
                return true;
            }
            
            return ParentInventory.Remove(this);
        }

        public abstract ulong GetPrice();
        public abstract InventoryItem Copy();

        public abstract InventoryItem Create(StorageDataContainer storageData);

        public static InventoryItem CreateItemFromStorageData(StorageDataContainer storageData)
        {
            InventoryItem item = null;

            if (storageData.KeyValueIs<string>("item_type"))
            {
                item = CreateItemFromTypeString((string)storageData["item_type"], storageData);
            }

            return item;
        }

        private static InventoryItem CreateItemFromTypeString(string typeString, StorageDataContainer storageData)
        {
            try
            {
                var type = Type.GetType(typeString);
                var item = Activator.CreateInstance(type) as InventoryItem;

                if (!item.SetStorageData(storageData))
                {
                    return null;
                }

                return item;
            }
            catch (Exception e)
            {
                Debug.Log(e);
                return null;
            }
        }

        public virtual StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["item_type"] = GetType().FullName;

            if (Target != null)
            {
                data["target_id"] = Target.Id;
            }

            return data;
        }

        public abstract bool SetStorageData(StorageDataContainer storageData);
    }
}