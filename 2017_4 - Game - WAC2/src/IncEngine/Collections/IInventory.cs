﻿using IncEngine.Storage;
using System;
using System.Collections.Generic;

namespace IncEngine.Collections
{
    public interface IInventory : IStorage
    {
        event Action<InventoryItem> Event_ItemAdd;
        event Action<InventoryItem> Event_ItemRemove;
        event Action Event_Change;

        List<InventoryItem> Items { get; }

        InventoryItem Find(int index);

        bool Add(InventoryItem item);
        bool Remove(InventoryItem item);
        bool Remove(int index);
        void RemoveRange(IEnumerable<InventoryItem> items);

        bool Contains(InventoryItem item);
        int  FindIndex(InventoryItem item);

        string GetItemId(InventoryItem item);
    }
}
