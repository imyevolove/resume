﻿namespace IncEngine.Collections
{
    public struct WeightValuePair<T> : IWeighted
    {
        public int  Weight  { get; private set; }
        public T    Value   { get; private set; }

        public WeightValuePair(int weight, T value)
        {
            Weight = weight;
            Value  = value;
        }
    }
}
