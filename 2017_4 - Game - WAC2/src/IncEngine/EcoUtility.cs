﻿namespace IncEngine
{
    public class EcoUtility
    {
        public static float KeyRate = 2.5f;

        public static float ConvertKeysToPrice(float value)
        {
            return KeyRate * value;
        }

        public static uint ConvertKeysToPriceUInt(float value)
        {
            return (uint)(KeyRate * value);
        }

        public static ulong ConvertKeysToPriceULong(float value)
        {
            return (ulong)(KeyRate * value);
        }
    }
}
