﻿using UnityEngine;

namespace IncEngine.Economy
{
    /// <summary>
    /// Economic Resource Types
    /// </summary>
    public enum ResourceType
    {
        Coin,
        Medal
    }

    public static class EconomicResources
    {
        public static EconomicResourceStructure ResourceCoin  = new EconomicResourceStructure("Coin",  ResourceType.Coin,  ResourcesUtility.LoadSpriteFromApplicationResources("ico_eco_coin"));
        public static EconomicResourceStructure ResourceMedal = new EconomicResourceStructure("Medal", ResourceType.Medal, ResourcesUtility.LoadSpriteFromApplicationResources("ico_eco_medal"));
    }
}
