﻿using UnityEngine;

namespace IncEngine.Economy
{
    /// <summary>
    /// Resource structure
    /// </summary>
    public struct EconomicResourceStructure
    {
        public readonly string Name;
        public  Sprite Icon;
        public readonly ResourceType Type;

        public EconomicResourceStructure(string name, ResourceType type, Sprite icon)
        {
            Name = name;
            Icon = icon;
            Type = type;
        }
    }
}
