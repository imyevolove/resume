﻿namespace IncEngine
{
    public struct DamageInfo
    {
        public bool IsCrit;
        public uint Value;
    }
}
