﻿using IncEngine.Crypto;
using System;
using System.IO;
using System.Linq;
using UnityEngine;

namespace IncEngine.Storage
{
    public class Storage
    {
        public static bool Encrypt = true;
        public static string PersistentDataPath { get { return Application.persistentDataPath; } }

        public static string MakePath(params string[] parts)
        {
            return parts.Aggregate((x, y) => Path.Combine(x, y));
        }

        ////////////////////////////////////////////////////////////
        public static string LoadText(string filename)
        {
            try
            {
                var text = File.ReadAllText(filename);
                return Encrypt ? EncryptionHelper.Decrypt(text) : text;
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return "";
            }
        }

        ////////////////////////////////////////////////////////////
        public static void SaveText(string filename, string text)
        {
            try
            {
                CreateDirectories(filename);

                if(Encrypt) text = EncryptionHelper.Encrypt(text);
                File.WriteAllText(filename, text);

                Debug.LogFormat("File was saved to: {0}", filename);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }
        }

        ////////////////////////////////////////////////////////////
        private static void CreateDirectories(string path)
        {
            path = Path.GetDirectoryName(path);
            Directory.CreateDirectory(path);
        }
    }
}
