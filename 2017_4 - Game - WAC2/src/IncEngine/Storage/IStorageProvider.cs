﻿namespace IncEngine.Storage
{
    public interface IStorageProvider<T>
    {
        T Load(string filename);
        void Save(T obj, string filename);
    }
}
