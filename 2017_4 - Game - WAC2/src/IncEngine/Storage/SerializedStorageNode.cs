﻿using System;

namespace IncEngine.Storage
{
    [Serializable]
    public struct SerializedStorageNode
    {
        public string key;
        public object value;
        public string type;
    }
}
