﻿namespace IncEngine.Storage
{
    public interface IStorage
    {
        StorageDataContainer    GetStorageData();
        bool                    SetStorageData(StorageDataContainer storageData);
    }
}
