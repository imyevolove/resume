﻿using System;

namespace IncEngine.Storage.Serialization
{
    public abstract class StorageSerializer
    {
        public abstract SerializedStorageNode Serialize(string key, object obj, StorageSerialization provider);
        public abstract object Deserialize(SerializedStorageNode obj, StorageSerialization provider);
        public abstract bool CanHandleType(Type type);
    }
}
