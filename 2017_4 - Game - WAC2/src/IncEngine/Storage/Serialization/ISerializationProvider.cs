﻿namespace IncEngine.Storage.Serialization
{
    public interface ISerializationProvider<TSerializer>
    {
        void AddSerializer(TSerializer handler);
        void RemoveSerializer(TSerializer handler);
    }
}
