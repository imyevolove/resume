﻿using System.Collections;
using System.Collections.Generic;

namespace IncEngine.Storage
{
    public interface IStorageContainer
    {
        int Count { get; }
        bool HasKey(string key);
        bool KeyValueIs<T>(string key);

        StorageNode this[int index] { get; set; }
        object this[string key] { get; set; }

        List<StorageNode> GetData();
        StorageNode Get(string id);
        StorageNode GetOrCreate(string id);
        void Set(StorageNode node);
        void Set(IStorageContainer container);
        bool TrySet(StorageNode node);


        string ToXml();
    }
}
