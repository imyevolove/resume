﻿using System;

namespace IncEngine.Storage
{
    [Serializable]
    public struct StorageNode
    {
        public string key;
        public object value;

        public bool IsEmpty()
        {
            return Equals(default(StorageNode));
        }
    }
}
