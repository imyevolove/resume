﻿using IncEngine.Data;
using IncEngine.Structures;
using System.Collections.Generic;
using Wac.Collections;

namespace IncEngine
{
    public class EntitiesManager
    {
        /// <summary>
        /// Contain maps
        /// </summary>
        public readonly static List<Map>        Maps        = new List<Map>();

        /// <summary>
        /// Contain cases
        /// </summary>
        public readonly static List<Case>       Cases       = new List<Case>();

        /// <summary>
        /// Contain collectoins
        /// </summary>
        public readonly static List<Collection> Collections = new List<Collection>();
        
        /// <summary>
        /// Contain stickers
        /// </summary>
        public readonly static List<Sticker>    Stickers    = new List<Sticker>();

        /// <summary>
        /// Contain entities like a guns, knives and gloves
        /// </summary>
        public readonly static EntityCollection Entities = new EntityCollection();

        /// <summary>
        /// Find sticker by item id
        /// </summary>
        /// <param name="skinId"></param>
        /// <returns></returns>
        public static Sticker FindSticker(string id)
        {
            return Stickers.Find(sticker => sticker.Id == id);
        }

        /// <summary>
        /// Find case by collection
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static Case FindCase(Collection collection)
        {
            return Cases.Find(cs => cs.Collection == collection);
        }

        /// <summary>
        /// Find case by skin
        /// </summary>
        /// <param name="skin"></param>
        /// <returns></returns>
        public static Case FindCase(Skin skin)
        {
            return Cases.Find(cs => cs.Skins.Contains(skin));
        }

        /// <summary>
        /// Find case by collection and quality
        /// </summary>
        /// <param name="skin"></param>
        /// <returns></returns>
        public static Case FindCase(Collection collection, Quality quality)
        {
            return Cases.Find(cs => cs.Collection == collection && cs.HasSkin(quality));
        }

        /// <summary>
        /// Find case by skin and quality
        /// </summary>
        /// <param name="skin"></param>
        /// <param name="quality"></param>
        /// <returns></returns>
        public static Case FindCase(Skin skin, Quality quality)
        {
            return Cases.Find(cs => cs.HasSkin(skin) && cs.HasSkin(quality));
        }

        /// <summary>
        /// Find case by quality
        /// </summary>
        /// <param name="quality"></param>
        /// <returns></returns>
        public static Case FindCase(Quality quality)
        {
            return Cases.Find(cs => cs.HasSkin(quality));
        }

        /// <summary>
        /// Find case by qualities
        /// </summary>
        /// <param name="qualities"></param>
        /// <returns></returns>
        public static Case FindCase(IEnumerable<Quality> qualities)
        {
            Case resultCase = null;
            foreach (var quality in qualities)
            {
                resultCase = FindCase(quality);

                if (resultCase != null) continue;
            }

            return resultCase;
        }

        /// <summary>
        /// Find case by skin
        /// </summary>
        /// <param name="skin"></param>
        /// <returns></returns>
        public static Case FindCase(IEnumerable<Collection> collections, Quality quality)
        {
            Case resultCase = null;
            foreach (var collection in collections)
            {
                resultCase = FindCase(collection, quality);

                if (resultCase != null) continue;
            }

            return resultCase;
        }

        public static void SortSkinsByQuality(List<Skin> skins)
        {
            skins.Sort((a, b) => Quality.GetIndex(b.Quality).CompareTo(Quality.GetIndex(a.Quality)));
        }

        public static void FetchFromGameData(GameData gameData)
        {
            Maps.AddRange(gameData.Maps);
            Cases.AddRange(gameData.Cases);
            Collections.AddRange(gameData.Collections);
            Stickers.AddRange(gameData.Stickers);
            Entities.Originals.AddRange(gameData.Entities.Originals);
            Entities.Skins.AddRange(gameData.Entities.Skins);
        }
    }
}