﻿using UnityEngine;

namespace IncEngine.UpdateManagement
{
    public class UpdateObject : MonoBehaviour
    {
        public event UpdateEvent Event_Update;
        public event UpdateEvent Event_FixedUpdate;
        
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        void Update()
        {
            if (Event_Update != null)
                Event_Update();
        }

        void FixedUpdate()
        {
            if (Event_FixedUpdate != null)
                Event_FixedUpdate();
        }
    }
}
