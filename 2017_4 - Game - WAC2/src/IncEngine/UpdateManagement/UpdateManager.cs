﻿using System;
using System.Collections;
using UnityEngine;

namespace IncEngine.UpdateManagement
{
    public class UpdateManager
    {
        protected static UpdateObject TickObject
        {
            get
            {
                if (m_TickObject == null)
                {
                    m_TickObject = new GameObject("TickObject", typeof(UpdateObject)).GetComponent<UpdateObject>();
                    m_TickObject.hideFlags = HideFlags.HideInHierarchy;
                }

                return m_TickObject;
            }
        }
        private static UpdateObject m_TickObject;

        public static event UpdateEvent Event_Update
        {
            add { TickObject.Event_Update += value; }
            remove { TickObject.Event_Update -= value;  }
        }

        public static event UpdateEvent Event_FixedUpdate
        {
            add { TickObject.Event_FixedUpdate += value; }
            remove { TickObject.Event_FixedUpdate -= value; }
        }

        public static Coroutine DelayedExecution(Action action, int pause)
        {
            return StartCoroutine(Delay(action, pause));
        }

        public static Coroutine StartCoroutine(IEnumerator enumerator)
        {
            return TickObject.StartCoroutine(enumerator);
        }

        public static void StopCoroutine(Coroutine coroutine)
        {
            TickObject.StopCoroutine(coroutine);
        }

        public static Coroutine StartRepeate(float period, Action action)
        {
            return TickObject.StartCoroutine(Repeater(period, action));
        }

        private static IEnumerator Delay(Action action, int pause)
        {
            yield return new WaitForSeconds(pause);
            action();
        }

        private static IEnumerator Repeater(float period, Action action)
        {
            while (true)
            {
                yield return new WaitForSeconds(period);
                action();
            }
        }
    }
}
