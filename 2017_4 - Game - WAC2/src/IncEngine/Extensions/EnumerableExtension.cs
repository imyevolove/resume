﻿using System;
using System.Collections;

namespace IncEngine.Extensions
{
    public static class EnumerableExtension
    {
        /// <summary>
        /// Return IEnumerable count
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int Count(this IEnumerable source)
        {
            var collection = source as ICollection;
            if (collection != null)
                return collection.Count;

            int count = 0;

            var enumerator = source.GetEnumerator();
            DynamicUsing(enumerator, () =>
            {
                while (enumerator.MoveNext())
                    count++;
            });

            return count;
        }

        public static void DynamicUsing(object resource, Action action)
        {
            try
            {
                action();
            }
            finally
            {
                IDisposable d = resource as IDisposable;
                if (d != null)
                    d.Dispose();
            }
        }
    }
}
