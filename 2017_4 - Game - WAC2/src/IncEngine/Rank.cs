﻿using System;
using UnityEngine;

namespace IncEngine
{
    [Serializable]
    public class Rank
    {
        [SerializeField]
        public Sprite image;
        [SerializeField]
        public string text;
        [SerializeField]
        public ulong weight;
    }
}
