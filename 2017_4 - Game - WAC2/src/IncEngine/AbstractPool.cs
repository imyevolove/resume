﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace IncEngine
{
    /// <summary>
    /// Abstract complete pool class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbstractPool<T> : IPool<T> where T : class, IPoolable
    {
        private List<IPoolable> m_FreeEntities   = new List<IPoolable>();
        private List<IPoolable> m_ActiveEntities = new List<IPoolable>();

        /// <summary>
        /// Create entity without registration
        /// </summary>
        /// <returns></returns>
        public abstract T CreateEntity();
        
        /// <summary>
        /// Return and activate free entity
        /// </summary>
        /// <returns></returns>
        public T GetFree()
        {
            T entity = (m_FreeEntities.Count == 0)
                ? CreateAndRegisterEntity()
                : m_FreeEntities.FirstOrDefault() as T;

            entity.Activate();

            return entity;
        }

        /// <summary>
        /// Return has status for entity
        /// </summary>
        /// <returns></returns>
        public bool Has(T entity)
        {
            return m_FreeEntities.Contains(entity) || m_ActiveEntities.Contains(entity);
        }

        /// <summary>
        /// Deactivate concret entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Deactivate(T entity)
        {
            if (!m_ActiveEntities.Contains(entity))
            {
                Debug.Log("Can't deactivate poolable item. Item not found in activated list");
                return false;
            }

            var removed = m_ActiveEntities.Remove(entity);

            if (removed)
            {
                entity.Deactivate();
            }

            return removed;
        }

        /// <summary>
        /// Deactivate all registered activated entities
        /// </summary>
        public void DeactivateAllActiveEntities()
        {
            foreach (var item in m_ActiveEntities.ToArray())
            {
                item.Deactivate();
            }

            m_ActiveEntities.Clear();
        }

        /// <summary>
        /// Create and register entity as free
        /// </summary>
        /// <returns></returns>
        protected T CreateAndRegisterEntity()
        {
            T entity = CreateEntity();
            
            entity.Event_Activate   += ActivateEntity;
            entity.Event_Deactivate += DeactivateEntity;

            entity.Deactivate();

            return entity;
        }

        /// <summary>
        /// Remove from free list
        /// </summary>
        /// <param name="entity"></param>
        private void ActivateEntity(IPoolable entity)
        {
            m_FreeEntities.Remove(entity);
            m_ActiveEntities.Add(entity);
        }

        /// <summary>
        /// Add to free list
        /// </summary>
        /// <param name="entity"></param>
        private void DeactivateEntity(IPoolable entity)
        {
            m_FreeEntities.Add(entity);
            m_ActiveEntities.Remove(entity);
        }
    }
}
