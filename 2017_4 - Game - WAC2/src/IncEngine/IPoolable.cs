﻿using System;

namespace IncEngine
{
    public interface IPoolable
    {
        event Action<IPoolable, bool> Event_ActiveChange;
        event Action<IPoolable> Event_Activate;
        event Action<IPoolable> Event_Deactivate;

        bool Active { get; }

        void Activate();
        void Deactivate();
    }
}
