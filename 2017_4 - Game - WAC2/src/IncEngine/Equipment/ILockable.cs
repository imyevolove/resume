﻿using System;

namespace IncEngine.Equipment
{
    public interface ILockable
    {
        event Action<bool> Event_LockChange;
        bool IsLocked { get; }

        void Lock();
        void Unlock();
    }
}
