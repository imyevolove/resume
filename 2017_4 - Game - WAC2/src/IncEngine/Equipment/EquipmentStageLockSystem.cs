﻿using UnityEngine;

namespace IncEngine.Equipment
{
    public class EquipmentStageLockSystem : BaseEquipmentLockSystem
    {
        protected StoreValue StoreValue { get; set; }

        public ILockSystemStageCalcMethod Calculator { get; protected set; }

        public EquipmentStageLockSystem(StoreValue storeValue, ILockSystemStageCalcMethod calculator)
        {
            Configure(storeValue, calculator, 0);
            RecalculateStageIndex();
        }

        public EquipmentStageLockSystem(StoreValue storeValue, ILockSystemStageCalcMethod calculator, int stageIndex)
        {
            Configure(storeValue, calculator, stageIndex);
            RecalculateStageIndex();
        }

        private void Configure(StoreValue storeValue, ILockSystemStageCalcMethod calculator, int stageIndex)
        {
            StoreValue = storeValue;
            Calculator = calculator;
            StageLockIndex = stageIndex;

            StoreValue.Event_Change += ListenValueChange;
        }

        private void RecalculateStageIndex()
        {
            RecalculateStageIndex(StoreValue.Value);
        }

        private void RecalculateStageIndex(ulong value)
        {
            var result = Calculator.Calculate(value);

            ///Debug.LogFormat("Value {0} New {1}, Last {2}", value, result, StageLockIndex);

            if (result > StageLockIndex)
            {
                StageLockIndex = result;
                CallAnyChangeEvent();
            }
        }

        private void ListenValueChange(ulong value)
        {
            RecalculateStageIndex(value);
            UnlockItemsIncludedInRange();
        }

        private void UnlockItemsIncludedInRange()
        {
            if (m_Elements.Count == 0) return;

            var index = Mathf.Clamp(StageLockIndex, 0, m_Elements.Count);

            for (var i = 0; i < index; i++)
            {
                Unlock(i);
            }

            CallAnyLockChangeEvent();
        }
    }
}
