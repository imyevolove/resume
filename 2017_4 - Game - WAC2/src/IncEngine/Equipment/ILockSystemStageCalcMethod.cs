﻿namespace IncEngine.Equipment
{
    public interface ILockSystemStageCalcMethod
    {
       int Calculate(ulong value);
    }
}
