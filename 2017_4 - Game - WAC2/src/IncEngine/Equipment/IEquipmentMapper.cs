﻿using System;
using System.Collections.Generic;
using IncEngine.Equipment.Entities;
using IncEngine.Storage;

namespace IncEngine.Equipment
{
    public interface IEquipmentMapper : IStorage
    {
        event Action Event_AnyChange;

        ulong GetSummaryDamage();
        ulong GetSummaryIncome();

        void AddEntity<T>(T entity) where T : EquipmentEntity;
        List<T> GetEntities<T>() where T : EquipmentEntity;

        EquipmentEntity FindEntity(object obj);
        EquipmentEntity FindEntity(string entityId);
    }
}
