﻿using System;
using System.Collections.Generic;

namespace IncEngine.Equipment
{
    using Entities;
    using IncEngine.Storage;
    using IncEngine.Structures;
    using UnityEngine;

    public class EquipmentMapper : IEquipmentMapper
    {
        public event Action Event_AnyChange;

        public ulong GetSummaryDamage() { return m_CachedSummaryDamage; }
        public ulong GetSummaryIncome() { return m_CachedSummaryIncome; }

        private List<EquipmentEntity> m_AllEntities = new List<EquipmentEntity>();

        private ulong m_CachedSummaryDamage;
        private ulong m_CachedSummaryIncome;

        private int m_HelperAllEntitiesIndex = 0;

        public virtual void RecalculateCachedValues()
        {
            m_CachedSummaryDamage = 0;
            m_CachedSummaryIncome = 0;

            m_HelperAllEntitiesIndex = 0;
            for (m_HelperAllEntitiesIndex = 0; 
                m_HelperAllEntitiesIndex < m_AllEntities.Count; 
                m_HelperAllEntitiesIndex++)
            {
                m_CachedSummaryDamage += m_AllEntities[m_HelperAllEntitiesIndex].GetDamage();
                m_CachedSummaryIncome += m_AllEntities[m_HelperAllEntitiesIndex].GetIncome();
            }
        }

        void IEquipmentMapper.AddEntity<T>(T entity)
        {
            m_AllEntities.Add(entity);
            entity.Event_AnyChange += OnAnyEntityChange;
        }

        List<T> IEquipmentMapper.GetEntities<T>()
        {
            List<T> list = new List<T>();
            T tmpItem;

            foreach (var item in m_AllEntities)
            {
                tmpItem = item as T;

                if (tmpItem == null) continue;
                list.Add(tmpItem);
            }

            return list;
        }

        protected virtual void OnAnyEntityChange()
        {
            RecalculateCachedValues();

            if (Event_AnyChange != null)
                Event_AnyChange();
        }

        public EquipmentEntity FindEntity(object obj)
        {
            try
            {
                if (obj is Skin)
                {
                    return m_AllEntities.Find(ee => ee.Entity.Equals(((Skin)obj).Original));
                }

                if (obj is Entity)
                {
                    return m_AllEntities.Find(ee => ee.Entity.Equals((Entity)obj));
                }

                return m_AllEntities.Find(entity => entity.Equals(obj));
            }
            catch (Exception)
            {
                Debug.LogWarning("Catch Equipment mapper Exception. Method: FindEntity");
                return null;
            }
        }

        public EquipmentEntity FindEntity(string id)
        {
            return m_AllEntities.Find(entity => entity.Entity != null && entity.Entity.Id == id);
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            List<StorageDataContainer> itemsData = new List<StorageDataContainer>();

            foreach (var item in m_AllEntities)
            {
                itemsData.Add(item.GetStorageData());
            }

            data["items"] = itemsData;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<List<StorageDataContainer>>("items"))
            {
                foreach (var itemData in ((ICollection<StorageDataContainer>)storageData["items"]))
                {
                    if (itemData == null || !itemData.KeyValueIs<string>("entity_id")) continue;

                    var item = FindEntity((string)itemData["entity_id"]);

                    if (item != null)
                    {
                        item.SetStorageData(itemData);
                    }
                }
            }

            return true;
        }
    }
}
