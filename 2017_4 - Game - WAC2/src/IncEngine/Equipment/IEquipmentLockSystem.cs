﻿using IncEngine.Equipment.Entities;
using System;
using System.Collections.Generic;

namespace IncEngine.Equipment
{
    public interface IEquipmentLockSystem
    {
        event Action Event_AnyChange;
        event Action Event_AnyLockChange;

        int StageLockIndex { get; }

        bool Add(EquipmentEntity entity);
        void AddRange(ICollection<EquipmentEntity> entity);
        bool Remove(EquipmentEntity entity);

        bool Unlock(int index);
        bool Lock(int index);
    }
}
