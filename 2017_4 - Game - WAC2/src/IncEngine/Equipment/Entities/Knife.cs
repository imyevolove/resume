﻿using IncEngine.Structures;

namespace IncEngine.Equipment.Entities
{
    /// <summary>
    /// Gun entity
    /// </summary>
    public class Knife : EquipmentEntity
    {
        public Knife(Entity entity) : base(entity)
        {
        }

        protected override void OnSkinChange()
        {
            base.OnSkinChange();

            Active = !(Skin == null);
        }

        protected override ulong CalculateDamageValue()
        {
            return base.CalculateDamageValue() * GetMultiplier();
        }

        protected override ulong CalculateIncomeValue()
        {
            return base.CalculateIncomeValue() * GetMultiplier();
        }

        private ulong GetMultiplier()
        {
            return Skin == null ? 0UL : 1UL;
        }
    }
}
