﻿using System;

namespace IncEngine.Equipment.Entities
{
    using IncEngine.Abilities;
    using IncEngine.Collections;
    using IncEngine.Storage;
    using Structures;
    using UnityEngine;
    using Wac;

    public abstract class EquipmentEntity : ISkinnable, IUpgradable, ILockable, IStorage
    {
        public event Action<bool> Event_LockChange;
        public event Action Event_SkinChange;
        public event Action Event_Upgrade;
        public event Action Event_AnyChange;
        public event Action<bool> Event_ActiveChange;

        public Entity       Entity          { get; private set; }
        
        public string       FullName        { get { return Skin != null ? Skin.FullName : Entity.Name; } }
        public string       EntityName      { get { return Entity.Name; } }
        public string       SkinName        { get { return Skin != null ? Skin.Name : ""; } }
        
        public Sprite       Icon            { get { return Skin != null ? Skin.Icon : Entity.Icon; } }
        public Color        QualityColor    { get { return Skin != null ? Skin.Quality.Color : Colors.DefaultEntityColor; } }
        
        public float        PriceMultiplier { get; set; }

        private ulong       m_CacheDamage;
        private ulong       m_CacheIncome;

        public ulong BaseDamage { get { return InventorySkin != null ? InventorySkin.BaseDamage : Entity.BaseDamage; } }
        public ulong BaseIncome { get { return InventorySkin != null ? InventorySkin.BaseIncome : Entity.BaseIncome; } }

        private bool m_Active;
        public bool Active
        {
            get { return m_Active; }
            set
            {
                if (m_Active == value) return;

                m_Active = value;

                OnActiveChange();

                if (Event_ActiveChange != null)
                    Event_ActiveChange(m_Active);
            }
        }

        /// <summary>
        /// Equipped skin
        /// </summary>
        public Skin Skin { get { return InventorySkin == null ? null : InventorySkin.Skin; } }
        public InventoryItem AttachedInventoryItem { get { return InventorySkin; } }
        public InventorySkinItem InventorySkin { get; private set; }
        
        public uint UpgradeLevel { get; private set; }

        /// <summary>
        /// Max available level count
        /// </summary>
        public uint UpgradeMaxLevel { get; set; }

        private bool M_IsLocked = true;
        public bool IsLocked
        {
            get { return M_IsLocked; }
            set
            {
                if (M_IsLocked == value) return;

                M_IsLocked = value;

                OnLockChange(M_IsLocked);
            }
        }

        public EquipmentEntity(Entity entity)
        {
            Entity = entity;
            UpgradeMaxLevel = uint.MaxValue;
            PriceMultiplier = 1.15f;
        }

        public void Lock()
        {
            IsLocked = true;
        }

        public void Unlock()
        {
            IsLocked = false;
        }

        public bool EquipSkin(InventorySkinItem inventorySkin)
        {
            if (inventorySkin == null || inventorySkin.Skin.Original != Entity) return false;

            InventorySkin = inventorySkin;

            OnSkinChange();
            OnAnyChange();
            
            return true;
        }

        public void RemoveEquipment()
        {
            InventorySkin = null;

            OnSkinChange();
            OnAnyChange();
        }

        public ulong GetDamage()
        {
            return m_CacheDamage;
        }

        public ulong GetIncome()
        {
            return m_CacheIncome;
        }

        public ulong GetUpgradePrice()
        {
            return (ulong)(Game.Player.Abilities.GetAbility(AbilityType.UpgradeReducePrice)
                .ApplyToValue(Entity.Price) * Mathf.Pow(UpgradeLevel + 1, PriceMultiplier) / PriceMultiplier);
        }

        /// <summary>
        /// Upgrade level for 1
        /// </summary>
        public bool Upgrade()
        {
            return Upgrade(1);
        }

        /// <summary>
        /// Upgrade level for level count
        /// </summary>
        /// <param name="level"></param>
        public bool Upgrade(uint level)
        {
            if (!CanUpgrade(level)) return false;

            UpgradeLevel += level;

            OnUpgradeLevelChange();
            OnAnyChange();
            
            return true;
        }

        /// <summary>
        /// Check upgrade available for 1 more level count
        /// </summary>
        /// <returns></returns>
        public bool CanUpgrade()
        {
            return (UpgradeLevel + 1) < UpgradeMaxLevel;
        }

        /// <summary>
        /// Check available upgrade level for count
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public bool CanUpgrade(uint level)
        {
            return (UpgradeLevel + level) < UpgradeMaxLevel;
        }

        protected void SetUpgradeLevel(uint level)
        {
            level = level < 0 ? 0 : level;
            level = level > UpgradeMaxLevel ? UpgradeMaxLevel : level;

            UpgradeLevel = level;

            OnUpgradeLevelChange();
            OnAnyChange();
        }

        /// <summary>
        /// Called when invoked any change event (update level/skin change)
        /// </summary>
        protected virtual void OnAnyChange()
        {
            UpdateCachedValues();

            if (Event_AnyChange != null)
                Event_AnyChange();
        }

        /// <summary>
        /// Update cached values
        /// </summary>
        protected void UpdateCachedValues()
        {
            m_CacheDamage = CalculateDamageValue();
            m_CacheIncome = CalculateIncomeValue();
        }

        /// <summary>
        /// Call when activity state changed
        /// </summary>
        protected virtual void OnActiveChange()
        {
        }

        /// <summary>
        /// Call when skin changed
        /// </summary>
        protected virtual void OnSkinChange()
        {
            if (Event_SkinChange != null)
                Event_SkinChange();
        }

        /// <summary>
        /// Call when upgrade level changed
        /// </summary>
        protected virtual void OnUpgradeLevelChange()
        {
            if (Event_Upgrade != null)
                Event_Upgrade();
        }

        /// <summary>
        /// Call when lock changed
        /// </summary>
        protected virtual void OnLockChange(bool lockStatus)
        {
            if (Event_LockChange != null)
            {
                Event_LockChange(lockStatus);
            }
        }

        /// <summary>
        /// Return calculated damage value
        /// </summary>
        /// <returns></returns>
        protected virtual ulong CalculateDamageValue()
        {
            return BaseDamage * UpgradeLevel;
        }

        /// <summary>
        /// Return calculated income value
        /// </summary>
        /// <returns></returns>
        protected virtual ulong CalculateIncomeValue()
        {
            return BaseIncome * UpgradeLevel;
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["entity_id"] = Entity.Id;
            data["attached_inventory_item_index"] = AttachedInventoryItem == null ? -1 : AttachedInventoryItem.GetIndex();
            data["upgrade_level"] = UpgradeLevel;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return false;
            
            var lvl = storageData.KeyValueIs<uint>("upgrade_level") ? (uint)storageData["upgrade_level"] : 0;
            
            if (storageData.KeyValueIs<int>("attached_inventory_item_index"))
            {
                var item = Game.Player.Inventory.Find((int)storageData["attached_inventory_item_index"]) as InventorySkinItem;
                if (item != null)
                {
                    EquipSkin(item);
                }
            }

            SetUpgradeLevel(lvl);

            return true;
        }
    }
}