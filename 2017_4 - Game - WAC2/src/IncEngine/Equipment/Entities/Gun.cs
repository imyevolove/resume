﻿using IncEngine.Structures;
using UnityEngine;

namespace IncEngine.Equipment.Entities
{
    /// <summary>
    /// Gun entity
    /// </summary>
    public class Gun : EquipmentEntity
    {
        public Gun(Entity entity) : base(entity)
        {
        }

        protected override void OnSkinChange()
        {
            base.OnSkinChange();

            Debug.Log("skin change");
        }
    }
}
