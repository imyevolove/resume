﻿using System;
using System.Collections.Generic;
using IncEngine.Equipment.Entities;

namespace IncEngine.Equipment
{
    public abstract class BaseEquipmentLockSystem : IEquipmentLockSystem
    {
        public event Action Event_AnyChange;
        public event Action Event_AnyLockChange;

        public int StageLockIndex { get; protected set; }

        protected List<EquipmentEntity> m_Elements = new List<EquipmentEntity>();

        public bool Add(EquipmentEntity entity)
        {
            if (m_Elements.Contains(entity)) return false;

            m_Elements.Add(entity);
            return true;
        }

        public void AddRange(ICollection<EquipmentEntity> entities)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }
        }

        public bool Remove(EquipmentEntity entity)
        {
            if (!m_Elements.Contains(entity)) return false;

            return m_Elements.Remove(entity);
        }

        public bool Lock(int index)
        {
            if (index < 0 || index >= m_Elements.Count) return false;

            try
            {
                m_Elements[index].Lock();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Unlock(int index)
        {
            if (index < 0 || index >= m_Elements.Count) return false;

            try
            {
                m_Elements[index].Unlock();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void CallAnyChangeEvent()
        {
            if (Event_AnyChange != null)
            {
                Event_AnyChange();
            }
        }

        protected void CallAnyLockChangeEvent()
        {
            if (Event_AnyLockChange != null)
            {
                Event_AnyLockChange();
            }
        }
    }
}
