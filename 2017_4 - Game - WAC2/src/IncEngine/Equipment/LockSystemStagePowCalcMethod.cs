﻿using UnityEngine;

namespace IncEngine.Equipment
{
    public class LockSystemStagePowCalcMethod : ILockSystemStageCalcMethod
    {
        public float Multiplier { get; protected set; }

        public const float Divider = 100f;

        public LockSystemStagePowCalcMethod(float multiplier)
        {
            Multiplier = multiplier;
        }

        public int Calculate(ulong value)
        {
            if (value < Divider) return 0;
            return (int)(Mathf.Log(value / Divider, Multiplier));
        }
    }
}
