﻿using IncEngine.Structures;
using SimpleJSON;
using System;
using System.Collections.Generic;

namespace IncEngine.Parsers
{
    public static class GameDataParserUtility
    {
        public static List<SkinDetails> ParseDetailsFromJSONArray(JSONArray arr)
        {
            List<SkinDetails> details = new List<SkinDetails>();

            foreach (JSONNode node in arr)
            {
                details.Add(new SkinDetails()
                {
                    ExteriorQuality = ExteriorQuality.Parse(node["condition"].Value, true),
                    Special = SkinSpecial.Parse(node["signature"].Value),
                    Price = ParseValueToULong(node["price"].ToString()),
                    BaseDamage = ParseValueToULong(node["stats"]["damage"].ToString()),
                    BaseIncome = ParseValueToULong(node["stats"]["income"].ToString())
                });
            }

            return details;
        }

        public static void ParseDetailsFromJSONArray(JSONArray arr, Skin skin)
        {
            skin.Details.AddRange(ParseDetailsFromJSONArray(arr));
        }

        public static IntermediateSkinStats ParseStatsFromJSONNode(JSONNode node)
        {
            if (node["stats"].IsObject)
            {
                node = node["stats"];
            }

            IntermediateSkinStats details = new IntermediateSkinStats();
            details.Damage = ParseValueToULong(node["damage"].ToString());
            details.Income = ParseValueToULong(node["income"].ToString());
            details.Price = ParseValueToULong(node["base_price"].ToString());

            return details;
        }

        public static ulong ParseValueToULong(string value)
        {
            try
            {
                return ulong.Parse(value);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
