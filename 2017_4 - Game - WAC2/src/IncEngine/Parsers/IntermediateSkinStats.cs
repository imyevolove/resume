﻿using System;

namespace IncEngine.Parsers
{
    public struct IntermediateSkinStats : IDisposable
    {
        public ulong Damage { get; set; }
        public ulong Income { get; set; }
        public ulong Price  { get; set; }

        public IntermediateSkinStats(ulong damage, ulong income)
        {
            Damage = damage;
            Income = income;
            Price  = 0;
        }

        public IntermediateSkinStats(ulong damage, ulong income, ulong price)
        {
            Damage = damage;
            Income = income;
            Price  = price;
        }

        public void Dispose()
        {
            
        }
    }
}
