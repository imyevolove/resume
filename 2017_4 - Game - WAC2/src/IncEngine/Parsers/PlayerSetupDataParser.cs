﻿using IncEngine.Data;
using UnityEngine;
using System;

namespace IncEngine.Persers
{
    public class PlayerSetupDataParser : IParser<PlayerSetupData>
    {
        /// <summary>
        /// Parse present text to gloves list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public PlayerSetupData Parse(string text)
        {
            PlayerSetupData data;

            try
            {
                data = JsonUtility.FromJson<PlayerSetupData>(text);
            }
            catch (Exception)
            {
                data = new PlayerSetupData();
            }

            return data;
        }
    }
}
