﻿namespace IncEngine.Persers
{
    public interface IParser<T>
    {
        T Parse(string text);
    }
}
