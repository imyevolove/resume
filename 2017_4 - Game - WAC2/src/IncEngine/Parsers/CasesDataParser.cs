﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;
using IncEngine.Data;
using IncEngine.Parsers;

namespace IncEngine.Persers
{
    public class CasesDataParser : IParser<List<Case>>
    {
        private GameData m_GameData;

        public CasesDataParser(GameData gameData)
        {
            m_GameData = gameData;
        }

        /// <summary>
        /// Parse present text to collections list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Case> Parse(string text)
        {
            List<Case> items = new List<Case>();

            var json = JSON.Parse(text);
            foreach (JSONNode node in json.AsArray)
            {
                var caseSkins = new List<Skin>();
                var itemsNode = node["items"].AsArray;

                foreach (JSONNode itemNode in itemsNode)
                {
                    var skin = m_GameData.FindSkin(itemNode.Value);
                    if (skin == null) continue;

                    caseSkins.Add(skin);
                }

                var caseEntity = new Case()
                {
                    Id = node["id"].Value,
                    Name = node["name"].Value,
                    Icon = ResourcesUtility.LoadSpriteFromApplicationResources(node["image_id"].Value),
                    Price = GameDataParserUtility.ParseValueToULong(node["price"].ToString()),
                    Collection = m_GameData.FindCollection(node["collectionId"].Value),
                    Skins = caseSkins
                };

                items.Add(caseEntity);
            }

            return items;
        }
    }
}
