﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;

namespace IncEngine.Persers
{
    public class CollectionsDataParser : IParser<List<Collection>>
    {
        /// <summary>
        /// Parse present text to collections list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Collection> Parse(string text)
        {
            List<Collection> items = new List<Collection>();

            var json = JSON.Parse(text);
            foreach (JSONNode node in json.AsArray)
            {
                items.Add(new Collection() {
                    Id = node["id"].Value,
                    Name = node["name"].Value,
                    Icon = ResourcesUtility.LoadSpriteFromApplicationResources(node["image_id"].Value),
                });
            }

            return items;
        }
    }
}
