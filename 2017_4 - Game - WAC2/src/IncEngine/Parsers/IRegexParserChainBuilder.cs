﻿using System.Text.RegularExpressions;

namespace IncEngine.Persers
{
    public interface IRegexParserChainBuilder<T>
    {
        IRegexParserChainBuilder<T> AddChain(Regex regex, T value);
        RegexParserChain<T> Build();
    }
}
