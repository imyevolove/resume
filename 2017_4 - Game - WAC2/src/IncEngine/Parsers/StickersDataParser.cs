﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;
using IncEngine.Parsers;

namespace IncEngine.Persers
{
    public class StickersDataParser : IParser<List<Sticker>>
    {
        /// <summary>
        /// Parse present text to stickers list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Sticker> Parse(string text)
        {
            List<Sticker> items = new List<Sticker>();

            var json = JSON.Parse(text);
            foreach (JSONNode node in json.AsArray)
            {
                items.Add(new Sticker() {
                    Id = node["id"].Value,
                    Name = node["name"].Value,
                    Icon = ResourcesUtility.LoadSpriteFromApplicationResources(node["image_id"].Value),
                    Price = GameDataParserUtility.ParseValueToULong(node["price"].ToString()),
                    Quality = StickerQuality.Parse(node["rarity"].Value, true)
                });
            }

            return items;
        }
    }
}
