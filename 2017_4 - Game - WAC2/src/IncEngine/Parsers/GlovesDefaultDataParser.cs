﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;
using IncEngine.Parsers;

namespace IncEngine.Persers
{
    public class GlovesDefaultDataParser : IParser<List<Entity>>
    {
        /// <summary>
        /// Parse present text to gloves list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Entity> Parse(string text)
        {
            List<Entity> items = new List<Entity>();

            var json = JSON.Parse(text);

            foreach (JSONNode node in json.AsArray)
            {
                items.Add(ParseEntity(node));
            }

            return items;
        }

        private Entity ParseEntity(JSONNode node)
        {
            using (var stats = GameDataParserUtility.ParseStatsFromJSONNode(node))
            {
                return new Entity(stats.Damage, stats.Income)
                {
                    Type = EntityType.Glove,
                    Id = node["id"].Value,
                    Name = node["name"].Value,
                    Price = stats.Price
                };
            }
        }
    }
}
