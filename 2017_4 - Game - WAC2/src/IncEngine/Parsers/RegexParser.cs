﻿using System.Text.RegularExpressions;

namespace IncEngine.Persers
{
    public class RegexParser<T> : IParser<T>
    {
        public readonly Regex Regex;
        public readonly T Value;

        public RegexParser(Regex regex, T value)
        {
            Regex = regex;
            Value = value;
        }
        
        public virtual T Parse(string text)
        {
            if (Regex.IsMatch(text)) return Value;
            return default(T);
        }
    }
}
