﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;

namespace IncEngine.Persers
{
    public class MapsDataParser : IParser<List<Map>>
    {
        /// <summary>
        /// Parse present text to maps list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Map> Parse(string text)
        {
            List<Map> items = new List<Map>();

            var json = JSON.Parse(text);
            foreach (JSONNode node in json.AsArray)
            {
                items.Add(new Map() {
                    Name    = node["name"].Value,
                    Icon = ResourcesUtility.LoadSpriteFromApplicationResources(node["icon_id"].Value),
                    Image   = ResourcesUtility.LoadSpriteFromApplicationResources(node["cover_image_id"].Value)
                });
            }

            return items;
        }
    }
}
