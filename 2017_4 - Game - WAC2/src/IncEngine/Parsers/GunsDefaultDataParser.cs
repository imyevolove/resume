﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;
using IncEngine.Parsers;
using Uni.DependencyInjection;
using Wac.Resources;
using UnityEngine;
using System.Linq;

namespace IncEngine.Persers
{
    public class GunsDefaultDataParser : IParser<List<Entity>>
    {
        /// <summary>
        /// Parse present text to gloves list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Entity> Parse(string text)
        {
            List<Entity> items = new List<Entity>();

            var json = JSON.Parse(text);

            foreach (JSONNode node in json.AsArray)
            {
                items.Add(ParseEntity(node));
            }

            return items;
        }

        private Entity ParseEntity(JSONNode node)
        {
            using (var stats = GameDataParserUtility.ParseStatsFromJSONNode(node))
            {
                return new Entity(stats.Damage, stats.Income)
                {
                    Type = EntityType.Gun,
                    Id = node["id"].Value,
                    Name = node["name"].Value,
                    Icon = ResourcesUtility.LoadSpriteFromApplicationResources(node["image_id"].Value),
                    Price = stats.Price
                };
            }
        }
    }
}
