﻿using System.Text.RegularExpressions;

namespace IncEngine.Persers
{
    public class RegexParserChain<T> : RegexParser<T>
    {
        public RegexParser<T> NextParser;

        public RegexParserChain(Regex regex, T value)
            : base(regex, value)
        {
        }

        public RegexParserChain(Regex regex, T value, RegexParser<T> nextParser)
            : base(regex, value)
        {
        }

        public override T Parse(string str)
        {
            if (base.Parse(str) != null) return Value;
            if (NextParser != null) return NextParser.Parse(str);
            return default(T);
        }
    }
}
