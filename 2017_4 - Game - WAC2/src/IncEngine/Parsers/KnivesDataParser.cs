﻿using System.Collections.Generic;
using IncEngine.Structures;
using SimpleJSON;
using IncEngine.Parsers;
using IncEngine.Data;

namespace IncEngine.Persers
{
    public class KnivesDataParser : IParser<List<Skin>>
    {
        private GameData m_GameData;

        public KnivesDataParser(GameData gameData)
        {
            m_GameData = gameData;
        }

        /// <summary>
        /// Parse present text to gloves list
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public List<Skin> Parse(string text)
        {
            List<Skin> items = new List<Skin>();

            var json = JSON.Parse(text);

            foreach (JSONNode node in json.AsArray)
            {
                var original = m_GameData.Entities.GetOriginal(node["originalID"].Value);
                
                /// Original entity not found
                if (original == null) continue;

                var skin = new Skin()
                {
                    Type = EntityType.Knive,
                    Id = node["id"].Value,
                    Name = node["skinName"].Value,
                    FullName = node["name"].Value,
                    Icon = ResourcesUtility.LoadSpriteFromApplicationResources(node["image_id"].Value),
                    Quality = Quality.QualityExceedinglyRare, /// All time Exceedingly Rare
                    Special = SkinSpecial.Parse(node["signature"].Value),
                    Original = original
                };

                GameDataParserUtility.ParseDetailsFromJSONArray(node["collections"].AsArray, skin);
                skin.RecalculateMinMaxPrices();

                /// Add skin
                items.Add(skin);
            }

            return items;
        }
    }
}
