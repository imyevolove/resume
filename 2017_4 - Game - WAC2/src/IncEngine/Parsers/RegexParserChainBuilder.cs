﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace IncEngine.Persers
{
    public class RegexParserChainBuilder<T> : IRegexParserChainBuilder<T>
    {
        private Dictionary<T, Regex> parserParameters = new Dictionary<T, Regex>();

        public IRegexParserChainBuilder<T> AddChain(Regex regex, T value)
        {
            parserParameters[value] = regex;
            return this;
        }

        public RegexParserChain<T> Build()
        {
            RegexParserChain<T> rootParser = null;
            RegexParserChain<T> nextParser = null;

            foreach (var parser in parserParameters)
            {
                var cParser = new RegexParserChain<T>(parser.Value, parser.Key);

                if (rootParser == null)
                {
                    rootParser = cParser;
                    nextParser = rootParser;
                    continue;
                }

                nextParser.NextParser = cParser;
                nextParser = cParser;
            }

            return rootParser;
        }
    }
}
