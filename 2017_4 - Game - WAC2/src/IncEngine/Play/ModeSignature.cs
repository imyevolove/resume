﻿namespace IncEngine.Play
{
    public struct ModeSignature
    {
        public readonly string Name;

        public ModeSignature(string name)
        {
            Name = name;
        }
    }
}
