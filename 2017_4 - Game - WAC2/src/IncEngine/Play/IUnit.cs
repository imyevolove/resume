﻿using System;

namespace IncEngine.Play
{
    public interface IUnit
    {
        event Action<IUnit> Event_Destroy;
        event Action<IUnit> Event_HealthChange;
        
        bool        Destroyed   { get; }
        ulong       Health      { get; }
        ulong       MaxHealth   { get; set; }
        UnitDetails Details     { get; }

        void Destroy();
        void Damage(ulong value);
        void Heal(ulong value);
        void HealMax();

        void Configure(ulong maxHealth);
        void Configure(IUnitHealthCalculator calculator, ulong index);
    }
}
