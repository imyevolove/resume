﻿namespace IncEngine.Play
{
    public interface IStage
    {
        ulong   Level   { get; }
        ITask   CurrentTask    { get; }

        void SetLevel(ulong level);
        void SetWaveIndex(uint index);

        Wave GetWave();
    }
}
