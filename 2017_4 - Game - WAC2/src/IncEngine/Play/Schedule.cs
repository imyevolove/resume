﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace IncEngine.Play
{
    public class Schedule
    {
        public readonly List<ScheduleUnit> Timeline = new List<ScheduleUnit>();

        public ulong CyclesCount { get { return (ulong)Timeline.Sum(unit => unit.Period); } }
        public ulong TasksIterationSummary { get { return (ulong)Timeline.Sum(unit => unit.Task.IterationsCount); } }
        public ulong CyclesIterationSummary { get { return (ulong)Timeline.Sum(unit => unit.Task.IterationsCount * unit.Period); } }

        public Schedule(params ScheduleUnit[] units)
        {
            if (units == null) return;

            foreach (var unit in units)
            {
                Set(unit.Task, unit.Period);
            }
        }

        public ulong GetTaskIterations(ulong limit)
        {
            var res = 0UL;
            var weight = 0UL;
            
            for (var i = 0; i < Timeline.Count; i++)
            {
                weight += Timeline[i].Period;

                if (weight >= limit)
                {
                    ///Debug.LogFormat("Period: {0}, Multiplier: {1} Limit: {2}", Timeline[i].Period, weight - (weight - limit), limit);
                    
                    res += Timeline[i].Task.IterationsCount * (weight - (weight - limit));
                    break;
                }

                res += Timeline[i].Task.IterationsCount * Timeline[i].Period;
            }
            
            return res;
        }

        public bool Contains<T>() where T : ITask
        {
            var type = typeof(T);
            return Timeline.Exists(unit => unit.Task.GetType() == type);
        }

        public bool Contains(Type type)
        {
            return Timeline.Exists(unit => unit.Task.GetType() == type);
        }

        public int FindIndex<T>() where T : ITask
        {
            var type = typeof(T);
            return Timeline.FindIndex(unit => unit.Task.GetType() == type);
        }

        public int FindIndex(Type type)
        {
            return Timeline.FindIndex(unit => unit.Task.GetType() == type);
        }

        public bool Remove<T>() where T : ITask
        {
            return Remove(typeof(T));
        }

        public bool Remove(Type type)
        {
            var index = FindIndex(type);

            if (index < 0)
            {
                return false;
            }
            else
            {
                Timeline.RemoveAt(index);
                return true;
            }
        }

        public ScheduleUnit Get<T>() where T : ITask
        {
            var type = typeof(T);
            return Timeline.Find(unit => unit.Task.GetType() == type);
        }

        public ScheduleUnit Get(Type type)
        {
            return Timeline.Find(unit => unit.Task.GetType() == type);
        }

        public bool Move<T>(int index) where T : ITask
        {
            return Move(typeof(T), index);
        }

        public bool Move(Type type, int index)
        {
            var fromIndex = FindIndex(type);

            var cantMove = fromIndex < 0 || Timeline.Count == 0 || fromIndex == index;

            if (cantMove)
            {
                return false;
            }

            var valueFrom = Timeline[fromIndex];
            Timeline.RemoveAt(fromIndex);

            var toIndex = Mathf.Clamp(index, 0, Timeline.Count - 1);
            Timeline.Insert(toIndex, valueFrom);

            return true;
        }

        public void Set(ITask task, uint period)
        {
            ScheduleUnit unit = Get(task.GetType());

            if (unit != null)
            {
                unit.Period = period;
            }
            else
            {
                unit = new ScheduleUnit(task, period);
                Timeline.Add(unit);
            }
        }

        public void Set(ScheduleUnit unit)
        {
            var type = unit.Task.GetType();

            if (Contains(type))
            {
                Remove(type);
            }
            else
            {
                Timeline.Add(unit);
            }
        }

        public ITask Evaluate(ulong time)
        {
            var summary = CyclesCount;
            var value = time % summary;
            
            if (value == 0 && time > 0)
            {
                value = summary;
            }

            /*
            Debug.LogFormat("Sum: {0}, Value: {1}, Time: {2}",
                summary, value, time
                );
            */

            var sum = 0UL;

            foreach (var unit in Timeline)
            {
                sum += unit.Period;
                
                if (sum >= value)
                {
                    return unit.Task;
                }
            }

            return null;
        }
    }
}
