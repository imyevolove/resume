﻿using System;
using UnityEngine;

namespace IncEngine.Play
{
    public class Unit : IUnit
    {
        public event Action<IUnit> Event_Destroy;
        public event Action<IUnit> Event_HealthChange;

        public bool Destroyed { get { return Health <= 0; } }

        private ulong m_Health;
        public ulong Health
        {
            get { return m_Health; }
        }

        private ulong m_MaxHealth;
        public ulong MaxHealth
        {
            get { return m_MaxHealth; }
            set
            {
                m_MaxHealth = value;

                OnHealthChange();
                CheckHealth();
            }
        }
        
        public UnitDetails Details { get; private set; }

        public void Destroy()
        {
            m_Health = 0;

            OnHealthChange();
            CheckHealth();
        }

        public void Damage(ulong value)
        {
            value = (ulong)Mathf.Clamp(value, 0, MaxHealth);

            try
            {
                m_Health = checked(m_Health - value);
            }
            catch (Exception)
            {
                m_Health = 0;
            }

            OnHealthChange();
            CheckHealth();
        }

        public void Heal(ulong value)
        {
            value = (ulong)Mathf.Clamp(value, 0, MaxHealth);

            try
            {
                m_Health = checked(m_Health + value);
            }
            catch (Exception)
            {
                m_Health = ulong.MaxValue;
            }

            OnHealthChange();
            CheckHealth();
        }

        public void HealMax()
        {
            m_Health = m_MaxHealth;

            OnHealthChange();
            CheckHealth();
        }

        public Unit(UnitDetails details)
        {
            Details     = details;
            m_MaxHealth = 1;
            m_Health    = 1;
        }

        public Unit(UnitDetails details, ulong health)
        {
            Details     = details;
            m_MaxHealth = health;
            m_Health    = health;
        }

        public void Configure(ulong maxHealth)
        {
            m_MaxHealth = maxHealth;
            m_Health = m_MaxHealth;

            OnHealthChange();
            CheckHealth();
        }

        public void Configure(IUnitHealthCalculator calculator, ulong index)
        {
            m_MaxHealth = calculator.Calculate(index);
            m_Health = m_MaxHealth;

            OnHealthChange();
            CheckHealth();
        }

        protected void CheckHealth()
        {
            if (m_Health <= 0)
            {
                OnHealthSpent();
            }
        }

        protected virtual void OnHealthSpent()
        {
            if (Event_Destroy != null)
            {
                Event_Destroy(this);
            }
        }

        protected virtual void OnHealthChange()
        {
            if (Event_HealthChange != null)
            {
                Event_HealthChange(this);
            }
        }
    }
}
