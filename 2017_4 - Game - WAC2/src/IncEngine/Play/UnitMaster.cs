﻿using System;

namespace IncEngine.Play
{
    public class UnitMaster : IUnit
    {
        private IUnit m_Unit;

        public event Action<IUnit> Event_Destroy;
        public event Action<IUnit> Event_HealthChange;

        public bool Destroyed { get { return m_Unit.Destroyed; } }

        public ulong Health
        {
            get { return m_Unit.Health; }
        }

        public ulong MaxHealth
        {
            get { return m_Unit.MaxHealth; }
            set { m_Unit.MaxHealth = value; }
        }

        public UnitDetails Details { get { return m_Unit.Details; } }

        public void Destroy()
        {
            m_Unit.Destroy();
        }

        public void HealMax()
        {
            m_Unit.HealMax();
        }

        public void Damage(ulong value)
        {
            m_Unit.Damage(value);
        }

        public void Heal(ulong value)
        {
            m_Unit.Heal(value);
        }

        public void SetTargetUnit(IUnit unit)
        {
            UnregisterEventListeners(m_Unit);
            RegisterEventListeners(unit);

            m_Unit = unit;
        }

        public void Configure(ulong maxHealth)
        {
            m_Unit.Configure(maxHealth);
        }

        public void Configure(IUnitHealthCalculator calculator, ulong index)
        {
            m_Unit.Configure(calculator, index);
        }

        protected virtual void RegisterEventListeners(IUnit unit)
        {
            if (unit == null) return;

            unit.Event_HealthChange += OnUnitEventHealthChange;
            unit.Event_Destroy  += OnUnitEventHealthSpent;
        }

        protected virtual void UnregisterEventListeners(IUnit unit)
        {
            if (unit == null) return;

            unit.Event_HealthChange -= OnUnitEventHealthChange;
            unit.Event_Destroy  -= OnUnitEventHealthSpent;
        }

        protected virtual void OnUnitEventHealthChange(IUnit unit)
        {
            if (Event_HealthChange != null)
            {
                Event_HealthChange(unit);
            }
        }

        protected virtual void OnUnitEventHealthSpent(IUnit unit)
        {
            if (Event_Destroy != null)
            {
                Event_Destroy(unit);
            }
        }
    }
}
