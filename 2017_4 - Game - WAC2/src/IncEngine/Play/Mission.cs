﻿using IncEngine.Personal;
using IncEngine.Storage;
using IncEngine.UpdateManagement;
using System;
using UnityEngine;
using Wac;

namespace IncEngine.Play
{
    public class Mission : IStorage
    {
        public readonly Stage       Stage;
        public readonly UnitMaster  Unit = new UnitMaster();
        public          Wave        Wave { get { return Stage.GetWave(); } }

        public event Action<ulong>  Event_GiveReward;
        public event Action<ulong>  Event_LevelChange;
        public event Action         Event_WaveChange;
        public event Action<IUnit>  Event_UnitChange;
        public event Action<IUnit>  Event_UnitDefeat;

        public event Action<IUnit>  Event_WaveLevelComplete;

        private readonly Player Player;
        private Coroutine PlayerDamagePerSecondCoroutine;

        public Mission(Stage stage)
        {
            Player = Game.Player;
            Stage = stage;

            Stage.Event_UnitChange  += OnUnitChange;
            Stage.Event_LevelChange += OnLevelChange;
            Stage.Event_WaveChange  += OnWaveChange;
            Stage.Event_WaveNext    += OnWaveLevelComplete;
            Stage.Event_UnitDefeat  += OnUnitDefeat;

            ReconfigureUnit();
        }

        public void EnablePlayerDamagePerSecond()
        {
            DisablePlayerDamagePerSecond();
            PlayerDamagePerSecondCoroutine = UpdateManager.StartRepeate(1, DamageUnitFromPlayerDPS);
        }

        public void DisablePlayerDamagePerSecond()
        {
            if (PlayerDamagePerSecondCoroutine == null) return;

            UpdateManager.StopCoroutine(PlayerDamagePerSecondCoroutine);
            PlayerDamagePerSecondCoroutine = null;
        }

        public void LevelFallback()
        {
            Stage.Fallback();
        }

        public bool DamageUnit(ulong value)
        {
            if (Stage.IsPause) return false;

            Unit.Damage(value);
            return true;
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["level"] = Stage.Level;
            data["wave_index"] = Stage.GetWave().Index;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return false;

            if (storageData.KeyValueIs<ulong>("level"))
            {
                Stage.SetLevel((ulong)storageData["level"]);
            }

            if (storageData.KeyValueIs<uint>("wave_index"))
            {
                Stage.SetWaveIndex((uint)storageData["wave_index"]);
            }

            return true;
        }

        /// <summary>
        /// Update unit configuration
        /// </summary>
        protected void ReconfigureUnit()
        {
            ReconfigureUnit(Stage.CurrentUnit);
        }

        protected void GiveUnitReward(IUnit unit, ITask task)
        {
            var reward = task.UnitRewardCalculator.Calculate(unit, Stage.IterationLevel);
            Player.Money.Increase(reward);

            OnGiveReward(reward);
        }

        /// <summary>
        /// Update unit configuration
        /// </summary>
        /// <param name="unit"></param>
        protected void ReconfigureUnit(IUnit unit)
        {
            Unit.SetTargetUnit(unit);
        }
        
        /// <summary>
        /// Called when level was changed
        /// </summary>
        /// <param name="level"></param>
        protected virtual void OnLevelChange(ulong level)
        {
            if (Event_LevelChange != null)
            {
                Event_LevelChange(level);
            }
        }

        /// <summary>
        /// Called when wave was changed
        /// </summary>
        protected virtual void OnWaveChange()
        {
            if (Event_WaveChange != null)
            {
                Event_WaveChange();
            }
        }

        /// <summary>
        /// Called when wave was complete level
        /// </summary>
        protected virtual void OnWaveLevelComplete(IUnit current, IUnit next)
        {
            if (Event_WaveLevelComplete != null)
            {
                Event_WaveLevelComplete(current);
            }
        }

        /// <summary>
        /// Called when unit was changed
        /// </summary>
        /// <param name="unit"></param>
        protected virtual void OnUnitChange(IUnit unit)
        {
            ReconfigureUnit(unit);

            if (Event_UnitChange != null)
            {
                Event_UnitChange(unit);
            }
        }

        /// <summary>
        /// Called when unit was defeat
        /// </summary>
        /// <param name="unit"></param>
        protected virtual void OnUnitDefeat(IUnit unit, ITask task)
        {
            GiveUnitReward(unit, task);
            GiveExperience((ulong)(Math.Log(unit.MaxHealth, 2) * 1.5f));

            if (Event_UnitDefeat != null)
            {
                Event_UnitDefeat(unit);
            }
        }

        protected virtual void OnGiveReward(ulong reward)
        {
            if (Event_GiveReward != null)
            {
                Event_GiveReward(reward);
            }
        }

        private void GiveExperience(ulong value)
        {
            Player.RankSystem.IncreaseExperience(value);
        }

        private void DamageUnitFromPlayerDPS()
        {
            DamageUnit(Player.GetIncome());
        }
    }
}
