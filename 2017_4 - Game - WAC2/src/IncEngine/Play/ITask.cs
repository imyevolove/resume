﻿using System.Collections.Generic;

namespace IncEngine.Play
{
    public interface ITask
    {
        string                  Name                    { get; }
        IMissionMode            Mode                    { get; }
        uint                    IterationsCount         { get; }
        List<UnitDetails>       AvailableUnitDetails    { get; }
        IUnitHealthCalculator   UnitHealthCalculator    { get; }
        IUnitRewardCalculator   UnitRewardCalculator    { get; }

        void Reconfigure();
        void Reconfigure(params object[] args);

        bool Compare(object obj);
    }
}
