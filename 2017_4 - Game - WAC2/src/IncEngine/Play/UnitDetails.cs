﻿using System;
using UnityEngine;

namespace IncEngine.Play
{
    [Serializable]
    public struct UnitDetails
    {
        [SerializeField] public Sprite Image;
        [SerializeField] public Sprite Icon;
    }
}
