﻿namespace IncEngine.Play
{
    public enum MissionModeState
    {
        Running,
        Idle,
        Pause
    }
}
