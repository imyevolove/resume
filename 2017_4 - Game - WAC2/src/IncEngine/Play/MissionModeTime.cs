﻿using IncEngine.UpdateManagement;
using UnityEngine;

namespace IncEngine.Play
{
    public class MissionModeTime : MissionMode
    {
        private Coroutine m_TimerCoroutine;
        
        public int TimerValue  { get; protected set; }
        public int TimerMax    { get; protected set; }

        public MissionModeTime(ModeSignature signature, int time)
            : base(signature)
        {
            TimerMax = time;
        }

        public Wave TargetWave { get { return TargetStage.GetWave(); } }

        public override string GetDetailsString()
        {
            var minutes = (TimerValue) / 60;
            var seconds = (TimerValue) % 60;

            return FormatTimerNumber(minutes) + ":" + FormatTimerNumber(seconds);
        }

        protected void RegisterEventListeners(IStage stage)
        {
            if (stage == null) return;

            stage.GetWave().Event_WaveEnd += OnWaveEnd;
        }

        protected void UnregisterEventListeners(IStage stage)
        {
            if (stage == null) return;

            stage.GetWave().Event_WaveEnd -= OnWaveEnd;
        }

        protected void OnWaveEnd()
        {
            End();
        }

        protected override void StartCore()
        {
            RegisterEventListeners(TargetStage);
            ResetTimer();
            StartTimer();
        }

        protected override void EndCore()
        {
            UnregisterEventListeners(TargetStage);
            StopTimer();
        }

        protected override void FallbackCore()
        {
            UnregisterEventListeners(TargetStage);
            StopTimer();
        }
        
        protected override void PauseCore()
        {
            UnregisterEventListeners(TargetStage);
            StopTimer();
        }

        protected override void ResumeCore()
        {
            RegisterEventListeners(TargetStage);
            StartTimer();
        }

        protected void StopTimer()
        {
            if (m_TimerCoroutine == null) return;

            UpdateManager.StopCoroutine(m_TimerCoroutine);
            m_TimerCoroutine = null;
        }

        protected void StartTimer()
        {
            StopTimer();
            m_TimerCoroutine = UpdateManager.StartRepeate(1, TimerNext);
        }

        protected void ResetTimer()
        {
            TimerValue = TimerMax;
        }

        protected void TimerNext()
        {
            TimerValue--;
            OnChange();
            CheckTimer();
        }

        protected void CheckTimer()
        {
            if (TimerValue <= 0)
            {
                StopTimer();
                Fallback();
            }
        }

        protected string FormatTimerNumber(int number)
        {
            var result = number.ToString();
            return result.Length >= 2 ? result : "0" + result;
        }

        protected override void RestartCore()
        {
            
        }
    }
}
