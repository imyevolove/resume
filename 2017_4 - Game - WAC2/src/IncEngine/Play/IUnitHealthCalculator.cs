﻿namespace IncEngine.Play
{
    public interface IUnitHealthCalculator
    {
        ulong Calculate(ulong index);
        void Calculate(IUnit unit, ulong index);
    }
}
