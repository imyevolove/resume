﻿using System;
using System.Collections.Generic;

namespace IncEngine.Play
{
    public class TaskSigned<T> : Task where T : struct
    {
        public override string Name { get { return Sign.ToString(); } }
        public T Sign { get; set; }

        public TaskSigned(
            uint iterationsCount, 
            MissionMode mode, 
            T sign, 
            IUnitHealthCalculator healthCalculator, 
            IUnitRewardCalculator rewardCalculator,
            List<UnitDetails> unitDetails) 
            : base(iterationsCount, mode, healthCalculator, rewardCalculator, unitDetails)
        {
            Sign = sign;
        }

        public TaskSigned(
            uint iterationsCount, 
            MissionMode mode, T sign, 
            IUnitHealthCalculator healthCalculator,
            IUnitRewardCalculator rewardCalculator,
            params UnitDetails[] unitDetails) 
            : base(iterationsCount, mode, healthCalculator, rewardCalculator, unitDetails)
        {
            Sign = sign;
        }

        /// <summary>
        /// Compare sign
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Compare(object obj)
        {
            return Sign.Equals(obj);
        }

        /// <summary>
        /// Compare sign task with current task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool Compare<TS>(TaskSigned<TS> task) where TS : struct
        {
            return Compare(this, task);
        }

        /// <summary>
        /// Compare sign task with current task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool Compare(TaskSigned<T> task)
        {
            return Compare<T>(this, task);
        }

        /// <summary>
        /// Compare two tasks
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static bool Compare<TS>(TaskSigned<TS> a, TaskSigned<TS> b) where TS : struct
        {
            return a.Sign.Equals(b.Sign);
        }

        /// <summary>
        /// Compare two tasks
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static bool Compare<TS1, TS2>(TaskSigned<TS1> a, TaskSigned<TS2> b) 
            where TS1 : struct
            where TS2 : struct
        {
            return a.Sign.Equals(b.Sign);
        }
    }
}
