﻿using System.Collections.Generic;

namespace IncEngine.Play
{
    public class Task : ITask
    {
        public virtual string           Name                    { get; set;         }
        public IMissionMode             Mode                    { get; private set; }
        public uint                     IterationsCount         { get; private set; }
        public List<UnitDetails>        AvailableUnitDetails    { get; private set; }
        public IUnitHealthCalculator    UnitHealthCalculator    { get; private set; }
        public IUnitRewardCalculator    UnitRewardCalculator    { get; private set; }

        public Task(
            uint iterationsCount, 
            MissionMode mode, 
            IUnitHealthCalculator healthCalculator, 
            IUnitRewardCalculator rewardCalculator, 
            List<UnitDetails> unitDetails)
        {
            IterationsCount         = iterationsCount;
            Mode                    = mode;
            UnitHealthCalculator    = healthCalculator;
            UnitRewardCalculator    = rewardCalculator;
            AvailableUnitDetails    = unitDetails ?? new List<UnitDetails>();
        }

        public Task(
            uint iterationsCount, 
            MissionMode mode, 
            IUnitHealthCalculator healthCalculator,
            IUnitRewardCalculator rewardCalculator,
            params UnitDetails[] unitDetails)
        {
            IterationsCount         = iterationsCount;
            Mode                    = mode;
            UnitHealthCalculator    = healthCalculator;
            UnitRewardCalculator    = rewardCalculator;
            AvailableUnitDetails    = new List<UnitDetails>();

            if (unitDetails != null)
            {
                foreach (var unit in unitDetails)
                {
                    AvailableUnitDetails.Add(unit);
                }
            }
        }
        
        public void Reconfigure()
        {
            /// Nothing todo
        }

        public void Reconfigure(params object[] args)
        {
            /// Nothing todo
        }

        public virtual bool Compare(object obj)
        {
            return Equals(obj);
        }
    }
}
