﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace IncEngine.Play
{
    public class Wave
    {
        public event Action Event_WaveChange;
        public event Action Event_WaveEnd;
        public event Action<IUnit, IUnit> Event_WaveNext;
        public event Action<IUnit> Event_UnitDefeat;

        private uint m_Max = 1;
        public uint Max
        {
            get { return m_Max; }
            protected set
            {
                m_Max = value;

                /// Can't be zero
                if (m_Max == 0)
                {
                    m_Max = 1;
                }

                OnChange();
            }
        }

        private uint m_index;
        public uint Index
        {
            get { return m_index; }
            protected set
            {
                var newValue = (uint)Mathf.Clamp(value, 0, Max);
                
                if (newValue != Index)
                {
                    m_index = newValue;
                    OnChange();

                    if (Index >= Max)
                    {
                        OnEnd();
                    }
                }
            }
        }

        public bool IsEnded { get { return Index >= Max; } }

        private List<IUnit> m_Units = new List<IUnit>();
        public IUnit CurrentUnit { get; protected set; }
        
        public Wave()
        { 
        }

        public Wave(ICollection<UnitDetails> unitDetails, uint count)
        {
            Configure(unitDetails, count);
        }

        public void SetIndex(uint index)
        {
            if (index < 0)   index = 0;
            if (index > Max) index = Max;

            Index = index;

            var count = Index;

            for (var i = 0; i < count; i++)
            {
                if (!m_Units[i].Destroyed)
                {
                    m_Units[i].Destroy();
                }
            }
        }

        /// <summary>
        /// Return readonly units collection
        /// </summary>
        /// <returns></returns>
        public List<IUnit> GetUnits()
        {
            return m_Units;
        }

        public void Configure(ICollection<UnitDetails> unitDetails, uint count)
        {
            m_Max = count;
            m_index = 0;
            
            GenerateUnits(unitDetails);
            OnChange();
        }

        public uint Next()
        {
            Index = Index + 1;
            return Index;
        }

        protected void AddUnitEventListeners(IUnit unit)
        {
            if (unit == null) return;

            RemoveUnitEventListeners(unit);

            unit.Event_Destroy += OnCurrentUnitHealthSpent;
        }

        protected void RemoveUnitEventListeners(IUnit unit)
        {
            if (unit == null) return;
            
            unit.Event_Destroy -= OnCurrentUnitHealthSpent;
        }

        protected void UpdateCurrentUnit()
        {
            RemoveUnitEventListeners(CurrentUnit);
            CurrentUnit = GetNextUnit();
            AddUnitEventListeners(CurrentUnit);
        }

        protected IUnit GetNextUnit()
        {
            var index = (int)Index;

            if (index == Max)
            {
                index = index - 1;
            }

            return m_Units[index];
        }

        protected virtual void GenerateUnits(ICollection<UnitDetails> unitDetails)
        {
            m_Units.Clear();

            if (unitDetails == null)
            {
                unitDetails = new List<UnitDetails>();
            }

            if (unitDetails.Count == 0)
            {
                unitDetails.Add(new UnitDetails());
            }

            var unitDetailsCount = unitDetails.Count;

            for (var i = 0; i < Max; i++)
            {
                var details = unitDetails.ElementAt(UnityEngine.Random.Range(0, unitDetailsCount));
                m_Units.Add(new Unit(details));
            }
        }

        protected virtual void OnCurrentUnitHealthSpent(IUnit unit)
        {
            if (unit.MaxHealth > 0)
            {
                OnUnitDefeat(unit);
            }

            Next();
        }

        protected virtual void OnChange()
        {
            OnNext(CurrentUnit, GetNextUnit());
            UpdateCurrentUnit();

            if (Event_WaveChange != null)
            {
                Event_WaveChange();
            }
        }

        protected virtual void OnUnitDefeat(IUnit unit)
        {
            if (Event_UnitDefeat != null)
            {
                Event_UnitDefeat(unit);
            }
        }

        protected virtual void OnEnd()
        {
            if (Event_WaveEnd != null)
            {
                Event_WaveEnd();
            }
        }

        protected virtual void OnNext(IUnit previousUnit, IUnit nextUnit)
        {
            if (Event_WaveNext != null)
            {
                Event_WaveNext(previousUnit, nextUnit);
            }
        }
    }
}
