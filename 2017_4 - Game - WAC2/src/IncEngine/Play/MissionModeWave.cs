﻿namespace IncEngine.Play
{
    public class MissionModeWave : MissionMode
    {
        public MissionModeWave(ModeSignature signature) 
            : base(signature)
        {
        }

        public Wave TargetWave { get { return TargetStage.GetWave(); } }
        
        public override string GetDetailsString()
        {
            return (TargetWave.Index) + " / " + TargetWave.Max;
        }

        protected void RegisterEventListeners(IStage stage)
        {
            if (stage == null) return;

            var wave = stage.GetWave();
            wave.Event_WaveEnd      += End;
            wave.Event_WaveChange   += OnChange;
        }

        protected void UnregisterEventListeners(IStage stage)
        {
            if (stage == null) return;

            var wave = stage.GetWave();
            wave.Event_WaveEnd      -= End;
            wave.Event_WaveChange   -= OnChange;
        }

        protected override void StartCore()
        {
            RegisterEventListeners(TargetStage);
        }

        protected override void EndCore()
        {
            UnregisterEventListeners(TargetStage);
        }

        protected override void FallbackCore()
        {
            UnregisterEventListeners(TargetStage);
        }

        protected override void PauseCore()
        {
            //UnregisterEventListeners(TargetStage);
        }

        protected override void ResumeCore()
        {
            //RegisterEventListeners(TargetStage);
        }

        protected override void RestartCore()
        {

        }
    }
}
