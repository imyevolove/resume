﻿namespace IncEngine.Play
{
    public interface IUnitRewardCalculator
    {
        ulong Calculate(IUnit unit, ulong index);
    }
}
