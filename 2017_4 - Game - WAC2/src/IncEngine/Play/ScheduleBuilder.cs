﻿namespace IncEngine.Play
{
    public class ScheduleBuilder
    {
        private Schedule m_Schedule = new Schedule();

        public ScheduleBuilder AddScheduleUnit(ScheduleUnit unit)
        {
            m_Schedule.Set(unit);
            return this;
        }

        public ScheduleBuilder AddScheduleUnit(ITask task, uint period)
        {
            m_Schedule.Set(task, period);
            return this;
        }

        public Schedule Build()
        {
            return m_Schedule;
        }
    }
}
