﻿using System.Collections.Generic;

namespace IncEngine.Play
{
    public class TaskRand : ITask
    {
        public string                   Name                    { get { return m_Task.Name;                 } }
        public IMissionMode             Mode                    { get { return m_Task.Mode;                 } }
        public uint                     IterationsCount         { get { return m_Task.IterationsCount;      } }
        public List<UnitDetails>        AvailableUnitDetails    { get { return m_Task.AvailableUnitDetails; } }
        public IUnitHealthCalculator    UnitHealthCalculator    { get { return m_Task.UnitHealthCalculator; } }
        public IUnitRewardCalculator    UnitRewardCalculator    { get { return m_Task.UnitRewardCalculator; } }

        public readonly List<ITask> Tasks = new List<ITask>();

        private ITask m_Task;

        public TaskRand(params ITask[] tasks)
        {
            foreach (var task in tasks)
            {
                Tasks.Add(task);
            }

            Reconfigure();
        }

        public void Reconfigure()
        {
            m_Task = Tasks[UnityEngine.Random.Range(0, Tasks.Count)];
        }
        
        public void Reconfigure(params object[] args)
        {
            if (args == null || args.Length == 0)
            {
                Reconfigure();
                return;
            }

            if (args[0] is uint)
            {
                m_Task = Tasks[(int)((uint)args[0] % Tasks.Count)];
                return;
            }

            Reconfigure();
        }

        public bool Compare(object obj)
        {
            return m_Task.Compare(obj);
        }
    }
}
