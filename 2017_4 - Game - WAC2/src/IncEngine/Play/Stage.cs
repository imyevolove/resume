﻿using IncEngine.UpdateManagement;
using System;
using UnityEngine;

namespace IncEngine.Play
{
    public class Stage : IStage
    {
        public event Action<ulong> Event_LevelChange;
        public event Action Event_LevelRestart;
        public event Action<ITask> Event_TaskChange;
        public event Action Event_TaskComplete;
        public event Action Event_WaveChange;
        public event Action Event_WaveEnd;
        public event Action Event_WaveUnitsUpdate;
        public event Action<IUnit> Event_UnitChange;
        public event Action<IUnit, ITask> Event_UnitDefeat;
        public event Action Event_TaskModeChangeTick;
        public event Action<IUnit, IUnit> Event_WaveNext;

        public bool IsPause { get; protected set; }

        public Schedule Schedule { get; private set; }

        private readonly Wave Wave;

        private ulong m_Level;
        public ulong Level
        {
            get { return m_Level; }
            protected set
            {
                m_Level = value;
            }
        }

        public ulong IterationLevel
        {
            get
            {
                var readyCycles = (ulong)Mathf.Floor(Level / Schedule.CyclesCount);
                var completeIterations = readyCycles * Schedule.CyclesIterationSummary;
                var readyCycleIterations = Schedule.GetTaskIterations(Level % Schedule.CyclesCount);
                var readyIterations = readyCycleIterations + Wave.Index;
                var result = completeIterations + readyIterations;
                
                /*
                Debug.LogFormat(
                    "Cycles: {0}, " +
                    "Iterations: {1}, " +
                    "Ready cycles: {2}, " +
                    "Complete Iterations: {3}, " +
                    "Ready Iterations: {4}, " +
                    "Current Index: {5}, " +
                    "Ready cycle iterations: {6}, " +
                    "Level: {7}, " +
                    "Level Di: {8}",
                    Schedule.CyclesCount,
                    Schedule.CyclesIterationSummary,
                    readyCycles,
                    completeIterations,
                    readyIterations,
                    result,
                    readyCycleIterations,
                    Level,
                    Level % Schedule.CyclesCount);
                    */

                return result;
            }
        }

        public ITask CurrentTask { get; protected set; }
        public IUnit CurrentUnit { get { return Wave.CurrentUnit; } }

        public Stage(Schedule schedule)
        {
            IsPause = false;

            Schedule = schedule;
            Wave = new Wave();

            PrepareLevelStage();
            RegisterEventListeners();

            Pause();
        }

        public void SetLevel(ulong level)
        {
            Level = level;

            PrepareLevelStage();
            OnLevelChange(Level);
        }

        public void SetWaveIndex(uint index)
        {
            Wave.SetIndex(index);
        }

        public void Pause()
        {
            if (IsPause) return;
            IsPause = true;
            
            CurrentTask.Mode.Pause();
        }

        public void Continue()
        {
            if (!IsPause) return;
            IsPause = false;
            
            CurrentTask.Mode.Restart();
        }

        /// <summary>
        /// Next level
        /// </summary>
        public ulong Next()
        {
            Level++;

            Continue();
            PrepareLevelStage();
            OnLevelChange(Level);

            return Level;
        }

        public ulong Fallback()
        {
            if (Level == 0)
            {
                Debug.Log("Can't fallback. Level is zero");
                return Level;
            }

            Level--;

            PrepareLevelStage();
            OnLevelChange(Level);

            return Level;
        }

        /// <summary>
        /// Return wave
        /// </summary>
        /// <returns></returns>
        public Wave GetWave()
        {
            return Wave;
        }

        /// <summary>
        /// Wave recofigurator
        /// </summary>
        protected void ReconfigureWave()
        {
            Wave.Configure(CurrentTask.AvailableUnitDetails, CurrentTask.IterationsCount);
            OnWaveUnitsUpdate();
        }

        /// <summary>
        /// Reconfigure unit and dispatch events
        /// </summary>
        protected void ReconfigureUnit()
        {
            Wave.CurrentUnit.Configure(CurrentTask.UnitHealthCalculator, IterationLevel);
            OnUnitChange(Wave.CurrentUnit);
        }

        /// <summary>
        /// Update task and dispatch events
        /// </summary>
        protected void UpdateTask()
        {
            var task = Schedule.Evaluate(Level + 1);

            if (CurrentTask != null)
            {
                CurrentTask.Mode.Event_ModeEnd      -= NextTask;
                CurrentTask.Mode.Event_ModeRestart  -= RestartLevel;
                CurrentTask.Mode.Event_ModeFallback -= RestartLevelDelay;
                CurrentTask.Mode.Event_ModeChange   -= OnTaskModeTickChange;
            }

            if (task != null)
            {
                CurrentTask = task;
                CurrentTask.Reconfigure(Level + 1);
                CurrentTask.Mode.Start(this, true);

                CurrentTask.Mode.Event_ModeEnd      += NextTask;
                CurrentTask.Mode.Event_ModeRestart  += RestartLevel;
                CurrentTask.Mode.Event_ModeFallback += RestartLevelDelay;
                CurrentTask.Mode.Event_ModeChange   += OnTaskModeTickChange;

                OnTaskChange(task);
            }
        }

        protected void NextTask()
        {
            OnTaskComplete();
            ContinueMissionDelay();
        }

        /// <summary>
        /// Events register method
        /// </summary>
        protected virtual void RegisterEventListeners()
        {
            Wave.Event_WaveChange   += OnWaveChange;
            Wave.Event_WaveEnd      += OnWaveEnd;
            Wave.Event_WaveNext     += OnWaveNext;
            Wave.Event_UnitDefeat   += OnUnitDefeat;
        }

        protected void PrepareLevelStage()
        {
            UpdateTask();
            ReconfigureWave();
        }

        protected void ContinueMissionDelay()
        {
            Pause();
            UpdateManager.DelayedExecution(ContinueMission, 2);
        }

        protected void ContinueMission()
        {
            Next();
        }

        protected void RestartLevelDelay()
        {
            Pause();
            UpdateManager.DelayedExecution(RestartLevel, 2);
        }

        protected void RestartLevel()
        {
            Continue();

            Wave.CurrentUnit.HealMax();

            OnLevelChange(Level);
            OnLevelRestart();
        }

        /// <summary>
        /// Called when task call change event
        /// </summary>
        protected virtual void OnTaskModeTickChange()
        {
            if (Event_TaskModeChangeTick != null)
            {
                Event_TaskModeChangeTick();
            }
        }

        /// <summary>
        /// Called when task was completed
        /// </summary>
        protected virtual void OnTaskComplete()
        {
            if (Event_TaskComplete != null)
            {
                Event_TaskComplete();
            }
        }

        /// <summary>
        /// Called when level was changed
        /// </summary>
        /// <param name="level"></param>
        protected virtual void OnLevelChange(ulong level)
        {
            if (Event_LevelChange != null)
            {
                Event_LevelChange(Level);
            }
        }

        /// <summary>
        /// Called when task was changed
        /// </summary>
        /// <param name="task"></param>
        protected virtual void OnTaskChange(ITask task)
        {
            if (Event_TaskChange != null)
            {
                Event_TaskChange(task);
            }
        }

        /// <summary>
        /// Called when wave was changed
        /// </summary>
        protected virtual void OnWaveChange()
        {
            if (!Wave.IsEnded)
            {
                ReconfigureUnit();
            }

            if (Event_WaveChange != null)
            {
                Event_WaveChange();
            }
        }

        /// <summary>
        /// Called when wave was end
        /// </summary>
        protected virtual void OnWaveEnd()
        {
            if (Event_WaveEnd != null)
            {
                Event_WaveEnd();
            }
        }

        /// <summary>
        /// Called when units was updated
        /// </summary>
        protected virtual void OnWaveUnitsUpdate()
        {
            if (Event_WaveUnitsUpdate != null)
            {
                Event_WaveUnitsUpdate();
            }
        }

        /// <summary>
        /// Called when unit was changed
        /// </summary>
        protected virtual void OnUnitChange(IUnit unit)
        {
            if (Event_UnitChange != null)
            {
                Event_UnitChange(unit);
            }
        }

        /// <summary>
        /// Called when unit was defeat
        /// </summary>
        protected virtual void OnUnitDefeat(IUnit unit)
        {
            if (Event_UnitDefeat != null)
            {
                Event_UnitDefeat(unit, CurrentTask);
            }
        }

        /// <summary>
        /// Called when level was restarted
        /// </summary>
        protected virtual void OnLevelRestart()
        {
            if (Event_LevelRestart != null)
            {
                Event_LevelRestart();
            }
        }

        /// <summary>
        /// Called when level was restarted
        /// </summary>
        protected virtual void OnWaveNext(IUnit previous, IUnit current)
        {
            if (Event_WaveNext != null)
            {
                Event_WaveNext(previous, current);
            }
        }
    }
}
