﻿using System;

namespace IncEngine.Play
{
    public interface IMissionMode
    {
        event Action Event_ModeRestart;
        event Action Event_ModeStart;
        event Action Event_ModeEnd;
        event Action Event_ModePause;
        event Action Event_ModeResume;
        event Action Event_ModeFallback;
        event Action Event_ModeChange;

        ModeSignature       Sign        { get; }
        MissionModeState    State       { get; }
        IStage              TargetStage { get; }

        void Start(IStage targetStage, bool forceRestart = false);
        void Fallback(bool preventEvent = false);
        void Pause();
        void Resume();
        void Restart();
        
        string GetDetailsString();
    }
}
