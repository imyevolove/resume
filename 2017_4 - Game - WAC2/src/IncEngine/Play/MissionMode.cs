﻿using System;

namespace IncEngine.Play
{
    public abstract class MissionMode : IMissionMode
    {
        public event Action Event_ModeRestart;
        public event Action Event_ModeStart;
        public event Action Event_ModeEnd;
        public event Action Event_ModePause;
        public event Action Event_ModeResume;
        public event Action Event_ModeFallback;
        public event Action Event_ModeChange;

        public ModeSignature    Sign        { get; private set; }
        public MissionModeState State       { get; protected set; }
        public bool             IsRunning   { get { return State == MissionModeState.Running; } }
        public bool             IsPause     { get { return State == MissionModeState.Pause; } }
        public bool             IsIdle      { get { return State == MissionModeState.Idle; } }

        public IStage TargetStage { get; private set; }

        public MissionMode(ModeSignature signature)
        {
            Sign = signature;
        }

        public void Restart()
        {
            Fallback(true);
            Start(TargetStage, true);

            RestartCore();
            OnRestart();
        }

        public void Start(IStage targetStage, bool forceRestart = false)
        {
            if (IsRunning || IsPause)
            {
                if(!forceRestart) return;
                Fallback();
            }

            State = MissionModeState.Running;
            TargetStage = targetStage;

            StartCore();
            OnStart();
        }

        public void Fallback(bool preventEvent = false)
        {
            if (!IsRunning) return;
            State = MissionModeState.Idle;

            FallbackCore();

            if (!preventEvent)
            {
                OnFallback();
            }
        }
        
        public void Pause()
        {
            if (!IsRunning) return;

            PauseCore();
            OnPause();
        }

        public void Resume()
        {
            if (!IsPause) return;

            ResumeCore();
            OnResume();
        }

        public abstract string GetDetailsString();

        protected void End()
        {
            if (!IsRunning) return;
            State = MissionModeState.Idle;

            EndCore();
            OnEnd();
        }

        protected abstract void RestartCore();
        protected abstract void StartCore();
        protected abstract void EndCore();
        protected abstract void PauseCore();
        protected abstract void ResumeCore();
        protected abstract void FallbackCore();

        protected virtual void OnChange()
        {
            if (Event_ModeChange != null)
            {
                Event_ModeChange();
            }
        }

        protected virtual void OnStart()
        {
            if (Event_ModeStart != null)
            {
                Event_ModeStart();
            }
        }

        protected virtual void OnEnd()
        {
            if (Event_ModeEnd != null)
            {
                Event_ModeEnd();
            }
        }

        protected virtual void OnFallback()
        {
            if (Event_ModeFallback != null)
            {
                Event_ModeFallback();
            }
        }

        protected virtual void OnPause()
        {
            if (Event_ModePause != null)
            {
                Event_ModePause();
            }
        }

        protected virtual void OnResume()
        {
            if (Event_ModeResume != null)
            {
                Event_ModeResume();
            }
        }

        protected virtual void OnRestart()
        {
            if (Event_ModeRestart != null)
            {
                Event_ModeRestart();
            }
        }
    }
}
