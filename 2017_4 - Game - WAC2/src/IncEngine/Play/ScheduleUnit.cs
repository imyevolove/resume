﻿namespace IncEngine.Play
{
    public class ScheduleUnit
    {
        public ITask    Task;
        public uint     Period;

        public ScheduleUnit(ITask task, uint period)
        {
            Task    = task;
            Period  = period;
        }
    }
}
