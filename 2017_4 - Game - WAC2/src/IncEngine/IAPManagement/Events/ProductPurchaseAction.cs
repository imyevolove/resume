﻿namespace IncEngine.IAPManagement.Events
{
    public delegate void ProductPurchaseAction(IProduct product);
}
