﻿namespace IncEngine.IAPManagement.Events
{
    /// <summary>
    /// Product event data
    /// </summary>
    public struct ProductEventData
    {
        public IProduct         product;
        public PurchaseStatus   status;
    }
}
