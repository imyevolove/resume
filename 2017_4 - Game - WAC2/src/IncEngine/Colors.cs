﻿using UnityEngine;

namespace IncEngine
{
    public static class Colors
    {
        public static readonly Color DefaultEntityColor = CreateColor(55, 55, 95);

        public static Color CreateColor(float r, float g, float b, float a = 1)
        {
            if (r > 1) r = r / 255;
            if (g > 1) g = g / 255;
            if (b > 1) b = b / 255;
            if (a > 1) a = a / 255;

            return new Color(r, g, b, a);
        }
    }
}
