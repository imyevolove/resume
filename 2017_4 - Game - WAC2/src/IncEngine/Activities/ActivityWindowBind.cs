﻿using IncEngine.UI;
using System;
using UnityEngine;

namespace IncEngine.Activities
{
    /// <summary>
    /// Contains header an content activity prefabs
    /// </summary>
    [Serializable]
    public class ActivityWindowBind<T> where T : struct
    {
        [SerializeField]
        public T Id;

        [SerializeField]
        public Activity BarActivity;

        [SerializeField]
        public Window Window;
    }
}
