﻿using System;

namespace IncEngine.Activities
{
    public class WindowContext : IActivity
    {
        public Activity BarActivity;
        public Activity ContentActivity;

        public event Action Event_Show;
        public event Action Event_Hide;

        public virtual void Hide()
        {
            if (BarActivity != null)
            {
                BarActivity.Hide();
            }

            ContentActivity.Hide();
            OnHide();
        }

        public virtual void Show()
        {
            if (BarActivity != null)
            {
                BarActivity.Show();
            }

            ContentActivity.Show();
            OnShow();
        }

        public WindowContext(Activity bar, Activity content)
        {
            BarActivity     = bar;
            ContentActivity = content;
        }

        protected virtual void OnShow()
        {
            if (Event_Show != null)
            {
                Event_Show();
            }
        }

        protected virtual void OnHide()
        {
            if (Event_Hide != null)
            {
                Event_Hide();
            }
        }
    }
}
