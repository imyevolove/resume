﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ЕСТЬ МЕСТАМИ БЫДЛОКОД, ПОПРАВИТЬ!!!
/// </summary>

namespace IncEngine.Activities
{
    public enum MenuState
    {
        Opened,
        Closed,
        Transition
    }

    [Serializable, DisallowMultipleComponent]
    public abstract class AbstractAppWindow<T> : MonoBehaviour where T : struct
    {
        public readonly Dictionary<T, WindowContext> Windows = new Dictionary<T, WindowContext>();

        [Header("Activities")]
        [SerializeField]
        public ActivityMenu     MenuActivity;
        [SerializeField]
        public ActivityWindow   WindowActivity;
        [SerializeField]
        public Image            BlackoutImage;

        [Header("Menu settings")]
        [SerializeField]
        public float            blackoutMaxOpacity;
        [SerializeField]
        public float            menuOpenedVisibleWidth;
        [SerializeField]
        public float            transitionDuration;
        [SerializeField]
        public bool             hideMenuWhenInvisible;
        [SerializeField]
        public AnimationCurve   animationCurve;

        [Header("Menu states")]
        [SerializeField]
        private MenuState       m_MenuState;
        
        public MenuState        MenuState           { get { return m_MenuState; } }
        public bool             IsOpenedMenu        { get { return m_MenuState == MenuState.Opened; } }
        public bool             IsMenuTransition    { get { return m_MenuState == MenuState.Transition; } }
        
        /// HELPERS
        private Vector2         m_MenuAnchoredPosition  = Vector2.zero;
        private Color           m_BlackoutColor         = Color.white;

        /// <summary>
        /// Next menu state
        /// </summary>
        public virtual void NextMenuState(bool fast = false)
        {
            if (IsOpenedMenu) CloseMenu(fast);
            else OpenMenu(fast);
        }

        /// <summary>
        /// Open menu
        /// </summary>
        public virtual void OpenMenu(bool fast = false)
        {
            if (m_MenuState == MenuState.Opened) return;

            OnMenuStartOpen();

            MenuActivity.Show();
            
            var pos = WindowActivity.Transform.rect.width - menuOpenedVisibleWidth;
            StartMenuTransition(pos, fast, MenuState.Opened);
        }

        /// <summary>
        /// Close menu
        /// </summary>
        public virtual void CloseMenu(bool fast = false)
        {
            if (m_MenuState == MenuState.Closed) return;

            OnMenuStartClose();

            StartMenuTransition(0, fast, MenuState.Closed);
        }
        
        /// <summary>
        /// Show window by identificator
        /// </summary>
        /// <param name="id"></param>
        public void ShowWindow(T id)
        {
            foreach (var window in Windows)
            {
                if (EqualityComparer<T>.Default.Equals(window.Key, id))
                     window.Value.Show();
                else window.Value.Hide();
            }
        }

        /// <summary>
        /// Get window by identficator
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WindowContext GetWindow(T id)
        {
            return Windows[id];
        }

        /// <summary>
        /// Register new window by bind
        /// </summary>
        /// <param name="bind"></param>
        /// <returns></returns>
        public bool RegisterWindow(ActivityWindowBind<T> bind)
        {
            if (Windows.ContainsKey(bind.Id)) return false;

            Windows.Add(bind.Id, InstantiateActivityWindowBind(bind));
            return true;
        }

        /// <summary>
        /// Unregister window by identificator
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UnregisterWindow(T id)
        {
            return Windows.Remove(id);
        }

        /// <summary>
        /// On opened menu event action
        /// </summary>
        protected virtual void OnMenuOpened()
        {
        }
        
        /// <summary>
        /// On closed menu event action
        /// </summary>
        protected virtual void OnMenuClosed()
        {
            if (hideMenuWhenInvisible) MenuActivity.Hide();
            TurnOffBlackout();
        }

        /// <summary>
        /// On start open menu event action
        /// </summary>
        protected virtual void OnMenuStartOpen()
        {
            TurnOnBlackout();
        }

        /// <summary>
        /// On start close menu event action
        /// </summary>
        protected virtual void OnMenuStartClose()
        { }

        /// <summary>
        /// On transition event action
        /// </summary>
        protected virtual void OnMenuTransition(float time)
        {
            SetBlackoutOpacity(time);
        }

        /// <summary>
        /// Instantiate new window context from bind
        /// </summary>
        /// <param name="bind"></param>
        /// <returns></returns>
        protected WindowContext InstantiateActivityWindowBind(ActivityWindowBind<T> bind)
        {
            Activity bar = null;

            if (bind.BarActivity != null)
            {
                bar = Instantiate(bind.BarActivity, WindowActivity.BarActivity.Transform, false);
                bar.gameObject.SetActive(true);
                bar.gameObject.SetActive(false);
            }

            Activity content = Instantiate(bind.Window, WindowActivity.ContentActivity.Transform, false);
            content.gameObject.SetActive(true);
            content.gameObject.SetActive(false);

            WindowContext context = new WindowContext(bar, content);
            return context;
        }

        /// <summary>
        /// Menu transition launcher
        /// </summary>
        /// <param name="x"></param>
        /// <param name="fast"></param>
        /// <param name="finalState"></param>
        protected void StartMenuTransition(float x, bool fast, MenuState finalState)
        {
            if (m_MenuState == MenuState.Transition) return;

            StopCoroutine("MenuTransition");
            StartCoroutine(MenuTransition(x, fast, finalState));
        }

        /// <summary>
        /// Transition for menu
        /// </summary>
        /// <param name="x"></param>
        /// <param name="fast"></param>
        /// <param name="finalState"></param>
        /// <returns></returns>
        protected IEnumerator MenuTransition(float x, bool fast, MenuState finalState)
        {
            if (fast) MoveWindowToXPosition(x);
            else yield return MoveWindowToXPositionAnimation(x, finalState == MenuState.Closed);
            
            m_MenuState = finalState;

            switch (m_MenuState)
            {
                case MenuState.Opened: OnMenuOpened(); break;
                case MenuState.Closed: OnMenuClosed(); break;
            }
        }
        
        /// <summary>
        /// Move window to position enumerator
        /// </summary>
        /// <param name="x"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        protected IEnumerator MoveMenuToXPositionEnumrator(float x, bool reverseTime, float duration)
        {
            var time = 0f;
            var step = 0f;
            var value = 0f;
            var timeDirection = 0f;

            var transform = WindowActivity.Transform;
            var startX = m_MenuAnchoredPosition.x;

            /// Set start position
            m_MenuAnchoredPosition.Set(
                transform.anchoredPosition.x,
                transform.anchoredPosition.y
                );

            m_MenuState = MenuState.Transition;

            while (time < 1)
            {
                step = animationCurve.Evaluate(time);
                value = Mathf.Lerp(startX, x, step);
                time += Time.deltaTime / duration;
                timeDirection = Mathf.Clamp01(reverseTime ? 1 - time : time);

                MoveWindowToXPosition(value);
                OnMenuTransition(timeDirection);

                yield return false;
            }

            MoveWindowToXPosition(x);
            OnMenuTransition(timeDirection);

            m_MenuState = MenuState.Transition;
        }

        /// <summary>
        /// Move window to x position animation
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        protected Coroutine MoveWindowToXPositionAnimation(float x, bool reverseTime)
        {
            StopCoroutine("MoveMenuToXPositionEnumrator");
            return StartCoroutine(MoveMenuToXPositionEnumrator(x, reverseTime, transitionDuration));
        }

        /// <summary>
        /// Move window to position by x coord
        /// </summary>
        /// <param name="x"></param>
        protected void MoveWindowToXPosition(float x)
        {
            var transform = WindowActivity.Transform;

            m_MenuAnchoredPosition.Set(x, transform.anchoredPosition.y);
            transform.anchoredPosition = m_MenuAnchoredPosition;
        }

        protected void TurnOffBlackout() { BlackoutImage.gameObject.SetActive(false); }
        protected void TurnOnBlackout() { BlackoutImage.gameObject.SetActive(true); }

        /// <summary>
        /// Set blackout sprite opacity
        /// </summary>
        /// <param name="opacity"></param>
        protected void SetBlackoutOpacity(float opacity)
        {
            opacity = Mathf.Clamp01(opacity);

            m_BlackoutColor = BlackoutImage.color;
            m_BlackoutColor.a = Mathf.Lerp(0, blackoutMaxOpacity, opacity);

            BlackoutImage.color = m_BlackoutColor;
        }
    }
}
