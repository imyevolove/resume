﻿using IncEngine.Localization.Mono;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.Activities
{
    public class MenuButton : Activity, IPointerClickHandler
    {
        public Image    Image;
        public TextLocalizer TextLocalizer;

        public event Action Event_Click;

        public void SetLocalizationKey(string key)
        {
            TextLocalizer.localizationKey = key;
            TextLocalizer.UpdateText();
        }

        public void SetText(string text)
        {
            TextLocalizer.SetCustomText(text);
        }

        public void SetImage(Sprite image)
        {
            Image.sprite = image;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Event_Click != null) Event_Click();
        }
    }
}
