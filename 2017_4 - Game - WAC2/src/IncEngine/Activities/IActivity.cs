﻿using System;

namespace IncEngine.Activities
{
    public interface IActivity
    {
        event Action Event_Show;
        event Action Event_Hide;

        void Show();
        void Hide();
    }
}
