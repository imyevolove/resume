﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine.Activities
{
    [Serializable]
    public class ActivityMenu : Activity
    {
        public Activity ButtonsContainer;

        [SerializeField]
        private MenuButton MenuButtonPrefab;

        private List<MenuButton> m_RegisteredButtons = new List<MenuButton>();

        public void AddButton(Sprite image, string text, Action action)
        {
            MenuButton button = Instantiate(MenuButtonPrefab, ButtonsContainer.Transform, false);
            button.Event_Click += action;
            button.SetImage(image);
            button.SetLocalizationKey(text);

            m_RegisteredButtons.Add(button);
        }
    }
}
