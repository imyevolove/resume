﻿using System;
using UnityEngine;

namespace IncEngine.Activities
{
    /// <summary>
    /// Window element
    /// </summary>
    [DisallowMultipleComponent, Serializable]
    public class Activity : MonoBehaviour, IActivity
    {
        public event Action Event_Show;
        public event Action Event_Hide;
        
        public bool IsShown { get { return gameObject.activeSelf; } }

        /// <summary>
        /// Cached transform component
        /// </summary>
        public RectTransform Transform
        {
            get
            {
                if (m_Transform == null)
                    m_Transform = GetComponent<RectTransform>();

                return m_Transform;
            }
        }
        private RectTransform m_Transform;
        
        /// <summary>
        /// Show activity
        /// </summary>
        public virtual void Show()
        {
            if (gameObject.activeSelf) return;
            gameObject.SetActive(true);

            OnShow();
        }

        /// <summary>
        /// Hide activity
        /// </summary>
        public virtual void Hide()
        {
            if (!gameObject.activeSelf) return;
            gameObject.SetActive(false);

            OnHide();
        }

        protected virtual void OnShow()
        {
            if (Event_Show != null)
            {
                Event_Show();
            }
        }

        protected virtual void OnHide()
        {
            if (Event_Hide != null)
            {
                Event_Hide();
            }
        }
    }
}
