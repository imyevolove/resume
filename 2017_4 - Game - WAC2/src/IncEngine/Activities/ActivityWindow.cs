﻿namespace IncEngine.Activities
{
    /// <summary>
    /// Contains header an content activities
    /// </summary>
    public class ActivityWindow : Activity
    {
        public Activity BarActivity;
        public Activity ContentActivity;

        public ActivityWindow(Activity bar, Activity content)
        {
            BarActivity     = bar;
            ContentActivity = content;
        }
    }
}
