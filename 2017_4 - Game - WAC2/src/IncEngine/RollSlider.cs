﻿using IncEngine.Activities;
using System;
using System.Collections;
using UnityEngine;

namespace IncEngine
{
    public enum RollSliderState
    {
        Idle,
        Rolling
    }

    [Serializable]
    public class RollSlider : MonoBehaviour
    {
        public event Action Event_RollStart;
        public event Action<int> Event_RollEnd;

        [SerializeField] public Activity content;
        [SerializeField] public Activity viewport;

        [Header("Settings")]
        [SerializeField] public float duration;
        [SerializeField] public float maxOffset;
        [SerializeField] public AnimationCurve curve;
        
        public bool             IsReady     { get { return State == RollSliderState.Idle; } }
        public RollSliderState  State       { get; protected set; }
        public int              ResultIndex { get; protected set; }

        private Coroutine m_RollCoroutine;

        private float   m_SliderValue;
        private Vector2 m_SliderHelperPosition = Vector2.zero;
        private float   m_SliderHelperDestinationPositionX;
        private float   m_SliderHelperStartPositionX;
        private float   m_SliderHelperMaxDestinationPositionX;
        private float   m_SliderHelperTemporaryPositionX;
        private float   m_SliderHelperOffset;
        private float   m_SliderHelperOffsetPercentage;

        protected virtual void Awake()
        {
            State = RollSliderState.Idle;
        }

        protected virtual void Start()
        {  
        }

        /// <summary>
        /// Start roll and force exit from 
        /// current roll action if rolling now
        /// </summary>
        public virtual void Roll(int count)
        {
            Stop();
            RestoreSlider();

            m_RollCoroutine = StartCoroutine(RollEnumerator(count));

            OnRollStart();
        }

        /// <summary>
        /// Force stop rolling.
        /// </summary>
        public void Stop()
        {
            if (State == RollSliderState.Idle)
            {
                return;
            }

            State = RollSliderState.Idle;

            if (m_RollCoroutine != null)
            {
                StopCoroutine(m_RollCoroutine);
                m_RollCoroutine = null;
            }
            
            OnRollEnd();
        }

        protected virtual void OnRollStart()
        {
            if (Event_RollStart != null)
            {
                Event_RollStart();
            }
        }

        protected virtual void OnRollEnd()
        {
            if (Event_RollEnd != null)
            {
                Event_RollEnd(ResultIndex);
            }
        }

        protected virtual void OnRoll(float positionX)
        { 
        }

        protected void RestoreSlider()
        {
            m_SliderHelperPosition.Set(0, content.Transform.anchoredPosition.y);
            content.Transform.anchoredPosition = m_SliderHelperPosition;
        }

        private IEnumerator RollEnumerator(int count)
        {
            State = RollSliderState.Rolling;
            m_SliderValue = 0;

            m_SliderHelperOffset = UnityEngine.Random.Range(0, maxOffset);
            m_SliderHelperOffset *= content.Transform.rect.width;

            m_SliderHelperMaxDestinationPositionX = content.Transform.rect.width - viewport.Transform.rect.width / 2;
            m_SliderHelperDestinationPositionX = -(m_SliderHelperMaxDestinationPositionX - m_SliderHelperOffset);
            m_SliderHelperStartPositionX = content.Transform.anchoredPosition.x;
            m_SliderHelperPosition.Set(content.Transform.anchoredPosition.x, content.Transform.anchoredPosition.y);
            m_SliderHelperTemporaryPositionX = 0f;

            m_SliderHelperOffsetPercentage = Math.Abs(100 / m_SliderHelperMaxDestinationPositionX * m_SliderHelperDestinationPositionX);

            ResultIndex = (int)(m_SliderHelperOffsetPercentage / 100 * count);
            if (ResultIndex >= count) ResultIndex -= 1;

            while (m_SliderValue < 1)
            {
                m_SliderValue += Time.deltaTime / duration;

                if (m_SliderValue > 1)
                {
                    m_SliderValue = 1;
                }
                
                m_SliderHelperTemporaryPositionX = Mathf.Lerp(m_SliderHelperStartPositionX, m_SliderHelperDestinationPositionX, curve.Evaluate(m_SliderValue));
                
                m_SliderHelperPosition.Set(m_SliderHelperTemporaryPositionX, m_SliderHelperPosition.y);
                content.Transform.anchoredPosition = m_SliderHelperPosition;

                OnRoll(m_SliderHelperDestinationPositionX);

                yield return null;
            }

            State = RollSliderState.Idle;
            m_RollCoroutine = null;

            OnRollEnd();
        }
    }
}
