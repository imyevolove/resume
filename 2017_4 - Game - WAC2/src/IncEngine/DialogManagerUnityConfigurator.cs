﻿using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine
{
    [Serializable]
    public class DialogManagerUnityConfigurator : MonoBehaviour
    {
        [SerializeField] private RectTransform m_Container;
        [SerializeField] private List<Dialog> m_Dialogues;

        protected virtual void Awake()
        {
            InitDialogues();
            ConfigureDialogManager();
        }

        private void ConfigureDialogManager()
        {
            if (m_Container != null)
            {
                DialogManager.SetContainer(m_Container);
            }

            foreach (var dialog in m_Dialogues)
            {
                DialogManager.RegisterDialog(dialog);
            }
        }

        private void InitDialogues()
        {
            if (m_Dialogues == null)
            {
                m_Dialogues = new List<Dialog>();
            }
        }
    }
}
