﻿using IncEngine.Collections;

namespace IncEngine
{
    public interface IMarket
    {
        ulong GetSalePrice(InventoryItem item);
    }
}
