﻿using System;

namespace IncEngine.AdsManagement
{
    public interface IAdsProvider
    {
        event Action Event_VideoReady;

        void Initialize();
        bool IsInitialized();

        void ShowBanner();
        void ShowBanner(AD_POSITION position);
        void HideBanner();

        void LoadInterstitial();
        void ShowInterstitial();
        bool CanShowInterstitial();

        void LoadVideo();
        void ShowVideo();
        bool CanShowVideo();

        void SetTesting(bool testing);
    }
}
