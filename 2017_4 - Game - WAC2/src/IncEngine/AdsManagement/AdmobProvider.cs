﻿namespace IncEngine.AdsManagement
{
    using admob;
    using System;
    using UnityEngine;

    public class AdmobProvider : IAdsProvider
    {
        public event Action Event_VideoReady;

        private Admob admob;
        private bool isInitialized = false;

        protected string bannerID;
        protected string interstitialID;
        protected string videoID;

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public AdmobProvider(string bannerID, string interstitialID, string videoID)
        {
            admob = Admob.Instance();
            this.interstitialID = interstitialID;
            this.bannerID       = bannerID;
            this.videoID        = videoID;
        }

        ////////////////////////////////////////////////////////////
        public void Initialize()
        {
            if (IsInitialized()) return;

            admob.initAdmob(bannerID, interstitialID);
            isInitialized = true;

            admob.rewardedVideoEventHandler += OnRewardedVideoEvent;

            LoadInterstitial();
            LoadVideo();
        }

        public void SetTesting(bool testing)
        {
            admob.setTesting(testing);
        }

        ////////////////////////////////////////////////////////////
        public bool IsInitialized()
        {
            return isInitialized;
        }

        ////////////////////////////////////////////////////////////
        public void ShowBanner()
        {
            admob.showBannerRelative(AdSize.Banner, AdPosition.BOTTOM_CENTER, 0);
            Debug.Log("SHOW BANNER");
        }

        ////////////////////////////////////////////////////////////
        public void ShowBanner(AD_POSITION position)
        {
            int pos = AdPosition.BOTTOM_CENTER;

            switch (position)
            {
                case AD_POSITION.TOP:
                    pos = AdPosition.TOP_CENTER;
                    break;
                case AD_POSITION.BOTTOM:
                    pos = AdPosition.BOTTOM_CENTER;
                    break;
            }

            admob.showBannerRelative(AdSize.Banner, pos, 0);
            Debug.Log("SHOW BANNER");
        }

        ////////////////////////////////////////////////////////////
        public void ShowBanner(AdSize size, AD_POSITION position)
        {
            int pos = AdPosition.BOTTOM_CENTER;

            switch (position)
            {
                case AD_POSITION.TOP:
                    pos = AdPosition.TOP_CENTER;
                    break;
                case AD_POSITION.BOTTOM:
                    pos = AdPosition.BOTTOM_CENTER;
                    break;
            }

            admob.showBannerRelative(size, pos, 0);
            Debug.Log("SHOW BANNER");
        }

        ////////////////////////////////////////////////////////////
        public void HideBanner()
        {
            admob.removeBanner();
            Debug.Log("HIDE BANNER");
        }

        ////////////////////////////////////////////////////////////
        public void ShowInterstitial()
        {
            admob.showInterstitial();
            LoadInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public void LoadInterstitial()
        {
            if (admob.isInterstitialReady()) return;
            admob.loadInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public bool CanShowInterstitial()
        {
            return admob.isInterstitialReady();
        }

        ////////////////////////////////////////////////////////////
        public void ShowVideo()
        {
            admob.showRewardedVideo();
            LoadVideo();
        }

        ////////////////////////////////////////////////////////////
        public void LoadVideo()
        {
            if (admob.isRewardedVideoReady()) return;
            admob.loadRewardedVideo(videoID);
            admob.rewardedVideoEventHandler += (e, d) => {

            };
        }

        ////////////////////////////////////////////////////////////
        public bool CanShowVideo()
        {
            return admob.isRewardedVideoReady();
        }

        protected virtual void OnRewardedVideoEvent(string eventName, string message)
        {
            if (eventName == AdmobEvent.onAdLoaded)
            {
                CallEventAction(Event_VideoReady);
            }
        }

        private void CallEventAction(Action action)
        {
            if (action == null) return;
            action.Invoke();
        }
    }
}
