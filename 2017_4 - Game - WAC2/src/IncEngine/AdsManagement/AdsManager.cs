﻿namespace IncEngine.AdsManagement
{
    using UnityEngine.Events;

    public enum AD_POSITION
    {
        TOP,
        BOTTOM
    }

    public class AdsManager
    {
        public static event UnityAction onChange;
        public static event UnityAction Event_BannerShow;
        public static event UnityAction Event_BannerHide;
        public static event UnityAction Event_VideoReady;

        public static bool CanShowAds { get; protected set; }
        private static IAdsProvider m_Provider;

        ////////////////////////////////////////////////////////////
        public static void Setup(IAdsProvider provider)
        {
            if (provider == null) return;

            RemoveEventListeners(m_Provider);
            AddEventListeners(provider);

            m_Provider = provider;

            if (!m_Provider.IsInitialized())
            {
                EnableAds();
                m_Provider.Initialize();
                CallOnChangeEvent();
            }
        }

        ////////////////////////////////////////////////////////////
        public static void ShowBanner()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!CanShowAds) return;
            m_Provider.ShowBanner();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowBanner(AD_POSITION position)
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!CanShowAds) return;

            m_Provider.ShowBanner(position);
            CallEvent(Event_BannerShow);
        }

        ////////////////////////////////////////////////////////////
        public static void HideBanner()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;

            m_Provider.HideBanner();
            CallEvent(Event_BannerHide);
        }

        ////////////////////////////////////////////////////////////
        public static void ShowInterstitial()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!CanShowAds) return;
            m_Provider.ShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public static bool CanShowInterstitial()
        {
            if (m_Provider == null || !m_Provider.IsInitialized() || !CanShowAds) return false;
            return m_Provider.CanShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowVideo()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!CanShowAds) return;
            m_Provider.ShowVideo();
        }

        ////////////////////////////////////////////////////////////
        public static bool CanShowVideo()
        {
            if (m_Provider == null || !m_Provider.IsInitialized() || !CanShowAds) return false;
            return m_Provider.CanShowVideo();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowAnyFullAd()
        {
            if (m_Provider == null) return;
            if (!m_Provider.IsInitialized()) return;
            if (!CanShowAds) return;

            if (m_Provider.CanShowVideo())
            {
                m_Provider.ShowVideo();
                return;
            }

            if (m_Provider.CanShowInterstitial())
            {
                m_Provider.ShowInterstitial();
                return;
            }
        }

        ////////////////////////////////////////////////////////////
        public static bool CanShowAnyFullAd()
        {
            if (m_Provider == null || !m_Provider.IsInitialized() || !CanShowAds) return false;
            return m_Provider.CanShowVideo() || m_Provider.CanShowInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public static void DisableAds()
        {
            if (CanShowAds == false) return;

            CanShowAds = false;
            HideBanner(); /// Hide banner if showing now
            CallOnChangeEvent();
        }

        ////////////////////////////////////////////////////////////
        public static void EnableAds()
        {
            if (CanShowAds == true) return;

            CanShowAds = true;
            CallOnChangeEvent();
        }

        ////////////////////////////////////////////////////////////
        protected static void CallOnChangeEvent()
        {
            if (onChange == null) return;
            onChange();
        }

        ////////////////////////////////////////////////////////////
        protected static void OnProviderEventVideoReady()
        {
            if (Event_VideoReady != null)
            {
                Event_VideoReady.Invoke();
            }
        }


        private static void AddEventListeners(IAdsProvider provider)
        {
            if (provider == null) return;

            provider.Event_VideoReady += OnProviderEventVideoReady;
        }

        private static void RemoveEventListeners(IAdsProvider provider)
        {
            if (provider == null) return;
            
            provider.Event_VideoReady -= OnProviderEventVideoReady;
        }

        private static void CallEvent(UnityAction action)
        {
            if (action != null)
            {
                action();
            }
        }
    }
}
