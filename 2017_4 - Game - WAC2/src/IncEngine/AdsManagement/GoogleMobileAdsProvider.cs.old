﻿using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using IncEngine.UpdateManagement;

namespace IncEngine.AdsManagement
{
    public class GoogleMobileAdsProvider : IAdsProvider
    {
        public event Action Event_VideoReady;
        
        private bool isInitialized = false;

        protected string bannerID;
        protected string interstitialID;
        protected string videoID;

        protected BannerView BannerAd { get; private set; }
        protected InterstitialAd InterstitialAd { get; private set; }
        protected readonly RewardBasedVideoAd RewardAd;

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public GoogleMobileAdsProvider(string bannerID, string interstitialID, string videoID)
        {
            this.interstitialID = interstitialID;
            this.bannerID       = bannerID;
            this.videoID        = videoID;

            BannerAd = new BannerView(this.bannerID, AdSize.Banner, AdPosition.Top);
            UpdateBannerAd();

            InterstitialAd = new InterstitialAd(this.interstitialID);
            UpdateInterstitialAd();

            RewardAd = RewardBasedVideoAd.Instance;
            UpdateRewardAd();

            RewardAd.OnAdLoaded += (obj, args) => 
            {
                Event_VideoReady();
            };

            UpdateManager.StartRepeate(60, UpdateBannerAd);
        }

        ////////////////////////////////////////////////////////////
        public void Initialize()
        {
            if (IsInitialized()) return;
            
            isInitialized = true;

            LoadInterstitial();
            LoadVideo();
        }

        public void SetTesting(bool testing)
        {
        }

        ////////////////////////////////////////////////////////////
        public bool IsInitialized()
        {
            return isInitialized;
        }

        ////////////////////////////////////////////////////////////
        public void ShowBanner()
        {
            BannerAd.Show();
        }

        ////////////////////////////////////////////////////////////
        public void ShowBanner(AD_POSITION position)
        {
            ShowBanner(AdSize.Banner, position);
        }

        ////////////////////////////////////////////////////////////
        public void ShowBanner(AdSize size, AD_POSITION position)
        {
            BannerAd.Hide();
            BannerAd.Destroy();

            var pos = AdPosition.Bottom;

            switch (position)
            {
                case AD_POSITION.TOP:
                    pos = AdPosition.Top;
                    break;
                case AD_POSITION.BOTTOM:
                    pos = AdPosition.Bottom;
                    break;
            }

            BannerAd = new BannerView(bannerID, size, pos);
            ShowBanner();
        }

        ////////////////////////////////////////////////////////////
        public void HideBanner()
        {
            BannerAd.Hide();
            Debug.Log("HIDE BANNER");
        }

        ////////////////////////////////////////////////////////////
        public void ShowInterstitial()
        {
            InterstitialAd.Show();
            LoadInterstitial();
        }

        ////////////////////////////////////////////////////////////
        public void LoadInterstitial()
        {
            if (CanShowInterstitial()) return;
            UpdateInterstitialAd();
        }

        ////////////////////////////////////////////////////////////
        public bool CanShowInterstitial()
        {
            return InterstitialAd.IsLoaded();
        }

        ////////////////////////////////////////////////////////////
        public void ShowVideo()
        {
            RewardAd.Show();
            LoadVideo();
        }

        ////////////////////////////////////////////////////////////
        public void LoadVideo()
        {
            if (CanShowVideo()) return;
            UpdateRewardAd();
        }

        ////////////////////////////////////////////////////////////
        public bool CanShowVideo()
        {
            return RewardAd.IsLoaded();
        }

        private void CallEventAction(Action action)
        {
            if (action == null) return;
            action.Invoke();
        }

        private void RequestAd(InterstitialAd ad)
        {
            AdRequest request = new AdRequest.Builder().Build();
            ad.LoadAd(request);
        }

        private void RequestAd(BannerView ad)
        {
            AdRequest request = new AdRequest.Builder().Build();
            ad.LoadAd(request);
        }

        private void RequestAd(RewardBasedVideoAd ad, string adId)
        {
            AdRequest request = new AdRequest.Builder().Build();
            ad.LoadAd(request, adId);
        }

        private void UpdateInterstitialAd()
        {
            RequestAd(InterstitialAd);
        }

        private void UpdateBannerAd()
        {
            RequestAd(BannerAd);
        }

        private void UpdateRewardAd()
        {
            RequestAd(RewardAd, videoID);
        }
    }
}
