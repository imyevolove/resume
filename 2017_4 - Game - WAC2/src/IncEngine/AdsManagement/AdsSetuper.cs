﻿using System;
using UnityEngine;
using Wac;

namespace IncEngine.AdsManagement
{
    [Serializable]
    public class AdsSetuper : MonoBehaviour
    {
        [SerializeField] public RectTransform RectHandled;

        protected virtual void Start()
        {
            
            var provider = new AdmobProvider(
                "ca-app-pub-1844390866925362/4154247537",
                "ca-app-pub-1844390866925362/5630980733",
                "ca-app-pub-1844390866925362/7107713936");
                
            //var provider = new AppodealProvider("b240f6a7895133c3e69206b31006c0ef6b99250c0134f780");
            provider.SetTesting(false);

            AdsManager.Setup(provider);
            
            AdsManager.Event_BannerShow += ResizeToAdsSize;
            AdsManager.Event_BannerHide += ResizeToNormalSize;

            Game.Profile.AdsState.Event_OnValueChange += OnAdsStateChange;
            OnAdsStateChange(Game.Profile.AdsState.Value);
        }

        private void OnAdsStateChange(bool state)
        {
            if (state)
            {
                ShowBanner();
            }
            else
            {
                AdsManager.HideBanner();
                AdsManager.DisableAds();
            }
        }

        private void ShowBanner()
        {
            AdsManager.ShowBanner(AD_POSITION.TOP);
        }

        private void ResizeToNormalSize()
        {
            RectHandled.offsetMax = new Vector2(RectHandled.offsetMax.x, 0);
        }

        private void ResizeToAdsSize()
        {
            RectHandled.offsetMax = new Vector2(RectHandled.offsetMax.x, -100);
        }
    }
}
