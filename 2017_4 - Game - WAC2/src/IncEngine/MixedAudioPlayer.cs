﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine
{
    [Serializable]
    public class MixedAudioPlayer : AudioPlayer
    {
        [SerializeField] public List<AudioClip> clips;

        public override void Play()
        {
            Play(clips[UnityEngine.Random.Range(0, clips.Count)]);
        }
    }
}
