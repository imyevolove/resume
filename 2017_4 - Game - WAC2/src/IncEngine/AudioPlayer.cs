﻿using System;
using UnityEngine;

namespace IncEngine
{
    [Serializable, RequireComponent(typeof(AudioSource))]
    public class AudioPlayer : MonoBehaviour
    {
        [HideInInspector] private AudioSource m_AudioSource;
        public AudioSource AudioSource
        {
            get
            {
                if (m_AudioSource == null)
                {
                    m_AudioSource = GetComponent<AudioSource>();
                }

                return m_AudioSource;
            }
        }

        public void Play(AudioClip clip)
        {
            AudioSource.PlayOneShot(clip);
        }

        public virtual void Play()
        {
            AudioSource.Play();
        }
    }
}
