﻿using System;

namespace IncEngine.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class ValueAttribute : Attribute
    {
        public readonly uint Value;

        public ValueAttribute(uint value)
        {
            Value = value;
        }
    }
}
