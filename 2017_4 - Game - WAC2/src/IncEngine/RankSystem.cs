﻿using IncEngine.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using Wac;

namespace IncEngine
{
    public class RankSystem : IStorage
    {
        public ulong ExperienceValue
        {
            get { return m_TotalProgress; }
            protected set
            {
                m_TotalProgress = value;
                
                /// Last rank.
                if (CurrentRank == LastRank)
                {
                    m_TotalProgress = m_TotalProgressStage;
                    return;
                }

                /// Progress value change event emitter
                if (Event_ProgressChange != null)
                    Event_ProgressChange();

                /// Next rank
                if (m_TotalProgress > m_TotalProgressStage)
                    RecalculateNextParameters();
            }
        }
        
        public float Progress01     { get { return ProgressValue / (float)ProgressStage; } }

        public ulong ProgressValue   { get { return NextRank.weight - (m_TotalProgressStage - ExperienceValue); } }
        public ulong ProgressStage   { get { return NextRank.weight; } }

        public int CurrentRankIndex { get; protected set; }

        public Rank CurrentRank     { get; protected set; }
        public Rank NextRank        { get; protected set; }
        public Rank LastRank        { get; protected set; }

        public event Action Event_ProgressChange;
        public event Action Event_RankChange;

        private List<Rank> m_Ranks = new List<Rank>();
        
        private ulong m_TotalProgressStage;
        private ulong m_TotalProgress;

        private Rank m_HelperPreviousRank;

        public RankSystem()
        {
        }
        public RankSystem(params Rank[] ranks)
        {
            AddRange(ranks);
        }
        public RankSystem(IEnumerable<Rank> ranks)
        {
            AddRange(ranks);
        }

        public void IncreaseExperience(ulong value)
        {
            ExperienceValue += Game.Player.Abilities.GetAbility(Abilities.AbilityType.ExperienceSpeedUpValue).ApplyToValue(value);
        }

        public void DecreaseExperience(ulong value)
        {
            ExperienceValue -= value;
        }

        /// <summary>
        /// Add rank
        /// </summary>
        /// <param name="rank"></param>
        public void Add(Rank rank)
        {
            m_Ranks.Add(rank);
            RecalculateNextParameters();
        }

        /// <summary>
        /// Add ranks range
        /// </summary>
        /// <param name="ranks"></param>
        public void AddRange(IEnumerable<Rank> ranks)
        {
            m_Ranks.AddRange(ranks);
            RecalculateNextParameters();
        }

        /// <summary>
        /// Remove rank by object
        /// </summary>
        /// <param name="rank"></param>
        public void Remove(Rank rank)
        {
            m_Ranks.Remove(rank);
            RecalculateNextParameters();
        }

        /// <summary>
        /// Remove rank at index
        /// </summary>
        /// <param name="index"></param>
        public void Remove(int index)
        {
            m_Ranks.RemoveRange(index, 1);
            RecalculateNextParameters();
        }

        protected void RecalculateNextParameters()
        {
            m_TotalProgressStage = 0;
            LastRank = m_Ranks.Last();
            NextRank = LastRank;

            for (var i = 0; i < m_Ranks.Count; i++)
            {
                if (i != 0)
                {
                    m_TotalProgressStage += m_Ranks[i].weight;

                    /// Set next rank
                    if (m_TotalProgressStage > m_TotalProgress)
                    {
                        NextRank = m_Ranks[i];
                        break;
                    }
                }
                
                CurrentRank = m_Ranks[i];
                CurrentRankIndex = i;
            }

            /// This code block using for listen rank change
            if (m_HelperPreviousRank != CurrentRank && Event_RankChange != null)
                Event_RankChange();

            m_HelperPreviousRank = CurrentRank;
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();
            
            data["experience"] = ExperienceValue;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return false;
            if (!storageData.KeyValueIs<ulong>("experience")) return false;

            ExperienceValue = (ulong)storageData["experience"];

            return true;
        }
    }
}
