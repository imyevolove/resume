using System.Collections.Generic;

namespace IncEngine.Localization
{
    public class LocalizationConfig
    {
        public readonly string localizationAssetsPath;

        //
        public Dictionary<string, LocalizationSignature> localizationSignatures { get; protected set; }

        public LocalizationConfig(string localizationAssetsPath)
        {
            this.localizationAssetsPath = localizationAssetsPath;
            localizationSignatures = new Dictionary<string, LocalizationSignature>();
        }
    }
}