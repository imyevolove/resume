using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using System.IO;

#if NETFX_CORE
    using WinRTLegacy.Xml;
    using XMLREADER = WinRTLegacy.Xml.XmlReader;
#else
    using System.Xml;
    using XMLREADER = System.Xml.XmlReader;
#endif

namespace IncEngine.Localization
{
    using IO;

    public static class LocalizationManager
    {
        public  static string CurrentLanguage { get; private set; }

        public static LocalizationConfig config;

        public static event UnityAction onChange;

        static readonly string[] formatStringValues = {""};

        /// <summary>
        /// Return available localization languages
        /// </summary>
        /// <returns></returns>
        public static string[] Languages
        {
            get { return verifiedLanguages.ToArray(); }
        }

        // Key - Bundle Pair
        private static Dictionary<string, LocalizationBundle> m_Localizations
            = new Dictionary<string, LocalizationBundle>();

        private static readonly string DefaultLanguage = "en";

        private static HashSet<string> verifiedLanguages = new HashSet<string>();

        static LocalizationManager()
        {
            CurrentLanguage = DefaultLanguage;

            // Default settings
            string  langPath = "Localization";
            config = new LocalizationConfig(langPath);
        }

        public static bool HasLanguage(string lang)
        {
            if (string.IsNullOrEmpty(lang)) return false;
            return verifiedLanguages.Contains(lang);
        }

        public static void SetConfigurations(LocalizationConfig cfg)
        {
            config = cfg;
        }

        public static void LoadConfigFile(string path)
        {
            TextAsset cfg = Resources.Load<TextAsset>(path);

            if (cfg == null)
            {
                Debug.LogErrorFormat("Config file not found: {0}", path);
                return;
            }

            config = new LocalizationConfig(Path.GetDirectoryName(path));

            var reader = XMLREADER.Create(new StringReader(cfg.text));
            while (reader.Read())
            {
                if (reader.NodeType == System.Xml.XmlNodeType.Element && reader.Name == "language")
                {
                    if (reader.HasAttributes)
                    {
                        string code = reader.GetAttribute("code");
                        string localName = reader.GetAttribute("name");

                        LocalizationSignature signature = new LocalizationSignature(code, localName);
                        
                        if(!config.localizationSignatures.ContainsKey(code))
                            config.localizationSignatures.Add(code, signature);
                    }
                }
            }

#if NETFX_CORE
            reader.Dispose();
#else
            reader.Close();
#endif

        }

        public static void LoadLocalizationFile<T>(string filePath, LocalizationSignature signature) where T : ILocalizationLoader, new()
        {
            VerifyLocalizationFile<T>(filePath, signature);

            if (!verifiedLanguages.Contains(signature.Code))
            {
                Debug.LogFormat("Localization with code ({0}) not verified", signature.Code);
                return;
            }

            if (m_Localizations.ContainsKey(signature.Code))
            {
                Debug.LogFormat("Localization with code ({0}) already exists", signature.Code);
                return;
            }

            ILocalizationLoader loader = new T();
            LocalizationDictionary dict = loader.Load(filePath);

            if (dict == null)
            {
                Debug.LogFormat("Localization with code ({0}) not loaded", signature.Code);
                return;
            }

            m_Localizations.Add(signature.Code, dict.ToBundle(signature));
            Debug.LogFormat("Localization with code ({0}) successfully loaded", signature.Code);
        }

        public static void VerifyLocalizationFile<T>(string filePath, LocalizationSignature signature) where T : ILocalizationLoader, new()
        {
            ILocalizationLoader loader = new T();
            bool verifyStatus = loader.Verify(filePath);
            if (verifyStatus)
            {
                verifiedLanguages.Add(signature.Code);
            }
        }

        public static void LoadLocalizations()
        {
            foreach(var signature in config.localizationSignatures.Values)
            LoadLocalizationFile<LocalizationLoaderAsset>(
                Path.Combine(config.localizationAssetsPath, signature.Code + ".txt"),
                signature
            );
        }
        
        /// <summary>
        /// Apply language as current localization
        /// </summary>
        /// <param name="language"></param>
        public static bool ApplyLocalizationLanguage(string language)
        {
            if (string.IsNullOrEmpty(language)) return false;
            if(!m_Localizations.ContainsKey(language))
            {
                Debug.LogFormat("Translation for language {0} not available", language);
                return false;
            }

            CurrentLanguage = language;
            DispatchOnChangeEvent();

            return true;
        }

        /// <summary>
        /// Add new localization bundle, or replace exists
        /// </summary>
        /// <param name="bundle"></param>
        public static void SetLocalizationBundle(LocalizationBundle bundle)
        {
            if(m_Localizations.ContainsKey(bundle.Signature.Code))
            {
                m_Localizations[bundle.Signature.Code] = bundle;
                Debug.LogFormat("Localization with code {0} was replaced", bundle.Signature.Code);
            
                if(bundle.Signature.Code == CurrentLanguage || bundle.Signature.Code == DefaultLanguage)
                    DispatchOnChangeEvent();
            }
            else 
            {
                m_Localizations.Add(bundle.Signature.Code, bundle);
                Debug.LogFormat("Localization with code {0} was added", bundle.Signature.Code);
            }
        }

        /// <summary>
        /// Return localization bundle for the given language code, or null.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public static LocalizationBundle GetLocalizationBundle(string language)
        {
            LocalizationBundle bundle;

            if(string.IsNullOrEmpty(language) || !m_Localizations.TryGetValue(language, out bundle))
                return null;
                
            return bundle;
        }

        /// <summary>
        /// Return localization bundle for the given signature, or null.
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        public static LocalizationBundle GetLocalizationBundle(LocalizationSignature signature)
        {
            LocalizationBundle bundle;

            if(string.IsNullOrEmpty(signature.Code) || !m_Localizations.TryGetValue(signature.Code, out bundle))
                return null;
                
            return bundle;
        }

        /// <summary>
        /// Returns the localization for the given key, or the key itself, if no translation exists.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="additionalValues"></param>
        public static string GetLocalization(string key, params string[] additionalValues)
        {
            LocalizationBundle localization;

            // Try get localization for the current language code
            localization = GetLocalizationBundle(CurrentLanguage);

            // If localization for current language not fount then
            if(localization == null)
            {
                Debug.LogWarningFormat("Language {0} not found", CurrentLanguage);

                // Try get localization for the default language code
                localization = GetLocalizationBundle(DefaultLanguage);
            }
            
            // Localization not found, return error string
            if(localization == null)
            {
                Debug.LogError("Not one localization was not founded");
                return "LOCALIZATION NOT FOUND";
            }

            if(string.IsNullOrEmpty(localization[key]))
            {
                Debug.LogWarningFormat("Translation for key {0} in {1} language not found",
                    key, CurrentLanguage);
                return "LOCALIZATION NOT FOUND";
            }

            // Language successfully founded
            // Return localization string with combined additional values
            return string.Format(localization[key], (additionalValues.Length <= 0) ? formatStringValues : additionalValues);
        }

        /// <summary>
        /// Dispatch onChange event
        /// </summary>
        /// <param name="bundle"></param>
        private static void DispatchOnChangeEvent()
        {
            if(onChange == null)
                return;
            onChange();
        }
    }
}