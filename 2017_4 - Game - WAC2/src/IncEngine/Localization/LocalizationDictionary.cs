using System.Collections.Generic;

namespace IncEngine.Localization
{
    public class LocalizationDictionary : Dictionary<string, string>
    {
        public new string this[string key]
        {
            get 
            {
                string value;
                return base.TryGetValue(key.ToUpper(), out value) 
                    ? value 
                    : string.Empty;
            }
            set 
            {
                if(base.ContainsKey(key))
                    base[key.ToUpper()] = value;
                else
                    base.Add(key.ToUpper(), value);
            }
        }

        public LocalizationBundle ToBundle(LocalizationSignature signature)
        {
            LocalizationBundle bundle = new LocalizationBundle(signature);
            
            foreach(var kvp in this)
                bundle[kvp.Key] = kvp.Value;
            
            return bundle;
        }
    }
}