namespace IncEngine.Localization.IO
{
    public interface ILocalizationLoader
    {
        LocalizationDictionary Load(string uri);
        bool Verify(string uri);
    }
}