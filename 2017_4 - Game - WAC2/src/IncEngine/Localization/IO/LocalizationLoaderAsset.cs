using System;
using System.IO;
using UnityEngine;

namespace IncEngine.Localization.IO
{
    public sealed class LocalizationLoaderAsset : ILocalizationLoader
    {
        public LocalizationDictionary Load(string path)
        {
            LocalizationDictionary localization = new LocalizationDictionary();

            string file = Path.ChangeExtension(path, null);
            TextAsset asset = Resources.Load<TextAsset>(file);

            try
            {
                string[] lines = asset.text.Split('\n');

                foreach (string line in lines)
                {
                    string[] keyValuePair = line.Split(new char[] { '=' }, 2);

                    if (keyValuePair.Length != 2)
                    {
                        Debug.LogWarningFormat("Invalid format of localization string. Actual {0}", line);
                        continue;
                    }

                    localization[keyValuePair[0]] = keyValuePair[1];
                }
            }
            catch (Exception exception)
            {
                localization = null;
                Debug.LogException(new Exception(string.Format("Localization file not found in directory ({0})", path), exception));
            }

            return localization;
        }

        public bool Verify(string uri)
        {
            string file = Path.ChangeExtension(uri, null);
            TextAsset asset = Resources.Load<TextAsset>(file);
            return asset != null;
        }
    }
}