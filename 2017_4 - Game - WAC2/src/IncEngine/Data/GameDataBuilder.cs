﻿using IncEngine.Persers;

namespace IncEngine.Data
{
    public class GameDataBuilder : IGameDataBuilder
    {
        private string m_MapsJsonText;
        private string m_GunsJsonText;
        private string m_GunsDefaultJsonText;
        private string m_KnivesJsonText;
        private string m_KnivesDefaultJsonText;
        private string m_GlovesJsonText;
        private string m_GlovesDefaultJsonText;
        private string m_StickersJsonText;
        private string m_CollectionsJsonText;
        private string m_CasesJsonText;

        public GameDataBuilder(
            string gunsJsonText,
            string gunsDefaultJsonText,
            string knivesJsonText,
            string knivesDefaultJsonText,
            string glovesJsonText,
            string glovesDefaultJsonText,
            string stickersJsonText,
            string collectionsJsonText,
            string casesJsonText,
            string mapsJsonText
            )
        {
            m_GunsJsonText          = gunsJsonText;
            m_GunsDefaultJsonText   = gunsDefaultJsonText;
            m_KnivesJsonText        = knivesJsonText;
            m_KnivesDefaultJsonText = knivesDefaultJsonText;
            m_GlovesJsonText        = glovesJsonText;
            m_GlovesDefaultJsonText = glovesDefaultJsonText;
            m_StickersJsonText      = stickersJsonText;
            m_CollectionsJsonText   = collectionsJsonText;
            m_CasesJsonText         = casesJsonText;
            m_MapsJsonText          = mapsJsonText;
        }

        public GameData Build()
        {
            GameData gameData = new GameData();
            
            /// MAPS
            var mapsDataParser = new MapsDataParser();
            var mapsData = mapsDataParser.Parse(m_MapsJsonText);
            gameData.Maps.AddRange(mapsData);

            /// STICKERS
            var stickersDataParser = new StickersDataParser();
            var stickersData = stickersDataParser.Parse(m_StickersJsonText);
            gameData.Stickers.AddRange(stickersData);

            /// COLLECTIONS
            var collectionsDataParser = new CollectionsDataParser();
            var collectionsData = collectionsDataParser.Parse(m_CollectionsJsonText);
            gameData.Collections.AddRange(collectionsData);

            /// GLOVES
            var glovesDefaultDataParser = new GlovesDefaultDataParser();
            var glovesDefaultData = glovesDefaultDataParser.Parse(m_GlovesDefaultJsonText);
            gameData.Entities.Originals.AddRange(glovesDefaultData);

            var glovesDataParser = new GlovesDataParser(gameData);
            var glovesData = glovesDataParser.Parse(m_GlovesJsonText);
            gameData.Entities.Skins.AddRange(glovesData);

            /// KNIVES
            var KnivesDefaultDataParser = new KnivesDefaultDataParser();
            var KnivesDefaultData = KnivesDefaultDataParser.Parse(m_KnivesDefaultJsonText);
            gameData.Entities.Originals.AddRange(KnivesDefaultData);

            var KnivesDataParser = new KnivesDataParser(gameData);
            var KnivesData = KnivesDataParser.Parse(m_KnivesJsonText);
            gameData.Entities.Skins.AddRange(KnivesData);

            /// GUNS
            var gunsDefaultDataParser = new GunsDefaultDataParser();
            var gunsDefaultData = gunsDefaultDataParser.Parse(m_GunsDefaultJsonText);
            gameData.Entities.Originals.AddRange(gunsDefaultData);

            var gunsDataParser = new GunsDataParser(gameData);
            var gunsData = gunsDataParser.Parse(m_GunsJsonText);
            gameData.Entities.Skins.AddRange(gunsData);

            /// CASES
            var casesDataParser = new CasesDataParser(gameData);
            var casesData = casesDataParser.Parse(m_CasesJsonText);
            gameData.Cases.AddRange(casesData);
            
            /// NORMALIZE SKINS COLLECTIONS
            foreach (var cs in gameData.Cases)
            {
                foreach (var skin in cs.Skins)
                {
                    skin.Collection = cs.Collection;
                }
            }

            return gameData;
        }
    }
}
