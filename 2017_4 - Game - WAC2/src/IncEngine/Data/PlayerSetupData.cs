﻿using System;
using UnityEngine;

namespace IncEngine.Data
{
    [Serializable]
    public class PlayerSetupData
    {
        [SerializeField] public uint baseDamage = 1;
        [SerializeField] public uint baseIncome = 1;
    }
}
