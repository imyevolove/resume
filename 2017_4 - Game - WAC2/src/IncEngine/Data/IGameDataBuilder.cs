﻿namespace IncEngine.Data
{
    public interface IGameDataBuilder
    {
        GameData Build();
    }
}
