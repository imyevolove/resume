﻿using System.Collections.Generic;
using IncEngine.Structures;
using Wac.Collections;

namespace IncEngine.Data
{
    public class GameData
    {
        public readonly List<Map>           Maps        = new List<Map>();
        public readonly List<Case>          Cases       = new List<Case>();
        public readonly List<Sticker>       Stickers    = new List<Sticker>();
        public readonly List<Collection>    Collections = new List<Collection>();
        public readonly EntityCollection    Entities    = new EntityCollection();

        /// <summary>
        /// Find collection by id
        /// </summary>
        /// <returns></returns>
        public Collection FindCollection(string id)
        {
            return Collections.Find(c => { return c.Id == id; });
        }

        /// <summary>
        /// Find any skin by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Skin FindSkin(string id)
        {
            return Entities.GetSkin(id);
        }
    }
}
