﻿using System;
using UnityEngine;
using IncEngine.Abilities;
using IncEngine.Science;

namespace IncEngine.Data
{
    [Serializable]
    public class TechBind : TechDetails
    {
        [SerializeField]
        public AbilityType  AbilityType;
    }
}
