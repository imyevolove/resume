﻿using System;

namespace IncEngine
{
    public interface ISession
    {
        void AddSingleton<T>(T implementation);
        T GetSingleton<T>();
        object GetSingleton(Type type);
    }
}
