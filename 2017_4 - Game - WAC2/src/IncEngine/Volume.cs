﻿namespace IncEngine
{
    public class Volume : SmartValue<int>
    {
        public Volume()
        {
        }

        public Volume(int value) : base(value)
        {
        }
    }
}
