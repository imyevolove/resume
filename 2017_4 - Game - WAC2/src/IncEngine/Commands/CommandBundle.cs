﻿using System.Collections.Generic;

namespace IncEngine.Commands
{
    /// <summary>
    /// Contain commands for sequential execution
    /// </summary>
    public class CommandBundle : ICommand
    {
        private List<ICommand> m_Commands = new List<ICommand>();

        /// <summary>
        /// Add command to bundle collection
        /// </summary>
        /// <param name="command"></param>
        public void Add(ICommand command)
        {
            m_Commands.Add(command);
        }

        /// <summary>
        /// Remove command from bundle collection
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public bool Remove(ICommand command)
        {
            return m_Commands.Remove(command);
        }

        /// <summary>
        /// Check contains command
        /// </summary>
        /// <param name="command"></param>
        public bool Contains(ICommand command)
        {
            return m_Commands.Contains(command);
        }

        /// <summary>
        /// Execute all registered commands in bundle collection
        /// </summary>
        public void Execute()
        {
            foreach (var command in m_Commands)
            {
                command.Execute();
            }
        }
    }
}
