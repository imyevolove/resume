﻿using System;
using System.Collections.Generic;

namespace IncEngine.Commands
{
    public class CommandRouter<TEnum> where TEnum : struct
    {
        private Dictionary<TEnum, List<ICommand>> m_Routes = new Dictionary<TEnum, List<ICommand>>();

        /// <summary>
        /// Register command
        /// </summary>
        /// <param name="command"></param>
        public void Add(TEnum id, ICommand command)
        {
            var list = GetCommandsListSafe(id);
            list.Add(command);
        }

        /// <summary>
        /// Remove command
        /// </summary>
        /// <param name="command"></param>
        public bool Remove(TEnum id, ICommand command)
        {
            var list = GetCommandsListSafe(id);
            return list.Remove(command);
        }

        /// <summary>
        /// Dispatch commands
        /// </summary>
        public void Dispatch()
        {
            foreach(var list in m_Routes.Values)
            {
                foreach (var command in list)
                {
                    command.Execute();
                }
            }
        }

        /// <summary>
        /// Return commands list safely.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private List<ICommand> GetCommandsListSafe(TEnum id)
        {
            List<ICommand> list;

            if (!m_Routes.TryGetValue(id, out list))
            {
                list = new List<ICommand>();
                m_Routes.Add(id, list);
            }

            return list;
        }
    }
}
