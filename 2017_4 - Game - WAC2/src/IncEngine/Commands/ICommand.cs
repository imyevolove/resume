﻿namespace IncEngine.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
