﻿namespace IncEngine.Personal
{
    using Storage;
    using System;

    public class ProfileSettings : IStorage
    {
        public Action onChange;

        public readonly Volume VolumeSound = new Volume(100);
        public readonly Volume VolumeMusic = new Volume(100);

        private string m_LanguageCode;
        public string LanguageCode
        {
            get { return m_LanguageCode; }
            set
            {
                m_LanguageCode = value;
                OnChange();
            }
        }

        private bool m_PreventOnChangeEventEmitter = false;

        public ProfileSettings()
        {
            VolumeSound.Event_OnValueChange += OnAnySmartValueChange;
            VolumeMusic.Event_OnValueChange += OnAnySmartValueChange;
        }

        ////////////////////////////////////////////////////////////
        public void Apply()
        {
            
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["volume_sound"]    = VolumeSound.Value;
            data["volume_music"]    = VolumeMusic.Value;
            data["language_code"]   = LanguageCode;

            return data;
        }

        ////////////////////////////////////////////////////////////
        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return false;

            m_PreventOnChangeEventEmitter = true;

            if (storageData.KeyValueIs<int>("volume_sound"))
            {
                VolumeSound.Value = (int)storageData["volume_sound"];
            }

            if (storageData.KeyValueIs<int>("volume_music"))
            {
                VolumeMusic.Value = (int)storageData["volume_music"];
            }

            if (storageData.KeyValueIs<string>("language_code"))
            {
                LanguageCode = (string)storageData["language_code"];
            }

            m_PreventOnChangeEventEmitter = false;
            OnChange();

            return true;
        }
        
        ////////////////////////////////////////////////////////////
        protected virtual void OnChange()
        {
            if (m_PreventOnChangeEventEmitter) return;

            if (onChange != null)
            {
                onChange();
            }

            Apply();
        }

        private void OnAnySmartValueChange<T>(T value)
        {
            OnChange();
        }
    }
}
