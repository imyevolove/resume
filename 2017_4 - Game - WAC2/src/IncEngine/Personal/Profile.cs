﻿namespace IncEngine.Personal
{
    using Storage;
    using System.IO;
    using UnityEngine;
    using Wac;

    public class Profile : IStorage
    {
        public string StorageFilePath
        {
            get
            {
                return Path.Combine(Storage.PersistentDataPath, string.Format("{0}/{1}", "WAC", "ac_" + profileID + ".pfl"));
            }
        }

        public readonly string profileID;
        public readonly ProfileSettings settings = new ProfileSettings();

        public readonly SmartValue<bool> AdsState = new SmartValue<bool>(true);
        public readonly SmartValue<bool> VoteWidgetState = new SmartValue<bool>(true);

        public readonly StorageDataContainer CustomData = new StorageDataContainer();

        ////////////////////////////////////////////////////////////
        public Profile(string id)
        {
            profileID = id;
        }

        ////////////////////////////////////////////////////////////
        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["player"] = Game.Player.GetStorageData();
            data["mission"] = Game.Mission.GetStorageData();
            data["settings"] = settings.GetStorageData();
            data["ads_state"] = AdsState.Value;
            data["vote_widget_state"] = VoteWidgetState.Value;
            data["custom_data"] = CustomData;

            return data;
        }

        ////////////////////////////////////////////////////////////
        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData == null) return false;

            /// Restore player data
            if (storageData.KeyValueIs<StorageDataContainer>("player"))
            {
                Game.Player.SetStorageData(storageData["player"] as StorageDataContainer);
            }

            /// Restore mission
            if (storageData.KeyValueIs<StorageDataContainer>("mission"))
            {
                Game.Mission.SetStorageData(storageData["mission"] as StorageDataContainer);
            }

            /// Restore settings
            if (storageData.KeyValueIs<StorageDataContainer>("settings"))
            {
                settings.SetStorageData((StorageDataContainer)storageData["settings"]);
            }

            /// Ads
            if (storageData.KeyValueIs<bool>("ads_state"))
            {
                AdsState.Value = (bool)storageData["ads_state"];
            }

            /// Vote
            if (storageData.KeyValueIs<bool>("vote_widget_state"))
            {
                VoteWidgetState.Value = (bool)storageData["vote_widget_state"];
            }

            /// Custom data
            if (storageData.KeyValueIs<StorageDataContainer>("custom_data"))
            {
                CustomData.Set((StorageDataContainer)storageData["custom_data"]);
            }

            return true;
        }
    }
}
