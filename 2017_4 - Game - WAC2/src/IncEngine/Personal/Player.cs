﻿using System;

namespace IncEngine.Personal
{
    using Abilities;
    using Collections;
    using Equipment;
    using IncEngine.Storage;
    using Science;
    using UnityEngine;

    public class Player : IStorage
    {
        /// <summary>
        /// Return mine player object
        /// </summary>
        public  static Player Mine { get { return m_MinePlayer; } }
        private static Player m_MinePlayer;

        public event Action     Event_AnyChange;

        public IInventory       Inventory           { get; protected set; }
        public AbilityMapper    Abilities           { get; protected set; }
        public RankSystem       RankSystem          { get; protected set; }
        public IEquipmentMapper Equipment           { get; protected set; }
        public TechMapper       Science             { get; protected set; }

        public StoreValue       Money               { get; private set; }
        public StoreValue       Medals              { get; private set; }

        public readonly StatisticsManager Statistics;
        
        private uint m_BaseDamage;
        public  uint BaseDamage
        {
            get { return m_BaseDamage; }
            set
            {
                m_BaseDamage = value;
                OnAnyChange();
            }
        }

        private uint m_BaseIncome;
        public  uint BaseIncome
        {
            get { return m_BaseIncome; }
            set
            {
                m_BaseIncome = value;
                OnAnyChange();
            }
        }
        
        private ulong m_CachedDamageValue;
        private ulong m_CachedIncomeValue;

        private Ability m_CachedAbilityDamageIncreaseValuePercentage;
        private Ability m_CachedAbilityIncomeIncreaseValuePercentage;
        private Ability m_CachedAbilityCriticalDamage;
        private Ability m_CachedAbilityCriticalChance;

        public Player(
            IInventory          inventory,
            AbilityMapper      abilitiesMapper,
            RankSystem         rankSystem,
            IEquipmentMapper    equipmentSystem,
            TechMapper         scienceManager,
            bool isMine = false
            )
        {
            Inventory       = inventory;
            Abilities       = abilitiesMapper;
            RankSystem      = rankSystem;
            Equipment       = equipmentSystem;
            Science         = scienceManager;

            Money           = new StoreValue();
            Medals          = new StoreValue();

            Statistics      = new StatisticsManager(this);

            m_CachedAbilityDamageIncreaseValuePercentage = Abilities.GetAbility(AbilityType.DamageIncreaseValuePercentage);
            m_CachedAbilityIncomeIncreaseValuePercentage = Abilities.GetAbility(AbilityType.IncomeIncreaseValuePercentage);

            m_CachedAbilityCriticalDamage = Abilities.GetAbility(AbilityType.TouchDamageCriticalDamage);
            m_CachedAbilityCriticalChance = Abilities.GetAbility(AbilityType.TouchDamageCriticalChance);

            ActivateEventListeners();
            RecalculateCachedValues();
          
            /// Register this player as mine player object
            if (m_MinePlayer == null || isMine) m_MinePlayer = this;
        }

        public bool PurchaseForMoney(ulong value)
        {
            return PurchaseMaster(Money, value);
        }

        public bool PurchaseForMedals(ulong value)
        {
            return PurchaseMaster(Medals, value);
        }

        /// <summary>
        /// Recalculate cached values (damage/income etc)
        /// </summary>
        public void RecalculateCachedValues()
        {
            m_CachedDamageValue = BaseDamage + Equipment.GetSummaryDamage();
            m_CachedDamageValue = m_CachedAbilityDamageIncreaseValuePercentage.ApplyToValue(m_CachedDamageValue);

            m_CachedIncomeValue = BaseIncome + Equipment.GetSummaryIncome();
            m_CachedIncomeValue = m_CachedAbilityIncomeIncreaseValuePercentage.ApplyToValue(m_CachedIncomeValue);
        }

        /// <summary>
        /// Return damage with chance modifiers
        /// </summary>
        /// <returns></returns>
        public ulong GetDamage()
        {
            return m_CachedDamageValue + GetCriticalDamage();
        }

        /// <summary>
        /// Return income with chance modifiers
        /// </summary>
        /// <returns></returns>
        public ulong GetIncome()
        {
            return m_CachedIncomeValue;
        }

        /// <summary>
        /// Return damage value witout crit and etc random modifiers
        /// </summary>
        /// <returns></returns>
        public ulong GetDamageWithoutRandomModifiers()
        {
            return m_CachedDamageValue;
        }

        /// <summary>
        /// Return income value witout crit and etc random modifiers
        /// </summary>
        /// <returns></returns>
        public ulong GetIncomeWithoutRandomModifiers()
        {
            return m_CachedIncomeValue;
        }

        /// <summary>
        /// Will be called when any change event
        /// </summary>
        protected virtual void OnAnyChange()
        {
            RecalculateCachedValues();

            if (Event_AnyChange != null)
                Event_AnyChange();
        }
        
        private bool PurchaseMaster(StoreValue storeValue, ulong value)
        {
            if (storeValue.Value >= value)
            {
                storeValue.Decrease(value);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Subscribe on events
        /// </summary>
        private void ActivateEventListeners()
        {
            Inventory.Event_ItemRemove += OnInventoryItemRemove;
            Equipment.Event_AnyChange += OnAnyChange;
            Abilities.Event_AnyAbilityChange += OnAnyAbilityChange;
        }

        private void OnInventoryItemRemove(InventoryItem item)
        {
            var entity = Equipment.FindEntity(item.Target);

            if (entity != null)
            {
                if (entity.AttachedInventoryItem == item)
                {
                    entity.RemoveEquipment();
                }
            }
        }

        private void OnAnyAbilityChange(Ability ability)
        {
            OnAnyChange();
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["stats_moneys"] = Money.Value;
            data["stats_medals"] = Medals.Value;

            data["statistics"] = Statistics.GetStorageData();
            data["inventory"] = Inventory.GetStorageData();
            data["equipment"] = Equipment.GetStorageData();
            data["ranks"] = RankSystem.GetStorageData();
            data["science"] = Science.GetStorageData();

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<ulong>("stats_moneys"))
            {
                Money.SetValue((ulong)storageData["stats_moneys"]);
            }

            if (storageData.KeyValueIs<ulong>("stats_medals"))
            {
                Medals.SetValue((ulong)storageData["stats_medals"]);
            }

            RestoreFromStorage(Statistics, storageData, "statistics");
            RestoreFromStorage(Inventory, storageData, "inventory");
            RestoreFromStorage(Equipment, storageData, "equipment");
            RestoreFromStorage(RankSystem, storageData, "ranks");
            RestoreFromStorage(Science, storageData, "science");

            return true;
        }

        private ulong GetCriticalDamage()
        {
            var offset = m_CachedAbilityCriticalChance.Value > 100 
                ? m_CachedAbilityCriticalChance.Value * 10 
                : 100;

            var chance = (ulong)Random.Range(0, offset);

            return m_CachedAbilityCriticalChance.Value >= chance
                ? m_CachedAbilityCriticalDamage.ApplyToValue(GetDamageWithoutRandomModifiers())
                : 0;
        }

        private void RestoreFromStorage(IStorage target, StorageDataContainer container, string key)
        {
            if (container.KeyValueIs<StorageDataContainer>(key))
            {
                target.SetStorageData(container[key] as StorageDataContainer);
            }
        }
    }
}
