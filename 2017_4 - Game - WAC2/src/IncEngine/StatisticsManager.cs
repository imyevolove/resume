﻿using IncEngine.Personal;
using IncEngine.Storage;

namespace IncEngine
{
    public class StatisticsManager : IStorage
    {
        public StoreValue MoneyCollected = new StoreValue();
        public StoreValue MedalsCollected = new StoreValue();

        public StoreValue MissionLevelCompleted = new StoreValue();
        public StoreValue MissionBossCompleted = new StoreValue();

        public StoreValue WeaponsCollected = new StoreValue();
        public StoreValue GunsCollected = new StoreValue();
        public StoreValue KnivesCollected = new StoreValue();
        public StoreValue GlovesCollected = new StoreValue();

        public StoreValue SkinsSolded = new StoreValue();
        public StoreValue Touches = new StoreValue();
        public StoreValue CasesOpened = new StoreValue();
        public StoreValue PastedStickers = new StoreValue();

        public StatisticsManager(Player player)
        {
            player.Money.Event_Increase  += (value) => { MoneyCollected.Increase(value); };
            player.Medals.Event_Increase += (value) => { MedalsCollected.Increase(value); };
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            data["MoneyCollected"] = MoneyCollected.Value;
            data["MedalsCollected"] = MedalsCollected.Value;

            data["MissionLevelCompleted"] = MissionLevelCompleted.Value;
            data["MissionBossCompleted"] = MissionBossCompleted.Value;

            data["WeaponsCollected"] = WeaponsCollected.Value;
            data["GunsCollected"] = GunsCollected.Value;
            data["KnivesCollected"] = KnivesCollected.Value;
            data["GlovesCollected"] = GlovesCollected.Value;
            
            data["SkinsSolded"] = SkinsSolded.Value;
            data["Touches"] = Touches.Value;
            data["CasesOpened"] = CasesOpened.Value;
            data["PastedStickers"] = PastedStickers.Value;
            
            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if(storageData == null) return false;

            DecodeValueFromStorageDataContainer(storageData, MoneyCollected,        "MoneyCollected");
            DecodeValueFromStorageDataContainer(storageData, MedalsCollected,       "MedalsCollected");
            DecodeValueFromStorageDataContainer(storageData, MissionLevelCompleted, "MissionLevelCompleted");
            DecodeValueFromStorageDataContainer(storageData, MissionBossCompleted,  "MissionBossCompleted");
            DecodeValueFromStorageDataContainer(storageData, WeaponsCollected,      "WeaponsCollected");
            DecodeValueFromStorageDataContainer(storageData, GunsCollected,         "GunsCollected");
            DecodeValueFromStorageDataContainer(storageData, KnivesCollected,       "KnivesCollected");
            DecodeValueFromStorageDataContainer(storageData, GlovesCollected,       "GlovesCollected");
            DecodeValueFromStorageDataContainer(storageData, SkinsSolded,           "SkinsSolded");
            DecodeValueFromStorageDataContainer(storageData, Touches,               "Touches");
            DecodeValueFromStorageDataContainer(storageData, CasesOpened,           "CasesOpened");
            DecodeValueFromStorageDataContainer(storageData, PastedStickers,        "PastedStickers");

            return true;
        }

        private void DecodeValueFromStorageDataContainer(StorageDataContainer container, StoreValue value, string key)
        {
            if (container.KeyValueIs<ulong>(key))
            {
                value.SetValue((ulong)container[key]);
            }
        }
    }
}
