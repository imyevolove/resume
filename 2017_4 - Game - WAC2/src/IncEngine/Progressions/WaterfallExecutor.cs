﻿using System.Collections.Generic;
using System.Linq;

namespace IncEngine.Progressions
{
    public enum WaterfallExecutorState
    {
        ERROR,
        DONE,
        PERFORMED
    }

    /// <summary>
    /// Executor for waterfall actions
    /// </summary>
    public class WaterfallExecutor
    {
        public WaterfallExecutorState State { get; protected set; }

        private List<WaterfallAction> m_Actions;
        private WaterfallCallback m_Callback;
        private int m_Index;

        public WaterfallExecutor(Waterfall waterfall, WaterfallCallback callback)
        {
            m_Index = -1;
            m_Actions = waterfall.Actions.ToList();
            m_Callback = callback;
            State = WaterfallExecutorState.PERFORMED;

            ExecuteNextAction();
        }

        protected void ExecuteNextAction(WaterfallException exception = null, params object[] args)
        {
            /// Handle exception
            if (exception != null)
            {
                State = WaterfallExecutorState.ERROR;
                m_Callback(exception);
                return;
            }

            m_Index++;

            /// Exit. 
            if (m_Index >= m_Actions.Count)
            {
                State = WaterfallExecutorState.DONE;
                m_Callback(null, args);
                return;
            }

            m_Actions[m_Index](ExecuteNextAction);
        }
    }
}
