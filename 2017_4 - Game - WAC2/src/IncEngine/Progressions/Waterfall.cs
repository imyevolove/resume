﻿using System.Collections.Generic;
using System.Linq;

namespace IncEngine.Progressions
{
    /// <summary>
    /// Waterfall. Sequential execution of operations
    /// </summary>
    public class Waterfall
    {
        public List<WaterfallAction> Actions { get; private set; }

        /// <summary>
        /// Default waterfall contructor
        /// </summary>
        public Waterfall()
        {
            Actions = new List<WaterfallAction>();
        }

        /// <summary>
        /// Instantiate waterfall with actions
        /// </summary>
        /// <param name="actions"></param>
        public Waterfall(List<WaterfallAction> actions)
        {
            Actions = actions;
        }

        /// <summary>
        /// Instantiate waterfall with actions by params
        /// </summary>
        /// <param name="actions"></param>
        public Waterfall(params WaterfallAction[] actions)
        {
            Actions = (actions == null) ? new List<WaterfallAction>() : actions.ToList();
        }

        /// <summary>
        /// Instantiate callback and execute now
        /// </summary>
        /// <param name="actions"></param>
        /// <param name="callback"></param>
        public Waterfall(List<WaterfallAction> actions, WaterfallCallback callback)
        {
            Actions = actions;
            Execute(callback);
        }

        /// <summary>
        /// Start execute actions
        /// </summary>
        public WaterfallExecutor Execute(WaterfallCallback callback)
        {
            var executor = new WaterfallExecutor(this, callback);
            return executor;
        }
    }
}
