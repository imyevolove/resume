﻿using System.Collections.Generic;

namespace IncEngine.Progressions
{
    public class WaterfallBuilder : IWaterfallBuilder
    {
        private List<WaterfallAction> m_Actions = new List<WaterfallAction>();

        /// <summary>
        /// Add action to actions list
        /// </summary>
        /// <param name="action"></param>
        public IWaterfallBuilder AddAction(WaterfallAction action)
        {
            m_Actions.Add(action);
            return this;
        }

        /// <summary>
        /// Build waterfall
        /// </summary>
        /// <returns></returns>
        public Waterfall Build()
        {
            return new Waterfall(m_Actions);
        }

        /// <summary>
        /// Build wnd execute waterfall
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public Waterfall Build(WaterfallCallback callback)
        {
            return new Waterfall(m_Actions, callback);
        }

        /// <summary>
        /// Return new WaterfallBuilder instance
        /// </summary>
        /// <returns></returns>
        public static WaterfallBuilder Create()
        {
            return new WaterfallBuilder();
        }
    }
}
