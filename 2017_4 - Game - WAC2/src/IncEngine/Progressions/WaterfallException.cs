﻿using System;

namespace IncEngine.Progressions
{
    public class WaterfallException : Exception
    {
        public WaterfallException()
        {
        }

        public WaterfallException(string message)
        : base(message)
        {
        }

        public WaterfallException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
