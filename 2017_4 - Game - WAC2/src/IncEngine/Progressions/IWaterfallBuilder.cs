﻿namespace IncEngine.Progressions
{
    public interface IWaterfallBuilder
    {
        /// <summary>
        /// Add action to actions list
        /// </summary>
        /// <param name="action"></param>
        IWaterfallBuilder AddAction(WaterfallAction action);

        /// <summary>
        /// Build waterfall
        /// </summary>
        /// <returns></returns>
        Waterfall Build();

        /// <summary>
        /// Build wnd execute waterfall
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        Waterfall Build(WaterfallCallback callback);
    }
}
