﻿namespace IncEngine.Formatters
{
    public interface INumberFormatter
    {
        string Format(ulong value);
        string Format(long value);
        string Format(uint value);
        string Format(int value);
        string Format(float value);
    }
}
