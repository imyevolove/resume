﻿using System.Globalization;

namespace IncEngine.Formatters
{
    public class NumberFormatterSpace : INumberFormatter
    {
        public readonly static NumberFormatInfo FormatInfo = new NumberFormatInfo { NumberGroupSeparator = " " };
        
        public string Format(ulong value)
        {
            return value.ToString("#,0", FormatInfo);
        }

        public string Format(long value)
        {
            return value.ToString("#,0", FormatInfo);
        }

        public string Format(uint value)
        {
            return value.ToString("#,0", FormatInfo);
        }

        public string Format(int value)
        {
            return value.ToString("#,0", FormatInfo);
        }

        public string Format(float value)
        {
            return value.ToString("#,0.000", FormatInfo);
        }
    }
}
