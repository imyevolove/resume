﻿using UnityEngine;

namespace IncEngine.Formatters
{
    public class NumberFormatterPrefix : INumberFormatter
    {
        ////////////////////////////////////////////////////////////
        public static readonly string[] DEFAULT_ABBREVIATION = new string[21]
        {
            "K", "M", "B", "T", "q", "Q", "s",
            "S", "O", "N", "d", "U", "D", "!",
            "@", "#", "$", "%", "^", "&", "*"
        };

        private const int DIVIDER = 3;

        public readonly string[] abbreviation;

        ////////////////////////////////////////////////////////////
        public NumberFormatterPrefix(string[] abbreviation)
        {
            this.abbreviation = abbreviation;
        }

        ////////////////////////////////////////////////////////////
        public string Format(ulong value)
        {
            if (value == 0) return "0";

            var index = Mathf.FloorToInt(Mathf.Log10(value) / DIVIDER);

            if (index <= 0) return value.ToString();

            var di = Mathf.Pow(10, index * DIVIDER);
            var result = value / di;
            var abbIndex = Mathf.Clamp(index - 1, 0, abbreviation.Length - 1);

            return result.ToString("#.###") + abbreviation[abbIndex];
        }

        ////////////////////////////////////////////////////////////
        public string Format(long value)
        {
            return GetNumberSign(value) + Format((ulong)Mathf.Abs(value));
        }

        ////////////////////////////////////////////////////////////
        public string Format(uint value)
        {
            return Format((ulong)Mathf.Abs(value));
        }

        ////////////////////////////////////////////////////////////
        public string Format(int value)
        {
            return GetNumberSign(value) + Format((ulong)Mathf.Abs(value));
        }

        ////////////////////////////////////////////////////////////
        public string Format(float value)
        {
            if (value == 0) return "0";

            var index = Mathf.FloorToInt(Mathf.Log10(value) / DIVIDER);

            if (index <= 0) return value.ToString();

            var di = Mathf.Pow(10, index * DIVIDER);
            var result = value / di;
            var abbIndex = Mathf.Clamp(index - 1, 0, abbreviation.Length - 1);

            return result.ToString("#.###") + abbreviation[abbIndex];
        }

        private string GetNumberSign(long value)
        {
            return value < 0 ? "-" : "";
        }

        private string GetNumberSign(int value)
        {
            return value < 0 ? "-" : "";
        }
    }
}
