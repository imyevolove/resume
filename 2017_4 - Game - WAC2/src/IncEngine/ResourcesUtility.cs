﻿using System.IO;
using UnityEngine;
using Wac.Resources;
using Uni.DependencyInjection;
using System.Collections.Generic;

namespace IncEngine
{
    public class ResourcesUtility
    {
        private static ISpriteProvider _spriteProvider;
        private static Dictionary<string, Sprite> _spriteProviderCache = new Dictionary<string, Sprite>();

        public static T Load<T>(string path) where T : UnityEngine.Object
        {
            path = NormalizePath(path);
            return Resources.Load<T>(path);
        }

        public static Texture2D LoadTexture2D(string path)
        {
            return Load<Texture2D>(path);
        }

        public static Sprite LoadSprite(string path)
        {
            return Load<Sprite>(path);
        }

        public static Sprite LoadSpriteFromApplicationResources(string name)
        {
            if (_spriteProvider == null)
            {
                _spriteProvider = Uni.ApplicationManagement.Application.Main.ServiceProvider.GetService<ISpriteProvider>();
            }

            Sprite result;

            _spriteProviderCache.TryGetValue(name, out result);

            if (result == null)
            {
                result = _spriteProviderCache[name] = _spriteProvider.Get(name);
            }

            return result;
        }

        public static string NormalizePath(string path)
        {
            return Path.ChangeExtension(path, null);
        }
    }
}