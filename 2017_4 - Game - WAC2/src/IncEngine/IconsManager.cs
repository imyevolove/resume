﻿using UnityEngine;

namespace IncEngine
{
    public class IconsManager
    {
        public static Sprite CoinIcon   { get { return ResourcesUtility.LoadSpriteFromApplicationResources("ico_eco_coin");  } }
        public static Sprite MedalIcon  { get { return ResourcesUtility.LoadSpriteFromApplicationResources("ico_eco_medal"); } }
        public static Sprite CaseIcon   { get { return ResourcesUtility.LoadSpriteFromApplicationResources("ico_eco_case");  } }
    }
}
