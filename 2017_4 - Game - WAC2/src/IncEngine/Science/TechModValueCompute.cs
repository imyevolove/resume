﻿namespace IncEngine.Science
{
    public class TechModValueCompute : ITechValueCompute
    {
        public virtual ulong Compute(ulong baseValue, ulong level)
        {
            return baseValue * level;
        }

        public virtual ulong ComputeDifference(ulong baseValue, ulong level, uint offset = 1)
        {
            var currentMod = Compute(baseValue, level);
            var nextMod = Compute(baseValue, level + offset);

            return nextMod - currentMod;
        }
    }
}