﻿using System.Collections.Generic;
using IncEngine.Abilities;
using IncEngine.Data;

namespace IncEngine.Science
{
    public static class TechMapperFactory
    {
        public static TechMapper CreateTechMapperFromBinds<TTechMapper>(IEnumerable<TechBind> binds, AbilityMapper abilityMapper) where TTechMapper : TechMapper, new()
        {
            ITechMapperBuilder<TTechMapper> scienceManagerBuilder = new TechMapperBuilder<TTechMapper>();

            foreach (var detalis in binds)
            {
                var ability = abilityMapper.GetAbility(detalis.AbilityType);
                var tech = new Tech(detalis, ability);

                scienceManagerBuilder.AddTech(tech);
            }

            return scienceManagerBuilder.Build();
        }
    }
}
