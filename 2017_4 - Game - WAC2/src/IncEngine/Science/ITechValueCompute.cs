﻿namespace IncEngine.Science
{
    public interface ITechValueCompute
    {
        ulong Compute(ulong baseValue, ulong level);
    }
}
