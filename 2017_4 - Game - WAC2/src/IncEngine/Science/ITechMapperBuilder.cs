﻿namespace IncEngine.Science
{
    public interface ITechMapperBuilder<TScienceManager> where TScienceManager : TechMapper, new()
    {
        ITechMapperBuilder<TScienceManager> AddTech(Tech tech);
        TScienceManager Build();
    }
}
