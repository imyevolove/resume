﻿using IncEngine.Storage;
using System;
using System.Collections.Generic;

namespace IncEngine.Science
{
    public class TechMapper : IStorage
    {
        public event Action<Tech> Event_AddTech;
        public event Action<Tech> Event_RemoveTech;

        /// <summary>
        /// Technologies list.
        /// Here contains all registered technologies.
        /// </summary>
        public List<Tech> Technologies { get; private set; }

        public TechMapper()
        {
            Technologies = new List<Tech>();
        }

        public Tech Find(string id)
        {
            return Technologies.Find(tech => tech.Id == id);
        }

        /// <summary>
        /// Add tech to manager.
        /// Return add status.
        /// </summary>
        /// <param name="tech"></param>
        /// <returns></returns>
        public bool AddTech(Tech tech)
        {
            if (Technologies.Contains(tech)) return false;

            Technologies.Add(tech);

            OnAddTech(tech);

            return true;
        }

        /// <summary>
        /// Add tech array
        /// </summary>
        /// <param name="techs"></param>
        public void AddTech(IEnumerable<Tech> techs)
        {
            foreach (var tech in techs)
                AddTech(tech);
        }

        /// <summary>
        /// Remove tech from manager by index.
        /// Return remove status.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool RemoveTech(int index)
        {
            if (index < 0 || Technologies.Count >= index) return false;
            return RemoveTech(Technologies[index]);
        }

        /// <summary>
        /// Remove tech from manager.
        /// Return remove status.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool RemoveTech(Tech tech)
        {
            var removed = Technologies.Remove(tech);

            if (removed) OnRemoveTech(tech);

            return removed;
        }

        /// <summary>
        /// Call when tech add
        /// </summary>
        /// <param name="tech"></param>
        protected virtual void OnAddTech(Tech tech)
        {
            if (Event_AddTech != null)
                Event_AddTech(tech);
        }

        /// <summary>
        /// Call when tech removed
        /// </summary>
        /// <param name="tech"></param>
        protected virtual void OnRemoveTech(Tech tech)
        {
            if (Event_RemoveTech != null)
                Event_RemoveTech(tech);
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();

            List<StorageDataContainer> techData = new List<StorageDataContainer>();

            foreach (var tech in Technologies)
            {
                techData.Add(tech.GetStorageData());
            }

            data["items"] = techData;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<List<StorageDataContainer>>("items"))
            {
                foreach (var itemData in ((ICollection<StorageDataContainer>)storageData["items"]))
                {
                    if (itemData.KeyValueIs<string>("id"))
                    {
                        var tech = Find((string)itemData["id"]);

                        if (tech != null)
                        {
                            tech.SetStorageData(itemData);
                        }
                    }
                }
            }

            return true;
        }
    }
}