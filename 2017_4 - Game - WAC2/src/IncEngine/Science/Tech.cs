﻿using System;
using UnityEngine;
using IncEngine.Abilities;
using IncEngine.Storage;
using Wac;

namespace IncEngine.Science
{
    [Serializable]
    public class Tech : IStorage
    {
        public event Action<Tech, uint> Event_Improve;
        public event Action<Tech> Event_Change;

        public uint                 Level           { get; protected set; }
        public TechPriceCompute     PriceCompute    { get; set; }
        public TechModValueCompute  ModValueCompute { get; set; }
        public Ability              Ability         { get; private set; }

        public string   Id              { get { return m_Details.Name.ToUpper(); } }
        public string   Name            { get { return m_Details.Name; } }
        public Sprite   Icon            { get { return m_Details.Icon; } }
        public ulong    BasePrice       { get { return m_Details.BasePrice; } }
        public ulong    BaseModValue    { get { return m_Details.BaseModValue; } }
        public uint     MaxLevel        { get { return m_Details.MaxLevel; } }
        public bool     IsMaximized     { get { return Level >= MaxLevel; } }

        private TechDetails m_Details;

        public Tech(TechDetails details, Ability ability)
        {
            m_Details = details;
            Ability = ability;

            Ability.Event_RestoredToBase += RestoreTechForAbility;
        }
        
        /// <summary>
        /// Return improve availability status
        /// </summary>
        /// <returns></returns>
        public bool CanImprove()
        {
            return Level < MaxLevel;
        }

        /// <summary>
        /// Return improve availability status for count
        /// </summary>
        /// <returns></returns>
        public bool CanImprove(uint count)
        {
            return (Level + count) <= MaxLevel;
        }

        /// <summary>
        /// Improve level by 1.
        /// </summary>
        /// <returns></returns>
        public bool Improve()
        {
            return Improve(1);
        }

        /// <summary>
        /// Improve level by count.
        /// </summary>
        /// <returns></returns>
        public bool Improve(uint count)
        {
            if (!CanImprove(count)) return false;

            Ability.Value += GetModifier(count);

            Level += count;

            OnLevelChange();

            return true;
        }

        public bool SetLevel(uint level)
        {
            level = (uint)Mathf.Clamp((int)level, 0, (int)MaxLevel);

            Ability.Value += GetModifier(level);

            Level = level;

            OnLevelChange();

            return true;
        }

        /// <summary>
        /// Get next improve price
        /// </summary>
        /// <returns></returns>
        public ulong GetPrice()
        {
            var priceCompute = PriceCompute ?? TechComputeMapper.priceCompute;
            var price = priceCompute.Compute(BasePrice, Level);
            price = Game.Player.Abilities.GetAbility(AbilityType.ScienceReducePrices).ApplyToValue(price);

            return price;
        }


        /// <summary>
        /// Get modifier
        /// </summary>
        /// <returns></returns>
        public ulong GetModifier()
        {
            var modCompute = ModValueCompute ?? TechComputeMapper.modValueCompute;
            return modCompute.ComputeDifference(BaseModValue, Level);
        }

        /// <summary>
        /// Get modifier
        /// </summary>
        /// <returns></returns>
        public ulong GetModifier(uint offset)
        {
            return GetModifier(Level, offset);
        }

        /// <summary>
        /// Get modifier
        /// </summary>
        /// <returns></returns>
        public ulong GetModifier(uint level, uint offset)
        {
            var modCompute = ModValueCompute ?? TechComputeMapper.modValueCompute;
            return modCompute.ComputeDifference(BaseModValue, level, offset);
        }

        /// <summary>
        /// Get next improve price
        /// </summary>
        /// <returns></returns>
        public ulong GetNextAbilityValue()
        {
            return Ability.Value + GetModifier();
        }

        public StorageDataContainer GetStorageData()
        {
            StorageDataContainer data = StorageDataContainer.CreateContainer();
            
            data["id"] = Id;
            data["level"] = Level;

            return data;
        }

        public bool SetStorageData(StorageDataContainer storageData)
        {
            if (storageData.KeyValueIs<uint>("level"))
            {
                var level = (uint)storageData["level"];
                Improve(level > MaxLevel ? MaxLevel : level);

                return true;
            }

            return false;
        }

        protected virtual void OnTechChange()
        {
            if (Event_Change != null)
                Event_Change(this);
        }

        protected virtual void OnLevelChange()
        {
            if (Event_Improve != null)
                Event_Improve(this, Level);

            OnTechChange();
        }

        private void RestoreTechForAbility()
        {
            Ability.Value += GetModifier(0, Level);
        }
    }
}