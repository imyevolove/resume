﻿using System;
using UnityEngine;

namespace IncEngine.Science
{
    [Serializable]
    public class TechDetails
    {
        [SerializeField]
        public string       Name;
        [SerializeField]
        public Sprite       Icon;
        [SerializeField]
        public uint         BasePrice;
        [SerializeField]
        public uint         BaseModValue;
        [SerializeField]
        public uint         MaxLevel;
    }
}