﻿namespace IncEngine.Science
{
    public class TechMapperBuilder<TScienceManager>
        : ITechMapperBuilder<TScienceManager> where TScienceManager : TechMapper, new()
    {
        TScienceManager scienceManager = new TScienceManager();

        public ITechMapperBuilder<TScienceManager> AddTech(Tech tech)
        {
            scienceManager.AddTech(tech);
            return this;
        }

        public TScienceManager Build()
        {
            return scienceManager;
        }
    }
}
