﻿using UnityEngine;

namespace IncEngine.Science
{
    public class TechPriceCompute : ITechValueCompute
    {
        public virtual ulong Compute(ulong baseValue, ulong level)
        {
            return (ulong)(baseValue * (level + 1) * Mathf.Max(1, level / 15 * 2));
        }
    }
}