﻿using IncEngine.UI;
using System;
using UnityEngine;

namespace IncEngine
{
    public class DialogManager
    {
        private static DialogManagerConfiguration m_Configuration = new DialogManagerConfiguration();
        public static RectTransform ParentContainer { get { return m_Configuration.ParentContainer; } }
        
        public static bool SetConfiguration(DialogManagerConfiguration configuration)
        {
            if (configuration == null) return false;

            m_Configuration = configuration;
            return true;
        }

        public static void SetContainer(RectTransform container)
        {
            m_Configuration.ParentContainer = container;
        }

        public static void RegisterDialog<T>(T original) where T : Dialog
        {
            if (original == null) return;

            var type = original.GetType();

            if (m_Configuration.pool.ContainsKey(type))
            {
                Debug.LogWarning(string.Format("Dialog [{0}] already registered", type.Name));
                return;
            }

            var pool = new MonoEntityPool<Dialog>(original);
            m_Configuration.pool.Add(type, pool);
        }

        public static T CreateDialog<T>() where T : Dialog
        {
            var pool = GetPool(typeof(T));
            if (pool == null) return null;

            var dialog = pool.GetFree();
            if (dialog == null) return null;

            if (ParentContainer != null)
            {
                dialog.RectTransform.SetParent(ParentContainer, false);
            }

            return dialog as T;
        }

        private static MonoEntityPool<Dialog> GetPool(Type type)
        {
            MonoEntityPool<Dialog> pool = null;
            m_Configuration.pool.TryGetValue(type, out pool);

            return pool;
        }
    }
}
