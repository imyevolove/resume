﻿using IncEngine.Dice;
using IncEngine.Structures;
using IncEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine
{
    [Serializable]
    public class CaseRollSlider : RollSlider
    {
        public event Action<Skin> Event_RollEndSkin;

        [SerializeField] public SkinRoll skinRollPrefab;
        [SerializeField] public Gradient lightingGradient;
        [SerializeField] public float lightingDistance;
        [SerializeField] public bool useLighting;

        private MonoEntityPool<SkinRoll> m_SkinRollPool;
        private List<SkinRoll> m_ActivatedSkinExlorers;

        private List<Skin> m_AvailableSkins;

        private float m_LightingHelperViewportCenter;
        private float m_LightingHelperViewportHalfWidth;
        private float m_LightingHelperExplorerPositionX;
        private float m_LightingHelperExplorerEvaluateTime;

        protected override void Awake()
        {
            base.Awake();

            m_SkinRollPool = new MonoEntityPool<SkinRoll>(skinRollPrefab);
            m_ActivatedSkinExlorers = new List<SkinRoll>();
            m_AvailableSkins = new List<Skin>();
        }

        protected override void Start()
        {
            base.Start();

            /// Avoid broken first rebuild.
            WaitForFrames(RebuildLighting, 1);
        }

        protected void OnValidate()
        {
            //if (!Application.isPlaying) return;
            //RebuildLighting();
        }

        public void RebuildRollItems(IEnumerable<Skin> skins)
        {
            DeactivateActivatedExplorerItems();

            foreach (var skin in skins)
            {
                var explorer = GetFreeSkinExplorerItem();
                explorer.SetTarget(skin);
                explorer.Transform.SetAsLastSibling();
            }

            RestoreSlider();
            RebuildLighting();
        }

        /// <summary>
        /// Rebuild skins with simple random
        /// </summary>
        /// <param name="cs"></param>
        /// <param name="count"></param>
        /// <param name="opener"></param>
        public void RebuildRollItems(Case cs, int count, CaseOpener opener)
        {
            m_AvailableSkins = opener.GetRandomSkins(cs, count);
            RebuildRollItems(m_AvailableSkins);
        }

        public void Roll(Case cs, int count, CaseOpener opener)
        {
            Stop();

            m_AvailableSkins = opener.GetRandomSkins(cs, count, null);
            RebuildRollItems(m_AvailableSkins);

            Roll(count);
        }

        /// <summary>
        /// Roll with subscribe on single shot end callback [index]
        /// </summary>
        /// <param name="cs"></param>
        /// <param name="count"></param>
        /// <param name="opener"></param>
        /// <param name="onEndAction"></param>
        public void Roll(Case cs, int count, CaseOpener opener, Action<int> onEndAction)
        {
            Action<int> tDelegate = null;
            tDelegate = (index) => {
                Event_RollEnd -= tDelegate;
                onEndAction(index);
            };

            Event_RollEnd += tDelegate;
            Roll(cs, count, opener);
        }

        /// <summary>
        /// Roll with subscribe on single shot end callback [Skin]
        /// </summary>
        /// <param name="cs"></param>
        /// <param name="count"></param>
        /// <param name="opener"></param>
        /// <param name="onEndAction"></param>
        public void Roll(Case cs, int count, CaseOpener opener, Action<Skin> onEndAction)
        {
            Action<Skin> tDelegate = null;
            tDelegate = (skin) => {
                Event_RollEndSkin -= tDelegate;
                onEndAction(skin);
            };

            Event_RollEndSkin += tDelegate;
            Roll(cs, count, opener);

        }

        public void RebuildLighting()
        {
            if (!useLighting || viewport == null) return;
            
            m_LightingHelperViewportCenter = Mathf.Abs(viewport.Transform.position.x);
            m_LightingHelperViewportHalfWidth = viewport.Transform.rect.width / 2;
            m_LightingHelperExplorerPositionX = 0f;
            m_LightingHelperExplorerEvaluateTime = 0f;

            foreach (var item in m_ActivatedSkinExlorers)
            {
                m_LightingHelperExplorerPositionX = Mathf.Abs(item.Transform.position.x - m_LightingHelperViewportCenter);
                m_LightingHelperExplorerEvaluateTime = Mathf.Clamp01(
                    (m_LightingHelperViewportCenter - m_LightingHelperExplorerPositionX) 
                    / m_LightingHelperViewportHalfWidth * lightingDistance);
                item.gradientImage.color = lightingGradient.Evaluate(m_LightingHelperExplorerEvaluateTime);
            }
        }

        protected override void OnRoll(float positionX)
        {
            base.OnRoll(positionX);
            RebuildLighting();
        }

        protected override void OnRollEnd()
        {
            base.OnRollEnd();

            if (Event_RollEndSkin != null)
            {
                var skin = ResultIndex > 0 && m_AvailableSkins.Count >= ResultIndex
                    ? m_AvailableSkins[ResultIndex] 
                    : null;
                Event_RollEndSkin(skin);
            }
        }

        protected void DeactivateActivatedExplorerItems()
        {
            foreach (var explorer in m_ActivatedSkinExlorers)
            {
                explorer.Deactivate();
            }

            m_ActivatedSkinExlorers.Clear();
        }

        protected void WaitForFrames(Action action, int count)
        {
            StartCoroutine(WatForFramesEnumerator(action, count));
        }

        private IEnumerator WatForFramesEnumerator(Action action, int count)
        {
            var counter = 0;
            while (counter < count)
            {
                counter++;
                yield return null;
            }

            action();
        }

        private SkinRoll GetFreeSkinExplorerItem()
        {
            var explorer = m_SkinRollPool.GetFree();
            explorer.transform.SetParent(content.Transform, false);

            m_ActivatedSkinExlorers.Add(explorer);
            
            return explorer;
        }
        
    }
}
