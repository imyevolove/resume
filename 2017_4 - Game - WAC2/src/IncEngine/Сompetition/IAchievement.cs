﻿using System;
using UnityEngine;

namespace IncEngine.Сompetition
{
    public interface IAchievement
    {
        event Action Event_Locked;
        event Action Event_Unlocked;

        string  Name        { get; }
        Sprite  Icon        { get; }
        bool    Unlocked    { get; }

        void    Lock();
        void    Unlock();
    }
}
