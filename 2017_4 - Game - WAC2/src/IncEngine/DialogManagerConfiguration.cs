﻿using IncEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine
{
    public class DialogManagerConfiguration
    {
        public readonly Dictionary<Type, MonoEntityPool<Dialog>> pool = new Dictionary<Type, MonoEntityPool<Dialog>>();
        public RectTransform ParentContainer { get; set; }
    }
}
