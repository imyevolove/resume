﻿using IncEngine.Exceptions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine
{
    /// <summary>
    /// GameSession используется для создания сессии игры.
    /// Сессия помогает осуществлять глобальную взаимосвязь между объектами
    /// </summary>
    public class GameSession : ISession
    {
        private static GameSession m_CurrentGameSession;
        public  static GameSession Current { get { return m_CurrentGameSession; } }

        public static bool HasRunnedSession { get { return m_CurrentGameSession != null; } }

        /// <summary>
        /// Стартуем новую игровую сессию.
        /// В случае если сессия уже существует будет вызвана ошибка.
        /// </summary>
        /// <returns></returns>
        public static TSession Start<TSession>() where TSession : GameSession, new()
        {
            /// Сессия уже запущена.
            if (HasRunnedSession)
            {
                throw new GameSessionRunnedException();
            }

            /// Запуск сессии
            TSession session = new TSession();

            m_CurrentGameSession = session;

            return session;
        }

        /// <summary>
        /// Возвращает текущую сессию с преобразованием типа
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <returns></returns>
        public static TSession GetCurrentSessionAs<TSession>() where TSession : GameSession
        {
            return m_CurrentGameSession as TSession;
        }

        private Dictionary<Type, object> m_SingletoneContainer = new Dictionary<Type, object>();

        protected GameSession()
        {
        }

        public void AddSingleton<T>(T implementation)
        {
            var type = typeof(T);

            if (m_SingletoneContainer.ContainsKey(type))
            {
                Debug.LogFormat(
                    "Singleton with type '{0}' already defined. Singleton will be replaced", 
                    type.FullName);

                m_SingletoneContainer.Remove(type);
            }

            m_SingletoneContainer.Add(type, implementation);
        }

        public T GetSingleton<T>()
        {
            try
            {
                return (T)m_SingletoneContainer[typeof(T)];
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public object GetSingleton(Type type)
        {
            try
            {
                return m_SingletoneContainer[type];
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
