﻿using System;
using UnityEngine;

namespace IncEngine
{
    [Serializable]
    public struct Range
    {
        [SerializeField] public float min;
        [SerializeField] public float max;

        /// <summary>
        /// Return random value between min[inclusive] and max[inclusive]
        /// </summary>
        /// <returns></returns>
        public float Random()
        {
            return UnityEngine.Random.Range(min, max);
        }
    }
}
