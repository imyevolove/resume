﻿namespace IncEngine.Abilities
{
    public class AbilityMemento
    {
        public readonly uint Value;

        public AbilityMemento(uint value)
        {
            Value = value;
        }
    }
}
