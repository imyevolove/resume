﻿using System;
using UnityEngine;

namespace IncEngine.Abilities
{
    using Events;

    public enum AbilityModValueType
    {
        Basic,
        Percentage
    }

    public enum AbilityModValueApplyType
    {
        Add,
        Devide
    }

    [Serializable]
    public class Ability
    {
        public event AbilityEvent Event_Modify;
        public event Action Event_RestoredToBase;

        [SerializeField] public AbilityModValueType valueType;
        [SerializeField] public AbilityModValueApplyType applyType;
        [SerializeField] public string Name;

        [SerializeField] private ulong m_Value;
        public ulong Value
        {
            get { return m_Value; }
            set
            {
                m_Value = value;
                OnModify();
            }
        }

        public Sprite   Icon { get; set; }


        private AbilityMemento m_BaseMemento;

        public Ability(uint baseValue)
        {
            Configure(baseValue, string.Empty);
        }

        public Ability(uint baseValue, string name)
        {
            Configure(baseValue, name);
        }

        public Ability(uint baseValue, string name, Sprite icon)
        {
            Configure(baseValue, name, icon);
        }

        public ulong ApplyToValue(ulong value)
        {
            if (Value == 0) return value;

            var mod = 0f;
            var result = 0UL;

            switch (valueType)
            {
                case AbilityModValueType.Percentage:
                    mod = value / 100f * Value;
                    break;
                case AbilityModValueType.Basic:
                default:
                    mod = Value;
                    break;
            }

            switch (applyType)
            {
                case AbilityModValueApplyType.Add:
                    result = value + (ulong)mod;
                    break;
                case AbilityModValueApplyType.Devide:
                default:
                    result = value - (ulong)mod;
                    break;
            }
            
            return result;
        }

        public void Restore()
        {
            Value = m_BaseMemento.Value;
            OnRestore();
        }

        protected virtual void OnModify()
        {
            if (Event_Modify != null)
            {
                Event_Modify(this, Value);
            }
        }

        protected virtual void OnRestore()
        {
            if (Event_RestoredToBase != null)
            {
                Event_RestoredToBase();
            }
        }

        private void Configure(uint baseValue, string name, Sprite icon = null)
        {
            m_BaseMemento = new AbilityMemento(baseValue);
            m_Value = baseValue;

            Name = name;
            Icon = icon;
        }
    }
}