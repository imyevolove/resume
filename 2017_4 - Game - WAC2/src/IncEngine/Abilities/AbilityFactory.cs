﻿namespace IncEngine.Abilities
{
    public enum AbilityType
    {
        ///
        TouchJackpotChance,
        TouchJackpotMultiplier,

        /// 
        ExperienceSpeedUpValue,
        
        ///
        TouchDamageCriticalDamage,
        TouchDamageCriticalChance,

        ///
        IncomeIncreaseValuePercentage,
        DamageIncreaseValuePercentage,

        ///
        SaleValueIncrease,
        UpgradeReducePrice,

        ///
        EarnCoinsIncreaseProfit,
        EarnCoinsFindMedalChance,
        EarnCoinsChanceMultiplie,
        EarnCoinsChance,

        ///
        FindCaseSpeedUp,

        ///
        IncreaseKnifeDropChance,
        IncreaseGloveDropChance,

        ///
        ReduceCasesPrice,
        
        ///
        ContractIncreaseBetterCraftChance,

        ///
        ScienceReducePrices,

        EarnCoinsFindMedalValue
    }

    public static class AbilityFactory
    {
        public static Ability CreateAbility(AbilityType abilityType)
        {
            IAbilityBuilder abilityBuilder = new AbilityBuilder();
            abilityBuilder.SetName(abilityType.ToString());

            switch (abilityType)
            {
                case AbilityType.TouchJackpotChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.TouchJackpotMultiplier:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
 
                case AbilityType.ExperienceSpeedUpValue:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
                    
                case AbilityType.TouchDamageCriticalDamage:
                    abilityBuilder.SetBaseValue(50);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.TouchDamageCriticalChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
                    
                case AbilityType.IncomeIncreaseValuePercentage:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.DamageIncreaseValuePercentage:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
                    
                case AbilityType.SaleValueIncrease:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.UpgradeReducePrice:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Devide);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.EarnCoinsIncreaseProfit:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Basic);
                    break;

                case AbilityType.EarnCoinsFindMedalChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.EarnCoinsFindMedalValue:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Basic);
                    break;

                case AbilityType.EarnCoinsChanceMultiplie:
                    abilityBuilder.SetBaseValue(2);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.EarnCoinsChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
                    
                case AbilityType.FindCaseSpeedUp:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Basic);
                    break;
                    
                case AbilityType.IncreaseKnifeDropChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.IncreaseGloveDropChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
                    
                case AbilityType.ReduceCasesPrice:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Devide);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;
                    
                case AbilityType.ContractIncreaseBetterCraftChance:
                    abilityBuilder.SetBaseValue(1);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Add);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                case AbilityType.ScienceReducePrices:
                    abilityBuilder.SetBaseValue(0);
                    abilityBuilder.SetIcon(ResourcesUtility.LoadSprite("aly/ico_aly_mouse"));
                    abilityBuilder.SetApplyType(AbilityModValueApplyType.Devide);
                    abilityBuilder.SetValueType(AbilityModValueType.Percentage);
                    break;

                default:
                    
                    break;
            }

            return abilityBuilder.Build();
        }
    }
}
