﻿using UnityEngine;

namespace IncEngine.Abilities
{
    public class AbilityBuilder : IAbilityBuilder
    {
        private AbilityModValueApplyType m_ApplyType;
        private AbilityModValueType m_ValueType;
        private Sprite m_Icon;
        private uint m_Value;
        private string m_Name;

        public IAbilityBuilder SetApplyType(AbilityModValueApplyType type)
        {
            m_ApplyType = type;
            return this;
        }

        public IAbilityBuilder SetBaseValue(uint value)
        {
            m_Value = value;
            return this;
        }

        public IAbilityBuilder SetIcon(Sprite sprite)
        {
            m_Icon = sprite;
            return this;
        }

        public IAbilityBuilder SetName(string name)
        {
            m_Name = name;
            return this;
        }

        public IAbilityBuilder SetValueType(AbilityModValueType type)
        {
            m_ValueType = type;
            return this;
        }

        public Ability Build()
        {
            return new Ability(m_Value, m_Name, m_Icon)
            {
                applyType = m_ApplyType,
                valueType = m_ValueType
            };
        }
    }
}
