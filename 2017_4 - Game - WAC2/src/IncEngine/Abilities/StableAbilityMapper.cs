﻿using System.Collections.Generic;

namespace IncEngine.Abilities
{
    public class StableAbilityMapper : AbilityMapper
    {
        private Dictionary<AbilityType, Ability> m_Abilities = new Dictionary<AbilityType, Ability>();

        /// <summary>
        /// Return ability by ability type.
        /// </summary>
        /// <param name="abilityType"></param>
        /// <returns></returns>
        public override Ability GetAbility(AbilityType abilityType)
        {
            Ability ability;

            if (!m_Abilities.TryGetValue(abilityType, out ability))
            {
                InstantiateAndRegisterAbility(abilityType);
                return GetAbility(abilityType);
            }

            return ability;
        }

        /// <summary>
        /// Set ability by type.
        /// </summary>
        /// <param name="abilityType"></param>
        /// <param name="ability"></param>
        public override void SetAbility(AbilityType abilityType, Ability ability)
        {
            AttachAbility(abilityType, ability);
        }

        /// <summary>
        /// Instantiate new ability by type and attach to container
        /// </summary>
        /// <param name="abilityType"></param>
        /// <returns></returns>
        protected Ability InstantiateAndRegisterAbility(AbilityType abilityType)
        {
            Ability ability = AbilityFactory.CreateAbility(abilityType);
            AttachAbility(abilityType, ability);

            return ability;
        }

        /// <summary>
        /// Attach ability to container
        /// </summary>
        /// <param name="abilityType"></param>
        /// <param name="ability"></param>
        protected void AttachAbility(AbilityType abilityType, Ability ability)
        {
            DeattachAbility(abilityType);
            m_Abilities[abilityType] = ability;

            ability.Event_Modify += OnAbilityChange;

            OnAbilityAttach(ability);
        }

        /// <summary>
        /// Deattach ability from container
        /// </summary>
        /// <param name="abilityType"></param>
        /// <returns></returns>
        protected bool DeattachAbility(AbilityType abilityType)
        {
            Ability ability;
            bool deleted = m_Abilities.TryGetValue(abilityType, out ability) && m_Abilities.Remove(abilityType);

            if (deleted)
            {
                ability.Event_Modify -= OnAbilityChange;
                OnAbilityDeattach(ability);
            }

            return deleted;
        }
    }
}