﻿using IncEngine.Abilities.Events;
using System;

namespace IncEngine.Abilities
{
    public abstract class AbilityMapper
    {
        public event AbilityMapperEvent Event_AbilityAttach;
        public event AbilityMapperEvent Event_AbilityDeattach;
        public event Action<Ability> Event_AnyAbilityChange;

        public abstract Ability GetAbility(AbilityType abilityType);
        public abstract void SetAbility(AbilityType abilityType, Ability ability);

        protected virtual void OnAbilityDeattach(Ability ability)
        {
            if (Event_AbilityDeattach != null)
                Event_AbilityDeattach(this, ability);
        }

        protected virtual void OnAbilityAttach(Ability ability)
        {
            if (Event_AbilityAttach != null)
                Event_AbilityAttach(this, ability);
        }

        protected virtual void OnAbilityChange(Ability ability, ulong value)
        {
            if (Event_AnyAbilityChange != null)
                Event_AnyAbilityChange(ability);
        }
    }
}
