﻿namespace IncEngine.Abilities.Events
{
    public delegate void AbilityMapperEvent(AbilityMapper abilitySystem, Ability ability);
}
