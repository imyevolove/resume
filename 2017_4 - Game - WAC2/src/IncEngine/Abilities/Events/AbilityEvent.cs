﻿namespace IncEngine.Abilities.Events
{
    public delegate void AbilityEvent(Ability ability, ulong value);
}
