﻿using UnityEngine;

namespace IncEngine.Abilities
{
    public interface IAbilityBuilder
    {
        IAbilityBuilder SetBaseValue(uint value);
        IAbilityBuilder SetName(string name);
        IAbilityBuilder SetIcon(Sprite sprite);
        IAbilityBuilder SetApplyType(AbilityModValueApplyType type);
        IAbilityBuilder SetValueType(AbilityModValueType type);
        Ability Build();
    }
}
