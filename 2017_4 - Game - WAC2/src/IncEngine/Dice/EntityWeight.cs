﻿using IncEngine.Collections;
using IncEngine.Structures;

namespace IncEngine.Dice
{
    public struct EntityWeight : IWeighted
    {
        public int          Weight      { get; set; }
        public EntityType   EntityType  { get; set; }
    }
}
