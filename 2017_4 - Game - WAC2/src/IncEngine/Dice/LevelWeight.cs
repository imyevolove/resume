﻿using IncEngine.Collections;

namespace IncEngine.Dice
{
    public struct LevelWeight : IWeighted
    {
        public int      Weight   { get; set; }
        public float    Level    { get; set; }
    }
}
