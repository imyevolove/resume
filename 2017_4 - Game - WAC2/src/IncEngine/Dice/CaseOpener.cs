﻿using IncEngine.Collections;
using IncEngine.Dice.Selectors;
using IncEngine.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace IncEngine.Dice
{
    public class CaseOpener
    {
        public readonly ISkinSelector SkinSelector;
        public readonly ISkinQualitySelector SkinQualitySelector;
        public readonly IExteriorQualitySelector ExteriorQualitySelector;
        public readonly ISkinSpecialSelector SkinSpecialSelector;

        public CaseOpener(
            ISkinSelector               skinSelector, 
            ISkinQualitySelector        skinQualitySelector,
            IExteriorQualitySelector    exteriorQualitySelector,
            ISkinSpecialSelector        skinSpecialSelector
            )
        {
            SkinSelector            = skinSelector;
            SkinQualitySelector     = skinQualitySelector;
            ExteriorQualitySelector = exteriorQualitySelector;
            SkinSpecialSelector     = skinSpecialSelector;
        }

        public Skin GetRandomSkin(Case cs, ISkinSelector selector = null)
        {
            if (selector == null) selector = SkinSelector;

            return selector.Select(cs.Skins);
        }

        public Skin GetRandomSkin(Case cs, Structures.Quality quality, ISkinSelector selector = null)
        {
            if (selector == null) selector = SkinSelector;

            var availableSkins = cs.Skins.Where(skin => skin.Quality == quality);
            
            return selector.Select(availableSkins);
        }

        public List<Skin> GetRandomSkins(Case cs, int count, ISkinSelector selector = null)
        {
            List<Skin> skins = new List<Skin>();

            for (var i = 0; i < count; i++)
            {
                skins.Add(GetRandomSkin(cs, selector));
            }

            return skins;
        }

        public Skin GetRandomSkinWithoutSelector(Case cs)
        {
            return cs.Skins[UnityEngine.Random.Range(0, cs.Skins.Count)];
        }

        public List<Skin> GetRandomSkins(Case cs, int count)
        {
            List<Skin> skins = new List<Skin>();

            for (var i = 0; i < count; i++)
            {
                skins.Add(GetRandomSkinWithoutSelector(cs));
            }

            return skins;
        }

        public InventorySkinItem GenerateInventoryItem(
            Skin skin,
            ISkinSelector skinSelector = null,
            IExteriorQualitySelector exteriorQualitySelector = null,
            ISkinSpecialSelector skinSpecialSelector = null
            )
        {
            if (skinSelector == null) skinSelector = SkinSelector;
            if (exteriorQualitySelector == null) exteriorQualitySelector = ExteriorQualitySelector;
            if (skinSpecialSelector == null) skinSpecialSelector = SkinSpecialSelector;

            ExteriorQuality exterior = exteriorQualitySelector.Select(skin.ExteriorQualities);
            SkinSpecial special = skinSpecialSelector.Select(skin.Special);

            if (!skin.HasSkin(exterior, special)) special = null;

            return new InventorySkinItem(skin, exterior, special);
        }

        public InventorySkinItem GetRandomSkinForInventory(
            Case cs,
            ISkinSelector skinSelector = null,
            IExteriorQualitySelector exteriorQualitySelector = null,
            ISkinSpecialSelector skinSpecialSelector = null
            )
        {
            Skin skin = GetRandomSkin(cs, skinSelector);
            return GenerateInventoryItem(skin, skinSelector, exteriorQualitySelector, skinSpecialSelector);
        }

        public InventorySkinItem GetRandomSkinForInventory(IEnumerable<Skin> skins, ulong priceLimit)
        {
            var availableSkins = skins.Where(sk => sk.MaxPrice <= priceLimit);
            var availableSkinsCount = availableSkins.Count();
            Skin targetSkin;

            if (availableSkinsCount == 0)
            {
                targetSkin = skins.OrderBy(sk => sk.MaxPrice).ElementAt(
                    UnityEngine.Random.Range(0, Mathf.Max(5, availableSkinsCount))
                    );
            }
            else
            {
                targetSkin = availableSkins.ElementAt(UnityEngine.Random.Range(0, availableSkinsCount));
            }

            return GenerateInventoryItem(targetSkin);
        }

        public InventorySkinItem[] GetRandomSkinsForInventory(IEnumerable<Skin> skins, ulong priceLimit, ulong count)
        {
            var result = new List<InventorySkinItem>();

            while (count > 0)
            {
                result.Add(GetRandomSkinForInventory(skins, priceLimit));

                count--;
            }

            return result.ToArray();
        }


        public InventorySkinItem GetRandomSkinForInventory(
            Case cs,
            Quality quality,
            ISkinSelector skinSelector = null,
            IExteriorQualitySelector exteriorQualitySelector = null,
            ISkinSpecialSelector skinSpecialSelector = null
            )
        {
            Skin skin = GetRandomSkin(cs, quality, skinSelector);
            return GenerateInventoryItem(skin, skinSelector, exteriorQualitySelector, skinSpecialSelector);
        }
    }
}
