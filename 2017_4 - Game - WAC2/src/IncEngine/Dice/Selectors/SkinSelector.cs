﻿using System.Collections.Generic;
using System.Linq;
using IncEngine.Structures;
using UnityEngine;
using System;

namespace IncEngine.Dice.Selectors
{
    public class SkinSelector : ISkinSelector
    {
        public readonly LevelWeight [] PriceWeights;
        public readonly EntityWeight[] EntityWeights;

        public SkinSelector()
        {
            PriceWeights = new LevelWeight[] 
            {
                new LevelWeight() { Weight = 5,     Level = 1       },
                new LevelWeight() { Weight = 15,    Level = 0.8f    },
                new LevelWeight() { Weight = 25,    Level = 0.5f    },
                new LevelWeight() { Weight = 55,    Level = 0.2f    },
                new LevelWeight() { Weight = 100,   Level = 0.1f    }
            };

            EntityWeights = new EntityWeight[]
            {
                new EntityWeight { Weight = 4,   EntityType = EntityType.Knive },
                new EntityWeight { Weight = 50,  EntityType = EntityType.Glove },
                new EntityWeight { Weight = 150, EntityType = EntityType.Gun }
            };
        }

        public SkinSelector(LevelWeight[] levelWeights, EntityWeight[] entityWeights)
        {
            PriceWeights    = levelWeights;
            EntityWeights   = entityWeights;
        }

        /// <summary>
        /// Возвращает скни с учетом правил веса
        /// </summary>
        /// <param name="skins"></param>
        /// <returns></returns>
        public Skin Select(IEnumerable<Skin> skins)
        {
            var types = EntityWeights.Where(eq => skins.Any(sk => sk.Type == eq.EntityType));
            var entityType = WeightedRandomization.Choose(types);

            if (skins.Any(sk => sk.Type == entityType.EntityType))
            {
                skins = skins.Where(sk => sk.Type == entityType.EntityType);
            }

            var level = WeightedRandomization.Choose(PriceWeights);
            var minMaxPrice = skins.Min(skin => skin.MaxPrice);
            var maxMaxPrice = skins.Max(skin => skin.MaxPrice);
            var targetPrice = Mathf.Lerp(minMaxPrice, maxMaxPrice, level.Level);
            
            var sortedSkins = skins.OrderBy(skin => skin.MaxPrice);
            var sortedAvailSkins = sortedSkins.Where(skin => skin.MaxPrice <= targetPrice);
            var sortedAvailSkinsCount = Mathf.CeilToInt(sortedSkins.Count() * level.Level);
            var sortedAvailSkinsStart = UnityEngine.Random.Range(0, sortedAvailSkinsCount - 1);
            Skin result = null;

            /*
             Debug.LogFormat("Level: {0}, Target Price: {1}, Min Price: {2}, Max Price: {3}, Level weight: {4}",
                level.Level, targetPrice, minMaxPrice, maxMaxPrice, level.Weight);
             */

            /*
            Debug.LogFormat("Start: {0}, End: {1}, Count: {2}",
                sortedAvailSkinsStart, sortedAvailSkinsCount, sortedSkins.Count());
            */

            if (sortedAvailSkinsCount > 0)
            {
                /// Fix this error
                try
                {
                    result = sortedAvailSkins.ElementAt(UnityEngine.Random.Range(sortedAvailSkinsStart, sortedAvailSkinsCount));
                }
                catch (Exception)
                {
                    result = sortedAvailSkins.FirstOrDefault();
                }
            }
            
            if (result == null)
            {
                Debug.Log("Selection is null. Getting first element");
                result = sortedSkins.ElementAt(0);
            }

            return result;
        }
    }
}
