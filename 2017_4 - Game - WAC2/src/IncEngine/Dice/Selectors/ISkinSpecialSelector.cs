﻿namespace IncEngine.Dice.Selectors
{
    using Structures;

    public interface ISkinSpecialSelector
    {
        SkinSpecial Select(SkinSpecial special);
    }
}