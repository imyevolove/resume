﻿using System.Collections.Generic;

namespace IncEngine.Dice.Selectors
{
    using Structures;

    public interface ISkinSelector
    {
        Skin Select(IEnumerable<Skin> skins);
    }
}