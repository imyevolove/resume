﻿namespace IncEngine.Dice.Selectors
{
    using Structures;
    using System.Collections.Generic;

    public interface IExteriorQualitySelector
    {
        ExteriorQuality Select(IEnumerable<ExteriorQuality> source);
    }
}