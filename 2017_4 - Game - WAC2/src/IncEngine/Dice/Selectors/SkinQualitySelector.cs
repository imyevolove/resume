﻿using System.Collections.Generic;
using IncEngine.Structures;

namespace IncEngine.Dice.Selectors
{
    public class SkinQualitySelector : ISkinQualitySelector
    {
        /// <summary>
        /// Возвращает объект SkinQuality
        /// </summary>
        /// <returns></returns>
        public Quality Select(IEnumerable<Quality> qualities)
        {
            return WeightedRandomization.Choose(qualities);
        }
    }
}
