﻿using System.Collections.Generic;
using IncEngine.Structures;

namespace IncEngine.Dice.Selectors
{
    public class ExteriorQualitySelector : IExteriorQualitySelector
    {
        /// <summary>
        /// Возвращает ExteriorQuality учитывая правила веса
        /// </summary>
        /// <returns></returns>
        public ExteriorQuality Select(IEnumerable<ExteriorQuality> qualities)
        {
            return WeightedRandomization.Choose(qualities);
        }
    }
}
