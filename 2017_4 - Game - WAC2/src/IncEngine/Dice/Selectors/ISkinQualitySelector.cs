﻿namespace IncEngine.Dice.Selectors
{
    using Structures;
    using System.Collections.Generic;

    public interface ISkinQualitySelector
    {
        Quality Select(IEnumerable<Quality> source);
    }
}