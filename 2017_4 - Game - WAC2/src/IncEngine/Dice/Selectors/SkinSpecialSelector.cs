﻿using IncEngine.Structures;

namespace IncEngine.Dice.Selectors
{
    public class SkinSpecialSelector : ISkinSpecialSelector
    {
        public SkinSpecial Select(SkinSpecial special)
        {
            if (special == null) return null;
            return WeightedRandomization.Choose(new SkinSpecial[2] { SkinSpecial.SpecialNone, special });
        }
    }
}
