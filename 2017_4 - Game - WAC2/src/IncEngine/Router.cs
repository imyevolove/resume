﻿using System;
using System.Collections.Generic;

namespace IncEngine
{
    public class Router<TEnum, TObject> where TEnum : struct
    {
        public readonly Dictionary<TEnum, TObject> Routes = new Dictionary<TEnum, TObject>();

        public bool Add(TEnum id, TObject obj)
        {
            if (Routes.ContainsKey(id))
            {
                return false;
            }

            Routes.Add(id, obj);

            return true;
        }

        public bool Remove(TEnum id)
        {
            return Routes.Remove(id);
        }
        
        public TObject Get(TEnum id)
        {
            TObject result;
            Routes.TryGetValue(id, out result);

            return result;
        }

        public bool Contains(TObject obj)
        {
            return Routes.ContainsValue(obj);
        }

        public bool Contains(TEnum id)
        {
            return Routes.ContainsKey(id);
        }
    }
}
