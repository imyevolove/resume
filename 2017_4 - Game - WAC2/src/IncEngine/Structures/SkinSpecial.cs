﻿using System.Text.RegularExpressions;
using IncEngine.Persers;
using IncEngine.Collections;

namespace IncEngine.Structures
{
    /// <summary>
    /// Special Flag used for verification skin specification.
    /// Flag can be stattrak or souvenir. null - flag do not require
    /// </summary>
    public class SkinSpecial : IWeighted
    {
        public readonly string  Name;
        public          int     Weight { get; set; }

        public static readonly SkinSpecial SpecialNone      = new SkinSpecial("Souvenir", 80);
        public static readonly SkinSpecial SpecialStattrak  = new SkinSpecial("Stattrak", 20);
        public static readonly SkinSpecial SpecialSouvenir  = new SkinSpecial("Souvenir", 20);

        private static readonly RegexParserChain<SkinSpecial> m_Parser = new RegexParserChainBuilder<SkinSpecial>()
            .AddChain(new Regex("stattrak", RegexOptions.IgnoreCase), SpecialStattrak)
            .AddChain(new Regex("souvenir", RegexOptions.IgnoreCase), SpecialSouvenir)
            .Build();

        protected SkinSpecial(string name, int weight)
        {
            Name    = name;
            Weight  = weight;
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback default rarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static SkinSpecial Parse(string str)
        {
            return m_Parser.Parse(str);
        }
    }
}
