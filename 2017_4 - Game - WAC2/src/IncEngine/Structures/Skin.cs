﻿using System.Collections.Generic;
using System.Linq;

namespace IncEngine.Structures
{
    public class Skin : AbstractEntityDetails
    {
        public string                   FullName            { get; set; }
        public SkinSpecial              Special             { get; set; }
        public Entity                   Original            { get; set; }
        public Quality                  Quality             { get; set; }
        public Collection               Collection          { get; set; }
        public ulong                    MinPrice            { get; protected set; }
        public ulong                    MaxPrice            { get; protected set; }
        
        public readonly List<SkinDetails> Details = new List<SkinDetails>();

        public ExteriorQuality[] ExteriorQualities { get { return Details.Select(details => details.ExteriorQuality).ToArray(); } }

        public Skin()
        {
        }

        public void RecalculateMinMaxPrices()
        {
            MinPrice = Details.Min(dt => dt.Price);
            MaxPrice = Details.Max(dt => dt.Price);
        }

        public ulong GetBaseDamage(ExteriorQuality exterior, SkinSpecial special)
        {
            var details = GetDetails(exterior, special);
            return details.BaseDamage;
        }

        public ulong GetBaseIncome(ExteriorQuality exterior, SkinSpecial special)
        {
            var details = GetDetails(exterior, special);
            return details.BaseIncome;
        }

        /// <summary>
        /// Return skin exists status
        /// </summary>
        /// <param name="exterior"></param>
        /// <param name="special"></param>
        /// <returns></returns>
        public bool HasSkin(ExteriorQuality exterior, SkinSpecial special)
        {
            return Details.Exists((info) => info.ExteriorQuality == exterior && info.Special == special);
        }

        /// <summary>
        /// Return skin exists status
        /// </summary>
        /// <param name="exterior"></param>
        /// <returns></returns>
        public bool HasSkin(ExteriorQuality exterior)
        {
            return Details.Exists((info) => info.ExteriorQuality == exterior);
        }

        /// <summary>
        /// Return skin price
        /// </summary>
        /// <param name="exterior"></param>
        /// <param name="special"></param>
        /// <returns></returns>
        public ulong GetPrice(ExteriorQuality exterior, SkinSpecial special)
        {
            var details = Details.Find((info) => info.ExteriorQuality == exterior && info.Special == special);
            return details.Price;
        }

        public SkinDetails GetDetails(ExteriorQuality exterior, SkinSpecial special)
        {
            var details = Details.Find((info) => info.ExteriorQuality == exterior && info.Special == special);
            return details;
        }
    }
}