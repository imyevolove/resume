﻿using System.Text.RegularExpressions;
using UnityEngine;
using IncEngine.Persers;

namespace IncEngine.Structures
{
    /// <summary>
    /// Rarity for stickers
    /// </summary>
    public class StickerQuality : IQuality
    {
        public string   Name    { get; protected set; }
        public Color    Color   { get; protected set; }

        public static readonly StickerQuality QualityHighGrade   = new StickerQuality("High grade", Colors.CreateColor(75, 105, 205));
        public static readonly StickerQuality QualityRemarkable  = new StickerQuality("Remarkable", Colors.CreateColor(136, 71, 255));
        public static readonly StickerQuality QualityExotic      = new StickerQuality("Exotic",     Colors.CreateColor(211, 44, 230));
        public static readonly StickerQuality QualityContraband  = new StickerQuality("Contraband", Colors.CreateColor(136, 106, 8));

        public static StickerQuality DefaultQuality { get { return QualityHighGrade; } }

        private static readonly RegexParserChain<StickerQuality> m_QualityParser = new RegexParserChainBuilder<StickerQuality>()
            .AddChain(new Regex("(?=.*high)(?=.*grade)",    RegexOptions.IgnoreCase),   QualityHighGrade)
            .AddChain(new Regex("remarkable",               RegexOptions.IgnoreCase),   QualityRemarkable)
            .AddChain(new Regex("exotic",                   RegexOptions.IgnoreCase),   QualityExotic)
            .AddChain(new Regex("contraband",               RegexOptions.IgnoreCase),   QualityContraband)
            .Build();

        protected StickerQuality(string name, Color color)
        {
            Name  = name;
            Color = color;
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback default rarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static StickerQuality Parse(string str, bool fallback = false)
        {
            var rarity = m_QualityParser.Parse(str);

            if (rarity == null && fallback) return DefaultQuality;
            return rarity;
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback StickerRarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static StickerQuality Parse(string str, StickerQuality fallback)
        {
            var rarity = m_QualityParser.Parse(str);
            return rarity == null ? fallback : rarity;
        }
    }
}
