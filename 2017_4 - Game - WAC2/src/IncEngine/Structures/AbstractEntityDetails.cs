﻿using UnityEngine;

namespace IncEngine.Structures
{
    public enum EntityType
    {
        Gun     = 1 << 0,
        Knive   = 1 << 1,
        Glove   = 1 << 2,
    }

    public abstract class AbstractEntityDetails
    {
        public string           Id          { get; set; }
        public string           Name        { get; set; }
        public Sprite           Icon        { get; set; }
        public virtual ulong    Price       { get; set; }
        public EntityType       Type        { get; set; }
    }
}
