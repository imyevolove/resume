﻿namespace IncEngine.Structures
{
    public struct SkinDetails
    {
        public ExteriorQuality ExteriorQuality;
        public SkinSpecial Special { get; set; }
        public ulong Price { get; set; }

        public ulong BaseIncome { get; set; }
        public ulong BaseDamage { get; set; }
    }
}
