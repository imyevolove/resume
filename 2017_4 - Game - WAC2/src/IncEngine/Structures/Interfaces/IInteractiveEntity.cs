﻿namespace IncEngine.Structures.Interfaces
{
    public interface IInteractiveEntity
    {
        ulong BaseDamage { get; }
        ulong BaseIncome { get; }
    }
}
