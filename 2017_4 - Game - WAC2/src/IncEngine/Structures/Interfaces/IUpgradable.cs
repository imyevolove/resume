﻿using System;

namespace IncEngine.Structures
{
    public interface IUpgradable
    {
        event Action Event_Upgrade;

        uint UpgradeLevel       { get; }
        uint UpgradeMaxLevel    { get; }

        bool CanUpgrade();
        bool CanUpgrade(uint level);

        bool Upgrade();
        bool Upgrade(uint level);

        ulong GetUpgradePrice();
    }
}
