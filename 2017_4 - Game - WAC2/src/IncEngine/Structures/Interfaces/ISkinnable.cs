﻿using System;

namespace IncEngine.Structures
{
    public interface ISkinnable
    {
        event Action Event_SkinChange;

        Skin Skin { get; }
    }
}
