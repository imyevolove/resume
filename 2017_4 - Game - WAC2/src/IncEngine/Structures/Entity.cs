﻿using IncEngine.Structures.Interfaces;

namespace IncEngine.Structures
{
    public class Entity : AbstractEntityDetails, IInteractiveEntity
    {
        public ulong BaseDamage { get; protected set; }
        public ulong BaseIncome { get; protected set; }

        public Entity(ulong baseDamage, ulong baseIncome)
        {
            BaseDamage = baseDamage;
            BaseIncome = baseIncome;
        }
    }
}
