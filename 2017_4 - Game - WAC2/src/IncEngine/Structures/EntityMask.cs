﻿using System.Collections.Generic;

namespace IncEngine.Structures
{
    public struct EntityMask
    {
        public readonly int Value;

        public EntityMask(int value)
        {
            Value = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool HasValue(int value)
        {
            return (Value & value) > 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool HasValue(EntityType type)
        {
            return HasValue((int)type);
        }

        /// <summary>
        /// Create EntityMask by parameters
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static EntityMask Create(params EntityType[] values)
        {
            var mask = 0;

            if (values != null)
            {
                foreach (var value in values)
                {
                    mask |= (int)value;
                }
            }

            return new EntityMask(mask);
        }

        /// <summary>
        /// Create EntityMask by parameters
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static EntityMask Create(IEnumerable<EntityType> values)
        {
            var mask = 0;

            if (values != null)
            {
                foreach (var value in values)
                {
                    mask |= (int)value;
                }
            }

            return new EntityMask(mask);
        }
    }
}
