﻿using UnityEngine;

namespace IncEngine.Structures
{
    public interface IQuality
    {
        string  Name    { get; }
        Color   Color   { get; }
    }
}