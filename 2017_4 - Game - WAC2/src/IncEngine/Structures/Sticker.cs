﻿using UnityEngine;

namespace IncEngine.Structures
{
    public class Sticker : AbstractEntityDetails
    {
        public Color            Color   { get { return Quality.Color; } }
        public StickerQuality   Quality { get; set; }
    }
}
