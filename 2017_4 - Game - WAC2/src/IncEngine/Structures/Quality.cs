﻿using System.Text.RegularExpressions;
using UnityEngine;
using IncEngine.Persers;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using IncEngine.Collections;

namespace IncEngine.Structures
{
    public class Quality : IWeighted, IQuality
    {
        public string   Name    { get; protected set; }
        public Color    Color   { get; protected set; }
        public int      Weight  { get; set; }

        public static Quality QualityConsumer           { get { return Qualities[8]; } }
        public static Quality QualityIndustrial         { get { return Qualities[7]; } }
        public static Quality QualityMilspec            { get { return Qualities[6]; } }
        public static Quality QualityRestricted         { get { return Qualities[5]; } }
        public static Quality QualityClassified         { get { return Qualities[4]; } }
        public static Quality QualityCovert             { get { return Qualities[3]; } }
        public static Quality QualityExceedinglyRare    { get { return Qualities[2]; } }
        public static Quality QualityContraband         { get { return Qualities[1]; } }
        public static Quality QualityExtraordinary      { get { return Qualities[0]; } }

        public static Quality DefaultQuality            { get { return QualityConsumer; } }

        public static List<Quality> Qualities = new List<Quality>()
        {
            new Quality("Extraordinary",    1,      Colors.CreateColor(224, 196, 47)),
            new Quality("Contraband",       2,      Colors.CreateColor(165, 131, 21)),
            new Quality("Exceedingly Rare", 5,     Colors.CreateColor(224, 196, 47)),
            new Quality("Covert",           50,     Colors.CreateColor(239, 96, 96)),
            new Quality("Classified",       200,     Colors.CreateColor(206, 81, 220)),
            new Quality("Restricted",       400,     Colors.CreateColor(147, 93, 245)),
            new Quality("Mil-spec",         1000,     Colors.CreateColor(108, 139, 245)),
            new Quality("Industrial",       1800,    Colors.CreateColor(141, 186, 253)),
            new Quality("Consumer",         2500,    Colors.CreateColor(197, 197, 197))
        };

        private static readonly RegexParserChain<Quality> m_QualityParser = new RegexParserChainBuilder<Quality>()
            .AddChain(new Regex("consumer",             RegexOptions.IgnoreCase), QualityConsumer)
            .AddChain(new Regex("industrial",           RegexOptions.IgnoreCase), QualityIndustrial)
            .AddChain(new Regex("(?=.*mil)(?=.*spec)",  RegexOptions.IgnoreCase), QualityMilspec)
            .AddChain(new Regex("restricted",           RegexOptions.IgnoreCase), QualityRestricted)
            .AddChain(new Regex("classified",           RegexOptions.IgnoreCase), QualityClassified)
            .AddChain(new Regex("covert",               RegexOptions.IgnoreCase), QualityCovert)
            .AddChain(new Regex("rare|gold",            RegexOptions.IgnoreCase), QualityExceedinglyRare)
            .AddChain(new Regex("contraband",           RegexOptions.IgnoreCase), QualityContraband)
            .AddChain(new Regex("extraordinary",        RegexOptions.IgnoreCase), QualityExtraordinary)
            .Build();

        protected Quality(string name, int weight, Color color)
        {
            Name    = name;
            Color   = color;
            Weight  = weight;
        }

        /// <summary>
        /// Return quality index 
        /// </summary>
        /// <returns></returns>
        public static int GetIndex(Quality quality)
        {
            return Qualities.IndexOf(quality);
        }

        /// <summary>
        /// Return next SkinQuality
        /// </summary>
        /// <returns></returns>
        public static Quality Next(Quality quality)
        {
            var index = GetIndex(quality) - 1;

            if (index < 0) return DefaultQuality;
            return Qualities[index];
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback default rarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static Quality Parse(string str, bool fallback = false)
        {
            var rarity = m_QualityParser.Parse(str);

            if (rarity == null)
            {
                Debug.LogFormat("Error parsing skin quality: {0}", str);
            }

            if (rarity == null && fallback)
            {
                return DefaultQuality;
            }

            return rarity;
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback StickerRarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static Quality Parse(string str, Quality fallback)
        {
            var rarity = m_QualityParser.Parse(str);

            if (rarity == null)
            {
                Debug.LogFormat("Error parsing skin quality: {0}", str);
            }

            return rarity == null ? fallback : rarity;
        }
    }
}
