﻿using UnityEngine;

namespace IncEngine.Structures
{
    public class Map
    {
        public string Name  { get; set; }
        public Sprite Icon  { get; set; }
        public Sprite Image { get; set; }
    }
}
