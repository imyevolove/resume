﻿using IncEngine.Abilities;
using System.Collections.Generic;
using Wac;

namespace IncEngine.Structures
{
    public class Case : AbstractEntityDetails
    {
        public Collection   Collection  { get; set; }
        public List<Skin>   Skins       { get; set; }
        
        public override ulong Price
        {
            get
            {
                return Game.Player.Abilities.GetAbility(AbilityType.ReduceCasesPrice).ApplyToValue(base.Price);
            }

            set
            {
                base.Price = value;
            }
        }

        public bool HasSkin(Skin skin)
        {
            return Skins.Contains(skin);
        }

        public bool HasSkin(Quality quality)
        {
            return Skins.Exists(sk => sk.Quality == quality);
        }
    }
}
