﻿using System.Text.RegularExpressions;
using IncEngine.Persers;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using IncEngine.Collections;
using IncEngine.Localization;

namespace IncEngine.Structures
{
    public class ExteriorQuality : IWeighted
    {
        public readonly string  Name;
        public          int     Weight { get; set; }

        public static ExteriorQuality QualityBattleScarred  { get { return Qualities[4]; } }
        public static ExteriorQuality QualityWellWorn       { get { return Qualities[3]; } }
        public static ExteriorQuality QualityFieldTested    { get { return Qualities[2]; } }
        public static ExteriorQuality QualityMinimalWear    { get { return Qualities[1]; } }
        public static ExteriorQuality QualityFactoryNew     { get { return Qualities[0]; } }

        public static ExteriorQuality DefaultQuality        { get { return QualityBattleScarred; } }

        public static List<ExteriorQuality> Qualities = new List<ExteriorQuality>()
        {
            new ExteriorQuality("Factory New",      10),
            new ExteriorQuality("Minimal Wear",     30),
            new ExteriorQuality("Field Tested",     60),
            new ExteriorQuality("Well Worn",        80),
            new ExteriorQuality("Battle Scarred",   100)
        };

        private static readonly RegexParserChain<ExteriorQuality> m_QualityParser = new RegexParserChainBuilder<ExteriorQuality>()
            .AddChain(new Regex("(?=.*battle)(?=.*scarred)",    RegexOptions.IgnoreCase), QualityBattleScarred)
            .AddChain(new Regex("(?=.*well)(?=.*worn)",         RegexOptions.IgnoreCase), QualityWellWorn)
            .AddChain(new Regex("(?=.*field)(?=.*tested)",      RegexOptions.IgnoreCase), QualityFieldTested)
            .AddChain(new Regex("(?=.*minimal)(?=.*wear)",      RegexOptions.IgnoreCase), QualityMinimalWear)
            .AddChain(new Regex("(?=.*factory)(?=.*new)",       RegexOptions.IgnoreCase), QualityFactoryNew)
            .Build();

        protected ExteriorQuality(string name, int weight)
        {
            Name    = name;
            Weight  = weight;
        }

        public static string GetLocalizationForExterior(ExteriorQuality exterior)
        {
            return GetLocalizationForExterior(exterior.Name);
        }

        public static string GetLocalizationForExterior(string exteriorName)
        {
            return LocalizationManager.GetLocalization("exterior_quality_" + exteriorName.Replace(" ", "_").ToLower());
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback default rarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static ExteriorQuality Parse(string str, bool fallback = false)
        {
            var rarity = m_QualityParser.Parse(str);

            if (rarity == null && fallback) return DefaultQuality;
            return rarity;
        }

        /// <summary>
        /// Parse string and search represent StickerRarity.
        /// If string is invalid then return fallback StickerRarity
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static ExteriorQuality Parse(string str, ExteriorQuality fallback)
        {
            var rarity = m_QualityParser.Parse(str);
            return rarity == null ? fallback : rarity;
        }
    }
}
