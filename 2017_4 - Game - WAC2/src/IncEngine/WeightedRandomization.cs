﻿using IncEngine.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IncEngine
{
    public static class WeightedRandomization
    {
        public static T Choose<T>(IEnumerable<T> list) where T : IWeighted
        {
            if (list.Count() == 0)
            {
                return default(T);
            }
            
            int totalweight = list.Sum(c => c.Weight);
            int choice = (int)(UnityEngine.Random.value * totalweight);
            int sum = 0;

            foreach (var obj in list)
            {
                for (int i = sum; i < obj.Weight + sum; i++)
                {
                    if (i >= choice)
                    {
                        return obj;
                    }
                }
                sum += obj.Weight;
            }

            return list.First();
        }
    }
}
