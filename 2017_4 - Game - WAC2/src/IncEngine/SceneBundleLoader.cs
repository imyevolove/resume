﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace IncEngine
{
    public class SceneBundleLoader : IDisposable
    {
        private List<SceneLoaderInfo> m_Scenes = new List<SceneLoaderInfo>();
        private SceneLoaderInfo m_CurrentLoadScene;
        private int m_SceneIndex = 0;

        private Dictionary<SceneLoaderInfo, Action> m_Events = new Dictionary<SceneLoaderInfo, Action>();
        private Action m_EndCallback;

        public LoadingState State { get; private set; }

        public enum LoadingState
        {
            Idle,
            Exec,
            Ended
        }

        public class SceneLoaderInfo
        {
            public string           Name    { get; protected set; }
            public int              Id      { get; protected set; }
            public LoadSceneMode    Mode    { get; protected set; }

            public SceneLoaderInfo(string name, LoadSceneMode mode)
            {
                Name    = name;
                Mode    = mode;
                Id      = -1;
            }

            public SceneLoaderInfo(int id, LoadSceneMode mode)
            {
                Id      = id;
                Mode    = mode;
            }

            public SceneLoaderInfo(string name, int id, LoadSceneMode mode)
            {
                Id      = id;
                Name    = name;
                Mode    = mode;
            }

            public bool Compare(string name, LoadSceneMode mode)
            {
                return Name == name && Mode == mode;
            }

            public bool Compare(int id, LoadSceneMode mode)
            {
                return Id == id && Mode == mode;
            }

            public bool Compare(string name, int id, LoadSceneMode mode)
            {
                return Id == id && Name == name && Mode == mode;
            }

            public bool Compare(Scene scene, LoadSceneMode mode)
            {
                return (scene.buildIndex == Id || scene.name == Name) && Mode == mode;
            }
        }

        public SceneBundleLoader()
        {
            SceneManager.sceneLoaded += OnSceneManagerSceneLoaded;
        }

        public void Dispose()
        {
            SceneManager.sceneLoaded -= OnSceneManagerSceneLoaded;
            m_EndCallback = null;
            m_Events.Clear();
        }

        public void AddScene(string name, LoadSceneMode mode)
        {
            AddScene(new SceneLoaderInfo(name, mode));
        }

        public void AddScene(string name, LoadSceneMode mode, Action callback)
        {
            AddScene(new SceneLoaderInfo(name, mode), callback);
        }

        public void AddScene(int index, LoadSceneMode mode)
        {
            AddScene(new SceneLoaderInfo(index, mode));
        }

        public void AddScene(int index, LoadSceneMode mode, Action callback)
        {
            AddScene(new SceneLoaderInfo(index, mode), callback);
        }

        public void Load()
        {
            if (State != LoadingState.Idle) return;

            State = LoadingState.Exec;
            LoadNext();
        }

        public void Load(Action callback)
        {
            m_EndCallback = callback;
            Load();
        }

        protected virtual void OnLoadEnd()
        {
            if (m_EndCallback != null)
            {
                m_EndCallback();
            }

            Dispose();
        }

        protected virtual void OnLoadScene(Scene scene)
        {

        }

        private void AddScene(SceneLoaderInfo info)
        {
            m_Scenes.Add(info);
        }

        private void AddScene(SceneLoaderInfo info, Action callback)
        {
            AddScene(info);

            if (m_Events.ContainsKey(info))
            {
                m_Events.Remove(info);
            }

            m_Events.Add(info, callback);
        }

        private void OnSceneManagerSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            m_SceneIndex++;

            if (m_CurrentLoadScene == null) return;
            if (m_CurrentLoadScene.Compare(scene, mode))
            {
                CallCallbackForScene(m_CurrentLoadScene);
                LoadNext();
            }
        }

        private void CallCallbackForScene(SceneLoaderInfo info)
        {
            if (!m_Events.ContainsKey(info)) return;
            m_Events[info].Invoke();
        }

        private void LoadNext()
        {
            if (State != LoadingState.Exec) return;

            if (m_SceneIndex >= m_Scenes.Count)
            {
                State = LoadingState.Ended;
                OnLoadEnd();
                return;
            }

            m_CurrentLoadScene = m_Scenes[m_SceneIndex];
            Debug.LogFormat("Loading next scene Index: '{0}'", m_SceneIndex);

            if (m_CurrentLoadScene.Id > 0)
            {
                SceneManager.LoadSceneAsync(m_CurrentLoadScene.Id, m_CurrentLoadScene.Mode);
            }
            else
            {
                SceneManager.LoadSceneAsync(m_CurrentLoadScene.Name, m_CurrentLoadScene.Mode);
            }
        }
    }
}
