﻿using IncEngine.Localization;
using UnityEngine.UI;
using Wac;

namespace IncEngine.UI
{
    public class BasicRankDrawer : AbstractRankDrawer
    {
        public Text     experienceText;
        public Slider   progressSlider;

        /// CAN BE OPTIMIZED
        protected override void OnProgressChange()
        {
            progressSlider.value = RankSystem.Progress01;
            experienceText.text = string.Format(
                "{0}\n{1} <color=#59c5d2>XP</color>", 
                LocalizationManager.GetLocalization("rank" + RankSystem.CurrentRankIndex).ToUpper(),
                Game.NumberFormatterSpace.Format(RankSystem.ExperienceValue));
        }
    }
}
