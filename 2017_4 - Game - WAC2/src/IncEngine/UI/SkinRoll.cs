﻿using IncEngine.Structures;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable]
    public class SkinRoll : Drawer<Skin>, IPoolable
    {
        public event Action<IPoolable, bool> Event_ActiveChange;
        public event Action<IPoolable> Event_Activate;
        public event Action<IPoolable> Event_Deactivate;

        [SerializeField] public Image qualityImage;
        [SerializeField] public Image iconImage;
        [SerializeField] public Image gradientImage;

        public bool Active { get { return gameObject.activeSelf; } }

        private RectTransform m_Transform;
        public RectTransform Transform
        {
            get
            {
                if (m_Transform == null)
                {
                    m_Transform = GetComponent<RectTransform>();
                }

                return m_Transform;
            }
        }

        public void Activate()
        {
            gameObject.SetActive(true);

            if (Event_Activate != null)
            {
                Event_Activate(this);
            }

            OnActiveChange(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);

            if (Event_Deactivate != null)
            {
                Event_Deactivate(this);
            }

            OnActiveChange(false);
        }

        protected override void OnRegisterTarget(Skin target)
        {
            qualityImage.color  = target.Quality.Color;
            iconImage.sprite    = target.Icon;
        }

        protected override void OnUnregisterTarget(Skin target)
        {

        }

        protected virtual void OnActiveChange(bool active)
        {
            if (Event_ActiveChange != null)
            {
                Event_ActiveChange(this, active);
            }
        }
    }
}
