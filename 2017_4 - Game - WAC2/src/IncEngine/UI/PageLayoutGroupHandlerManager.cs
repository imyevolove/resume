﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable]
    public class PageLayoutGroupHandlerManager : MonoBehaviour
    {
        [SerializeField] public PageLayoutGroup Page;
        [SerializeField] public List<HandlerOptions> Handlers;

        [Serializable]
        public class HandlerOptions
        {
            [SerializeField] public PageLayoutGroupHandler handler;
            [SerializeField] public bool limited;
            [SerializeField] public uint limit;
        }

        protected virtual void Start()
        {
            if (Handlers == null)
            {
                Handlers = new List<HandlerOptions>();
            }

            foreach (var handler in Handlers)
            {
                handler.handler.Page = Page;
            }

            Page.Event_PagesCountChange += OnPagesCountChange;

            Redraw();
        }

        protected void OnPagesCountChange(uint count)
        {
            Redraw();
        }

        protected virtual void Redraw()
        {
            var deactivateAll = false;

            foreach (var handler in Handlers)
            {
                if (deactivateAll)
                {
                    handler.handler.enabled = false;
                    continue;
                }

                if (!handler.limited || handler.limited && handler.limit <= Page.PagesCount)
                {
                    handler.handler.enabled = true;
                    deactivateAll = true;
                }

                handler.handler.enabled = false;
                continue;
            }
        }
    }
}
