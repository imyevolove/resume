﻿using System;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable]
    public abstract class PageLayoutGroupHandler : MonoBehaviour
    {
        [SerializeField] private PageLayoutGroup m_Page;
        public PageLayoutGroup Page
        {
            get { return m_Page; }
            set
            {
                UnregisterPageLayoutGroupEvents(m_Page);
                RegisterPageLayoutGroupEvents(value);

                m_Page = value;
                
                OnPageLayoutGroupChange();
            }
        }

        [SerializeField] public RectTransform content;

        public bool IsEnabled { get; private set; }

        protected virtual void Awake()
        {
            
        }

        protected virtual void Start()
        {
            
        }
        
        protected abstract void RedrawCore();
        protected abstract void OnPageChange(uint index);
        protected abstract void OnPagesCountChange(uint index);

        protected virtual void RegisterPageLayoutGroupEvents(PageLayoutGroup page)
        {
            if (page == null) return;

            page.Event_CurrentPageChange += OnPageChange;
            page.Event_PagesCountChange += OnPagesCountChange;
        }

        protected virtual void UnregisterPageLayoutGroupEvents(PageLayoutGroup page)
        {
            if (page == null) return;

            page.Event_CurrentPageChange -= OnPageChange;
            page.Event_PagesCountChange -= OnPagesCountChange;
        }

        protected virtual void OnPageLayoutGroupChange()
        {
            Redraw();
        }

        private void Redraw()
        {
            if (!isActiveAndEnabled) return;
            RedrawCore();
        }
    }
}
