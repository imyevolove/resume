﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Wac;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public class ResourceValueDrawer : Drawer<StoreValue>
    {
        [SerializeField]
        public Text vlaueText;
        
        protected void SV_EventListenerValueChange(ulong value)
        {
            OnValueChange(value);
        }

        protected virtual void OnValueChange(ulong value)
        {
            vlaueText.text = Game.NumberFormatter.Format(value);
        }

        protected override void OnRegisterTarget(StoreValue target)
        {
            OnValueChange(target.Value);
            target.Event_Change += SV_EventListenerValueChange;
        }

        protected override void OnUnregisterTarget(StoreValue target)
        {
            target.Event_Change -= SV_EventListenerValueChange;
        }
    }
}
