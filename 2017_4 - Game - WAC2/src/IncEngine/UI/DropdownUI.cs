﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class DropdownUI : UIMonoBehaviour, IPointerClickHandler
    {
        [SerializeField] public Text SelectionText;
        [SerializeField] public ToggleButton ToggleButtonPrefab;
        [SerializeField] public RectTransform Content;

        [HideInInspector] private Animator m_Animator;
        public Animator Animator { get { return m_Animator; } }

        [HideInInspector] private List<OptionData> m_Options = new List<OptionData>();

        public SelectionEvent onSelectionChange = new SelectionEvent();

        private OptionData m_Selection;
        public OptionData Selection
        {
            get { return m_Selection; }
            private set
            {
                m_Selection = value;
                OnSelectionChange(value);
            }
        }

        public const string AnimatorStateKeyword = "active";

        public bool DropdownStatus
        {
            get { return Animator.GetBool(AnimatorStateKeyword); }
            set
            {
                Animator.SetBool(AnimatorStateKeyword, value);
            }
        }

        private MonoEntityPool<ToggleButton> m_TogglePool;
        private List<ToggleButton> m_RegisteredToggle = new List<ToggleButton>();

        public class OptionData
        {
            public string Name;
            public object Target;
        }

        public class SelectionEvent : UnityEvent<OptionData> { }

        protected virtual void Awake()
        {
            m_TogglePool = new MonoEntityPool<ToggleButton>(ToggleButtonPrefab);
            m_Animator = GetComponent<Animator>();
            Rebuild();
        }

        public void SetSelection(string name)
        {
            var option = m_Options.Find(opt => string.Equals(opt.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (option != null)
            {
                Selection = option;
            }
        }

        public void SetSelection(object target)
        {
            var option = m_Options.Find(opt => opt.Target.Equals(target));

            if (option != null)
            {
                Selection = option;
            }
        }

        public void AddOptions(IEnumerable<string> options)
        {
            foreach (var name in options)
            {
                m_Options.Add(new OptionData()
                {
                    Name = name
                });
            }

            Rebuild();
        }

        public void AddOptions(IEnumerable<OptionData> options)
        {
            foreach (var option in options)
            {
                m_Options.Add(option);
            }

            Rebuild();
        }

        public void RemoveOptions()
        {
            m_Options.Clear();
            m_RegisteredToggle.Clear();
            Selection = null;

            Rebuild();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            DropdownStatus = !DropdownStatus;
            RedrawContent();
        }

        private void Rebuild()
        {
            m_TogglePool.DeactivateAllActiveEntities();
            
            foreach (var option in m_Options)
            {
                var toggle = CreateToggleButton();
                toggle.SetText(option.Name);
            }

            Selection = m_Options.FirstOrDefault();
            RedrawContent();
        }

        private ToggleButton CreateToggleButton()
        {
            var toggle = m_TogglePool.GetFree(Content);
            toggle.Toggle = false;

            m_RegisteredToggle.Add(toggle);

            toggle.onClick.RemoveListener(OnToggleClick);
            toggle.onClick.AddListener(OnToggleClick);

            return toggle;
        }

        private void ActivateToggle()
        {
            var index = m_Options.FindIndex(option => option == Selection);
            var toggle = index < 0 || index >= m_RegisteredToggle.Count ? null : m_RegisteredToggle[index];

            ActivateToggle(toggle);
        }

        private void ActivateToggle(ToggleButton target)
        {
            foreach (var toggle in m_RegisteredToggle)
            {
                toggle.Toggle = target == toggle;
            }
        }

        private void OnToggleClick(ToggleButton toggle)
        {
            var index = m_RegisteredToggle.FindIndex(tog => tog == toggle);

            if (index < 0 || index >= m_Options.Count) return;

            Selection = m_Options[index];
        }

        protected virtual void OnSelectionChange(OptionData option)
        {
            if (option == null) return;

            onSelectionChange.Invoke(option);
            SelectionText.text = option.Name;

            ActivateToggle();
        }

        protected void RedrawContent()
        {
            Content.gameObject.SetActive(DropdownStatus);
        }
    }
}
