﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public class TabView : MonoBehaviour
    {
        [SerializeField]
        private List<TabRoute> m_Routes = new List<TabRoute>();
        public List<TabRoute> Routes { get { return m_Routes; } }
        
        void Start()
        {
            if (m_Routes == null) return;

            foreach (var route in m_Routes)
                RegisterRoute(route);

            Debug();
        }

        public void AddRoute(TabRoute route)
        {
            RegisterRoute(route);
            Debug();
        }

        public void AddRouteRange(IEnumerable<TabRoute> routes)
        {
            foreach(var route in routes)
                RegisterRoute(route);
            Debug();
        }

        protected void RegisterRoute(TabRoute route)
        {
            route.Button.Event_Click += CreateRouteAction(route);

            if (!Routes.Contains(route)) Routes.Add(route);
        }

        protected void Debug()
        {
            if (m_Routes.Count > 0) m_Routes[0].Button.Click();
        }

        protected Action CreateRouteAction(TabRoute route)
        {
            return () =>
            {
                foreach (var _route in m_Routes)
                {
                    if (_route == route)
                    {
                        _route.Content.Show();
                        _route.Button.Activate();
                    }
                    else
                    {
                        _route.Content.Hide();
                        _route.Button.Deactivate();
                    }
                }
            };
        }
    }
}
