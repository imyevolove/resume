﻿using IncEngine.Localization;
using UnityEngine.UI;
using Wac;

namespace IncEngine.UI
{
    public class DetailedRankDrawer : AbstractRankDrawer
    {
        public Text     experienceText;
        public Text     rankDetailsText;
        public Slider   progressSlider;

        /// CAN BE OPTIMIZED
        protected override void OnProgressChange()
        {
            progressSlider.value = RankSystem.Progress01;

            rankDetailsText.text = string.Format(
                "{0}\n{1} <color=#59c5d2>{3}</color> / {2} <color=#59c5d2>{3}</color>", 
                GetLocalization("rank" + RankSystem.CurrentRankIndex),
                Game.NumberFormatterSpace.Format(RankSystem.ProgressValue),
                Game.NumberFormatterSpace.Format(RankSystem.ProgressStage),
                GetLocalization("xp"));

            experienceText.text = string.Format(
                "{0}\n{1} <color=#59c5d2>{2}</color>",
                GetLocalization("experience"),
                Game.NumberFormatterSpace.Format(RankSystem.ExperienceValue),
                GetLocalization("xp"));
        }

        private string GetLocalization(string key)
        {
            return LocalizationManager.GetLocalization(key).ToUpper();
        }
    }
}
