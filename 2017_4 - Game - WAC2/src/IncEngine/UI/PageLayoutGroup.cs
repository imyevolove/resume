﻿using System;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable]
    public abstract class PageLayoutGroup : MonoBehaviour
    {

        public event Action<uint> Event_PagesCountChange;
        public event Action<uint> Event_CurrentPageChange;
        public event Action<uint> Event_CurrentPageNext;
        public event Action<uint> Event_CurrentPageBack;

        public uint PagesCount          { get; private set; }
        public uint CurrentPageIndex    { get; private set; }

        public bool IsFirstPage         { get { return CurrentPageIndex == 0; } }
        public bool IsLastPage          { get { return (CurrentPageIndex + 1) >= PagesCount; } }

        protected virtual void OnValidate()
        {
            OnPagesCountChange(PagesCount);
        }

        protected virtual void Awake()
        {
            OnPagesCountChange(PagesCount);
        }
        
        public void SetPagesCount(uint count)
        {
            PagesCount = count;
            OnPagesCountChange(PagesCount);
        }

        public bool Next()
        {
            if (CurrentPageIndex >= PagesCount - 1) return false;

            CurrentPageIndex++;
            OnCurrentPageNext(CurrentPageIndex);
            OnCurrentPageChange(CurrentPageIndex);

            return true;
        }

        public bool Back()
        {
            if (CurrentPageIndex <= 0) return false;

            CurrentPageIndex--;
            OnCurrentPageBack(CurrentPageIndex);
            OnCurrentPageChange(CurrentPageIndex);

            return true;
        }

        public bool Select(uint index)
        {
            var maxIndex = PagesCount - 1;
            if (maxIndex < 0) maxIndex = 0;

            index = (uint)Mathf.Clamp(index, 0, maxIndex);

            CurrentPageIndex = index;
            OnCurrentPageChange(CurrentPageIndex);

            return true;
        }

        protected void CallPageEvent(Action<uint> ev)
        {
            if (ev != null)
            {
                ev(PagesCount);
            }
        }

        protected virtual void OnPagesCountChange(uint count)
        {
            if (CurrentPageIndex > count)
            {
                Select((uint)Mathf.Clamp(count - 1, 0, count));
            }

            CallPageEvent(Event_PagesCountChange);
        }

        protected virtual void OnCurrentPageChange(uint index)
        {
            CallPageEvent(Event_CurrentPageChange);
        }

        protected virtual void OnCurrentPageNext(uint index)
        {
            CallPageEvent(Event_CurrentPageNext);
        }

        protected virtual void OnCurrentPageBack(uint index)
        {
            CallPageEvent(Event_CurrentPageBack);
        }
    }
}
