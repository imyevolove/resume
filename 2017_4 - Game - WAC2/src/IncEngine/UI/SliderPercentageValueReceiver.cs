﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable]
    public class SliderPercentageValueReceiver : UIMonoBehaviour
    {
        [SerializeField] public Text ValueText;
        [SerializeField] private Slider m_Slider;

        protected virtual void Start()
        {
            SetSlider(m_Slider);
            Redraw();
        }

        public void SetSlider(Slider slider)
        {
            UnregisterEventListeners(m_Slider);
            RegisterEventListeners(slider);

            m_Slider = slider;
        }

        public void Redraw()
        {
            if (m_Slider == null) return;
            
            ValueText.text = m_Slider.value.ToString();
        }

        protected virtual void OnSliderValueChange(float value)
        {
            Redraw();
        }

        private void RegisterEventListeners(Slider slider)
        {
            if (slider == null) return;

            slider.onValueChanged.AddListener(OnSliderValueChange);
        }

        private void UnregisterEventListeners(Slider slider)
        {
            if (slider == null) return;

            slider.onValueChanged.RemoveListener(OnSliderValueChange);
        }
    }
}
