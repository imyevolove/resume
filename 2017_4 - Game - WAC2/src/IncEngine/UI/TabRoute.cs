﻿using System;
using UnityEngine;
using IncEngine.Activities;

namespace IncEngine.UI
{
    [Serializable]
    public class TabRoute
    {
        [SerializeField]
        public TabButton Button;

        [SerializeField]
        public Activity Content;
    }
}