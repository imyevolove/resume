﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable]
    public class PageLayoutGroupLayersHandler : PageLayoutGroupHandler
    {
        [SerializeField] public Text pagesInfoText;
        [SerializeField] public Button buttonPrevious;
        [SerializeField] public Button buttonNext;

        protected override void Awake()
        {
            base.Awake();

            buttonPrevious.onClick.AddListener(PrevPage);
            buttonNext.onClick.AddListener(NextPage);
        }

        public void OnEnable()
        {
            UnregisterPageLayoutGroupEvents(Page);
            RegisterPageLayoutGroupEvents(Page);
        }

        public void OnDisable()
        {
            UnregisterPageLayoutGroupEvents(Page);
        }

        protected override void OnPageChange(uint index)
        {
            RedrawCore();
        }

        protected override void OnPagesCountChange(uint index)
        {
            RedrawCore();
        }

        protected override void RedrawCore()
        {
            if (Page == null) return;

            pagesInfoText.text = Page.PagesCount == 0 
                ? "EMPTY"
                : (Page.CurrentPageIndex + 1) + "/" + Page.PagesCount;

            buttonPrevious.gameObject.SetActive(!Page.IsFirstPage);
            buttonNext.gameObject.SetActive(!Page.IsLastPage);
        }

        protected void NextPage()
        {
            Page.Next();
        }

        protected void PrevPage()
        {
            Page.Back();
        }
    }
}
