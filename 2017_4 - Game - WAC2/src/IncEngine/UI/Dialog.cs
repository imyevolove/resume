﻿using IncEngine.UpdateManagement;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public abstract class Dialog : UIPoolableElement
    {
        [SerializeField] public Text TitleText;
        [SerializeField] public Text MessageText;

        private Coroutine m_ExecutionCoroutine;
        private object m_Selection;
        private bool m_SelectionWasChanged;

        protected virtual void Awake()
        {
        }

        public virtual Dialog Configure(string title, string message)
        {
            TitleText.text = title;
            MessageText.text = message;

            return this;
        }

        public virtual Dialog Execute(Action<object> callback)
        {
            if (m_ExecutionCoroutine != null)
            {
                StopCoroutine(m_ExecutionCoroutine);
                Debug.LogError("Previously started dialog is stopped");
            }
            
            m_ExecutionCoroutine = UpdateManager.StartCoroutine((WaitForSelectionSignal(callback)));

            return this;
        }

        public virtual void SelectOption(object option)
        {
            if (!ValidateOption(option))
            {
                Debug.Log("Option is not validated");
                return;
            }

            m_Selection = option;
            m_SelectionWasChanged = true;
        }
        
        protected virtual void OnSolutionChosen(object option)
        {
            Deactivate();
        }

        protected virtual void OnExecute()
        {
            Activate();
        }

        protected abstract bool ValidateOption(object option);

        private IEnumerator WaitForSelectionSignal(Action<object> callback)
        {
            yield return StartCoroutine(WaitForSelectionChange());
            
            m_ExecutionCoroutine = null;

            OnSolutionChosen(m_Selection);
            callback(m_Selection);
        }

        private IEnumerator WaitForSelectionChange()
        {
            m_SelectionWasChanged = false;
            while (!m_SelectionWasChanged) { yield return false; }
        }
    }
}
