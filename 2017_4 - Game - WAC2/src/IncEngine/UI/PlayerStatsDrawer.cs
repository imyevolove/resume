﻿using System;
using UnityEngine;
using UnityEngine.UI;
using IncEngine.Personal;
using IncEngine.Localization;
using Wac;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public class PlayerStatsDrawer : MonoBehaviour
    {
        public Text Text;

        public Player Player { get; protected set; }

        public void SetTarget(Player player)
        {
            if (Player != null) RemoveEventListeners(player);

            Player = player;
            RegisterEventListeners(Player);

            UpdateUI();
        }

        protected virtual void Start()
        {
            LocalizationManager.onChange += UpdateUI;
        }

        protected void RegisterEventListeners(Player player)
        {
            RemoveEventListeners(player);

            player.Event_AnyChange += RS_EventListenerAnyChange;
        }

        protected void RemoveEventListeners(Player player)
        {
            player.Event_AnyChange -= RS_EventListenerAnyChange;
        }

        protected void RS_EventListenerAnyChange()
        {
            UpdateUI();
        }

        protected void UpdateUI()
        {
            Text.text = string.Format(
                "{0} <color=#ffda4a>{2}</color> / {1} <color=#ffda4a>{3}</color>",
                LocalizationManager.GetLocalization("dpc"),
                LocalizationManager.GetLocalization("dps"),
                Game.NumberFormatter.Format(Player.GetDamageWithoutRandomModifiers()),
                Game.NumberFormatter.Format(Player.GetIncomeWithoutRandomModifiers())
                );
        }
    }
}
