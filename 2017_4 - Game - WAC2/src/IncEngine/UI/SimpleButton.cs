﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable]
    public class SimpleButton : UIMonoBehaviour, IPointerClickHandler
    {
        [SerializeField] public Text buttonText;

        public UnityEvent onClick = new UnityEvent();

        public void SetButtonText(string text)
        {
            buttonText.text = text;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            onClick.Invoke();
        }
    }
}
