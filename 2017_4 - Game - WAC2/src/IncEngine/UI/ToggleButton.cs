﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class ToggleButton : UIPoolableElement, IPointerClickHandler
    {
        [HideInInspector] private Animator m_Animator;
        public Animator Animator { get { return m_Animator; } }

        [SerializeField] public Text Text;

        public ToggleEvent onClick = new ToggleEvent();

        public class ToggleEvent : UnityEvent<ToggleButton>
        {
        }

        public const string AnimatorStateKeyword = "active";

        public bool Toggle
        {
            get { return Animator.GetBool(AnimatorStateKeyword); }
            set
            {
                if (!gameObject.activeInHierarchy) return;
                Animator.SetBool(AnimatorStateKeyword, value);
            }
        }

        public void SetText(string text)
        {
            Text.text = text;
        }

        protected virtual void Awake()
        {
            m_Animator = GetComponent<Animator>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            onClick.Invoke(this);
        }
    }
}
