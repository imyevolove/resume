﻿using IncEngine.Localization;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public abstract class AbstractRankDrawer : MonoBehaviour
    {
        [Header("Properties")]
        public Image image;

        public RankSystem RankSystem { get; protected set; }

        public void SetTarget(RankSystem rankSystem)
        {
            if (RankSystem != null) RemoveEventListeners(rankSystem);

            RankSystem = rankSystem;
            RegisterEventListeners(RankSystem);

            UpdateUI();
        }

        protected virtual void Start()
        {
            LocalizationManager.onChange += UpdateUI;
        }

        protected void RegisterEventListeners(RankSystem rankSystem)
        {
            RemoveEventListeners(rankSystem);

            rankSystem.Event_ProgressChange += RS_EventListenerProgressChange;
            rankSystem.Event_RankChange += RS_EventListenerRankChange;
        }

        protected void RemoveEventListeners(RankSystem rankSystem)
        {
            rankSystem.Event_ProgressChange -= RS_EventListenerProgressChange;
            rankSystem.Event_RankChange -= RS_EventListenerRankChange;
        }

        protected void RS_EventListenerProgressChange()
        {
            OnProgressChange();
        }

        protected void RS_EventListenerRankChange()
        {
            OnRankChange();
        }

        protected void UpdateUI()
        {
            OnRankChange();
            OnProgressChange();
        }

        protected virtual void OnRankChange()
        {
            image.sprite = RankSystem.CurrentRank.image;
            OnProgressChange();
        }

        /// CAN BE OPTIMIZED
        protected abstract void OnProgressChange();
    }
}