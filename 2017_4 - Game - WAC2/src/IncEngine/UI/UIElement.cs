﻿using System;

namespace IncEngine.UI
{
    public abstract class UIElement<T> : Drawer<T>, IPoolable where T : class
    {
        public bool Active
        {
            get { return gameObject.activeSelf; }
            set
            {
                if (Active == value) return;

                gameObject.SetActive(value);

                if (Event_ActiveChange != null)
                    Event_ActiveChange(this, value);
            }
        }

        public event Action<IPoolable> Event_Activate;
        public event Action<IPoolable> Event_Deactivate;
        public event Action<IPoolable, bool> Event_ActiveChange;

        /// <summary>
        /// Activate poolable element
        /// </summary>
        public void Activate()
        {
            if (Active) return;
            Active = true;
            
            OnActivate();
        }

        /// <summary>
        /// Deactivate poolable element
        /// </summary>
        public void Deactivate()
        {
            if (!Active) return;
            Active = false;

            OnDeactivate();
        }

        protected virtual void OnActivate()
        {
            if (Event_Activate != null)
            {
                Event_Activate(this);
            }

            CallActiveChangeEvent();
        }

        protected virtual void OnDeactivate()
        {
            if (Event_Deactivate != null)
            {
                Event_Deactivate(this);
            }

            CallActiveChangeEvent();
        }

        protected void CallActiveChangeEvent()
        {
            if (Event_ActiveChange != null)
            {
                Event_ActiveChange(this, Active);
            }
        }
    }
}