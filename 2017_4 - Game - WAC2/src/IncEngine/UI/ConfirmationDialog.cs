﻿using System;
using UnityEngine;

namespace IncEngine.UI
{
    public enum ConfirmationDialogResult
    {
        Yes,
        No
    }

    [Serializable]
    public class ConfirmationDialog : Dialog
    {
        [SerializeField] public SimpleButton ButtonYes;
        [SerializeField] public SimpleButton ButtonNo;

        protected override void Awake()
        {
            base.Awake();

            ButtonYes.onClick.AddListener(() => { Confirm(ConfirmationDialogResult.Yes); });
            ButtonNo.onClick.AddListener(() => { Confirm(ConfirmationDialogResult.No); });
        }

        public ConfirmationDialog Configure(string title, string message, string messageYes, string messageNo)
        {
            Configure(title, message);

            ButtonYes.SetButtonText(messageYes);
            ButtonNo.SetButtonText(messageNo);

            return this;
        }

        public void Confirm(ConfirmationDialogResult option)
        {
            SelectOption(option);
        }

        public ConfirmationDialog Execute(Action<ConfirmationDialogResult> callback)
        {
            return (ConfirmationDialog)Execute((object obj) => 
            {
                callback(CastOption(obj));
            });
        }

        protected override bool ValidateOption(object option)
        {
            return option is ConfirmationDialogResult;
        }

        protected ConfirmationDialogResult CastOption(object option)
        {
            return (option is ConfirmationDialogResult)
                    ? (ConfirmationDialogResult)option
                    : default(ConfirmationDialogResult);
        }
    }
}
