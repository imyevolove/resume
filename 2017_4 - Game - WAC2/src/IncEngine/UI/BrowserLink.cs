﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [SerializeField, RequireComponent(typeof(Image))]
    public class BrowserLink : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] public string Url;

        public void OpenLink()
        {
            Application.OpenURL(Url);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OpenLink();
        }
    }
}