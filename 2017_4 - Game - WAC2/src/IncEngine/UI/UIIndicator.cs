﻿using System;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable]
    public class UIIndicator : UIPoolableElement
    {
        [Tooltip("Use '" + AnimatorActiveParameterName + "' bool parameter for handle indicator state from animator")]
        [SerializeField]
        public Animator animator;

        public event Action<bool> Event_IndicationChange;
        public const string AnimatorActiveParameterName = "indication";

        public bool IsActive { get { return animator.GetBool(AnimatorActiveParameterName); } }
        
        public void SetIndication(bool status)
        {
            if (IsActive == status) return;

            animator.SetBool(AnimatorActiveParameterName, status);
            OnIndicationChange();
        }

        public void EnableIndication()
        {
            if (IsActive == true) return;

            animator.SetBool(AnimatorActiveParameterName, true);
            OnIndicationChange();
        }

        public void DisableIndication()
        {
            if (IsActive == false) return;

            animator.SetBool(AnimatorActiveParameterName, false);
            OnIndicationChange();
        }

        protected virtual void OnIndicationChange()
        {
            if (Event_IndicationChange != null)
            {
                Event_IndicationChange(IsActive);
            }
        }
    }
}
