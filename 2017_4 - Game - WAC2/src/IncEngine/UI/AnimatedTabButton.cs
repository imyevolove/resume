﻿using System;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class AnimatedTabButton : TabButton
    {
        [HideInInspector] private Animator m_Animator;
        public Animator Animator
        {
            get
            {
                if (m_Animator == null)
                {
                    m_Animator = GetComponent<Animator>();
                }

                return m_Animator;
            }
        }

        [SerializeField] public string AnimationControlKeyName;

        public override void Activate()
        {
            base.Activate();

            Animator.SetBool(AnimationControlKeyName, true);
        }

        public override void Deactivate()
        {
            base.Deactivate();

            Animator.SetBool(AnimationControlKeyName, false);
        }
    }
}
