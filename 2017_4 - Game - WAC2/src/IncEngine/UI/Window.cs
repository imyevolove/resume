﻿using IncEngine.Activities;
using System;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public class Window : Activity
    {
        [SerializeField]
        public string windowName;
    }
}