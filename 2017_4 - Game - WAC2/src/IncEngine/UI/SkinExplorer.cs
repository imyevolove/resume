﻿using System;
using UnityEngine;
using IncEngine.Structures;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable]
    public class SkinExplorer : Drawer<Skin>, IPoolable
    {
        [SerializeField]
        public Text titleText;
        [SerializeField]
        public Text subTitleText;
        [SerializeField]
        public Image image;

        public bool Active { get { return gameObject.activeSelf; } }

        public event Action<IPoolable> Event_Activate;
        public event Action<IPoolable, bool> Event_ActiveChange;
        public event Action<IPoolable> Event_Deactivate;

        private RectTransform m_Transform;
        public RectTransform Transform
        {
            get
            {
                if (m_Transform == null)
                {
                    m_Transform = GetComponent<RectTransform>();
                }

                return m_Transform;
            }
        }

        public void Activate()
        {
            gameObject.SetActive(true);

            if (Event_Activate != null)
            {
                Event_Activate(this);
            }

            OnActiveChange(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);

            if (Event_Deactivate != null)
            {
                Event_Deactivate(this);
            }

            OnActiveChange(false);
        }

        protected override void OnRegisterTarget(Skin target)
        {
            titleText.text      = target.Original.Name.ToUpper();
            subTitleText.text   = target.Name.ToUpper();
            subTitleText.color  = target.Quality.Color;
            image.sprite        = target.Icon;
        }

        protected override void OnUnregisterTarget(Skin target)
        {
            
        }

        protected virtual void OnActiveChange(bool active)
        {
            if (Event_ActiveChange != null)
            {
                Event_ActiveChange(this, active);
            }
        }
    }
}
