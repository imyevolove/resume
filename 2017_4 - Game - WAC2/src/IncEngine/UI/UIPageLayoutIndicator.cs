﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace IncEngine.UI
{
    public class UIPageLayoutIndicator : UIIndicator, IPointerClickHandler
    {
        public IndicatorEvent onClick = new IndicatorEvent();
        [HideInInspector] public uint Index = 0;

        public class IndicatorEvent : UnityEvent<uint>
        {
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            onClick.Invoke(Index);
        }
    }
}
