﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable]
    public class ScrollRectGridBoost : MonoBehaviour
    {
        [SerializeField] public UIPoolableElement capItemPrefab;
        [SerializeField] public int itemsPerLayout;
        [SerializeField] public GridLayoutGroup GridLayoutGroup;
        [SerializeField] public ScrollRect ScrollRect;

        public Rect LayoutSize { get; protected set; }

        RectTransform m_RectTransform;
        public RectTransform RectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }

                return m_RectTransform;
            }
        }

        private int CurrentLayoutIndex = 0;

        protected virtual void Start()
        {
            RecalculateLayout();

            ScrollRect.onValueChanged.AddListener((vec) => {
                Debug.Log(vec);
                if (vec.y > 1f)
                {
                    RecalculateLayoutIndex(-1);
                    Rebuild();
                }
                else if (vec.y < 0f)
                {
                    RecalculateLayoutIndex(1);
                    Rebuild();
                }
            });

            Rebuild();
        }

        public void Rebuild()
        {
            var counter = 0;
            var minIndex = itemsPerLayout * CurrentLayoutIndex;
            var maxIndex = minIndex + itemsPerLayout;

            Debug.Log(CurrentLayoutIndex);

            foreach (RectTransform child in ScrollRect.content)
            {
                child.gameObject.SetActive(counter >= minIndex && counter <= maxIndex);
                counter++;
            }
        }

        protected void RecalculateLayoutIndex(int axis)
        {
            var index = CurrentLayoutIndex + axis;

            if (index < 0) return;
            if (CurrentLayoutIndex * itemsPerLayout - itemsPerLayout > GridLayoutGroup.transform.childCount) return;

            CurrentLayoutIndex = index;
            
            if (axis < 0)
            {
                ScrollRect.verticalNormalizedPosition = 0.5f;
            }
            else
            {
                ScrollRect.verticalNormalizedPosition = 0.5f;
            }
        }

        protected virtual void OnLayoutIndexChanged(int index)
        {
            Rebuild();
        }

        protected void RecalculateLayout()
        {
            var cellFullWidth = GridLayoutGroup.cellSize.x + GridLayoutGroup.padding.left + GridLayoutGroup.padding.right;
            var cellFullHeight = GridLayoutGroup.cellSize.y + GridLayoutGroup.padding.top + GridLayoutGroup.padding.bottom;

            var verticalItemsCount = 0;

            switch (GridLayoutGroup.constraint)
            {
                case GridLayoutGroup.Constraint.FixedColumnCount:
                    verticalItemsCount = itemsPerLayout / GridLayoutGroup.constraintCount;
                    break;

                case GridLayoutGroup.Constraint.FixedRowCount:
                    verticalItemsCount = GridLayoutGroup.constraintCount;
                    break;

                case GridLayoutGroup.Constraint.Flexible:
                default:
                    var hcount = Mathf.FloorToInt(RectTransform.rect.size.x / cellFullWidth);
                    hcount = hcount <= 0 ? 1 : hcount;

                    verticalItemsCount = itemsPerLayout / hcount;
                    break;
            }

            LayoutSize = new Rect(0, 0, 0, cellFullHeight * verticalItemsCount);
        }
    }
}
