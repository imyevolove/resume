﻿using UnityEngine;

namespace IncEngine.UI
{
    /// <summary>
    /// Abstract drawer class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [SerializeField]
    public abstract class Drawer<T> : UIMonoBehaviour
    {
        public T Target { get; protected set; }
        public bool HasTarget { get { return Target != null; } }

        [HideInInspector]
        private bool m_IsDestroyed = false;

        protected virtual void Awake()
        {
        }

        public void SetTarget(T target)
        {
            if (m_IsDestroyed) return;
            if (Target != null) OnUnregisterTarget(Target);
            
            Target = target;
            OnRegisterTarget(Target);
        }

        public void Destroy()
        {
            if (m_IsDestroyed) return;

            OnUnregisterTarget(Target);
            Destroy(gameObject);

            m_IsDestroyed = true;
        }

        protected abstract void OnUnregisterTarget(T target);
        protected abstract void OnRegisterTarget(T target);
    }
}
