﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace IncEngine.UI
{
    [Serializable]
    public class PageLayoutGroupPointerHandler : PageLayoutGroupHandler
    {
        [SerializeField] public UIPageLayoutIndicator indicatorPrefab;

        protected MonoEntityPool<UIPageLayoutIndicator> m_InidicatorsPool;
        protected List<UIPageLayoutIndicator> m_Indicators = new List<UIPageLayoutIndicator>();

        protected override void Awake()
        {
            base.Awake();

            Setup();
        }
        
        public void OnEnable()
        {

            UnregisterPageLayoutGroupEvents(Page);
            RegisterPageLayoutGroupEvents(Page);
        }

        public void OnDisable()
        {
            UnregisterPageLayoutGroupEvents(Page);
        }

        protected void Setup()
        {
            if (m_InidicatorsPool == null)
            {
                m_InidicatorsPool = new MonoEntityPool<UIPageLayoutIndicator>(indicatorPrefab);
            }
        }

        protected override void RedrawCore()
        {
            Setup();

            m_InidicatorsPool.DeactivateAllActiveEntities();
            m_Indicators.Clear();
            
            if (Page == null) return;

            for (var i = 0; i < Page.PagesCount; i++)
            {
                var indicator = GetFreeIndicator();
                indicator.Index = (uint)i;
                indicator.onClick.RemoveListener(IndicatorClickHandler);
                indicator.onClick.AddListener(IndicatorClickHandler);

                m_Indicators.Add(indicator);
            }

            RedrawIndicators();
        }
        
        protected override void OnPageChange(uint index)
        {
            RedrawIndicators();
        }

        protected override void OnPagesCountChange(uint index)
        {
            RedrawCore();
        }

        protected void RedrawIndicators()
        {
            if (Page == null) return;

            for (var i = 0; i < m_Indicators.Count; i++)
            {
                m_Indicators[i].SetIndication(i == Page.CurrentPageIndex);
            }
        }

        protected UIPageLayoutIndicator GetFreeIndicator()
        {
            var indicator = m_InidicatorsPool.GetFree(content);
            indicator.RectTransform.SetAsLastSibling();

            return indicator;
        }

        protected void IndicatorClickHandler(uint index)
        {
            Page.Select(index);
        }
    }
}
