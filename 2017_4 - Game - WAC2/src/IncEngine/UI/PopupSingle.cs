﻿using System;
using UnityEngine;

namespace IncEngine.UI
{
    public class PopupSingle : MonoBehaviour
    {
        public event Action Event_Show;
        public event Action Event_Hide;

        public bool IsShown     { get { return gameObject.activeSelf; } }
        public bool IsLocked    { get { return m_Lock; } }

        private bool m_Lock = false;

        public void Lock()
        {
            m_Lock = true;
        }

        public void Unlock()
        {
            m_Lock = false;
        }

        /// <summary>
        /// Show popup window
        /// </summary>
        public void Show()
        {
            gameObject.SetActive(true);
            OnShow();
        }

        /// <summary>
        /// Hide popup window
        /// </summary>
        public void Hide()
        {
            if (IsLocked)
            {
                Debug.Log("Can't hide popup. Popup is locked");
                return;
            }

            gameObject.SetActive(false);
            OnHide();
        }

        /// <summary>
        /// Wil be called when show
        /// </summary>
        protected virtual void OnShow()
        {
            if (Event_Show != null)
            {
                Event_Show();
            }
        }

        /// <summary>
        /// Wil be called when hide
        /// </summary>
        protected virtual void OnHide()
        {
            if (Event_Hide != null)
            {
                Event_Hide();
            }
        }
    }
}
