﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable, DisallowMultipleComponent]
    public class PurchaseButton : MonoBehaviour, IPointerClickHandler
    {
        public event Action Event_Click;

        [SerializeField] public Image ResourceIconImage;
        [SerializeField] public Text ResourceCountText;
        [SerializeField] public Text TitleText;

        public void SetPrice(uint price)
        {
            ResourceCountText.text = price.ToString();
        }

        public void SetPrice(int price)
        {
            ResourceCountText.text = price.ToString();
        }

        public void SetPrice(float price)
        {
            ResourceCountText.text = price.ToString();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick();
        }

        protected virtual void OnClick()
        {
            if (Event_Click != null) Event_Click();
        }
    }
}
