﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IncEngine.UI
{
    [Serializable, RequireComponent(typeof(Image))]
    public class TabButton : MonoBehaviour, IPointerClickHandler
    {
        public event Action Event_Click;

        [SerializeField]
        public Text Text;

        void Awake()
        {
        }

        public virtual void SetText(string text)
        {
            if (Text == null) return;
            Text.text = text;
        }

        public void Click()
        {
            if (Event_Click != null) Event_Click();
        }

        public virtual void Activate()
        {
        }

        public virtual void Deactivate()
        {
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Click();
        }
    }
}
