﻿using IncEngine.Collections;
using IncEngine.Structures;

namespace IncEngine.Deals
{
    public class ContractEntityTypeValidator : ContractValidator
    {
        public EntityMask Mask { get; protected set; }
        public ContractValidatorActionType ActionType { get; protected set; }

        public ContractEntityTypeValidator()
        {
            Configure(ContractValidatorActionType.Include, new EntityMask(0));
        }

        public ContractEntityTypeValidator(ContractValidatorActionType action, params EntityType[] values)
        {
            Configure(action, EntityMask.Create(values));
        }

        public void Configure(ContractValidatorActionType action, EntityMask mask)
        {
            Mask = mask;
            ActionType = action;
        }

        public override bool Validate(InventorySkinItem item)
        {
            return ContractValidatorUtility.ConvertValidatorAction(ActionType,
                Enabled ? Mask.HasValue(item.EntityType) : true);
        }
    }
}
