﻿using IncEngine.Collections;
using IncEngine.Crypto;
using IncEngine.Dice;
using IncEngine.Personal;
using IncEngine.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace IncEngine.Deals
{
    public class Contract
    {
        public event Action                     Event_Clear;
        public event Action<InventorySkinItem>  Event_Add;
        public event Action<InventorySkinItem>  Event_Remove;
        public event Action                     Event_FirstAdd;
        public event Action                     Event_Change;
        public event Action                     Event_TransactionChange;

        public int                              maxContractItems = 10;

        public List<InventorySkinItem>          ContractItems       { get; private set; }
        public int                              ContractItemsCount  { get { return ContractItems.Count; } }
        public bool                             IsFilled            { get { return ContractItems.Count >= maxContractItems; } }
        public bool                             IsEmpty             { get { return ContractItems.Count == 0; } }
        public ContractValidator                Validator           { get; protected set; }
        public CaseOpener                       CaseOpener          { get; protected set; }
        public Player                           Owner               { get; set; }

        public readonly ContractQualityValidator QualityValidator;

        public uint     TransactionIndex        { get { return m_TransactionCounter + 1; } }
        public string   TransactionSignature    { get { return MD5.CreateHash("c" + TransactionIndex); } }

        private uint m_TransactionCounter = 0;

        public Contract(ContractValidator validator, CaseOpener opener)
        {
            ContractItems   = new List<InventorySkinItem>();
            CaseOpener      = opener;

            QualityValidator = new ContractQualityValidator(ContractValidatorActionType.Include)
            {
                Enabled = false
            };

            Validator = new ContractValidatorChainBuilder()
                .AddValidator(validator)
                .AddValidator(QualityValidator)
                .Build();
        }

        public Contract(ContractValidator validator, CaseOpener opener, Player owner)
        {
            ContractItems   = new List<InventorySkinItem>();
            CaseOpener      = opener;
            Owner           = owner;

            QualityValidator = new ContractQualityValidator(ContractValidatorActionType.Include)
            {
                Enabled = false
            };

            Validator = new ContractValidatorChainBuilder()
                .AddValidator(validator)
                .AddValidator(QualityValidator)
                .Build();
        }

        public bool HasContractItem(InventorySkinItem item)
        {
            return ContractItems.Contains(item);
        }
        
        /// <summary>
        /// Add item to contract
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Add(InventorySkinItem item)
        {
            if (IsFilled)
            {
                Debug.Log("Contract filled");
                return false;
            }

            if (ContractItems.Contains(item))
            {
                Debug.Log("Item already exists. Can't add item");
                return false;
            }

            if (!Validator.Validate(item))
            {
                return false;
            }

            ContractItems.Add(item);
            
            if (ContractItemsCount == 1)
            {
                QualityValidator.Configure(item.Quality as Quality);
                QualityValidator.Enable();

                OnFirstAdd();
            }

            OnAdd(item);
            OnChange();

            return true;
        }

        /// <summary>
        ///  Remove item from contract
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(InventorySkinItem item)
        {
            var status = ContractItems.Remove(item);

            if (ContractItemsCount == 0)
            {
                QualityValidator.Disable();
            }

            if (status)
            {
                OnRemove(item);
                OnChange();
            }

            return status;
        }

        /// <summary>
        /// Clear contract
        /// </summary>
        public void Reset()
        {
            if (ContractItemsCount == 0) return;

            ContractItems.Clear();
            QualityValidator.Disable();

            OnClear();
            OnChange();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public InventorySkinItem ApproveTransaction(bool assignResultToPlayer = true)
        {
            if (!IsFilled)
            {
                Debug.Log("Contract is not filled");
                return null;
            }

            if (CaseOpener == null)
            {
                Debug.Log("Case opener not defined");
                return null;
            }

            if (Owner == null)
            {
                Debug.Log("Owner not defined");
                return null;
            }

            var index = UnityEngine.Random.Range(0, ContractItems.Count);
            var item = ContractItems[index];
            var targetQuality = Structures.Quality.Next(item.Quality as Quality);

            /// Available collections
            var collections = ContractItems.Select(_i => _i.Skin.Collection).Distinct().ToArray();
            Array.Sort(collections, (a, b) => 1 - 2 * UnityEngine.Random.Range(0, 1));
            
            var cs = EntitiesManager.FindCase(collections, targetQuality);
            InventorySkinItem inventorySkinItem = null;

            if (cs == null)
            {
                Debug.Log(EntitiesManager.FindCase(Structures.Quality.QualityClassified));

                if (cs == null)
                {
                    Debug.Log("Case not found. Switch to random case.");

                    cs = EntitiesManager.Cases[UnityEngine.Random.Range(0, EntitiesManager.Cases.Count)];
                    inventorySkinItem = CaseOpener.GetRandomSkinForInventory(cs);
                }
            }

            if (inventorySkinItem == null)
            {
                inventorySkinItem = CaseOpener.GetRandomSkinForInventory(cs, targetQuality);
            }
            
            if (inventorySkinItem == null)
            {
                Debug.Log("Inventory item was not generated");
                return null;
            }

            if (assignResultToPlayer && !Owner.Inventory.Add(inventorySkinItem))
            {
                Debug.Log("Could not add item to player inventory");
                return null;
            }

            Owner.Inventory.RemoveRange(ContractItems.Cast<InventoryItem>());
            Reset();

            m_TransactionCounter++;
            OnTrasnsactionChange();

            return inventorySkinItem;
        }

        protected virtual void OnAdd(InventorySkinItem item)
        {
            if (Event_Add != null)
            {
                Event_Add(item);
            }
        }

        protected virtual void OnRemove(InventorySkinItem item)
        {
            if (Event_Remove != null)
            {
                Event_Remove(item);
            }
        }

        protected virtual void OnChange()
        {
            if (Event_Change != null)
            {
                Event_Change();
            }
        }

        protected virtual void OnClear()
        {
            if (Event_Clear != null)
            {
                Event_Clear();
            }
        }

        protected virtual void OnTrasnsactionChange()
        {
            if (Event_TransactionChange != null)
            {
                Event_TransactionChange();
            }
        }

        protected virtual void OnFirstAdd()
        {
            if (Event_FirstAdd != null)
            {
                Event_FirstAdd();
            }
        }
    }
}
