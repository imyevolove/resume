﻿using System.Linq;
using IncEngine.Collections;
using IncEngine.Structures;

namespace IncEngine.Deals
{
    public class ContractSpecialValidator : ContractValidator
    {
        public SkinSpecial[]                Specials    { get; protected set; }
        public ContractValidatorActionType  ActionType  { get; protected set; }

        public ContractSpecialValidator()
        {
            Configure(ContractValidatorActionType.Include, new SkinSpecial[0]);
        }

        public ContractSpecialValidator(ContractValidatorActionType action, params SkinSpecial[] values)
        {
            Configure(action, values);
        }

        public void Configure(ContractValidatorActionType action, params SkinSpecial[] values)
        {
            Specials = values;
            ActionType = action;
        }

        public override bool Validate(InventorySkinItem item)
        {
            if (!Enabled) return true;
            return ContractValidatorUtility.ConvertValidatorAction(ActionType,
                Enabled ? (item.Special == null || Specials.Contains(item.Special)) : true);
        }
    }
}
