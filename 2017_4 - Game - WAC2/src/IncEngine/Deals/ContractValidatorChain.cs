﻿using System.Collections.Generic;
using IncEngine.Collections;

namespace IncEngine.Deals
{
    public class ContractValidatorChain : ContractValidator
    {
        private List<ContractValidator> m_Validators = new List<ContractValidator>();

        /// <summary>
        /// Validate item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool Validate(InventorySkinItem item)
        {
            foreach (var validator in m_Validators)
            {
                if (!validator.Validate(item))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Add validator to chain
        /// </summary>
        /// <param name="validator"></param>
        public void AddValidator(ContractValidator validator)
        {
            m_Validators.Add(validator);
        }

        /// <summary>
        /// Remove validator from chain
        /// </summary>
        /// <param name="validator"></param>
        /// <returns></returns>
        public bool RemoveValidator(ContractValidator validator)
        {
            return m_Validators.Remove(validator);
        }
    }
}
