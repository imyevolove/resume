﻿using IncEngine.Collections;

namespace IncEngine.Deals
{
    public class ContractCollectionValidator : ContractValidator
    {
        public override bool Validate(InventorySkinItem item)
        {
            if (!Enabled) return true;
            return item.Skin.Collection != null;
        }
    }
}
