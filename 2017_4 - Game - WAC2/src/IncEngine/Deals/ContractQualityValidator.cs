﻿using System.Linq;
using IncEngine.Collections;

namespace IncEngine.Deals
{
    public class ContractQualityValidator : ContractValidator
    {
        public Structures.Quality[]     Qualities   { get; protected set; }
        public ContractValidatorActionType  ActionType  { get; protected set; }

        public ContractQualityValidator(ContractValidatorActionType action, params Structures.Quality[] values)
        {
            Configure(action, values);
        }

        public void Configure(ContractValidatorActionType action, params Structures.Quality[] values)
        {
            Qualities = values == null ? new Structures.Quality[0] : values;
            ActionType = action;
        }

        public void Configure(params Structures.Quality[] values)
        {
            Qualities = values == null ? new Structures.Quality[0] : values;
        }

        public override bool Validate(InventorySkinItem item)
        {
            return ContractValidatorUtility.ConvertValidatorAction(ActionType,
                Enabled ? Qualities.Contains(item.Quality) : true);
        }
    }
}
