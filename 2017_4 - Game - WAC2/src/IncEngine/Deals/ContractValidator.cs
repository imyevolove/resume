﻿using IncEngine.Collections;

namespace IncEngine.Deals
{
    public abstract class ContractValidator
    {
        public bool Enabled = true;
        public abstract bool Validate(InventorySkinItem item);

        public void Enable()
        {
            Enabled = true;
        }

        public void Disable()
        {
            Enabled = false;
        }
    }
}