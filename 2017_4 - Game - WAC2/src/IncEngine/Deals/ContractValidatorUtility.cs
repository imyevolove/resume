﻿namespace IncEngine.Deals
{
    public static class ContractValidatorUtility
    {
        public static bool ConvertValidatorAction(ContractValidatorActionType action, bool validationStatus)
        {
            switch (action)
            {
                case ContractValidatorActionType.Exclude:
                    return !validationStatus;

                case ContractValidatorActionType.Include:
                default: return validationStatus;
            }
        }
    }
}
