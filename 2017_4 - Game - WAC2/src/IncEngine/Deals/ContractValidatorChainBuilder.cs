﻿namespace IncEngine.Deals
{
    public class ContractValidatorChainBuilder
    {
        private ContractValidatorChain m_ChainValidator;

        public ContractValidatorChainBuilder()
        {
            m_ChainValidator = new ContractValidatorChain();
        }

        public ContractValidatorChainBuilder AddValidator(ContractValidator validator)
        {
            m_ChainValidator.AddValidator(validator);
            return this;
        }

        public ContractValidatorChain Build()
        {
            return m_ChainValidator;
        }
    }
}
