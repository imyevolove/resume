﻿namespace IncEngine.Common
{
    public delegate void IncrementalProgressEvent(IncrementalProgress sender, ulong value);
}
