﻿using System;
using UnityEngine;

namespace IncEngine.Common
{
    [Serializable]
    public class IncrementalProgress
    {
        public event IncrementalProgressEvent Tick;
        public event IncrementalProgressEvent End;

        [SerializeField]
        public ulong MaxValue;
        public ulong Value   {
            get { return m_Value; }
            set
            {
                m_Value = value;

                if (m_Value > MaxValue) {
                    m_Value = 0;
                    CallEndEvent(value);
                }

                CallTickEvent(value);
            }
        }

        public float Percentage01   { get { return Value / (float)MaxValue; } }
        public float Percentage     { get { return Percentage01 * 100; } }

        private ulong m_Value;

        public IncrementalProgress()
        {
        }

        public IncrementalProgress(ulong max)
        {
            MaxValue = max;
        }

        protected void CallTickEvent(ulong value)
        {
            if (Tick == null) return;
            Tick(this, value);
        }

        protected void CallEndEvent(ulong value)
        {
            if (End == null) return;
            End(this, m_Value);
        }
    }
}
