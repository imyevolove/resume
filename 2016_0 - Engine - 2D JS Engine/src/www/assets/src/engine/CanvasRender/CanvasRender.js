var CanvasRender = (function () {
    // region static properties
    // endregion
    function  CanvasRender() {
        var $ = this;

        // region # private properties
        var _canvasHTMLElement,
            _canvasContext,
            _extents;
        // endregion
        // region # constructor
        function constructor() {
            _extents = Vector2.prototype.zero;
            _canvasHTMLElement = document.createElement("canvas");
            _canvasHTMLElement.setAttribute("style", "position:absolute;width:100%;height:100%;top:0;left:0");

            WebGL2D.enable(_canvasHTMLElement);
            _canvasContext = _canvasHTMLElement.getContext("2d");

            document.body.appendChild(_canvasHTMLElement);
            window.addEventListener("resize", $.resize);
            $.resize();
        }
        // endregion
        // region # public

        this.__defineGetter__("canvas", function () {
            return _canvasHTMLElement;
        });
        this.__defineGetter__("context", function () {
            return _canvasContext;
        });

        this.__defineGetter__("extents", function () {
            return _extents;
        });

        this.resize = function () {
            _canvasHTMLElement.width  = _canvasHTMLElement.offsetWidth;
            _canvasHTMLElement.height = _canvasHTMLElement.offsetHeight;
            _extents.x = _canvasHTMLElement.width  / 2;
            _extents.y = _canvasHTMLElement.height / 2;
        };

        this.clear = function () {
            _canvasContext.clearRect(0, 0, _canvasHTMLElement.width, _canvasHTMLElement.height);
        };

        this.fillBackground = function (color) {
            if(color == void 0 || !color.is(Color)) return;
            _canvasContext.fillStyle = "#" + color.hex;
            _canvasContext.fillRect(0, 0, _canvasHTMLElement.width, _canvasHTMLElement.height);
        };

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return CanvasRender;
})();