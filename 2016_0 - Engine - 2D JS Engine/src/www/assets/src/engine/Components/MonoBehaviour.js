var MonoBehaviour = (function () {
    // region static properties
    // endregion
    function  MonoBehaviour(gameObject) {
        var $ = this;
        // region # private properties
        var _update;
        // endregion
        // region # constructor
        function constructor() {
            _update = UpdateManager.registerUpdateOnLevel(UpdateManager.levels.logic);
            _update.callUpdate = function () {
                $.update();
            };

            this.destroy = function () {
                UpdateManager.destroyUpdateOnLevel(UpdateManager.levels.logic, _update);
            };
        }
        // endregion
        // region # public

        this.start  = function () {};
        this.awake  = function () {};
        this.update = function () {};

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    MonoBehaviour = MonoBehaviour.extend(Component);
    return MonoBehaviour;
})();