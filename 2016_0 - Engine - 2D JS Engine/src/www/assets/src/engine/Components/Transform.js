var Transform = (function () {
    // region static properties
    // endregion
    function  Transform() {
        // region # private properties
        var $ = this;
        var _position,
            _rotation,
            _scale,
            _parentTransform,
            _childrenTransforms;
        // endregion
        // region # constructor
        function constructor() {
            _position = Vector3.prototype.zero;
            _rotation = Vector3.prototype.zero;
            _scale = Vector3.prototype.one;

            _parentTransform = null;
            _childrenTransforms = [];
        }
        // endregion
        // region # public

        // Position
        this.__defineGetter__("position", function () {
            return _position;
        });
        this.__defineSetter__("position", function (value) {
            if(value == void 0) throw new ErrorArgsMissing();
            if(!value.is(Vector3)) throw new ErrorWrongType(Vector3);
            _position.set(value);
        });

        // Rotation
        this.__defineGetter__("rotation", function () {
            return _rotation;
        });
        this.__defineSetter__("rotation", function (value) {
            if(value == void 0) throw new ErrorArgsMissing();
            if(!value.is(Vector3)) throw new ErrorWrongType(Vector3);
            _rotation.set(value);
        });

        // Scale
        this.__defineGetter__("scale", function () {
            return _scale;
        });
        this.__defineSetter__("scale", function (value) {
            if(value == void 0 || !value.is(Vector3)) throw new Error("Arg is not Vector 3");
            _scale.set(value);
        });

        this.__defineGetter__("scaleAtParent", function () {
            if(_parentTransform == void 0) return _scale;
            return Vector3.prototype.multiply(_parentTransform.scaleAtParent, _scale);
        });

        //
        this.setParent = function (parentTransform) {
            try {
                if(parentTransform == void 0) throw new ErrorArgsMissing();
                if(!parentTransform.is(Transform)) throw new ErrorWrongType(Transform);
                if(parentTransform == this) throw new Error("Can't set parent itself");
                if(!!~_childrenTransforms.indexOf(parentTransform)) throw new Error("Can't set children at parent");
                parentTransform._addChild(this, false);
                _position.set(parentTransform.inverseTransformPoint($));
                _parentTransform = parentTransform;
            } catch(err) {
                throw new Error(err);
            }
        };

        this.removeParent = function () {
            try {
                if(_parentTransform == void 0) throw new Error("Parent transform not found");
                _parentTransform._removeChild(this);
                _position = $.worldPosition;
                _scale = $.scaleAtParent;
                _rotation = $.worldRotation;
                _parentTransform = null;
            } catch(err) {
                throw new Error(err);
            }

        };

        //
        this._addChild = function (childTransform, recursive) {
            try {
                if(childTransform == void 0) throw new ErrorArgsMissing();
                if(!childTransform.is(Transform)) throw new ErrorWrongType(Transform);
                if(childTransform == this) throw new Error("Can't set children at parent");
                if(_parentTransform == childTransform) throw new Error("Can't set children at parent");
                if(!!~_childrenTransforms.indexOf(childTransform)) throw new Error("Required children already exists");
                if(recursive == true) childTransform.setParent(this);
                _childrenTransforms.push(childTransform);
            } catch(err) {
                throw new Error(err);
            }
        };

        //
        this._removeChild = function (childTransform, recursive) {
            if(childTransform == void 0) throw new ErrorArgsMissing();
            if(!childTransform.is(Transform)) throw new ErrorWrongType(Transform);
            if(!~_childrenTransforms.indexOf(childTransform)) throw new Error("Specified children not found");
            try {
                _childrenTransforms.splice(_childrenTransforms.indexOf(childTransform), 1);
                if(recursive == true) childTransform.removeParent(this);
            } catch(err) {
                throw new Error(err);
            }

        };

        //
        this.__defineGetter__("parent", function () {
            return _parentTransform;
        });

        //
        this.__defineGetter__("childCount", function () {
            return _childrenTransforms.length;
        });
        this.getChild = function(childIndex) {
            return _childrenTransforms[childIndex];
        };

        /**
         * Transform world position to local position
         * @param transform
         */
        this.inverseTransformPoint = function (transform) {
            if(transform == void 0) throw new ErrorArgsMissing();
            if(!transform.is(Transform)) throw new ErrorWrongType(Transform);
            return Vector3.prototype.subtract(transform.worldPosition, $.worldPosition);
        };

        /**
         * Transform local position to world position
         * @param transform
         */
        this.transformPoint = function (transform) {
            if(transform == void 0) throw new ErrorArgsMissing();
            if(!transform.is(Transform)) throw new ErrorWrongType(Transform);
            return Vector3.prototype.addition(transform.localPosition, $.worldPosition);
        };

        this.__defineGetter__("worldPosition", function () {
            if(_parentTransform == void 0) return _position;
            var wp = _parentTransform.transformPoint(this);
            var rot = rotateAroundPoint2D(_parentTransform.worldPosition, wp, -_parentTransform.worldRotation.y);
            return new Vector3(rot.x, rot.y, wp.z);
        });
        this.setWorldPosition = function (value) {
            if(value == void 0) throw new ErrorArgsMissing();
            if(!value.is(Vector3)) throw new ErrorWrongType(Vector3);
            if(_parentTransform == void 0) {
                _position.set(value);
            } else {
                console.log("asd");
                var wp = _parentTransform.worldPosition;
                _position.x = value.x - wp.x;
                _position.y = value.y - wp.y;
                _position.z = value.z - wp.z;
            }
        };
        
        this.__defineGetter__("localPosition", function () {
            return _position;
        });
        this.setLocalPosition = function (value) {
            if(value == void 0) throw new ErrorArgsMissing();
            if(!value.is(Vector3)) throw new ErrorWrongType(Vector3);
            _position.set(value);

        };

        this.__defineGetter__("worldRotation", function () {
            if(_parentTransform == void 0) return _rotation;
            return Vector3.prototype.addition(_parentTransform.worldRotation, _rotation);
        });
        this.setWorldRotation = function (value) {
            if(value == void 0) throw new ErrorArgsMissing();
            if(!value.is(Vector3)) throw new ErrorWrongType(Vector3);
            if(_parentTransform == void 0) {
                _rotation.set(value);
            } else {
                var wr = _parentTransform.worldRotation;
                _rotation.x = value.x - wr.x;
                _rotation.y = value.y - wr.y;
                _rotation.z = value.z - wr.z;
            }
        };

        this.__defineGetter__("localRotation", function () {
            return _rotation;
        });
        this.setLocalRotation = function (value) {
            if(value == void 0) throw new ErrorArgsMissing();
            if(!value.is(Vector3)) throw new ErrorWrongType(Vector3);
            _rotation.set(value);

        };

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Transform = Transform.extend(Component);

    function rotateAroundPoint2D(center, point, angle) {
        if(center == void 0 || point == void 0 || angle == void 0) throw new ErrorArgsMissing();
        if(!center.is(Vector2) && !center.is(Vector3) || !point.is(Vector2) && !point.is(Vector3)) throw new ErrorWrongType(Vector3, Vector2);

        angle = angle * Math.deg2rad;

        var rotatedX = Math.cos(angle) * (point.x - center.x) - Math.sin(angle) * (point.y-center.y) + center.x;
        var rotatedY = Math.sin(angle) * (point.x - center.x) + Math.cos(angle) * (point.y - center.y) + center.y;

        return {
            x: rotatedX,
            y: rotatedY
        };
    }

    return Transform;
})();

/*
    // Пример переопределения методов. * Надо бы запилить такую фияу как-нибудь
    this.setLocalPosition = Function;
    redefineFunction(this, "setLocalPosition", [Number, Number, Number], function(x, y, z) {
        return _rotation.set(new Vector(x, y, z));
    });
    redefineFunction(this, "setLocalPosition", [Vector3], function(vector) {
        return _rotation.set(vector);
    });
*/