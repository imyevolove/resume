var SpriteRenderer = (function () {
    // region static properties
    // endregion
    function  SpriteRenderer() {
        // region # private properties
        var $ = this;
        var _sprite;
        // endregion
        // region # constructor
        function constructor() {
            _sprite = null;
        }
        // endregion
        // region # public
        this.draw = function (camera) {
            if(camera == void 0) throw new Error("Camera arg is missing");
            if(!camera.is(Camera)) throw new Error("Camera arg is not Camera type");
            if(_sprite == void 0) return;
            if(_sprite.image == void 0) return;

            var ctx = camera.render.context;

            var worldRotation = $.transform.worldRotation;
            var scale = $.transform.scaleAtParent;

            // Sprite Size at scale
            var spriteWidth, spriteHeight, spriteHWidth, spriteHHeight;
            (function (scale) {
                spriteWidth  = _sprite.rect.width * scale.x * camera.size;
                spriteHeight = _sprite.rect.height * scale.y * camera.size;
                spriteHWidth  = spriteWidth  / 2;
                spriteHHeight = spriteHeight / 2;
            })($.transform.scaleAtParent);

            var renderExtents = camera.render.extents;

            var spritePosition = camera.worldPositionToScreenAtCenter($.transform.worldPosition);
            spritePosition.x -= spriteHWidth;
            spritePosition.y -= spriteHHeight;

            ctx.save();
            ctx.translate(spritePosition.x + renderExtents.x + spriteHWidth, spritePosition.y + renderExtents.y + spriteHHeight);
            ctx.rotate(worldRotation.y * Math.deg2rad);
            ctx.translate(-spritePosition.x - renderExtents.x - spriteHWidth, -spritePosition.y - renderExtents.y - spriteHHeight);

            ctx.drawImage(
                _sprite.image,
                _sprite.rect.x,
                _sprite.rect.y,
                _sprite.rect.width,
                _sprite.rect.height,
                spritePosition.x + renderExtents.x,
                spritePosition.y + renderExtents.y,
                spriteWidth,
                spriteHeight
            );
            ctx.restore();
        };

        this.__defineGetter__("sprite", function () {
            return _sprite;
        });
        this.__defineSetter__("sprite", function (value) {
            if(value == void 0) throw new Error("Arg is missing");
            if(value.is(Sprite)) {
                _sprite = value;
            } else if(value.is(Image)) {
                _sprite = new Sprite();
                _sprite.image = value;
            } else {
                throw new Error("Arg is not Sprite or Image type");
            }
        });

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    SpriteRenderer = SpriteRenderer.extend(Renderer);
    return SpriteRenderer;
})();
