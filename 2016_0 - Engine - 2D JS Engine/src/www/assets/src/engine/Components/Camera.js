var Camera = (function () {
    // region static properties

    // endregion
    function Camera() {
        // region # private properties
        var $ = this;
        var _canvasRender,
            _canvasRenderContext,
            _depth,
            _clearFlag,
            _background,
            _size,
            _snapToPixel;
        // endregion
        // region # constructor
        function constructor() {
            _canvasRender = new CanvasRender();
            _canvasRenderContext = _canvasRender.context;
            _depth = 0;
            _size = 1;
            _snapToPixel = false;
            _clearFlag = Camera.prototype.clearFlags.SolidColor;
            _background = Color.prototype.createFromHex("7AC0DF");
            updateDepthOnCanvas();
            updateSnapToPixel();
        }
        // endregion
        // region # public

        this.start  = function () {
            console.log("Camera Start");
            console.log("Renderer components", Renderer.prototype.getRegisteredComponents());
        };
        this.draw = function () {
            switch(_clearFlag) {
                case Camera.prototype.clearFlags.DontClear:
                    break;
                case Camera.prototype.clearFlags.DepthOnly:
                    _canvasRender.clear();
                    break;
                case Camera.prototype.clearFlags.SolidColor:
                default:
                    _canvasRender.clear();
                    _canvasRender.fillBackground(_background);
                    break;
            }
            var renderComponents = Renderer.prototype.getRegisteredComponents();
            for(var i = 0; i < renderComponents.length; i++) {
                renderComponents[i].draw($);
            }
        };

        // Clear Flag
        this.__defineGetter__("clearFlag", function () {
            return _clearFlag;
        });
        this.__defineSetter__("clearFlag", function (v) {
            if(v == void 0 || !v.isNumber()) return;
            _clearFlag = v;
        });

        // Depth
        this.__defineGetter__("depth", function () {
            return _depth;
        });
        this.__defineSetter__("depth", function (v) {
            if(v == void 0 || !v.isNumber() || v < 0) return;
            _depth = v;
            updateDepthOnCanvas();
        });

        // Size
        this.__defineGetter__("size", function () {
            return _size;
        });
        this.__defineSetter__("size", function (v) {
            if(v == void 0 || !v.isNumber() || v < 0) return;
            _size = v;
        });

        // Background
        this.__defineGetter__("background", function () {
            return _background;
        });
        this.__defineSetter__("background", function (v) {
            if(v == void 0 || !v.is(Color)) return;
            _background = v;
        });

        // Snap to pixel
        this.__defineGetter__("snapToPixel", function () {
            return _snapToPixel;
        });
        this.__defineSetter__("snapToPixel", function (v) {
            if(v == void 0 || !v.isBool()) return;
            _snapToPixel = v;
            updateSnapToPixel();
        });
        this.enableSnapToPixel = function () {
            $.snapToPixel = true;
        };
        this.disableSnapToPixel = function () {
            $.snapToPixel = false;
        };

        this.__defineGetter__("render", function () {
            return _canvasRender;
        });

        this.worldPositionToScreenAtCenter = function (positionVector) {
            if(positionVector == void 0) throw new ErrorArgsMissing();
            if(!positionVector.is(Vector3)) throw new ErrorWrongType(Vector3);
            return new Vector3(
                positionVector.x - $.transform.worldPosition.x,
                -positionVector.y  + $.transform.worldPosition.y,
                0
            );
        };

        // Update canvas depth
        function updateDepthOnCanvas() {
            _canvasRender.canvas.style.zIndex = _depth;
        }

        // Update snap to pixel state
        function updateSnapToPixel() {
            _canvasRenderContext.imageSmoothingEnabled = !_snapToPixel;
        }

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Camera = Camera.extend(RendererComponent);
    Camera.prototype.clearFlags = {
        SolidColor: 0,
        DontClear: 1,
        DepthOnly: 2
    };
    Camera.prototype.destroy = function () {
        console.log("Not defined");
    };
    return Camera;
})();
