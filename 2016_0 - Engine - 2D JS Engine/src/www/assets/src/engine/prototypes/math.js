Math.clamp = function (value, min, max) {
    return Math.max(min, Math.min(max, value))
};
Math.clamp01 = function (value) {
    return Math.max(0, Math.min(1, value))
};
Math.lerp = function (A, B, t) {
    return A + t * (B - A);
};
Math.deg2rad = Math.PI/180;
Math.rad2deg = 180/Math.PI;