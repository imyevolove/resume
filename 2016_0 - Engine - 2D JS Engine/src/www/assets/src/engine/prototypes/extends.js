Function.prototype.extend = function (C) {
    var scope = {
        extend_cl: C, // Extended constructor
        self_cl: this, // Self constructor
        args: [].slice.call(arguments).slice(1) // Slice first args element
    };

    var finaly = new Function("scope",
        "return function " + scope.self_cl.name + "(){ scope.extend_cl.apply(this, scope.args.length <= 0 ? arguments : scope.args); scope.self_cl.apply(this, arguments); }"
    )(scope);

    scope.self_cl.prototype = Object.create(C.prototype);
    scope.self_cl.prototype.constructor = C;

    finaly.prototype = Object.create(this.prototype);
    finaly.prototype.constructor = this;

    return finaly;
};