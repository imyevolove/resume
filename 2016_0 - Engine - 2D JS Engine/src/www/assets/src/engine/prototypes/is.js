Object.prototype.isString = function () {
    return typeof this.valueOf() === "string";
};
Object.prototype.isArray  = function () {
    return Array.isArray(this.valueOf());
};
Object.prototype.isObject = function () {
    var value = this.valueOf();
    return Object.prototype.isPrototypeOf(value) && !Array.isArray(value);
};
Object.prototype.isNumber = function () {
    return typeof this.valueOf() === "number";
};
Object.prototype.isFunction = function () {
    return Function.prototype.isPrototypeOf(this.valueOf());
};
Object.prototype.isBool = function () {
    return typeof this.valueOf() == "boolean";
};
Object.prototype.is = function (_class) {
    if(_class == Number) {
        return ((typeof this.valueOf()) == "number");
    } else if(_class == String) {
        return ((typeof this.valueOf()) == "string");
    }
    return _class.prototype.isPrototypeOf(this.valueOf());
};