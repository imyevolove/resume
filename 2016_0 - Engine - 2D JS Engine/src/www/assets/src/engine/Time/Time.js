var Time = (function () {
    // region static properties
    // endregion
    function  Time() {
        // region # private properties
        var _update;
        var _deltaTime = 0;
        var _lastUpdate;
        // endregion
        // region # constructor
        function constructor() {
            _update = UpdateManager.registerUpdateOnLevel("logic");
            _lastUpdate = Date.now();

            _update.callUpdate = updateDeltaTime;
        }
        // endregion
        // region # public
        this.__defineGetter__("deltaTime", function () {
            return _deltaTime;
        });
        // endregion
        // region # private functions
        function updateDeltaTime() {
            var now = Date.now();
            _deltaTime = (now - _lastUpdate) * 0.001;
            _lastUpdate = now;
        }
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return new Time();
})();