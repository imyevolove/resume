var GameObject = (function () {
    // region static properties
    var gameObjects = [];
    // endregion
    function  GameObject(tag) {
        // region # private properties
        var $ = this;
        var _tag,
            _components,
            _flagActive,
            _transformComponent;
        // endregion
        // region # constructor
        function constructor() {
            _tag = (tag != void 0 && tag.isString()) ? tag : "GameObject";
            _components = [];
            gameObjects.push(this);
            _transformComponent = $.addComponent(Transform, ComponentFlags.FIXED);
        }
        // endregion
        // region # public

        // Name
        this.__defineGetter__("tag", function () {
            return _tag;
        });
        this.__defineSetter__("tag", function (v) {
            if(v == void 0 || !v.isString()) return;
            _tag = v;
        });

        // Transform
        this.__defineGetter__("transform", function () {
            return _transformComponent;
        });

        //
        this.__defineGetter__("isActive", function () {
            return  _flagActive;
        });
        this.setActive = function (flag) {
            if(flag == void 0 || !flag.isBool()) return;
            _flagActive = flag;
        };

        // region # components kit
        // Component add
        this.addComponent = function (component, componentFlag) {
            if(component == void 0 || !component.prototype.is(Component)) return;
            if($.getComponent(component) != void 0) return;

            component = new component(this);
            _components.push(component);

            if(componentFlag != void 0)
                component.componentFlag = componentFlag;

            component.transform = (component.is(Transform) ? component : _transformComponent)

            if(component.start != void 0 && component.start.isFunction())
                component.start();

            return component;
        };

        // Component get
        this.getComponent = function (componentFunction) {
            if(componentFunction == void 0 || !componentFunction.isFunction()) return null;
            for(var i in _components)
                if(_components[i].is(componentFunction)) return _components[i];
            return null;
        };

        // Component remove
        this.removeComponent = function (componentFunction) {
            if(componentFunction == void 0 || !componentFunction.isFunction()) return false;
            for(var i in _components) {
                if(_components[i].is(componentFunction)) {
                    if(_components[i].componentFlag != ComponentFlags.FIXED) {
                        _components.slice(i, 1);
                        return true;
                    } else {
                        Console.Error("You can't delete this component. Component have componentFlag:FIXED");
                        return false;
                    }
                }
            }
            return false;
        };
        // endregion

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    GameObject.prototype.destroy = function () {
        throw new Error("not defined");
    };
    return GameObject;
})();