var Vector2 = (function () {
    // region static properties
    // endregion
    function  Vector2(x, y) {
        // region # private properties
        var $ = this;
        // endregion
        // region # constructor
        function constructor() {
            if(x == void 0 || !x.isNumber()) x = 0;
            if(y == void 0 || !y.isNumber()) y = 0;
        }
        // endregion
        // region # public

        // X
        this.__defineGetter__("x", function () { return x; });
        this.__defineSetter__("x", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            x = value;
        });

        // Y
        this.__defineGetter__("y", function () { return y; });
        this.__defineSetter__("y", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            y = value;
        });

        this.clone = function () {
            return new Vector2(x, y);
        };

        this.set = function (vector) {
            if(vector == void 0) throw new ErrorArgsMissing();
            if(!vector.is(Vector2)) throw new ErrorWrongType(Vector2);

            x = vector.x;
            y = vector.y;
        };

        this.__defineGetter__("length", function () {
            return Math.sqrt(x + y);
        });
        this.__defineGetter__("normalized", function () {
            var ln = $.length;
            return new Vector2(x / ln, y / ln);
        });

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Vector2.prototype.__defineGetter__("one", function () {
        return new Vector2(1, 1);
    });
    Vector2.prototype.__defineGetter__("zero", function () {
        return new Vector2(0, 0);
    });
    Vector2.prototype.addition = function (vectorA, vectorB) {
        return new Vector2(
            vectorA.x + vectorB.x,
            vectorA.y + vectorB.y
        );
    };
    Vector2.prototype.subtract = function (vectorA, vectorB) {
        return new Vector2(
            vectorA.x - vectorB.x,
            vectorA.y - vectorB.y
        );
    };
    Vector2.prototype.multiply = function (vectorA, vectorB) {
        return new Vector2(
            vectorA.x * vectorB.x,
            vectorA.y * vectorB.y
        );
    };
    return Vector2;
})();