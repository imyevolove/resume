var Vector3 = (function () {
    // region static properties
    // endregion
    function  Vector3(x, y, z) {
        // region # private properties
        var $ = this;
        // endregion
        // region # constructor
        function constructor() {
            if(x == void 0 || !x.isNumber()) x = 0;
            if(y == void 0 || !y.isNumber()) y = 0;
            if(z == void 0 || !z.isNumber()) z = 0;
        }
        // endregion
        // region # public

        // X
        this.__defineGetter__("x", function () { return x; });
        this.__defineSetter__("x", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            x = value;
        });

        // Y
        this.__defineGetter__("y", function () { return y; });
        this.__defineSetter__("y", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            y = value;
        });

        // Z
        this.__defineGetter__("z", function () { return z; });
        this.__defineSetter__("z", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            z = value;
        });

        this.clone = function () {
            return new Vector3(x, y, z);
        };

        this.set = function (vector) {
            if(vector == void 0) throw new ErrorArgsMissing();
            if(!vector.is(Vector3)) throw new ErrorWrongType(Vector3);

            x = vector.x;
            y = vector.y;
            z = vector.z;
        };

        this.__defineGetter__("length", function () {
            return Math.sqrt(x + y + z);
        });
        this.__defineGetter__("normalized", function () {
            var ln = $.length;
            return new Vector3(x / ln, y / ln, z / ln);
        });

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Vector3.prototype.__defineGetter__("one", function () {
        return new Vector3(1, 1, 1);
    });
    Vector3.prototype.__defineGetter__("zero", function () {
        return new Vector3(0, 0, 0);
    });
    Vector3.prototype.addition = function (vectorA, vectorB) {
        return new Vector3(
            vectorA.x + vectorB.x,
            vectorA.y + vectorB.y,
            vectorA.z + vectorB.z
        );
    };
    Vector3.prototype.subtract = function (vectorA, vectorB) {
        return new Vector3(
            vectorA.x - vectorB.x,
            vectorA.y - vectorB.y,
            vectorA.z - vectorB.z
        );
    };
    Vector3.prototype.multiply = function (vectorA, vectorB) {
        return new Vector3(
            vectorA.x * vectorB.x,
            vectorA.y * vectorB.y,
            vectorA.z * vectorB.z
        );
    };
    return Vector3;
})();