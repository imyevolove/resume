var Rect = (function () {
    // region static properties
    // endregion
    function  Rect(x, y, width, height) {
        // region # private properties

        // endregion
        // region # constructor
        function constructor() {
            if(x == void 0 || !x.isNumber()) x = 0;
            if(y == void 0 || !y.isNumber()) y = 0;
            if(width == void 0 || !width.isNumber()) width = 0;
            if(height == void 0 || !height.isNumber()) height = 0;
        }
        // endregion
        // region # public

        // X
        this.__defineGetter__("x", function () { return x; });
        this.__defineSetter__("x", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            x = value;
        });

        // Y
        this.__defineGetter__("y", function () { return y; });
        this.__defineSetter__("y", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            y = value;
        });

        // Width
        this.__defineGetter__("width", function () { return width; });
        this.__defineSetter__("width", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            width = value;
        });

        // Height
        this.__defineGetter__("height", function () { return height; });
        this.__defineSetter__("height", function (value) {
            if(value == void 0 || !value.isNumber()) return;
            height = value;
        });

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return Rect;
})();