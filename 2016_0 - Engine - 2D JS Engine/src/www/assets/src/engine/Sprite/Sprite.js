var Sprite = (function () {
    // region static properties
    // endregion
    function  Sprite() {
        // region # private properties
        var _image,
            _rect;
        // endregion
        // region # constructor
        function constructor() {
            _image = null;
            _rect = new Rect(0, 0, 0, 0);
        }
        // endregion
        // region # public

        // Image
        this.__defineGetter__("image", function () {
            return _image;
        });
        this.__defineSetter__("image", function (value) {
            if(value == void 0) throw new Error("Arg is missing");
            if(!value.is(Image) && !value.is(HTMLImageElement)) throw new Error("Arg is not Image type");

            _image = value;
            updateRectByFullImage();
        });

        // Rect
        this.__defineGetter__("rect", function () {
            return _rect;
        });
        
        // endregion
        // region # private functions
        function updateRectByFullImage() {
            if(_image == void 0) return;

            _rect.x = _rect.y = 0;
            _rect.width  = _image.width;
            _rect.height = _image.height;
        }
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return Sprite;
})();