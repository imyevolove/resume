var Renderer = (function () {
    // region static properties
    var _components = [];
    // endregion
    function  Renderer() {
        // region # private properties
        var $ = this;
        // endregion
        // region # constructor
        function constructor() {
            _components.push(this);
            this.destroy = function () {
                _components.splice(_components.indexOf(this, 1));
            };
        }
        // endregion
        // region # public
        this.draw = function () {};

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Renderer = Renderer.extend(Component);
    Renderer.prototype.getRegisteredComponents = function () {
        return _components;
    };
    return Renderer;
})();