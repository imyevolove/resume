var Component = (function () {
    // region static properties

    // endregion
    function Component(gameObject) {
        if(gameObject == void 0 || !gameObject.is(GameObject))
            throw new Error("Component arg gameObject is void 0 or not GameObject");

        // region # private properties
        var $ = this;
        var _flagEnabled = false;
        // endregion
        // region # constructor
        function constructor() {

        }
        // endregion
        // region # public

        /* Callbacks */
        this.onEnable = function () {};
        this.onDisable = function () {};

        this.__defineGetter__("gameObject", function () {
            return gameObject;
        });

        //
        this.__defineGetter__("enabled", function () {
            return _flagEnabled;
        });
        this.__defineSetter__("enabled", function (flag) {
            if(flag == void 0 || !flag.isBool()) return;
            if($.componentFlag == ComponentFlags.FIXED) throw new Error("Can't enable/disable fixed component");
            if(_flagEnabled != flag) {
                _flagEnabled = flag;
                // Call state change Events
                if(_flagEnabled && $.onEnable != void 0 && $.onEnable.isFunction()) $.onEnable();
                else if(!_flagEnabled && $.onDisable != void 0 && $.onDisable.isFunction()) $.onDisable();
            }
        });

        this.enable = function () {
            $.enabled = true;
        };
        this.disable = function () {
            $.enabled = false;
        };

        this.componentFlag = ComponentFlags.FIXED;
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Component.prototype.destroy = function () {
        throw new Error("not defined");
    };
    return Component;
})();