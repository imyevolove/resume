var RendererComponent = (function () {
    // region static properties
    var _components = [];
    // endregion
    function  RendererComponent(gameObject) {
        // region # private properties
        var $ = this;
        var _update;
        // endregion
        // region # constructor
        function constructor() {
            _components.push(this);
            _update = UpdateManager.registerUpdateOnLevel(UpdateManager.levels.render);
            _update.callUpdate = function () {
                $.draw();
            };

            this.destroy = function () {
                UpdateManager.destroyUpdateOnLevel(UpdateManager.levels.render, _update);
                _components.splice(_components.indexOf(this, 1));
            };
        }
        // endregion
        // region # public

        this.draw = function () {};

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    RendererComponent = RendererComponent.extend(Component);
    RendererComponent.prototype.getRegisteredComponents = function () {
        return _components;
    };
    return RendererComponent;
})();
