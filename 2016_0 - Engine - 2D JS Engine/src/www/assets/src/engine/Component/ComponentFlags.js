var ComponentFlags = (function () {
    // region static properties
    // endregion
    function  ComponentFlags() {
        // region # private properties

        // endregion
        // region # constructor
        function constructor() {}
        // endregion
        // region # public
        this.DEFAULT = 1 << 0;
        this.FIXED = 1 << 1;
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return new ComponentFlags();
})();