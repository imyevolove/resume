var Resources = (function () {
    // region static properties
    // endregion
    function  Resources() {
        // region # private properties
        var _cache = {};
        // endregion
        // region # constructor
        function constructor() {}
        // endregion
        // region # public
        this.load = function (type, path, callback) {
            // Checking for null
            if(type == void 0) throw new Error("You must specify the type of downloadable resource");
            if(path == void 0) throw new Error("You must specify the path to the downloaded resource");

            // Checking for types
            if(!type.isFunction()) throw new Error("Arg resource type must be a function");
            if(!path.isString()) throw new Error("Arg resource path must be a string");

            var loader = null;
            switch (type) {
                case Image:
                    loader = imgLoad;
                    break;
                case Text:
                    loader = xhrLoad;
                    break;
            }

            if(loader == void 0) throw new Error("Unknown type");
            loader(path, function (status, result) {
                if(status) {
                    _cache[path] = result;
                    if(callback != void 0 && callback.isFunction()) callback(result);
                } else if(callback != void 0 && callback.isFunction()) callback(null);
            });
        };
        // endregion
        // region # private function
        function xhrLoad(uri, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if(xhr.readyState == 4) {
                    if(xhr.status == 200) {
                        callback(true, xhr.responseText);
                    } else {
                        callback(false);
                    }
                }
            };
            xhr.open("GET", uri);
            xhr.send();
        }
        function imgLoad(uri, callback) {
            var img = new Image();
            img.src = uri;
            img.onload = function () {
                callback(true, img);
            };
            img.onerror = function () {
                callback(false);
            };
        }
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return new Resources();
})();