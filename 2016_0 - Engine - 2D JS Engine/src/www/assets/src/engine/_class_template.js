var CLASS_NAME = (function () {
    // region static properties
    // endregion
    function  CLASS_NAME() {
        // region # private properties

        // endregion
        // region # constructor
        function constructor() {}
        // endregion
        // region # public

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return CLASS_NAME;
})();