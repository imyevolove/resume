var Color = (function () {
    // region static properties
    // endregion
    function  Color(r, g, b, a) {
        // region # private properties
        var _r , _g, _b, _a;
        // endregion
        // region # constructor
        function constructor() {
            _r = (r != void 0 && r.isNumber()) ? Math.clamp01(r) : 1;
            _g = (g != void 0 && g.isNumber()) ? Math.clamp01(g) : 1;
            _b = (b != void 0 && b.isNumber()) ? Math.clamp01(b) : 1;
            _a = (a != void 0 && a.isNumber()) ? Math.clamp01(a) : 1;
        }
        // endregion
        // region # public

        // Red
        this.__defineGetter__("r", function () { return _r; });
        this.__defineSetter__("r", function (v) {
            if(v == void 0 || !v.isNumber()) return;
            _r = Math.clamp01(v);
        });

        // Green
        this.__defineGetter__("g", function () { return _g; });
        this.__defineSetter__("g", function (v) {
            if(v == void 0 || !v.isNumber()) return;
            _g = Math.clamp01(v);
        });

        //Blue
        this.__defineGetter__("b", function () { return _b; });
        this.__defineSetter__("b", function (v) {
            if(v == void 0 || !v.isNumber()) return;
            _b = Math.clamp01(v);
        });

        // Alpha
        this.__defineGetter__("a", function () { return _a; });
        this.__defineSetter__("a", function (v) {
            if(v == void 0 || !v.isNumber()) return;
            _a = Math.clamp01(v);
        });
        
        // Hex
        this.__defineGetter__("hex", function () {
            return convertColorSegmentToHex(_r) + convertColorSegmentToHex(_g) + convertColorSegmentToHex(_b);
        });

        // Convert color float segment to color hex string segment
        function convertColorSegmentToHex(value) {
            var h = (Math.floor(value * 255)).toString(16);
            return (h.length == 1 ? "0" + h : h);
        }
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    Color.prototype.createFromHex = function (hex) {
        if(hex == void 0 || !hex.isString()) throw new Error("Hex value is not string");
        if(hex.length != 6) throw new Error("Hex value must contain 6 character");
        return new Color(
            parseFloat((parseInt(hex.substr(0, 2), 16) / 255).toFixed(1)),
            parseFloat((parseInt(hex.substr(2, 2), 16) / 255).toFixed(1)),
            parseFloat((parseInt(hex.substr(4, 2), 16) / 255).toFixed(1)),
            1
        );
    };
    Color.prototype.white = Color.prototype.createFromHex("ffffff");
    Color.prototype.black = Color.prototype.createFromHex("000000");
    Color.prototype.red = Color.prototype.createFromHex("ff0000");
    Color.prototype.green = Color.prototype.createFromHex("00ff00");
    Color.prototype.blue = Color.prototype.createFromHex("0000ff");
    Color.prototype.yellow = Color.prototype.createFromHex("ffff00");
    Color.prototype.aqua = Color.prototype.createFromHex("00ffff");
    Color.prototype.fuchsia = Color.prototype.createFromHex("ff00ff");
    return Color;
})();