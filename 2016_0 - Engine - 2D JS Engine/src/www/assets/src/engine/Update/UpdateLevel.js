var UpdateLevel = (function () {
    // endregion
    function  UpdateLevel() {
        var listeners;
        // region # private properties

        // endregion
        // region # constructor
        function constructor() {
            listeners = [];
        }
        // endregion
        // region # public

        this.registerUpdateListener = function() {
            var update = new Update();
            listeners.push(update);
            return update;
        };

        this.destroyUpdateListener = function (updateListener) {
            if(updateListener == void 0 || !updateListener.is(Update)) return;
            listeners.splice(listeners.indexOf(updateListener), 1);
        };

        this.callUpdates = function () {
            for(var i = 0; i < listeners.length; i++) {
                listeners[i].callUpdate();
            }
        };

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return UpdateLevel;
})();