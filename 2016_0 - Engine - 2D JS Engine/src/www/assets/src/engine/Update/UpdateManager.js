var UpdateManager = (function () {
    // region static properties
    // endregion
    function UpdateManager() {
        // region # private properties
        var $ = this;
        var _animationFramePtr,
            _animationFrameTimeoutPtr,
            _requestAnimationFrame,
            _cancelAnimationFrame;
        var _frameLimit = 0,
            _frameMS = 0,
            _launched = false;
        var _updateLevels = {},
            _updateLevelKeys = [];
        // endregion
        // region # constructor
        function constructor() {
            $.frameLimit = 60;
            _updateLevelKeys = Object.keys(UpdateManager.prototype.levels);
            for(var i = 0; i < _updateLevelKeys.length; i++)
                _updateLevels[_updateLevelKeys[i]] = new UpdateLevel();

            initializeAnimationFrame.call(this);
            $.stop(); //
        }
        // endregion
        // region # public

        this.__defineGetter__("launched", function () { return _launched; });
        this.__defineGetter__("passIDs", function () { return _updateLevelKeys; });

        this.__defineGetter__("frameLimit", function () { return _frameLimit; });
        this.__defineSetter__("frameLimit", function (value) {
            if(value == void 0 || !value.isNumber() || value < 0) return;
            _frameLimit = value;
            _frameMS = parseInt(1000 / _frameLimit);
            if(_launched) $.resume(); // Stop and resume now
        });

        this.registerUpdateOnLevel = function (levelID) {
            if(_updateLevels[levelID] == void 0) throw new Error("Pass not found");
            return _updateLevels[levelID].registerUpdateListener();
        };
        this.destroyUpdateOnLevel = function (levelID, update) {
            if(_updateLevels[levelID] == void 0) throw new Error("Pass not found");
            _updateLevels[levelID].destroyUpdateListener(update);
        };

        this.stop = function () {
            _cancelAnimationFrame(_animationFramePtr);
            clearTimeout(_animationFrameTimeoutPtr);
            _launched = false;
        };
        this.resume = function () {
            if(_launched) $.stop();
            _launched = true;
            resumeAnimationFrame();
        };
        // endregion
        // region # inside function

        // Bind animation frame methods
        function initializeAnimationFrame() {
            _requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
            _cancelAnimationFrame  = window.cancelAnimationFrame || window.mozCancelAnimationFrame;
        }

        function resumeAnimationFrame() {
            updateAnimationFrame();
        }
        
        // Updating animation
        function updateAnimationFrame() {
            for(var i = 0; i < _updateLevelKeys.length; i++)
                _updateLevels[_updateLevelKeys[i]].callUpdates();
            _animationFrameTimeoutPtr = setTimeout(function () {
                _animationFramePtr = _requestAnimationFrame(updateAnimationFrame);
            }, 1000 / _frameLimit);
        }
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    UpdateManager.prototype.levels = {
        physics: "physics",
        logic: "logic",
        render: "render"

    };
    return new UpdateManager();
})();