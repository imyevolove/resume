var Update = (function () {
    // endregion
    function  Update() {
        // region # private properties

        // endregion
        // region # constructor
        function constructor() {

        }

        // endregion
        // region # public
        this.callUpdate = function () {
            console.log("default call update function");
        };
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return Update;
})();