var SortingLayer = (function () {
    // region static properties
    var layers = [];
    // endregion
    function  SortingLayer() {
        // region # private properties

        // endregion
        // region # constructor
        function constructor() {
            layers.push(this);
        }
        // endregion
        // region # public

        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    SortingLayer = SortingLayer.extend(Layer);

    SortingLayer.prototype.__defineGetter__("layers", function () {
        return layers;
    });
    return SortingLayer;
})();