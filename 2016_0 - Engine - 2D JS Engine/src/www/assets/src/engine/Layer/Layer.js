var Layer = (function () {
    // region static properties
    // endregion
    function  Layer(name, value) {
        // region # private properties
        var _name  = "";
        var _value = 0;
        // endregion
        // region # constructor
        function constructor() {
            _value = (value != void 0 && value.isNumber()) ? value : 0;
            _name = (name != void 0 && name.isString()) ? name : "New Layer";
        }
        // endregion
        // region # public
        this.__defineSetter__("value", function (v) {
            if(v == void 0 || !v.isNumber() || v < 0) return;
            _value = v;
        });
        this.__defineGetter__("value", function () {
            return _value;
        });
        this.__defineSetter__("name", function (v) {
            if(v == void 0 || !v.isString()) return;
            _name = name;
        });
        this.__defineGetter__("name", function () {
            return _name;
        });
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return Layer;
})();