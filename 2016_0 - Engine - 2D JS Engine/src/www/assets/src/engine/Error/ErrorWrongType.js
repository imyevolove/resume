function ErrorWrongType(type) {
    var types = [].slice.call(arguments);
    var typesNames = [];
    for(var i = 0; i < types.length; i++) {
        if((types[i] != void 0 && types[i].isFunction()))
            typesNames.push(types[i].name);
    }


    this.name = "ErrorWrongType";
    this.message = "Wrong type." + ((typesNames.length > 0) ? (" Required type " + typesNames.join(" or ")) : "");
}
ErrorWrongType.prototype = Object.create(Error.prototype);
ErrorWrongType.prototype.constructor = ErrorWrongType;