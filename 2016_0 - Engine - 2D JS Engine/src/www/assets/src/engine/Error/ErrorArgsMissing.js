function ErrorArgsMissing(message) {
    this.name = "ErrorArgsMissing";
    this.message = message || "Arg is missing";
}
ErrorArgsMissing.prototype = Object.create(Error.prototype);
ErrorArgsMissing.prototype.constructor = ErrorArgsMissing;