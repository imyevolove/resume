var utility = utility || {};
utility.fps = (function () {
    // region static properties
    // endregion
    function  Fps() {
        var $ = this;
        // region # private properties
        var _fpsElement,
            _update,
            _fps;
        // endregion
        // region # constructor
        function constructor() {
            _fps = 0;

            _fpsElement = document.createElement("span");
            _fpsElement.setAttribute("style", "z-index:1000000;position:fixed;left:10px;top:10px;padding:8px 12px;font-size:14px;font-weight:bold;background:black;color:white");

            _update = UpdateManager.registerUpdateOnLevel(UpdateManager.levels.logic);
            _update.callUpdate = function () {
                _fpsElement.innerHTML = $.fpsCount;
            };
        }
        // endregion
        // region # public
        this.show = function () {
            if(_fpsElement.parentElement != void 0) return;
            document.body.appendChild(_fpsElement);
        };
        this.hide = function () {
            if(_fpsElement.parentElement == void 0) return;
            _fpsElement.parentElement.removeChild(_fpsElement);
        };
        this.__defineGetter__("fpsCount", function () {
            _fps =Math.lerp(_fps, 1 / Time.deltaTime, Time.deltaTime * 2);
            return parseInt(_fps);
        });
        // endregion
        // region # initializing
        constructor.call(this);
        // endregion
    }
    return new Fps();
})();