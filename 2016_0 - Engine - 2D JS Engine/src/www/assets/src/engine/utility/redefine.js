function redefine(scope, name, args, body) {
    if(scope == void 0) throw new ErrorArgsMissing("Scope is missing");
    if(name == void 0) throw new ErrorArgsMissing("Name is missing");
    if(args == void 0) throw new ErrorArgsMissing("Args is missing");
    if(body == void 0) throw new ErrorArgsMissing("Body is missing");
    if(!scope.isObject()) throw new ErrorWrongType(Object);
    if(!name.isString()) throw new ErrorWrongType(String);
    if(!args.isArray()) throw new ErrorWrongType(Array);
    if(!body.isFunction()) throw new ErrorWrongType(Function);
    for(var a = 0; a < args.length; a++)
        if(args[a] == void 0 || !args[a].isFunction())
            throw new ErrorWrongType(Function);

    scope[name] = function () {
        if(arguments.length != args.length) throw new ErrorArgsMissing("Args count is wrong");
        for(var i = 0; i < arguments.length; i++) {
            if(!arguments[i].is(args[i])) throw new ErrorWrongType(args[i]);
        }
        body.apply(null, arguments);
    }
}