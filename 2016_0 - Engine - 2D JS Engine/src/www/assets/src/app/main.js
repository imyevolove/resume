utility.fps.show();
UpdateManager.resume();

var gameObjectCamera = new GameObject("Camera");
var camera = gameObjectCamera.addComponent(Camera);
camera.enableSnapToPixel();

var sprite1, sprite2;

Resources.load(Image, "/assets/resources/images/sprite.png", function(image) {
    if(image == void 0) return;

    sprite1 = createSprite(image);
    sprite1.transform.scale = new Vector3(0.5, 0.5, 0.5);
    sprite1.transform.rotation.y = 0;
    sprite1.transform.position.x = 0;
    sprite1.transform.position.y = 0;

    sprite2 = createSprite(image);
    sprite2.transform.rotation.y = 0;
    sprite2.transform.position.x = 100;
    sprite2.transform.position.y = 100;
    sprite2.transform.setParent(sprite1.transform);

    setInterval(function() {
        sprite1.transform.rotation.y += 1;

    }, 1000/60);
});

function createSprite(image) {
    var sprite = new GameObject("Sprite");
    var spriteRenderer = sprite.addComponent(SpriteRenderer);

    spriteRenderer.sprite = image;
    return sprite;
}