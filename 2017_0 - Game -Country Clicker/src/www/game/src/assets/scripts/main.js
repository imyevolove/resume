// Main game enter point
(function()
{
    var appsInitializer = Application.initializeApps;
    Application.initializeApps = null;

    waterfall.create([
        function (next)
        {
            assets.include("/assets/asset.json");
            assets.listenLoadEndLastAddedAssets(function () {
                console.log("Assets loaded");
                next();
            });
        },

        /// Require application scripts
        function (next)
        {
            assets.requireScripts(next);
        },

        /// Authorization
        function (next)
        {
            appsInitializer();
            next();
        }
    ]);
})();