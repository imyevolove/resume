var LeaderBoardScreenBehaviour = (function () {

    function LeaderBoardScreenBehaviour()
    {
        var behaviour_that = this;
        var leaderboardUIItems      = [],
            leaderboardFreeUIItems  = [];

        var m_ItemTemplate = new Template();
        m_ItemTemplate.setText(resources.get("resources/tpl.leaderboard_item.html"));

        this.extend(ScreenBehaviour, arguments);

        this.rootDOMElement                 = xDom.get("leaderboard-root");
        this.itemsContainerDOMElement       = xDom.get("leaderboard-items-container");
        this.controlsContainerDOMElement    = xDom.get("leaderboard-controls-container");
        this.titleDOMElement                = xDom.get("leaderboard-label-title");
        this.closeDOMButton                 = xDom.get("leaderboard-close-button");

        var m_Scroll = new Scroll(this.itemsContainerDOMElement);

        m_Scroll.scrollElement.containerElement.style.bottom = "25px";
        m_Scroll.scrollElement.containerElement.style.right = "2px";
        m_Scroll.scrollElement.scrollElement.style.backgroundColor = "rgba(255, 255, 255, .4)";

        var m_LeaderboardControlsManager = new LeaderboardControlsManager();
        m_LeaderboardControlsManager.addControl("country", "/assets/media/icons/earth.png", function (item, data) {
            item.setIcon("/assets/media/flags/" + data.code + ".png");
            item.setTitle(localization.get("country." + data.id));
            item.setSubTitle(valueShorter.short(data.players_count || 0, 2)  + " " + localization.get("players_title"));
            item.setBalanceValue(data.balance, data.balance_level || 0);

            if(xData.country.getId() == data.id) item.moveToFirstPosition();
        });
        m_LeaderboardControlsManager.addControl("player", "/assets/media/icons/user.png", function (item, data) {
            item.setIcon("/assets/media/flags/" + data.country.code + ".png");
            item.setTitle(data.name);
            item.setSubTitle(localization.get("country." + data.country.id) + " / " + data.platform_name);
            item.setBalanceValue(data.statistics.balance || 0, data.statistics.balance_level || 0);

            if(xData.player.id == data.id) item.moveToFirstPosition();
        });
        m_LeaderboardControlsManager.action = function (control) { requestLeaderboard(control.id); };

        var m_OriginalShowFunction = this.show;
        this.show = function ()
        {
            m_LeaderboardControlsManager.action(m_LeaderboardControlsManager.getActiveControl());
            m_OriginalShowFunction();
        };

        initialization.call(this);
        function initialization()
        {
            initializeViewEvents();

            ///
            behaviour_that.hide();
            behaviour_that.closeDOMButton.on("click", behaviour_that.hide);
        }

        function initializeViewEvents()
        {
            initializeControlsView();

            localization.addOnChangeEventListener(updateLocalization);
            updateLocalization  ();
        }

        function LeaderboardControlsManager()
        {
            var
                manager_this = this,
                m_Action = function () {},
                m_Controls = [],
                m_CurrentControl = undefined;

            this.containerElement = undefined;

            this.__defineGetter__("action", function () { return m_Action; });
            this.__defineSetter__("action", function (action) {
                if(!type.isFunction(action)) throw new Error("Invalid argument type; Action must be a function");
                m_Action = action;
            });

            this.addControl = function (id, iconUrl, controlAction)
            {
                if(!type.isString(id)) throw new Error("Invalid arg type;");
                if(m_Controls.some(function (i) { return i.id == id;})) throw new Error("Control id '{0}' already exists".format(id));

                var control = new Control(id, iconUrl, controlAction);
                control.execute = controlAction;

                m_Controls.push(control);

                if(!m_CurrentControl)
                    manager_this.setActiveControl(control);
            };

            this.setActiveControl = function (control)
            {
                if(!type.is(control, Control)) throw new Error("Invalid argument type;");
                if(m_Controls.indexOf(control) < 0) throw new Error("Control not registered");

                if(control == m_CurrentControl)
                {
                    console.warn("Control already active");
                    return;
                }

                if(!!m_CurrentControl) m_CurrentControl.applyActiveStyle(false);
                control.applyActiveStyle(true);

                m_CurrentControl = control;
                m_Action(m_CurrentControl);
            };

            this.getActiveControl = function ()
            {
                return m_CurrentControl;
            };

            function Control(id, iconUrl, renderItemFunction)
            {
                var control_this = this;

                var button = createControlButton(iconUrl, behaviour_that.controlsContainerDOMElement);
                button.on("click", function () { manager_this.setActiveControl(control_this); });

                this.__defineGetter__("id", function () { return id; });
                this.__defineGetter__("icon", function () { return iconUrl; });
                this.__defineGetter__("buttonElement", function () { return button; });


                this.renderItem = type.isFunction(renderItemFunction) ? renderItemFunction : function(item, data) {};

                this.applyActiveStyle = function (isActive)
                {
                    isActive
                        ? button.setAttribute("active", "true")
                        : button.removeAttribute("active");
                }
            }

            function createControlButton(iconUrl, parent)
            {
                var button = document.createElement("div");
                    button.className = "_control";
                    button.style.backgroundImage = "url(" + iconUrl + ")";

                parent.appendChild(button);
                return button
            }
        }

        function initializeControlsView()
        {

        }

        function updateLocalization()
        {
            behaviour_that.titleDOMElement.innerHTML = localization.get("leaderboard_title");
        }

        function requestLeaderboard(leaderboardType, callback)
        {
            xGame.screens.loading.show();

            leaderboard.type = leaderboardType;
            leaderboard.update(function (result) {
                xGame.screens.loading.hide();

                if(!result) return;

                clear();

                var
                    control = m_LeaderboardControlsManager.getActiveControl(),
                    item;

                for(var i = 0; i < result.length; i++)
                {
                    item = getUIItem();
                    item.show();

                    item.setRatingIndex(i + 1);
                    item.getElement().setAttribute("rating", (i + 1).toString());

                    control.renderItem(item, result[i]);

                    /// Sticker control
                    item.sticker[i > 0 ? "disable" : "enable"]();
                }

                if(typeof callback == "function") callback();
                m_Scroll.scrollToUp();
            })
        }

        function clear()
        {
            leaderboardFreeUIItems = [];
            for(var i = 0; i < leaderboardUIItems.length; i++)
            {
                leaderboardUIItems[i].hide();
                leaderboardFreeUIItems.push(leaderboardUIItems[i]);
            }
        }

        function getUIItem()
        {
            return leaderboardFreeUIItems.pop() || createItem();
        }

        function createItem()
        {
            var item = new UIItem(behaviour_that.itemsContainerDOMElement);

            leaderboardUIItems.push(item);
            return item;
        }

        function UIItem(parent)
        {
            var content = m_ItemTemplate.createContent();
            var element = content.children[0];

            this.sticker = new Sticker();
            this.sticker.parent = element.querySelector("._icon");
            this.sticker.imageSrc = "/assets/media/icons/crown2.png";
            this.sticker.disable();

            this.moveToFirstPosition = function ()
            {
                var prnt = element.parentElement;
                if(prnt && prnt.children.length > 0 && prnt.children[0] != element)
                {
                    prnt.insertBefore(element, prnt.children[0]);
                }
            };

            this.show = function ()
            {
                parent.appendChild(element);
            };

            this.hide = function ()
            {
                element.delete();
            };

            this.getElement = function ()
            {
                return element;
            };

            this.setRatingIndex = function (value)
            {
                element.querySelector(".lbi-section-position").innerHTML = value;
            };

            this.setTitle = function (value)
            {
                element.querySelector("._country-name").innerHTML = value;
            };

            this.setSubTitle = function (value)
            {
                element.querySelector("._country-players").innerHTML = value;
            };

            this.setBalanceValue = function (value, level)
            {
                element.querySelector(".lbi-section-balance").innerHTML = valueShorter.short(value, 2)
                    + " <span class='golden-type'>" + valueShorter.getBIGINTIndexCharacter(level) + "</span>";
            };

            this.setIcon = function (url)
            {
                element.querySelector("._icon").style.backgroundImage = "url(" + url + ")";
            };
        }

        /// Sticker / Usage for decoration
        function Sticker()
        {
            var m_StickerElement = document.createElement("div");
            m_StickerElement.className = "sticker";

            var m_Parent = undefined;
            var m_ImageSrc = "";

            this.defineGetterAndSetter(
                "parent",
                function () { return m_Parent; },
                function (element) {
                    if(!type.isHTMLElement(element)) throw new TypeError();
                    if(element == m_StickerElement) throw new Error();

                    m_Parent = element;
                });

            this.defineGetterAndSetter(
                "imageSrc",
                function () { return m_ImageSrc; },
                function (src) {
                    if(!type.isString(src)) throw new TypeError();

                    m_ImageSrc = src;
                    updateImage();
                });

            this.enable     = function () { m_StickerElement.appendTo(m_Parent); };
            this.disable    = function () { m_StickerElement.delete(); };

            function updateImage()
            {
                m_StickerElement.style.backgroundImage = "url(" + m_ImageSrc + ")";
            }
        }
    }

    return LeaderBoardScreenBehaviour;

})();