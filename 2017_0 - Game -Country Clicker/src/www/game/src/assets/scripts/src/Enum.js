var Enum = function (properties)
{
    if(!type.isTryObject(properties)) throw new Error("Invalid argument type");

    for(var key in properties)
    {
        if(!type.isNumber(properties[key]) && !type.isString(properties[key]))
            throw new Error("Enum value must be a string or number");
        this.defineReadonlyProperty(key, properties[key]);
    }
};