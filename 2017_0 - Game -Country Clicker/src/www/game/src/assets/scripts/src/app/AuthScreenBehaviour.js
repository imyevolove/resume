var AuthScreenBehaviour = (function () {

    function AuthScreenBehaviour()
    {
        var that = this;

        this.extend(ScreenBehaviour, arguments);

        this.rootDOMElement     = xDom.get("authorization-screen");
        this.messageDOMElement  = this.rootDOMElement.querySelector(".auth-text");

        this.setMessageText = function (text)
        {
            that.messageDOMElement.innerHTML = text;
        };

        initialization.call(this);
        function initialization()
        {

        }
    }

    return AuthScreenBehaviour;

})();