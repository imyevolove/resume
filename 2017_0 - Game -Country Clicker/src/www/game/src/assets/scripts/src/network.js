var network = (function () {

    function Network()
    {
        var m_RequestMethods = ["GET", "POST"];
            m_RequestMethods.default = m_RequestMethods[0];

        var rx_Method       = new RegExp(m_RequestMethods.join("|"), "gi");

        this.request = function (options)
        {
            if(typeof options != "object")
                throw new Error("Invalid type");

            /// Normalizing method
            options.method = (typeof options.method == "string")
                ? options.method.match(rx_Method)[0] || m_RequestMethods.default
                : m_RequestMethods.default;
            options.method = options.method.toUpperCase();

            /// Normalizing parameters
            options.params = (typeof options.params == "object") ? options.params : {};

            /// Normalizing url
            options.url = options.method == "GET" ? createUrlParameters(options.url, options.params) : options.url;

            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function ()
            {
                if(xhr.readyState == 4)
                {
                    if(xhr.status === 200)
                    {
                        if(typeof options.success == "function")
                            options.success(getXHRResultData(xhr));
                    }
                    else
                    {
                        if(typeof options.fail == "function")
                            options.fail(getXHRResultData(xhr));
                    }
                }
            };

            xhr.open(options.method, options.url);
            xhr.send(options.method == "POST" && createFormData(options.params));
        };

        function getXHRResultData(xhr)
        {
            return {
                status: xhr.status,
                response: xhr.responseText
            };
        }

        /// Create url string with 'get' params style
        function createUrlParameters(url, params)
        {
            var urlParamsStr = "?";

            for(var key in params)
            {
                if(typeof params[key] != "string" && typeof params[key] != "number") continue;
                urlParamsStr += key + "=" + params[key] + "&";
            }

            /// Remove last '&' symbol
            urlParamsStr = urlParamsStr.slice(0, -1);

            /// Replace extra '?' symbols
            /// Return result url
            return (url + urlParamsStr).replace(/[\?]/g, function(match, offset, all) {
                return match === "?" ? (all.indexOf("?") === offset ? '?' : '&') : '&';
            });
        }

        /// Create FormData for transfer large data
        function createFormData(params)
        {
            var formData = new FormData();

            for(var key in params)
                formData.append(key, params[key]);

            return formData;
        }
    }

    return new Network();

})();