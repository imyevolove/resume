var Player = (function(){

    function Player()
    {
        var that = this;

        this.money = new ValueNumber(0);
        this.unsavedMoney = new ValueNumber(0);

        this.id = undefined;

        this.increaseMoney = function (value)
        {
            if(!type.isNumber(value))
                throw new Error("Invalid type");

            that.money.inc(value);
            that.unsavedMoney.inc(value);
        }
    }

    return Player;

})();