/*
function AnimationTimeline()
{
    var that = this,
        m_Time = 0,
        m_Keyframes = {};

    this.speed = 0.05;

    this.addAnimation = function (timeStart, timeEnd, action)
    {
        if(!type.isNumber(timeStart)) throw new Error("Invalid argument type; Arg action must be a number");
        if(!type.isNumber(timeEnd)) throw new Error("Invalid argument type; Arg action must be a number");
        if(!type.isFunction(action)) throw new Error("Invalid argument type; Arg action must be a function");
        if(!!m_Keyframes[time]) throw new Error("Keyframe for time '{0}' already defined".format(time));

        var keyframe = new AnimationKeyframe();
        keyframe.timeRange.start   = timeStart;
        keyframe.timeRange.end     = timeEnd;
        keyframe.animation.action  = action;

        m_Keyframes[time] = action;
    };

    this.removeAnimationByTime = function (time)
    {
        delete m_Keyframes[time];
    };

    this.removeAnimationByIndex = function (index)
    {
        var keys = Object.keys(m_Keyframes);
        if(index >= keys.length) throw new Error("Out of range");

        var key = keys[index];

        delete m_Keyframes[key];
    };

    this.updateHandler = undefined;

    this.play = function ()
    {
        if(type.is(updateHandler, Update)) throw new Error("Update handler not defined");

        stopUpdates();
        runUpdates();
    };

    this.stop = function ()
    {
        stopUpdates();
    };

    function runUpdates()
    {
        that.updateHandler.addListener(update);
    }

    function stopUpdates()
    {
        that.updateHandler.removeListener(update);
    }

    function update()
    {
        if(m_Time >= 1)
        {
            callAnimations();
            stopUpdates();

            return ;
        }

        callAnimations();
    }
}


function AnimationKeyframe()
{
    this.timeRange = new AnimationTimeRange();
    this.animation = new Animation();
}

function AnimationTimeRange(start, end)
{
    if(!type.isNumber(start)) throw new Error("Invalid argument type; Arg action must be a number");
    if(!type.isNumber(end)) throw new Error("Invalid argument type; Arg action must be a number");

    this.start = start;
    this.end = end;
}
*/
function Animation()
{
    var
        that = this,
        m_CurrentFrame = 0,
        m_Speed = 10,
        m_Length = 60;

    this.__defineGetter__("currentFrame", function () { return m_CurrentFrame; });
    this.__defineGetter__("currentTime", function () { return m_CurrentFrame / m_Length; });

    this.__defineGetter__("speed", function () { return m_Speed; });
    this.__defineSetter__("speed", function (value) {
        if(!type.isNumber(value)) throw new Error("Invalid argument type; Argument value must be a number");
        m_Speed = Math.clamp01(value);
    });

    this.__defineGetter__("length", function () { return m_Length; });
    this.__defineSetter__("length", function (value) {
        if(!type.isNumber(value)) throw new Error("Invalid argument type; Argument value must be a number");
        m_Length = Math.abs(value);
    });

    this.action = function (time) {};

    this.play = function ()
    {
        m_CurrentFrame = 0;

        stopUpdate();
        startUpdate();
    };

    this.stop = function ()
    {
        m_CurrentFrame = 0;
        stopUpdate();
    };

    this.resume = function ()
    {
        stopUpdate();
        startUpdate();
    };

    this.pause = function ()
    {
        stopUpdate();
    };

    function startUpdate()
    {
        Update.mainUpdate.addListener(update);
    }

    function stopUpdate()
    {
        Update.mainUpdate.removeListener(update);
    }

    function update()
    {
        m_CurrentFrame += m_Speed;

        if(m_CurrentFrame >= m_Length)
        {
            that.action(1);
            stopUpdate();
            return;
        }

        that.action(m_CurrentFrame / m_Length);
    }
}