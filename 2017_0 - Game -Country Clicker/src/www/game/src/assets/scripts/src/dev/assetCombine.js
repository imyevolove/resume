var assetCombine = (function ()
{
    function AssetCombine(asset)
    {
        this.combine =function (asset)
        {
            downloadScriptsText(asset.getScriptPaths(), 0, "", function (text) {
                ///console.log(text);
                download("(function(){\n" + text + "\n})()", "game.txt", "plain/text");
            });
        };

        function downloadScriptsText(urls, index, text, callback)
        {
            if(index >= urls.length)
            {
                callback(text);
                return;
            }

            network.request({
                url: urls[index],
                success: function (response) {
                    downloadScriptsText(urls, ++index, text + "\n" + response.response, callback);
                },
                fail: function () {
                    downloadScriptsText(urls, ++index, text, callback);
                }
            });
        }

        function download(text, name, type) {
            var a = document.createElement("a");
            var file = new Blob([text], {type: type});
            a.href = URL.createObjectURL(file);
            a.download = name;
            a.click();
        }
    }

    return new AssetCombine();
})();