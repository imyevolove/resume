var Application = (function () {

    var apps = [];

    function Application()
    {
        apps.push(this);

        this.initialize = function () {};
    }
    Application.initializeApps = function ()
    {
        for(var i = 0; i < apps.length; i++)
        {
            if(!type.isFunction(apps[i].initialize)) continue;
            apps[i].initialize();
        }
    };


    return Application;

})();