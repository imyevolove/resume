var localization = (function () {

    var EVENTS = {
        PACKAGE_CHANGED: "package.change"
    };


    var nl2br = new RegExp(/([^>])\n+/, "g");

    function LocalizationManager()
    {
        var that = this;
        var
            m_EventsManager = new EventsSystem(),
            m_Package = {};

        ///
        this.setLocalizationPackage = function (package)
        {
            if(!Object.prototype.isPrototypeOf(package) || Array.isArray(package))
            {
                console.warn("Package arg must be a object");
                return;
            }

            m_Package = package;
            m_EventsManager.dispatchEvent(EVENTS.PACKAGE_CHANGED);
        };

        ///
        this.getLocalizationPackage = function ()
        {
            return m_Package;
        };

        ///
        this.get = function (key)
        {
            return m_Package[key] && m_Package[key].replace(nl2br, "<br />");
        };

        ///
        this.download = function (localization_code)
        {
            api.methods.getLocalization(localization_code, function (response) {
                /// Error
                if(!response["response"]) return;
                that.setLocalizationPackage(response.response);
            });
        };

        //
        this.addOnChangeEventListener = function (callback)
        {
            m_EventsManager.addEventListener(EVENTS.PACKAGE_CHANGED, callback);
        };

        ///
        this.removeOnChangeEventListener = function (callback)
        {
            m_EventsManager.removeEventListener(EVENTS.PACKAGE_CHANGED, callback);
        };
    }

    return new LocalizationManager();

})();