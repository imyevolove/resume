var valueShorter = (function () {

    function valueShorter()
    {
        var m_Tags = ["", "K", "M", "B", "T", "q", "Q", "s", "S", "o", "O",
            "n", "N", "d", "D", "u", "U", "y", "Y", "r", "R", "w", "W",
            "i", "I", "p", "P", "a", "A", "f", "F", "g", "G", "h", "H",
            "j", "J", "k", "l", "L", "z", "Z", "x", "X", "c", "C", "v", "V",
            "n", "N", "m", "!", "@", "#", "$", "%", "^", "&", "*"];

        var m_TagsBIGINT = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"];

        var m_BIGINTDel = m_TagsBIGINT.map(function (v, i) { return Math.pow(10, i * 6); });
        var m_Del = m_Tags.map(function (v, i) { return Math.pow(10, i * 3); });

        this.short = function (value, num)
        {
            var valueStr = value.toString();
            if(valueStr.length == 0) return value;

            var index = Math.floor((valueStr.length - 1) / 3);
            if(index > m_Tags.length) return value;

            return parseFloat((value / m_Del[index]).toFixed(num || 2)) + m_Tags[index];
        };

        this.getBIGINTIndexCharacter = function (value)
        {
            if(!type.isNumber(value) || value == 0) return "";

            var u_index = Math.floor(Math.log10(value))
            var index = (u_index > m_TagsBIGINT.length) ? (m_TagsBIGINT.length - 1) : u_index;

            var char =  m_TagsBIGINT[index];
            return char + u_index;
        }
    }
    
    return new valueShorter();
    
})();