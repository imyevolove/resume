var type = (function () {

    function _Type()
    {
        ///
        this.is = function(obj, _class)
        {
            return _class.prototype.isPrototypeOf(obj);
        };

        ///
        this.isHTMLElement = function(element)
        {
            return HTMLElement.prototype.isPrototypeOf(element);
        };

        ///
        this.isBool = function(obj)
        {
            return typeof obj == "boolean";
        };

        ///
        this.isFunction = function(obj)
        {
            return Function.prototype.isPrototypeOf(obj);
        };

        ///
        this.isNumber = function(obj)
        {
            return typeof obj == "number";
        };

        ///
        this.isString = function(obj)
        {
            return typeof obj == "string";
        };

        ///
        this.isArray = function(obj)
        {
            return Array.isArray(obj);
        };

        ///
        this.isObject = function(obj)
        {
            return Object.prototype.isPrototypeOf(obj);
        };

        ///
        this.isTryObject = function(obj)
        {
            if(
                !Object.prototype.isPrototypeOf(obj) ||
                Array.isArray(obj) ||
                Function.prototype.isPrototypeOf(obj)
            ) return false;
            return true;
        };
    }

    return new _Type();

})();