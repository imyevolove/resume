var Template = (function () {

    function Template()
    {
        var
            m_Text = "",
            m_Modifiers = {};

        ///
        this.setText = function (text)
        {
            if(typeof text != "string")
                throw new Error("Argument invalid type");

            m_Text = text;
        };

        ///
        this.getText = function ()
        {
            return m_Text;
        };

        ///
        this.setProperty = function (name, value)
        {
            m_Modifiers[name] = value;
        };

        ///
        this.createContent = function ()
        {
            var template = document.createElement("template");
            template.innerHTML = applyModifiers(m_Text, m_Modifiers);

            return template.content;
        };

        function applyModifiers(text, modifiers)
        {
            for(var key in modifiers)
                text = text.replace(new RegExp("{*" + key + "}", 'g'), modifiers[key]);
            return text;
        }
    }

    return Template;

})();