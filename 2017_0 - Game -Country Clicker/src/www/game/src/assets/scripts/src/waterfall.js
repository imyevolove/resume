var waterfall = (function() {

    return {

        create: function (actions_array, error_callback) 
        {
            var
                index = 0,
                lock_status = false;

            next();

            function next(status, data)
            {
                /// waterfall locked
                if(lock_status)
                {
                    console.error("Waterfall locked");
                    return;
                }

                /// Returned error status
                if(status === false)
                {
                    lock_status = true;
                    if(typeof error_callback == "function") error_callback(data);
                    return;
                }

                /// All actions was called
                if(index >= actions_array.length)
                {
                    lock_status = true;
                    return;
                }

                actions_array[index++](next, data);
            }
        }

    };

})();