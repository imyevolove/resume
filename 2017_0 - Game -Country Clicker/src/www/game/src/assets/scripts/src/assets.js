var assets = (function () {

    var QUEUE_STATE = {
        IDLE    : 1 << 0,
        QUEUE   : 1 << 1
    };

    var EVENTS = {
        "ALL_ASSETS_LOADED": "files.loaded",
        "ALL_SCRIPTS_REQUIRED": "scripts.required"
    };

    function AssetsManager()
    {
        var
            m_EventsManager = new EventsSystem(),
            m_Assets = [],
            m_AssetQueueState = QUEUE_STATE.IDLE,
            m_RequireScriptsQueueState = QUEUE_STATE.IDLE,
            m_AssetsLoadingStack = [];

        /// Include asset and load
        this.include = function (asset_url)
        {
            if(m_Assets.some(function(asset) {return asset.getAssetFilePath() == asset_url;}))
            {
                console.info("Asset '{0}' already added. Adding prevented");
                return;
            }

            m_AssetsLoadingStack.push(new Asset(asset_url));
            downloadNextAsset();
        };

        /// Return all assets
        this.getAssets = function ()
        {
            return m_Assets;
        };

        ///
        this.requireScripts = function (callback)
        {
            if(m_AssetQueueState == QUEUE_STATE.QUEUE) return console.warn("Assets in loading progress");
            if(m_RequireScriptsQueueState == QUEUE_STATE.QUEUE) return console.warn("Scripts requiring progress");

            requireScriptsNextAsset(m_Assets, 0, callback);
        };

        /// Find file entry in file
        this.findFile = function (file_path)
        {
            var file = undefined;
            for(var i = 0; i < m_Assets.length; i++)
            {
                file = m_Assets[i].getFile(file_path);
                if(file != undefined && file != null) return file;
            }
        };

        /// Refresh all assets
        this.Refresh = function ()
        {
            if(m_AssetQueueState == QUEUE_STATE.QUEUE) return;

            m_AssetsLoadingStack = [].concat(m_Assets);
            m_Assets = [];

            downloadNextAsset();
        };

        /// Listen last added assets
        /// If all loaded then call callback now
        this.listenLoadEndLastAddedAssets = function (callback)
        {
            if(typeof callback != "function")
            {
                console.warn("Callback is not a Function");
                return;
            }

            if(m_AssetQueueState == QUEUE_STATE.IDLE)
            {
                callback();
                return;
            }

            var _callback = function () {
                m_EventsManager.removeEventListener(EVENTS.ALL_ASSETS_LOADED, _callback);
                callback();
            };
            m_EventsManager.addEventListener(EVENTS.ALL_ASSETS_LOADED, _callback);
        };

        function requireScriptsNextAsset(assets, index, callback)
        {
            if(m_RequireScriptsQueueState == QUEUE_STATE.QUEUE) return;
            if(index >= assets.length)
            {
                if(typeof callback == "function") callback();
                return;
            }

            m_RequireScriptsQueueState = QUEUE_STATE.QUEUE;

            assets[index].requireScripts(function () {
                m_RequireScriptsQueueState = QUEUE_STATE.IDLE;
                requireScriptsNextAsset(assets, ++index, callback);
            });
        }

        function downloadNextAsset()
        {
            if(m_AssetQueueState == QUEUE_STATE.QUEUE) return;
            if(m_AssetsLoadingStack.length == 0) return;

            m_AssetQueueState = QUEUE_STATE.QUEUE;

            var asset =  m_AssetsLoadingStack.shift();
            asset.Refresh(function () {
                m_AssetQueueState = QUEUE_STATE.IDLE;

                if(asset.isLoaded()) m_Assets.push(asset);
                if(m_AssetsLoadingStack.length == 0) m_EventsManager.dispatchEvent(EVENTS.ALL_ASSETS_LOADED);

                downloadNextAsset();
            });
        }
    }

    return new AssetsManager();

})();