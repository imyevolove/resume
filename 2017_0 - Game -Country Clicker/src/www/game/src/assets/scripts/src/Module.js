var Module = (function () {

    var modules = {};

    function Module(name)
    {
        if(!type.isString(name)) throw new Error("Invalid argument type; Argument 'name' must be a string");
        if(name.length <= 0) throw new Error("Invalid argument; Argument 'name' is empty");
        if(!!modules[name]) throw new Error("Module '{0}' already defined".format(name));

        ////////////////////////////////////////////////////////
        this.requireScript = function (url, callback)
        {
            var element = document.createElement("script");
            element.type = "text/javascript";
            element.src  = url;
            element.setAttribute("defer", "");

            requireElement(element, callback);
        };

        ////////////////////////////////////////////////////////
        this.requireStyle  = function (url, callback)
        {
            var element = document.createElement("style");
            element.type    = "text/css";
            element.rel     = "stylesheet";
            element.href    = url;

            requireElement(element, callback);
        };
    }

    function requireElement(element, callback)
    {
        var callbackLoad = function () {
            if(!type.isFunction(callback)) return;
            callback(new RequireResult(true, element));

            removeListeners();
        };

        var callbackError = function () {
            if(!type.isFunction(callback)) return;
            callback(new RequireResult(false, null));

            document.head.removeChild(element);

            removeListeners();
        };

        document.head.appendChild(element);
        addListeners(callbackLoad, callbackError);

        function removeListeners()
        {
            element.removeEventListener("load",     callbackLoad);
            element.removeEventListener("error",    callbackError);
        }

        function addListeners(callbackLoad, callbackError)
        {
            element.addEventListener("load",    callbackLoad);
            element.addEventListener("error",   callbackError);
        }
    }

    function RequireResult(status, element)
    {
        this.__defineGetter__("status",  function () { return status; });
        this.__defineGetter__("element", function () { return element; });
    }
    
    return Module;

})();