/// VK Social module
var vkModule = (function() {

    var module = new VKModule();

    function VKModule()
    {
        var that = this;

        this.extend(Module, ["VKModule"]);

        waterfall.create([
            function (next)
            {
                that.requireScript("https://vk.com/js/api/xd_connection.js?2", function (result) {
                    next(result.status);
                });
            },
            function (next)
            {
                VK.init(next.bind(null, true), next.bind(null, false, "VK SDK not initialized"), "5.62");
            },
            function (next)
            {
                init.call(that);
                next(true);
            }
        ], function (data) {
            console.log("VK Module initialization failure", data);
        });


        function init()
        {
            var qParams = location.getSearchParameters();
            this.user = new VKUser(qParams.viewer_id, qParams.sid, qParams.auth_key, qParams.access_token);

            this.user.friends.update();
        }
    }

    function VKUser(id, sid, authKey, accessToken)
    {
        this.extend(VKUserBase, [id]);

        this.sid = sid;
        this.authKey = authKey;
        this.accessToken = accessToken;

        this.friends = [];

        /// Update friends list
        this.friends.update = function (callback)
        {
            ///console.log(VK);
            VK.api("apps.getFriendsList", {access_token: accessToken, extended: 1, count: 100, type : "request", test_mode: 1}, function (data) {
                ///console.log(data);
                if(type.isFunction(callback)) callback(data);
            });
        };
    }

    function VKUserBase(id)
    {
        this.id = id;
    }

    return module;

})();