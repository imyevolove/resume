var CountrySelectScreenBehaviour = (function () {

    function CountrySelectScreenBehaviour()
    {
        var that = this;
        this.extend(ScreenBehaviour, arguments);

        var m_UIItems      = [],
            m_FreeUIItems  = [];

        var m_ItemTemplate = new Template();
        m_ItemTemplate.setText(resources.get("resources/tpl.country_select_item.html"));


        this.rootDOMElement = xDom.get("country-select-wrapper");
        this.itemsContainerDOMElement = xDom.get("country-select-items");
        this.closeButtonDOMElement = xDom.get("country-select-close");

        var m_Scroll = new Scroll(this.itemsContainerDOMElement);
        m_Scroll.scrollElement.containerElement.style.right = "2px";
        m_Scroll.scrollElement.scrollElement.style.backgroundColor = "rgba(255, 255, 255, .4)";

        var m_OriginalShowFunc = this.show;
        this.show = function ()
        {
            updateCountriesData(m_OriginalShowFunc);
        };

        initialization.call(this);
        function initialization()
        {
            that.hide();

            that.closeButtonDOMElement.on("click", that.hide);

            xData.country.addOnChangeIdentificationDataEventListener(updateActiveItems);
            localization.addOnChangeEventListener(updateItemsLocalization);
        }

        function updateActiveItems()
        {
            var currentCountryID = xData.country.getId();
            for(var i = 0; i < m_UIItems.length; i++)
            {
                if(m_UIItems[i].getTargetCountryID() == currentCountryID) m_UIItems[i].activate();
                else m_UIItems[i].deactivate();
            }
        }

        function updateItemsLocalization()
        {
            for(var i = 0; i < m_UIItems.length; i++)
            {
                m_UIItems[i].setName(localization.get("country." + m_UIItems[i].getTargetCountryID()));
            }
        }

        function updateCountriesData(callback)
        {
            xGame.screens.loading.show();
            api.methods.getCountriesList({view: "minimal"}, function (response) {
                xGame.screens.loading.hide();

                if(!response || !response["response"])
                {
                    that.hide();
                }
                else
                {
                    clear();

                    var countries = filterAndSortCountries(response.response.items);
                    drawCountries(countries);
                    updateActiveItems();
                }

                if(type.isFunction(callback)) callback();
                m_Scroll.scrollToUp();
            })
        }

        function drawCountries(countries)
        {
            var item, activeItem;
            for(var i = 0; i < countries.length; i++)
            {
                item = getUIItem();
                item.show();
                item.setTargetCountry(countries[i]);
                item.setIcon("/assets/media/flags/" + countries[i].code + ".png");
                item.setName(countries[i].name);

                if(countries[i].id == xData.country.getId()) activeItem = item.getElement();
            }

            var prnt = activeItem.parentElement;
            if(prnt.children.length > 0 && prnt.children[0] != activeItem)
                prnt.insertBefore(activeItem, prnt.children[0]);
        }

        function filterAndSortCountries(countries)
        {
            for(var i = 0; i < countries.length; i++)
                countries[i]["name"] = localization.get("country." + countries[i].id);

            countries = countries.sort(function (a, b) {
                return a.name.localeCompare(b.name);
            });

            return countries;
        }

        function clear()
        {
            m_FreeUIItems = [];
            for(var i = 0; i < m_UIItems.length; i++)
            {
                m_UIItems[i].hide();
                m_FreeUIItems.push(m_UIItems[i]);
            }
        }

        function getUIItem()
        {
            return m_FreeUIItems.pop() || createItem();
        }

        function createItem()
        {
            var item = new UIItem(that.itemsContainerDOMElement);

            m_UIItems.push(item);
            return item;
        }

        function UIItem(parent)
        {
            var content = m_ItemTemplate.createContent();
            var element = content.children[0];
            var m_TargetCountry;

            element.on("click", function () {
                if(!m_TargetCountry) return;
                if(m_TargetCountry.id == xData.country.getId()) return;

                xGame.screens.loading.show();
                api.methods.changeCountry({country_id: m_TargetCountry.id}, function (response) {
                    xGame.screens.loading.hide();

                    if(!response || !response["response"] || !response.response.status) return;

                    xData.country.setIdentificationData(m_TargetCountry.id, m_TargetCountry.code);
                })
            });

            this.show = function ()
            {
                parent.appendChild(element);
            };

            this.activate = function ()
            {
                element.setAttribute("active", true);
            };

            this.deactivate = function ()
            {
                element.removeAttribute("active");
            };

            this.hide = function ()
            {
                element.delete();
            };

            this.setTargetCountry = function (country)
            {
                m_TargetCountry = country;
            };

            this.getTargetCountry = function ()
            {
                return m_TargetCountry;
            };

            this.getTargetCountryID = function ()
            {
                return !m_TargetCountry ? null : m_TargetCountry.id;
            };

            this.getElement = function ()
            {
                return element;
            };

            this.setName = function (value)
            {
                element.querySelector("._name").innerHTML = value;
            };

            this.setIcon = function (url)
            {
                element.querySelector("._icon").style.backgroundImage = "url(" + url + ")";
            };
        }
    }

    return CountrySelectScreenBehaviour;

})();