var xDom = (function () {
    
    function XDom() {}

    // Find element in dom
    XDom.prototype.get = function (value)
    {
        return document.getElementById(value) || document.querySelector(value);
    };

    return new XDom();
    
})();