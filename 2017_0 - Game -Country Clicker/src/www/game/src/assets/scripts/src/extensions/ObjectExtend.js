Object.defineProperty(Object.prototype, "extend", {
    writable: false,
    enumerable: false,
    configurable: false,
    value: function (target, args)
    {
        target.apply(this, args);

        if(!(target.prototype == this.constructor.prototype))
        {
            this.__proto__ = this.constructor.prototype = Object.create(target.prototype);
            this.constructor.prototype.constructor = target;
        }
    }
});

Object.defineProperty(Object.prototype, "defineReadonlyProperty", {
    writable: false,
    enumerable: false,
    configurable: false,
    value: function (name, value)
    {
        Object.defineProperty(this, name, {
            writable: false,
            enumerable: false,
            configurable: false,
            value: value
        });
    }
});

Object.defineProperty(Object.prototype, "defineGetterAndSetter", {
    writable: false,
    enumerable: false,
    configurable: false,
    value: function (name, getter, setter)
    {
        if(!type.isFunction(getter) || !type.isFunction(setter))
            throw new TypeError();

        this.__defineGetter__(name, getter);
        this.__defineSetter__(name, setter);
    }
});