var ValueNumber = (function () {

    function ValueNumber(value)
    {
        var that = this;

        arguments[0] = typeof arguments[0] == "number" ? arguments[0] : 0;

        this.extend(Value, arguments);

        /// Override setter
        var original_set = this.set;
        this.set = function (value)
        {
            if(typeof value != "number")
                throw new Error("Type exceptions; Invalid Type");
            return original_set(value);
        };

        this.inc = function (value)
        {
            if(typeof value != "number")
                throw new Error("Type exceptions; Invalid Type");
            return original_set(that.get() + value);
        };
    }

    return ValueNumber;

})();