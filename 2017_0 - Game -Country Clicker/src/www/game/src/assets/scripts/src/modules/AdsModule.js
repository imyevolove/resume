/// VK Social module
var adsModule = (function() {

    var module = new AdsModule();

    function AdsModule()
    {
        var that = this;
        var container = document.getElementById("ads-root-container");

        this.extend(Module, ["AdsModule"]);

        this.addLink = function (name, icon, url)
        {
            var link = new AdsLinkEntity(container);
            link
                .setName(name)
                .setIcon(icon)
                .setUrl(url);
        };

        init();
        function init()
        {
            /*network.request({
                method: "GET",
                url: "https://api.vk.com/method/groups.getById",
                params: {
                    group_id: 104689708
                },
                success: function (result) {
                    try
                    {
                        var response = JSON.parse(result);
                        response = response.response[0];

                        module.addLink("Official developer community", response.photo, "https://vk.com/club" + response.gid);
                    }
                    catch(e) {}
                }
            });*/

            that.addLink("Official developer community", "/assets/media/icons/vk.png", "https://vk.com/club104689708");
            that.addLink("Our Android games", "/assets/media/icons/gp.png", "https://play.google.com/store/apps/dev?id=6978304903959327663");
        }
    }

    function AdsLinkEntity(parent)
    {
        var that = this;

        var m_Element = document.createElement("a");
        m_Element.target = "_blank";
        m_Element.className = "ads-entity-link";
        m_Element.appendTo(parent);

        var m_IconElement = document.createElement("div");
        m_IconElement.className = "_icon";
        m_IconElement.appendTo(m_Element);

        var m_NameElement = document.createElement("div");
        m_NameElement.className = "_name";
        m_NameElement.appendTo(m_Element);

        this.setIcon = function (icon)
        {
            m_IconElement.style.background = "url(" + icon + ")";
            return that;
        };

        this.setName = function (name)
        {
            m_NameElement.innerHTML = name;
            return that;
        };

        this.setUrl  = function (url)
        {
            m_Element.href = url;
            return that;
        };
    }

    return module;

})();