var ParticlesSystem = (function () {

    var EVENTS = {
        PARTICLE_CLICK: "event.particle.click"
    };

    function ParticlesSystem()
    {
        var that = this;

        var m_EventsManager = new EventsSystem(),
            freePool = [],
            particles = [];

        this.parent = document.body;

        this.lifeTime = 1;
        this.customClass = "";
        this.image = "";

        // Instantiate particles
        this.emit = function (count)
        {
            for(var i = 0; i < count; i++)
            {
                spawnParticle();
            }
        };

        ///
        this.onClick = function (callback)
        {
            m_EventsManager.addEventListener(EVENTS.PARTICLE_CLICK, callback);
        };

        ///
        this.removeOnClick = function (callback)
        {
            m_EventsManager.removeEventListener(EVENTS.PARTICLE_CLICK, callback);
        };

        function spawnParticle()
        {
            var particle = getParticleFromPool() || instantiateParticle();

            /// Stylize particle
            particle.element.className = "particle " + that.customClass;
            particle.element.style.backgroundImage = "url(" + that.image + ")";
            particle.element.style.left = (Math.random() * that.parent.offsetWidth) + "px";
            particle.element.style.top = (Math.random() * that.parent.offsetHeight / 4) + "px";

            /// Register particle in particles system
            particles.push(particle);
            particle.spawn(that.lifeTime * 1000);
        }

        function getParticleFromPool()
        {
            return freePool.splice(0, 1)[0];
        }

        function instantiateParticle()
        {
            var particle = new PoolableParticle();
            particle.element.appendTo(that.parent);

            /// Register particle events
            particle.element.on("click", m_EventsManager.dispatchEvent.bind(null, EVENTS.PARTICLE_CLICK, particle));

            ///
            return particle;
        }

        function Particle()
        {
            var that = this;
            var
                m_DelayDestroyTimeout,
                m_lifetime;

            this.element = document.createElement("div");

            this.spawn = function (lifetime)
            {
                m_lifetime = lifetime;

                that.element.style.display = "block";
                m_DelayDestroyTimeout = setTimeout(that.destroy, m_lifetime * 1000);
            };

            this.destroy = function ()
            {
                that.element.style.display = "none";
                clearTimeout(m_DelayDestroyTimeout);
            };
        }

        function PoolableParticle()
        {
            var that = this;

            this.extend(Particle);

            var m_OriginalDestroyFunc = this.destroy;
            this.destroy = function ()
            {
                m_OriginalDestroyFunc();

                particles.splice(particles.indexOf(that), 1);
                if(freePool.indexOf(that) < 0) freePool.push(that);
            }
        }

    }

    
    return ParticlesSystem;
    
})();