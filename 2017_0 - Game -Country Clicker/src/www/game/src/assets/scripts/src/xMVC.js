var xMVC = (function() {

    function XMVC()
    {
        var mvc = this;
        var
            m_Controllers = {},
            m_Models = {},
            m_Views = {};

        ///
        this.registerModel = function (id, func)
        {
            if(typeof id != "string" || typeof func != "function")
                throw new Error("Invalid Type Exception");

            if(!!m_Models[id])
                throw new Error("Model '{0}' already defined".format(id));

            m_Models[id] = Model.bind(null, func);
            return m_Models[id];
        };

        ///
        this.createModel = function (id, options)
        {
            if(!m_Models[id])
                throw new Error("Model '{0}' not found".format(id));

            return new m_Models[id](options);
        };

        ///
        this.registerController = function (id, func)
        {
            if(typeof id != "string" || typeof func != "function")
                throw new Error("Invalid Type Exception");

            if(!!m_Controllers[id])
                throw new Error("Controller '{0}' already defined".format(id));

            m_Controllers[id] = Controller.bind(null, func);
            return m_Controllers[id];
        };

        ///
        this.createController = function (id, options)
        {
            if(!m_Controllers[id])
                throw new Error("Controller '{0}' not found".format(id));

            return new m_Controllers[id](options);
        };

        ///
        this.registerView = function (id, func)
        {
            if(typeof id != "string" || typeof func != "function")
                throw new Error("Invalid Type Exception");

            if(!!m_Views[id])
                throw new Error("View '{0}' already defined".format(id));

            m_Views[id] = View.bind(null, func);
            return m_Views[id];
        };

        ///
        this.createView = function (id, options)
        {
            if(!m_Views[id])
                throw new Error("View '{0}' not found".format(id));

            return new m_Views[id](options);
        };

        ///
        function Controller(func, options)
        {
            options = typeof options == "object" ? options : {};

            var sandbox = Object.create(options);

            func.call(this, sandbox);

            /// Return attached model
            this.getModel = function()
            {
                return sandbox.model;
            };

            /// Return attached view
            this.getView = function()
            {
                return sandbox.view;
            };

            /// Call initialize function
            if(typeof this.initialize == "function") this.initialize();
        }

        ///
        function Model(func, options)
        {
            options = typeof options == "object" ? options : {};

            var sandbox =  Object.create(options);
            sandbox.eventsSystem = new EventsSystem();
            sandbox.events = Object.create({});

            func.call(this, sandbox);

            ///
            this.on = function (eventID, listener)
            {
                sandbox.eventsSystem.addEventListener(eventID, listener);
            };

            ///
            this.removeEventListener = function (eventID, listener)
            {
                sandbox.eventsSystem.removeEventListener(eventID, listener);
            };

            ///
            this.update = function ()
            {
                for(var key in sandbox.events)
                    if (sandbox.events.hasOwnProperty(key))
                        sandbox.events[key]();
            };

            /// Call initialize function
            if(typeof this.initialize == "function") this.initialize();
        }

        ///
        function View(func, options)
        {
            options = typeof options == "object" ? options : {};

            var sandbox = Object.create(options);

            ///
            this.getSandbox = function()
            {
                return sandbox;
            };

            /// Override for more controls
            this.observe = function(model)
            {
                sandbox.model = model;
            };

            func.call(this, sandbox);

            /// Call initialize function
            if(typeof this.initialize == "function") this.initialize();
        }
    }

    return new XMVC();

})();