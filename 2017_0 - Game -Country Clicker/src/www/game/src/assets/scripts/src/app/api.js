var api = (function () {
    
    function Api()
    {
        var that = this;
        var unknownErrorResponse = {error: {code: -1, message: "unknown"}};

        this.access_token = "";
        this.apiUrl = location.origin + "/services/api";
        this.methods = {
            authorize: function (callback)
            {
                that.request("get", "auth", location.getSearchParameters(), callback);
            },
            userGet: function (callback)
            {
                that.request("get", "user.get", { access_token: that.access_token }, callback);
            },
            getCountry: function (countryID, callback)
            {
                var data = {};
                data.access_token = that.access_token;
                data.id = countryID;

                that.request("get", "country.get", data, callback);
            },
            balanceIncrease: function (value, callback)
            {
                var data = {};
                data.access_token = that.access_token;
                data.value = value;

                that.request("get", "balance.increase", data, callback);
            },
            balanceTransfer: function (data, callback)
            {
                data = normalizeObject(data);
                data.access_token = that.access_token;

                that.request("get", "balance.transfer", data, callback);
            },
            getLeaderboardCountries: function (data, callback)
            {
                data = normalizeObject(data);
                data.access_token = that.access_token;

                that.request("get", "leaderboard.get", data, callback);
            },
            getLocalization: function (localization_code, callback)
            {
                var data = {};
                data.access_token = that.access_token;
                data.country_code = localization_code;

                that.request("get", "localization.get", data, callback);
            },
            getCountriesList: function (data, callback)
            {
                data = normalizeObject(data);
                data.access_token = that.access_token;

                that.request("get", "country.list", data, callback);
            },
            changeCountry: function (data, callback)
            {
                data = normalizeObject(data);
                data.access_token = that.access_token;

                that.request("get", "country.change", data, callback);
            }
        };

        this.request = function (requestMethod, method, params, callback)
        {
            network.request({
                method: requestMethod,
                url: that.apiUrl +  "/" + method,
                params: params,

                success: function (response) {
                    if(typeof callback != "function") return;

                    var data = {};
                    try {
                        data = JSON.parse(response.response);
                    } catch(err) { data = {error: {code: -1, message: "unknown"}}; }

                    callback(data);
                },
                fail: function () {
                    if(typeof callback != "function") return;
                    callback(unknownErrorResponse);
                }
            })
        };

        function normalizeObject(obj)
        {
            return (!Object.prototype.isPrototypeOf(obj) || Array.isArray(obj)) ? {} : obj
        }
    }

    return new Api();
    
})();