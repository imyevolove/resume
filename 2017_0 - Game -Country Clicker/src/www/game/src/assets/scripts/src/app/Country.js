var Country = (function(){

    var EVENTS = {
        IDENTIFICATION_DATA_CHANGE: "identification.data.change",
        BALANCE_DATA_CHANGE: "balance.data.change"
    };

    function Country()
    {
        var that = this;
        var m_EventsManager = new EventsSystem();
        var m_IdentificationData = {id: 0, code: ""};
        var m_BalanceData = {balance: 0, level: 0};

        this.setIdentificationData = function (id, code)
        {
            if(!type.isString(code) || !type.isNumber(id))
                throw new Error("Invalid data");

            m_IdentificationData.code = code;
            m_IdentificationData.id = id;

            m_EventsManager.dispatchEvent(EVENTS.IDENTIFICATION_DATA_CHANGE);
        };

        this.setBalanceData = function (balance, level)
        {
            if(!type.isNumber(balance) || !type.isNumber(level))
                throw new Error("Invalid data");

            if(type.isNumber(balance))
            {
                m_BalanceData.balance = balance;
            }

            if(type.isNumber(level))
            {
                m_BalanceData.level = level;
            }

            m_EventsManager.dispatchEvent(EVENTS.BALANCE_DATA_CHANGE);
        };

        this.getCode = function () { return m_IdentificationData.code; };
        this.getId = function () { return m_IdentificationData.id; };

        this.getBalance = function () { return m_BalanceData.balance; };
        this.getBalanceLevel = function () { return m_BalanceData.level; };

        this.addOnChangeBalanceDataEventListener = function (callback)
        {
            m_EventsManager.addEventListener(EVENTS.BALANCE_DATA_CHANGE, callback);
        };

        this.removeOnChangeBalanceDataEventListener = function (callback)
        {
            m_EventsManager.removeEventListener(EVENTS.BALANCE_DATA_CHANGE, callback);
        };

        this.addOnChangeIdentificationDataEventListener = function (callback)
        {
            m_EventsManager.addEventListener(EVENTS.IDENTIFICATION_DATA_CHANGE, callback);
        };

        this.removeOnChangeIdentificationDataEventListener = function (callback)
        {
            m_EventsManager.removeEventListener(EVENTS.IDENTIFICATION_DATA_CHANGE, callback);
        };
    }

    return Country;

})();