var GameScreenBehaviour = (function () {

    function GameScreenBehaviour()
    {
        var that = this;
        var transferMoneyMin = 200;
        var clickerTitleAnimation = new ClickerTitleAnimation(xDom.get("clicker-title"), "clicker-title-hider", 2);

        var moneySounds = new VariativeAudio();
        moneySounds.addAudioSource(new AudioSource("/assets/resources/audio/whoosh_1.mp3"));
        moneySounds.addAudioSource(new AudioSource("/assets/resources/audio/whoosh_2.mp3"));
        moneySounds.addAudioSource(new AudioSource("/assets/resources/audio/whoosh_3.mp3"));

        var clickSounds = new VariativeAudio();
        clickSounds.addAudioSource(new AudioSource("/assets/resources/audio/punch_1.mp3"));
        clickSounds.addAudioSource(new AudioSource("/assets/resources/audio/punch_2.mp3"));
        clickSounds.addAudioSource(new AudioSource("/assets/resources/audio/punch_3.mp3"));

        this.extend(ScreenBehaviour, arguments);

        this.particles = new ParticlesSystem();

        this.particles.durationParticleEffect = function () {};
        this.particles.enterParticleEffect = function () {};
        this.particles.exitParticleEffect = function () {};

        this.particles.onClick(function (particle) {
            moneySounds.play();
            xGame.clickerBonus.trigger();
            particle.destroy();

        });

        this.rootDOMElement             = xDom.get("application-root");
        this.particlesDOMElement        = xDom.get("particles-container");
        this.playerMoneyDOMElement      = xDom.get("player-money-label");
        this.leaderboardDOMButton       = xDom.get("leaderboard-button");
        this.countryMoneyDOMElement     = xDom.get("country-money-label");
        this.clickDOMElement            = xDom.get("click-area-box");
        this.transferButtonDOMElement   = xDom.get("transfer-money-button");
        this.countryIconDOMElement      = xDom.get("country-icon");
        this.clickerTitleDOMElement     = xDom.get("clicker-title");

        var playerMoneyNumberSlider = new NumberSlider();
        playerMoneyNumberSlider.hideUnusedNumbers = false;
        playerMoneyNumberSlider.container = this.playerMoneyDOMElement;
        playerMoneyNumberSlider.setValue(100000).setValue(0);

        initialization.call(this);
        function initialization()
        {
            initializeViewEvents();
            initializeEffects();
            initializeActions();
        }

        function initializeEffects()
        {
            that.particles.lifeTime = 5;
            that.particles.parent = that.particlesDOMElement;
            that.particles.customClass = "particle-money";
            that.particles.image = "/assets/media/particles/dollar.png";
        }

        function initializeViewEvents()
        {
            xData.player .money.addOnChangeEventListener(updatePlayerMoneyUI );
            xData.country.addOnChangeBalanceDataEventListener(updateCountryMoneyUI);
            xData.country.addOnChangeIdentificationDataEventListener(updateCountryIconUI);
            localization.addOnChangeEventListener(updateLocalization);

            updatePlayerMoneyUI ();
            updateCountryMoneyUI();
            updateCountryIconUI ();
            updateLocalization  ();
        }
        
        function initializeActions()
        {
            that.clickDOMElement.on("click", clickAction);
            that.transferButtonDOMElement.on("click", transferAction);
            that.leaderboardDOMButton.on("click", showLeaderboard);
            that.countryIconDOMElement.on("click", showCountrySelect);
        }

        function showLeaderboard()
        {
            xGame.screens.leaderboard.show();
        }

        function showCountrySelect()
        {
            xGame.screens.countrySelect.show();
        }

        function transferAction()
        {
            if(xData.player.money.get() < transferMoneyMin) return;
            xGame.transferAllMoneyCountry();
        }
        
        function clickAction()
        {
            clickSounds.play();
            xGame.click();
            that.particles.emit(1);
            clickerTitleAnimation.trigger();
        }

        function ClickerTitleAnimation(targetElement, hiderClassName, backwardReactionTime)
        {
            var timeout;
            this.trigger = function ()
            {
                targetElement.classList.add(hiderClassName);

                clearTimeout(timeout);
                timeout = setTimeout(applyHider, backwardReactionTime * 1000);
            };

            function applyHider()
            {
                targetElement.classList.remove(hiderClassName);
            }
        }

        function updateTransferButtonState()
        {
            that.transferButtonDOMElement.setAttribute("transfer-state", xData.player.money.get() < transferMoneyMin ? "locked" : "unlocked")
        }

        function updateLocalization()
        {
            that.transferButtonDOMElement.innerHTML = localization.get("money_transfer_title");
            that.clickerTitleDOMElement.innerHTML = localization.get("clicker_title");
        }

        function updatePlayerMoneyUI ()
        {
            playerMoneyNumberSlider.setValue(valueShorter.short(xData.player.money.get()));
            //that.playerMoneyDOMElement.innerHTML  = valueShorter.short(xData.player.money.get());
            updateTransferButtonState();
        }

        function updateCountryIconUI()
        {
            that.countryIconDOMElement.style.backgroundImage = "url(/assets/media/flags/" + xData.country.getCode() + ".png)";
        }

        function updateCountryMoneyUI()
        {
            that.countryMoneyDOMElement.innerHTML = valueShorter.short(xData.country.getBalance())
                + " <span class='golden-type'>" + valueShorter.getBIGINTIndexCharacter(xData.country.getBalanceLevel()) + "</span>";

            that.countryMoneyDOMElement.removeAttribute("anim");
            setTimeout(function () {
                that.countryMoneyDOMElement.setAttribute("anim", "value-update");
            }, 10);
        }
    }

    return GameScreenBehaviour;

})();