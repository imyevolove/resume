var AudioSource = (function ()
{
    function AudioSource(src)
    {
        var that = this;
        var m_Volume = 1,
            m_Src = type.isString(src) ? src : "",
            m_AudioPool = [];

        this.__defineGetter__("src", function () { return m_Src; });
        this.__defineSetter__("src", function (value) {
            if(!type.isString(value)) throw new Error("Invalid Argument Type; Argument must be a string");

            m_Src = value;
        });

        this.__defineGetter__("volume", function () { return m_Volume; });
        this.__defineSetter__("volume", function (value) {
            if(!type.isNumber(value)) throw new Error("Invalid Argument Type; Argument must be a number");

            m_Volume = Math.clamp01(value);
            for(var i = 0; i < m_AudioPool.length; i++) m_AudioPool[i].volume = m_Volume;
        });

        this.play = function ()
        {
            var audio = getFreeAudio();

            if(audio.src != m_Src && audio.src != location.origin + "" + m_Src)
                audio.src = m_Src;

            audio.volume = m_Volume;
            audio.play();
        };

        this.resume = function ()
        {
            for(var i = 0; i < m_AudioPool.length; i++)
            {
                if(!m_AudioPool[i].paused) continue;
                m_AudioPool[i].play();
            }
        };

        this.pause = function ()
        {
            for(var i = 0; i < m_AudioPool.length; i++)
            {
                m_AudioPool[i].pause();
            }
        };

        this.stop = function ()
        {
            for(var i = 0; i < m_AudioPool.length; i++)
            {
                m_AudioPool[i].pause();
                m_AudioPool[i].currentTime = 0;
                m_AudioPool[i].paused = false;
                m_AudioPool[i].ended = true;
            }
        };

        function getFreeAudio()
        {
            for(var i = 0; i < m_AudioPool.length; i++)
            {
                if(!m_AudioPool[i].ended) continue;
                return m_AudioPool[i];
            }

            var audio = new Audio();
            m_AudioPool.push(audio);

            return audio;
        }
    }

    return AudioSource;

})();