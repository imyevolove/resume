Math.clamp = function (value, min, max)
{
    return Math.min(Math.max(value, min), max);
};

Math.clamp01 = function (value)
{
    return Math.clamp(value, 0, 1)
};

Math.randomRange = function (min, max)
{
    return Math.random() * (max - min) + min;
};

Math.randomRangeInt = function (min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
};