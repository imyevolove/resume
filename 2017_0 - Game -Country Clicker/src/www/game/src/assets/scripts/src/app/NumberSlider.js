var NumberSlider = (function () {

    function NumberSlider()
    {
        var that = this;
        var
            m_Container = undefined,
            m_HideUnusedNumbers = true;
        var m_NumberElements = [];

        /// Parent container
        this.defineGetterAndSetter("container", function ()
        {
            return m_Container;
        }, function (value)
        {
            if(!type.isHTMLElement(value)) throw new TypeError();

            m_Container = value;
            m_Container.innerHTML = "";

            modifyContainer();
            // updateLinks();
        });

        ///
        this.defineGetterAndSetter("hideUnusedNumbers", function () { return m_HideUnusedNumbers; },
            function (value)
            {
                if(!type.isBool(value)) throw new TypeError();
                m_HideUnusedNumbers = value;
            });

        /// Value setter
        this.setValue = function (value)
        {
            if(!type.isNumber(value) && !type.isString(value)) throw new TypeError();
            if(!type.isHTMLElement(m_Container)) throw new Error("Container is not defined");

            printValue(value);
            return this;
        };

        function modifyContainer()
        {
            if(!type.isHTMLElement(m_Container)) throw new Error("Container is not defined");
            m_Container.classList.add("number-slide-wrapper");
        }

        function updateLinks()
        {
            if(!type.isHTMLElement(m_Container)) throw new Error("Container is not a html element");
            m_NumberElements = [].slice.call(m_Container.childNodes).filter(function (el) { return isNumberElement(el); });
        }

        function printValue(value)
        {
            var valueStr = value.toString();
            var tmpNumberSlide = undefined;

            for(var i = 0; i < valueStr.length; i++)
            {
                tmpNumberSlide = getNumberElementByIndex(i);
                tmpNumberSlide.setText(valueStr[i]);
                tmpNumberSlide.enable();
            }

            if(m_HideUnusedNumbers)
                disableExtraNumbers(valueStr.length);
            else
                refreshExtraNumbers(valueStr.length);
        }

        function disableExtraNumbers(atIndex)
        {
            for(atIndex; atIndex < m_NumberElements.length; atIndex++)
            {
                m_NumberElements[atIndex].disable();
            }
        }

        function refreshExtraNumbers(atIndex)
        {
            for(atIndex; atIndex < m_NumberElements.length; atIndex++)
            {
                m_NumberElements[atIndex].setText("-");
            }
        }

        function isNumberElement(element)
        {
            return type.isHTMLElement(element) && (element.className == "number-slide");
        }

        function getNumberElementByIndex(index)
        {
            if(index >= m_NumberElements.length)
            {
                m_NumberElements.push(createNumberElement(m_Container));
                return getNumberElementByIndex(index);
            }

            return m_NumberElements[index];
        }

        function createNumberElement(parent)
        {
            return new NumberSlide(parent);
        }

        function NumberSlide(parent)
        {
            var that = this;

            this.element = document.createElement("span");
            this.element.className = "number-slide";

            if(type.isHTMLElement(parent))
                this.element.appendTo(parent);

            this.setText = function (value) { that.element.innerHTML = value; };
            this.enable  = controlElementVisible.bind(null, this.element, "inline-block");
            this.disable = controlElementVisible.bind(null, this.element, "none");
        }

        function controlElementVisible(element, displayState)
        {
            element.style.display = displayState;
        }
    }

    return NumberSlider;

})();