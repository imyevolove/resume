// Append element to element
HTMLElement.prototype.appendTo = function (parent)
{
    parent.appendChild(this);
};

// Delete element from dom
HTMLElement.prototype.delete = function ()
{
    if(!this.parentElement) return;
    this.parentElement.removeChild(this);
};