var Update = (function () {

    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    var cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame;

    var EVENTS = {
        UPDATE: "event.update"
    };

    function Update()
    {
        var
            m_AnimationFrameID = undefined,
            m_EventsManager = new EventsSystem();

        this.addListener = function (callback)
        {
            m_EventsManager.addEventListener(EVENTS.UPDATE, callback);
        };

        this.removeListener = function (callback)
        {
            m_EventsManager.removeEventListener(EVENTS.UPDATE, callback);
        };

        this.run = function ()
        {
            stopUpdate();
            update();
        };

        this.stop = function ()
        {
            stopUpdate();
        };

        function stopUpdate()
        {
            cancelAnimationFrame(m_AnimationFrameID);
        }

        function update()
        {
            m_AnimationFrameID = requestAnimationFrame(update);
            m_EventsManager.dispatchEvent(EVENTS.UPDATE);
        }
    }

    Update.mainUpdate = new Update();
    Update.mainUpdate.run();

    Object.defineProperty(Update, "mainUpdate", {
        writable: false,
        enumerable: false,
        configurable: false,
        value: Update.mainUpdate
    });


    return Update;

})();