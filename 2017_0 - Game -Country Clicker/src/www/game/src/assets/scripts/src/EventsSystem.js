var EventsSystem = (function () {
    
    function EventsSystem()
    {
        var m_Channels = {};

        ///
        this.addEventListener = function (eventID, listener)
        {
            if(typeof listener != "function")
                throw new Error("Invalid Type Exception; Listener is not Function");
            if(!m_Channels[eventID]) m_Channels[eventID] = [];
            if(m_Channels[eventID].indexOf(listener) >= 0)
                return console.warn("Listener '{0}' already registered".format(eventID));

            m_Channels[eventID].push(listener);
        };

        ///
        this.removeEventListener = function (eventID, listener)
        {
            if(!m_Channels[eventID]) return;

            var index = m_Channels[eventID].indexOf(listener);

            if(index < 0) return;
            m_Channels[eventID].splice(index, 1);
        };

        ///
        this.removeAllEventListeners = function (eventID)
        {
            if(typeof eventID == "string") m_Channels[eventID] = [];
            else m_Channels = {};
        };

        ///
        this.dispatchEvent = function (eventID, data)
        {
            if(!m_Channels[eventID]) return;

            for(var i = 0; i < m_Channels[eventID].length; i++)
            {
                m_Channels[eventID][i](data);
            }
        }
    }
    
    return EventsSystem;
    
})();