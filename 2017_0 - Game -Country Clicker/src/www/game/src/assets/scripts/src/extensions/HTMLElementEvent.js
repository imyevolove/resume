// Add event listener
HTMLElement.prototype.on = function (eventID, action, useCapture)
{
    typeof useCapture == "boolean" || (useCapture = false);
    this.addEventListener(eventID, action, useCapture);

    return this;
};
