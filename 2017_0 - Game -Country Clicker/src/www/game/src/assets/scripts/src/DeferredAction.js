var DeferredAction = (function () {

    function DeferredAction()
    {
        var that = this;
        var counter = 0;

        this.idle = 0;
        this.action = undefined;
        this.next = function ()
        {
            counter++;

            if(counter >= that.idle)
            {
                counter = 0;
                if(typeof that.action == "function") that.action();
            }
        };
    }

    return DeferredAction;

})();