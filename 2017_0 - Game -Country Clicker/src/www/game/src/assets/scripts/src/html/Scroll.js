var Scroll = (function () {

    function Scroll(target)
    {
        if(!type.isHTMLElement)
        {
            throw new Error("Invalid type");
        }

        var that = this;

        this.scrollElement;

        this.scrollToUp = function ()
        {
            that.scrollElement.setYFast(0);
        };

        initializeScroll.call(this, target);
        function initializeScroll(elementContainer)
        {
            that.scrollElement = new ScrollElement(elementContainer);

            /// Add handlers
            addScrollListeners(that.scrollElement);
            addScrollTouchListener(that.scrollElement);
            addScrollHandlerDragListener(that.scrollElement);
        }

        function ScrollElement(content)
        {
            var that = this;
            var m_SmoothMovement = new SmoothMovement();
            var m_TempMaxScrollTop = 0,
                m_TempScrollElementPosition = 0;

            this.containerElement   = document.createElement("div");
            this.scrollElement      = document.createElement("div");
            this.contentElement     = content;

            applyStyles.call(this);

            this.containerElement.appendChild(this.scrollElement);
            this.contentElement.parentElement.appendChild(this.containerElement);

            m_SmoothMovement.onupdate = function (x, y)
            {
                content.scrollTop = content.scrollTop + y;
                moveScrollElement();
            };

            this.setY = function (value)
            {
                m_SmoothMovement.setVelocity(m_SmoothMovement.velocity.x, value);
            };

            this.setYFast = function (value)
            {
                m_SmoothMovement.resetVelocity();
                m_SmoothMovement.stopMovement();

                content.scrollTop = value;
                moveScrollElement();
            };

            this.moveY = function (value)
            {
                m_SmoothMovement.addForce(m_SmoothMovement.velocity.x, value);
            };

            function moveScrollElement()
            {
                m_TempMaxScrollTop = that.contentElement.scrollHeight - that.contentElement.clientHeight;
                m_TempScrollElementPosition = ((that.containerElement.clientHeight - that.scrollElement.offsetHeight)
                                                * (that.contentElement.scrollTop / m_TempMaxScrollTop));
                that.scrollElement.style.top = m_TempScrollElementPosition + "px";
            }

            function applyStyles()
            {
                that.contentElement.style.overflow = "hidden";

                that.containerElement.style.position = "absolute";
                that.containerElement.style.width = "5px";
                that.containerElement.style.top = "0";
                that.containerElement.style.bottom = "0";
                that.containerElement.style.right = "0";

                that.scrollElement.style.width = "100%";
                that.scrollElement.style.height = "10px";
                that.scrollElement.style.borderRadius = "5px";
                that.scrollElement.style.backgroundColor = "rgba(0, 0, 0, .4)";
            }
        }

        function SmoothMovement()
        {
            var that = this;
            var
                m_MovementInterval = undefined,
                m_VelocityDelta = {x: 0, y: 0},
                m_TempResistanceResult = 0;

            this.resistance = 0.16;
            this.velocity = {x: 0, y: 0};

            this.onupdate;

            ///
            this.addForce = function (x, y)
            {
                that.velocity.x += x;
                that.velocity.y += y;

                activateMovement();
            };

            ///
            this.setVelocity = function (x, y)
            {
                that.velocity.x = x;
                that.velocity.y = y;

                activateMovement();
            };

            ///
            this.resetVelocity = function () { that.velocity.x = that.velocity.y = 0; deactivateMovement(); };

            ///
            this.stopMovement = function () { deactivateMovement(); };

            function activateMovement()
            {
                if(m_MovementInterval === undefined)
                {
                    deactivateMovement();
                    m_MovementInterval = setInterval(update, 1000 / 60);
                }
            }

            function deactivateMovement()
            {
                if(m_MovementInterval === undefined) return;
                clearInterval(m_MovementInterval);
                m_MovementInterval = undefined;
            }
            
            function update()
            {
                if(that.velocity.x == 0 && that.velocity.y == 0)
                {
                    deactivateMovement();
                    return;
                }

                m_VelocityDelta.x = resist(that.velocity.x, that.resistance);
                m_VelocityDelta.y = resist(that.velocity.y, that.resistance);

                that.velocity.x -= m_VelocityDelta.x;
                that.velocity.y -= m_VelocityDelta.y;

                if(Math.abs(that.velocity.x) < 0.1) that.velocity.x = 0;
                if(Math.abs(that.velocity.y) < 0.1) that.velocity.y = 0;

                if(type.isFunction(that.onupdate))
                    that.onupdate(m_VelocityDelta.x, m_VelocityDelta.y);
            }

            function resist(value, resistance)
            {
                if(value == 0) return 0;

                m_TempResistanceResult = value * resistance;
                return m_TempResistanceResult;
            }
        }

        function addScrollTouchListener(scroll)
        {
            addSelectMoveListener(scroll.contentElement, function (e) {
                scroll.moveY(-e.movementY);
            });
        }

        function addScrollListeners(scroll)
        {
            scroll.contentElement.addEventListener("wheel", function (e) {
                scroll.moveY(e.deltaY);
                e.preventDefault();
            });
        }

        function addScrollHandlerDragListener(scroll)
        {
            /*addSelectMoveListener(scroll.scrollElement, function (e) {
                console.log(e);
            });*/
        }

        function addSelectMoveListener(element, action)
        {
            var isPressed = false;

            element.addEventListener("mousedown", function () {
                if(isPressed) return;
                isPressed = true;

                window.removeEventListener("mousemove", action);
                window.addEventListener("mousemove", action);
            });
            window.addEventListener("mouseup", function () {
                isPressed = false;

                window.removeEventListener("mousemove", action);
            });
        }
    }

    return Scroll;

})();