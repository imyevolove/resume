String.format = function (text)
{
    /// Retrieve params from arguments
    /// Replace templates {index} to params[index]
    for(var i = 1; i < arguments.length; i++)
        text = text.replace(new RegExp("{*" + (i - 1) + "}", 'g'), arguments[i]);

    return text;
};
String.prototype.format = function ()
{
    var result = this.valueOf();

    /// Retrieve params from arguments
    /// Replace templates {index} to params[index]
    for(var i = 0; i < arguments.length; i++)
        result = result.replace(new RegExp("{*" + (i) + "}", 'g'), arguments[i]);

    return result;
};