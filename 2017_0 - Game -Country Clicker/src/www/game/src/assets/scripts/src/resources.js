var resources = (function () {

    function Resources()
    {
        this.get = function (file_path)
        {
            return assets.findFile(file_path);
        };
    }

    return new Resources();

})();