var ClickerBonus = (function () {

    var EVENTS = {
        PROGRESS_SCORE_CHANGE: "event.progress.score.change",
        PROGRESS_SCORE_ZERO: "event.progress.score.zero",
        PROGRESS_SCORE_THRESHOLD_CHANGE: "event.progress.score.threshold.change",
        PROGRESS_LEVEL_CHANGE: "event.progress.level.change",
        PROGRESS_LEVEL_UP: "event.progress.level.up",
        PROGRESS_LEVEL_DOWN: "event.progress.level.down"
    };

    function ClickerBonus()
    {
        var that = this;

        ///
        var
            m_EventsManager = new EventsSystem(),
            m_RecessionWaitTimeout,
            m_RecessionInterval;

        ///
        var
            m_ProgressLevel = 0,
            m_ProgressScore = 0,
            m_ProgressLevelScoreThreshold = 0;

        ///
        var multiplieSoundEffects = [
            new AudioSource("/assets/resources/audio/unlock_1.mp3"),
            new AudioSource("/assets/resources/audio/unlock_1.mp3"),
            new AudioSource("/assets/resources/audio/unlock_1.mp3"),
            new AudioSource("/assets/resources/audio/unlock_2.mp3"),
            new AudioSource("/assets/resources/audio/unlock_2.mp3")
        ];

        /// Options
        this.progressLevelMax = 5; /// 5 - by default
        this.defaultScoreValue = 10;
        this.recessionDelayTime = 2;

        /// Elements
        this.containerElement;
        this.valueElement;
        this.progressLineElement;

        initialization.call(this);
        function initialization()
        {
            m_EventsManager.addEventListener(EVENTS.PROGRESS_SCORE_ZERO,    hideContainer);
            m_EventsManager.addEventListener(EVENTS.PROGRESS_SCORE_ZERO,    redrawProgressLevel);
            m_EventsManager.addEventListener(EVENTS.PROGRESS_LEVEL_CHANGE,  redrawProgressLevel);
            m_EventsManager.addEventListener(EVENTS.PROGRESS_SCORE_CHANGE,  showContainer);
            m_EventsManager.addEventListener(EVENTS.PROGRESS_SCORE_CHANGE,  redrawProgressLevelScore);
            m_EventsManager.addEventListener(EVENTS.PROGRESS_SCORE_THRESHOLD_CHANGE,  redrawProgressLevelScore);
            m_EventsManager.addEventListener(EVENTS.PROGRESS_SCORE_ZERO,    redrawProgressLevelScore);

            m_EventsManager.addEventListener(EVENTS.PROGRESS_LEVEL_UP,      playSoundEffect)

            reset();
        }

        this.getMultiplierLevel = function ()
        {
            return Math.min(m_ProgressLevel + 1, that.progressLevelMax);
        };

        /// Functions
        this.trigger = function ()
        {
            if(m_ProgressLevelScoreThreshold == 0)
                updateProgressLevelScoreThresholdUp();

            stopRecession();
            checkLevelUp();
            changeScore(+1);
            startRecessionWaiting();
        };

        ///
        function startRecessionWaiting()
        {
            clearTimeout(m_RecessionWaitTimeout);
            m_RecessionWaitTimeout = setTimeout(startRecession, that.recessionDelayTime * 1000);
        }

        ///
        function startRecession()
        {
            m_RecessionInterval = setInterval(recessionScore, 1000 / 30);
        }

        ///
        function stopRecession()
        {
            clearInterval(m_RecessionInterval);
        }

        function reset()
        {
            changeScore(0, true);
            changeLevel(0, true);
            stopRecession();
        }

        ///
        function recessionScore()
        {
            if(m_ProgressScore <= 0)
            {
                if(m_ProgressLevel <= 0)
                {
                    reset();
                    m_EventsManager.dispatchEvent(EVENTS.PROGRESS_SCORE_ZERO);
                }
                else
                {
                    changeLevel(-1);
                    updateProgressLevelScoreThresholdDown();
                    m_EventsManager.dispatchEvent(EVENTS.PROGRESS_LEVEL_DOWN);
                }
                return;
            }

            changeScore(-1);
        }

        ///
        function playSoundEffect()
        {
            multiplieSoundEffects[m_ProgressLevel].play();
        }

        ///
        function redrawProgressLevelScore()
        {
            if(!that.progressLineElement) return;
            ///console.log(m_ProgressScore, m_ProgressLevel);
            that.progressLineElement.style.width = (m_ProgressScore > 0 || m_ProgressLevel > 0)
                ? 100 * (m_ProgressScore / m_ProgressLevelScoreThreshold) + "%"
                : 0;
        }

        ///
        function redrawProgressLevel()
        {
            if(!that.valueElement) return;
            that.valueElement.innerHTML = that.getMultiplierLevel() + "x";
        }

        ///
        function hideContainer()
        {
            if(!that.containerElement) return;
            that.containerElement.setAttribute("visibility", "hidden");
        }

        ///
        function showContainer()
        {
            if(!that.containerElement) return;
            that.containerElement.setAttribute("visibility", "visible");
        }

        ///
        function changeScore(value, changeFully)
        {
            m_ProgressScore = (changeFully === true) ? value : m_ProgressScore + value;
            m_ProgressScore = Math.max(0, Math.min(m_ProgressScore, m_ProgressLevelScoreThreshold));

            m_EventsManager.dispatchEvent(EVENTS.PROGRESS_SCORE_CHANGE);
        }

        ///
        function changeLevel(value, changeFully)
        {
            var newLevel = m_ProgressLevel + value;

            m_ProgressLevel = (changeFully === true) ? value : m_ProgressLevel + value;
            m_ProgressLevel = Math.max(0, Math.min(m_ProgressLevel, that.progressLevelMax - 1));

            if(newLevel != m_ProgressLevel) return false;

            m_EventsManager.dispatchEvent(EVENTS.PROGRESS_LEVEL_CHANGE);

            return true;
        }

        ///
        function updateProgressLevelScoreThresholdUp()
        {
            if(m_ProgressLevel >= that.progressLevelMax - 1) return;

            m_ProgressLevelScoreThreshold = getLevelScoreThreshold();
            m_ProgressScore = 0;

            m_EventsManager.dispatchEvent(EVENTS.PROGRESS_SCORE_THRESHOLD_CHANGE);
        }

        ///
        function updateProgressLevelScoreThresholdDown()
        {
            m_ProgressLevelScoreThreshold = getLevelScoreThreshold();
            m_ProgressScore = m_ProgressLevelScoreThreshold;

            m_EventsManager.dispatchEvent(EVENTS.PROGRESS_SCORE_THRESHOLD_CHANGE);
        }

        ///
        function checkLevelUp()
        {
            if(m_ProgressScore >= m_ProgressLevelScoreThreshold)
            {
                if(!changeLevel(+1)) return;
                updateProgressLevelScoreThresholdUp();

                m_EventsManager.dispatchEvent(EVENTS.PROGRESS_LEVEL_UP);
            }
        }

        ///
        function getLevelScoreThreshold()
        {
            return  that.defaultScoreValue * (1.6 ^ (Math.min(m_ProgressLevel + 1, 140) - 1) +
                    Math.min(m_ProgressLevel + 1, 140) - 1) * (1.15 ^ Math.max(m_ProgressLevel + 1 - 140, 0));
        }
    }

    return ClickerBonus;

})();

/// xGame.click();
/// particle.on("click", xGame.dispatchEvent(""));