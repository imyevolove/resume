var ScreenBehaviour = (function () {

    function ScreenBehaviour()
    {
        var that = this;

        this.rootDOMElement = undefined;

        this.show = function ()
        {
            if(!HTMLElement.prototype.isPrototypeOf(that.rootDOMElement)) return;
            that.rootDOMElement.setAttribute("screen-state", "show");
        };

        this.hide = function ()
        {
            if(!HTMLElement.prototype.isPrototypeOf(that.rootDOMElement)) return;
            that.rootDOMElement.setAttribute("screen-state", "hide");
        };
    }

    return ScreenBehaviour;

})();