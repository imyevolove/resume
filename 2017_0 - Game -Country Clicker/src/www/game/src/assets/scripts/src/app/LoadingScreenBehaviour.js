var LoadingScreenBehaviour = (function () {

    function LoadingScreenBehaviour()
    {
        var that = this;
        this.extend(ScreenBehaviour, arguments);

        this.rootDOMElement = xDom.get("loading-screen");

        initialization.call(this);
        function initialization()
        {
            that.hide();
        }
    }

    return LoadingScreenBehaviour;

})();