var xGame = (function () {

    function Game()
    {
        var that = this;
        var access_token = "";

        this.extend(Application);

        this.EVENTS = {
            PARTICLE_CLICK: "game.event.click"
        };
        this.events = new EventsSystem();

        var deferredSave = new DeferredAction();
        deferredSave.idle = 100;
        deferredSave.action = function ()
        {
            sendEarnMoney();
        };
        this.events.addEventListener(this.EVENTS.PARTICLE_CLICK, deferredSave.next);

        this.screens = {
            game: new GameScreenBehaviour(),
            auth: new AuthScreenBehaviour(),
            loading: new LoadingScreenBehaviour(),
            leaderboard: new LeaderBoardScreenBehaviour(),
            countrySelect: new CountrySelectScreenBehaviour()
        };

        this.clickerBonus = new ClickerBonus();
        this.clickerBonus.containerElement = xDom.get("clicker-bonus-multiplier");
        this.clickerBonus.valueElement = this.clickerBonus.containerElement.querySelector("._value");
        this.clickerBonus.progressLineElement = this.clickerBonus.containerElement.querySelector("._line");

        this.initialize = function ()
        {
            waterfall.create([

                /// Authorization
                function (next)
                {
                    that.auth(function (status, data) {
                        if(status)
                        {
                            that.screens.auth.hide();
                        }
                        else
                        {
                            that.screens.auth.setMessageText("Authorization failed");
                            console.log(data);
                        }

                        next(status);
                    });
                },

                /// Load Localization
                function ()
                {
                    localization.download(xData.country.getCode());
                    xData.country.addOnChangeIdentificationDataEventListener(function () {
                        localization.download(xData.country.getCode());
                    })
                }
            ]);
        };

        /// Authorization process
        this.auth = function (callback)
        {
            callback = (typeof callback == "function" ? callback : function () {});
            waterfall.create([function (next) {
                api.methods.authorize(function (response) {

                    /// Error
                    if(!response || !response["response"] || !response["response"]["access_token"]) return next(false, response);

                    /// Save token
                    access_token = response.response.access_token;
                    api.access_token = access_token;

                    next(true, response);
                });
            }, function (next) {
                api.methods.userGet(function (response) {
                    if(!response || !response["response"]) return next(false);

                    xData.player .money .set(response.response.balance);
                    xData.player .id = response.response.id;
                    xData.country.setBalanceData(response.response.country.balance, response.response.country.balance_level);
                    xData.country.setIdentificationData(response.response.country.id, response.response.country.code);

                    initializeGameEvents();
                    callback(true);
                });
            }], function (data) { callback(false, data); });
        };

        this.click = function ()
        {
            xData.player.increaseMoney(1 * that.clickerBonus.getMultiplierLevel());
            that.events.dispatchEvent(that.EVENTS.PARTICLE_CLICK);
        };

        this.updateCountryData = function ()
        {
            api.methods.getCountry(xData.country.getId(), function (response) {
                if(response["response"])
                {
                    xData.country.setBalanceData(response.response.balance, response.response.balance_level);
                }
            })
        };

        this. transferAllMoneyCountry = function (callback)
        {
            waterfall.create([
                    function (next)
                    {
                        sendEarnMoney(function (data) {
                            if(!data)
                            {
                                next(false);
                                return;
                            }

                            next(true, data);
                        });
                    },
                    function (next, money)
                    {
                        api.methods.balanceTransfer({receiver: "c" + xData.country.getId(), amount: money}, function (response) {
                            if(response && response["response"])
                            {
                                xData.player.money .set(response.response.balance);
                                xData.country.setBalanceData(response["response"].country.balance, response["response"].country.balance_level);

                                next(true);
                                return;
                            }

                            if(type.isFunction(callback)) callback(true);
                            next(false);
                        });

                    }],
                /// Error
                function ()
                {
                    if(type.isFunction(callback)) callback(false);
                });
        };

        function sendEarnMoney(callback)
        {
            if(xData.player.unsavedMoney.get() == 0)
            {
                if(type.isFunction(callback)) callback(xData.player.money.get());
                return;
            }

            var unsavedMoneyValue = xData.player.unsavedMoney.get();
            xData.player.unsavedMoney.set(0);

            api.methods.balanceIncrease(unsavedMoneyValue, function (response) {

                if(!response)
                {
                    xData.player.unsavedMoney.set(unsavedMoneyValue);
                    if(type.isFunction(callback)) callback(null);
                    return;
                }

                if(response["error"])
                {
                    if(response.error["code"] == 150)
                    {
                        xData.player.unsavedMoney.set(0);
                        xData.player.money.set(response.error.data.balance);

                        setTimeout(function () {
                            alert(response.error.message);
                        }, 1);
                    }

                    if(type.isFunction(callback)) callback(null);
                }
                else if(response["response"])
                {
                    /// Apply changes
                    xData.player.money.set(response.response.balance + xData.player.unsavedMoney.get());
                    if(type.isFunction(callback)) callback(response.response.balance);
                }
            });
        }

        function initializeGameEvents()
        {
            /// On Browser close
            window.addEventListener("beforeunload", function () {
                deferredSave.action();
            });

            setInterval(that.updateCountryData, 30 * 1000);

            xData.country.addOnChangeIdentificationDataEventListener(updateCountryData);
        }

        function updateCountryData()
        {
            api.methods.getCountry(xData.country.getId(), function (response) {
                if(!response || !response["response"]) return;
                xData.country.setBalanceData(response.response.balance, response.response.balance_level);
            })
        }
    }

    return new Game();

})();