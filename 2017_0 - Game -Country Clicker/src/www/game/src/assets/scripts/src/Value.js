var Value = (function () {

    function Value(value)
    {
        var EVENTS = {
            CHANGE: "on.change.event"
        };

        var
            m_Value = value,
            m_EventSystem = new EventsSystem();

        ///
        this.get = function ()
        {
            return m_Value;
        };

        ///
        this.set = function (value)
        {
            m_Value = value;

            m_EventSystem.dispatchEvent(EVENTS.CHANGE, m_Value);

            return m_Value;
        };

        ///
        this.getTypeName = function ()
        {
            return typeof m_Value;
        };

        ///
        this.addOnChangeEventListener = function (listener)
        {
            m_EventSystem.addEventListener(EVENTS.CHANGE, listener);
        };

        ///
        this.removeOnChangeEventListener = function (listener)
        {
            m_EventSystem.removeEventListener(EVENTS.CHANGE, listener);
        };
    }

    return Value;

})();