var VariativeAudio = (function ()
{
    function VariativeAudio()
    {
        var that = this;
        var m_Audios = [];

        this.addAudioSource = function (audioSource)
        {
            if(!type.is(audioSource, AudioSource)) throw new TypeError();
            m_Audios.push(audioSource);
        };

        this.play = function ()
        {
            if(!m_Audios.length) return;

            var audioSource = m_Audios[Math.randomRangeInt(0, m_Audios.length - 1)];
            audioSource.play();
        }
    }

    return VariativeAudio;

})();