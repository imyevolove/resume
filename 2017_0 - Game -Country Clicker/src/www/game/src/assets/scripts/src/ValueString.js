var ValueString = (function () {

    function ValueString(value)
    {
        var that = this;

        arguments[0] = typeof arguments[0] == "string" ? arguments[0] : 0;

        this.extend(Value, arguments);

        /// Override setter
        var original_set = this.set;
        this.set = function (value)
        {
            if(typeof value != "string")
                throw new Error("Type exceptions; Invalid Type");
            return original_set(value);
        };
    }

    return ValueString;

})();