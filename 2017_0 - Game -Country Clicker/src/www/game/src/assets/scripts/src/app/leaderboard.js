var leaderboard = (function () {

    function Leaderboard()
    {
        var that = this;
        var items = [];
        this.type = "";
        this.getItems = function () { return items; };
        this.update = function (callback)
        {
            callback = typeof callback == "function" ? callback : function () {};
            api.methods.getLeaderboardCountries({type: that.type, count: 200}, function (response) {
                if(!response || !response["response"])
                {
                    callback(null);
                    return;
                }

                switch(response.response.type)
                {
                    case "player":
                        items = sortPlayers(response.response.items);
                        break;
                    default:
                        items = sort(response.response.items);
                }

                callback(items);
            });
        };
    }

    function sortPlayers(arr)
    {
        return arr.sort(function (a, b)
        {
            var x = b.statistics.balance_level - a.statistics.balance_level;
            return x == 0 ? b.statistics.balance - a.statistics.balance : x;
        });
    }

    function sort(arr)
    {
        return arr.sort(function (a, b)
        {
            var x = b.balance_level - a.balance_level;
            return x == 0 ? b.balance - a.balance : x;
        });
    }

    return new Leaderboard();

})();