var Asset = (function () {

    var EVENTS = {
        "FILES_LOADED": "files.loaded",
        "ASSET_LOADED": "asset.loaded",
        "ASSET_LOADING_ERROR": "asset.loading.error"
    };

    var ASSET_STATE = {
        UNLOADED: 1 << 0,
        LOADING : 1 << 1,
        LOADED  : 1 << 2,
        BROKEN  : 1 << 3
    };

    function Asset(asset_url)
    {
        var
            m_AssetStatus   = ASSET_STATE.UNLOADED,
            m_AssetFilePath = asset_url,
            m_EventsManager = new EventsSystem(),
            m_AssetHeaderFile = [], m_AssetFiles = [];

        this.isLoaded  = function () { return m_AssetStatus == ASSET_STATE.LOADED; };
        this.isBroken  = function () { return m_AssetStatus == ASSET_STATE.BROKEN; };
        this.isLoading = function () { return m_AssetStatus == ASSET_STATE.LOADING; };

        ///
        this.getAssetFilePath = function ()
        {
            return m_AssetFilePath;
        };

        /// Request file from asset files
        this.getFile = function (file)
        {
            return m_AssetFiles[file] || undefined;
        };

        ///
        this.getScriptPaths = function ()
        {
            return m_AssetHeaderFile["scripts"];
        };

        ///
        this.requireScripts = function (callback)
        {
            if((m_AssetStatus & (ASSET_STATE.BROKEN | ASSET_STATE.UNLOADED)) > 0)
            {
                console.warn("Asset unloaded or broken; Require scripts is prevented");
                return;
            }

            if(m_AssetStatus != ASSET_STATE.LOADED)
            {
                var _callback = function ()
                {
                    m_EventsManager.removeEventListener(EVENTS.ASSET_LOADED, _callback);
                    callback();
                };
                m_EventsManager.addEventListener(EVENTS.ASSET_LOADED, _callback);

                console.warn("Asset in loading progress; Require scripts will be called when asset is loaded");
                return;
            }

            tryRequireScripts(callback);
        };

        /// Refresh asset data
        /// Download files
        this.Refresh = function (callback)
        {
            callback = typeof callback == "function" ? callback : function () {};

            m_AssetHeaderFile   = [];
            m_AssetFiles        = [];

            waterfall.create([
                function (next)
                {
                    downloadAssetHeaderFile(m_AssetFilePath, next);
                },
                function (next)
                {
                    downloadAssetFiles(m_AssetHeaderFile.files || [], function () {
                        m_EventsManager.dispatchEvent(EVENTS.FILES_LOADED);
                        next();
                    });
                },
                function (next)
                {
                    m_EventsManager.dispatchEvent(EVENTS.ASSET_LOADED);
                    m_AssetStatus = ASSET_STATE.LOADED;
                    callback(true);
                    next();
                }
            ], function () {
                m_EventsManager.dispatchEvent(EVENTS.ASSET_LOADING_ERROR);
                m_AssetStatus = ASSET_STATE.BROKEN;
                callback(false);
            });
        };

        ///
        function tryRequireScripts(callback)
        {
            var includedScripts = [].slice.call(document.getElementsByTagName('script'));
            includedScripts = includedScripts.map(function (script) { return script.getAttribute("src")});
            includedScripts = includedScripts.filter(function (src) { return !!src; });

            var requireScriptsQueue = Array.isArray(m_AssetHeaderFile["scripts"]) ? m_AssetHeaderFile["scripts"] : [];
            requireScriptsQueue = requireScriptsQueue.filter(function (src) {
                return includedScripts.indexOf(src) < 0;
            });

            requireNext(0);

            function requireNext(index)
            {
                if(index >= requireScriptsQueue.length)
                {
                    if(typeof callback == "function") callback();
                    return;
                }

                var script =  document.createElement("script");
                script.setAttribute("type", "text/javascript");
                script.src = requireScriptsQueue[index] + "?t=" + Date.now();
                script.onload = script.onerror = function () {
                    requireNext(++index);
                };
                script.appendTo(document.head);
            }
        }

        /// Try download each asset files one by one
        function downloadAssetFiles(assetFiles, endCallback)
        {
            var m_FIndex = -1;

            downloadNextFile();
            function downloadNextFile()
            {
                m_FIndex++;

                if(m_FIndex >= assetFiles.length)
                {
                    if(typeof endCallback == "function") endCallback();
                    return;
                }

                network.request({
                    method: "get",
                    url: m_AssetHeaderFile.dir_root + "/" + assetFiles[m_FIndex] + "?t=" + Date.now(),

                    success: fileCallbackSuccess,
                    fail: fileCallbackFail
                });
            }

            function fileCallbackSuccess(response)
            {
                m_AssetFiles[assetFiles[m_FIndex]] = response.response;
                downloadNextFile();
            }

            function fileCallbackFail()
            {
                console.warn("File '{0}' in asset '{1}' not loaded".format(assetFiles[m_FIndex], m_AssetFilePath));
                downloadNextFile();
            }
        }

        /// Try download asset header file on presented url
        function downloadAssetHeaderFile(url, callback)
        {
            callback = typeof callback == "function" ? callback : function () {};
            network.request({
                url: url,

                /// Successfully handler
                success: function (response)
                {
                    try
                    {
                        /// Parse asset file
                        /// Asset header have json structure by default
                        m_AssetHeaderFile = JSON.parse(response.response);
                        callback(true);
                    }
                    catch(exception)
                    {
                        /// Asset was not parsed
                        /// File structure contain errors or wrong format
                        console.error("Asset parse exception");
                        callback(false);
                    }
                },

                /// Failure handler
                fail: function ()
                {
                    /// Network error or file not found on presented url
                    console.error("Asset not found in '{0}'".format(url));
                    callback(false);
                }
            })
        }
    }

    return Asset;

})();