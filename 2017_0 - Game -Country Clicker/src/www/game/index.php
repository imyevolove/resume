<?php

function fmt($filepath)
{
    return file_exists($filepath) ? filemtime($filepath) : 0;
}

function GetGameFileSrc($filepath)
{
    return "/assets/" . $filepath . "?t=" . fmt(DIR_GAME . "/src/assets/" . $filepath);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Country Clicker v0.1</title>

        <!-- META -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <!-- STYLES -->
        <?php
        Renderer::RenderIncludeCss(GetGameFileSrc("fonts/blogger/blogger-font.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/generic.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/country.select.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/loading.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/leaderboard.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/auth.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/app.clicker_bonus.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/theads.css"));
        Renderer::RenderIncludeCss(GetGameFileSrc("styles/app.css"));
        ?>

        <!-- SCRIPTS -->
        <?php
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/extensions/ObjectExtend.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/extensions/StringFormatter.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/extensions/HTMLElementControl.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/extensions/HTMLElementEvent.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/extensions/LocationURLParser.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Math.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/type.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Enum.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/AudioSource.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/html/Scroll.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/waterfall.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Template.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/DeferredAction.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/xDom.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Value.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/ValueNumber.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/ValueString.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/EventsSystem.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/xMVC.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/ParticlesSystem.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/network.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Asset.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Update.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Animation.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/assets.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/resources.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Application.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/Module.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/dev/assetCombine.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/src/modules/AdsModule.js"));
        Renderer::RenderIncludeJSScript(GetGameFileSrc("scripts/main.js"));
        ?>

        <?php echo \Game\Platforms\Platforms::GetCurrentPlatform()->GetAdditionalHeaderText(); ?>
    </head>
    <body>
        <div class="mixed-container">
            <div class="con_application-wrapper">
                <?php Renderer::RenderHTML(DIR_GAME . "/src/assets/render_partials/_loading.html"); ?>
                <?php Renderer::RenderHTML(DIR_GAME . "/src/assets/render_partials/_auth.html"); ?>
                <?php Renderer::RenderHTML(DIR_GAME . "/src/assets/render_partials/_country.selection.html"); ?>
                <?php Renderer::RenderHTML(DIR_GAME . "/src/assets/render_partials/_leaderboard.html"); ?>
                <?php Renderer::RenderHTML(DIR_GAME . "/src/assets/render_partials/_game.html"); ?>
            </div>
            <div class="con_links-wrapper" id="ads-root-container">
                <div class="ads-entity-size-blank"></div>
            </div>
        </div>
    </body>
</html>