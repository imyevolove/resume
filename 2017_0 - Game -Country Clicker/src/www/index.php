<?php session_start();

define('TIMEZONE', 'Europe/Moscow');
date_default_timezone_set(TIMEZONE);

define("DIR_ROOT", __DIR__);
define("DIR_GAME", DIR_ROOT . "/game");
define("DIR_SERVER_ROOT", DIR_ROOT . "/server");
define("DIR_DATA", DIR_ROOT . "/server/data");

require_once DIR_SERVER_ROOT . "/src/path.php";
require_once DIR_SERVER_ROOT . "/src/autoload.php";

require_once DIR_SERVER_ROOT . "/main.php";