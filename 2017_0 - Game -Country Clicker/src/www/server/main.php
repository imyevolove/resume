<?php

use Phroute\Phroute\RouteCollector;
use Api\API;

$app_config = Config::Get("app");

// ROUTER
$router = new RouteCollector();

$router->any('/game', function() {
    require_once "game/index.php";
});

$router->any('/game.vk', function() {
    $_GET["platform"] = "vk";
    header('Location: /game?' . http_build_query($_GET));
});

// routes here
if(isset($app_config) && $app_config["dev_mode"] == true)
{
    $router->any('/emulator', function() {
        Renderer::Render("game/emulator/vk.html");
    });
}

$router->any('/services/api/{method}', function($method = null) {
    try
    {
        API::Execute($method);
    }
    catch (Exception $exception)
    {
        Renderer::Render("pages/errors/404.html");
    }
});

// DISPATCHER
try
{
    $dispatcher = new Phroute\Phroute\Dispatcher($router->getData());
    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
}

// 404 HERE
catch (Exception $e)
{
    require_once "pages/errors/404.html";
}
