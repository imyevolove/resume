<?php namespace Storage;

class Database
{
    private static  $mysqli = null;
    private static  $multi_query_list = array();

    ///
    public static function Query($query_str)
    {
        if(self::$mysqli == null) self::Connect();

        self::ClearResults();

        $result = self::$mysqli->query($query_str);
        return $result;
    }

    ///
    public static function BeginTransaction() : void
    {
        if(self::$mysqli == null) self::Connect();
        self::$mysqli->autocommit(FALSE);
    }

    ///
    public static function Commit() : void
    {
        if(self::$mysqli == null) self::Connect();
        self::$mysqli->commit();
    }

    ///
    public static function Rollback() : void
    {
        if(self::$mysqli == null) self::Connect();
        self::$mysqli->rollback();
    }

    ///
    public static function ClearMultiQueryList()
    {
        self::$multi_query_list = array();
    }

    ///
    public static function PushMultiQuery(string $query)
    {
        array_push(self::$multi_query_list, $query);
    }

    ///
    public static function ExecuteMultiQuery()
    {
        if(self::$mysqli == null) self::Connect();

        $result = self::$mysqli->multi_query(join(";", self::$multi_query_list));

        return $result;
    }

    ///
    public static function GetError() : string
    {
        if(is_null(self::$mysqli)) return "";
        return self::$mysqli->error;
    }

    ///
    public static function CreateDatabase($db_name) : bool
    {
        $db_config = \Config::Get("db");
        $mysqli = new \mysqli($db_config["host"], $db_config["user"], $db_config["password"]);

        $result = $mysqli->query("CREATE DATABASE IF NOT EXISTS $db_name DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");

        if(!$result) var_dump($mysqli->error);

        return $result;
    }

    ///
    private static function ClearResults()
    {
        do {
            self::$mysqli->use_result();
        }while( self::$mysqli->more_results() && self::$mysqli->next_result() );
    }

    ///
    private static function Connect() : void
    {
        $db_config = \Config::Get("db");
        self::$mysqli = new \mysqli($db_config["host"], $db_config["user"], $db_config["password"], $db_config["db_name"]);

        if (self::$mysqli->connect_error)
        {
            die('Connect Error (' . self::$mysqli->connect_errno . ') ' . self::$mysqli->connect_error);
        }

        self::$mysqli->query("SET timezone = '" . TIMEZONE . "'");
    }
}