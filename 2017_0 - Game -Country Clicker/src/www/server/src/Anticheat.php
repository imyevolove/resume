<?php

use Entities\User;

class Anticheat
{
    const USER_INCREMENTAL_BALANCE_VALUE_MAX = 500;
    const USER_INCREMENTAL_BALANCE_TIME_DIFFERENCE_MAX = 1; /// 1 sec

    /// Checking the possibility to make the operation of adding value to the balance
    public static function VerifyUserIncrementBalanceOperation(User $user, int $value)
    {
        $time_difference = time() - $user->GetLastActivityTime();
        $time_difference = $time_difference == 0 ? 1 : $time_difference;

        $max_value = self::USER_INCREMENTAL_BALANCE_VALUE_MAX * $time_difference;
        return $max_value >= $value;
    }
}