<?php namespace Game;

class Balance
{
    public $balance;
    public $balanceLevel;

    public function __construct(int $balance, int $balanceLevel)
    {
        $this->balance = $balance;
        $this->balanceLevel = $balanceLevel;
    }
}