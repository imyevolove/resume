<?php namespace Game\Statistics;


use Entities\User;
use Game\BalanceNormalizer;
use Storage\Database;

class UserStatistics implements IStatisticsEntity
{
    private $data = array();
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $loaded_data = self::GetUserStatisticsData($user);
        $this->data  = $loaded_data;
    }

    public function GetUser() : ?User
    {
        return $this->user;
    }

    public function GetBalance() : int
    {
        if (!is_array($this->data) || !array_key_exists("balance", $this->data)) return 0;
        return intval($this->data["balance"]);
    }

    public function GetBalanceLevel() : int
    {
        if (!is_array($this->data) || !array_key_exists("balance_level", $this->data)) return 0;
        return intval($this->data["balance_level"]);
    }

    public function GetJSONData() : array
    {
        if (!is_array($this->data) || !array_key_exists("balance_level", $this->data)) return array();
        return array(
            "balance" => $this->GetBalance(),
            "balance_level" => $this->GetBalanceLevel()
        );
    }

    public function IncrementBalance(int $value) : bool
    {
        if(!isset($this->data) || !isset($this->user)) return false;

        $value = abs($value);
        $balance = BalanceNormalizer::NormalizeIncrementBalance($this->GetBalance(), $this->GetBalanceLevel(), $value);

        $status = Database::Query("UPDATE statistics SET balance = " . $balance->balance . ", balance_level = " . $balance->balanceLevel . "  WHERE user_id = " . $this->user->GetID());
        return $status;
    }

    private static function GetUserStatisticsData(User $user) : ?array
    {
        $user_id = $user->GetID();
        $user_statistics = self::LoadUserStatisticsData($user_id);

        if(!isset($user_statistics))
        {
            $creating_row_status = self::CreateUserStatisticsRow($user_id);
            if($creating_row_status) $user_statistics = self::LoadUserStatisticsData($user_id);
        }

        if(!isset($user_statistics)) return null;

        return (array)$user_statistics;
    }

    private static function LoadUserStatisticsData(int $user_id)
    {
        $query_result = Database::Query("SELECT * FROM statistics WHERE user_id = " . $user_id);

        if((empty($query_result) || is_null($query_result) || $query_result->num_rows == 0)) return null;
        return $query_result->fetch_assoc();
    }

    private static function CreateUserStatisticsRow(int $user_id) : bool
    {
        $query_str = "INSERT INTO statistics (user_id) VALUES ('$user_id')";
        $query_result = Database::Query($query_str);

        return $query_result;
    }
}