<?php namespace Game\Statistics;

use Entities\User;
use Storage\Database;

class Statistics
{
    public static function GetUserStatistics(User $user) : UserStatistics
    {
        return new UserStatistics($user);
    }
}