<?php namespace Game;

class BalanceNormalizer
{
    public static function NormalizeIncrementBalance(int $balance, int $balanceLevel, int $incrementBalance, int $incrementBalanceLevel = 0)
    {
        $incrementBalance       = abs($incrementBalance);
        $incrementBalanceLevel  = abs($incrementBalanceLevel);

        $difference = (PHP_INT_MAX - $balance);

        $balance_level_new  = $balanceLevel + $incrementBalanceLevel;
        $balance_new        = $balance;

        if ($incrementBalance > $difference)
        {
            $balance_level_new++;
            $balance_new = $incrementBalance - $difference;
        }
        else
        {
            $balance_new = $balance + $incrementBalance;
        }

        return new Balance($balance_new, $balance_level_new);
    }
}