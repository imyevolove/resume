<?php namespace Game\Platforms;


use Entities\User;

class VKPlatform implements IPlatform
{
    public function GetName()   : string { return "Vkontakte";}
    public function GetID()     : string { return "vk"; }

    public function GetAdditionalHeaderText(): string
    {
        return
            "<script src=\"/assets/scripts/src/modules/VKModule.js\"  type=\"text/javascript\" defer></script>";
    }

    ///
    public function Authorize() : ?User
    {
        try
        {
            /// Validate input data
            /// Return null user if token is invalid
            if(!self::IsTrueToken()) return null;

            /// Try load user data
            $user = \AuthorizationService::AuthorizeSocial($_GET["viewer_id"], $_GET["platform"]);

            if(is_null($user))
            {
                $user = \AuthorizationService::RegisterSocialUser($_GET["viewer_id"], $_GET["platform"]);

                /// User register successfully
                /// Try change country by social input data
                if(!is_null($user))
                {
                    /// Catch request errors
                    /// Change language operation is not priory
                    try
                    {
                        /// Request user data from vk social api
                        $user_vk_data = self::RequestVKUserProfile($_GET["viewer_id"]);

                        if(isset($user_vk_data))
                        {
                            $user_vk_country_id = $user_vk_data->country;
                            $user_vk_profile_name = $user_vk_data->first_name . " " .  $user_vk_data->last_name;

                            if(strlen($user_vk_profile_name) <= 1)
                                $user_vk_profile_name = "PLAYER_" . $user->GetID();

                            /// Change player name received from vk
                            $user->ChangeName($user_vk_profile_name);

                            if(isset($user_vk_country_id))
                            {
                                /// Country vk id received
                                /// Try obtain iso country id by vk country id
                                if($user_vk_country_id >= 0)
                                {
                                    /// Search iso country id
                                    $iso_country_id = self::SearchCountryByVKCountryID($user_vk_country_id);

                                    /// Country id founded
                                    /// Change user country
                                    if($iso_country_id >= 0) $user->ChangeCountry($iso_country_id);
                                }
                            }
                        }

                    } catch(\Exception $exception) { }
                }
            }

            return $user;
        }
            /// Exception error. Return null result
        catch(\Exception $exception) { return null; }
    }

    ///
    private static function RequestVKUserProfile($user_id)
    {
        try
        {
            /// Request query string
            $query = http_build_query(array(
                "user_ids"      => $user_id,
                "fields"        => "country"
            ));

            /// Request user country from vk
            $response_body = file_get_contents("https://api.vk.com/method/users.get?" . $query);

            if(!isset($response_body)) return null;

            $response_json = json_decode($response_body);

            if(
                !isset($response_json) ||
                !isset($response_json->response) ||
                !isset($response_json->response[0]) ||
                !isset($response_json->response[0]->country)
            ) return -1;
            return $response_json->response[0];

        } catch(\Exception $exception) { return null; }
    }

    ///
    private static function SearchCountryByVKCountryID(int $vk_country_id) : int
    {
        try
        {
            $vk_countries_text = file_get_contents(normalize_path(DIR_DATA . "/vk.countries.json"));
            $vk_countries = json_decode($vk_countries_text);

            foreach($vk_countries as $vk_country)
            {
                if($vk_country->vk_id == $vk_country_id)
                    return $vk_country->id;
            }

            return -1;
        } catch(\Exception $exception) { return -1; }
    }

    ///
    private function IsTrueToken() : bool
    {
        $vk_config = \Config::Get("vk");
        $auth_key = md5($vk_config["app_id"] . '_' . $_GET["viewer_id"] . '_' . $vk_config["app_secret"]);

        return $auth_key == $_GET["auth_key"];
    }
}