<?php namespace Game\Platforms;

define("DIR_GAME_PLATFORMS", DIR_GAME . "/platforms");

class Platforms
{
    private static $platforms = array();

    public static function InitializePlatforms()
    {
        self::$platforms["WEB"] = new WEBPlatform();
        self::$platforms["VK"]  = new VKPlatform();
    }

    public static function GetPlatformByID(string $id) : ?IPlatform
    {
        $id = strtoupper($id);

        $platformHandler = array_key_exists($id, self::$platforms) ? self::$platforms[$id] : null;
        return $platformHandler;
    }

    public static function GetCurrentPlatform() : IPlatform
    {
        $platformKey = isset($_GET["platform"]) ? $_GET["platform"] : "web";
        $platformKey = strtoupper($platformKey);

        $platformHandler = array_key_exists($platformKey, self::$platforms) ? self::$platforms[$platformKey] : self::GetDefaultPlatform();
        return $platformHandler;
    }

    private static function GetDefaultPlatform()
    {
        return self::$platforms["WEB"];
    }
}
Platforms::InitializePlatforms();