<?php namespace Game\Platforms;


use Entities\User;

class WEBPlatform implements IPlatform
{
    public function GetName()   : string { return "Browser";}
    public function GetID()     : string { return "web"; }

    public function GetAdditionalHeaderText(): string
    {
        return "";
    }

    public function Authorize(): ?User
    {
        throw new \Exception("NOT AVAILABLE");
    }
}