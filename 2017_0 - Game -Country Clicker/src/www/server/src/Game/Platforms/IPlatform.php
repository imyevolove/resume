<?php namespace Game\Platforms;

use Entities\User;

interface IPlatform
{
    public function GetName() : string;
    public function GetID() : string;
    public function GetAdditionalHeaderText() : string;
    public function Authorize() : ?User;
}