<?php

use Entities\Country;
use Storage\Database;

class Localization
{
    public const DEFAULT_LOCALIZATION_CODE = "US";

    public static function GetDefaultPackage()
    {
        return self::GetPackage(self::DEFAULT_LOCALIZATION_CODE);
    }

    public static function GetPackage(?string $localization_code)
    {
        $file_path = self::GetLocalizationFilePath($localization_code);
        if(!file_exists($file_path)) return null;

        try
        {
            $package = file_get_contents($file_path);
            $package = json_decode($package);

            return $package;
        }
        catch(Exception $exception) { return null; }
    }

    private static function GetLocalizationFilePath($localization_code) : string
    {
        return normalize_path(DIR_DATA . "/localization/" . $localization_code . ".json");
    }
}