<?php

use Storage\Database;
use Entities\User;

class AuthorizationService
{
    private static $authorized_user = null;

    ///
    public static function GetAuthorizedUser() : ?User
    {
        return self::$authorized_user;
    }

    ///
    public static function IsAuthorized() : bool
    {
        return !is_null(self::$authorized_user);
    }

    ///
    public static function WriteUserToSession()
    {
        if(is_null(self::$authorized_user)) return;
        $_SESSION["user_token"] = self::$authorized_user->GetToken();
    }

    ///
    public static function AuthorizeBySession() : ?User
    {
        if(empty($_SESSION["user_token"]) || !is_string($_SESSION["user_token"])) return null;
        return self::AuthorizeByToken($_SESSION["user_token"]);
    }

    ///
    public static function AuthorizeByToken($token) : ?User
    {
        $query_str = "SELECT * FROM users WHERE token = '" . $token . "'";
        $query_result = Database::Query($query_str);

        if(empty($query_result) || is_null($query_result) || $query_result->num_rows == 0) return null;

        $user = new User($query_result->fetch_assoc());

        if(!is_null($user))
        {
            self::$authorized_user = $user;
            $user->RegisterActivityTime();
        }
        return $user;
    }

    ///
    public static function AuthorizeSocial($user_id, $platform) : ?User
    {
        $query_str = "SELECT * FROM users WHERE platform = '" . $platform . "' and user_id = '" . $user_id . "'";
        $query_result = Database::Query($query_str);

        if(empty($query_result) || is_null($query_result) || $query_result->num_rows == 0) return null;

        $user = new User($query_result->fetch_assoc());

        if(!is_null($user))
        {
            self::$authorized_user = $user;
            $user->RegisterActivityTime();
        }
        return $user;
    }

    ///
    public static function RegisterSocialUser($user_id, $platform) : ?User
    {
        $query_str = "INSERT INTO users (user_id, platform, token) VALUES ('" . $user_id. "', '" . $platform . "', '" . self::GenerateToken($user_id, $platform) . "')";
        $query_result = Database::Query($query_str);

        if(!$query_result) return null;
        return self::AuthorizeSocial($user_id, $platform);
    }

    ///
    private static function GenerateToken() : string
    {
        $token = uniqid(rand(), true);

        foreach (func_get_args() as $arg)
        {
            if(is_string($arg) || is_numeric($arg))
            {
                $token .= "_" . $arg;
            }
        }

        $token = hash("sha256", $token, false);

        foreach (func_get_args() as $arg)
        {
            if(is_string($arg) || is_numeric($arg))
            {
                $token .= "_" . $arg;
            }
        }

        return $token;
    }
}