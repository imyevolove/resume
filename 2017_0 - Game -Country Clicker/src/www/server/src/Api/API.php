<?php  namespace Api;

require_once "ApiErrors.php";

define("DIR_API_METHODS", normalize_path(__DIR__ . "/Methods"));

class API
{
    ///
    public static function Execute(string $method_name)
    {
        try
        {
            $method_public_file_path    = normalize_path(DIR_API_METHODS . "/public/"    . $method_name . ".php");
            $method_protected_file_path = normalize_path(DIR_API_METHODS . "/protected/" . $method_name . ".php");
            $method_file_path = "";

            $user = null;

            if(file_exists($method_protected_file_path))
            {
                if(empty($_GET["access_token"]) || !is_string($_GET["access_token"]))
                    self::ResponseErrorTemplate(API_ERROR_INVALID_TOKEN);

                $user = \AuthorizationService::AuthorizeByToken(htmlspecialchars($_GET["access_token"]));

                if(!\AuthorizationService::IsAuthorized())
                    self::ResponseErrorTemplate(API_ERROR_INVALID_TOKEN);

                $method_file_path = $method_protected_file_path;
            }
            else
            {
                if(!file_exists($method_public_file_path))
                    throw new \Exception("Handler with id '" . $method_name . "' is not defined");
                $method_file_path = $method_public_file_path;
            }

            require_once $method_file_path;

            $method_class_name = str_replace(".", " ", $method_name);
            $method_class_name = ucwords($method_class_name);
            $method_class_name = str_replace(" ", "", $method_class_name);

            $method = new $method_class_name();
            $method->Execute();

            throw new \Exception("Unprocessed api method; Fix it");
        }
        catch(\Exception $ex)
        {
            self::ResponseErrorTemplate(API_ERROR_UNKNOWN_ERROR, (object)array("error_message" => $ex->getMessage()));
        }
    }

    ///
    public static function ResponseError(int $code, string $message, ?\stdClass $data = null)
    {
        $r_data = array(
            "error" => array(
                "code" => $code,
                "message" => $message
            )
        );

        if(!empty($data) && !is_null($data))
            $r_data["error"]["data"] = $data;

        self::Response($r_data);
    }

    ///
    public static function ResponseErrorTemplate(array $template, ?\stdClass $data = null)
    {
        self::ResponseError($template["code"], $template["message"], $data);
    }

    ///
    public static function ResponseOK(array $data)
    {
        $r_data = array(
            "response" => $data
        );

        self::Response($r_data);
    }

    ///
    public static function Response(array $data)
    {
        header('Content-Type: application/json');

        $text = json_encode((object)$data, JSON_UNESCAPED_UNICODE);
        echo $text;

        exit();
    }

}