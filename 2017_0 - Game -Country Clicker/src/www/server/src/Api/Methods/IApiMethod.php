<?php namespace Api\Methods;

interface IApiMethod
{
    public function Execute() : void;
}