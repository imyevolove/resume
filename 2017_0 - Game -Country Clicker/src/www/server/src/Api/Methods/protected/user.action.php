<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class UserAction implements IApiMethod
{
    public function Execute() : void
    {
        try
        {
            if(!AuthorizationService::IsAuthorized())
                API::ResponseError(API_ERROR_AUTHORIZATION_INVALID_DATA["code"], API_ERROR_AUTHORIZATION_INVALID_DATA["message"]);

            if(!isset($_GET["method"]))
                API::ResponseError(API_ERROR_INVALID_INPUT_DATA["code"], API_ERROR_INVALID_INPUT_DATA["message"]);

            switch ($_GET["method"])
            {
                case "save":
                    if(!isset($_GET["balance"]))
                        API::ResponseError(API_ERROR_INVALID_INPUT_DATA["code"], API_ERROR_INVALID_INPUT_DATA["message"]);

                    $balance = intval($_GET["balance"]);

                    $user = AuthorizationService::GetAuthorizedUser();
                    $status = $user->SetAndSaveData(array(
                        "balance" => $balance
                    ));

                    if($status)
                    {
                        API::ResponseOK($user->GetJSONData());
                    }
                    else
                    {
                        API::ResponseError(API_ERROR_UNKNOWN_ERROR["code"], API_ERROR_UNKNOWN_ERROR["message"]);
                    }

                break;
                default:
                    API::ResponseError(API_ERROR_INVALID_INPUT_DATA["code"], API_ERROR_INVALID_INPUT_DATA["message"]);
                    break;
            }
        }
        catch(\Exception $exception)
        {
            API::ResponseError(API_ERROR_UNKNOWN_ERROR["code"], API_ERROR_UNKNOWN_ERROR["message"]);
        }
    }
}