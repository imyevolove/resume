<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class BalanceTransfer implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized())
            API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);

        if(empty($_GET["receiver"]) || empty($_GET["amount"]))
            API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        $receiver   = $_GET["receiver"];
        $amount     = intval($_GET["amount"]);

        if($amount <= 0)
            API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        $receiver_type = substr($receiver, 0, 1);
        $receiver_id = intval(substr($receiver, 1, strlen($receiver)));

        if($receiver_type != "c")
            API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        $user    = AuthorizationService::GetAuthorizedUser();
        $country = Country::GetCountry($receiver_id);

        if(is_null($country))
            API::ResponseErrorTemplate(API_ERROR_COUNTRY_NOT_FOUND);

        if($user->GetBalance() < $amount)
            API::ResponseErrorTemplate(API_ERROR_USER_NOT_ENOUGH_MONEY);

        Database::BeginTransaction();
        $q1 = $country->IncreaseBalance($amount);
        $q2 = Database::Query("UPDATE users SET balance = balance - " . $amount . " WHERE id = " . $user->GetID());

        if(!!$q1 && !!$q2)
        {
            Database::Commit();
            API::ResponseOK(array(
                "balance" => $user->GetBalance() - $amount,
                "country" => (object)$country->GetJSONData()
            ));
        }
        else
        {
            Database::Rollback();
            API::ResponseErrorTemplate(API_ERROR_UNKNOWN_ERROR);
        }

        $data = $user->GetData();

        $country_data = Country::GetCountry(intval($data["country_id"]));
    }
}