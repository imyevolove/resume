<?php

use Api\API;
use Api\Methods\IApiMethod;

class BalanceIncrease implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized())
        API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);

        if(!isset($_GET["value"]))
            API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        $user = AuthorizationService::GetAuthorizedUser();
        $inc_value = intval($_GET["value"]);

        if($inc_value <= 0)
            API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        /// file_put_contents(DIR_DATA . "/lg.txt", $user->GetLastActivityTime() . " - " . $time . "\n", FILE_APPEND);

        if(!Anticheat::VerifyUserIncrementBalanceOperation($user, $inc_value))
            Api::ResponseErrorTemplate(API_ERROR_SECURITY_OPERATION_REJECTED, (object)array(
                "balance" => $user->GetBalance()
            ));

        $incrementBalanceStatus = $user->IncrementBalance($inc_value);

        if($incrementBalanceStatus)
        {
            API::ResponseOK($user->GetJSONData());
        }
        else
        {
            API::ResponseErrorTemplate(API_ERROR_UNKNOWN_ERROR);
        }
    }
}