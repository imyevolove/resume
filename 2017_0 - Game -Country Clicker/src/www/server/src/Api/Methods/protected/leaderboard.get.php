<?php

use Api\API;
use Api\Methods\IApiMethod;
use Social\Leaderboard;

class LeaderboardGet implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized()) API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);

        $limit_property = empty($_GET["count"]) ? 0 : intval($_GET["count"]);
        $type_property = empty($_GET["type"]) ? Leaderboard::LEADERBOARD_TYPES[0] : $_GET["type"];
        $type_property = Leaderboard::NormalizeLeaderboardType($type_property);

        try
        {
            $items = Leaderboard::GetLeaderboard($type_property, $limit_property);
            API::ResponseOK(array("type" => $type_property, "items" => $items));
        }
        catch (Exception $exception)
        {
            Api::ResponseErrorTemplate(API_ERROR_UNKNOWN_ERROR);
        }
    }
}