<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class UserGet implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized())
            API::ResponseError(API_ERROR_AUTHORIZATION_INVALID_DATA["code"], API_ERROR_AUTHORIZATION_INVALID_DATA["message"]);

        $user = AuthorizationService::GetAuthorizedUser();

        API::ResponseOK($user->GetJSONData());
    }
}