<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class LocalizationGet implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized())
            API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);

        $localization_code = isset($_GET["country_code"]) ? $_GET["country_code"] : null;

        if(!isset($localization_code))
        {
            $country_id = AuthorizationService::GetAuthorizedUser()->GetCountryID();
            $country = Country::GetCountry($country_id);

            if(!isset($country)) API::ResponseErrorTemplate(API_ERROR_COUNTRY_NOT_FOUND);

            $localization_code = $country->GetCode();
        }

        $localization_package = Localization::GetPackage($localization_code);
        if(!isset($localization_package)) $localization_package = Localization::GetDefaultPackage();

        if(!isset($localization_package))
            API::ResponseErrorTemplate(API_ERROR_LOCALIZATION_NOT_FOUND);

        API::ResponseOK((array)$localization_package);
    }
}