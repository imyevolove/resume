<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class CountryChange implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized()) API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);
        if(empty($_GET["country_id"])) API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        $country_id     = intval($_GET["country_id"]);
        $change_status  = AuthorizationService::GetAuthorizedUser()->ChangeCountry($country_id);

        API::ResponseOK(array(
            "status" => $change_status
        ));
    }
}