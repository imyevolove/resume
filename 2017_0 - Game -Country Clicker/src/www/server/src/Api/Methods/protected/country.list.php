<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class CountryList implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized()) API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);

        $countries = Country::GetCountries();
        $is_minimal_details = !!(isset($_GET["view"]) && $_GET["view"] == "minimal");

        $data = array();
        $data_item = null;

        foreach ($countries as $country)
        {
            $data_item = $country->GetJSONData();

            if($is_minimal_details)
            {
                unset($data_item["balance"]);
                unset($data_item["players_count"]);
            }

            array_push($data, $data_item);
        }

        API::ResponseOK(array("items" => $data));
    }
}