<?php

use Api\API;
use Api\Methods\IApiMethod;
use Entities\Country;
use Storage\Database;

class CountryGet implements IApiMethod
{
    public function Execute() : void
    {
        if(!AuthorizationService::IsAuthorized()) API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);
        if(empty($_GET["id"])) API::ResponseErrorTemplate(API_ERROR_INVALID_INPUT_DATA);

        $country = Country::GetCountry(intval($_GET["id"]));

        if(!isset($country)) API::ResponseErrorTemplate(API_ERROR_COUNTRY_NOT_FOUND);

        API::ResponseOK($country->GetJSONData());
    }
}