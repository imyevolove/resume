<?php

use Api\API;
use Api\Methods\IApiMethod;
use Game\Platforms\Platforms;

class Auth implements IApiMethod
{
    public function Execute() : void
    {
        $platform = Platforms::GetCurrentPlatform();

        if(!isset($platform))
            API::ResponseErrorTemplate(API_ERROR_UNKNOWN_ERROR);

        if(!$platform->Authorize())
            API::ResponseErrorTemplate(API_ERROR_AUTHORIZATION_INVALID_DATA);

        API::ResponseOK(array("access_token" => AuthorizationService::GetAuthorizedUser()->GetToken()));
    }
}