<?php namespace Api;

define("API_ERROR_UNKNOWN_ERROR", array("code" => -1, "message" => "Unknown error"));
define("API_ERROR_INVALID_TOKEN", array("code" => 5, "message" => "Invalid token"));
define("API_ERROR_AUTHORIZATION_INVALID_DATA", array("code" => 10, "message" => "Authorization fail; Invalid data"));
define("API_ERROR_INVALID_INPUT_DATA", array("code" => 11, "message" => "Invalid input data"));
define("API_ERROR_COUNTRY_NOT_FOUND", array("code" => 1000, "message" => "Country not found"));
define("API_ERROR_USER_NOT_ENOUGH_MONEY", array("code" => 1001, "message" => "Not enough money"));
define("API_ERROR_LOCALIZATION_NOT_FOUND", array("code" => 1111, "message" => "Localization not found"));
define("API_ERROR_SECURITY_OPERATION_REJECTED", array("code" => 150, "message" => "Operation rejected the security system"));
