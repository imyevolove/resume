<?php namespace Entities;


interface IEntity
{
    public function GetShortID() : string;
    public function GetJSONData() : array;
}