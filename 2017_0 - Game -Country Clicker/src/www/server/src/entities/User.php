<?php namespace Entities;

use Game\Platforms\Platforms;
use Game\Statistics\UserStatistics;
use Storage\Database;

class User implements IEntity
{
    private $data = array();
    private $_user_statistics;

    public function __construct($data)
    {
        $this->data = (array)$data;
    }

    public function SaveToSession()
    {
        $_SESSION["user_token"] = $this->GetToken();
    }

    public function GetShortID(): string
    {
        if (!array_key_exists("id", $this->data)) return "";
        return "u" . $this->data["id"];
    }

    public function GetJSONData(): array
    {
        if (!is_array($this->data)) return array();
        return array(
            "balance" => intval($this->GetBalance()),
            "id" => $this->GetID(),
            "name" => $this->GetName(),
            "platform" => $this->GetPlatformID(),
            "platform_name" => $this->GetPlatformName(),
            "country" => Country::GetCountry($this->GetCountryID())->GetJSONData()
        );
    }

    public function GetID() : int
    {
        if (!is_array($this->data) || !array_key_exists("id", $this->data)) return 0;
        return intval($this->data["id"]);
    }

    public function GetName() : string
    {
        if (!is_array($this->data) || !isset($this->data["profile_name"])) return "";
        return $this->data["profile_name"];
    }

    public function GetPlatformID() : string
    {
        if (!is_array($this->data) || !isset($this->data["platform"])) return 0;
        return $this->data["platform"];
    }

    public function GetPlatformName() : string
    {
        if (!is_array($this->data) || !isset($this->data["platform"])) return 0;

        $platform = Platforms::GetPlatformByID($this->data["platform"]);

        return isset($platform) ? $platform->GetName() : "";
    }

    public function GetCountryID() : int
    {
        if (!is_array($this->data) || !array_key_exists("country_id", $this->data)) return "";
        return intval($this->data["country_id"]);
    }

    public function GetBalance(): int
    {
        if (!is_array($this->data) || !isset($this->data["balance"])) return 0;
        return intval($this->data["balance"]);
    }

    public function GetLastActivityTime() : int
    {
        if (!is_array($this->data) || !isset($this->data["last_activity"])) return 0;
        return intval(strtotime($this->data["last_activity"]));
    }

    public function GetToken(): string
    {
        if (!is_array($this->data) || !array_key_exists("token", $this->data)) return "";
        return $this->data["token"];
    }

    public function RegisterActivityTime() : bool
    {
        if (!is_array($this->data)) return false;
        return Database::Query("UPDATE users SET last_activity = CURRENT_TIMESTAMP WHERE id = " . intval($this->data["id"]));
    }

    public function ChangeCountry(int $country_id) : bool
    {
        if (!is_array($this->data)) return false;
        return Database::Query("UPDATE users SET country_id = $country_id WHERE id = " . intval($this->data["id"]));
    }

    public function ChangeName(string $name) : bool
    {
        if (!is_array($this->data)) return false;
        return Database::Query("UPDATE users SET profile_name = '$name' WHERE id = " . intval($this->data["id"]));
    }

    public function SetAndSaveData(array $data) : bool
    {
        if(!isset($this->data) || !isset($this->data["id"])) return false;

        $fields = array();

        foreach ($data as $key => $value)
        {
            if(!array_key_exists($key, $this->data)) continue;

            if(is_string($value))
                array_push($fields, $key . "='". $value . "'");
            else if(is_numeric($value))
                array_push($fields, $key . "=". $value);
        }

        if(count($fields) <= 0) return false;

        $status = Database::Query("UPDATE users SET " . join(',', $fields) . " WHERE id = " . intval($this->data["id"]));

        if($status)
        {
            foreach ($data as $key => $value)
            {
                if(!array_key_exists($key, $this->data)) continue;
                $this->data[$key] = $value;
            }
        }

        return $status;
    }

    public function IncrementBalance(int $value) : bool
    {
        if(!isset($this->data)) return false;

        $value = abs($value);

        $status = Database::Query("UPDATE users SET balance = balance + $value WHERE id = " . $this->GetID());
        $this->RegisterActivityTime();

        if($status)
        {
            $statistics = $this->GetStatistics();
            $statistics->IncrementBalance($value);
            $this->data["balance"] = $this->data["balance"] + $value;
        }

        return $status;
    }

    public function GetData()
    {
        return $this->data;
    }

    public function GetStatistics() : UserStatistics
    {
        if(!isset($this->_user_statistics))
            $this->_user_statistics = new UserStatistics($this);

        return $this->_user_statistics;
    }

    private function ProcessUserDataValue(string $key, callable $handler)
    {
        if(!is_array($this->data)) return;
        if(!array_key_exists($key, $this->data)) return;
        $this->data[$key] = call_user_func($handler, $this->data[$key]);
    }
}