<?php namespace Entities;

use Game\BalanceNormalizer;
use Storage\Database;

class Country implements IEntity
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function GetID() : int
    {
        if(!is_array($this->data) || !isset($this->data["id"])) return -1;
        return intval($this->data["id"]);
    }

    public function GetShortID(): string
    {
        if(!is_array($this->data) || !array_key_exists("id", $this->user_data)) return "";
        return "u" . $this->data["id"];
    }

    public function GetJSONData() : array
    {
        if(!is_array($this->data)) return "";
        return array(
            "id" => $this->GetID(),
            "code" => $this->GetCode(),
            "balance" => $this->GetBalance(),
            "balance_level" => $this->GetBalanceLevel(),
            "players_count" => $this->GetPlayersCount()
        );
    }

    public function GetCode() : string
    {
        if(!is_array($this->data) || !isset($this->data["code"])) return "";
        return $this->data["code"];
    }

    public function GetBalance() : int
    {
        if (!is_array($this->data) || !isset($this->data["balance"])) return 0;
        return intval($this->data["balance"]);
    }

    public function GetBalanceLevel() : int
    {
        if (!is_array($this->data) || !isset($this->data["balance_level"])) return 0;
        return intval($this->data["balance_level"]);
    }

    public function GetPlayersCount() : int
    {
        if (!is_array($this->data) || !isset($this->data["players"])) return 0;
        return intval($this->data["players"]);
    }

    public function IncreaseBalance(int $value)
    {
        $value = abs($value);
        $balance = BalanceNormalizer::NormalizeIncrementBalance($this->GetBalance(), $this->GetBalanceLevel(), $value);

        $result = Database::Query("UPDATE countries SET balance = " . $balance->balance . ", balance_level = " . $balance->balanceLevel . " WHERE id = " . intval($this->data["id"]));

        if($result)
        {
            $this->data["balance"] = $balance->balance;
            $this->data["balance_level"] = $balance->balanceLevel;
        }

        return $result;
    }

    public static function GetCountry(int $id) : ?Country
    {
        $result = Database::Query("SELECT * FROM countries WHERE id = " . $id);

        if(empty($result) || is_null($result) || $result->num_rows == 0) return null;

        $country = new Country($result->fetch_assoc());
        return $country;
    }

    public static function GetCountries() : array
    {
        $countries = array();
        $result = Database::Query("SELECT * FROM countries");

        if(empty($result) || is_null($result) || $result->num_rows == 0) return $countries;

        while($country = $result->fetch_assoc())
        {
            array_push($countries, new Country($country));
        }

        return $countries;
    }
}