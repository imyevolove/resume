<?php
/**
 * Created by PhpStorm.
 * User: 1111111111
 * Date: 07.02.2017
 * Time: 22:52
 */

namespace Social;


class LeaderboardEntity implements ILeaderboardEntity
{
    private $rating_position;
    private $title;
    private $subtitle;
    private $score;
    private $icon;


    public function __construct($rating_position, $title, $subtitle, $score, $icon)
    {
        $this->rating_position = $rating_position;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->score = $score;
        $this->icon = $icon;
    }

    public function GetRatingPosition(): int
    {
        return $this->rating_position;
    }

    public function GetScore(): int
    {
        return $this->score;
    }

    public function GetTitle(): string
    {
        return $this->title;
    }

    public function GetSubTitle(): string
    {
        return $this->title;
    }

    public function GetIcon(): string
    {
        return $this->icon;
    }
}