<?php namespace Social;

use Entities\Country;
use Entities\User;
use Game\Statistics\Statistics;
use Storage\Database;

class Leaderboard
{
    public const LEADERBOARD_TYPES = [
        "country",
        "player"
    ];

    public static function NormalizeLeaderboardType(string $leaderboard_type) : string
    {
        return in_array($leaderboard_type, self::LEADERBOARD_TYPES) ? $leaderboard_type : self::LEADERBOARD_TYPES[0];
    }

    public static function GetLeaderboard(string $leaderboard_type, int $entities_count) : array
    {
        $leaderboard_type = self::NormalizeLeaderboardType($leaderboard_type);

        switch ($leaderboard_type)
        {
            case self::LEADERBOARD_TYPES[0]:
                return self::GetCountriesLeaderboard($entities_count);
            case self::LEADERBOARD_TYPES[1]:
                return self::GetPlayersLeaderboard($entities_count);
            default: return [];
        }
    }

    public static function GetCountriesLeaderboard(int $entities_count) : array
    {
        $entities = array();
        $result = Database::Query("SELECT * FROM countries ORDER BY balance_level DESC, balance DESC" . ($entities_count > 0 ? " LIMIT $entities_count" : ""));

        if(empty($result) || is_null($result) || $result->num_rows == 0) return $entities;

        while($entity = $result->fetch_assoc())
        {
            $en = new Country($entity);
            array_push($entities, $en->GetJSONData());
        }

        return $entities;
    }

    public static function GetPlayersLeaderboard(int $entities_count) : array
    {
        $entities = array();
        $result = Database::Query("SELECT * FROM users ORDER BY balance DESC" . ($entities_count > 0 ? " LIMIT $entities_count" : ""));

        if(empty($result) || is_null($result) || $result->num_rows == 0) return $entities;

        while($entity = $result->fetch_assoc())
        {
            $en = new User($entity);
            $data = $en->GetJSONData();
            $data["statistics"] = Statistics::GetUserStatistics($en)->GetJSONData();
            array_push($entities, $data);
        }

        return $entities;
    }
}