<?php
/**
 * Created by PhpStorm.
 * User: 1111111111
 * Date: 07.02.2017
 * Time: 22:50
 */

namespace Social;


interface ILeaderboardEntity
{
    public function GetRatingPosition() : int;
    public function GetScore() : int;
    public function GetTitle() : string;
    public function GetSubTitle() : string;
    public function GetIcon() : string;
}