<?php

class Renderer
{
    public static function RenderIncludeJSScript(string $filepath)
    {
        echo "<script type='text/javascript' src='$filepath' defer></script>";
    }

    public static function RenderIncludeCss(string $filepath)
    {
        echo "<link type='text/css' rel='stylesheet' href='$filepath' />";
    }

    public static function Render($file_path)
    {
        if(file_exists($file_path))
            require_once $file_path;
    }

    public static function RenderHTML($file_path)
    {
        if(file_exists($file_path))
            echo file_get_contents($file_path);
    }
}