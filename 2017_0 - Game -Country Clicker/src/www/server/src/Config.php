<?php

define("CONFIG_DIR", normalize_path(DIR_ROOT . "/cfg"));

class ConfigInfo
{
    public $parser;
    public $extension;
    public $file_name;
    public $file_path;

    public function __construct($file_name, $extension, $file_path, $parser)
    {
        $this->file_name = $file_name;
        $this->extension = $extension;
        $this->file_path = $file_path;
        $this->parser = $parser;
    }
}

class Config
{
    private static $loaded_configs = array();
    private static $parsers = array();

    public static function Initialize()
    {
        self::$parsers["ini"]  = function ($text) { return parse_ini_string($text); };
        self::$parsers["json"] = function ($text) { return json_decode($text); };
    }

    public static function Get($config_name)
    {
        try
        {
            $config_name = strtolower($config_name);

            /// Check loaded configs
            if(array_key_exists($config_name, self::$loaded_configs))
                return self::$loaded_configs[$config_name];

            /// Get config info
            $config_info = self::GetConfigInfo($config_name);

            /// Config not found
            if(is_null($config_info)) return null;

            $config_text = file_get_contents($config_info->file_path);
            if(empty($config_text) || is_null($config_text)) return null;

            try
            {
                /// Parse config text
                $config_data = call_user_func($config_info->parser, $config_text);

                /// Save config in cache
                self::$loaded_configs[$config_info->file_name] = $config_data;

                /// return result config
                return $config_data;
            }
            catch(Exception $exception)
            {
                return null;
            }
        }
        catch(Exception $exception)
        {
            return null;
        }

    }

    private static function GetConfigInfo($config_name)
    {
        foreach(self::$parsers as $extension => $parser)
        {
            $config_file_path = normalize_path(CONFIG_DIR . "/" . $config_name . "." . $extension);
            if(!file_exists($config_file_path)) continue;

            return new ConfigInfo(
                $config_name,
                $extension,
                $config_file_path,
                $parser
            );
        }
        return null;
    }
}
Config::Initialize();