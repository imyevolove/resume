-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Фев 09 2017 г., 22:39
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ccgdb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
  `id` int(10) NOT NULL,
  `code` varchar(2) NOT NULL,
  `players` bigint(255) NOT NULL DEFAULT '0',
  `balance` bigint(255) NOT NULL DEFAULT '0',
  `balance_level` bigint(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `countries`:
--

-- --------------------------------------------------------

--
-- Структура таблицы `statistics`
--

CREATE TABLE `statistics` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `balance` bigint(255) NOT NULL DEFAULT '0',
  `balance_level` bigint(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `statistics`:
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `user_id` varchar(50) DEFAULT '',
  `platform` varchar(50) NOT NULL DEFAULT '',
  `profile_name` varchar(255) NOT NULL DEFAULT '',
  `token` varchar(255) NOT NULL,
  `balance` bigint(255) NOT NULL DEFAULT '0',
  `country_id` int(50) NOT NULL DEFAULT '840',
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `users`:
--

--
-- Триггеры `users`
--
DELIMITER $$
CREATE TRIGGER `countries_count_trigger_update` AFTER UPDATE ON `users` FOR EACH ROW IF NEW.country_id <> OLD.country_id
    THEN  
        UPDATE countries 
        SET players = players - 1 
        WHERE id = OLD.country_id AND players > 0;
        
        UPDATE countries 
        SET players = players + 1 
        WHERE id = NEW.country_id;
    END IF
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `countries_counter_trigger_insert` BEFORE INSERT ON `users` FOR EACH ROW UPDATE countries 
    SET players = players + 1 
    WHERE id = NEW.country_id
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `prevent_wrong_country_id` BEFORE UPDATE ON `users` FOR EACH ROW IF NEW.country_id <> OLD.country_id AND NOT EXISTS (SELECT * FROM countries WHERE id = NEW.country_id) THEN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'country not found';
    END IF
$$
DELIMITER ;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `UNIQUE_CODE` (`code`);

--
-- Индексы таблицы `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uacc` (`user_id`,`platform`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
