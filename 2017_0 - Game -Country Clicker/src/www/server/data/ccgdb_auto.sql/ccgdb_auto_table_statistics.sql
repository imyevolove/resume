
-- --------------------------------------------------------

--
-- Структура таблицы `statistics`
--

CREATE TABLE `statistics` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `balance` bigint(255) NOT NULL DEFAULT '0',
  `balance_level` bigint(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `statistics`:
--
