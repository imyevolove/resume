
-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `user_id` varchar(50) DEFAULT '',
  `platform` varchar(50) NOT NULL DEFAULT '',
  `profile_name` varchar(255) NOT NULL DEFAULT '',
  `token` varchar(255) NOT NULL,
  `balance` bigint(255) NOT NULL DEFAULT '0',
  `country_id` int(50) NOT NULL DEFAULT '840',
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `users`:
--
