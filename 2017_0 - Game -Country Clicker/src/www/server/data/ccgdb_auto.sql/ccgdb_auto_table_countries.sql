
-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
  `id` int(10) NOT NULL,
  `code` varchar(2) NOT NULL,
  `players` bigint(255) NOT NULL DEFAULT '0',
  `balance` bigint(255) NOT NULL DEFAULT '0',
  `balance_level` bigint(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- СВЯЗИ ТАБЛИЦЫ `countries`:
--
