<?php

use Storage\Database;

$file = normalize_path(DIR_DATA . "/db.sql");
$config = Config::Get("db");

$dsn = "mysql:host=" . $config["host"] . ";dbname=" . $config["db_name"] . ";charset=UTF8";
$opt = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);
$pdo = new PDO($dsn, $config["user"], $config["password"], $opt);
$qr = $pdo->exec(file_get_contents($file));