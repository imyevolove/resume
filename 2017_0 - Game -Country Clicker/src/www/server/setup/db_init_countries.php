<?php

use Storage\Database;

$countries_data_text = file_get_contents(normalize_path(DIR_ROOT . "/data/countries.json"));
$countries_data = json_decode($countries_data_text);

Database::ClearMultiQueryList();

// Insert countries data into table
foreach ($countries_data as $country)
    Database::PushMultiQuery("INSERT IGNORE INTO countries (id, code) values (" . $country->id . ", '" . $country->code . "')");

$result = Database::ExecuteMultiQuery();

var_dump(Database::GetError());