(function(Page) {

    Page.__defineGetter__("element", function() { return document.getElementById("page-container"); });
    Page.__defineGetter__("content", function() { return Page.element.innerHTML; });
    Page.__defineSetter__("content", function(value) { Page.element.innerHTML = value; });

    Page.Open = function (viewMethod, historyPath, callback, params)
    {
        var wait = new Wait(document.getElementById("page-container"));

        if(!Object.prototype.isPrototypeOf(params)) params = null;

        Location.SetHistoryPathname(historyPath);
        Request(viewMethod, GET, params || {format: "html"}, function(content) {
            wait.Exit();
            Page.content = content;
            if(Function.prototype.isPrototypeOf(callback)) callback();
        });
    };

    Page.OpenIndexPage = function ()
    {
        Page.Open("/view/index", "/", function() {
            Menu.Update();
        });
    };

    Page.OpenLegalPage = function()
    {
        Page.Open("/view/legal", "/legal", function() {
            Document.InitScrollBar();
        });
    };

    Page.OpenEconomyPage = function()
    {
        Page.Open("/view/economy", "/economy", function() {
            Document.InitScrollBar();
        });
    };
})(window["Page"] || (window["Page"] = {}));