(function(Contacts) {
    var wait;

    Contacts.ShowInViewer = function()
    {
        wait = new Wait(document.querySelector(".page_separate"));

        Location.SetHistoryPathname("/contacts");
        Menu.SelectButton("contacts");

        Request("/view/contacts", GET, {format: "html"}, function(content) {
            wait.Exit();
            Viewer.content = content;
            SimpleScrollbar.initEl(document.querySelector(".contacts_container_wrap"));
        });
    };

})(window["Contacts"] || (window["Contacts"] = {}));