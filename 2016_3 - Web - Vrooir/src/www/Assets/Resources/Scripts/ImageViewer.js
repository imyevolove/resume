(function(ImageViewer) {
    var c_ViewerInfo = null;

    ImageViewer.View = function(url)
    {
        P_ViewIn(Page.element, url);

        var fullScreenButton = document.createElement("div");
        fullScreenButton.className = "image-viewer_full-screen-button";
        fullScreenButton.innerHTML = 'Открыть во весь экран';
        fullScreenButton.addEventListener("click", function() {
            ImageViewer.ViewFullScreen(c_ViewerInfo.image);
        });
        c_ViewerInfo.viewer.appendChild(fullScreenButton);
    };

    ImageViewer.ViewFullScreen = function(url)
    {
        P_ViewIn(document.body, url);
    };

    function P_RemoveCurrentViewer()
    {
        if(!!c_ViewerInfo) c_ViewerInfo.viewer.RemoveViewer();
    }

    function P_ViewIn(parent, url)
    {
        P_RemoveCurrentViewer();

        var viewerElement = document.createElement("div");
        viewerElement.className = "image-viewer_wrap page-viewer-entry-animation-play2-fast";

        var closeButton = document.createElement("div");
        closeButton.className = "image-viewer_close-button";
        closeButton.innerHTML = '<i class="fa fa-times" aria-hidden="true"></i>';
        closeButton.addEventListener("click", function() {
            var parent = viewerElement.parentElement;
            if(!!parent)
            {
                parent.removeChild(viewerElement);
                c_ViewerInfo = null;
            }
        });

        var imageBox = document.createElement("div");
        imageBox.className = "image-viewer_image-box";
        imageBox.style.backgroundImage = "url(" + url + ")";

        viewerElement.appendChild(imageBox);
        viewerElement.appendChild(closeButton);
        parent.appendChild(viewerElement);

        viewerElement.RemoveViewer = function ()
        {
            var parent = viewerElement.parentElement;
            if(!!parent)
            {
                parent.removeChild(viewerElement);
                c_ViewerInfo = null;
            }
        };

        c_ViewerInfo = new ViewerInfo(viewerElement, url);
    }

    function ViewerInfo(viewerElement, image)
    {
        this.__defineGetter__("image", function() { return image; });
        this.__defineGetter__("viewer", function() { return viewerElement; });
    }
})(window["ImageViewer"] || (window["ImageViewer"] = {}));