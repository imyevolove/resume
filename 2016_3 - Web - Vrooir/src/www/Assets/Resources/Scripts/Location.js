(function(Location) {

    Location.__defineGetter__("pathname", function() { return location.pathname.toLowerCase(); });
    Location.SetHistoryPathname = function(pathname) { window.history.pushState("", "", pathname); };

    P_Constructor();

    //
    function P_Constructor()
    {
        P_InitOnloadViewerEvents();
    }

    //
    function P_InitOnloadViewerEvents()
    {
        switch (Location.pathname)
        {
            case "/news":
                News.ShowInViewer();
                break;
            case "/contacts":
                Contacts.ShowInViewer();
                break;
            case "/pricelist":
                PriceList.ShowInViewer();
                break;
            case "/document":
            case "/legal":
            case "/economy":
                Document.InitScrollBar();
                break;
            case "/gallery/nature":
            case "/gallery/hunting":
            case "/gallery/fishing":
                Gallery.InitExplorerScrollBar();
                break;
        }
    }

})(window["Location"] || (window["Location"] = {}));