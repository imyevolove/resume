(function(TooltipManager) {
    TooltipManager.AttachTo = function(element, message)
    {
        var tooltip = new Tooltip(message);
        tooltip.attach(element);
        tooltip.type("light").effect("fade");

        tooltip.show();
        element.addEventListener("mouseover", function() {
            tooltip.show();
        }, false);

        element.addEventListener("mouseout", function() {
            tooltip.hide();
        }, false);
    };
})(window["TooltipManager"] || (window["TooltipManager"] = {}));