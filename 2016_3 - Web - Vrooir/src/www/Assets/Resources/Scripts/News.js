(function(News) {
    var NEWS_ALL_LOADED = 0x1,
        NEWS_LOADING = 0x2,
        NEWS_LOADED = 0x3,
        NEWS_EMPTY = 0x4;

    var shift = 0;
    var status = NEWS_EMPTY;

    var newsContainer = null;
    var wait = null;
    var waitSpwanTime = 0;
    var minWaitTime = 1;

    News.ShowInViewer = function()
    {
        status = NEWS_EMPTY;
        Viewer.content = "";
        shift = 0;

        wait = new Wait(document.querySelector(".page_separate"));
        waitSpwanTime = Date.now();

        RequestMoreNews();

        Menu.SelectButton("news");
        Location.SetHistoryPathname("/news");
    };

    News.HideInViewer = function()
    {
        Viewer.content = "";
        status = NEWS_EMPTY;
        Menu.DisableButtonSelection("news");
    };

    function RequestMoreNews()
    {
        status = NEWS_LOADING;
        Request("/view/news.list", GET, {page: shift, mode: "mini", format: "html", wrap: (!shift ? 1 : 0)}, function(content) {
            status = NEWS_LOADED;

            if(!!wait)
            {
                var deltaTime = Date.now() - waitSpwanTime;
                deltaTime = minWaitTime * 1000 - deltaTime;

                if(deltaTime > 0)
                {
                    setTimeout(function() {
                        Next();
                    }, deltaTime);
                }
                else
                {
                    Next();
                }
            }

            if(!content.length)
            {
                status = NEWS_ALL_LOADED;
                return;
            }

            if(!shift)
            {
                Viewer.content += content;

                var element = Viewer.viewerElement.firstChild;
                SimpleScrollbar.initEl(element);
                newsContainer = element.querySelector(".ss-content");

                element.classList.add("page-viewer-entry-animation-play");

                newsContainer.onscroll = function() {
                    if(!this.parentElement)
                    {
                        this.onscroll = null;
                        return;
                    }

                    var maxScroll = this.scrollHeight - this.offsetHeight;
                    if(this.scrollTop > (maxScroll - 100))
                    {
                        RequestMoreNews();
                    }
                };
            }
            else
            {
                newsContainer.innerHTML += content;
            }

            shift++;

            function Next()
            {
                wait.Exit();
                wait = null;
            }
        });
    }

})(window["News"] || (window["News"] = {}));