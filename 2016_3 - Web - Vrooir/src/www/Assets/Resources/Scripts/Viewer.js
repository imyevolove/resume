(function(Viewer) {

    Viewer.__defineGetter__("viewerElement", function() { return document.getElementById("fast-page-viewer"); });

    // ������ � ���������
    Viewer.__defineGetter__("content", function() {
        if(!Viewer.viewerElement) throw new Error("Can't set content; Viewer not found");
        return Viewer.viewerElement.innerHTML;
    });
    Viewer.__defineSetter__("content", function(value) {
        if(!Viewer.viewerElement) throw new Error("Can't set content; Viewer not found");
        Viewer.viewerElement.innerHTML = value;
    });

    Viewer.Open = function(url, callback)
    {
        if(!Viewer.viewerElement) throw new Error("Can't open page in viewer; Viewer not found");
        Request(url, POST, null, function(content) {
            console.log(content);
        });
    };
})(window["Viewer"] || (window["Viewer"] = {}));