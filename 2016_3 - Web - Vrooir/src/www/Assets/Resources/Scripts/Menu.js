(function(Menu) {
    var buttons = {};
    P_SearchAllButtons();

    Menu.Update = function()
    {
        P_SearchAllButtons();
    };

    Menu.GetButton = function(buttonID)
    {
        buttonID = buttonID.toLowerCase();
        return buttons[buttonID] || (buttons[buttonID] = P_SearchButton(buttonID));
    };

    Menu.SelectButton = function(buttonID)
    {
        console.log(buttonID);
        buttonID = buttonID.toLowerCase();

        for(var id in buttons)
        {
            if(id == buttonID) buttons[id].setAttribute("pointer", "selected");
            else buttons[id].removeAttribute("pointer");
        }
    };

    Menu.DisableButtonSelection = function (buttonID)
    {
        buttonID = buttonID.toLowerCase();

        if(!!buttons[buttonID])
            buttons[buttonID].removeAttribute("pointer", "selected");
    };

    function P_SearchButton(buttonID)
    {
        var elements = [].slice.call(document.getElementsByTagName("*"));
        var filtered = elements.filter(function(tag) { return (tag.getAttribute("button_id") == buttonID); });
        return filtered[0];
    }

    function P_SearchAllButtons()
    {
        var elements = [].slice.call(document.getElementsByTagName("*"));
        elements.filter(function(tag) {
            var attrValue = tag.getAttribute("button_id");
            buttons[attrValue] = tag;
        });
    }
})(window["Menu"] || (window["Menu"] = {}));