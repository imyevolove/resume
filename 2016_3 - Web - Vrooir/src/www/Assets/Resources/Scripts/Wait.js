﻿function Wait(parent)
{
    var element;
    CreateDOM();

    this.Exit = function ()
    {
        element.className = "wait-local-screen page-wait-exit-animation-play";
        setTimeout(function() {
            if(element.parentElement)
                element.parentElement.removeChild(element);
        }, 1001);
    };

    function CreateDOM()
    {
        element = document.createElement("div");
        element.className = "wait-local-screen page-wait-entry-animation-play";

        var table = document.createElement("div");
        table.className = "wait-local-screen_table";

        var cell = document.createElement("div");
        cell.className = "wait-local-screen_cell";

        cell.innerHTML = "Загрузка";

        element.appendChild(table);
        table.appendChild(cell);
        parent.appendChild(element);
    }
}