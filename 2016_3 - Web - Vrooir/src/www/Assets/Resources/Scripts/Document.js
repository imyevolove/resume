(function(Document) {

    Document.__defineGetter__("documentBodyElement", function() { return document.getElementById("document-body"); });
    Document.__defineGetter__("documentContentElement", function() { return document.getElementById("document-content"); });

    Document.InitScrollBar = function ()
    {
        SimpleScrollbar.initEl(Document.documentContentElement);
    };

    Document.Open = function(documentID)
    {
        Page.Open("/view/document", "/document?doc=" + documentID, function() {
            Document.InitScrollBar();
        }, {doc: documentID});
    }
})(window["Document"] || (window["Document"] = {}));