var POST = "POST", GET = "GET";

function Request(url, method, parameters, callback)
{
    // ������������ ������ �������
    method = method.toUpperCase();
    method = method.match(/^GET|POST$/) || "GET";

    // ������ ��� �������� ������������ ������
    var s_Parameters = "";

    if(Object.prototype.isPrototypeOf(parameters))
    {
        for(var key in parameters)
        {
            s_Parameters += key + "=" + encodeURIComponent(parameters[key]) + "&";
        }
        s_Parameters = s_Parameters.replace(/&\s*$/, "");
    }

    // ���� ����� ������� GET
    // �������� ��������� ������� � URL
    if(method == "GET") url += "?" + s_Parameters;

    // ��� ������ ������� � ��������
    // ����� � ���� ���? ��� �� ��������� ����������� �������� -_-
    var xhr = new XMLHttpRequest();

    // �������� �� ������� �������
    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState != 4) return;
        callback(xhr.responseText, xhr.status);
    };

    // ��������� ����������
    xhr.open(method, url, true);

    if(method == "POST")
    {
        //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        s_Parameters = new FormData();
        Object.keys(parameters).forEach(function(key) {
            switch (typeof parameters[key])
            {
                case "string":
                case "number":
                    s_Parameters.append(key, parameters[key]);
            }
        });
    }

    // ���������� ������
    xhr.send(method == "POST" ? s_Parameters : null);
}