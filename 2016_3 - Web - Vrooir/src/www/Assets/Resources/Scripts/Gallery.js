(function(Gallery) {
    Gallery.InitExplorerScrollBar = function()
    {
        var element = document.getElementById("gallery-explorer");
        if(!element) return;

        SimpleScrollbar.initEl(element);
    };

    Gallery.OpenGalleryCategories = function ()
    {
        Page.Open("/view/gallery.categories", "/gallery");
    };

    Gallery.OpenGalleryNature = function ()
    {
        Page.Open("/view/gallery.nature", "/gallery/nature", function() {
            Gallery.InitExplorerScrollBar();
        });
    };

    Gallery.OpenGalleryHunting = function ()
    {
        Page.Open("/view/gallery.hunting", "/gallery/hunting", function() {
            Gallery.InitExplorerScrollBar();
        });
    };

    Gallery.OpenGalleryFishing = function ()
    {
        Page.Open("/view/gallery.fishing", "/gallery/fishing", function() {
            Gallery.InitExplorerScrollBar();
        });
    };
})(window["Gallery"] || (window["Gallery"] = {}));