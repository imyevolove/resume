(function(PriceList) {
    var wait;

    PriceList.ShowInViewer = function()
    {
        wait = new Wait(document.querySelector(".page_separate"));

        Location.SetHistoryPathname("/pricelist");
        Menu.SelectButton("price_list");

        Request("/view/price.list", GET, {format: "html"}, function(content) {
            wait.Exit();
            Viewer.content = content;

            var element = Viewer.viewerElement.querySelector(".pricelist_list_container");
            SimpleScrollbar.initEl(element);
        });
    };

})(window["PriceList"] || (window["PriceList"] = {}));