(function(Dashboard) {
    Dashboard.News = {};
    Dashboard.News.Remove = function(newsID)
    {
        if(confirm("Вы уверены, что хотите удалить статью?"))
        {
            Request("/dashboard/controls/news.remove", POST, {news_id: newsID}, function(result) {
                try
                {
                    console.log(result);
                    result = JSON.parse(result);
                    if(!result.status)
                    {
                        var el = document.getElementById("news-i" + newsID);
                        el.parentElement.removeChild(el);
                    }
                    else
                    {
                        alert("Ошибка удаления записи");
                    }
                }
                catch(err)
                {
                    alert("Ошибка");
                }
            });
        }
    };
    Dashboard.News.Edit = function(newsID)
    {
        location = "/dashboard/news/editor?nid=" + newsID;
    };
})(window["Dashboard"] || (window["Dashboard"] = {}));