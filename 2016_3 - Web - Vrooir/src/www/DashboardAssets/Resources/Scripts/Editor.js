var ACTION_SAVE = 0xA;
var ACTION_CREATE = 0xB;

(function(Editor) {
    P_InitExitEvent();
    P_InitImageDragAndDropEvents();

    Editor.data = {
        image: ""
    };
    Editor.data.__defineGetter__("title", function() {
        var el = document.getElementById("ai-title");
        return !!el ? el.value : "";
    });
    Editor.data.__defineGetter__("text", function() {
        var el = document.getElementById("ai-text");
        return !!el ? el.value : "";
    });

    Editor.LoadDataFromSessionStorage = function ()
    {
        console.log("Restore data from session storage");
        P_RestoreEditorData();
    };

    Editor.SetData = function(title, text, imageURL)
    {
        console.log("Set Data");
        document.getElementById("ai-title").value = title;
        document.getElementById("ai-text").value  = text.replace(/<br\s*\/?>/mg,"\n");
        //Editor.data.image = imageURL;

        var wait = new Wait(document.body);

        var xhr = new XMLHttpRequest();
        xhr.open('GET', imageURL, true);
        xhr.responseType = 'blob';

        xhr.onload = function(e)
        {
            if (this.status == 200)
            {
                var blob = new Blob([this.response], {type: 'image/png'});
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onload = function()
                {
                    Editor.data.image = reader.result;
                    P_SaveToSessionStorage();
                    sessionStorage.setItem("E_ArticleImage", Editor.data.image);
                    P_RestoreEditorData();
                    wait.Exit();
                };
            }
        };

        xhr.send();
    };

    Editor.SetButtonAction = function (action)
    {
        var el = document.getElementById("article_form");
        switch (action)
        {
            case ACTION_SAVE:
                el.action = "/dashboard/news/save";
                break;
            case ACTION_CREATE:
                el.action = "/dashboard/news/create";
                break;
        }
    };

    function P_SaveToSessionStorage()
    {
        sessionStorage.setItem("E_ArticleTitle", Editor.data.title);
        sessionStorage.setItem("E_ArticleText",  Editor.data.text);
        if(!sessionStorage.getItem("E_ArticleImage")) sessionStorage.setItem("E_ArticleImage", Editor.data.image);
    }

    function P_RestoreEditorData()
    {
        document.getElementById("ai-title").value = sessionStorage.getItem("E_ArticleTitle");
        document.getElementById("ai-text").value = sessionStorage.getItem("E_ArticleText");
        document.getElementById("ai-image").style.backgroundImage = "url(" + sessionStorage.getItem('E_ArticleImage') + ")";
        document.getElementById("ai-image-hidden").value = sessionStorage.getItem("E_ArticleImage");
    }

    function P_InitImageDragAndDropEvents()
    {
        var e_Target = document.getElementById("ai-image");
        if(!e_Target) return;

        e_Target.addEventListener("dragover", function(e) {
            e.preventDefault();
            console.log("drag over");
        });

        e_Target.addEventListener("dragleave", function() {
            console.log("drag leave");
        });

        e_Target.addEventListener("drop", P_OnDropEvent);
        e_Target.addEventListener("dragdrop", P_OnDropEvent);

        function P_OnDropEvent(e)
        {
            e.preventDefault();
            var file = e.dataTransfer.files[0];
            var wait = new Wait(document.body);

            switch (file.type)
            {
                case "image/png":
                case "image/jpg":
                case "image/jpeg":
                    var fr = new FileReader();
                    fr.readAsDataURL(file);
                    fr.onloadend = function()
                    {
                        Editor.data.image = fr.result;
                        sessionStorage.setItem("E_ArticleImage", fr.result);
                        document.getElementById("ai-image-hidden").value = fr.result;

                        //document.getElementById("ai-image-hidden").files[0] = fr.result;
                        e_Target.setAttribute("state", "loaded");
                        e_Target.style.backgroundImage = "url(" + fr.result + ")";
                        wait.Exit();
                    };
                    break;
                default : wait.Exit(); alert("Только для изображений");
            }
        }
    }

    function P_InitExitEvent()
    {
        window.onbeforeunload = function() {
            P_SaveToSessionStorage();
            return "are you sure to exit?";
        };
    }


})(window["Editor"] || (window["Editor"] = {}));
