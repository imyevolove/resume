var TAB_BUTTON_NEWS         = "news",
    TAB_BUTTON_CONTACTS     = "contacts",
    TAB_BUTTON_PRICELIST    = "pricelist",
    TAB_BUTTON_PHOTOS       = "photos",
    TAB_BUTTON_ECONOMY      = "economy",
    TAB_BUTTON_LEGAL        = "legal";

(function(Dashboard) {
    var se = document.getElementById("document-container");
    if(!!se) SimpleScrollbar.initEl(se);

    Dashboard.Menu = {};
    Dashboard.Menu.buttons = {};
    Dashboard.Menu.SetActiveTab = function(tabButtonID)
    {
        for(var key in Dashboard.Menu.buttons)
        {
            if(key == tabButtonID)
            {
                Dashboard.Menu.buttons[key].setAttribute("pointer", "selected");
            }
            else
            {
                Dashboard.Menu.buttons[key].removeAttribute("pointer");
            }
        }
    };
    Dashboard.Menu.ReBindButtons = function()
    {
        var elements = document.getElementsByTagName("*");
        var tab = "";

        for(var el in elements)
        {
            if(!elements[el].getAttribute) continue;
            tab = elements[el].getAttribute("tab-name");
            if(!!tab)
            {
                Dashboard.Menu.buttons[tab] = elements[el];
            }
        }
    };
    Dashboard.Menu.SearchButton = function(id)
    {
        var elements = document.getElementsByTagName("*");
        var tab = "";

        for(var el in elements)
        {
            tab = elements[el].getAttribute("tab-name");
            if(!!tab && tab == id)
            {
                Dashboard.Menu.buttons[tab] = elements[el];
                return elements[el];
            }
        }
        return null;
    };

    Dashboard.Contacts = {};
    Dashboard.Contacts.Remove = function(id)
    {
        if(confirm("Ву уверены, что хотите удалить контакт?"))
        {
            Request("/dashboard/controls/contacts.remove", POST, {contact_id: id}, function(response) {
                try
                {
                    response = JSON.parse(response);
                    if(!!response.error)
                    {
                        alert(response.error.message);
                    }
                    else
                    {
                        var el = document.getElementById("contact-i" + id);
                        if (el) el.parentElement.removeChild(el);
                    }
                }
                catch(e)
                {
                    alert("Error");
                }
            });
        }
    };
    Dashboard.Contacts.Create = function(header, people, phone, email)
    {
        Request("/dashboard/controls/contacts.create", POST, {header: header, people: people, phone: phone, email: email}, function(response) {
            try
            {
                response = JSON.parse(response);
                if(!!response.error)
                {
                    alert(response.error.message);
                }
                else
                {
                    location = location.href;
                }
            }
            catch(e)
            {
                alert("Error");
            }
        });
    };

    Dashboard.PriceList = {};
    Dashboard.PriceList.Register = function(header, price)
    {
        Request("/dashboard/controls/price.list.add", POST, {header: header, price: price}, function(response) {
            try
            {
                response = JSON.parse(response);
                if(!!response.error)
                {
                    alert(response.error.message);
                }
                else
                {
                    location = location.href;
                }
            }
            catch(e)
            {
                alert("Error");
            }
        });
    };
    Dashboard.PriceList.Remove = function(id)
    {
        if(confirm("Ву уверены, что хотите удалить элемент?"))
        {
            Request("/dashboard/controls/price.list.remove", POST, {id: id}, function(response) {
                try
                {
                    response = JSON.parse(response);
                    if(!!response.error)
                    {
                        alert(response.error.message);
                    }
                    else
                    {
                        var el = document.getElementById("price-i" + id);
                        if (el) el.parentElement.removeChild(el);
                    }
                }
                catch(e)
                {
                    alert("Error");
                }
            });
        }
    };

    Dashboard.Photos = {};
    Dashboard.Photos.Delete = function (id)
    {
        if(confirm("Ву уверены, что хотите удалить Фотографию?"))
        {
            Request("/dashboard/controls/photos.remove", POST, {id: id}, function(response) {
                try
                {
                    response = JSON.parse(response);
                    if(!!response.error)
                    {
                        alert(response.error.message);
                    }
                    else
                    {
                        var el = document.getElementById("photo-i" + id);
                        if (el) el.parentElement.removeChild(el);
                    }
                }
                catch(e)
                {
                    alert("Error");
                }
            });
        }
    };

    Dashboard.Menu.ReBindButtons();

})(window["Dashboard"] || (window["Dashboard"] = {}));