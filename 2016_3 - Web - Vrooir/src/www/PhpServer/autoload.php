<?php

class Autoload
{
    public static $Directory = "";

    public static function __Autoload($class_name)
    {

        $path = join(DIRECTORY_SEPARATOR, array(self::$Directory, $class_name . '.php'));
        $path = str_replace("\\", "/", $path);
        $path = preg_replace('#/+#', '/', $path);

        if(file_exists($path))
        {
            require_once $path;
        }
        else
        {
            throw new Exception("Unable to load:" . $path);
        }
    }
}

class AutoloadException extends Exception{}

spl_autoload_register("Autoload::__Autoload");