<?php

$p_Page = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
$p_Main = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "GalleryPage.html"));

$p_Final = Renderer::Format($p_Page, array("{PAGE_CONTAINER}"), $p_Main);

Renderer::Draw($p_Final);