<?php

$docID = hexdec($_GET["doc"]);
if(is_null($docID) || empty($docID) || !is_integer($docID))
{
    Render404Page();
}

$document = DB::Request("SELECT * FROM news WHERE id = $docID", FETCH_ASSOC);

if(empty($document) || is_null($document))
{
    Render404Page();
}

$p_Page = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
$p_Document = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "DocumentPage.html"));

$p_Document  = Renderer::Format(
    $p_Document,
    array(
        "{DOCUMENT_IMAGE}",
        "{DOCUMENT_HEADER}",
        "{DOCUMENT_TEXT}"
    ),
    array(
        "/upload/news/" . $document['image'],
        $document['header'],
        nl2br(Renderer::ParseTextURLs($document['document']))
    )
);
$p_Final = Renderer::Format($p_Page, array("{PAGE_CONTAINER}"), $p_Document);

Renderer::Draw($p_Final);

function Render404Page()
{
    Renderer::Draw(File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Error", "404.html")));
}