<?php

$limit = 10000000;
$shift = (int)$_GET["page"] * $limit;

$q_Selection = ($_GET["mode"] == "mini" ? "id, header, image" : "*");

$data = DB::Request("SELECT $q_Selection FROM news ORDER BY id DESC LIMIT $shift, $limit", FETCH_ARRAY);

foreach ($data as $item)
{
    $item->image = "/upload/news/" . $item->image;
}

switch(strtolower($_GET["format"]))
{
    case "html":
        $p_NewsItem = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "NewsMiniItem.html"));
        $p_NewsContainer = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "NewsMiniContainer.html"));
        $content = "";

        foreach($data as $item)
        {
            $content .= Renderer::Format(
                $p_NewsItem,
                array(
                    "{NEWS_IMAGE}",
                    "{NEWS_HEADER}",
                    "{NEWS_READ_ID}"
                ),
                array(
                    $item->image,
                    $item->header,
                    dechex((int)$item->id)
                )
            );
        }

        if(!((int)$_GET["wrap"]))
        {
            echo $content;
        }
        else
        {
            echo Renderer::Format($p_NewsContainer, array("{NEWS_CONTAINER}"), array($content));

        }
        break;
    case "json":
    default:
        echo json_encode($data);
        break;
}