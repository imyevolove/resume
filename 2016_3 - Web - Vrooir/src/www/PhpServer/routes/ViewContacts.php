<?php

$data = DB::Request("SELECT * FROM contacts", FETCH_ARRAY);

switch(strtolower($_GET["format"]))
{
    case "html":
        $p_ContactsItem = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "ContactsItem.html"));
        $p_ContactsContainer = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "ContactsContainer.html"));
        $content = "";

        foreach($data as $item)
        {
            $content .= Renderer::Format(
                $p_ContactsItem, array("{HEADER}", "{PEOPLE}", "{PHONE}", "{EMAIL}"),
                array(
                    $item->header,
                    $item->people,
                    (empty($item->phone) ? "" : '<i class="fa fa-phone" aria-hidden="true"></i> ' . $item->phone),
                    (empty($item->email) ? "" : '<i class="fa fa-envelope-o" aria-hidden="true"></i> ' . $item->email)
                )
            );
        }

        echo Renderer::Format($p_ContactsContainer, array("{CONTACTS_CONTAINER}"), array($content));
        break;
    case "json":
    default:
        echo json_encode($data);
        break;
}