<?php

$data = DB::Request("SELECT * FROM price_list ORDER BY id DESC", FETCH_ARRAY);

switch(strtolower($_GET["format"]))
{
    case "html":
        $p_ContactsItem = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "PriceListItem.html"));
        $p_ContactsContainer = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "PriceListContainer.html"));
        $content = "";

        foreach($data as $item)
        {
            $content .= Renderer::Format(
                $p_ContactsItem, array("{HEADER}", "{PRICE}"),
                array($item->header, $item->price)
            );
        }

        echo Renderer::Format($p_ContactsContainer, array("{PRICE_LIST_CONTAINER}"), array($content));
        break;
    case "json":
    default:
        echo json_encode($data);
        break;
}