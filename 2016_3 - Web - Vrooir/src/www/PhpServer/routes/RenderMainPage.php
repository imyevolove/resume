<?php

$p_Page = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
$p_Main = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "MainPage.html"));

$p_Main  = Renderer::Format($p_Main, array("{COPYRIGHT_YEAR}"), date("Y"));
$p_Final = Renderer::Format($p_Page, array("{PAGE_CONTAINER}"), $p_Main);

Renderer::Draw($p_Final);