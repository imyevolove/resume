<?php
define("SERVER_ROOT", __DIR__);

require_once("autoload.php");
Autoload::$Directory = __DIR__ . "/lib/";


use \File;
use \Path;
use \Renderer;
use \Router;
use \DB;
use \Dashboard;

function DrawMainPage()
{
    RequireRouteFunction("RenderMainPage");
}

function DrawGalleryCategoriesPage()
{
    RequireRouteFunction("RenderGalleryPage");
}

function DrawDocumentPage()
{
    RequireRouteFunction("RenderDocumentPage");
}

function DrawLegalInformationPage()
{
    $d_Legal = Renderer::ParseTextURLs(Dashboard\View\Legal::GetDocumentText());

    $p_Legal = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "StaticTemplates", "Legal.html"));
    $p_Legal = Renderer::Format($p_Legal, "{DOCUMENT}", nl2br($d_Legal));

    $p_Page  = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
    $p_Page  = Renderer::Format($p_Page, "{PAGE_CONTAINER}", $p_Legal);
    Renderer::Draw($p_Page);
}

function DrawEconomyPage()
{
    $d_Eco = Renderer::ParseTextURLs(Dashboard\View\Economy::GetDocumentText());

    $p_Eco = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "StaticTemplates", "Economy.html"));
    $p_Eco = Renderer::Format($p_Eco, "{DOCUMENT}", nl2br($d_Eco));

    $p_Page  = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
    $p_Page  = Renderer::Format($p_Page, "{PAGE_CONTAINER}", $p_Eco);
    Renderer::Draw($p_Page);
}

function DrawGalleryCategoriesExploreNaturePage()
{
    DrawGalleryCategoriesExplorePage("nature");
}

function DrawGalleryCategoriesExploreHuntingPage()
{
    DrawGalleryCategoriesExplorePage("hunting");
}

function DrawGalleryCategoriesExploreFishingPage()
{
    DrawGalleryCategoriesExplorePage("fishing");
}

function GetFalleryCategoryExplorerContent($category)
{
    $data = DB::Request("SELECT image FROM photo_gallery WHERE category='$category'", FETCH_ARRAY);
    $content = "";

    $p_Page  = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
    $p_Main  = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "GalleryExplorerPage.html"));
    $p_Photo = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "GalleryPhotoItem.html"));

    foreach($data as $photo)
    {
        $content .= Renderer::Format(
            $p_Photo,
            array(
                "{PHOTO_IMAGE_FULL}",
                "{PHOTO_IMAGE_THUMB}"
            ),
            array(
                "/upload/gallery/" . $photo->image,
                "/upload/gallery/" . Dashboard\View\Photos::GetThumbnail($photo->image)
            )
        );
    }

    $p_Main = Renderer::Format($p_Main, array("{PHOTOS}"), $content);

    return $p_Main;
}

function DrawGalleryCategoriesExplorePage($category)
{
    $p_Page  = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Page.html"));
    $p_Final = Renderer::Format($p_Page, array("{PAGE_CONTAINER}"), GetFalleryCategoryExplorerContent($category));

    Renderer::Draw($p_Final);
}

function DrawViewNewsList()
{
    RequireRouteFunction("ViewNewsList");
}

function DrawViewContacts()
{
    RequireRouteFunction("ViewContacts");
}

function DrawViewPriceList()
{
    RequireRouteFunction("ViewPriceList");
}

function DrawViewDocument()
{
    RequireRouteFunction("ViewDocument");
}

// ����������� ������� ������ 404
Router::Register404Error(function() {
    $path = Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Error", "404.html");
    $content = File::Load($path);
    Renderer::Draw($content);
});

// ������� ��������
Router::Register("/", DrawMainPage);
Router::Register("/news", DrawMainPage);
Router::Register("/document", DrawDocumentPage);
Router::Register("/price", DrawMainPage);
Router::Register("/legal", DrawLegalInformationPage);
Router::Register("/economy", DrawEconomyPage);
Router::Register("/contacts", DrawMainPage);
Router::Register("/pricelist", DrawMainPage);
Router::Register("/gallery", DrawGalleryCategoriesPage);
Router::Register("/gallery/nature", DrawGalleryCategoriesExploreNaturePage);
Router::Register("/gallery/hunting", DrawGalleryCategoriesExploreHuntingPage);
Router::Register("/gallery/fishing", DrawGalleryCategoriesExploreFishingPage);

Router::Register("view/news.list", DrawViewNewsList);
Router::Register("view/contacts", DrawViewContacts);
Router::Register("view/price.list", DrawViewPriceList);
Router::Register("view/document", DrawViewDocument);

Router::Register("view/legal", function()
{
    $d_Legal = Renderer::ParseTextURLs(Dashboard\View\Legal::GetDocumentText());

    $p_Legal = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "StaticTemplates", "Legal.html"));
    $p_Legal = Renderer::Format($p_Legal, "{DOCUMENT}", nl2br($d_Legal));

    Renderer::Draw($p_Legal);
});

Router::Register("view/economy", function()
{
    $d_Eco = Renderer::ParseTextURLs(Dashboard\View\Economy::GetDocumentText());

    $p_Eco = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "StaticTemplates", "Economy.html"));
    $p_Eco = Renderer::Format($p_Eco, "{DOCUMENT}", nl2br($d_Eco));

    Renderer::Draw($p_Eco);
});

Router::Register("view/index", function() {
    $p_Main = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "MainPage.html"));
    $p_Main  = Renderer::Format($p_Main, array("{COPYRIGHT_YEAR}"), date("Y"));
    Renderer::Draw($p_Main);
});

Router::Register("view/gallery.categories", function() {
    $p_Main = File::Load(Path::Combine(DOCUMENT_ROOT, "Assets", "View", "Templates", "GalleryPage.html"));
    Renderer::Draw($p_Main);
});

Router::Register("view/gallery.nature", function() {
    Renderer::Draw(GetFalleryCategoryExplorerContent("nature"));
});

Router::Register("view/gallery.hunting", function() {
    Renderer::Draw(GetFalleryCategoryExplorerContent("hunting"));
});

Router::Register("view/gallery.fishing", function() {
    Renderer::Draw(GetFalleryCategoryExplorerContent("fishing"));
});

Router::Register("dashboard", function() {
    Dashboard::RenderPage();
});

Router::RegisterBundle(array(
    "dashboard/news",
    "dashboard/contacts",
    "dashboard/photos",
    "dashboard/economy",
    "dashboard/legal",
    "dashboard/pricelist",
    "dashboard/news/editor",
    "dashboard/news/create",
    "dashboard/photos/add",
    "dashboard/news/save",
    "dashboard/legal/save",
    "dashboard/economy/save"
), function() {
    Dashboard::RenderDashboardPage();
});

Router::Register("dashboard/controls/news.remove", function() {
    VerifyDashboardAccess();

    $result = DB::Request("DELETE FROM news WHERE id = " . (int)$_POST["news_id"]);

    $response_object = new \StdClass();
    $response_object->response = new \StdClass();
    $response_object->response->status = $result;
    $response_object->response->msg = $_POST["news_id"];

    echo json_encode($response_object);
    exit();
});

Router::Register("dashboard/controls/contacts.remove", function() {
    VerifyDashboardAccess();

    $cid = (int)$_POST['contact_id'];

    if(!$cid)
    {
        ResponseJSONStatusError(0, "contact id error");
    }

    $delete_status = DB::Request("DELETE FROM contacts WHERE id = $cid");
    if($delete_status)
    {
        ResponseJSONStatusOK(true);
    }
    else
    {
        ResponseJSONStatusError(0, "deleting error");
    }

});

Router::Register("dashboard/controls/contacts.create", function() {
    VerifyDashboardAccess();

    $header = htmlspecialchars($_POST['header']);
    $people = htmlspecialchars($_POST['people']);
    $phone  = htmlspecialchars($_POST['phone']);
    $email  = htmlspecialchars($_POST['email']);

    if((empty($header) || empty($people)) || (empty($phone) && empty($email)))
    {
        ResponseJSONStatusError(0, "missing args");
    }
    else
    {
        $insert_status = DB::Request("INSERT INTO contacts (header, people, phone, email) VALUES ('$header', '$people', '$phone', '$email')");
        if($insert_status)
        {
            ResponseJSONStatusOK(true);
        }
        else
        {
            ResponseJSONStatusError(0, "creating error");
        }
    }

});

Router::Register("dashboard/controls/price.list.add", function() {
    VerifyDashboardAccess();
    $header = htmlspecialchars($_POST['header']);
    $price  = (int)$_POST['price'];

    if(empty($header) || !$price)
    {
        ResponseJSONStatusError(0, "missing args");
    }
    else
    {
        $insert_status = DB::Request("INSERT INTO price_list (header, price) VALUES ('$header', $price)");
        if($insert_status)
        {
            ResponseJSONStatusOK(true);
        }
        else
        {
            ResponseJSONStatusError(0, "adding error");
        }
    }

});

Router::Register("dashboard/controls/price.list.remove", function() {
    VerifyDashboardAccess();
    $pid = (int)$_POST['id'];

    if(!$pid)
    {
        ResponseJSONStatusError(0, "price item id error");
    }

    $delete_status = DB::Request("DELETE FROM price_list WHERE id = $pid");
    if($delete_status)
    {
        ResponseJSONStatusOK(true);
    }
    else
    {
        ResponseJSONStatusError(0, "deleting error");
    }

});

Router::Register("dashboard/controls/photos.remove", function() {
    VerifyDashboardAccess();
    $pid = (int)$_POST['id'];

    if(!$pid)
    {
        ResponseJSONStatusError(0, "Photo with id ($pid) not found");
    }

    $delete_status = DB::Request("DELETE FROM photo_gallery WHERE id = $pid");
    if($delete_status)
    {
        ResponseJSONStatusOK(true);
    }
    else
    {
        ResponseJSONStatusError(0, "deleting error");
    }

});

function VerifyDashboardAccess()
{
    session_start();
    if(!Dashboard::IsAccess())
    {
        header("location: /dashboard");
        exit();
    }
}

function ResponseJSONStatusOK($data)
{
    $response_object = new \StdClass();
    $response_object->response = $data;

    echo json_encode($response_object);
    exit();
}

function ResponseJSONStatusError($code, $message)
{
    $response_object = new \StdClass();
    $response_object->error->code    = $code;
    $response_object->error->message = $message;

    echo json_encode($response_object);
    exit();
}

function RequireRouteFunction($filename)
{
    $path = __DIR__ . "/routes/" . $filename . ".php";
    require $path;
}