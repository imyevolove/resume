<?php

define("FETCH_ASSOC", "assoc");
define("FETCH_ARRAY", "array");

class DB
{
    private static $settings = null;
    private static $connection = null;

    public static function Request($query, $method = null)
    {
        self::Connect();

        $result = mysql_query($query);

        switch($method)
        {
            case FETCH_ASSOC:
                return mysql_fetch_assoc($result);
            case FETCH_ARRAY:
                $data = array();
                $num = mysql_num_fields($result);
                $types = new \StdClass();

                $tmpInf;
                $tmpInfName;
                $tmpInfType;
                $tmpRow;

                for($i = 0; $i < $num; $i++)
                {
                    $tmpInf = mysql_fetch_field($result, $i);
                    $tmpInfName = $tmpInf->name;
                    $tmpInfType = $tmpInf->type;
                    $types->$tmpInfName = $tmpInfType;
                }

                while($row = mysql_fetch_array($result, true))
                {
                    $tmpRow = new \StdClass();
                    foreach($row as $key => $value)
                    {
                        $tmpRow->$key = self::FormatValueByType($value, $types->$key);
                    }
                    array_push($data, $tmpRow);
                }
                return $data;
            default: return $result;
        }
    }

    public static function Connect()
    {
        if(!is_null(self::$connection)) return;

        $settings = self::GetSettings();
        self::$connection = mysql_connect(
            $settings['host'],
            $settings['user'],
            $settings['password']
        );
        mysql_set_charset('utf8', self::$connection);
        mysql_select_db($settings['name']);
    }

    public static function Disconnect()
    {
        if(is_null(self::$connection)) return;

        mysql_close(self::$connection);
        self::$connection = null;
    }

    private static function GetSettings()
    {
        if(is_null(self::$settings))
            self::$settings = parse_ini_file(SERVER_ROOT . "/conf/db.ini");

        return self::$settings;
    }

    private static function FormatValueByType($value, $type)
    {
        switch($type)
        {
            case "int": return (int)$value;
            case "string": return (string)$value;
            default: return $value;
        }
    }
}