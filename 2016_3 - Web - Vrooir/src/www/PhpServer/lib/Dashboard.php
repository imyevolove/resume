<?php

use \Renderer;
use \File;
use \Path;
use \DB;
use \Dashboard\View\News;
use \Dashboard\View\Contacts;
use \Dashboard\View\PriceList;
use \Dashboard\View\Photos;
use \Dashboard\View\Legal;
use \Dashboard\View\Economy;

class Dashboard
{
    public static function RenderPage()
    {
        // Старутем сессию
        session_start();

        // Проверяем попытку авторизации
        $p_Login = self::FormatInputData($_POST["login"]);
        $p_Psswd = self::StrToHash(self::FormatInputData($_POST["password"]));

        if(!empty($p_Login) && !empty($p_Psswd))
        {
            $auth_UserData = DB::Request("SELECT * FROM admins WHERE login = '$p_Login' AND password = '$p_Psswd'", FETCH_ASSOC);
            if(!empty($auth_UserData) && !!$auth_UserData)
            {
                $_SESSION["uid"] = $auth_UserData["uid"];
                header("location: /dashboard/news");
                exit();
            }
        }

        // Попытка авторизации с помощью сессии
        if(self::IsAccess())
        {
            header("location: /dashboard/news");
        }
        else
        {
            self::RenderAuthPage();
        }
    }

    public static function RenderDashboardPage()
    {
        // Старутем сессию
        session_start();

        if(!self::IsAccess())
        {
            header("location: /dashboard");
            exit();
        }

        $url = preg_replace('/\\?.*/', '', $_SERVER['REQUEST_URI']);
        $sector = str_replace("/dashboard/", "", $url);

        switch($sector) {
            case "news":
                $p_Dashboard = self::GetModifiedDashboardIndexPage("news", News::GetContent());
                Renderer::Draw($p_Dashboard);
                break;
            case "contacts":
                $p_Dashboard = self::GetModifiedDashboardIndexPage("contacts", Contacts::GetContent());
                Renderer::Draw($p_Dashboard);
                break;
            case "pricelist":
                $p_Dashboard = self::GetModifiedDashboardIndexPage("pricelist", PriceList::GetContent());
                Renderer::Draw($p_Dashboard);
                break;
            case "photos":
                $p_Dashboard = self::GetModifiedDashboardIndexPage("photos", Photos::GetContent());
                Renderer::Draw($p_Dashboard);
                break;
            case "economy":
                $p_Dashboard = self::GetModifiedDashboardIndexPage("economy", Economy::GetContent());
                Renderer::Draw($p_Dashboard);
                break;
            case "legal":
                $p_Dashboard = self::GetModifiedDashboardIndexPage("legal", Legal::GetContent());
                Renderer::Draw($p_Dashboard);
                break;
            case "news/editor":
                $nid = (int)$_GET['nid'];
                $p_NewsEditor = File::Load(Path::Combine(DOCUMENT_ROOT, "DashboardAssets", "View", "Page", "NewsEditor.html"));
                $script = "";

                if (empty($nid) || !$nid)
                {
                    $script = "Editor.LoadDataFromSessionStorage();";
                }
                else
                {

                    $data = DB::Request("SELECT * FROM news WHERE id = $nid", FETCH_ASSOC);

                    if(!empty($data) && !!$data)
                    {
                        $nid   = $data["id"];
                        $title = $data["header"];
                        $text  = nl2br($data["document"]);
                        $text  = str_replace(array("\r","\n"),"", $text);

                        $image = "/upload/news/" . $data["image"];
                        $script = "Editor.SetData('$title', '$text', '$image');
                        Editor.SetButtonAction(ACTION_SAVE);
                        document.getElementById('ai-news-id').value = $nid;
                        ";
                    }
                    else
                    {
                        $script = "sessionStorage.clear()";
                    }
                }

                $p_NewsEditor = Renderer::Format($p_NewsEditor, array("{SCRIPT}"), array($script));
                Renderer::Draw($p_NewsEditor);
                break;
            case "news/create":
                $title = htmlspecialchars($_POST["title"], ENT_QUOTES);
                $text  = htmlspecialchars($_POST["text" ], ENT_QUOTES);
                $image = $_POST["image"];

                $image = str_replace(' ','+', $image);
                $image =  substr($image, strpos($image,",") + 1);
                $image = base64_decode($image);

                if(empty($image))
                {
                    echo "Не указано изображение или ошибка в чтении изображения";
                    //header("location: /dashboard/news/editor");
                    exit();
                }

                try
                {
                    $imageFile = imagecreatefromstring($image);

                    if(!empty($title) && !empty($text) && !!$imageFile)
                    {
                        $fname = uniqid('upl_') . ".png";
                        $fpath = DOCUMENT_ROOT . "/upload/news/" . $fname;
                        $file = fopen($fpath, 'w');
                        fwrite($file, $image);
                        fclose($file);

                        $author_uid = $_SESSION["uid"];
                        $insert_result = DB::Request("
                                                      INSERT INTO news (author, image, document, header, creation_date)
                                                      VALUES('$author_uid', '$fname', '$text', '$title', " . time() . ") ");

                        if(!!$insert_result)
                        {
                            header("location: /dashboard");
                            exit();
                        }
                        else
                        {
                            echo "Ошибка вставки записи";
                            echo mysql_error();
                            //header("location: /dashboard/news/editor");
                            exit();
                        }
                    }

                    header("location: /dashboard");
                }
                catch(Exception $e)
                {
                    header("location: /dashboard/news/editor");
                    exit();
                }
                break;
            case "news/save":
                $nid   = (int)$_POST["id"];
                $title = $_POST["title"];
                $text  = $_POST["text" ];
                $image = $_POST["image"];

                $image = str_replace(' ', '+', $image);
                $image =  substr($image, strpos($image, ",") + 1);
                $image = base64_decode($image);

                if(empty($nid) || !$nid)
                {
                    header("location: /dashboard");
                    exit();
                }

                if(empty($image))
                {
                    header("location: /dashboard/news/editor?nid=$nid");
                    exit();
                }

                try
                {
                    $imageFile = imagecreatefromstring($image);

                    if(!empty($title) && !empty($text) && !!$imageFile)
                    {
                        $fname = uniqid('upl_') . ".png";
                        $fpath = DOCUMENT_ROOT . "/upload/news/" . $fname;
                        $file = fopen($fpath, 'w');
                        fwrite($file, $image);
                        fclose($file);

                        $author_uid = $_SESSION["uid"];
                        $insert_result = DB::Request("
                                                      UPDATE news SET
                                                      author = '$author_uid',
                                                      image = '$fname',
                                                      document = '$text',
                                                      header = '$title',
                                                      creation_date = " . time() . "
                                                      WHERE id = $nid");

                        if(!!$insert_result)
                        {
                            header("location: /dashboard");
                            exit();
                        }
                        else
                        {
                            header("location: /dashboard/news/editor?nid=$nid");
                            exit();
                        }
                    }

                    header("location: /dashboard");
                }
                catch(Exception $e)
                {
                    header("location: /dashboard/news/editor?nid=$nid");
                    exit();
                }
                break;
            case "photos/add":
                $allowedTypes = array("image/jpeg", "image/jpg", "image/png");
                $detectedType = $_FILES["image-file"]['type'];
                $error = !in_array($detectedType, $allowedTypes);

                $category = htmlspecialchars($_POST["category"]);
                $image = $_FILES["image-file"];

                if($error || empty($category))
                {
                    echo "Ошибка. Не указано изображение или категория";
                    exit();
                }

                $uploaddir = DOCUMENT_ROOT . "/upload/gallery/";
                $uploadfilename = uniqid("upl_gl_") . "." . pathinfo($image['name'], PATHINFO_EXTENSION);
                $uploadfile = $uploaddir . $uploadfilename;

                if (move_uploaded_file($image['tmp_name'], $uploadfile))
                {
                    $insert_result = DB::Request("INSERT INTO photo_gallery (category, image) VALUES('$category', '$uploadfilename')");
                    if($insert_result)
                    {
                        // $uploaddir . "thumbs/" . $uploadfilename
                        self::create_cropped_thumbnail($uploadfile, 250, 250, "thm");

                        header("location: /dashboard/photos");
                    }
                    else
                    {
                        echo "Ошибка создания записи";
                    }

                    exit();
                }
                else
                {
                    echo "Ошибка загрузки файла";
                    exit();
                }

                break;
            case "legal/save":
                $document = htmlspecialchars($_POST["document"]);
                if(empty($document))
                {
                    echo "Ошибка сохранения";
                    exit();
                }
                else
                {
                    Legal::SetDocumentText($document);
                    header("location: /dashboard/legal");
                }
                break;
            case "economy/save":
                $document = htmlspecialchars($_POST["document"]);
                if(empty($document))
                {
                    echo "Ошибка сохранения";
                    exit();
                }
                else
                {
                    Economy::SetDocumentText($document);
                    header("location: /dashboard/economy");
                }
                break;
        }

        exit();
    }

    public static function GetModifiedDashboardIndexPage($active_button_id, $content)
    {
        $p_Dashboard = File::Load(Path::Combine(DOCUMENT_ROOT, "DashboardAssets", "View", "Page", "Index.html"));
        $p_Dashboard = Renderer::Format($p_Dashboard, array("{ACTIVE_BUTTON}", "{PAGE_CONTAINER}"), array($active_button_id, $content));
        return $p_Dashboard;
    }

    public static function IsAccess()
    {
        $i = $_SESSION["uid"];

        if(empty($i))
        {
           return false;
        }

        $user_data = DB::Request("SELECT * FROM admins WHERE uid = '$i'", FETCH_ASSOC);
        if(empty($user_data) || !$user_data)
        {
            return false;
        }
        return true;
    }

    private static function StrToHash($str)
    {
        return md5(md5($str));
    }

    private static function FormatInputData($str)
    {
        return htmlspecialchars($str);
    }

    private static function RenderAuthPage()
    {
        Renderer::Draw(File::Load(Path::Combine(DOCUMENT_ROOT, "DashboardAssets", "View", "Page", "Auth.html")));
        exit();
    }

    private static function create_cropped_thumbnail($image_path, $thumb_width, $thumb_height, $prefix)
    {

        if (!(is_integer($thumb_width) && $thumb_width > 0) && !($thumb_width === "*")) {
            echo "The width is invalid";
            exit(1);
        }

        if (!(is_integer($thumb_height) && $thumb_height > 0) && !($thumb_height === "*")) {
            echo "The height is invalid";
            exit(1);
        }

        $extension = pathinfo($image_path, PATHINFO_EXTENSION);
        switch ($extension) {
            case "jpg":
            case "jpeg":
                $source_image = imagecreatefromjpeg($image_path);
                break;
            case "gif":
                $source_image = imagecreatefromgif($image_path);
                break;
            case "png":
                $source_image = imagecreatefrompng($image_path);
                break;
            default:
                exit(1);
                break;
        }

        $source_width = imageSX($source_image);
        $source_height = imageSY($source_image);

        if (($source_width / $source_height) == ($thumb_width / $thumb_height)) {
            $source_x = 0;
            $source_y = 0;
        }

        if (($source_width / $source_height) > ($thumb_width / $thumb_height)) {
            $source_y = 0;
            $temp_width = $source_height * $thumb_width / $thumb_height;
            $source_x = ($source_width - $temp_width) / 2;
            $source_width = $temp_width;
        }

        if (($source_width / $source_height) < ($thumb_width / $thumb_height)) {
            $source_x = 0;
            $temp_height = $source_width * $thumb_height / $thumb_width;
            $source_y = ($source_height - $temp_height) / 2;
            $source_height = $temp_height;
        }

        $target_image = ImageCreateTrueColor($thumb_width, $thumb_height);

        imagecopyresampled($target_image, $source_image, 0, 0, $source_x, $source_y, $thumb_width, $thumb_height, $source_width, $source_height);

        $bsn = pathinfo($image_path, PATHINFO_BASENAME);
        $bdr = pathinfo($image_path, PATHINFO_DIRNAME);
        switch ($extension) {
            case "jpg":
            case "jpeg":
                imagejpeg($target_image, $bdr . "/" . $prefix . "_" . $bsn);
                break;
            case "gif":
                imagegif($target_image, $bdr . "/" . $prefix . "_" . $bsn);
                break;
            case "png":
                imagepng($target_image, $bdr . "/" . $prefix . "_" . $bsn);
                break;
            default:
                exit(1);
                break;
        }

        imagedestroy($target_image);
        imagedestroy($source_image);
    }
}