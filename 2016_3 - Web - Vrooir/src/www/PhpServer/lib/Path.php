<?php

class Path
{
    public static function Combine()
    {
        $args = func_get_args();
        return join(DIRECTORY_SEPARATOR, $args);
    }
}