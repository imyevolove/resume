<?php

class File
{
    public static function Load($path)
    {
        if(file_exists($path))
        {
            return file_get_contents($path);
        }
        else
        {
            throw new FileNotExistsException("File ($path) not exists");
        }
    }
}

class FileNotExistsException extends \Exception {}