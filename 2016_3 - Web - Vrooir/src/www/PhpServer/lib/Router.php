<?php

/**
 * Created by PhpStorm.
 * User: �������
 * Date: 13.05.2016
 * Time: 17:28
 */
class Router
{
    private static $Routes = array();
    private static $Error404Func;

    public static function Register($url, $callback)
    {
        if(!is_string($url)) throw new \RouterException("Url parameter is not string");
        if(!is_callable($callback)) throw new \RouterException("Callback parameter is not function");

        $url = self::FormatUrl($url);
        self::$Routes[$url] = $callback;
    }

    public static function RegisterBundle($urls, $callback)
    {
        foreach($urls as $url)
        {
            self::Register($url, $callback);
        }
    }

    public static function Register404Error($callback)
    {
        if(!is_callable($callback)) throw new \RouterException("Callback parameter is not function");
        self::$Error404Func = $callback;
    }

    public static function __ShutdownEvent()
    {
        $url = preg_replace('/\\?.*/', '', $_SERVER['REQUEST_URI']);

        foreach (self::$Routes as $path => $callback)
        {
            if($path == $url)
            {
                call_user_func($callback);
                exit();
            }
        }

        if(is_callable(self::$Error404Func)) call_user_func(self::$Error404Func);
        exit();
    }

    private static function FormatUrl($url)
    {
        if(!is_string($url)) throw new \RouterException("Url parameter is not string");

        // ��������� ����
        $url = "/" . $url;

        // ������� GET ������
        $url = preg_replace('/\\?.*/', '', $url);

        // ������� ������� �����
        $url = preg_replace('#/+#','/',$url);

        return $url;
    }
}

class RouterException extends \Exception {}

// ������������ ������� �������� ����
register_shutdown_function("Router::__ShutdownEvent");