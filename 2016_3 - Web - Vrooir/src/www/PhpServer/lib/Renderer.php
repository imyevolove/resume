<?php

// Отвечает за отрисовку страниц сайта
class Renderer
{

    public static function Draw($content)
    {
        echo $content;
    }

    public static function Format($subjectString, $searchArray, $replaceArray)
    {
        return str_replace($searchArray, $replaceArray, $subjectString);
    }

    public static function ParseTextURLs($text)
    {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        $img_array = array("JPG", "JPEG", "PNG", "GIF", "WEBP");

        if(preg_match_all($reg_exUrl, $text, $urls))
        {
            $urls = array_unique($urls[0]);
            $lkc = 0;

            foreach($urls as $url)
            {

                $ext = pathinfo($url, PATHINFO_EXTENSION);

                if(in_array(strtoupper($ext), $img_array))
                {
                    $text = str_replace($url, "<img src='$url' width='100%'></img> ", $text);
                }
                else
                {
                    $lkc++;
                    $text = str_replace($url, "<a target='_blank' href='{$url}'>Ссылка $lkc</a> ", $text);
                }
            }
            return $text;
        }
        else
        {
            return $text;
        }
    }
}