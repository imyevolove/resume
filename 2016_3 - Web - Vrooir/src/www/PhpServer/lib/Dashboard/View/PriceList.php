<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.05.2016
 * Time: 20:37
 */

namespace Dashboard\View;


use \DB;

class PriceList
{
    public static function GetContent()
    {
        return '
                <form class="dh_contacts_form">
                    <table>
                        <tr>
                            <td class="dh_contacts_form_sector">
                                <div>Товар или оглавление</div>
                                <input type="text" id="form_price_list_header" />
                            </td>
                            <td class="dh_contacts_form_sector">
                                <div>Цена</div>
                                <input type="number" id="form_price_list_price" />
                            </td>
                            <td class="dh_contacts_form_apply">
                                <a onclick="Dashboard.PriceList.Register(
                                document.getElementById(\'form_price_list_header\').value,
                                document.getElementById(\'form_price_list_price\'  ).value
                                )">Создать</a>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="dh_body_container">
                    <div class="dh_body_container_inside">
                        <div class="dh_body_container_inside_fix">
                            <div class="dh_body_container_scrollable" id="document-container">
                                 ' . self::BuildPriceListHTML() . '
                            </div>
                        </div>
                    </div>
                </div>
                ';
    }

    private static function BuildPriceListHTML()
    {
        $contacts = DB::Request("SELECT * FROM price_list ORDER BY id DESC", FETCH_ARRAY);
        $content = "";

        foreach($contacts as $contacts_data)
        {
            $content .= self::CreatePriceFormHTML(
                $contacts_data->id,
                $contacts_data->header,
                $contacts_data->price
            );
        }

        return $content;
    }

    private static function CreatePriceFormHTML($id, $header, $price)
    {
        return '
        <div class="dh_contacts_item" id="price-i' . $id . '">
            <div>'  . $header . '</div>
            <div>'  . $price . '</div>
            <div>
                <a class="dh_contacts_control_delete" onclick="Dashboard.PriceList.Remove(' . $id . ');">удалить</a>
            </div>
        </div>
        ';
    }
}