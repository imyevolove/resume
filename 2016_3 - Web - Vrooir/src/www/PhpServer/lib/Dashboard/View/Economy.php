<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 20.05.2016
 * Time: 23:47
 */

namespace Dashboard\View;
use \Path;
use \File;

class Economy
{
    public static function GetContent()
    {
        $document = self::GetDocumentText();

        return "
                <form class='dh_legal_form' action='/dashboard/economy/save' target='_self' method='post'>
                    <div style='width: 100%; height: 100%;'>
                         <textarea name='document' style='resize: none' placeholder='текст'>$document</textarea>
                    </div>
                    <div class='dh_legal_controls_wrap'>
                        <div class='dh_legal_controls'>
                            <button>Сохранить</button>
                        </div>
                    </div>
                </form>
        ";
    }

    public static function GetDocumentText()
    {
        return htmlspecialchars_decode(File::Load(self::GetFilePath()));
    }

    public static function SetDocumentText($text)
    {
        $file = fopen(self::GetFilePath(), "w+");
        fwrite($file, htmlspecialchars($text));
        fclose($file);
    }

    public static function GetFilePath()
    {
        return Path::Combine(DOCUMENT_ROOT, "docs", "economy.doc");
    }
}