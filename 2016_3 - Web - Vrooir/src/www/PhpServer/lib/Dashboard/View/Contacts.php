<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.05.2016
 * Time: 20:37
 */

namespace Dashboard\View;
use \DB;

class Contacts
{
    public static function GetContent()
    {
        return '
                <form class="dh_contacts_form">
                    <table>
                        <tr>
                            <td class="dh_contacts_form_sector">
                                <div>Должность</div>
                                <input type="text" id="form_contacts_header" />
                            </td>
                            <td class="dh_contacts_form_sector">
                                <div>ФИО</div>
                                <input type="text" id="form_contacts_name" />
                            </td>
                            <td class="dh_contacts_form_sector">
                                <div>Телефон</div>
                                <input type="text" id="form_contacts_phone" />
                            </td>
                            <td class="dh_contacts_form_sector">
                                <div>Эл. почта</div>
                                <input type="email" id="form_contacts_email" />
                            </td>
                            <td class="dh_contacts_form_apply">
                                <a onclick="Dashboard.Contacts.Create(
                                document.getElementById(\'form_contacts_header\').value,
                                document.getElementById(\'form_contacts_name\'  ).value,
                                document.getElementById(\'form_contacts_phone\' ).value,
                                document.getElementById(\'form_contacts_email\' ).value
                                )">Добавить</a>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="dh_body_container">
                    <div class="dh_body_container_inside">
                        <div class="dh_body_container_inside_fix">
                            <div class="dh_body_container_scrollable" id="document-container">
                                 ' . self::BuildContactsListHTML() . '
                            </div>
                        </div>
                    </div>
                </div>
                ';
    }

    private static function BuildContactsListHTML()
    {
        $contacts = DB::Request("SELECT * FROM contacts", FETCH_ARRAY);
        $content = "";

        foreach($contacts as $contacts_data)
        {
            $content .= self::CreateContactFormHTML(
                $contacts_data->id,
                $contacts_data->header,
                $contacts_data->people,
                $contacts_data->phone,
                $contacts_data->email
            );
        }

        return $content;
    }

    private static function CreateContactFormHTML($id, $header, $people, $phone, $email)
    {
        return '
        <div class="dh_contacts_item" id="contact-i' . $id . '">
            <div>'  . $header . '</div>
            <div>'  . $people . '</div>
            <div>'   . $phone  . '</div>
            <div>'  . $email  . '</div>
            <div>
                <a class="dh_contacts_control_delete" onclick="Dashboard.Contacts.Remove(' . $id . ');">удалить</a>
            </div>
        </div>
        ';
    }
}