<?php

namespace Dashboard\View;
use \DB;

class News
{
    public static function GetContent()
    {
        $news_list = DB::Request("
              SELECT ns.id, ns.header, ns.image, LEFT(ns.document, 350) AS excerpt, am.login as author, ns.creation_date as date
              FROM news as ns, admins as am
              WHERE ns.author = am.uid
              ORDER BY ns.id DESC",
            FETCH_ARRAY
        );
        $news_content = "";

        foreach ($news_list as $article)
        {
            $news_content .= self::BuildItemContent(
                $article->id,
                $article->header,
                "/upload/news/" . $article->image,
                $article->excerpt,
                $article->author,
                self::TimestampToFDate($article->date)
            );
        }

        return '
                   <div class="dh_controls_wrap">
                        <div class="dh_tab_controls_wrap">
                            <div class="dh_tab_controls_place">
                                <div class="dh_controls_info">
                                    <h4>Менеджер новостей</h4>
                                    <h5>Чтобы начать работу выберите одну из задач справа</h5>
                                </div>
                                <div class="dh_controls_container">
                                    <div class="dh_button dh_icon_plus" onclick="sessionStorage.clear();location = \'/dashboard/news/editor\';"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dh_body_container">
                        <div class="dh_body_container_inside">
                            <div class="dh_body_container_inside_fix">
                                <div class="dh_body_container_scrollable" id="document-container"><div class="dh_news_container">
                        <div class="dh_news_container_list">
                            ' . $news_content . '
                        </div>
                    </div></div>
                </div>
            </div>
        </div>
        ';
    }

    private static function TimestampToFDate($timestamp)
    {
        if(empty($timestamp) || !is_numeric($timestamp))
        {
            return "undefined";
        }

        return gmdate("d-m-Y (H:i)", $timestamp);
    }

    private static function BuildItemContent($id, $title, $image, $text, $author, $date)
    {
        return '
                 <div class="dh_news_item" id="news-i' . $id . '">
                <div class="dh_news_item_preview_icon">
                    <div class="dh_news_item_icon" style="background-image: url(' . $image . ')"></div>
                </div>
                <div class="dh_news_item_info">
                    <div class="dh_news_item_header">
                        <div class="dh_news_item_header_box">
                            <div class="dh_news_item_header_info">
                                <h2>' . $title . '</h2>
                                <div class="dh_news_item_stats">
                                    <a class="dh_news_item_author">
                                        ' . $author . '
                                    </a><a class="dh_news_item_date">
                                        ' . $date . '
                                    </a>
                                </div>
                            </div>
                            <div class="dh_news_item_header_controls">
                                <div class="dh_button dh_icon_edit"   onclick="Dashboard.News.Edit(\'' . $id . '\')"></div>
                                <div class="dh_button dh_icon_remove" onclick="Dashboard.News.Remove(\'' . $id . '\')"></div>
                            </div>
                        </div>
                    </div>
                    <div class="dh_news_item_document">' . $text . '</div>
                </div>
            </div>
            ';
    }
}