<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.05.2016
 * Time: 20:38
 */

namespace Dashboard\View;
use \DB;

class Photos
{
    public static function GetContent()
    {
        return "
        <div class='dh_photos_wrap'>
            <div class='dh_photos_container_wrap'>
                <div class='dh_photos_container' id='document-container'>
                    " . self::BuildPhotosListHTML() . "
                </div>
            </div>
            <div class='dh_photos_controls_wrap'>
                <form class='dh_photos_add_form' target='_self' method='post' enctype='multipart/form-data' action='/dashboard/photos/add'>
                    <div class='dh_photos_table'>
                        <div class='dh_photos_category'>
                            <div class='dh_photos_category_item'>
                                <input type='radio' name='category' value='nature'  checked />
                                <span>Природа</span>
                            </div>
                            <div class='dh_photos_category_item'>
                                <input type='radio' name='category' value='hunting' />
                                <span>Охота</span>
                            </div>
                            <div class='dh_photos_category_item'>
                                <input type='radio' name='category' value='fishing' />
                                <span>Рыбалка</span>
                            </div>
                        </div>
                        <div class='dh_photos_file'>
                            <input type='file' accept='image/*' name='image-file' />
                        </div>
                        <div class='dh_photos_buttons'>
                            <button>Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        ";
    }

    private static function BuildPhotosListHTML()
    {
        $content = "";
        $photos = DB::Request("SELECT * FROM photo_gallery ORDER BY category DESC", FETCH_ARRAY);

        foreach($photos as $photo_data)
        {
            $content .= self::BuildPhotoItemHTML(
                $photo_data->id,
                $photo_data->category,
                self::GetThumbnail($photo_data->image)
            );
        }

        return $content;
    }

    private static function BuildPhotoItemHTML($id, $category, $image)
    {
        return  "<div class='dh_photos_item' id='photo-i$id'>
                    <div class='dh_photos_preview'>
                        <div class='dh_photos_image' style='background-image: url(/upload/gallery/$image)'></div>
                    </div>
                    <div class='dh_photos_info'>
                        <div class='dh_photos_category_info'>$category</div>
                        <div class='dh_photos_delete' onclick='Dashboard.Photos.Delete(\"$id\")'>Удалить</div>
                    </div>
                </div>";
    }

    public static function IsHaveThumbnail($image)
    {
        return file_exists(DOCUMENT_ROOT . "/upload/gallery/thm_$image");
    }

    public static function GetThumbnail($image)
    {
        if(self::IsHaveThumbnail($image)) return "thm_" . $image;
        return $image;
    }
}