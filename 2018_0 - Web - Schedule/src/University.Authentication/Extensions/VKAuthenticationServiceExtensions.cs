﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using University.Authentication.VK;

namespace University.Authentication
{
    public static class VKAuthenticationServiceExtensions
    {
        public static AuthenticationBuilder AddVK(this AuthenticationBuilder builder, Action<VKOptions> setup)
        {
            var options = new VKOptions();
            setup.Invoke(options);

            return builder;
        }
    }
}
