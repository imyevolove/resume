﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class EducationSpecialtyMapConfigurator : EntityMappingConfiguration<EducationSpecialty>
    {
        public override void Configure(EntityTypeBuilder<EducationSpecialty> builder)
        {
            builder
                .HasIndex(p => p.SpecialtyCode)
                .IsUnique(true);
        }
    }
}
