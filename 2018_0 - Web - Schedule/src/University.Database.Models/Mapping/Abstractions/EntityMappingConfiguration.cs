﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace University.Database.Models
{
    public abstract class EntityMappingConfiguration<T> : IEntityMappingConfiguration<T> where T : class
    {
        public abstract void Configure(EntityTypeBuilder<T> builder);

        public virtual void Configure(ModelBuilder builder)
        {
            Configure(builder.Entity<T>());
        }
    }
}
