﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class QualificationMapConfigurator : EntityMappingConfiguration<Qualification>
    {
        public override void Configure(EntityTypeBuilder<Qualification> builder)
        {
            builder
                .HasIndex(p => p.Name)
                .IsUnique(true);
        }
    }
}
