﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class ScheduleRecordMapConfigurator : EntityMappingConfiguration<ScheduleRecord>
    {
        public override void Configure(EntityTypeBuilder<ScheduleRecord> builder)
        {
            builder
                .HasOne(record => record.ScheduleSheet)
                .WithMany(sheet => sheet.Records)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
