﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class DisciplineMethodMapConfigurator : EntityMappingConfiguration<DisciplineMethod>
    {
        public override void Configure(EntityTypeBuilder<DisciplineMethod> builder)
        {
            builder
                .HasIndex(p => p.Name)
                .IsUnique(true);
        }
    }
}
