﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class ScheduleMapConfigurator : EntityMappingConfiguration<Schedule>
    {
        public override void Configure(EntityTypeBuilder<Schedule> builder)
        {
            builder
                .HasIndex(p => new
                {
                    p.StudentGroupForeignKey,
                    p.ScheduleTypeForeignKey
                }).IsUnique(true);
        }
    }
}
