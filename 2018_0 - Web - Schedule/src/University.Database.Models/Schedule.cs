﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace University.Database.Models
{
    /// <summary>
    /// Модель расписания.
    /// </summary>
    public class Schedule : Entity
    {
        /// <summary>
        /// Статус публикации расписания
        /// </summary>
        [Required]
        public PublicationStatus PublicationStatus { get; set; } = PublicationStatus.Unpublished;

        [Required]
        public string StudentGroupForeignKey { get; set; }

        /// <summary>
        /// Таргетированная группа
        /// </summary>
        [ForeignKey("StudentGroupForeignKey")]
        public StudentGroup StudentGroup { get; set; }

        [Required]
        public string ScheduleTypeForeignKey { get; set; }

        /// <summary>
        /// Тип расписания
        /// </summary>
        [ForeignKey("ScheduleTypeForeignKey")]
        public ScheduleType ScheduleType { get; set; }
        
        /// <summary>
        /// Листы расписания
        /// </summary>
        public List<ScheduleSheet> Sheets { get; set; }
    }
}
