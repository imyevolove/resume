﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace University.Database.Models
{
    public class User : IdentityUser
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        [Required]
        public string UserLastName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [Required]
        public string UserFirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        [Required]
        public string UserMiddleName { get; set; }

        /// <summary>
        /// Create new user with auto-generated UserName
        /// </summary>
        /// <returns></returns>
        public static User CreateNewUser()
        {
            return new User { UserName = Guid.NewGuid().ToString() };
        }
    }
}
