﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace University.Database.Models
{
    /// <summary>
    /// Модель метода проведения дисциплины.
    /// </summary>
    public class DisciplineMethod : Entity
    {
        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(200)]
        public string Name { get; set; }
    }
}
