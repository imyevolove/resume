﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Database.Models
{
    /// <summary>
    /// Статус публикации чего-либо.
    /// </summary>
    public enum PublicationStatus
    {
        /// <summary>
        /// Неопубликовано и не должно быть показано всем пользователям
        /// </summary>
        Unpublished = 0,

        /// <summary>
        /// Опубликовано и может быть показано всем пользователям
        /// </summary>
        Published = 1
    }
}
