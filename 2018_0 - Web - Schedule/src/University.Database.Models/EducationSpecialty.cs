﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Database.Models
{
    public class EducationSpecialty : Entity
    {
        /// <summary>
        /// Полное имя направления подготовки
        /// </summary>
        [Required]
        public string FullName { get; set; }

        /// <summary>
        /// Кодовое имя кафедры, например 'ПИ'
        /// </summary>
        [Required]
        public string CodeName { get; set; }

        /// <summary>
        /// Код специальность, например '09.03.03'
        /// </summary>
        [Required]
        public string SpecialtyCode { get; set; }
    }
}
