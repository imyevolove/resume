﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace University.Database.Models
{
    /// <summary>
    /// Тип расписания.
    /// Используется для разграничения типов расписания.
    /// Например: Учебное, Сессия.
    /// </summary>
    public class ScheduleType : Entity
    {
        /// <summary>
        /// Наименование типа расписания
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
