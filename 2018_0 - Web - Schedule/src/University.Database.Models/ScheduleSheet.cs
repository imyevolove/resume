﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace University.Database.Models
{
    /// <summary>
    /// Модель расписания с индикацией приоритета недели.
    /// </summary>
    public class ScheduleSheet : Entity
    {
        [Required]
        public string ScheduleId { get; set; }
        public Schedule Schedule { get; set; }

        /// <summary>
        /// Приоритет недели.
        /// Используется для выбора расписания конкретной недели.
        /// </summary>
        [Required]
        public int WeekPriority { get; set; }

        /// <summary>
        /// Записи листа расписания.
        /// </summary>
        public List<ScheduleRecord> Records { get; set; }

        public int GetWeekPriorityIndex()
        {
            if (Schedule == null) return -1;
            if (Schedule.Sheets == null || Schedule.Sheets.Count <= 0) return 0;
            return Schedule.Sheets.IndexOf(this);
        }
    }
}
