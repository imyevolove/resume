﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class DashboardScheduleManagerDisplayViewModel
    {
        public readonly StudentGroup[] StudentGroups;
        public readonly ScheduleType[] ScheduleTypes;

        public DashboardScheduleManagerDisplayViewModel(
            StudentGroup[] studentGroups,
            ScheduleType[] scheduleTypes)
        {
            StudentGroups = studentGroups;
            ScheduleTypes = scheduleTypes;
        }
    }
}
