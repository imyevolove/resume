﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class NavigationCategoryViewModel
    {
        public string CategoryName { get; set; }
        public NavigationItemViewModel[] Items { get; set; }
    }
}
