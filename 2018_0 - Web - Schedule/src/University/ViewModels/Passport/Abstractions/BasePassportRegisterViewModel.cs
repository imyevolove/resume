﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Validation.DataAnnotations;

namespace University.ViewModels
{
    public abstract class BasePassportRegisterViewModel
    {
        /// <summary>
        /// Почта
        /// </summary>
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [StringLength(100, ErrorMessage = "{0} должен быть не менее {2} и не более {1} символов.", MinimumLength = 6)]
        [RequireDigits(ErrorMessage = "{0} должен содержать хотя бы одну цифру ('0'-'9').")]
        [RequireCharacterLowercase(ErrorMessage = "{0} должен содержать хотя бы один символ в нижнем регистре")]
        [RequireCharacterUppercase(ErrorMessage = "{0} должен содержать хотя бы один символ в верхнем регистре")]
        [RequireOnlyAlphanumeric(ErrorMessage = "{0} может содержать только латиницу и цифры")]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        [Compare("Password", ErrorMessage = "Пароль и пароль подтверждения не совпадают")]
        public string ConfirmPassword { get; set; }
    }
}
