﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiScheduleRecordGetResponseViewModel
    {
        public readonly string Id;
        public readonly Instructor Instructor;
        public readonly Discipline Discipline;
        public readonly DisciplineMethod DisciplineMethod;
        public readonly DayOfWeek Day;
        public readonly int StartTime;
        public readonly int Duration;
        public readonly string Location;

        public ApiScheduleRecordGetResponseViewModel(ScheduleRecord scheduleRecord)
        {
            Id = scheduleRecord.Id;
            Instructor = scheduleRecord.Instructor;
            Discipline = scheduleRecord.Discipline;
            DisciplineMethod = scheduleRecord.DisciplineMethod;
            StartTime = scheduleRecord.StartTime;
            Duration = scheduleRecord.Duration;
            Location = scheduleRecord.Location;
            Day = scheduleRecord.Day;
        }
    }
}
