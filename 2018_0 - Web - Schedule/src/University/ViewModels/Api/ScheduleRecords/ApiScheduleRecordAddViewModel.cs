﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiScheduleRecordAddViewModel : BaseApiScheduleRecordViewModel
    {
        [Required]
        public string ScheduleSheetId { get; set; }
    }
}
