﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiStudentGroupEditViewModel : ApiStudentGroupAddViewModel
    {
        [Required]
        public string StudentGroupId { get; set; }

        public bool? Expired { get; set; }
    }
}
