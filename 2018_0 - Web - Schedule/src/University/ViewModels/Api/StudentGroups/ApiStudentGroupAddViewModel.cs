﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiStudentGroupAddViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime OrganizationDate { get; set; }

        [Required]
        public string SpecialtyId { get; set; }

        [Required]
        public string EducationMethodId { get; set; }

        [Required]
        public string QualificationId { get; set; }
    }
}
