﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiInstructorsAddViewModel
    {
        [Required]
        [Display(Name = "Имя преподавателя")]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Фамилия преподавателя")]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Отчество преподавателя")]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        public string MiddleName { get; set; }
    }
}
