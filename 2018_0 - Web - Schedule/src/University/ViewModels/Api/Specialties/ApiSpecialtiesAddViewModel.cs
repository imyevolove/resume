﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiSpecialtiesAddViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Полное имя специальности")]
        public string FullName { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Кодовое имя специальности")]
        public string CodeName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Код специальности")]
        public string SpecialtyCode { get; set; }
    }
}
