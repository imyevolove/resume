﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiScheduleTypeEditViewModel : ApiScheduleTypeAddViewModel
    {
        [Required]
        public string ScheduleTypeId { get; set; }
    }
}
