﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiScheduleTypeAddViewModel
    {
        [Required]
        [Display(Name = "Наименование типа расписания")]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        public string Name { get; set; }
    }
}
