﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiScheduleSheetsEditViewModel
    {
        [Required]
        public string ScheduleSheetId { get; set; }

        public int? WeekPriority { get; set; }
    }
}
