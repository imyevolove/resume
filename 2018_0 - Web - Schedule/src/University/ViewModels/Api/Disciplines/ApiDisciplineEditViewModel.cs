﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiDisciplineEditViewModel : ApiDisciplineAddViewModel
    {
        [Required]
        public string DisciplineId { get; set; }
    }
}
