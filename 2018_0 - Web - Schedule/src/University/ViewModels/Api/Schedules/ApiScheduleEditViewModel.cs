﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiScheduleEditViewModel : ApiScheduleAddViewModel
    {
        [Required]
        public string ScheduleId { get; set; }

        public PublicationStatus? PublicationStatus { get; set; }
    }
}
