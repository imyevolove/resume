﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiScheduleGetResponseViewModel
    {
        public readonly string Id;
        public readonly PublicationStatus PublicationStatus;
        public readonly ScheduleType ScheduleType;
        public readonly StudentGroup StudentGroup;
        public readonly List<ApiScheduleSheetsGetResponseViewModel> Sheets = new List<ApiScheduleSheetsGetResponseViewModel>();

        public ApiScheduleGetResponseViewModel(Schedule schedule)
        {
            Id = schedule.Id;
            PublicationStatus = schedule.PublicationStatus;
            ScheduleType = schedule.ScheduleType;
            StudentGroup = schedule.StudentGroup;

            if (schedule.Sheets != null)
            {
                foreach (var sheet in schedule.Sheets)
                {
                    if (sheet == null) continue;
                    Sheets.Add(new ApiScheduleSheetsGetResponseViewModel(sheet));
                }
            }
        }
    }
}
