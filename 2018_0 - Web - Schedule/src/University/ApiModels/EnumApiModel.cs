﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University.ApiModels
{
    public class EnumApiModel
    {
        public string Name { get; set; }
        public int Index { get; set; }
    }
}
