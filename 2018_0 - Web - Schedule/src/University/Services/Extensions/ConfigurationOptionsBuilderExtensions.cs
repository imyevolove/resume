﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.MicrosoftAccount;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authentication.Twitter;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using University.Authentication.VK;
using University.Communication;

namespace University.Services
{
    public static class ConfigurationOptionsBuilderExtensions
    {
        public static EmailSenderOptions GetEmailSenderOptions(this IConfiguration config)
        {
            var section = config.GetSection("Smtp:Google");
            var credentials = section.GetSection("Credentials");
            return new EmailSenderOptions
            {
                Host = section["Host"],
                Port = section.GetValue("Port", 587),
                DefaultSender = new MailAddress(credentials["Username"]),
                Username = credentials["Username"],
                Password = credentials["Password"]
            };
        }

        public static GoogleOptions ConfigureGoogleAuthenticationOptions(this IConfiguration config, GoogleOptions options)
        {
            return ConfigureOAuthOptions(options, config, "Authentication:Google");
        }

        public static FacebookOptions ConfigureFacebookAuthenticationOptions(this IConfiguration config, FacebookOptions options)
        {
            return ConfigureOAuthOptions(options, config, "Authentication:Facebook");
        }

        public static TwitterOptions ConfigureTwitterAuthenticationOptions(this IConfiguration config, TwitterOptions options)
        {
            var section = config.GetSection("Authentication:Twitter");
            options.ConsumerKey = section["ConsumerKey"];
            options.ConsumerSecret = section["ConsumerSecret"];
            options.RetrieveUserDetails = true;
            return options;
        }

        public static MicrosoftAccountOptions ConfigureFacebookAuthenticationOptions(this IConfiguration config, MicrosoftAccountOptions options)
        {
            return ConfigureOAuthOptions(options, config, "Authentication:MicrosoftAccount");
        }

        public static VKOptions ConfigureVKAuthenticationOptions(this IConfiguration config, VKOptions options)
        {
            var credentials = config.GetSection("Authentication:VK");

            /// Без версии работать не будет
            options.Version = credentials["Version"];
            
            return ConfigureOAuthOptions(options, credentials);
        }

        private static T ConfigureOAuthOptions<T>(T options, IConfiguration configuration, string section) where T : OAuthOptions
        {
            return ConfigureOAuthOptions(options, configuration.GetSection(section));
        }

        private static T ConfigureOAuthOptions<T>(T options, IConfigurationSection section) where T : OAuthOptions
        {
            options.ClientId = section["ClientId"];
            options.ClientSecret = section["ClientSecret"];
            return options;
        }
    }
}
