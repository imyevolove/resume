﻿window.layouts = window.layouts || {};

window.layouts.async = window.layouts.async || {};

window.layouts.async.build = function (lockElement, errorRootElement, errorLogElement) {
    return new AsyncLayoutHandler(new ErrorBox(errorRootElement, errorLogElement), lockElement);
};

window.layouts.async.buildAutoApiHandler = function (rootFormElement, apiFunction, validResponseHandler, invalidResponseHandler)
{
    /// нормализуем входные параметры
    validResponseHandler = utils.normalizeFunction(validResponseHandler, function () { window.location.reload(); });
    invalidResponseHandler = utils.normalizeFunction(invalidResponseHandler);

    /// статус текущего запроса
    var requesting = false;

    /// Элементы слоя
    var blocker = rootFormElement.querySelector(".async-operation-block");
    var messageBox = rootFormElement.querySelector(".messages-container");

    /// ОБъект - обработчик слоя
    var layout = window.layouts.async.build(blocker, messageBox, messageBox);

    /// Кнопка работы с api
    var submitter = rootFormElement.querySelector(".submitter");
    submitter.addEventListener("click", function ()
    {
        /// Блокируем запросы отправки сообщения, 
        /// если запрос уже был создан и еще не был обработан
        if (requesting) {
            console.log("At least one request already was sended. Wait.");
            return;
        }

        /// Проводим валидацию формы
        var validationErrors = utils.validateFormErrors(rootFormElement);
        
        /// Форма не прошла валидацию
        if (validationErrors.length > 0) {
            layout.error.print(validationErrors);
            invalidResponseHandler(null);
            return;
        }

        /// Помечаем, что запрос был задан
        requesting = true;

        /// Блокируем доступ к форме и удаляем сообщения об ошибке, если таковые были
        layout.setActiveLock(true);
        layout.error.setActive(false);

        /// Формируем параметры запроса из формы
        var data = utils.getFormParameters(rootFormElement);

        /// Отправляем api запрос
        apiFunction(data, function (res)
        {
            /// Маркируем запрос как завершенный
            requesting = false;

            /// Снимаем блокировку с панели
            layout.setActiveLock(false);
            
            /// Результат был получен
            if (res['response'] != null)
            {
                validResponseHandler(res);
                //utils.clearFormParameters(rootFormElement);
            }
            /// Произошла ошибка
            else if (res['error'] != null)
            {
                invalidResponseHandler(res);

                /// Выводим сообщение об ошибке
                layout.error.print(res.error.errorMessage);
                console.error(res);
            }
        });
    });

    return layout;
};

window.layouts.async.buildAutoItemApiHandler = function (rootElement, lockElement, apiFunction, data, validResponseHandler, invalidResponseHandler)
{
    /// нормализуем входные параметры
    validResponseHandler = window.utils.normalizeFunction(validResponseHandler, function () { window.location.reload(); });
    invalidResponseHandler = window.utils.normalizeFunction(invalidResponseHandler);
    
    /// ОБъект - обработчик слоя
    var layout = new AsyncItemLayoutHandler(lockElement);

    /// Кнопка работы с api
    var submitter = rootElement.querySelector(".submitter");
    submitter.addEventListener("click", function ()
    {
        /// Блокируем доступ
        layout.setActiveLock(true);
        
        /// Отправляем api запрос
        apiFunction(data, function (res)
        {
            /// Снимаем блокировку с панели
            layout.setActiveLock(false);
            
            /// Результат был получен
            if (res['response'] != null)
            {
                validResponseHandler(res);
            }
            else
            {
                invalidResponseHandler(res);
            }
        });
    });

    return layout;
};

function ErrorBox(rootElement, logElement) {
    var that = this;

    this.elements = {};
    this.elements.rootElement = rootElement;
    this.elements.logElement = logElement;

    this.print = function (text)
    {
        var message = "";

        if (Array.isArray(text))
        {
            [].slice.call(text).forEach(function (msg) {
                message += "•" + msg + "<br/>";
            });

            message = message.replace(/^\s*<br\s*\/?>|<br\s*\/?>\s*$/g, '');
        }
        else
        {
            message = text;
        }

        that.elements.logElement.innerHTML = message;
        that.setActive(true);
    };

    this.setActive = function (enabled) {
        that.elements.rootElement.setAttribute("active", enabled ? "" : "no");
    };
}

function AsyncLayoutHandler(errorBox, lockElement) {
    var that = this;

    this.error = errorBox;

    this.elements = {};
    this.elements.lockElement = lockElement;

    this.setActiveLock = function (enabled) {
        that.elements.lockElement.setAttribute("active", enabled ? "" : "no");
    };

    (function () {
        that.error.setActive(false);
        that.setActiveLock(false);
    })();
}

function AsyncItemLayoutHandler(lockElement) {
    var that = this;
    
    this.elements = {};
    this.elements.lockElement = lockElement;

    this.setActiveLock = function (enabled) {
        that.elements.lockElement.setAttribute("active", enabled ? "" : "no");
    };

    (function () {
        that.setActiveLock(false);
    })();
}