window.dashboard = (function () {
    return {};
})();
window.dashboard.popup = window.dashboard.popup || {};

/// Windowed popup
(function (context)
{
    var windowed = context.windowed = {};
    var rootElement,
        contentElement,
        loaderElement,
        contentWrapperElement;

    windowed.getRootElement = function () { return rootElement; };
    windowed.getContentElement = function () { return contentElement; };
    windowed.updatePointers = function ()
    {
        rootElement = document.querySelector('[popup="windowed"]');
        contentElement = rootElement.querySelector('[popup-ref="content"]');
        loaderElement = rootElement.querySelector('[popup-ref="loader"]');
        contentWrapperElement = rootElement.querySelector('[popup-ref="content-wrapper"]');
    };

    windowed.loader = {};
    windowed.loader.setActive = function (enabled) { setActiveLoader(enabled); };

    windowed.close = function ()
    {
        clearContent();
        setActive(false);
    };

    windowed.load = function (url, parameters, callback)
    {
        setActiveContent(false);
        callback = utils.normalizeFunction(callback);
        
        setActive(true);

        window.request("post", url, parameters, (response) =>
        {
            setActiveContent(true);
            renderContent(response);
            callback(contentElement);
        });
    }

    function setActive(enabled) {
        rootElement.setAttribute("active", enabled ? "" : "no");
    }

    function setActiveContent(enabled) {
        contentWrapperElement.setAttribute("active", enabled ? "" : "no");
        setActiveLoader(false);
    }

    function setActiveLoader(enabled) {
        loaderElement.setAttribute("active", enabled ? "" : "no");
    }

    function renderContent(content)
    {
        if (!HTMLElement.prototype.isPrototypeOf(contentElement))
        {
            console.warn("Windowed popup have no content element. Content was not rendered");
            return;
        }

        contentElement.innerHTML = content;
    }

    function clearContent()
    {
        renderContent("");
    }

}).call(null, window.dashboard.popup);
window.dashboard = window.dashboard || {};
window.dashboard.models = window.dashboard.models || {};
window.dashboard.models.makeLoaderIconHTML = function (size)
{
    return '<svg width="' + size + 'px" height="' + size + 'px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-double-ring" style="background: none;"><circle cx="50" cy="50" ng-attr-r="{{config.radius}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-dasharray="{{config.dasharray}}" fill="none" stroke-linecap="round" r="40" stroke-width="8" stroke="#32a0da" stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(151.765 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1.7s" begin="0s" repeatCount="indefinite"></animateTransform></circle><circle cx="50" cy="50" ng-attr-r="{{config.radius2}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke="{{config.c2}}" ng-attr-stroke-dasharray="{{config.dasharray2}}" ng-attr-stroke-dashoffset="{{config.dashoffset2}}" fill="none" stroke-linecap="round" r="31" stroke-width="8" stroke="#72b4d7" stroke-dasharray="48.69468613064179 48.69468613064179" stroke-dashoffset="48.69468613064179" transform="rotate(-151.765 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;-360 50 50" keyTimes="0;1" dur="1.7s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>';
};

function ModelItemOptions()
{
    this.data = {};
    this.apiCatalog = undefined;
    this.parentElement = undefined;
    this.template = "";
    this.elementRefNames = [];
    this.dataRefAssociate = [];
    this.editFormUrl = "";
}

function BaseModelItem(options, updateFunction)
{
    updateFunction = utils.normalizeFunction(updateFunction);

    var that = this;

    var blocker = undefined;

    var references = {};

    this.remove = function ()
    {
        var element = references.root.element;
        if (!HTMLElement.prototype.isPrototypeOf(element) ||
            !HTMLElement.prototype.isPrototypeOf(element.parentElement)) return;
        element.parentElement.removeChild(element);
    };

    this.update = function ()
    {
        var elementNames = [];
        for (var p in options.dataRefAssociate)
            elementNames.push(p);

        var _tmpFormRefName = "",
            _tmpFormRefElement = undefined;

        for (var rfIndex = 0; rfIndex < elementNames.length; rfIndex++)
        {
            _tmpFormRefName = elementNames[rfIndex];
            _tmpFormRefElement = references[_tmpFormRefName];

            if (!RefElement.prototype.isPrototypeOf(_tmpFormRefElement) ||
                !HTMLElement.prototype.isPrototypeOf(_tmpFormRefElement.element))
                continue;

            _tmpFormRefElement.element.innerHTML = utils.getParameterByPath(options.data, options.dataRefAssociate[_tmpFormRefName]);
        }

        updateFunction(references);
    };

    this.forceUpdate = function ()
    {
        blocker.setActive(true);

        options.apiCatalog.get({ Id: options.data.id }, (response) =>
        {
            blocker.setActive(false);

            if (response["response"])
                options.data = response["response"];

            that.update();
        })
    };

    /// Init element
    (function ()
    {
        var template = document.createElement('div');
        template.innerHTML = options.template;

        /// Получаем рут элемент
        references.root = new RefElement("root", template.firstElementChild, -1);
        var root = references.root.element;
        
        /// Получаем и настраиваем блокер
        references.blocker = new RefElement("blocker", root.querySelector('[blocker]'), -1);
        var blockerElement = references.blocker.element;
        blocker = new Blocker(blockerElement);
        
        /// Получаем и записываем референсные элементы
        var allRefElements = root.querySelectorAll("[data-ref]");
        var _tmpRefName = "";
        for (var rIndex = 0; rIndex < allRefElements.length; rIndex++)
        {
            _tmpRefName = allRefElements[rIndex].getAttribute("data-ref");
            references[_tmpRefName] = new RefElement(_tmpRefName, allRefElements[rIndex], rIndex);
        }

        references.buttonDelete = new RefElement("button-delete", root.querySelector('[control="delete"]'), -1);
        references.buttonDelete.element.addEventListener("click", function ()
        {
            if (confirm("Вы уверены, что хотите удалить эту запись безвозвратно?"))
            {
                blocker.setActive(true);
                options.apiCatalog.delete({ Id: options.data.id },
                    () =>
                    {
                        blocker.setActive(false);
                        that.remove();
                    },
                    (err) =>
                    {
                        blocker.setActive(false);
                        console.error(err);
                    }
                );
            }
        });

        references.buttonEdit = new RefElement("button-edit", root.querySelector('[control="edit"]'), -1);
        references.buttonEdit.element.addEventListener("click", function ()
        {
            dashboard.popup.windowed.load(options.editFormUrl, null, (content) =>
            {
                var _elForm = content.getElementsByTagName("form")[0];
                var _formRefElements = [];

                /// Получаем и записываем референсные элементы из формы
                var formReferences = {};
                var _tmpFormRefName = "",
                    _tmpFormRefElement = undefined;

                var elementNames = [];
                for (var p in options.editRefAssociate)
                    elementNames.push(p);

                for (var rfIndex = 0; rfIndex < elementNames.length; rfIndex++)
                {
                    _tmpFormRefName = elementNames[rfIndex];
                    _tmpFormRefElement = formReferences[_tmpFormRefName] = new RefElement(_tmpFormRefName, _elForm.querySelector('[data-ref="' + _tmpFormRefName + '"]'), rfIndex);
                    _tmpFormRefElement.dataRefName = options.editRefAssociate[_tmpFormRefName];
                    _formRefElements.push(_tmpFormRefElement);

                    // Заполнение референсного элемента, если возможно
                    if (HTMLElement.prototype.isPrototypeOf(_tmpFormRefElement.element))
                    {
                        var value = utils.getParameterByPath(options.data, _tmpFormRefElement.dataRefName);
                        
                        if (_tmpFormRefElement.element.getAttribute("type") == "checkbox")
                            _tmpFormRefElement.element.checked = value;

                        if (_tmpFormRefElement.element.getAttribute("type") == "date")
                        {
                            var date = new Date(Date.parse(value));
                            value = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
                        }

                        _tmpFormRefElement.element.value = value;
                    }
                }
                
                var _elApplyButton = content.querySelector('[control="apply"]');
                _elApplyButton.addEventListener("click", function ()
                {
                    dashboard.popup.windowed.loader.setActive(true);
                });

                window.layouts.async.buildAutoApiHandler(
                    _elForm,
                    options.apiCatalog.edit,
                    () =>
                    {
                        _formRefElements.forEach((ref) =>
                        {
                            var value = ref.element.value;

                            if (ref.element.getAttribute("type") == "checkbox")
                            {
                                value = ref.element.checked;
                            }
                            options.data[ref.dataRefName] = value;
                        });

                        that.forceUpdate();
                        dashboard.popup.windowed.close();
                    },
                    () =>
                    {
                        console.log("error")
                        dashboard.popup.windowed.loader.setActive(false);
                    }
                );
            });
        });

        options.parentElement.appendChild(root);
        that.update();
    })();
}

/// ScheduleType
function ScheduleTypeItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Name: "name", Id: "id" };
    options.editFormUrl = "/dashboard/schedules/types/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';
    
    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name[0];
    });
}

/// Specialty
function SpecialtyItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { "full-name": "fullName", "code-name": "codeName", "specialty-code": "specialtyCode" };
    options.editRefAssociate = { Id: "id", FullName: "fullName", CodeName: "codeName", SpecialtyCode: "specialtyCode"  };
    options.editFormUrl = "/dashboard/specialties/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="code-name" class="collection--item--icon" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="full-name" class="cl-item-name"></div>'
        + '<div class="cl-item-desc">Код направления: <b data-ref="specialty-code"></b></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options);
}

/// Instructor
function InstructorItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { };
    options.editRefAssociate = { FirstName: "firstName", LastName: "lastName", MiddleName: "middleName", Id: "id" };
    options.editFormUrl = "/dashboard/instructors/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="full-name" class="cl-item-name"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';
    
    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.firstName[0] + data.middleName[0];
        refs["full-name"].element.innerHTML = (data.lastName + " " + data.firstName + " " + data.middleName).replace(/\b\w/g, l => l.toUpperCase());
    });
}

/// Discipline
function DisciplineItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name"  };
    options.editFormUrl = "/dashboard/disciplines/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name.split(' ').map(x => x[0]).join('').substr(0, 3).toUpperCase();
    });
}

/// Discipline method
function DisciplineMethodItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name" };
    options.editFormUrl = "/dashboard/disciplineMethods/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name.split(' ').map(x => x[0]).join('').substr(0, 3).toUpperCase();
    });
}

/// Education method
function EducationMethodItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name" };
    options.editFormUrl = "/dashboard/education/methods/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name.split(' ').map(x => x[0]).join('').substr(0, 3).toUpperCase();
    });
}

/// Qualification
function QualificationItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name" };
    options.editFormUrl = "/dashboard/education/qualifications/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name[0].toUpperCase();
    });
}

/// StudentGroup
function StudentGroupItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = {
        name: "name"
    };
    options.editRefAssociate = {
        Id: "id",
        Name: "name",
        OrganizationDate: "organizationDate",
        Expired: "expired",
        SpecialtyId: "specialty.id",
        QualificationId: "qualification.id",
        EducationMethodId: "educationMethod.id"
    };
    options.editFormUrl = "/dashboard/students/groups/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div class="cl-item-name cl-item-row-content">'
        + '<div data-ref="name"></div>'
        + '<div class="cl-item-marker-name cl-item-marker-blue" data-ref="education-method" ></div>'
        + '<div class="cl-item-marker-name cl-item-marker-blue" data-ref="qualification" ></div>'
        + '<div class="cl-item-marker-name cl-item-marker-red" data-ref="closed">Группа закрыта</div>'
        + '</div>'
        + '<div class="cl-item-desc">'
        + '<span data-ref="desc"></span>'
        + '</div > '
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["closed"].element.setAttribute("active", data.expired);
        //refs["name"].element.innerHTML = data.name + " / " + data.educationMethod.name + " / " + data.qualification.name
        refs["tag-name"].element.innerHTML = data.specialty.codeName;
        refs["education-method"].element.innerHTML = data.educationMethod.name;
        refs["qualification"].element.innerHTML = data.qualification.name;
        refs["desc"].element.innerHTML = data.specialty.specialtyCode + " / " + data.specialty.fullName
            + " / " + (new Date(Date.parse(data.organizationDate))).toLocaleDateString();
    });
}

///
function SchedulePreviewItem(data, apiCatalog, parent, template)
{
    var that = this;
    var editScheduleUrl = "/dashboard/schedules/editor"
    var rootElement = undefined;
    var blocker = undefined;

    var references = {};
    var controls = {};

    this.remove = function ()
    {
        if (!HTMLElement.prototype.isPrototypeOf(rootElement) ||
            !HTMLElement.prototype.isPrototypeOf(rootElement.parentElement)) return;
        rootElement.parentElement.removeChild(rootElement);
    };

    this.update = function ()
    {
        console.log(data);
        references["group-name"].setText(data.studentGroup.name);
        references["single-group-name"].setText(data.studentGroup.name);
        references["sheet-count"].setText(data.sheets.length);
        references["schedule-type-name"].setText(data.scheduleType.name);
        references["name"].setText("Расписание группы «" + data.studentGroup.name + "»");
        references["education-method-name"].setText(data.studentGroup.educationMethod.name);
        references["qualification-name"].setText(data.studentGroup.qualification.name);
    };

    (function ()
    {
        var templateElement = document.createElement('div');
        templateElement.innerHTML = utils.decodeHtml(template);

        /// Получаем рут элемент
        var root = rootElement = templateElement.firstElementChild;

        references = new ReferenceElementsBuilder("data-ref").build(root);
        controls = new ReferenceElementsBuilder("control").build(root);

        blocker = new Blocker(root.querySelector('[blocker]'));

        setupControls();

        parent.appendChild(root);
        that.update();
    })();

    function setupControls()
    {
        controls["delete"].element.addEventListener("click", () =>
        {
            if (confirm("Вы уверены, что хотите удалить расписание безвозвратно?"))
            {
                blocker.setActive(true);
                apiCatalog.delete({ Id: data.id },
                    () =>
                    {
                        blocker.setActive(false);
                        that.remove();
                    },
                    (err) =>
                    {
                        blocker.setActive(false);
                        console.error(err);
                    }
                );
            }
        });

        controls["edit"].element.addEventListener("click", () =>
        {
            try
            {
                blocker.setActive(true);

                var requestUrl = editScheduleUrl + "?ScheduleId=" + data.id;
                window.navigation.goAsync('Редактирование расписания', editScheduleUrl + "?ScheduleId=" + data.id, app.pageRendererContainerRoute.getElement());
            }
            catch (ex)
            {
                blocker.setActive(false);
            }
        });
    }
}
dashboard.controllers = dashboard.controllers || {};

dashboard.controllers.builder = dashboard.controllers.builder || {};

dashboard.controllers.builder.buildController = function (controllerName, collectionRootElement, itemClass, apiCatalog)
{
    var options = new DashboardControllerOptions();
    options.rootElement = collectionRootElement;
    options.apiCatalog = apiCatalog;
    options.itemClass = itemClass;

    return dashboard.controllers[controllerName] = new DashboardController(options);
};

/// Specialties controller
dashboard.controllers.builder.buildSpecialtiesController = function (collectionRootElement)
{
    return dashboard.controllers.builder.buildController("specialties", collectionRootElement, SpecialtyItem, window.api.catalog.specialties);
}

/// Instructors controller
dashboard.controllers.builder.buildInstructorsController = function (collectionRootElement)
{
    return dashboard.controllers.builder.buildController("instructors", collectionRootElement, InstructorItem, window.api.catalog.instructors);
}

/// Disciplines controller
dashboard.controllers.builder.buildDisciplinesController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("disciplines", collectionRootElement, DisciplineItem, window.api.catalog.disciplines);
}

/// Discipline methods controller
dashboard.controllers.builder.buildDisciplineMethodsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("disciplineMethods", collectionRootElement, DisciplineMethodItem, window.api.catalog.disciplineMethods);
}

/// Schedule types controller
dashboard.controllers.builder.buildScheduleTypesController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("scheduleTypes", collectionRootElement, ScheduleTypeItem, window.api.catalog.scheduleTypes);
}

/// Education methods controller
dashboard.controllers.builder.buildEducationMethodsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("educationMethods", collectionRootElement, EducationMethodItem, window.api.catalog.educationMethods);
}

/// Qualifications controller
dashboard.controllers.builder.buildQualificationsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("qualifications", collectionRootElement, QualificationItem, window.api.catalog.qualifications);
}

/// Student groups controller
dashboard.controllers.builder.buildStudentGroupsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("studentGroups", collectionRootElement, StudentGroupItem, window.api.catalog.studentGroups);
}

/// Schedules preview controller
dashboard.controllers.builder.buildSchedulesPreviewController = function (rootElement, itemTemplate) 
{
    var options = new DashboardControllerOptions();
    options.rootElement = rootElement;
    options.apiCatalog = window.api.catalog.schedules;
    options.itemClass = SchedulePreviewItem;
    options.itemTemplate = itemTemplate;

    return dashboard.controllers.schedulesPreview = new DashboardSchedulePreviewController(options);
}

function DashboardControllerOptions()
{
    this.rootElement = undefined;
    this.apiCatalog = undefined;
    this.itemClass = undefined;
}

function DashboardController(options) {
    var that = this;
    var loadedItemsData = [];
    var renderedItems = [];

    /// Container for drawing items
    this.collectionContainer = options.rootElement.querySelector(".collection-drawer");

    /// Blocker element
    this.blocker = new Blocker(options.rootElement.querySelector("[blocker]"));

    this.requestItemsData = function (callback) {
        callback = utils.normalizeFunction(callback);
        options.apiCatalog.get({},
            (response) =>
            {
                if (response['response'])
                    that.setRawItemsData(response.response);

                console.log(response);
                callback(response);
            });
    };

    this.forceUpdate = function (callback) {
        that.blocker.setActive(true);
        that.requestItemsData((res) => {
            that.redrawCollectionItems();
            that.blocker.setActive(false);
        });
    };

    this.setRawItemsData = function (itemsData) {
        loadedItemsData = [];
        
        for (var index = 0; index < itemsData.length; index++)
            loadedItemsData.push(itemsData[index]);
    };

    this.removeItems = function () {
        for (var index = 0; index < renderedItems.length; index++)
            renderedItems[index].remove();

        renderedItems = [];
    };

    this.redrawCollectionItems = function () {
        that.removeItems();

        for (var index = 0; index < loadedItemsData.length; index++) {
            var item = new options.itemClass(loadedItemsData[index], options.apiCatalog, that.collectionContainer);
            renderedItems.push(item);
        }
    };

    this.getLoadedData = function ()
    {
        return loadedItemsData;
    };

    this._addRenderedItem = function (item)
    {
        renderedItems.push(item);
    };
}

function DashboardSchedulePreviewController(options)
{
    DashboardController.call(this, options);

    var that = this;
    var activeCategory = new CategoryData("preview-active-box"),
        inactiveCategory = new CategoryData("preview-inactive-box");

    var _removeItems = this.removeItems;
    this.removeItems = function ()
    {
        _removeItems();
        activeCategory.resetCounter();
        inactiveCategory.resetCounter();
    };

    this.redrawCollectionItems = function ()
    {
        that.removeItems();

        var loadedItemsData = that.getLoadedData();
        for (var index = 0; index < loadedItemsData.length; index++)
        {
            var category = loadedItemsData[index].publicationStatus == 0 ? inactiveCategory : activeCategory;
            category.incrementCounter();

            var item = new options.itemClass(loadedItemsData[index], options.apiCatalog, category.collectionElement, options.itemTemplate);
            this._addRenderedItem(item);
        }
    };

    function CategoryData(dataRefName)
    {
        var that = this;
        var counterValue = 0;

        this.rootElement = options.rootElement.querySelector('[data-ref="' + dataRefName + '"]');
        this.counterElement = this.rootElement.querySelector('[data-ref="items-counter"]');
        this.collectionElement = this.rootElement.querySelector('[data-ref="items-container"]');

        this.incrementCounter = function ()
        {
            counterValue++;
            redrawCounter();
        };

        this.resetCounter = function ()
        {
            counterValue = 0;
            redrawCounter();
        };

        function redrawCounter()
        {
            that.counterElement.innerHTML = counterValue;
        }
    }
}
function ScheduleEditorSetupOptions(sheduleId, rootElement)
{
    this.scheduleId = sheduleId;
    this.rootElement = rootElement;

    this.sheetItemTemplate = "";
    this.recordListTemplate = "";
    this.recordItemTemplate = "";
}

window.dashboard.scheduleEditor = (function ()
{
    var creatorRequestMode = {
        create: "create",
        edit: "edit"
    };
    return new ScheduleEditor();

    function ScheduleEditor()
    {
        var scheduleId = "";
        var that = this;
        var scheduleShortData;
        var schedulePublicationState = 0;

        var scheduleSheetsManager,
            scheduleRecordsManager;

        var elementReferences = {};
        var controlReferences = {};

        var publicateButton,
            publicationState;

        var blocker;

        this.setup = function (options)
        {
            removeAllManagersEventListeners();
            removeAllManagers();

            scheduleId = options.scheduleId;
            publicationState = options.rootElement.querySelector("[data-ref='schedule-publication-state']");
            publicateButton = options.rootElement.querySelector("[control='schedule-publication']");
            publicateButton.addEventListener("click", function ()
            {
                if (scheduleShortData == undefined || scheduleShortData == null) return;
                that.setPublicationStateCode(schedulePublicationState == 0 ? 1 : 0);
            });

            var blockerElements = options.rootElement.querySelectorAll("[blocker]");
            blocker = new Blocker(blockerElements[blockerElements.length - 1]);
            
            /// Создаем менеджер листов
            scheduleSheetsManager = new ScheduleSheetManager(
                options.scheduleId,
                window.api.catalog.scheduleSheets,
                options.rootElement.querySelector("[data-ref='schedule-sheets-manager']"),
                options.sheetItemTemplate,
                blocker);

            /// Создаем менеджер записей
            scheduleRecordsManager = new ScheduleRecordManager(
                options.scheduleId,
                options.rootElement.querySelector("[data-ref='schedule-records-manager']"),
                options.recordListTemplate,
                options.recordItemTemplate,
                blocker);

            addAllManagersEventListeners();

            that.updateScheduleDetails();
        }

        this.update = function ()
        {
            scheduleSheetsManager.forceUpdate();
        };

        this.updateScheduleDetails = function ()
        {
            api.catalog.schedules.get({ Id: scheduleId }, (response) =>
            {
                if (response["response"])
                {
                    scheduleShortData = response["response"];
                    schedulePublicationState = scheduleShortData.publicationStatus;
                    updatePublicationStateContent(schedulePublicationState);
                }
                else
                {
                    console.error(response);
                }
            });
        };

        this.setPublicationStateCode = function (state)
        {
            blocker.setActive(true);
            api.catalog.schedules.publication({ ScheduleId: scheduleId, State: state }, (response) =>
            {
                blocker.setActive(false);
                if (response["response"])
                {
                    schedulePublicationState = response["response"].publicationStatus;
                    updatePublicationStateContent(schedulePublicationState);
                }
                else
                {
                    console.error(response);
                }
            });
        };

        function updatePublicationStateContent(status)
        {
            publicateButton.innerHTML = status ? "Убрать публикацию" : "Опубликовать";
            if (status) publicateButton.classList.add("published");
            else publicateButton.classList.remove("published");

            publicationState.innerHTML = status ? "Расписание опубликовано" : "Расписание не опубликовано";
        }

        function onSheetSelected(sheet)
        {
            scheduleRecordsManager.setSheet(sheet);
        }

        function removeAllManagers()
        {
            scheduleSheetsManager = null;
            scheduleRecordsManager = null;
        }

        function addAllManagersEventListeners()
        {
            addSheetManagerEventListeners();
        }

        function removeAllManagersEventListeners()
        {
            removeSheetManagerEventListeners();
        }

        function addSheetManagerEventListeners()
        {
            if (scheduleSheetsManager == null) return;
            scheduleSheetsManager.onSheetSelectionChanged.addEventListener(onSheetSelected);
        }

        function removeSheetManagerEventListeners()
        {
            if (scheduleSheetsManager == null) return;
            scheduleSheetsManager.onSheetSelectionChanged.removeEventListener(onSheetSelected);
        }
    }

    function ScheduleSheetManager(scheduleId, apiCatalog, rootElement, itemTemplate, rootBlocker)
    {
        var that = this;
        var elementsReferences = new ReferenceElementsBuilder("data-ref").build(rootElement);
        var controlsReferences = new ReferenceElementsBuilder("control").build(rootElement);
        
        var listElement = elementsReferences["schedule-sheets-list"].element,
            addSheetButton = controlsReferences["add"].element;

        var blocker = new Blocker(rootElement.querySelector("[blocker]"));

        var items = [];
        var loadedData = [];
        
        var selectedSheet = undefined;

        this.getAllSheets = function ()
        {
            return loadedData;
        };

        this.onSheetSelectionChanged = new EventHandler();

        this.selectSheetByIndex = function (index)
        {
            try
            {
                setSelectedSheet(loadedData[index]);
            }
            catch (err)
            {
                console.error(err);
            }
        };

        this.selectSheetById = function (id)
        {
            try
            {
                for (var i = 0; i < loadedData.length; i++)
                {
                    if (loadedData[i].id == id)
                    {
                        setSelectedSheet(loadedData[i]);
                        return;
                    }
                }
            }
            catch (err)
            {
                console.error(err);
            }
        };

        this.getSelectedSheet = function ()
        {
            return selectedSheet;
        };

        this.removeItems = function ()
        {
            for (var i = 0; i < items.length; i++)
                items[i].remove();
            items = [];
        };

        this.forceUpdate = function (callback)
        {
            callback = utils.normalizeFunction(callback);

            blocker.setActive(true);
            rootBlocker.setActive(true);
            apiCatalog.getLinked({ ScheduleId: scheduleId }, (response) =>
            {
                if (response["response"])
                {
                    loadedData = response["response"];
                    updateLoadedData();
                    
                    that.update();
                    setSelectedSheet(loadedData[0]);
                }
                else
                {
                    console.error(response);
                }

                blocker.setActive(false);
                rootBlocker.setActive(false);
                callback();
            });
        };

        this.update = function ()
        {
            var oldLoadedData = [].slice.call(loadedData);

            that.removeItems();

            loadedData = oldLoadedData;
            updateLoadedData();

            if (loadedData.length <= 0) return;

            var maxWeekdayIndex = loadedData.reduce((max, p) => p.weekPriorityIndex > max ? p.weekPriorityIndex : max, loadedData[0].weekPriorityIndex);
            for (var i = 0; i < loadedData.length; i++)
            {
                var item = new ScheduleSheetItem(loadedData[i], that, maxWeekdayIndex, apiCatalog, listElement, itemTemplate);
                item.onRemove.addEventListener(onItemRemoved);
                item.onSelect.addEventListener(onItemSelected);

                items.push(item);
            }
        };

        function onSelectedSheetChanged()
        {
            var item;
            for (var i = 0; i < items.length; i++)
            {
                item = items[i];
                if (item == null) continue;

                item.setSelectedStatus(item.getData() == selectedSheet);
            }

            that.onSheetSelectionChanged.invoke(selectedSheet);
        }

        function setSelectedSheet(data)
        {
            if (data == null) return;

            selectedSheet = data;
            onSelectedSheetChanged();
        }

        function onItemSelected(item)
        {
            setSelectedSheet(item.getData());
        }

        function onItemRemoved(item)
        {
            item.onRemove.removeEventListener(onItemRemoved);
            item.onSelect.removeEventListener(onItemSelected);

            var data = item.getData();
            removeDataFromLoadedData(data);
            loadedData = computeWeekPriorityIndexes(loadedData);
            
            updateAllItems();

            if (selectedSheet == data)
                that.selectSheetByIndex(0);
        }

        function removeDataFromLoadedData(data)
        {
            if (data == null) return;

            var index = loadedData.indexOf(data);
            if (index < 0) return;

            loadedData.splice(index, 1);
        }

        function computeWeekPriorityIndexes(dataList)
        {
            var sorted = [].slice.call(dataList).sort(function (a, b) { return a.weekPriority > b.weekPriority; })
            for (var i = 0; i < sorted.length; i++)
            {
                sorted[i].weekPriorityIndex = i;
            }

            return sorted;
        }

        function updateAllItems()
        {
            for (var i = 0; i < items.length; i++)
                items[i].update();
        }

        function updateLoadedData()
        {
            loadedData = [].slice.call(loadedData).sort(function (a, b) { return a.weekPriority > b.weekPriority; });
            loadedData = computeWeekPriorityIndexes(loadedData);
        }

        /// Constructor
        (function ()
        {
            addSheetButton.addEventListener("click", () =>
            {
                apiCatalog.add({ ScheduleId: scheduleId }, (response) =>
                {
                    if (response["response"])
                    {
                        that.forceUpdate();
                    }
                    else
                    {
                        console.error(response);
                    }
                });
            });
        })();
    }

    function ScheduleSheetItem(data, sheetManager, maxWeekdayIndex, apiCatalog,  parent, template)
    {
        var that = this;
        var selectedClassName = "sheet-item-selected";

        var rootElement = undefined;
        var blocker;

        var elementReferences = {};
        var controlReferences = {};

        var deleteButton = undefined; 
        var moveDownButton,
            moveUpButton;

        this.onSelect = new EventHandler();
        this.onRemove = new EventHandler();

        this.getData = function ()
        {
            return data;
        };

        this.setSelectedStatus = function (selected)
        {
            if (selected)
            {
                rootElement.classList.add(selectedClassName);
            }
            else
            {
                rootElement.classList.remove(selectedClassName);
            }
        };

        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["name"].setText((data.weekPriorityIndex + 1) + " Неделя");
            moveDownButton.setAttribute("active", data.weekPriorityIndex == 0 ? "no" : "");
            moveUpButton.setAttribute("active", data.weekPriorityIndex == maxWeekdayIndex ? "no" : "");
        };

        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            blocker = new Blocker(rootElement.querySelector("[blocker]"));

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);

            root.addEventListener("click", () => { that.onSelect.invoke(that); });

            moveDownButton = controlReferences["move-down"].element;
            moveDownButton.addEventListener("click", (event) =>
            {
                sendApiMoveRequest(-1);
                event.stopPropagation();
            }, true);

            moveUpButton = controlReferences["move-up"].element;
            moveUpButton.addEventListener("click", (event) =>
            {
                sendApiMoveRequest(1);
                event.stopPropagation();
            }, true);

            deleteButton = controlReferences["delete"].element;
            deleteButton.addEventListener("click", (event) =>
            {
                event.stopPropagation()

                if (!confirm("Вы уверены, что хотите удалить таблицу расписания безвозвратно? Также вместе с таблицой будут удалены все ее записи"))
                    return;

                blocker.setActive(true);
                apiCatalog.delete({ Id: data.id }, function (response)
                {
                    if (response["response"])
                    {
                        that.remove();
                    }
                    else
                    {
                        console.error(response);
                    }
                    blocker.setActive(false);
                });
            }, true);

            parent.appendChild(root);
            that.update();
        })();

        function sendApiMoveRequest(direction)
        {
            blocker.setActive(true);
            apiCatalog.move({ ScheduleSheetId: data.id, Direction: direction }, function (response)
            {
                if (response["response"])
                {
                    replaceSheetWeekdayPriority(response["response"]);
                    sheetManager.forceUpdate(() =>
                    {
                        sheetManager.selectSheetById(data.id);
                    });
                }
                else
                {
                    console.error(response);
                }
                blocker.setActive(false);
            });
        }

        function replaceSheetWeekdayPriority(newSheet)
        {
            var sheets = sheetManager.getAllSheets();

            for (var i = 0; i < sheets.length; i++)
            {
                if (sheets[i].id == newSheet.id)
                {
                    sheets[i].weekPriority = newSheet.weekPriority;
                    return;
                }
            }
        }
    }

    function ScheduleRecordManager(scheduleId, rootElement, listTemplate, itemTemplate, rootBlocker)
    {
        var that = this;
        var elementsReferences = new ReferenceElementsBuilder("data-ref").build(rootElement);
        var controlsReferences = new ReferenceElementsBuilder("control").build(rootElement);
        var scrollbar = rootElement;

        var listElement = rootElement;
        var columns = [];

        var blocker = new Blocker(rootElement.querySelector("[blocker]"));
        var currentSheet = undefined;

        this.setSheet = function (sheet)
        {
            if (currentSheet == sheet) return;
            currentSheet = sheet;

            redrawAll();
        };

        (function ()
        {
            /// Build columens
            for (var i = 0; i < daysOfTheWeek.length; i++)
                columns.push(new ScheduleRecordList(i, listElement, listTemplate, itemTemplate));
        })();

        function redrawAll()
        {
            updateColumns();
            scrollbar.scrollLeft = scrollbar.scrollTop = 0;
        }

        function updateColumns()
        {
            for (var i = 0; i < columns.length; i++)
            {
                columns[i].removeAllRecords();
                columns[i].setSheet(currentSheet);
                columns[i].reset();
            }
        }
    }

    function ScheduleRecordList(weekdayIndex, parent, template, itemTemplate)
    {
        var apiCatalog = window.api.catalog.scheduleRecords;
        var that = this;
        var selectedClassName = "sheet-item-selected";

        var recordItems = [];
        var targetSheet = undefined;

        var createEditRequestRecordTarget = undefined;
        var creatorApplyAction = creator_createAction;
        var targetCreatorRequestMode = creatorRequestMode.create;
        var rootElement,
            creatorElement,
            collectionElement,
            scrollbar;

        var elementReferences = {};
        var controlReferences = {};

        var blocker;
    
        this.reset = function ()
        {
            setCreatorActive(false);
        };

        this.getSheet = function ()
        {
            return targetSheet;
        };

        this.setSheet = function (sheet)
        {
            if (sheet == null || sheet == undefined || sheet == targetSheet) return;

            targetSheet = sheet;
            that.update();
        };

        this.onRemove = new EventHandler();
        
        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["weekday"].setText(daysOfTheWeek[weekdayIndex]);

            that.removeAllRecords();

            if (targetSheet != undefined && targetSheet != null)
                that.addRecordRange(targetSheet.records.filter(function (record) { return record.day === weekdayIndex; }));

            updateRecordPositions();
            scrollbar.scrollLeft = 0;
            scrollbar.scrollTop = 0;
        };

        this.addRecord = function (record)
        {
            recordItems.push(new ScheduleRecordItem(that, record, apiCatalog, collectionElement, itemTemplate));
        };

        this.addRecordRange = function (records)
        {
            for (var i = 0; i < records.length; i++)
                that.addRecord(records[i]);
        };

        this.removeAllRecords = function ()
        {
            for (var i = 0; i < recordItems.length; i++)
                recordItems[i].remove();
            recordItems = [];
        };

        this.requestEdit = function (record)
        {
            createEditRequestRecordTarget = record;
            targetCreatorRequestMode = creatorRequestMode.edit;

            prepareCreator();
            setCreatorActive(true);
        };

        this.requestCreate = function ()
        {
            targetCreatorRequestMode = creatorRequestMode.create;

            prepareCreator();
            setCreatorActive(true);
        };

        function updateRecordPositions()
        {
            if (recordItems.length == 0) return;
            var sortedRecords = recordItems.sort((a, b) =>
            {
                aData = a.getData();
                bData = b.getData();
                return aData.startTime != bData.startTime ? aData.startTime > bData.startTime : aData.duration > bData.duration;
            });
            sortedRecords.forEach((item) => { item.moveToEndHierarchy(); });
        }

        function prepareCreator()
        {
            if (targetCreatorRequestMode == creatorRequestMode.create)
            {
                elementReferences["creator-title"].setText("Новая запись");
                controlReferences["creator-apply"].setText("Добавить");
                creatorApplyAction = creator_createAction;
                utils.clearFormParameters(creatorElement);
            }
            else if (targetCreatorRequestMode == creatorRequestMode.edit && createEditRequestRecordTarget != undefined)
            {
                elementReferences["creator-title"].setText("Редактирование записи");
                controlReferences["creator-apply"].setText("Сохранить");
                creatorApplyAction = creator_editAction;
                putCreatorParameters(createEditRequestRecordTarget);
            }
        }

        function setCreatorActive(enabled)
        {
            creatorElement.setAttribute("active", enabled ? "" : "no");
        }

        function hasSheet()
        {
            return !(targetSheet == null || targetSheet == undefined);
        }

        function creator_createAction(parameters, callback)
        {
            apiCatalog.add(parameters, (response) =>
            {
                if (response["response"])
                {
                    if (!hasSheet())
                    {
                        console.error("Sheet is not defined");
                    }
                    else
                    {
                        console.log("Record added", response["response"]);
                        targetSheet.records.push(response["response"]);

                        that.update();
                    }
                }
                else
                {
                    console.error(response);
                }
                callback(response);
                setCreatorActive(false);
            });
        }

        function creator_editAction(parameters, callback)
        {
            parameters.ScheduleRecordId = createEditRequestRecordTarget.id;
            apiCatalog.edit(parameters, (response) =>
            {
                if (response["response"])
                {
                    if (!hasSheet())
                    {
                        console.error("Sheet is not defined");
                    }
                    else
                    {
                        console.log("Record edited", response["response"]);

                        replaceRecord(response["response"]);
                        that.update();
                    }
                }
                else
                {
                    console.error(response);
                }
                callback(response);
                setCreatorActive(false);
            });
        }

        function replaceRecord(newRecord)
        {
            var sheet = that.getSheet();
            for (var i = 0; i < sheet.records.length; i++)
            {
                if (sheet.records[i].id == newRecord.id)
                {
                    sheet.records[i] = newRecord;
                    return;
                }
            }
        }

        function putCreatorParameters(record)
        {
            elementReferences["InstructorId"].element.value = record.instructor.id;
            elementReferences["DisciplineId"].element.value = record.discipline.id;
            elementReferences["DisciplineMethodId"].element.value = record.disciplineMethod.id;
            elementReferences["Location"].element.value = record.location;
            elementReferences["StartTime"].element.value = getTimeStringFromTimeSpan(record.startTime);
            elementReferences["EndTime"].element.value = getTimeStringFromTimeSpan(record.startTime + record.duration);
            console.log(elementReferences);
        }

        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            scrollbar = root.querySelector(".scrollbar");

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);
            
            collectionElement = elementReferences["collection"].element;

            creatorElement = elementReferences["entity-creator"].element;
            setCreatorActive(false);

            blocker = new Blocker(creatorElement.querySelector("[blocker]"));

            controlReferences["add"].element.addEventListener("click", (event) =>
            {
                that.requestCreate();
            });

            controlReferences["creator-close"].element.addEventListener("click", (event) =>
            {
                setCreatorActive(false);
            });

            controlReferences["creator-apply"].element.addEventListener("click", (event) =>
            {
                if (!hasSheet())
                {
                    console.error("Sheet is not defined");
                    return;
                }

                if (!utils.validateForm(creatorElement))
                {
                    alert("Заполните все поля");
                    return;
                }

                var parameters = utils.getFormParameters(creatorElement);
                parameters.ScheduleSheetId = targetSheet.id;
                parameters.Day = weekdayIndex;
                parameters.Duration = parameters.EndTime - parameters.StartTime;
                delete parameters.EndTime;

                if (parameters.Duration <= 0)
                {
                    alert("Время введено неверно");
                    return;
                }

                blocker.setActive(true);
                creatorApplyAction(parameters, () => { blocker.setActive(false); });
            });

            parent.appendChild(root);
            utils.addSimpleTouchScroller(scrollbar);

            that.update();
        })();
    }

    function ScheduleRecordItem(recordList, recordData, apiCatalog, parent, template)
    {
        var that = this;

        var rootElement = undefined;

        var elementReferences = {};
        var controlReferences = {};

        var deleteButton,
            editButton;
        var blocker;

        this.onRemove = new EventHandler();

        this.getData = function ()
        {
            return recordData;
        };

        this.moveToEndHierarchy = function ()
        {
            rootElement.parentElement.insertBefore(rootElement, null);
        };

        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["time"].setText(getTimeStringFromTimeSpan(recordData.startTime) + " - " + getTimeStringFromTimeSpan(recordData.startTime + recordData.duration));
            elementReferences["discipline-method"].setText(recordData.disciplineMethod.name);
            elementReferences["discipline"].setText(recordData.discipline.name);
            elementReferences["instructor"].setText(recordData.instructor.fullName);
            elementReferences["duration"].setText(recordData.duration + " минут");
            elementReferences["location"].setText(recordData.location);
        };

        function removeRecordFromSheet(sheet, record)
        {
            if (sheet == undefined || sheet == null || !Array.isArray(sheet.records)) return;

            var index = sheet.records.indexOf(record);
            if (index < 0) return;

            sheet.records.splice(index, 1);
        }

        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            blocker = new Blocker(rootElement.querySelector("[blocker]"));

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);
            
            deleteButton = controlReferences["delete"].element;
            deleteButton.addEventListener("click", (event) =>
            {
                event.stopPropagation();

                if (!confirm("Вы уверены, что хотите удалить запись?"))
                    return;

                var sheet = recordList.getSheet();

                blocker.setActive(true);
                apiCatalog.delete({ Id: recordData.id }, function (response)
                {
                    if (response["response"])
                    {
                        removeRecordFromSheet(sheet, recordData);
                        that.remove();
                    }
                    else
                    {
                        console.error(response);
                    }
                    blocker.setActive(false);
                });
            });

            editButton = controlReferences["edit"].element;
            editButton.addEventListener("click", (event) =>
            {
                recordList.requestEdit(recordData);
            });

            root.recordItem = that;

            parent.appendChild(root);
            that.update();
        })();
    }

    function getTimeStringFromTimeSpan(timeSpan)
    {
        var hours = ("0" + Math.floor(timeSpan / 60)).slice(-2);
        var minutes = ("0" + timeSpan % 60).slice(-2);
        return hours + ":" + minutes;
    }
})();