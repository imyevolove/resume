﻿window.dashboard = window.dashboard || {};
window.dashboard.models = window.dashboard.models || {};
window.dashboard.models.makeLoaderIconHTML = function (size)
{
    return '<svg width="' + size + 'px" height="' + size + 'px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-double-ring" style="background: none;"><circle cx="50" cy="50" ng-attr-r="{{config.radius}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-dasharray="{{config.dasharray}}" fill="none" stroke-linecap="round" r="40" stroke-width="8" stroke="#32a0da" stroke-dasharray="62.83185307179586 62.83185307179586" transform="rotate(151.765 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1.7s" begin="0s" repeatCount="indefinite"></animateTransform></circle><circle cx="50" cy="50" ng-attr-r="{{config.radius2}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke="{{config.c2}}" ng-attr-stroke-dasharray="{{config.dasharray2}}" ng-attr-stroke-dashoffset="{{config.dashoffset2}}" fill="none" stroke-linecap="round" r="31" stroke-width="8" stroke="#72b4d7" stroke-dasharray="48.69468613064179 48.69468613064179" stroke-dashoffset="48.69468613064179" transform="rotate(-151.765 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;-360 50 50" keyTimes="0;1" dur="1.7s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>';
};

function ModelItemOptions()
{
    this.data = {};
    this.apiCatalog = undefined;
    this.parentElement = undefined;
    this.template = "";
    this.elementRefNames = [];
    this.dataRefAssociate = [];
    this.editFormUrl = "";
}

function BaseModelItem(options, updateFunction)
{
    updateFunction = utils.normalizeFunction(updateFunction);

    var that = this;

    var blocker = undefined;

    var references = {};

    this.remove = function ()
    {
        var element = references.root.element;
        if (!HTMLElement.prototype.isPrototypeOf(element) ||
            !HTMLElement.prototype.isPrototypeOf(element.parentElement)) return;
        element.parentElement.removeChild(element);
    };

    this.update = function ()
    {
        var elementNames = [];
        for (var p in options.dataRefAssociate)
            elementNames.push(p);

        var _tmpFormRefName = "",
            _tmpFormRefElement = undefined;

        for (var rfIndex = 0; rfIndex < elementNames.length; rfIndex++)
        {
            _tmpFormRefName = elementNames[rfIndex];
            _tmpFormRefElement = references[_tmpFormRefName];

            if (!RefElement.prototype.isPrototypeOf(_tmpFormRefElement) ||
                !HTMLElement.prototype.isPrototypeOf(_tmpFormRefElement.element))
                continue;

            _tmpFormRefElement.element.innerHTML = utils.getParameterByPath(options.data, options.dataRefAssociate[_tmpFormRefName]);
        }

        updateFunction(references);
    };

    this.forceUpdate = function ()
    {
        blocker.setActive(true);

        options.apiCatalog.get({ Id: options.data.id }, (response) =>
        {
            blocker.setActive(false);

            if (response["response"])
                options.data = response["response"];

            that.update();
        })
    };

    /// Init element
    (function ()
    {
        var template = document.createElement('div');
        template.innerHTML = options.template;

        /// Получаем рут элемент
        references.root = new RefElement("root", template.firstElementChild, -1);
        var root = references.root.element;
        
        /// Получаем и настраиваем блокер
        references.blocker = new RefElement("blocker", root.querySelector('[blocker]'), -1);
        var blockerElement = references.blocker.element;
        blocker = new Blocker(blockerElement);
        
        /// Получаем и записываем референсные элементы
        var allRefElements = root.querySelectorAll("[data-ref]");
        var _tmpRefName = "";
        for (var rIndex = 0; rIndex < allRefElements.length; rIndex++)
        {
            _tmpRefName = allRefElements[rIndex].getAttribute("data-ref");
            references[_tmpRefName] = new RefElement(_tmpRefName, allRefElements[rIndex], rIndex);
        }

        references.buttonDelete = new RefElement("button-delete", root.querySelector('[control="delete"]'), -1);
        references.buttonDelete.element.addEventListener("click", function ()
        {
            if (confirm("Вы уверены, что хотите удалить эту запись безвозвратно?"))
            {
                blocker.setActive(true);
                options.apiCatalog.delete({ Id: options.data.id },
                    () =>
                    {
                        blocker.setActive(false);
                        that.remove();
                    },
                    (err) =>
                    {
                        blocker.setActive(false);
                        console.error(err);
                    }
                );
            }
        });

        references.buttonEdit = new RefElement("button-edit", root.querySelector('[control="edit"]'), -1);
        references.buttonEdit.element.addEventListener("click", function ()
        {
            dashboard.popup.windowed.load(options.editFormUrl, null, (content) =>
            {
                var _elForm = content.getElementsByTagName("form")[0];
                var _formRefElements = [];

                /// Получаем и записываем референсные элементы из формы
                var formReferences = {};
                var _tmpFormRefName = "",
                    _tmpFormRefElement = undefined;

                var elementNames = [];
                for (var p in options.editRefAssociate)
                    elementNames.push(p);

                for (var rfIndex = 0; rfIndex < elementNames.length; rfIndex++)
                {
                    _tmpFormRefName = elementNames[rfIndex];
                    _tmpFormRefElement = formReferences[_tmpFormRefName] = new RefElement(_tmpFormRefName, _elForm.querySelector('[data-ref="' + _tmpFormRefName + '"]'), rfIndex);
                    _tmpFormRefElement.dataRefName = options.editRefAssociate[_tmpFormRefName];
                    _formRefElements.push(_tmpFormRefElement);

                    // Заполнение референсного элемента, если возможно
                    if (HTMLElement.prototype.isPrototypeOf(_tmpFormRefElement.element))
                    {
                        var value = utils.getParameterByPath(options.data, _tmpFormRefElement.dataRefName);
                        
                        if (_tmpFormRefElement.element.getAttribute("type") == "checkbox")
                            _tmpFormRefElement.element.checked = value;

                        if (_tmpFormRefElement.element.getAttribute("type") == "date")
                        {
                            var date = new Date(Date.parse(value));
                            value = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
                        }

                        _tmpFormRefElement.element.value = value;
                    }
                }
                
                var _elApplyButton = content.querySelector('[control="apply"]');
                _elApplyButton.addEventListener("click", function ()
                {
                    dashboard.popup.windowed.loader.setActive(true);
                });

                window.layouts.async.buildAutoApiHandler(
                    _elForm,
                    options.apiCatalog.edit,
                    () =>
                    {
                        _formRefElements.forEach((ref) =>
                        {
                            var value = ref.element.value;

                            if (ref.element.getAttribute("type") == "checkbox")
                            {
                                value = ref.element.checked;
                            }
                            options.data[ref.dataRefName] = value;
                        });

                        that.forceUpdate();
                        dashboard.popup.windowed.close();
                    },
                    () =>
                    {
                        console.log("error")
                        dashboard.popup.windowed.loader.setActive(false);
                    }
                );
            });
        });

        options.parentElement.appendChild(root);
        that.update();
    })();
}

/// ScheduleType
function ScheduleTypeItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Name: "name", Id: "id" };
    options.editFormUrl = "/dashboard/schedules/types/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';
    
    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name[0];
    });
}

/// Specialty
function SpecialtyItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { "full-name": "fullName", "code-name": "codeName", "specialty-code": "specialtyCode" };
    options.editRefAssociate = { Id: "id", FullName: "fullName", CodeName: "codeName", SpecialtyCode: "specialtyCode"  };
    options.editFormUrl = "/dashboard/specialties/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="code-name" class="collection--item--icon" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="full-name" class="cl-item-name"></div>'
        + '<div class="cl-item-desc">Код направления: <b data-ref="specialty-code"></b></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options);
}

/// Instructor
function InstructorItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { };
    options.editRefAssociate = { FirstName: "firstName", LastName: "lastName", MiddleName: "middleName", Id: "id" };
    options.editFormUrl = "/dashboard/instructors/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="full-name" class="cl-item-name"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';
    
    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.firstName[0] + data.middleName[0];
        refs["full-name"].element.innerHTML = (data.lastName + " " + data.firstName + " " + data.middleName).replace(/\b\w/g, l => l.toUpperCase());
    });
}

/// Discipline
function DisciplineItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name"  };
    options.editFormUrl = "/dashboard/disciplines/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name.split(' ').map(x => x[0]).join('').substr(0, 3).toUpperCase();
    });
}

/// Discipline method
function DisciplineMethodItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name" };
    options.editFormUrl = "/dashboard/disciplineMethods/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name.split(' ').map(x => x[0]).join('').substr(0, 3).toUpperCase();
    });
}

/// Education method
function EducationMethodItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name" };
    options.editFormUrl = "/dashboard/education/methods/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name.split(' ').map(x => x[0]).join('').substr(0, 3).toUpperCase();
    });
}

/// Qualification
function QualificationItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = { name: "name" };
    options.editRefAssociate = { Id: "id", Name: "name" };
    options.editFormUrl = "/dashboard/education/qualifications/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div data-ref="name" class="cl-item-name"></div>'
        + '<div data-ref="desc" class="cl-item-desc"></div>'
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["tag-name"].element.innerHTML = data.name[0].toUpperCase();
    });
}

/// StudentGroup
function StudentGroupItem(data, apiCatalog, parent)
{
    var options = new ModelItemOptions();
    options.data = data;
    options.apiCatalog = apiCatalog;
    options.parentElement = parent;
    options.dataRefAssociate = {
        name: "name"
    };
    options.editRefAssociate = {
        Id: "id",
        Name: "name",
        OrganizationDate: "organizationDate",
        Expired: "expired",
        SpecialtyId: "specialty.id",
        QualificationId: "qualification.id",
        EducationMethodId: "educationMethod.id"
    };
    options.editFormUrl = "/dashboard/students/groups/forms.edit";
    options.template = '<div class="collection--item">'
        + '<div class="collection--item-wrapper">'
        + '<div data-ref="tag-name" class="collection--item--icon collection--ii-min" format="name"></div>'
        + '<div class="collection--item--content">'
        + '<div class="cl-item-name cl-item-row-content">'
        + '<div data-ref="name"></div>'
        + '<div class="cl-item-marker-name cl-item-marker-blue" data-ref="education-method" ></div>'
        + '<div class="cl-item-marker-name cl-item-marker-blue" data-ref="qualification" ></div>'
        + '<div class="cl-item-marker-name cl-item-marker-red" data-ref="closed">Группа закрыта</div>'
        + '</div>'
        + '<div class="cl-item-desc">'
        + '<span data-ref="desc"></span>'
        + '</div > '
        + '</div>'
        + '<div class="cl-item-controls">'
        + '<div control="edit" class="base-button button-action-outline material-icon">edit</div>'
        + '<div control="delete" class="base-button button-delete-outline material-icon">delete_forever</div>'
        + '</div></div>'
        + '<div class="async-operation-block" mode = "await" active = "no" blocker>'
        + window.dashboard.models.makeLoaderIconHTML(45)
        + '</div></div >';

    BaseModelItem.call(this, options, (refs) =>
    {
        data = options.data;
        refs["closed"].element.setAttribute("active", data.expired);
        //refs["name"].element.innerHTML = data.name + " / " + data.educationMethod.name + " / " + data.qualification.name
        refs["tag-name"].element.innerHTML = data.specialty.codeName;
        refs["education-method"].element.innerHTML = data.educationMethod.name;
        refs["qualification"].element.innerHTML = data.qualification.name;
        refs["desc"].element.innerHTML = data.specialty.specialtyCode + " / " + data.specialty.fullName
            + " / " + (new Date(Date.parse(data.organizationDate))).toLocaleDateString();
    });
}

///
function SchedulePreviewItem(data, apiCatalog, parent, template)
{
    var that = this;
    var editScheduleUrl = "/dashboard/schedules/editor"
    var rootElement = undefined;
    var blocker = undefined;

    var references = {};
    var controls = {};

    this.remove = function ()
    {
        if (!HTMLElement.prototype.isPrototypeOf(rootElement) ||
            !HTMLElement.prototype.isPrototypeOf(rootElement.parentElement)) return;
        rootElement.parentElement.removeChild(rootElement);
    };

    this.update = function ()
    {
        console.log(data);
        references["group-name"].setText(data.studentGroup.name);
        references["single-group-name"].setText(data.studentGroup.name);
        references["sheet-count"].setText(data.sheets.length);
        references["schedule-type-name"].setText(data.scheduleType.name);
        references["name"].setText("Расписание группы «" + data.studentGroup.name + "»");
        references["education-method-name"].setText(data.studentGroup.educationMethod.name);
        references["qualification-name"].setText(data.studentGroup.qualification.name);
    };

    (function ()
    {
        var templateElement = document.createElement('div');
        templateElement.innerHTML = utils.decodeHtml(template);

        /// Получаем рут элемент
        var root = rootElement = templateElement.firstElementChild;

        references = new ReferenceElementsBuilder("data-ref").build(root);
        controls = new ReferenceElementsBuilder("control").build(root);

        blocker = new Blocker(root.querySelector('[blocker]'));

        setupControls();

        parent.appendChild(root);
        that.update();
    })();

    function setupControls()
    {
        controls["delete"].element.addEventListener("click", () =>
        {
            if (confirm("Вы уверены, что хотите удалить расписание безвозвратно?"))
            {
                blocker.setActive(true);
                apiCatalog.delete({ Id: data.id },
                    () =>
                    {
                        blocker.setActive(false);
                        that.remove();
                    },
                    (err) =>
                    {
                        blocker.setActive(false);
                        console.error(err);
                    }
                );
            }
        });

        controls["edit"].element.addEventListener("click", () =>
        {
            try
            {
                blocker.setActive(true);

                var requestUrl = editScheduleUrl + "?ScheduleId=" + data.id;
                window.navigation.goAsync('Редактирование расписания', editScheduleUrl + "?ScheduleId=" + data.id, app.pageRendererContainerRoute.getElement());
            }
            catch (ex)
            {
                blocker.setActive(false);
            }
        });
    }
}