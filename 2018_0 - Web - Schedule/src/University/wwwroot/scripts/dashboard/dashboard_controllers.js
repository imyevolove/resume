﻿dashboard.controllers = dashboard.controllers || {};

dashboard.controllers.builder = dashboard.controllers.builder || {};

dashboard.controllers.builder.buildController = function (controllerName, collectionRootElement, itemClass, apiCatalog)
{
    var options = new DashboardControllerOptions();
    options.rootElement = collectionRootElement;
    options.apiCatalog = apiCatalog;
    options.itemClass = itemClass;

    return dashboard.controllers[controllerName] = new DashboardController(options);
};

/// Specialties controller
dashboard.controllers.builder.buildSpecialtiesController = function (collectionRootElement)
{
    return dashboard.controllers.builder.buildController("specialties", collectionRootElement, SpecialtyItem, window.api.catalog.specialties);
}

/// Instructors controller
dashboard.controllers.builder.buildInstructorsController = function (collectionRootElement)
{
    return dashboard.controllers.builder.buildController("instructors", collectionRootElement, InstructorItem, window.api.catalog.instructors);
}

/// Disciplines controller
dashboard.controllers.builder.buildDisciplinesController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("disciplines", collectionRootElement, DisciplineItem, window.api.catalog.disciplines);
}

/// Discipline methods controller
dashboard.controllers.builder.buildDisciplineMethodsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("disciplineMethods", collectionRootElement, DisciplineMethodItem, window.api.catalog.disciplineMethods);
}

/// Schedule types controller
dashboard.controllers.builder.buildScheduleTypesController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("scheduleTypes", collectionRootElement, ScheduleTypeItem, window.api.catalog.scheduleTypes);
}

/// Education methods controller
dashboard.controllers.builder.buildEducationMethodsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("educationMethods", collectionRootElement, EducationMethodItem, window.api.catalog.educationMethods);
}

/// Qualifications controller
dashboard.controllers.builder.buildQualificationsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("qualifications", collectionRootElement, QualificationItem, window.api.catalog.qualifications);
}

/// Student groups controller
dashboard.controllers.builder.buildStudentGroupsController = function (collectionRootElement) 
{
    return dashboard.controllers.builder.buildController("studentGroups", collectionRootElement, StudentGroupItem, window.api.catalog.studentGroups);
}

/// Schedules preview controller
dashboard.controllers.builder.buildSchedulesPreviewController = function (rootElement, itemTemplate) 
{
    var options = new DashboardControllerOptions();
    options.rootElement = rootElement;
    options.apiCatalog = window.api.catalog.schedules;
    options.itemClass = SchedulePreviewItem;
    options.itemTemplate = itemTemplate;

    return dashboard.controllers.schedulesPreview = new DashboardSchedulePreviewController(options);
}

function DashboardControllerOptions()
{
    this.rootElement = undefined;
    this.apiCatalog = undefined;
    this.itemClass = undefined;
}

function DashboardController(options) {
    var that = this;
    var loadedItemsData = [];
    var renderedItems = [];

    /// Container for drawing items
    this.collectionContainer = options.rootElement.querySelector(".collection-drawer");

    /// Blocker element
    this.blocker = new Blocker(options.rootElement.querySelector("[blocker]"));

    this.requestItemsData = function (callback) {
        callback = utils.normalizeFunction(callback);
        options.apiCatalog.get({},
            (response) =>
            {
                if (response['response'])
                    that.setRawItemsData(response.response);

                console.log(response);
                callback(response);
            });
    };

    this.forceUpdate = function (callback) {
        that.blocker.setActive(true);
        that.requestItemsData((res) => {
            that.redrawCollectionItems();
            that.blocker.setActive(false);
        });
    };

    this.setRawItemsData = function (itemsData) {
        loadedItemsData = [];
        
        for (var index = 0; index < itemsData.length; index++)
            loadedItemsData.push(itemsData[index]);
    };

    this.removeItems = function () {
        for (var index = 0; index < renderedItems.length; index++)
            renderedItems[index].remove();

        renderedItems = [];
    };

    this.redrawCollectionItems = function () {
        that.removeItems();

        for (var index = 0; index < loadedItemsData.length; index++) {
            var item = new options.itemClass(loadedItemsData[index], options.apiCatalog, that.collectionContainer);
            renderedItems.push(item);
        }
    };

    this.getLoadedData = function ()
    {
        return loadedItemsData;
    };

    this._addRenderedItem = function (item)
    {
        renderedItems.push(item);
    };
}

function DashboardSchedulePreviewController(options)
{
    DashboardController.call(this, options);

    var that = this;
    var activeCategory = new CategoryData("preview-active-box"),
        inactiveCategory = new CategoryData("preview-inactive-box");

    var _removeItems = this.removeItems;
    this.removeItems = function ()
    {
        _removeItems();
        activeCategory.resetCounter();
        inactiveCategory.resetCounter();
    };

    this.redrawCollectionItems = function ()
    {
        that.removeItems();

        var loadedItemsData = that.getLoadedData();
        for (var index = 0; index < loadedItemsData.length; index++)
        {
            var category = loadedItemsData[index].publicationStatus == 0 ? inactiveCategory : activeCategory;
            category.incrementCounter();

            var item = new options.itemClass(loadedItemsData[index], options.apiCatalog, category.collectionElement, options.itemTemplate);
            this._addRenderedItem(item);
        }
    };

    function CategoryData(dataRefName)
    {
        var that = this;
        var counterValue = 0;

        this.rootElement = options.rootElement.querySelector('[data-ref="' + dataRefName + '"]');
        this.counterElement = this.rootElement.querySelector('[data-ref="items-counter"]');
        this.collectionElement = this.rootElement.querySelector('[data-ref="items-container"]');

        this.incrementCounter = function ()
        {
            counterValue++;
            redrawCounter();
        };

        this.resetCounter = function ()
        {
            counterValue = 0;
            redrawCounter();
        };

        function redrawCounter()
        {
            that.counterElement.innerHTML = counterValue;
        }
    }
}