window.api = window.api || {};

window.api.paths = {
    base: "/api/"
};

window.api.paths.combine = function () {
    return [].slice.call(arguments).join("/").replace(/([^:]\/)\/+/g, "$1");
};

window.api.paths.makeApiPath = function (method) {
    return window.api.paths.combine(window.api.paths.base, method);
}

window.api.request = function (method, parameters, callback) {
    var path = window.api.paths.makeApiPath(method);
    window.request("post", path, parameters, function (response) {
        var responseObject = {};

        try {
            responseObject = JSON.parse(response);
        }
        catch (err) {
            responseObject = {
                error: {
                    errorCode: -1,
                    requestPath: path,
                    errorMessage: "Request failed. Response: " + response
                }
            };
        }

        callback(responseObject);
    });
};

window.api.catalog = (function () {
    var api = window.api;
    var paths = api.paths;

    var catalog = {};
    catalog.createApiCatalog = function createApiCatalog(categoryName, methodBase, methods) {
        catalog[categoryName] = (function () {

            var section = {};

            methods.forEach(function (methodName) {
                section[methodName] = function (parameters, callback) {
                    api.request(section[methodName].method, parameters, callback)
                }

                section[methodName].method = makeMethod(methodName);
                section[methodName].endPoint = paths.makeApiPath(section[methodName].method);
            });

            function makeMethod(name) {
                return methodBase + "." + name;
            }

            return section;
        })();
    }

    return catalog;
})();
api.catalog.createApiCatalog("instructors", "instructors", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("specialties", "specialties", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("disciplines", "disciplines", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("disciplineMethods", "disciplineMethods", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("educationMethods", "educationMethods", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("scheduleTypes", "scheduleTypes", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("qualifications", "qualifications", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("studentGroups", "studentGroups", ["add", "delete", "edit", "get"]);
api.catalog.createApiCatalog("schedules", "schedules", ["add", "delete", "edit", "get", "publication", "getPublic"]);
api.catalog.createApiCatalog("scheduleSheets", "schedules.sheets", ["add", "delete", "edit", "get", "getLinked", "move"]);
api.catalog.createApiCatalog("scheduleRecords", "schedules.records", ["add", "delete", "edit", "get"]);