﻿window.navigation = window.navigation || {};

(function (context)
{
    context.setUrlPath = function (data, title, url)
    {
        window.history.pushState(data, title, url);
    };

    context.nowIs = function (link) {
        return location.href.indexOf(link) >= 0;
    };

    context.go = function (link, forceGo)
    {
        if (!forceGo) {
            if (context.nowIs(link)) {
                return false;
            }
        }

        location.href = link;
    };

    context.goAsync = function (title, link, containerElement, callback)
    {
        callback = utils.normalizeFunction(callback);
        window.request("post", link, null, (response, xhr) =>
        {
            console.log("Async navigation ended. Url: " + xhr.responseURL);

            try
            {
                containerElement.innerHTML = response;
                utils.callScriptsInside(containerElement);
            }
            catch (err)
            {
                console.error(err);
            }

            context.setUrlPath(null, title, xhr.responseURL);
            callback(response);
        });

        return false;
    };

    context.reloadManualNavigations = true;

    window.addEventListener('popstate', function (event)
    {
        if (context.reloadManualNavigations)
            location.href = location.href;
    }, false);

})(window.navigation);