window.utils = window.utils || {};

utils.addSimpleHorizontalMouseWheelScroller = function (element)
{
    element.addEventListener("mousewheel", function (event, delta)
    {
        this.scrollLeft -= (event.deltaY < 0 ? 1 : -1) * 30;
        event.preventDefault();
    });
};


utils.addSimpleTouchScroller = (function ()
{
    var activeScrollbar = undefined;
    var threshold = 2;

    return function (element)
    {
        var dragging = false;
        var startScroll = {};

        var onMouseUp = function (event)
        {
            if (!canActivate()) return;

            startScroll.x = Math.abs(element.scrollLeft);
            startScroll.y = Math.abs(element.scrollTop);
            dragging = true;
        };

        var onMouseDown = function (event)
        {
            dragging = false;
            activeScrollbar = null;
        };

        var onMouseMove = function (event)
        {
            if (!dragging) return;
            if (!canActivate())
            {
                dragging = false;
                return;
            }

            if (element.parentElement == undefined)
            {
                element.removeEventListener("mousedown", onMouseUp);
                window.removeEventListener("mouseup", onMouseDown);
                window.removeEventListener("mousemove", onMouseMove);

                return;
            }

            element.scrollLeft -= event.movementX;
            element.scrollTop -= event.movementY;

            if (Math.abs(startScroll.x - Math.abs(element.scrollLeft)) > threshold || Math.abs(startScroll.y - Math.abs(element.scrollTop)) > threshold)
            {
                activeScrollbar = element;
            }
        }

        element.addEventListener("mousedown", onMouseUp);
        window.addEventListener("mouseup", onMouseDown);
        window.addEventListener("mousemove", onMouseMove);

        function canActivate()
        {
            return activeScrollbar == null || activeScrollbar == undefined || activeScrollbar == element;
        }
    }
})();

utils.removeElement = function (element)
{
    if (!HTMLElement.prototype.isPrototypeOf(element) ||
        !HTMLElement.prototype.isPrototypeOf(element.parentElement)) return;
    element.parentElement.removeChild(element);
};

utils.decodeHtml = function (input)
{
    var decoderElement = document.createElement('textarea');
    decoderElement.innerHTML = input;
    return decoderElement.value;
};

utils.getParameterByPath = function (targetObject, path)
{
    var parameterNames = path.split('.');
    var currentObject = targetObject;

    if (parameterNames.length == 1)
        return currentObject[path];

    for (var pIndex = 0; pIndex < parameterNames.length; pIndex++)
    {
        try
        {
            currentObject = currentObject[parameterNames[pIndex]];
        }
        catch (ex)
        {
            console.error("Object: ", targetObject, "Don't have parameters by path: ", path);
            return null;
        }
    }

    return currentObject;
};

utils.preventEnterKeyPress = function (e)
{
    return e.keyCode != 13;
}

utils.normalizeFunction = function (func, fallback)
{
    return Function.prototype.isPrototypeOf(func)
        ? func
        : Function.prototype.isPrototypeOf(fallback)
            ? fallback
            : function () { };
};

utils.getFormParameters = function (form)
{
    var data = {};

    [].slice.call(form.elements).forEach(function (element)
    {
        if (!InputParameterParser.prototype.isHtmlElement(element)) return;
        if (element.name == "") return;

        var parsers = utils.getFormParameters.parsers;
        var parseResult = undefined;
        for (var i = 0; i < parsers.length; i++)
        {
            parseResult = parsers[i].parse(element);
            if (parseResult == undefined || parseResult == null) continue;

            data[element.name] = parseResult;
            return;
        }
    });

    return data;
}
utils.getFormParameters.parsers = [
    /// Toggle parser
    new TypedInputParameterParser("checkbox", (input) =>
    {
        return input.checked;
    }),
    /// Time parser
    new TypedInputParameterParser("time", (input) =>
    {
        var parts = input.value.split(":");
        if (parts == null || parts == undefined || parts.length != 2) return 0;


        var hourse = parseInt(parts[0]);
        var minutes = parseInt(parts[1]);
        if (!Number.isInteger(hourse) || !Number.isInteger(minutes)) return 0;

        return hourse * 60 + minutes;
    }),
    /// Default parser
    new InputParameterParser((input) =>
    {
        return input.value;
    })
];

utils.clearFormParameters = function (form)
{
    [].slice.call(form.elements).forEach(function (element)
    {
        if (element.getAttribute("type") == "hidden") return;
        element.value = "";
    });
}

utils.validateFormErrors = function (form)
{
    var errors = [];

    [].slice.call(form.elements).forEach(function (element)
    {
        if (!element.hasAttribute('required') || element.value.length != "") return;
        errors.push("Поле '" + element.name + "' обязательно для заполнения");
    });

    return errors;
}

utils.validateForm = function (form)
{
    for (var index = 0; index < form.elements.length; index++)
    {
        if (form.elements[index].hasAttribute('required') && form.elements[index].value.length == "")
            return false;
    }

    return true;
}

utils.callScriptsInside = function (element)
{
    var scritps = element.getElementsByTagName('script')
    for (var n = 0; n < scritps.length; n++)
        eval(scritps[n].innerHTML);
}

function TypedInputParameterParser(attributeName, parserFunction)
{
    InputParameterParser.call(this, (input) =>
    {
        if (!InputParameterParser.prototype.isHtmlElement(input) || input.getAttribute("type") == null || input.getAttribute("type").toLowerCase() != attributeName.toLowerCase()) return undefined;
        return parserFunction(input);
    });
}

function InputParameterParser(parserFunction)
{
    var that = this;
    this.parse = function (input)
    {
        return parserFunction(input);
    };
}
InputParameterParser.prototype.isHtmlElement = function (element)
{
    return HTMLElement.prototype.isPrototypeOf(element);
};