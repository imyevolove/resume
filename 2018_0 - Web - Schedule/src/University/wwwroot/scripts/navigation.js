window.navigation = window.navigation || {};

(function (context)
{
    context.setUrlPath = function (data, title, url)
    {
        window.history.pushState(data, title, url);
    };

    context.nowIs = function (link) {
        return location.href.indexOf(link) >= 0;
    };

    context.go = function (link, forceGo)
    {
        if (!forceGo) {
            if (context.nowIs(link)) {
                return false;
            }
        }

        location.href = link;
    };

    context.goAsync = function (title, link, containerElement, callback)
    {
        callback = utils.normalizeFunction(callback);
        window.request("post", link, null, (response, xhr) =>
        {
            console.log("Async navigation ended. Url: " + xhr.responseURL);

            try
            {
                containerElement.innerHTML = response;
                utils.callScriptsInside(containerElement);
            }
            catch (err)
            {
                console.error(err);
            }

            context.setUrlPath(null, title, xhr.responseURL);
            callback(response);
        });

        return false;
    };

    context.reloadManualNavigations = true;

    window.addEventListener('popstate', function (event)
    {
        if (context.reloadManualNavigations)
            location.href = location.href;
    }, false);

})(window.navigation);
window.navigation = window.navigation || {};

window.navigation.createMenuAutoHandlerForApp = function (menuRootElement)
{
    return window.navigation.createMenuAutoHandler(menuRootElement, "href", "dboard-menu-item", "dboard-mi-active");
}

window.navigation.createMenuAutoHandlerForLink = function (menuRootElement, linkClass, activeClass)
{
    return window.navigation.createMenuAutoHandler(menuRootElement, "href", linkClass, activeClass);
}

window.navigation.createMenuAutoHandler = function(menuRootElement, linkTag, linkClass, activeClass)
{
    var rootElement = menuRootElement;
    var links = [].slice.call(rootElement.querySelectorAll("." + linkClass));
    var menuHandler = new MenuHandler(rootElement, links, linkTag, activeClass);
    menuHandler.update();

    return menuHandler;
}

function MenuHandler(container, links, linkTag, activeClass)
{
    var that = this;
    var themeCompactClass = "dboard-compact-view";

    this.turnTheme = function ()
    {
        if (container.classList.contains(themeCompactClass))
            container.classList.remove(themeCompactClass);
        else
            container.classList.add(themeCompactClass);
    };

    this.turnActive = function ()
    {
        that.setActive(container.getAttribute("active") == "no");
    };

    this.setActive = function (enabled)
    {
        container.setAttribute("active", enabled ? "" : "no");
    };

    this.update = function ()
    {
        var locationHref = location.href;
        var isActive = false;

        var marked = [];

        for (var index = 0; index < links.length; index++)
        {
            isActive = window.navigation.nowIs(links[index].getAttribute(linkTag));

            if (isActive)
                marked.push(links[index]);

            links[index].classList.remove(activeClass);
        }

        var priorityElement = undefined;
        var priorityWeight = -1;

        marked.forEach((e) =>
        {
            var attr = e.getAttribute(linkTag);
            if (attr.length > priorityWeight)
            {
                priorityWeight = attr.length;
                priorityElement = e;
            }
        });

        if (HTMLElement.prototype.isPrototypeOf(priorityElement))
            priorityElement.classList.add(activeClass);
    };
}