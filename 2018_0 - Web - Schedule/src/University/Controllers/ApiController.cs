﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using University.Api;
using University.ApiModels;
using University.Authorization;
using University.Database;
using University.Database.Models;
using University.ViewModels;

namespace University.Controllers
{
    [Authorize(Policy = PolicyConstants.PolicyManagers)]
    [Route("[controller]/[action]")]
    public class ApiController : Controller
    {
        private readonly ILogger m_Logger;
        private readonly EducationSpecialtyManager m_EducationSpecialtyManager;
        private readonly EducationMethodManager m_EducationMethodManager;
        private readonly InstructorManager m_InstructorManager;
        private readonly DisciplineManager m_DisciplineManager;
        private readonly DisciplineMethodManager m_DisciplineMethodManager;
        private readonly ScheduleManager m_ScheduleManager;
        private readonly ScheduleRecordManager m_ScheduleRecordManager;
        private readonly ScheduleSheetManager m_ScheduleSheetManager;
        private readonly ScheduleTypeManager m_ScheduleTypeManager;
        private readonly StudentGroupManager m_StudentGroupManager;
        private readonly QualificationManager m_QualificationManager;

        public ApiController(
            ILogger<ApiController> logger,
            EducationSpecialtyManager educationSpecialtyManager,
            EducationMethodManager educationMethodManager,
            InstructorManager instructorManager,
            DisciplineManager disciplineManager,
            DisciplineMethodManager disciplineMethodManager,
            ScheduleManager scheduleManager,
            ScheduleRecordManager scheduleRecordManager,
            ScheduleSheetManager scheduleSheetManager,
            ScheduleTypeManager scheduleTypeManager,
            StudentGroupManager studentGroupManager,
            QualificationManager qualificationManager)
        {
            m_Logger = logger;
            m_EducationSpecialtyManager = educationSpecialtyManager;
            m_EducationMethodManager = educationMethodManager;
            m_InstructorManager = instructorManager;
            m_DisciplineManager = disciplineManager;
            m_DisciplineMethodManager = disciplineMethodManager;
            m_ScheduleManager = scheduleManager;
            m_ScheduleRecordManager = scheduleRecordManager;
            m_ScheduleSheetManager = scheduleSheetManager;
            m_ScheduleTypeManager = scheduleTypeManager;
            m_StudentGroupManager = studentGroupManager;
            m_QualificationManager = qualificationManager;
        }

        #region Student groups api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/studentGroups.get")]
        public async Task<JsonResult> StudentGroupGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_StudentGroupManager, model, true);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/studentGroups.add")]
        public async Task<JsonResult> StudentGroupAdd(ApiStudentGroupAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);
            
            var entity = new StudentGroup { Expired = false };
                entity = await FillStudentGroup(entity, model);

            var result = await m_StudentGroupManager.CreateAsync(entity);
            return CreateSimpleResponse(result, entity, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/studentGroups.delete")]
        public async Task<JsonResult> StudentGroupDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_StudentGroupManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/studentGroups.edit")]
        public async Task<JsonResult> StudentGroupEdit(ApiStudentGroupEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_StudentGroupManager.FindByIdAsync(model.StudentGroupId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.Expired = model.Expired ?? entity.Expired;
            entity = await FillStudentGroup(entity, model);

            var editResult = await m_StudentGroupManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }

        private async Task<StudentGroup> FillStudentGroup(StudentGroup studentGroup, ApiStudentGroupAddViewModel model)
        {
            var educationMethod = (await m_EducationMethodManager.FindByIdAsync(model.EducationMethodId)).Entity;
            var qualification = (await m_QualificationManager.FindByIdAsync(model.QualificationId)).Entity;
            var specialty = (await m_EducationSpecialtyManager.FindByIdAsync(model.SpecialtyId)).Entity;

            studentGroup.Name = model.Name;
            studentGroup.OrganizationDate = model.OrganizationDate;
            studentGroup.EducationMethodForeignKey = educationMethod?.Id;
            studentGroup.QualificationForeignKey = qualification?.Id;
            studentGroup.SpecialtyForeignKey = specialty?.Id;

            return studentGroup;
        }
        #endregion

        #region Qualification api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/qualifications.get")]
        public async Task<JsonResult> QualificationGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_QualificationManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/qualifications.add")]
        public async Task<JsonResult> QualificationAdd(ApiQualificationAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var entity = new Qualification
            {
                Name = RemoveUnnecessarySpaces(model.Name)
            };

            var result = await m_QualificationManager.CreateAsync(entity);
            return CreateSimpleResponse(result, entity, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/qualifications.delete")]
        public async Task<JsonResult> QualificationDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_QualificationManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/qualifications.edit")]
        public async Task<JsonResult> QualificationEdit(ApiQualificationEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_QualificationManager.FindByIdAsync(model.QualificationId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.Name = RemoveUnnecessarySpaces(model.Name);

            var editResult = await m_QualificationManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion

        #region Education specialty api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/specialties.get")]
        public async Task<JsonResult> SpecialtyGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_EducationSpecialtyManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/specialties.add")]
        public async Task<JsonResult> SpecialtyAdd(ApiSpecialtiesAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var specialty = new EducationSpecialty
            {
                CodeName = model.CodeName.Replace(" ", string.Empty),
                FullName = model.FullName,
                SpecialtyCode = model.SpecialtyCode
            };

            var result = await m_EducationSpecialtyManager.CreateAsync(specialty);
            return CreateSimpleResponse(result, specialty, model, ApiErrorCode.CreateEntityFailed);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/specialties.delete")]
        public async Task<JsonResult> SpecialtyDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_EducationSpecialtyManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/specialties.edit")]
        public async Task<JsonResult> SpecialtyEdit(ApiSpecialtiesEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var findResult = await m_EducationSpecialtyManager.FindByIdAsync(model.SpecialtyId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.FullName = model.FullName;
            entity.CodeName = model.CodeName;
            entity.SpecialtyCode = model.SpecialtyCode;

            var editResult = await m_EducationSpecialtyManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion

        #region Education method api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/educationMethods.get")]
        public async Task<JsonResult> EducationMethodGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_EducationMethodManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/educationMethods.add")]
        public async Task<JsonResult> EducationMethodAdd(ApiEducationMethodAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var entity = new EducationMethod
            {
                Name = RemoveUnnecessarySpaces(model.Name)
            };

            var result = await m_EducationMethodManager.CreateAsync(entity);
            return CreateSimpleResponse(result, entity, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/educationMethods.delete")]
        public async Task<JsonResult> EducationMethodDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_EducationMethodManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/educationMethods.edit")]
        public async Task<JsonResult> EducationMethodEdit(ApiEducationMethodEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_EducationMethodManager.FindByIdAsync(model.EducationMethodId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.Name = RemoveUnnecessarySpaces(model.Name);

            var editResult = await m_EducationMethodManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion

        #region Instructor api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/instructors.get")]
        public async Task<JsonResult> InstructorGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_InstructorManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/instructors.add")]
        public async Task<JsonResult> InstructorAdd(ApiInstructorsAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var instructor = new Instructor
            {
                FirstName = model.FirstName.Replace(" ", string.Empty),
                LastName = model.LastName.Replace(" ", string.Empty),
                MiddleName = model.MiddleName.Replace(" ", string.Empty)
            };

            var result = await m_InstructorManager.CreateAsync(instructor);
            return CreateSimpleResponse(result, instructor, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/instructors.delete")]
        public async Task<JsonResult> InstructorDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_InstructorManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/instructors.edit")]
        public async Task<JsonResult> InstructorEdit(ApiInstructorsEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_InstructorManager.FindByIdAsync(model.InstructorId);
            if(!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.FirstName = model.FirstName;
            entity.LastName = model.LastName;
            entity.MiddleName = model.MiddleName;

            var editResult = await m_InstructorManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion

        #region Discipline api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/disciplines.get")]
        public async Task<JsonResult> DisciplineGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_DisciplineManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/disciplines.add")]
        public async Task<JsonResult> DisciplineAdd(ApiDisciplineAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var discipline = new Discipline
            {
                Name = RemoveUnnecessarySpaces(model.Name)
            };

            var result = await m_DisciplineManager.CreateAsync(discipline);
            return CreateSimpleResponse(result, discipline, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/disciplines.delete")]
        public async Task<JsonResult> DisciplineDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_DisciplineManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/disciplines.edit")]
        public async Task<JsonResult> DisciplineEdit(ApiDisciplineEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_DisciplineManager.FindByIdAsync(model.DisciplineId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.Name = RemoveUnnecessarySpaces(model.Name);

            var editResult = await m_DisciplineManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion
        
        #region Discipline method api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/disciplineMethods.get")]
        public async Task<JsonResult> DisciplineMethodGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_DisciplineMethodManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/disciplineMethods.add")]
        public async Task<JsonResult> DisciplineMethodAdd(ApiDisciplineMethodAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var discipline = new DisciplineMethod
            {
                Name = RemoveUnnecessarySpaces(model.Name)
            };

            var result = await m_DisciplineMethodManager.CreateAsync(discipline);
            return CreateSimpleResponse(result, discipline, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/disciplineMethods.delete")]
        public async Task<JsonResult> DisciplineMethodDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_DisciplineMethodManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/disciplineMethods.edit")]
        public async Task<JsonResult> DisciplineMethodEdit(ApiDisciplineMethodEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_DisciplineMethodManager.FindByIdAsync(model.DisciplineMethodId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.Name = RemoveUnnecessarySpaces(model.Name);

            var editResult = await m_DisciplineMethodManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion

        #region Schedule api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.get")]
        public async Task<JsonResult> ScheduleGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_ScheduleManager, model, async (entity) => 
            {
                await m_ScheduleManager.LoadRelationsAsync(entity);
                await m_QualificationManager.FindByIdAsync(entity.StudentGroup.QualificationForeignKey);
                await m_EducationMethodManager.FindByIdAsync(entity.StudentGroup.EducationMethodForeignKey);
                return new ApiScheduleGetResponseViewModel(entity);
            }, true);
        }

        [AllowAnonymous]
        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.getPublic")]
        public async Task<JsonResult> ScheduleGetPublic(ApiSchedulePublicGetViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            Schedule[] schedules;

            if (!string.IsNullOrEmpty(model.StudentGroupId))
            {
                schedules = m_ScheduleManager.Repository
                    .Find(schedule => schedule.PublicationStatus == PublicationStatus.Published && schedule.StudentGroupForeignKey == model.StudentGroupId)
                    .ToArray();
            }
            else if (!string.IsNullOrEmpty(model.InstructorId))
            {
                schedules = m_ScheduleManager.Repository
                    .Find(schedule => schedule.PublicationStatus == PublicationStatus.Published && schedule.Sheets.Any(sheet => sheet.Records.Any(record => record.InstructorId == model.InstructorId)))
                    .ToArray();
            }
            else
            {
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);
            }

            /// Очень плохой подход к запросу данных
            foreach (var entity in schedules)
            {
                await m_ScheduleManager.LoadRelationsAsync(entity);

                foreach (var scheet in entity.Sheets)
                {
                    await m_ScheduleSheetManager.LoadRelationsAsync(scheet);

                    foreach (var record in scheet.Records)
                        await m_ScheduleRecordManager.LoadRelationsAsync(record);
                }
            }

            if (!string.IsNullOrEmpty(model.InstructorId))
            {
                var instructorSchedules = new List<Schedule>();

                foreach (var schedule in schedules)
                {
                    if (!instructorSchedules.Exists(s => s.ScheduleType == schedule.ScheduleType))
                        instructorSchedules.Add(new Schedule { Id = Guid.NewGuid().ToString(), ScheduleType = schedule.ScheduleType, Sheets = new List<ScheduleSheet>() });

                    var instructorSchedule = instructorSchedules.FirstOrDefault(s => s.ScheduleType == schedule.ScheduleType);

                    schedule.Sheets.Sort((x, y) => x.WeekPriority.CompareTo(y.WeekPriority));
                    if (instructorSchedule.Sheets.Count < schedule.Sheets.Count)
                    {
                        var difference = schedule.Sheets.Count - instructorSchedule.Sheets.Count;
                        while (difference > 0)
                        {
                            instructorSchedule.Sheets.Add(new ScheduleSheet
                            {
                                Id = Guid.NewGuid().ToString(),
                                WeekPriority = instructorSchedule.Sheets.Count,
                                Records = new List<ScheduleRecord>()
                            });
                            difference--;
                        }
                    }

                    for (var i = 0; i < schedule.Sheets.Count; i++)
                    {
                        foreach (var record in schedule.Sheets[i].Records)
                        {
                            if (record.InstructorId != model.InstructorId) continue;
                            instructorSchedule.Sheets[i].Records.Add(record);
                        }
                    }
                }

                foreach (var schedule in instructorSchedules)
                    schedule.Sheets = schedule.Sheets.Where(s => s.Records.Count > 0).ToList();

                schedules = instructorSchedules.Where(s => s.Sheets.Count > 0).ToArray();
            }

            List<ApiScheduleGetResponseViewModel> collection = new List<ApiScheduleGetResponseViewModel>();
            foreach (var schedule in schedules)
                collection.Add(new ApiScheduleGetResponseViewModel(schedule));

            return CreateSimpleCollectionResponse(collection);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.add")]
        public async Task<JsonResult> ScheduleAdd(ApiScheduleAddViewModel model)
        {
            var valid = ModelState.IsValid;

            if (valid)
            {
                var studentGroup = (await m_StudentGroupManager.FindByIdAsync(model.StudentGroupId)).Entity;
                valid = studentGroup != null || studentGroup.Expired;
            }
            
            if (!valid)
            {
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);
            }

            var entity = new Schedule();
            entity = await FillSchedule(entity, model);

            var result = await m_ScheduleManager.CreateAsync(entity);
            return CreateSimpleResponse(result, result.Succeeded ? new ApiScheduleGetResponseViewModel(entity) : null, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.delete")]
        public async Task<JsonResult> ScheduleDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_ScheduleManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.edit")]
        public async Task<JsonResult> ScheduleEdit(ApiScheduleEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_ScheduleManager.FindByIdAsync(model.ScheduleId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.PublicationStatus = model.PublicationStatus ?? entity.PublicationStatus;
            entity = await FillSchedule(entity, model);

            var editResult = await m_ScheduleManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, editResult.Succeeded ? new ApiScheduleGetResponseViewModel(entity) : null, model, ApiErrorCode.EditEntityFailed);
        }


        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.publication")]
        public async Task<JsonResult> SchedulePublication(ApiSchedulePublicationViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_ScheduleManager.FindByIdAsync(model.ScheduleId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.PublicationStatus = model.State;

            var editResult = await m_ScheduleManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, editResult.Succeeded ? new ApiScheduleGetResponseViewModel(entity) : null, model, ApiErrorCode.EditEntityFailed);
        }

        private async Task<Schedule> FillSchedule(Schedule schedule, ApiScheduleAddViewModel model)
        {
            var studentGroup = (await m_StudentGroupManager.FindByIdAsync(model.StudentGroupId)).Entity;
            var scheduleType = (await m_ScheduleTypeManager.FindByIdAsync(model.ScheduleTypeId)).Entity;

            schedule.ScheduleTypeForeignKey = scheduleType?.Id;
            schedule.StudentGroupForeignKey = studentGroup?.Id;

            return schedule;
        }
        #endregion

        #region Schedule records api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.records.get")]
        public async Task<JsonResult> ScheduleRecordGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_ScheduleRecordManager, model, (e) => new ApiScheduleRecordGetResponseViewModel(e), true);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.records.delete")]
        public async Task<JsonResult> ScheduleRecordDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_ScheduleRecordManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.records.add")]
        public async Task<JsonResult> ScheduleRecordAdd(ApiScheduleRecordAddViewModel model)
        {
            /// Проверка общего состояния модели
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            /// Проверка id таблицы
            if (!(await EntityExists(m_ScheduleSheetManager, model.ScheduleSheetId)))
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, $"Sheet '{ model.ScheduleSheetId}' not exists");

            var entity = new ScheduleRecord
            {
                ScheduleSheetId = model.ScheduleSheetId
            };

            var fillReult = await FillSheduleRecord(entity, model);
            if (!fillReult.Successed)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, fillReult.ErrorMessage);

            var result = await m_ScheduleRecordManager.CreateAsync(entity);
            return CreateSimpleResponse(result, result.Succeeded ? new ApiScheduleRecordGetResponseViewModel(entity) : null, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.records.edit")]
        public async Task<JsonResult> ScheduleRecordEdit(ApiScheduleRecordEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_ScheduleRecordManager.FindByIdAsync(model.ScheduleRecordId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;

            var fillReult = await FillSheduleRecord(entity, model);
            if (!fillReult.Successed)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, fillReult.ErrorMessage);

            var editResult = await m_ScheduleRecordManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, editResult.Succeeded ? new ApiScheduleRecordGetResponseViewModel(entity) : null, model, ApiErrorCode.EditEntityFailed);
        }

        private async Task<PerformResult> FillSheduleRecord<TModel>(ScheduleRecord record, TModel model)
            where TModel : BaseApiScheduleRecordViewModel
        {
            /// Проверка id инструктора
            if (!(await EntityExists(m_InstructorManager, model.InstructorId)))
                return new PerformResult(false, $"Instructor with id '{model.InstructorId}' not exists");

            /// Проверка id дисциплины
            if (!(await EntityExists(m_DisciplineManager, model.DisciplineId)))
                return new PerformResult(false, $"Discipline with id '{model.DisciplineId}' not exists");

            /// Проверка id дисциплины
            if (!(await EntityExists(m_DisciplineMethodManager, model.DisciplineMethodId)))
                return new PerformResult(false, $"Discipline method with id '{model.DisciplineMethodId}' not exists");

            var timeLimit = 24 * 60;
            var startTime = model.StartTime;
            var duration = model.Duration;

            if (duration <= 0 || duration >= timeLimit || startTime < 0 || startTime >= timeLimit)
                return new PerformResult(false, $"Time is not in range. Start time: '{startTime}', duration: {duration}, time limit: {timeLimit}");

            record.InstructorId = model.InstructorId;
            record.DisciplineId = model.DisciplineId;
            record.DisciplineMethodId = model.DisciplineMethodId;
            record.Day = model.Day;
            record.Duration = duration;
            record.StartTime = startTime;
            record.Location = model.Location;

            return new PerformResult(true);
        }
        #endregion

        #region Schedule sheets api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.sheets.get")]
        public async Task<JsonResult> ScheduleSheetGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_ScheduleSheetManager, model, (entity) => new ApiScheduleSheetsGetResponseViewModel(entity), true);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.sheets.getLinked")]
        public async Task<JsonResult> ScheduleSheetGetLinked(ApiScheduleSheetsGetLinkedViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);
            
            /// Collection items request
            var result = m_ScheduleSheetManager.Repository.Find(e => e.ScheduleId == model.ScheduleId);

            foreach (var entity in result)
            {
                await m_ScheduleSheetManager.LoadRelationsAsync(entity);

                foreach(var record in entity.Records)
                    await m_ScheduleRecordManager.LoadRelationsAsync(record);
            }

            return CreateSimpleCollectionResponse(result.Reverse().Select(e => new ApiScheduleSheetsGetResponseViewModel(e)));
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.sheets.delete")]
        public async Task<JsonResult> ScheduleSheetDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_ScheduleSheetManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.sheets.add")]
        public async Task<JsonResult> ScheduleSheetAdd(ApiScheduleSheetsAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);
            }

            var schedule = (await m_ScheduleManager.FindByIdAsync(model.ScheduleId)).Entity;
            if (schedule == null)
            {
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, $"Schedule '{model.ScheduleId}' not exists");
            }

            var newWeekPriority = 0;

            if (model.WeekPriority.HasValue)
            {
                newWeekPriority = model.WeekPriority.Value;
            }
            else
            {
                await m_ScheduleManager.LoadRelationsAsync(schedule, nameof(schedule.Sheets));
                newWeekPriority = schedule.Sheets != null && schedule.Sheets.Count > 0
                    ? schedule.Sheets.Max(s => s != null ? s.WeekPriority : -1) + 1
                    : 0;
            }

            var entity = new ScheduleSheet
            {
                ScheduleId = schedule.Id,
                WeekPriority = newWeekPriority
            };

            var result = await m_ScheduleSheetManager.CreateAsync(entity);
            return CreateSimpleResponse(result, result.Succeeded ? new ApiScheduleSheetsGetResponseViewModel(entity) : null, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.sheets.edit")]
        public async Task<JsonResult> ScheduleSheetEdit(ApiScheduleSheetsEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_ScheduleSheetManager.FindByIdAsync(model.ScheduleSheetId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.WeekPriority = model.WeekPriority ?? entity.WeekPriority;

            var editResult = await m_ScheduleSheetManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, editResult.Succeeded ? new ApiScheduleSheetsGetResponseViewModel(entity) : null, model, ApiErrorCode.EditEntityFailed);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/schedules.sheets.move")]
        public async Task<JsonResult> ScheduleSheetMove(ApiScheduleSheetsMoveViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            if (model.Direction == 0)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, "Direction can't be a zero");

            var findResult = await m_ScheduleSheetManager.FindByIdAsync(model.ScheduleSheetId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);
            
            var entity = findResult.Entity;

            var schedule = (await m_ScheduleManager.FindByIdAsync(entity.ScheduleId)).Entity;
            if (schedule == null)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, $"Schedule '{entity.ScheduleId}' not exists");

            await m_ScheduleManager.LoadRelationsAsync(schedule, nameof(schedule.Sheets));

            var sign = Math.Sign(model.Direction);
            var otherSheets = schedule.Sheets.Where(sheet => sheet != entity);

            ApiErrorCode exceptionErrorCode = ApiErrorCode.Unknown;

            using (var transaction = await m_ScheduleSheetManager.Repository.Context.Database.BeginTransactionAsync())
            {
                try
                {
                    var downPrioritySheet = otherSheets.OrderBy(sheet => Math.Abs(entity.WeekPriority - sheet.WeekPriority)).First();
                    var upPrioritySheet = otherSheets.OrderBy(sheet => Math.Abs((downPrioritySheet.WeekPriority + 1) - sheet.WeekPriority)).First();

                    var newPriority = 0;

                    if (sign <= 0)
                    {
                        if (downPrioritySheet == null || downPrioritySheet.WeekPriority > entity.WeekPriority)
                        {
                            exceptionErrorCode = ApiErrorCode.InvalidParameters;
                            throw new Exception("Priority already is minimized");
                        }

                        newPriority = downPrioritySheet.WeekPriority == entity.WeekPriority 
                            ? downPrioritySheet.WeekPriority - 1 
                            : downPrioritySheet.WeekPriority;

                        downPrioritySheet.WeekPriority = entity.WeekPriority;
                        var downUpdateResult = await m_ScheduleSheetManager.UpdateAsync(downPrioritySheet);
                        if (!downUpdateResult.Succeeded)
                        {
                            exceptionErrorCode = ApiErrorCode.EditEntityFailed;
                            throw new Exception("Update up sheet error");
                        }

                        entity.WeekPriority = newPriority;
                    }
                    else
                    {
                        if (upPrioritySheet == null || upPrioritySheet.WeekPriority < entity.WeekPriority)
                        {
                            exceptionErrorCode = ApiErrorCode.InvalidParameters;
                            throw new Exception("Priority already is maximized");
                        }

                        newPriority = upPrioritySheet.WeekPriority == entity.WeekPriority
                            ? upPrioritySheet.WeekPriority + 1
                            : upPrioritySheet.WeekPriority;

                        upPrioritySheet.WeekPriority = entity.WeekPriority;
                        var upUpdateResult = await m_ScheduleSheetManager.UpdateAsync(upPrioritySheet);
                        if (!upUpdateResult.Succeeded)
                        {
                            exceptionErrorCode = ApiErrorCode.EditEntityFailed;
                            throw new Exception("Update down sheet error");
                        }

                        entity.WeekPriority = newPriority;
                    }

                    var editResult = await m_ScheduleSheetManager.UpdateAsync(entity);
                    if (!editResult.Succeeded)
                    {
                        exceptionErrorCode = ApiErrorCode.EditEntityFailed;
                        throw new Exception("Update sheet error");
                    }

                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    return CreateErrorResponse(exceptionErrorCode, model, ex.Message);
                }

                return CreateSimpleOkResponse(new ApiScheduleSheetsGetResponseViewModel(entity));
            }
        }
        #endregion

        #region Schedule types api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/scheduleTypes.get")]
        public async Task<JsonResult> ScheduleTypeGet(ApiEntityGetViewModel model)
        {
            return await HandleGetApiRequest(m_ScheduleTypeManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/scheduleTypes.add")]
        public async Task<JsonResult> ScheduleTypeAdd(ApiScheduleTypeAddViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model, ModelState);

            var entity = new ScheduleType
            {
                Name = RemoveUnnecessarySpaces(model.Name)
            };

            var result = await m_ScheduleTypeManager.CreateAsync(entity);
            return CreateSimpleResponse(result, entity, model, ApiErrorCode.CreateEntityFailed, result.ErrorMessage);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/scheduleTypes.delete")]
        public async Task<JsonResult> ScheduleTypeDelete(ApiEntityDeleteViewModel model)
        {
            return await HandleDeleteApiRequest(m_ScheduleTypeManager, model);
        }

        [HttpPost, HttpGet]
        [Route("/[controller]/scheduleTypes.edit")]
        public async Task<JsonResult> ScheduleTypeEdit(ApiScheduleTypeEditViewModel model)
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, model);

            var findResult = await m_ScheduleTypeManager.FindByIdAsync(model.ScheduleTypeId);
            if (!findResult.Succeeded)
                return CreateErrorResponse(ApiErrorCode.EntityNotFound, model);

            var entity = findResult.Entity;
            entity.Name = RemoveUnnecessarySpaces(model.Name);

            var editResult = await m_ScheduleTypeManager.UpdateAsync(entity);
            return CreateSimpleResponse(editResult, entity, model, ApiErrorCode.EditEntityFailed);
        }
        #endregion

        #region Utils api methods
        [HttpPost, HttpGet]
        [Route("/[controller]/getPublicationStatuses")]
        public JsonResult UtilsPublicationStatus()
        {
            var collection = EnumUtils.GetKeyValuePairs<PublicationStatus>()
                .Select(kvp => new EnumApiModel { Name = kvp.Value, Index = (int)kvp.Key });
            return CreateSimpleOkResponse(collection);
        }

        [AllowAnonymous]
        [HttpPost, HttpGet]
        [Route("/[controller]/getWeekdays")]
        public JsonResult UtilsWeekdays()
        {
            var collection = EnumUtils.GetKeyValuePairs<DayOfWeek>()
                .Select(kvp => new EnumApiModel { Name = kvp.Value, Index = (int)kvp.Key });
            return CreateSimpleCollectionResponse(collection);
        }

        [AllowAnonymous]
        [HttpPost, HttpGet]
        [Route("/[controller]/getServerTime")]
        public JsonResult UtilsServerTime()
        {
            return CreateSimpleOkResponse(DateTime.Now.ToFileTime());
        }
        #endregion

        #region Helpers
        private static async Task<T> LoadEntity<T>(Manager<T> manager, string id) where T : Entity
        {
            return (await manager.FindByIdAsync(id)).Entity;
        }

        private static async Task<bool> EntityExists<T>(Manager<T> manager, string id) where T : Entity
        {
            return (await LoadEntity(manager, id)) != null;
        }

        private static string RemoveUnnecessarySpaces(string value)
        {
            return Regex.Replace(value, @"\s+", " ").Trim();
        }
        #endregion

        #region Api request handlers
        private async Task<JsonResult> HandleGetApiRequest<TEntity, TResponseModel>(Manager<TEntity> manager, ApiEntityGetViewModel requestModel, Func<TEntity, TResponseModel> responseBuilder, bool loadRelations = false)
            where TEntity : Entity
            where TResponseModel : class

        {
            return await HandleGetApiRequest(manager, requestModel, (TEntity entity) => { return Task<TResponseModel>.Factory.StartNew(() => responseBuilder.Invoke(entity)); }, loadRelations);
        }

        private async Task<JsonResult> HandleGetApiRequest<TEntity, TResponseModel>(Manager<TEntity> manager, ApiEntityGetViewModel requestModel, Func<TEntity, Task<TResponseModel>> responseBuilder, bool loadRelations = false)
            where TEntity : Entity
            where TResponseModel : class

        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, requestModel, ModelState);

            /// Collection items request
            if (string.IsNullOrEmpty(requestModel.Id))
            {
                var result = await manager.Repository.GetAsync();

                if (loadRelations)
                {
                    foreach (var entity in result)
                        await manager.LoadRelationsAsync(entity);
                }

                var entities = result.Reverse();
                var collection = new List<TResponseModel>();
                foreach (var eItem in entities)
                {
                    if (eItem == null) continue;
                    collection.Add(await responseBuilder.Invoke(eItem));
                }

                return CreateSimpleCollectionResponse(collection);
            }

            /// Single item request
            else
            {
                var result = await manager.FindByIdAsync(requestModel.Id);
                if (result.Entity != null && loadRelations)
                    await manager.LoadRelationsAsync(result.Entity);

                return CreateSimpleResponse(
                    result,
                    result.Entity != null ? (await responseBuilder.Invoke(result.Entity)) : null,
                    requestModel,
                    ApiErrorCode.EntityNotFound);
            }
        }

        private async Task<JsonResult> HandleGetApiRequest<TEntity>(Manager<TEntity> manager, ApiEntityGetViewModel requestModel, bool loadRelations = false)
            where TEntity : Entity

        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, requestModel, ModelState);
            
            /// Collection items request
            if (string.IsNullOrEmpty(requestModel.Id))
            {
                var result = await manager.Repository.GetAsync();

                if (loadRelations)
                {
                    foreach (var entity in result)
                        await manager.LoadRelationsAsync(entity);
                }

                return CreateSimpleCollectionResponse(result.Reverse());
            }

            /// Single item request
            else
            {
                var result = await manager.FindByIdAsync(requestModel.Id);
                if (result.Entity != null && loadRelations)
                    await manager.LoadRelationsAsync(result.Entity);

                return CreateSimpleResponse(result, result.Entity, requestModel, ApiErrorCode.EntityNotFound);
            }
        }

        private async Task<JsonResult> HandleDeleteApiRequest<TEntity>(Manager<TEntity> manager, ApiEntityDeleteViewModel requestModel)
            where TEntity : Entity
        {
            if (!ModelState.IsValid)
                return CreateErrorResponse(ApiErrorCode.InvalidParameters, requestModel, ModelState);

            var result = await manager.DeleteByIdAsync(requestModel.Id);
            return CreateSimpleResponse(result, new { removed = result.Succeeded }, requestModel, ApiErrorCode.DeleteEntityFailed);
        }
        #endregion

        #region Response builders
        private static JsonResult CreateSimpleCollectionResponse<T>(List<T> collection)
        {
            return new JsonResult(ApiResponse.CreateOkResponse(collection));
        }

        private static JsonResult CreateSimpleCollectionResponse<T>(T[] array)
        {
            return new JsonResult(ApiResponse.CreateOkResponse(array));
        }

        private static JsonResult CreateSimpleCollectionResponse<T>(IEnumerable<T> collection)
        {
            return CreateSimpleCollectionResponse(collection.ToArray());
        }
        
        private static JsonResult CreateSimpleResponse<T>(ManagerResult<T> result, object okObject, object errorObject, ApiErrorCode errorCode)
            where T : Entity
        {
            return new JsonResult(result.Succeeded
               ? ApiResponse.CreateOkResponse(okObject) as object
               : ApiResponse.CreateErrorResponse(errorCode, errorObject) as object);
        }

        private static JsonResult CreateSimpleResponse<T>(ManagerResult<T> result, object okObject, object errorObject, ApiErrorCode errorCode, string errorMessage)
           where T : Entity
        {
            return new JsonResult(result.Succeeded
               ? ApiResponse.CreateOkResponse(okObject) as object
               : ApiResponse.CreateErrorResponse(errorCode, errorMessage, errorObject) as object);
        }

        private static JsonResult CreateErrorResponse(ApiErrorCode errorCode, object errorObject)
        {
            return new JsonResult(ApiResponse.CreateErrorResponse(errorCode, errorObject));
        }

        private static JsonResult CreateErrorResponse(ApiErrorCode errorCode, object errorObject, string errorMessage)
        {
            return new JsonResult(ApiResponse.CreateErrorResponse(errorCode, errorMessage, errorObject));
        }

        private static JsonResult CreateErrorResponse(ApiErrorCode errorCode, object errorObject, ModelStateDictionary modelState)
        {
            var errorMessage = modelState.Values.SelectMany(v => v.Errors).FirstOrDefault(e => e != null)?.ErrorMessage ?? "Invalid parameters";
            return new JsonResult(ApiResponse.CreateErrorResponse(errorCode, errorMessage, errorObject));
        }

        private static JsonResult CreateSimpleOkResponse(object okObject)
        {
            return new JsonResult(ApiResponse.CreateOkResponse(okObject));
        }
        #endregion

        public class PerformResult
        {
            public readonly bool Successed;
            public readonly string ErrorMessage;

            public PerformResult(bool successsed, string errorMessage = null)
            {
                Successed = successsed;
                ErrorMessage = errorMessage;
            }
        }
    }
}