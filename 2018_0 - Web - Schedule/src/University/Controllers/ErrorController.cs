﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace University.Controllers
{
    public class ErrorController : Controller
    {
        public static string DefaultErrorDescription = "Упс, что-то случилось...";
        public static readonly Dictionary<int, string> ErrorDescriptions = new Dictionary<int, string>
        {
            { 404, "Страница не найдена" }
        };

        private readonly ILogger m_Logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            m_Logger = logger;
        }

        [Route("/Error")]
        public IActionResult Any(int code)
        {
            var feature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            ViewBag.StatusCode = code;
            ViewBag.StatusCodeDescription = GetStatusCodeDescription(code);
            ViewBag.OriginalPath = feature?.OriginalPath;
            ViewBag.OriginalQueryString = feature?.OriginalQueryString;
            
            return View("Any");
        }

        private string GetStatusCodeDescription(int statusCode)
        {
            return ErrorDescriptions.ContainsKey(statusCode) ? ErrorDescriptions[statusCode] : DefaultErrorDescription;
        }
    }
}
