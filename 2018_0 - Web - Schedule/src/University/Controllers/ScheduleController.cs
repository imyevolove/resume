﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Authorization;
using University.Database;
using University.ViewModels;

namespace University.Controllers
{
    ///[Authorize(Policy = PolicyConstants.PolicyManagers)]
    [Route("[controller]/[action]")]
    public class ScheduleController : Controller
    {
        private readonly ILogger m_Logger;
        private readonly StudentGroupManager m_StudentGroupManager;
        private readonly InstructorManager m_InstructorManager;
        private readonly ScheduleConfiguration m_ScheduleConfiguration;

        public ScheduleController(
            ILogger<ScheduleController> logger,
            StudentGroupManager studentGroupManager,
            InstructorManager instructorManager,
            ScheduleConfiguration scheduleConfiguration)
        {
            m_Logger = logger;
            m_StudentGroupManager = studentGroupManager;
            m_InstructorManager = instructorManager;
            m_ScheduleConfiguration = scheduleConfiguration;
        }

        [Route("")]
        [Route("/[controller]/")]
        public async Task<IActionResult> Index()
        {
            var model = new SchedulePageDisplayViewModel(
                await m_StudentGroupManager.Repository.GetAsync(),
                await m_InstructorManager.Repository.GetAsync(),
                m_ScheduleConfiguration);
            return View(model);
        }
    }
}