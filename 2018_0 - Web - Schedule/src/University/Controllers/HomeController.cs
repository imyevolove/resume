﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace University.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        [Route("")]
        [Route("Index")]
        [Route("Home")]
        public IActionResult Index() => View();

        [Route("Privacy")]
        public IActionResult Privacy() => View();
    }
}
