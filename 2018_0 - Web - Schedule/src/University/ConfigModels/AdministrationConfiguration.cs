﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University
{
    public class AdministrationConfiguration
    {
        public List<string> Admins { get; set; } = new List<string>();
    }
}
