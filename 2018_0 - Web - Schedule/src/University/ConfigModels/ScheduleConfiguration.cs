﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University
{
    public class ScheduleConfiguration
    {
        public ScheduleWeekCountdownDate WeekCountdownDate { get; set; }

        public class ScheduleWeekCountdownDate
        {
            /// <summary>
            /// Месяц
            /// </summary>
            public int Month { get; set; }

            /// <summary>
            /// Дата / День
            /// </summary>
            public int Date { get; set; }
        }
    }
}
