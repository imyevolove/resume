﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University
{
    public static class EnumUtils
    {
        public static KeyValuePair<T, string>[] GetKeyValuePairs<T>()
            where T : struct, IConvertible
        {
            string[] names = Enum.GetNames(typeof(T));
            T[] values = (T[])Enum.GetValues(typeof(T));

            var array = new KeyValuePair<T, string>[names.Length];

            for (var index = 0; index < names.Length; index++)
                array[index] = new KeyValuePair<T, string>(values[index], names[index]);

            return array;
        }
    }
}
