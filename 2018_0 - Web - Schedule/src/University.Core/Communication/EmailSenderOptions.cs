﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace University.Communication
{
    public class EmailSenderOptions
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public MailAddress DefaultSender { get; set; }
    }
}
