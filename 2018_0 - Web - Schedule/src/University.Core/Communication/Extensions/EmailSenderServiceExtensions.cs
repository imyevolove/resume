﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace University.Communication
{
    public static class EmailSenderServiceExtensions
    {
        public static IServiceCollection AddEmailSender(this IServiceCollection services, EmailSenderOptions options)
        {
            return services.AddSingleton<IEmailSender>(new EmailSender(options));
        }

        public static IServiceCollection AddEmailSender(this IServiceCollection services, Action<EmailSenderOptions> setupAction)
        {
            var options = new EmailSenderOptions();
            setupAction.Invoke(options);

            return services.AddEmailSender(options);
        }
    }
}
