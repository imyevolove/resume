﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace University.Communication
{
    public class EmailSender : IEmailSender
    {
        public EmailSenderOptions Options { get; private set; }
        public MailAddress DefaultSender { get { return Options.DefaultSender; } }

        public EmailSender(EmailSenderOptions options)
        {
            Options = options;
        }

        public async Task SendEmailAsync(MailAddress from, MailAddress to, string subject, string message)
        {
            MailMessage mail = new MailMessage(from, to)
            {
                Subject = subject,
                Body = message,
                IsBodyHtml = true,
                Priority = MailPriority.High
            };

            using (SmtpClient smtp = new SmtpClient(Options.Host, Options.Port))
            {
                smtp.Credentials = new NetworkCredential(Options.Username, Options.Password);
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(mail);
            }
        }

        public Task SendEmailWithDefaultSenderAsync(MailAddress to, string subject, string message)
        {
            return SendEmailAsync(DefaultSender, to, subject, message);
        }
    }
}
