﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using University.Communication;

namespace University
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailWithDefaultSenderAsync(email, "Подтвердите вашу почту",
                $@"Пожалуйста, подтвердите ваш аккаунт кликнув по этой ссылке: <a href='{HtmlEncoder.Default.Encode(link)}'>подтвердить почту</a>
                   <br /> Пожалуйста, если вы не регистрировались на сайте, проигнорируйте данное сообщение");
        }

        public static Task SendPasswordResetAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailWithDefaultSenderAsync(email, "Восстановление пароля доступа",
                $@"Пожалуйста, восстановите ваш пароль кликнув по этой ссылке: <a href='{HtmlEncoder.Default.Encode(link)}'>восстановить пароль доступа</a>
                   <br /> Пожалуйста, если вы не регистрировались на сайте, проигнорируйте данное сообщение");
        }
    }
}
