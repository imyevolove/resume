﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Authentication.VK
{
    public static class VKDefaults
    {
        public const string AuthenticationScheme = "VK";
        public static readonly string DisplayName = "VK";
        public static readonly string AuthorizationEndpoint = "https://oauth.vk.com/authorize";
        public static readonly string TokenEndpoint = "https://oauth.vk.com/access_token";
        public static readonly string UserInformationEndpoint = "https://api.vk.com/method/users.get.json";
    }
}
