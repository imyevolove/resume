﻿using System;
using Microsoft.AspNetCore.Authentication;
using University.Authentication.VK;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class VKExtensions
    {
        public static AuthenticationBuilder AddVK(this AuthenticationBuilder builder)
             => builder.AddVK(VKDefaults.AuthenticationScheme, _ => { });

        public static AuthenticationBuilder AddVK(this AuthenticationBuilder builder, Action<VKOptions> configureOptions)
            => builder.AddVK(VKDefaults.AuthenticationScheme, configureOptions);

        public static AuthenticationBuilder AddVK(this AuthenticationBuilder builder, string authenticationScheme, Action<VKOptions> configureOptions)
            => builder.AddVK(authenticationScheme, VKDefaults.DisplayName, configureOptions);

        public static AuthenticationBuilder AddVK(this AuthenticationBuilder builder, string authenticationScheme, string displayName, Action<VKOptions> configureOptions)
            => builder.AddOAuth<VKOptions, VKHandler>(authenticationScheme, displayName, configureOptions);
    }
}