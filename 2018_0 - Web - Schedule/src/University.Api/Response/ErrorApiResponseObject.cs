﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Api
{
    public struct ErrorApiResponseObject
    {
        public int errorCode;
        public string errorMessage;
        public object requestParameters;

        public ErrorApiResponseObject(int errorCode, string errorMessage, object requestParameters = null)
        {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.requestParameters = requestParameters;
        }
    }
}
