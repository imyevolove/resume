﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace University.Api
{
    public class ApiResponse
    {
        private ApiResponse()
        {
        }

        public static OkApiResponseWrapper CreateOkResponse(object responseObject)
        {
            return new OkApiResponseWrapper(responseObject);
        }

        public static ErrorApiResponseWrapper CreateErrorResponse(ErrorApiResponseObject responseObject)
        {
            return new ErrorApiResponseWrapper(responseObject);
        }

        public static ErrorApiResponseWrapper CreateErrorResponse(ApiErrorCode errorCode, string errorMessage, object requestParameters)
        {
            return CreateErrorResponse(new ErrorApiResponseObject
            {
                errorCode = (int)errorCode,
                errorMessage = errorMessage,
                requestParameters = requestParameters
            });
        }

        public static ErrorApiResponseWrapper CreateErrorResponse(ApiErrorCode errorCode, object requestParameters)
        {
            var enumType = typeof(ApiErrorCode);
            var errorCodeAttribute = enumType.GetMember(errorCode.ToString())[0]
                .GetCustomAttributes(typeof(ErrorCodeMessageAttribute), false)
                .Cast<ErrorCodeMessageAttribute>()
                .FirstOrDefault(attr => attr != null);

            return CreateErrorResponse(errorCode, errorCodeAttribute?.message ?? "Error message undefined", requestParameters);
        }
    }
}
