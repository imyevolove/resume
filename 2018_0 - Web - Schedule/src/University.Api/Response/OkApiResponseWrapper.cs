﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Api
{
    public struct OkApiResponseWrapper
    {
        public readonly object response;

        public OkApiResponseWrapper(object response)
        {
            this.response = response;
        }
    }
}
