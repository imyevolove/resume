﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Api
{
    public enum ApiErrorCode
    {
        [ErrorCodeMessage("Unknown error")]
        Unknown = 0,

        [ErrorCodeMessage("One of the parameters specified was missing or invalid")]
        InvalidParameters = 100,

        [ErrorCodeMessage("Entity not found")]
        EntityNotFound = 101,

        [ErrorCodeMessage("Create new entity error")]
        CreateEntityFailed = 200,

        [ErrorCodeMessage("Delete entity error")]
        DeleteEntityFailed = 201,

        [ErrorCodeMessage("Edit entity error")]
        EditEntityFailed = 202,

        [ErrorCodeMessage("Unauthorized request")]
        Unauthorized = 1000
    }
}
