﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using University.Database.Models;

namespace University.Authorization
{
    public class AuthorizationHelper
    {
        /// <summary>
        /// Возвращает все найденные результаты определения
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <returns></returns>
        public static List<DefinitionInfo<TFieldType, TAttribute>> GetDefines<TClass, TFieldType, TAttribute>() 
            where TClass : class
            where TFieldType : class
            where TAttribute : Attribute
        {
            var targetType = typeof(TClass);
            var targetValueType = typeof(TFieldType);
            var defaultValue = default(TFieldType);

            return
                /// Получаем поля
                targetType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static)
                /// Получаем значение, если атрибут найден и удовлетворяет всем параметрам
                .Select(field =>
                {
                    var attribute = field.GetCustomAttribute<TAttribute>();
                    if (attribute == null || !field.FieldType.IsAssignableFrom(targetValueType) || Equals(field.GetValue(null), defaultValue)) return null;
                    return new DefinitionInfo<TFieldType, TAttribute>(field, attribute);
                }) 
                /// Фильтруем неподходящие определения                
                .Where(info => info != null)
                /// Возвращаем результат в виде коллекции определений
                .ToList();
        }

        public static List<IdentityRole> GetIdentityRoleDefines<TClass>() where TClass : class
        {
            return GetDefines<TClass, string, DefineRoleAttribute>().Select(info => new IdentityRole(info.Value)).ToList();
        }

        public static List<DefinitionInfo<string, PolicyRequirementsAttribute>> GetPolicyDefines<TClass>() 
            where TClass : class
        {
            return GetDefines<TClass, string, PolicyRequirementsAttribute>();
        }
    }
}
