﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace University.Authorization
{
    public class DefinitionInfo<TValue, TAttribute>
        where TValue : class
        where TAttribute : Attribute
    {
        public TAttribute Attribute { get; private set; }
        public FieldInfo Field { get; private set; }

        public string Name { get { return Field.Name; } }
        public TValue Value { get { return Field.GetValue(null) as TValue; } }

        public DefinitionInfo(FieldInfo field, TAttribute attribute)
        {
            Field = field;
            Attribute = attribute;
        }
    }
}
