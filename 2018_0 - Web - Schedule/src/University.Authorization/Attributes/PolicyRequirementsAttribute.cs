﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace University.Authorization
{
    [AttributeUsage(AttributeTargets.Field)]
    public class PolicyRequirementsAttribute : Attribute
    {
        public string[] Roles { get; set; }

        public void Configure(AuthorizationPolicyBuilder builder)
        {
            if (Roles != null)
            {
                builder.RequireRole(Roles.Where(role => !string.IsNullOrEmpty(role)));
            }
        }
    }
}
