﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public static class ManagerServiceExtensions
    {
        public static IServiceCollection AddManager<TManager, TEntity, TContext>(this IServiceCollection services)
            where TManager : Manager<TEntity>
            where TEntity : Entity
            where TContext : DbContext
        {
            return services.AddScoped(provider => Activator.CreateInstance(typeof(TManager), provider.GetService<TContext>()) as TManager);
        }
    }
}
