﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public static class AppManagerServiceExtensions
    {
        public static IServiceCollection AddAllUniversityManagers<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            services
                .AddEducationSpecialtyManager<TContext>()
                .AddEducationMethodManager<TContext>()
                .AddInstructorManager<TContext>()
                .AddDisciplineManager<TContext>()
                .AddDisciplineMethodManager<TContext>()
                .AddScheduleManager<TContext>()
                .AddScheduleRecordManager<TContext>()
                .AddScheduleSheetManager<TContext>()
                .AddScheduleTypeManager<TContext>()
                .AddQualificationManager<TContext>()
                .AddStudentGroupManagerManager<TContext>();

            return services;
        }

        public static IServiceCollection AddStudentGroupManagerManager<TContext>(this IServiceCollection services)
          where TContext : DbContext
        {
            return services.AddManager<StudentGroupManager, StudentGroup, TContext>();
        }

        public static IServiceCollection AddQualificationManager<TContext>(this IServiceCollection services)
          where TContext : DbContext
        {
            return services.AddManager<QualificationManager, Qualification, TContext>();
        }

        public static IServiceCollection AddScheduleTypeManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<ScheduleTypeManager, ScheduleType, TContext>();
        }

        public static IServiceCollection AddScheduleManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<ScheduleManager, Schedule, TContext>();
        }

        public static IServiceCollection AddScheduleSheetManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<ScheduleSheetManager, ScheduleSheet, TContext>();
        }

        public static IServiceCollection AddScheduleRecordManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<ScheduleRecordManager, ScheduleRecord, TContext>();
        }

        public static IServiceCollection AddDisciplineMethodManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<DisciplineMethodManager, DisciplineMethod, TContext>();
        }

        public static IServiceCollection AddDisciplineManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<DisciplineManager, Discipline, TContext>();
        }

        public static IServiceCollection AddInstructorManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<InstructorManager, Instructor, TContext>();
        }

        public static IServiceCollection AddEducationMethodManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<EducationMethodManager, EducationMethod, TContext>();
        }

        public static IServiceCollection AddEducationSpecialtyManager<TContext>(this IServiceCollection services)
           where TContext : DbContext
        {
            return services.AddManager<EducationSpecialtyManager, EducationSpecialty, TContext>();
        }
    }
}
