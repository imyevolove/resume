﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using University.Database.Models;

namespace University.Database
{
    public class ScheduleRecordManager : Manager<ScheduleRecord>
    {
        public ScheduleRecordManager(DbContext context) : base(context)
        {
        }
    }
}
