﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using University.Database.Models;

namespace University.Database
{
    public class ScheduleManager : Manager<Schedule>
    {
        public ScheduleManager(DbContext context) : base(context)
        {
        }
    }
}
