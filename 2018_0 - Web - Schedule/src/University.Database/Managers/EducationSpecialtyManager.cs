﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class EducationSpecialtyManager : Manager<EducationSpecialty>
    {
        public EducationSpecialtyManager(DbContext context) : base(context)
        {
        }
    }
}
