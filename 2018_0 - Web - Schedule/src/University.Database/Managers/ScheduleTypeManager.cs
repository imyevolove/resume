﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class ScheduleTypeManager : Manager<ScheduleType>
    {
        public ScheduleTypeManager(DbContext context) : base(context)
        {
        }
    }
}
