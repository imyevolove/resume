﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class QualificationManager : Manager<Qualification>
    {
        public QualificationManager(DbContext context) : base(context)
        {
        }
    }
}
