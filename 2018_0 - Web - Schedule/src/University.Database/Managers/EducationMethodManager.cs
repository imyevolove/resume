﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class EducationMethodManager : Manager<EducationMethod>
    {
        public EducationMethodManager(DbContext context) : base(context)
        {
        }
    }
}
