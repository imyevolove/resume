﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.Database
{
    public class Manager<TEntity>
        where TEntity : Entity
    {
        public readonly Repository<TEntity, DbContext> Repository;

        public Manager(DbContext context)
        {
            Repository = new Repository<TEntity, DbContext>(context);
        }

        public async Task<ManagerResult<TEntity>> CreateAsync(TEntity entity)
        {
            return await Execute(entity, Repository.AddAsync);
        }

        public async Task<ManagerResult<TEntity>> DeleteAsync(TEntity entity)
        {
            var removed = Repository.Remove(entity);
            return removed ? await ExecuteSave(entity) : new ManagerResult<TEntity>(entity, removed, "Entity not exists");
        }

        public async Task<ManagerResult<TEntity>> DeleteByIdAsync(string id)
        {
            var result = await FindByIdAsync(id);
            return result.Succeeded ? await DeleteAsync(result.Entity) : result;
        }

        public async Task<ManagerResult<TEntity>> UpdateAsync(TEntity entity)
        {
            Repository.Update(entity);
            return await ExecuteSave(entity);
        }

        public async Task<ManagerResult<TEntity>> FindByIdAsync(string id)
        {
            var entity = await Repository.FirstOrDefaultAsync(ent => ent.Id == id);
            return new ManagerResult<TEntity>(entity, entity != null && entity.Id == id);
        }

        public async Task LoadRelationsAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var navigations = Repository.Context.Entry(entity).Navigations;
            foreach (var navigation in navigations)
            {
                if (navigation.IsLoaded) continue;
                await navigation.LoadAsync();
            }
        }

        public async Task LoadRelationsAsync(TEntity entity, params string[] propertyNames)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var entry = Repository.Context.Entry(entity);
            var navigations = propertyNames.Select(name => entry.Navigation(name));

            foreach (var navigation in navigations)
            {
                if (navigation.IsLoaded) continue;
                await navigation.LoadAsync();
            }
        }

        private async Task<ManagerResult<TEntity>> Execute(TEntity entity, Func<TEntity, Task<bool>> action)
        {
            string errorMessage = null;
            bool status = await action.Invoke(entity);

            if (status)
            {
                var saveResult = await ExecuteSave();
                status = saveResult.Successed;
                errorMessage = saveResult.ErrorMessage;
            }

            return new ManagerResult<TEntity>(entity, status, errorMessage);
        }

        private async Task<ManagerResult<TEntity>> ExecuteSave(TEntity entity)
        {
            var saveResult = await ExecuteSave();
            return new ManagerResult<TEntity>(entity, saveResult.Successed, saveResult.ErrorMessage);
        }

        private async Task<SaveResult> ExecuteSave()
        {
            try
            {
                var status = await Repository.SaveChangesAsync() > 0;
                return new SaveResult(status, null);
            }
            catch (Exception ex)
            {
                return new SaveResult(false, ex?.InnerException.Message ?? ex.Message);
            }
        }

        public struct SaveResult
        {
            public bool Successed { get; private set; }
            public string ErrorMessage { get; private set; }

            public SaveResult(string errorMessage) 
                : this(!string.IsNullOrEmpty(errorMessage), errorMessage)
            {
            }

            public SaveResult(bool successed, string errorMessage)
            {
                Successed = successed;
                ErrorMessage = errorMessage;
            }
        }
    }
}
