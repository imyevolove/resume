﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public static class RepositoryServiceExtensions
    {
        public static IServiceCollection AddRepository<TRepository, TEntity, TContext>(this IServiceCollection services)
            where TRepository : Repository<TEntity, TContext>
            where TEntity : Entity
            where TContext : DbContext
        {
            return services.AddScoped<TRepository>();
        }

        public static IServiceCollection AddRepository<TEntity, TContext>(this IServiceCollection services)
            where TEntity : Entity
            where TContext : DbContext
        {
            return AddRepository<Repository<TEntity, TContext>, TEntity, TContext>(services);
        }
    }
}
