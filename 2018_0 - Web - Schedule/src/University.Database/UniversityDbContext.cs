﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class UniversityDbContext : IdentityDbContext<User>
    {
        public DbSet<EducationSpecialty> EducationSpecialties { get; set; }
        public DbSet<EducationMethod> EducationMethods { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<DisciplineMethod> DisciplineMethods { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduleSheet> ScheduleSheets { get; set; }
        public DbSet<ScheduleRecord> ScheduleRecords { get; set; }

        public UniversityDbContext(DbContextOptions<UniversityDbContext> options)
               : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.AddEntityConfigurationsFromAssembly(Assembly.GetAssembly(typeof(User)));
        }
    }
}
