﻿# Дипломный проект

**ASP.NETCore C#**

**Web-сервис расписаний** 

Проект может быть расширен до полнофункционального портала, поскольку все данные слабосвязаны и могут быть отредактированы в панели управления.

![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/0.png)
![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/3.png)
![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/5.png)
![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/6.png)
![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/4.png)
![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/1.png)
![Screenshot](https://bitbucket.org/imyevolove/resume/raw/master/2018_0%20-%20Web%20-%20Schedule/screenshots/2.png)