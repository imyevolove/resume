﻿namespace Dotto.Serialization
{
    public interface ISerializable
    {
        void Serialization(SerializationContainer container);
        void Deserialization(SerializationContainer container);
    }
}
