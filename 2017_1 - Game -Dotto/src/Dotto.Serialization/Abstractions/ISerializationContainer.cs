﻿using System.Collections.Generic;

namespace Dotto.Serialization
{
    public interface ISerializationContainer
    {
        int Count { get; }
        bool HasKey(string key);
        bool KeyValueIs<T>(string key);

        SerializationNode this[int index] { get; set; }
        object this[string key] { get; set; }

        List<SerializationNode> GetData();
        SerializationNode Get(string id);
        SerializationNode GetOrCreate(string id);
        void Set(SerializationNode node);
        void Set(ISerializationContainer container);
        bool TrySet(SerializationNode node);


        string ToXml();
    }
}
