﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Dotto.Serialization
{
    using System.Linq;

    [Serializable]
    public class SerializationContainer : ISerializationContainer
    {
        protected static readonly StorageSerialization StorageSerialization = new StorageSerialization();
        static SerializationContainer()
        {
            StorageSerialization.AddSerializer(new EnumSerializer());
            StorageSerialization.AddSerializer(new NodeSerializer());
            StorageSerialization.AddSerializer(new SerializableObjectSerializer());
            StorageSerialization.AddSerializer(new ContainerSerializer());
            StorageSerialization.AddSerializer(new ListSerializer());
            StorageSerialization.AddSerializer(new Vector3Serializer());
        }

        private static Type[] m_ExtraTypes = {
                    typeof(SerializationNode),
                    typeof(Node),
                    typeof(SerializableObjectSerializer),
                    typeof(SerializationContainer),
                    typeof(EnumSerializer),
                    typeof(Vector3),
                    typeof(List<Node>),
                    typeof(List<SerializationNode>),
                    typeof(List<SerializationContainer>)
                };

        protected List<SerializationNode> data = new List<SerializationNode>();

        [NonSerialized]
        protected readonly SerializationNode defaultKVP = default(SerializationNode);
        
        public int Count { get { return data.Count; } }

        public SerializationNode this[int index]
        {
            get
            {
                return data[index];
            }
            set
            {
                data[index] = value;
            }
        }

        public object this[string id]
        {
            get
            {
                return Find(id).value;
            }
            set
            {
                var result = Find(id);

                if (!result.Equals(defaultKVP))
                {
                    data.Remove(result);
                    Debug.LogFormat("Data with id [{0}] was rewrited", id);
                }

                data.Add(new SerializationNode() { key = id, value = value });
            }
        }

        ////////////////////////////////////////////////////////////
        public static SerializationContainer CreateContainer()
        {
            return new SerializationContainer();
        }

        ////////////////////////////////////////////////////////////
        public bool HasKey(string key)
        {
            var result = data.Find(pair => { return pair.key == key; });
            return !result.Equals(defaultKVP);
        }

        ////////////////////////////////////////////////////////////
        public bool KeyValueIs<T>(string key)
        {
            var result = data.Find(pair => { return pair.key == key; });
            return result.value is T;
        }

        ////////////////////////////////////////////////////////////
        public string ToXml()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Node), m_ExtraTypes);

            XmlWriterSettings settings = new XmlWriterSettings
            {
                NewLineHandling = NewLineHandling.None,
                Indent = false
            };

            StringWriter stringWriter = new StringWriter();

            XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings);
            xmlSerializer.Serialize(xmlWriter, StorageSerialization.Serialize("root", this));

            return stringWriter.ToString();
        }

        ////////////////////////////////////////////////////////////
        public void DeserializeOverwrite(string xml)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(Node), m_ExtraTypes);
                Node root;

                using (TextReader reader = new StringReader(xml))
                {
                    root = (Node)serializer.Deserialize(reader);
                }

                var container = StorageSerialization.Deserialize<SerializationContainer>(root);
                data = container.GetData();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        ////////////////////////////////////////////////////////////
        public static SerializationContainer Deserialize(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return new SerializationContainer();

            try
            {
                var serializer = new XmlSerializer(typeof(Node), m_ExtraTypes);
                Node root;

                using (TextReader reader = new StringReader(xml))
                {
                    root = (Node)serializer.Deserialize(reader);
                }

                var container = StorageSerialization.Deserialize<SerializationContainer>(root);
                return container;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return null;
            }
        }

        public void Deserialize<T>(string key, ref T value)
        {
            if (!KeyValueIs<T>(key)) return;
            value = (T)this[key];
        }

        ////////////////////////////////////////////////////////////
        public List<SerializationNode> GetData()
        {
            return data;
        }

        public override string ToString()
        {
            return string.Join(";", data.Select(item => string.Format("Key: {0}, Value: {1}", item.key, item.value)).ToArray());
        }

        public SerializationNode Get(string id)
        {
            return Find(id);
        }

        public SerializationNode GetOrCreate(string id)
        {
            var result = Find(id);

            if (result.IsEmpty())
            {
                result = new SerializationNode() { key = id };
            }

            return result;
        }

        public void Set(ISerializationContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("Container is empty");
            }

            for (int i = 0; i < container.Count; i++)
            {
                Set(container[i]);
            }
        }

        public void Set(SerializationNode node)
        {
            if (node.IsEmpty())
            {
                throw new NullReferenceException("Node is empty");
            }

            data.Add(node);
        }

        public bool TrySet(SerializationNode node)
        {
            if (node.IsEmpty())
            {
                throw new NullReferenceException("Node is empty");
            }

            if (HasKey(node.key)) return false;

            Set(node);

            return true;
        }

        private SerializationNode Find(string key)
        {
            var result = data.Find(pair => { return pair.key == key; });
            return result;
        }

        public static SerializationContainer Serialize(ISerializable target)
        {
            var container = new SerializationContainer();
            target.Serialization(container);
            return container;
        }
    }
}
