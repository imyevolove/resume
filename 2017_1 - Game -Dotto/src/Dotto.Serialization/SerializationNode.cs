﻿using System;

namespace Dotto.Serialization
{
    [Serializable]
    public struct SerializationNode
    {
        public string key;
        public object value;

        public bool IsEmpty()
        {
            return Equals(default(SerializationNode));
        }
    }
}
