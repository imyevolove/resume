﻿using System;
using System.Collections.Generic;

namespace Dotto.Serialization
{
    public class StorageSerialization : ISerializerProvider<Serializer>
    {
        protected HashSet<Serializer> serializers = new HashSet<Serializer>();
        
        ////////////////////////////////////////////////////////////
        public Node Serialize(string key, object obj)
        {
            if (obj == null)
            {
                obj = "";
            }

            var type = obj.GetType();
            var serializer = FindSerializer(type);
            if (serializer == null)
            {
                return new Node()
                {
                    key = key,
                    type = type.AssemblyQualifiedName,
                    value = obj
                };
            }

            return serializer.Serialize(key, obj, this);
        }

        ////////////////////////////////////////////////////////////
        public object Deserialize(object obj)
        {
            if (obj is Node)
            {
                var serializedData = (Node)obj;
                var type = Type.GetType(serializedData.type);

                if (type == null)
                {
                    throw new Exception("Type is null");
                }

                var serializer = FindSerializer(type);

                if (serializer == null)
                    return serializedData.value;

                return serializer.Deserialize((Node)obj, this);
            }
            
            return obj;
        }

        ////////////////////////////////////////////////////////////
        public T Deserialize<T>(object obj)
        {
            var result = Deserialize(obj);

            if (result is T)
                return (T)result;

            return default(T);
        }

        ////////////////////////////////////////////////////////////
        protected Serializer FindSerializer(Type type)
        {
            foreach (var serializer in serializers)
            {
                if (serializer.CanHandleType(type))
                    return serializer;
            }
            return null;
        }

        ////////////////////////////////////////////////////////////
        public void AddSerializer(Serializer handler)
        {
            serializers.Add(handler);
        }

        ////////////////////////////////////////////////////////////
        public void RemoveSerializer(Serializer handler)
        {
            serializers.Remove(handler);
        }
    }

    /*
     StorageSerialization serialization = new StorageSerialization();
     StorageSerialization.AddSerializer(new ListSerializer());
     StorageSerialization.AddSerializer(new StorageNode());
     StorageSerialization.AddSerializer(new StorageDataContainerSerializer());

     */
}
