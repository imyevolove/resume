﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dotto.Serialization
{
    using Utils;

    public class Vector3Serializer : Serializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(Vector3), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(Node obj, StorageSerialization provider)
        {
            var vector = new Vector3();
            try
            {
                var values = (List<Node>)obj.value;
                vector.Set((float)values[0].value, (float)values[1].value, (float)values[2].value);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return vector;
        }

        ////////////////////////////////////////////////////////////
        public override Node Serialize(string key, object obj, StorageSerialization provider)
        {
            var serialiedCollection = new List<Node>();
            var vector = (Vector3)obj;
            
            serialiedCollection.Add(provider.Serialize("x", vector.x));
            serialiedCollection.Add(provider.Serialize("y", vector.y));
            serialiedCollection.Add(provider.Serialize("z", vector.z));

            return new Node()
            {
                key = key,
                type = vector.GetType().AssemblyQualifiedName,
                value = serialiedCollection
            };
        }
    }
}
