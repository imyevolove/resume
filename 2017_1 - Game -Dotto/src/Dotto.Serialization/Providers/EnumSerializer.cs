﻿using System;

namespace Dotto.Serialization
{
    using Utils;

    public class EnumSerializer : Serializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(Enum), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(Node obj, StorageSerialization provider)
        {
            var destType = Type.GetType(obj.type);
            return Enum.ToObject(destType, obj.value);
        }

        ////////////////////////////////////////////////////////////
        public override Node Serialize(string key, object obj, StorageSerialization provider)
        {
            return new Node()
            {
                key = key,
                value = (int)obj,
                type = obj.GetType().AssemblyQualifiedName
            };
        }
    }
}
