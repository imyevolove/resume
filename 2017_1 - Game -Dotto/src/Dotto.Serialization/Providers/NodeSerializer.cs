﻿using System;

namespace Dotto.Serialization
{
    using Utils;

    public class NodeSerializer : Serializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(SerializationNode), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(Node obj, StorageSerialization provider)
        {
            return new SerializationNode()
            {
                key = obj.key,
                value = provider.Deserialize(obj.value)
            };
        }

        ////////////////////////////////////////////////////////////
        public override Node Serialize(string key, object obj, StorageSerialization provider)
        {
            var data = (SerializationNode)obj;
            return new Node()
            {
                key = key,
                value = provider.Serialize(data.key, data.value),
                type = data.GetType().AssemblyQualifiedName
            };
        }
    }
}
