﻿using Dotto.Utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Dotto.Serialization
{
    public class ContainerSerializer : Serializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(ISerializationContainer), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(Node obj, StorageSerialization provider)
        {
            var container = new SerializationContainer();

            foreach (var item in provider.Deserialize<IList>(obj.value))
            {
                var e = (Node)item;
                container[e.key] = provider.Deserialize(e);
            }

            return container;
        }

        ////////////////////////////////////////////////////////////
        public override Node Serialize(string key, object obj, StorageSerialization provider)
        {
            List<Node> serializedCollecionData = new List<Node>();
            var container = (SerializationContainer)obj;

            foreach (var item in container.GetData())
                serializedCollecionData.Add(provider.Serialize(item.key, item.value));

            return new Node()
            {
                key = key,
                type = typeof(SerializationContainer).AssemblyQualifiedName,
                value = serializedCollecionData
            };
        }
    }
}
