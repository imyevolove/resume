﻿using System;

namespace Dotto.Serialization
{
    public abstract class Serializer
    {
        public abstract Node Serialize(string key, object obj, StorageSerialization provider);
        public abstract object Deserialize(Node obj, StorageSerialization provider);
        public abstract bool CanHandleType(Type type);
    }
}
