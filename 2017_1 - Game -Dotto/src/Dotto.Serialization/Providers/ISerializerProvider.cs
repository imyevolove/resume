﻿namespace Dotto.Serialization
{
    public interface ISerializerProvider<TSerializer>
    {
        void AddSerializer(TSerializer handler);
        void RemoveSerializer(TSerializer handler);
    }
}
