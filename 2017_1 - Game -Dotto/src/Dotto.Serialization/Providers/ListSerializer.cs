﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Dotto.Serialization
{
    using UnityEngine;
    using Utils;

    public class ListSerializer : Serializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(IList), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(Node obj, StorageSerialization provider)
        {
            var collection = (IList)Activator.CreateInstance(Type.GetType(obj.type));

            foreach (var item in (IList)obj.value)
                collection.Add(provider.Deserialize(item));

            return collection;
        }

        ////////////////////////////////////////////////////////////
        public override Node Serialize(string key, object obj, StorageSerialization provider)
        {
            var serialiedCollection = new List<Node>();
            var collection = (IList)obj;

            foreach (var c in collection)
                serialiedCollection.Add(provider.Serialize("key", c));

            return new Node()
            {
                key = key,
                type = collection.GetType().AssemblyQualifiedName,
                value = serialiedCollection
            };
        }
    }
}
