﻿using Dotto.Utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Dotto.Serialization
{
    public class SerializableObjectSerializer : Serializer
    {
        ////////////////////////////////////////////////////////////
        public override bool CanHandleType(Type type)
        {
            return ReflectionHelper.IsAssignableFrom(typeof(ISerializable), type);
        }

        ////////////////////////////////////////////////////////////
        public override object Deserialize(Node obj, StorageSerialization provider)
        {
            var serializer = new ContainerSerializer();
            return serializer.Deserialize(obj, provider);
        }

        ////////////////////////////////////////////////////////////
        public override Node Serialize(string key, object obj, StorageSerialization provider)
        {
            if (obj is ISerializable)
            {
                var sObj = SerializationContainer.Serialize(obj as ISerializable);
                var serializer = new ContainerSerializer();
                return serializer.Serialize(key, sObj, provider);
            }
            else
            {
                return new Node()
                {
                    key = key,
                    value = obj,
                    type = obj.GetType().AssemblyQualifiedName
                };
            }
        }
    }
}
