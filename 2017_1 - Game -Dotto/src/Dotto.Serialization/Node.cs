﻿using System;

namespace Dotto.Serialization
{
    [Serializable]
    public struct Node
    {
        public string key;
        public object value;
        public string type;
    }
}
