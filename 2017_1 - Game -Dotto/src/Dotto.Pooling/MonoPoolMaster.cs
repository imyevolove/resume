﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dotto.Pooling
{
    [Serializable]
    public class MonoPoolMaster : IMonoPoolMaster
    {
        protected Dictionary<object, object> Pools = new Dictionary<object, object>();
        
        /// <summary>
        /// Вернет новую копию предоставленного объекта
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original"></param>
        /// <returns></returns>
        public virtual T GetFree<T>(T original) where T : Component
        {
            var pool = RegisterPool(original);
            return pool.GetFree();
        }

        /// <summary>
        /// Вернет true, если пул с указанным типо был зарегистрирован
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual bool ContainsPool<T>(T original) where T : Component
        {
            return Pools.ContainsKey(original);
        }

        /// <summary>
        /// Вернет объект пула, если таковой уже был зарегистрирован однажды
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual IMonoPool<T> GetPool<T>(T original) where T : Component
        {
            return RegisterPool(original);
        }

        /// <summary>
        /// Вернет зарегистрированный пул, если пул уже был создан, то вернется старая копия
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original"></param>
        /// <returns></returns>
        private IMonoPool<T> RegisterPool<T>(T original) where T : Component
        {
            if (original == null)
                throw new ArgumentNullException("Original object must be defined");
            
            IMonoPool<T> pool = null;

            if (!ContainsPool(original))
            {
                pool = new MonoPool<T>(original);
                Pools.Add(original, pool);
            }
            else
            {
                pool = Pools[original] as IMonoPool<T>;
            }

            return pool;
        }

        public bool Release<T>(T original, T obj) where T : Component
        {
            var pool = GetPool(original);
            if (pool == null) return false;
            return pool.Release(obj);
        }
    }
}
