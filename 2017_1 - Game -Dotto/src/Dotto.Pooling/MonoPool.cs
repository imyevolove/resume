﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dotto.Pooling
{
    [SerializeField]
    public class MonoPool<T> : IMonoPool<T>
        where T : Component
    {
        /// <summary>
        /// Оригинальный объект для спавна
        /// </summary>
        public T Orignal { get; protected set; }

        protected List<T> PoolItems = new List<T>();

        public MonoPool(T original)
        {
            Orignal = original;
        }

        /// <summary>
        /// Возвращает свободный объект пула
        /// </summary>
        /// <returns></returns>
        public virtual T GetFree()
        {
            var free = PoolItems.FirstOrDefault(o => ValidateFreeObject(o));
            if (free == null)
            {
                if (Orignal == null)
                    throw new NullReferenceException("Can't create pool object. Original object must be defined");

                free = UnityEngine.Object.Instantiate(Orignal);
                PoolItems.Add(free);
            }

            SetObjectActivity(free, true);

            return free;
        }

        /// <summary>
        /// Отключает объект принадлежащие пулу
        /// </summary>
        public bool Release(T obj)
        {
            if (!PoolItems.Contains(obj)) return false;
            SetObjectActivity(obj, false);
            return true;
        }

        /// <summary>
        /// Отключает все объекты принадлежащие пулу
        /// </summary>
        public virtual void ReleaseAll()
        {
            foreach (var obj in PoolItems)
            {
                if (obj != null)
                    SetObjectActivity(obj, false);
            }
        }

        /// <summary>
        /// Проводит проверку, свободен ли объект
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected virtual bool ValidateFreeObject(T obj)
        {
            return obj != null && !obj.gameObject.activeSelf;
        }

        /// <summary>
        /// Устанавливает объекту статус активности
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="status"></param>
        protected virtual void SetObjectActivity(T obj, bool status)
        {
            obj.gameObject.SetActive(status);
        }
    }
}
