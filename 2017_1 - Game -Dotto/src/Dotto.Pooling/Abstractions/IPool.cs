﻿namespace Dotto.Pooling
{
    public interface IPool<T>
    {
        T GetFree();
        bool Release(T obj);
        void ReleaseAll();
    }
}
