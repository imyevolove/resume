﻿using UnityEngine;

namespace Dotto.Pooling
{
    public interface IMonoPoolMaster
    {
        bool Release<T>(T original, T obj) where T : Component;
        T GetFree<T>(T original) where T : Component;
        IMonoPool<T> GetPool<T>(T original) where T : Component;
        bool ContainsPool<T>(T original) where T : Component;
    }
}
