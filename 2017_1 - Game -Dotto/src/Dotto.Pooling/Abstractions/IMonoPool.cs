﻿using UnityEngine;

namespace Dotto.Pooling
{
    public interface IMonoPool<T> : IPool<T> where T : Component
    {
        T Orignal { get; }
    }
}
