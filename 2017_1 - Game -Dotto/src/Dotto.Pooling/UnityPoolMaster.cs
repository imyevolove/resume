﻿using System;
using UnityEngine;

namespace Dotto.Pooling
{
    [Serializable]
    public class UnityPoolMaster : MonoBehaviour, IMonoPoolMaster
    {
        private static UnityPoolMaster m_Instance;
        public static UnityPoolMaster Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = FindObjectOfType<UnityPoolMaster>();

                if (m_Instance == null)
                    m_Instance = new GameObject("auto_created_UnityPoolMaster", typeof(UnityPoolMaster)).GetComponent<UnityPoolMaster>();

                return m_Instance;
            }
        }

        private MonoPoolMaster m_PoolMaster = new MonoPoolMaster();

        public bool ContainsPool<T>(T original) where T : Component
        {
            return m_PoolMaster.ContainsPool(original);
        }

        public T GetFree<T>(T original) where T : Component
        {
            var obj = m_PoolMaster.GetFree(original);
            obj.transform.SetParent(transform, false);
            return obj;
        }

        public IMonoPool<T> GetPool<T>(T original) where T : Component
        {
            return m_PoolMaster.GetPool(original);
        }

        public bool Release<T>(T original, T obj) where T : Component
        {
            return m_PoolMaster.Release(original, obj);
        }
    }
}
