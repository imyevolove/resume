﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.Popups
{
    [Serializable]
    public class ConfirmationPopup : SkipablePopup
    {
        [SerializeField] protected Button cancelButton;
        [SerializeField] protected Button confirmButton;
        
        protected virtual void Awake()
        {
            cancelButton.onClick.AddListener(CancelAction);
            confirmButton.onClick.AddListener(ConfirmAction);
        }

        private void CancelAction()
        {
            HidePopup();
            CompleteOperation(true);
        }

        private void ConfirmAction()
        {
            HidePopup();
            CompleteOperation(false);
        }
    }
}
