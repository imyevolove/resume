﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Dotto.Popups
{
    [Serializable]
    public class AwaitPopup : Popup
    {
        public void Cancel()
        {
            HidePopup();
            CompleteOperation(true);
        }
    }
}
