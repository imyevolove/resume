﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Popups
{
    public class PopupAsyncOperation
    {
        public bool IsCanceled { get; private set; }
        public bool IsDone { get; private set; }

        public event Action<PopupAsyncOperation> Event_Completed;

        internal void Complete(bool isCanceled)
        {
            if (IsDone)
            {
                Debug.LogWarning("Can't complete operation. Operation already was completed before.");
                return;
            }

            IsDone = true;
            IsCanceled = isCanceled;

            DispatchCompletedEvent();
        }

        private void DispatchCompletedEvent()
        {
            if (Event_Completed == null) return;
            Event_Completed.Invoke(this);
        }
    }
}
