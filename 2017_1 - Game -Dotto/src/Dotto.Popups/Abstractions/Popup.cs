﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.Popups
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    public abstract class Popup : MonoBehaviour
    {
        [SerializeField] protected Text messageText;
        private PopupAsyncOperation m_LastOperation;

        public string Message
        {
            get { return messageText.text; }
            set { messageText.text = value; }
        }
        
        protected virtual void ShowPopup()
        {
            gameObject.SetActive(true);
        }

        protected virtual void HidePopup()
        {
            gameObject.SetActive(false);
        }

        public virtual PopupAsyncOperation Show()
        {
            CompleteOperation(true);

            ShowPopup();

            m_LastOperation = new PopupAsyncOperation();
            return m_LastOperation;
        }

        protected void CompleteOperation(bool isCanceled)
        {
            if (m_LastOperation == null || m_LastOperation.IsDone) return;
            m_LastOperation.Complete(isCanceled);
        }
    }
}
