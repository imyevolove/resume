﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.EventSystems;

namespace Dotto.Popups
{
    [Serializable]
    public abstract class SkipablePopup : Popup, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            HidePopup();
            CompleteOperation(true);
        }
    }
}
