﻿using Dotto.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Popups
{
    [Serializable]
    public class PopupManager : MonoSingleton<PopupManager>
    {
        [SerializeField] public ConfirmationPopup watchAdsPopup;
        [SerializeField] public ConfirmationPopup usePowerupConfirmationPopup;
        [SerializeField] public ConfirmationPopup messageBox;
        [SerializeField] public AwaitPopup awaitPopup;
    }
}
