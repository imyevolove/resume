﻿using Dotto.Powerups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [CreateAssetMenu(menuName = "Powerup database")]
    public class PowerupDatabase : ScriptableObject
    {
        [SerializeField] private PowerupConfigurations[] m_Configurations = new PowerupConfigurations[0];
        public PowerupConfigurations[] Configurations { get { return m_Configurations; } }

        protected void OnEnable()
        {
            var powerupIds = Enum.GetValues(typeof(PowerupId)).Cast<PowerupId>().ToArray();
            var newConfigurations = m_Configurations.Distinct();
            m_Configurations = newConfigurations.ToArray();

            if (m_Configurations.Length != powerupIds.Length)
            {
                var addList = new List<PowerupConfigurations>(m_Configurations);

                for (var i = 0; i < powerupIds.Length; i++)
                {
                    if (Array.Exists(m_Configurations, c => c.Id == powerupIds[i])) continue;
                    addList.Add(new PowerupConfigurations(powerupIds[i]));
                }

                m_Configurations = addList.ToArray();
            }
        }

        public PowerupConfigurations Find(PowerupId id)
        {
            return m_Configurations.FirstOrDefault(c => c.Id == id);
        }
    }
}
