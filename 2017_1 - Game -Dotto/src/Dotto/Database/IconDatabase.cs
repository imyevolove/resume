﻿using Dotto.Economy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [CreateAssetMenu(menuName = "Icons database")]
    public class IconDatabase : ScriptableObject
    {
        [SerializeField] private List<CurrencyIconPair> m_CurrencyIcons = new List<CurrencyIconPair>();
        public List<CurrencyIconPair> CurrencyIcons { get { return m_CurrencyIcons; } }

        [Serializable]
        public class CurrencyIconPair : BaseKeyIconPair<CurrencyKind>
        {
        }

        [Serializable]
        public abstract class BaseKeyIconPair<TKey>
        {
            [SerializeField] public TKey key;
            [SerializeField] public Sprite icon;
        }
    }
}
