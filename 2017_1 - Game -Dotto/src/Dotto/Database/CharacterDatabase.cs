﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dotto
{
    [CreateAssetMenu(menuName = "Character database")]
    public class CharacterDatabase : ScriptableObject
    {
        [SerializeField] private List<CharacterTheme> m_Themes = new List<CharacterTheme>();
        public List<CharacterTheme> Themes { get { return m_Themes; } }

        public CharacterTheme Find(string guid, CharacterTheme fallback)
        {
            var result = Themes.FirstOrDefault(theme => theme.Guid == guid);
            return result == null ? fallback : result;
        }

        public CharacterTheme Find(string guid, bool fallbackFirst = false)
        {
            return Find(guid, fallbackFirst ? Themes[0] : null);
        }
    }
}
