﻿using Dotto.Economy;
using Dotto.Powerups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class PowerupConfigurations
    {
        [SerializeField] public PowerupId Id { get; private set; }
        [SerializeField] public string name;
        [SerializeField] public Price price;
        [SerializeField] public Sprite icon;

        public PowerupConfigurations(PowerupId id)
        {
            Id = id;
        }

        public PowerupConfigurations(PowerupId id, Price price)
        {
            Id = id;
            this.price = price;
        }

        public override bool Equals(object obj)
        {
            var config = obj as PowerupConfigurations;
            return config != null && Id == config.Id && price.Equals(config.price);
        }

        public override int GetHashCode()
        {
            var hashCode = 1311302826;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Price>.Default.GetHashCode(price);
            hashCode = hashCode * -1521134295 + EqualityComparer<Sprite>.Default.GetHashCode(icon);
            return hashCode;
        }
    }
}
