﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Dotto
{
    [Serializable]
    public class StageInfo
    {
        [SerializeField] private string m_SceneName;
        public string SceneName
        {
            get { return m_SceneName; }
            private set { m_SceneName = value; }
        }

        public int Index { get; set; }

        public StageInfo(Scene scene) : this(scene.name)
        {
        }
        public StageInfo(string sceneName)
        {
            SceneName = sceneName;
        }
    }
}
