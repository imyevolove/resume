﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public static class IconDatabaseExtensions
    {
        public static Sprite FindByKey<T, TKey>(this List<T> collection, TKey key) where T : IconDatabase.BaseKeyIconPair<TKey>
        {
            var kvp = collection.FirstOrDefault(i => i.key.Equals(key));
            return kvp == null ? null : kvp.icon;
        }
    }
}
