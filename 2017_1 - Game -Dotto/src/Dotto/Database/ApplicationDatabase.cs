﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Database
{
    [CreateAssetMenu(menuName = "Application database")]
    public class ApplicationDatabase : ScriptableObject
    {
        private const string DatabasePath = "DB/MainDatabase";

        private static ApplicationDatabase m_Instance;
        public static ApplicationDatabase Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = LoadDatabase(DatabasePath);

                    if(m_Instance == null)
                    {
                        Debug.LogErrorFormat("Database 'ApplicationDatabase' not loaded. Search path: {0}", DatabasePath);
                    }
                }

                return m_Instance;
            }
        }

        [SerializeField] private StageDatabase m_Stages;
        public StageDatabase Stages { get { return m_Stages; } }

        [SerializeField] private CharacterDatabase m_Characters;
        public CharacterDatabase Characters { get { return m_Characters; } }

        [SerializeField] private PowerupDatabase m_Powerups;
        public PowerupDatabase Powerups { get { return m_Powerups; } }

        [SerializeField] private IconDatabase m_Icons;
        public IconDatabase Icons { get { return m_Icons; } }

        private static ApplicationDatabase LoadDatabase(string path)
        {
            return Resources.Load<ApplicationDatabase>(path);
        }
    }
}
