﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Dotto
{
    [CreateAssetMenu(menuName = "Stage database")]
    public class StageDatabase : ScriptableObject
    {
        [SerializeField] private List<StageInfo> m_Stages = new List<StageInfo>();
        public List<StageInfo> Stages { get { return m_Stages; } }

        protected virtual void OnEnable()
        {
            for (var i = 0; i < Stages.Count; i++)
                Stages[i].Index = i;
        }

        public StageInfo FindNext(StageInfo stage, StageInfo fallback)
        {
            var index = FindIndex(stage);
            var nextIndex = index + 1;

            if (index < 0 || nextIndex >= Stages.Count) return fallback;
            return Stages[nextIndex];
        }

        public int FindIndex(StageInfo stage)
        {
            return Stages.IndexOf(stage);
        }

        public void SwapStage(StageInfo stage, int index)
        {
            if (Stages.Contains(stage) == null) return;

            index = Mathf.Clamp(index, 0, Stages.Count - 1);
            Stages.Remove(stage);
            Stages.Insert(index, stage);
        }

        public void SwapToStart(StageInfo stage)
        {
            var inCollectionIndex = Stages.IndexOf(stage);
            if (inCollectionIndex < 0) return;

            SwapStage(stage, inCollectionIndex - 1);
        }

        public void SwapToend(StageInfo stage)
        {
            var inCollectionIndex = Stages.IndexOf(stage);
            if (inCollectionIndex < 0) return;

            SwapStage(stage, inCollectionIndex + 1);
        }

        public StageInfo FindByIndex(int index)
        {
            try
            {
                return Stages[index];
            }
            catch (Exception)
            {
                return Stages[0];
            }
        }

        public bool HasScene(string sceneName)
        {
            return Stages.Exists(stage => stage.SceneName == sceneName);
        }
    }
}
