﻿using System;
using UnityEngine;

namespace Dotto
{
    using Billing;

    public sealed class BillingResources
    {
        private BillingResources()
        {
        }

        public static readonly BillingProduct AdvertisementBlocker = new BillingProduct("pdt_adblock", (player) => { player.AdvertisementDisabled.Value = true; });

        public class BillingProduct
        {
            public readonly IProduct Product;
            public readonly Action<Player> ApplyAction;

            public BillingProduct(string id, Action<Player> applyAction)
            {
                Product = new IAPProduct(id, UnityEngine.Purchasing.ProductType.Consumable);
                ApplyAction = applyAction;
            }

            public void Purchase()
            {
                var player = Player.Local;
                if (player == null)
                    throw new NullReferenceException("Local player nor defined");

                Product.Purchase((data) =>
                {
                    if (data.status != PurchaseStatus.Fail)
                    {
                        ApplyAction(player);
                        Debug.LogFormat("Purcashed: {0}", Product.ProductID);
                    }
                    else
                    {
                        Debug.LogErrorFormat("Not purcashed: {0}. Status: {1}", Product.ProductID, data.status);
                    }
                });
            }
        }
    }
}
