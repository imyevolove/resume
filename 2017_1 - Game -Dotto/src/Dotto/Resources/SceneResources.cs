﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Dotto
{
    public sealed class SceneResources
    {
        private SceneResources()
        {
        }

        public static readonly SceneRoute GameScene = new SceneRoute("scn_Game");

        public class SceneRoute
        {
            public readonly string SceneName;

            public SceneRoute(string sceneName)
            {
                SceneName = sceneName;
            }

            public AsyncOperation LoadScene()
            {
                return SceneManager.LoadSceneAsync(SceneName);
            }
        }
    }
}