﻿using Dotto.Socials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VK.Unity;

namespace Dotto
{
    public class GameVars
    {
        private GameVars()
        {
        }

        public readonly static ISocial Social = new VKSocial(Scope.Friends, Scope.Groups, Scope.Notifications, Scope.Wall);
    }
}
