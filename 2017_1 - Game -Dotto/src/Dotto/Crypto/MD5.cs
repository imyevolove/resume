﻿using System.Text;

public static class MD5
{
    public static string CreateHash(string input)
    {
        byte[] inputBytes = Encoding.UTF8.GetBytes(input);
        byte[] hash;

#if UNITY_WINRT
            hash = UnityEngine.Windows.Crypto.ComputeMD5Hash(inputBytes);
#else
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        hash = md5.ComputeHash(inputBytes);
#endif
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }
}
