﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(Point))]
    public abstract class BaseWallReaction : MonoBehaviour
    {
        private Point m_Target;
        public Point Target
        {
            get
            {
                if (m_Target == null)
                    m_Target = GetComponent<Point>();

                return m_Target;
            }
        }

        protected virtual void Awake()
        {
            Target.Event_OnDestruct.AddListener(OnDestruct);
            Target.Event_OnRestore.AddListener(OnRestore);
        }

        protected abstract void OnDestruct(Point.DestructEventData eventData);
        protected abstract void OnRestore();
    }
}
