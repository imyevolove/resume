﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class BasicWallReaction : BaseWallReaction
    {
        [SerializeField] protected ParticleSystem collideParticleSystem;

        protected override void OnDestruct(Point.DestructEventData eventData)
        {
            if(collideParticleSystem != null)
                ParticleMaster.Instance.Play(collideParticleSystem, eventData.ContactPoint, Quaternion.FromToRotation(Vector3.up, eventData.ContactDirection));
        }

        protected override void OnRestore()
        {
        }
    }
}
