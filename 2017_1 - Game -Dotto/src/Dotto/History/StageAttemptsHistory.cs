﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto
{
    public class StageAttemptsHistory : IStageAttemptsHistory
    {
        private Dictionary<StageInfo, Attempt> m_Attempts = new Dictionary<StageInfo, Attempt>();

        private class Attempt
        {
            public int counter = 0;
        }

        public void ClearAttempts(StageInfo stage)
        {
            if (stage == null) return;
            if (!m_Attempts.ContainsKey(stage)) return;
            m_Attempts[stage].counter = 0;
        }

        public int GetAttemptsCount(StageInfo stage)
        {
            return stage != null && m_Attempts.ContainsKey(stage) ? m_Attempts[stage].counter : 0;
        }

        public int AddAttempt(StageInfo stage)
        {
            Attempt attemp = null;

            if (!m_Attempts.TryGetValue(stage, out attemp))
            {
                attemp = new Attempt();
                m_Attempts.Add(stage, attemp);
            }

            attemp.counter++;
            return m_Attempts[stage].counter;
        }
    }
}
