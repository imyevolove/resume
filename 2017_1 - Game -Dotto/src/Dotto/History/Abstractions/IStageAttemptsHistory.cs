﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto
{
    public interface IStageAttemptsHistory
    {
        int AddAttempt(StageInfo stage);
        int GetAttemptsCount(StageInfo stage);
        void ClearAttempts(StageInfo stage);
    }
}
