﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public class AnimatorParameterMemento
    {
        private static Dictionary<AnimatorControllerParameterType, AnimatorParameterHandler> AnimatorParameterHandlers
              = new Dictionary<AnimatorControllerParameterType, AnimatorParameterHandler>
              {
                  {
                    AnimatorControllerParameterType.Int,
                    new AnimatorParameterHandler(
                        (animator, parameter) => { return animator.GetInteger(parameter.name); },
                        (animator, memento) => { animator.SetInteger(memento.ParameterName, (int)memento.ParameterValue); }
                        )
                  },
                  {
                    AnimatorControllerParameterType.Float,
                    new AnimatorParameterHandler(
                        (animator, parameter) => { return animator.GetFloat(parameter.name); },
                        (animator, memento) => { animator.SetFloat(memento.ParameterName, (float)memento.ParameterValue); }
                        )
                  },
                  {
                    AnimatorControllerParameterType.Bool,
                    new AnimatorParameterHandler(
                        (animator, parameter) => { return animator.GetBool(parameter.name); },
                        (animator, memento) => { animator.SetBool(memento.ParameterName, (bool)memento.ParameterValue); }
                        )
                  }
              };

        public readonly AnimatorControllerParameterType ParameterType;
        public readonly string ParameterName;
        public readonly object ParameterValue;
        
        public class AnimatorParameterHandler
        {
            public delegate object PullParameterValueDelegate(Animator animator, AnimatorControllerParameter parameter);
            public delegate void RestoreParameterValueDelegate(Animator animator, AnimatorParameterMemento memento);

            public readonly PullParameterValueDelegate PullParamterValueFunction;
            public readonly RestoreParameterValueDelegate RestoreParameterValueFunction;

            public AnimatorParameterHandler(
                PullParameterValueDelegate pullParameterValue,
                RestoreParameterValueDelegate restoreParameterValue)
            {
                PullParamterValueFunction = pullParameterValue;
                RestoreParameterValueFunction = restoreParameterValue;
            }
        }
        
        public AnimatorParameterMemento(Animator animator, AnimatorControllerParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentNullException("Animator parameter must be defined");

            if (animator == null)
                throw new ArgumentNullException("Animator must be defined");

            ParameterType = parameter.type;
            ParameterName = parameter.name;

            AnimatorParameterHandler handler = null;
            AnimatorParameterHandlers.TryGetValue(parameter.type, out handler);

            if(handler != null)
                ParameterValue = handler.PullParamterValueFunction.Invoke(animator, parameter);
        }

        public void Restore(Animator animator)
        {
            Restore(animator, this);
        }

        public static void Restore(Animator animator, AnimatorParameterMemento memento)
        {
            if (memento == null)
                throw new ArgumentNullException("Memento must be defined");

            if (animator == null)
                throw new ArgumentNullException("Animator must be defined");

            AnimatorParameterHandler handler = null;
            AnimatorParameterHandlers.TryGetValue(memento.ParameterType, out handler);

            if (handler != null)
                handler.RestoreParameterValueFunction.Invoke(animator, memento);
        }
    }
}
