﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public class AnimatorMemento
    {
        public readonly Animator Animator;
        public AnimatorParameterMemento[] Parameters { get; protected set; }
        
        public AnimatorMemento(Animator animator)
        {
            Animator = animator;
            Parameters = PullAnimatorParameters(animator);
        }

        public void Restore()
        {
            Restore(Animator);
        }

        public void Restore(Animator animator)
        {
            Restore(this, animator);
        }

        public static void Restore(AnimatorMemento memento, Animator animator)
        {
            if (memento == null)
                throw new ArgumentNullException("Memento must be defined");

            if (animator == null)
                throw new ArgumentNullException("Animator must be defined");

            foreach (var parameter in memento.Parameters)
                parameter.Restore(animator);
        }

        public static AnimatorParameterMemento[] PullAnimatorParameters(Animator animator)
        {
            AnimatorParameterMemento[] parameters = new AnimatorParameterMemento[animator.parameterCount];

            for (var i = 0; i < animator.parameterCount; i++)
                parameters[i] = new AnimatorParameterMemento(animator, animator.parameters[i]);

            return parameters;
        }
    }
}
