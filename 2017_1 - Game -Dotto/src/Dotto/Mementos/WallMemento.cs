﻿namespace Dotto
{
    public class WallMemento
    {
        public Point Source { get; private set; }
        public bool Active { get; private set; }

        public WallMemento(Point wall)
        {
            Source = wall;
            Active = wall.Active;
        }
    }
}