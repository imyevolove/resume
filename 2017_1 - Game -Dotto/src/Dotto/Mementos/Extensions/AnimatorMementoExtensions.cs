﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public static class AnimatorMementoExtensions
    {
        public static AnimatorMemento GetMemento(this Animator animator)
        {
            return new AnimatorMemento(animator);
        }
    }
}
