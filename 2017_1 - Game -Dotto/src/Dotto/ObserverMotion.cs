﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class ObserverMotion : MonoBehaviour
    {
        [SerializeField] public Transform target;
        [SerializeField] public float speed;

        protected virtual void Update()
        {
            if (target == null) return;
            MoveToTarget();
        }

        protected virtual void MoveToTarget()
        {
            transform.position = Vector2.Lerp(transform.position, target.position, Time.deltaTime * speed);
        }
    }
}
