﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(Ball))]
    public class PlayerCharacterThemeChanger : MonoBehaviour
    {
        private Ball m_Ball;
        private Player m_Player;

        public void ApplyTheme(CharacterTheme theme)
        {
            m_Ball.UseTheme(theme);
        }

        protected void ApplyPlayerTheme()
        {
            ApplyTheme(m_Player.CurrentCharacterTheme);
        }

        protected virtual void Awake()
        {
            m_Player = Player.Local;
            m_Ball = GetComponent<Ball>();
            m_Player.Event_CharacterThemeChanged.AddListener(OnPlayerCharacterThemeChanged);
        }

        protected virtual void Start()
        {
            ApplyPlayerTheme();
        }

        protected virtual void OnDestroy()
        {
            m_Player.Event_CharacterThemeChanged.RemoveListener(OnPlayerCharacterThemeChanged);
        }

        protected virtual void OnPlayerCharacterThemeChanged(CharacterTheme theme)
        {
            ApplyTheme(theme);
        }
    }
}
