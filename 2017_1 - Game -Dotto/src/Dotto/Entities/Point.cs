﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(Animator))]
    public class Point : Entity
    {
        public readonly DestructEvent Event_OnDestruct = new DestructEvent();
        public readonly UnityEvent Event_OnRestore = new UnityEvent();

        [SerializeField] public AudioBundle destructionAudioBundle;

        public bool IsDestroyed { get; private set; }

        public Animator Animator { get; private set; }

        private WallMemento m_AwakeState;

        #region Data structures and event handlers

        public class DestructEvent : UnityEvent<DestructEventData>
        {
        }

        public struct DestructEventData
        {
            public IDestroyer Destroyer { get; private set; }
            public ContactPoint2D Contact { get; private set; }
            public Vector2 ContactDirection { get; private set; }
            public Vector2 ContactPoint { get; private set; }

            public DestructEventData(IDestroyer destroyer, ContactPoint2D contact, Vector2 contactPoint, Vector2 contactDirection)
            {
                Destroyer = destroyer;
                Contact = contact;
                ContactDirection = contactDirection;
                ContactPoint = contactPoint;
            }
        }

        #endregion

        protected override void Awake()
        {
            base.Awake();
            Animator = GetComponent<Animator>();

            m_AwakeState = new WallMemento(this);
            UpdateState();
        }

        /*
        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            HandleCollision(collision.gameObject, collision.contacts.FirstOrDefault());
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            ContactPoint2D[] contacts = new ContactPoint2D[0];
            collision.GetContacts(contacts);

            HandleCollision(collision.gameObject, contacts.FirstOrDefault());
        }
        */

        /// <summary>
        /// Разрушает объект
        /// </summary>
        public void Destruct(DestructEventData data)
        {
            if (!Active) return;

            SetState(false);
            DispatchDestructEvents(data);
            PlayDestructionSound();
        }

        /// <summary>
        /// Восстанавливает объект
        /// </summary>
        public void Restore()
        {
            ResetFromMemento(m_AwakeState);
            DispatchRestoreEvents();
        }

        /// <summary>
        /// Событие разрушения объекта
        /// </summary>
        /// <param name="eventData"></param>
        protected virtual void OnDestruct(DestructEventData eventData)
        {
        }

        /// <summary>
        /// Событие восстановления объекта
        /// </summary>
        protected virtual void OnRestore()
        {
        }

        protected void SetState(bool state)
        {
            Active = state;
            UpdateState();
        }

        protected virtual void UpdateState()
        {
            Collider.enabled = Active;
            Animator.SetBool("enabled", Active);
        }

        private void ResetFromMemento(WallMemento memento)
        {
            SetState(memento.Active);
        }

        private void HandleCollision(GameObject collider, ContactPoint2D contact)
        {
            if (!Active || collider == null) return;

            var destroyer = collider.GetComponent<IDestroyer>();
            if (destroyer == null) return;

            Destruct(new DestructEventData(destroyer, contact, contact.point, contact.normal));
        }

        #region Event dispatchers

        /// <summary>
        /// Вызывает события разрушения объекта
        /// </summary>
        /// <param name="eventData"></param>
        private void DispatchDestructEvents(DestructEventData eventData)
        {
            OnDestruct(eventData);
            Event_OnDestruct.Invoke(eventData);
        }

        /// <summary>
        /// Вызывает события восстановления объекта
        /// </summary>
        private void DispatchRestoreEvents()
        {
            OnRestore();
            Event_OnRestore.Invoke();
        }

        private void PlayDestructionSound()
        {
            if (destructionAudioBundle == null) return;
            destructionAudioBundle.Play(Player.Local.Configurations.AudioVolume.Value);
        }

        #endregion
    }
}