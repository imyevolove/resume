﻿using System;
using System.Linq;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class Ball : Entity, ITeleportable, IDestroyer
    {
        [SerializeField] public TrailRenderer trail;
        
        [SerializeField] public Vector2 movementDirection;

        [SerializeField] private float m_ForceMin = 1f;
        public float ForceMin
        {
            get { return m_ForceMin; }
            set
            {
                m_ForceMin = value;
                UpdateForce();
            }
        }

        public CharacterTheme Theme { get; private set; }

        private float m_Force;
        public float Force
        {
            get { return m_Force; }
            set
            {
                m_Force = value;
                UpdateForce();
            }
        }

        private Vector3 m_Position;
        [SerializeField] private AntiSticking m_AntiSticking = new AntiSticking();

        #region Unity methods

        protected override void Awake()
        {
            base.Awake();
            m_Position = Transform.position;
            UpdateForce();
        }

        protected virtual void Update()
        {
            if (!Active) return;
        }

        protected virtual void FixedUpdate()
        {
            if (!Active) return;
            Rigidbody.velocity = movementDirection * Force;
        }

        protected virtual void OnEnable()
        {
            if (trail != null)
                trail.Clear();
        }

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            if (!Active || collision.contacts.Length == 0) return;

            HandleCollision(collision.gameObject, collision.contacts.FirstOrDefault(), true);
        }

        protected virtual void OnCollisionStay2D(Collision2D collision)
        {
            if (!Active || collision.contacts.Length == 0) return;
            
            m_AntiSticking.NextCall(collision.collider.gameObject);

            if (m_AntiSticking.IsEnded)
                Reflect(collision.contacts.FirstOrDefault());
        }

        protected void OnCollisionExit2D(Collision2D collision)
        {
            m_AntiSticking.Reset();
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            ContactPoint2D[] contacts = new ContactPoint2D[0];
            collision.GetContacts(contacts);

            var contact = contacts.FirstOrDefault();
            
            HandleCollision(collision.gameObject, contact, false);
        }

        #endregion

        public void UseTheme(CharacterTheme theme)
        {
            Theme = theme;

            Renderer.sprite = theme.CharacterImage;
            trail.colorGradient = trail.colorGradient.ChangeColor(theme.CharacterColor);
        }

        public void Freeze(Vector2 position)
        {
            this.EnableObject();

            Active = false;
            Collider.enabled = false;

            Transform.position = position;
            Rigidbody.velocity = Vector3.zero;
        }

        public void Launch(Vector2 position, Vector2 direction, float force)
        {
            this.EnableObject();

            Active = true;
            Collider.enabled = true;

            Transform.position = position;
            movementDirection = direction;
            //Force = force;
            Force = m_ForceMin;
        }
        
        public void Teleport(Portal portal)
        {
            if (!Active) return;

            transform.position = portal.transform.position;
            movementDirection = portal.transform.up;

            trail.Clear();
        }

        protected virtual void Translate()
        {
            Rigidbody.MovePosition(m_Position);
        }

        protected void Reflect(ContactPoint2D contact)
        {
            Reflect(contact.normal);
        }

        protected void Reflect(Vector2 normal)
        {
            Debug.DrawRay(Transform.position, movementDirection * 2, Color.red, 2f);
            movementDirection = Vector2.Reflect(movementDirection, normal);
            Debug.DrawRay(Transform.position, movementDirection * 3, Color.green, 2f);
        }

        private void UpdateForce()
        {
            m_Force = Mathf.Max(m_Force, m_ForceMin);
        }

        private bool DestroyPotentialWall(Collision2D collision)
        {
            if (collision.contacts.Length == 0)
                return false;

            return DestroyPotentialWall(collision.gameObject, collision.contacts[0]);
        }

        private bool DestroyPotentialWall(GameObject obj, ContactPoint2D contact)
        {
            if (obj == null) return false;

            var wall = obj.transform.GetComponent<Point>();
            if (wall == null) return false;

            wall.Destruct(new Point.DestructEventData(this, contact, transform.position, -movementDirection));
            return true;
        }

        private void HandleCollision(GameObject other, ContactPoint2D contact, bool reflect)
        {
            if (!Active) return;

            var entity = other.GetComponent<Entity>();
            if (entity == null) return;

            var point = entity as Point;
            if (point == null)
            {
                var wall = entity as Wall;
                if (wall != null)
                {
                    Reflect(contact);
                }
                return;
            }

            DestroyPotentialWall(other, contact);
            
            if (reflect)
                Reflect(contact);
        }
    }
}
