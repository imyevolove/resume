﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(Collider2D))]
    [DisallowMultipleComponent]
    public abstract class Entity : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer m_Renderer;
        public SpriteRenderer Renderer { get { return m_Renderer; } }

        public Rigidbody2D Rigidbody { get; private set; }
        public Collider2D Collider { get; private set; }

        private Transform m_Transform;
        public Transform Transform { get { return GetReference(ref m_Transform); } }

        public readonly ActiveStateChangedEvent Event_OnActiveChanged = new ActiveStateChangedEvent();

        public class ActiveStateChangedEvent : UnityEvent<bool>
        {
        }

        [SerializeField] private bool m_Active = true;
        public bool Active
        {
            get { return m_Active; }
            set
            {
                m_Active = value;
                DispatchActiveStateChangedEvents();
            }
        }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody2D>();
            Collider = GetComponent<Collider2D>();
        }

        private void DispatchActiveStateChangedEvents()
        {
            if (Event_OnActiveChanged == null) return;
            Event_OnActiveChanged.Invoke(Active);
        }

        private T GetReference<T>(ref T value) where T : Component
        {
            if (value == null)
                value = GetComponent<T>();
            return value;
        }
    }
}
