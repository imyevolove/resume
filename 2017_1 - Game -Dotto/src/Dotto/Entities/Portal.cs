﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(Collider2D))]
    public class Portal : MonoBehaviour
    {
        private Collider2D m_Collider;
        public Collider2D Collider
        {
            get
            {
                if (m_Collider == null)
                    m_Collider = GetComponent<Collider2D>();
                return m_Collider;
            }
        }

        [SerializeField] private Portal m_ConnectedPortal;
        public Portal ConnectedPortal
        {
            get { return m_ConnectedPortal; }
            set
            {
                var currentConnectedPortal = m_ConnectedPortal;

                if (value == this)
                    value = null;
                
                if (value == null)
                {
                    RemoveConnection();
                }
                else
                {
                    m_ConnectedPortal = value;
                    if (value.ConnectedPortal != this)
                        value.ConnectedPortal = this;
                }
            }
        }

        public bool IsConnected { get { return ConnectedPortal != null; } }
        public bool IsLocked { get; private set; }

        private Coroutine m_LockTimeoutCoroutine;

        public void RemoveConnection()
        {
            if (m_ConnectedPortal == null) return;
            
            var connectedPortal = m_ConnectedPortal;

            m_ConnectedPortal = null;

            connectedPortal.RemoveConnection();
        }

        public void LockTeleport()
        {
            StartLockTimeout();
            IsLocked = true;
        }

        protected virtual void OnEnable()
        {
            UnlockTeleportImmediate();
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (!IsConnected || IsLocked) return;

            var teleportable = collision.GetComponent<ITeleportable>();
            if (teleportable == null) return;

            teleportable.Teleport(ConnectedPortal);
            ConnectedPortal.LockTeleport();
        }

        protected void UnlockTeleportImmediate()
        {
            IsLocked = false;
        }

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            ConnectedPortal = m_ConnectedPortal;
        }
#endif

        protected virtual void StartLockTimeout()
        {
            StopLockTimeout();
            m_LockTimeoutCoroutine = StartCoroutine(LockTimeout());
        }

        protected virtual void StopLockTimeout()
        {
            if (m_LockTimeoutCoroutine == null) return;
            StopCoroutine(m_LockTimeoutCoroutine);
            m_LockTimeoutCoroutine = null;
        }

        private IEnumerator LockTimeout()
        {
            yield return new WaitForSeconds(0.5f);
            UnlockTeleportImmediate();
        }
    }
}
