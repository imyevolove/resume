﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class Wall : Entity
    {
        [SerializeField] public AudioBundle contactAudio;

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            PlayContactSound();
        }

        private void PlayContactSound()
        {
            if (contactAudio == null) return;
            contactAudio.Play(Player.Local.Configurations.AudioVolume.Value);
        }
    }
}
