﻿using System;
using UnityEngine;

namespace Dotto
{
    using Billing;
    using Dotto.Localization;
    using VK.Unity;

    [Serializable]
    public sealed class Startup : MonoBehaviour
    {
        [SerializeField] private int m_TargetFPS;

        private void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            Application.targetFrameRate = Mathf.Max(m_TargetFPS, 30);

            /// Init IAP
            IAPManager.AddProduct(BillingResources.AdvertisementBlocker.Product);
            IAPManager.Initialize();

            /// Init VK
            VKSDK.Init(() => Debug.Log("VK Initialize status: " + VKSDK.IsInitialized));

            LocalizationManager.SetLocalization(Application.systemLanguage);

            SceneResources.GameScene.LoadScene();
        }
    }
}
