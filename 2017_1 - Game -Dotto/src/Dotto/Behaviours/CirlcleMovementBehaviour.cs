﻿using Dotto;
using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [DisallowMultipleComponent]
    public class CirlcleMovementBehaviour : BaseMovementBehaviour
    {
        [SerializeField] public bool lookAtCenter;
        [SerializeField] public float radius;

        private float m_Delta;
        private Vector2 m_CachedDir;

        public override Bounds Bounds
        {
            get { return new Bounds(Transform.position, new Vector3(radius, radius, 0)); }
        }

        protected virtual void Update()
        {
            if (target == null) return;
            m_Delta += (speed * Time.deltaTime) % 1;

            var angle = (360f * m_Delta + Transform.rotation.eulerAngles.z) * Mathf.Deg2Rad;
            m_CachedDir.Set(
                Mathf.Cos(angle),
                Mathf.Sin(angle)
                );

            var center = (Vector2)Transform.position;
            target.position = center + m_CachedDir * radius;

            if (lookAtCenter)
            {
                var rotZ = Mathf.Atan2(-m_CachedDir.y, -m_CachedDir.x) * Mathf.Rad2Deg;
                target.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotZ);
            }
        }

        protected virtual void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(Transform.position, radius);
        }
    }
}
