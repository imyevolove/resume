﻿using Dotto;
using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [DisallowMultipleComponent]
    public class MovementBehaviour : BaseMovementBehaviour
    {
        [SerializeField] public Vector2 from;
        [SerializeField] public Vector2 to;
        [SerializeField] [Range(0f, 1f)] public float offset = 0f;
        [SerializeField] public bool worldSpace = true;

        public override Bounds Bounds
        {
            get
            {
                var bounds = new Bounds();
                bounds.Encapsulate(NormalizedFrom);
                bounds.Encapsulate(NormalizedTo);

                return bounds;
            }
        }

        [NonSerialized] private float m_Delta;

        protected Vector2 NormalizedFrom { get { return (worldSpace ? from : ((Vector2)Transform.position + from)); } }
        protected Vector2 NormalizedTo { get { return (worldSpace ? to : ((Vector2)Transform.position + to)); } }

        protected virtual void Update()
        {
            if (target == null) return;
            m_Delta = Mathf.PingPong(offset + Time.time * speed, 1);
            target.position = Vector3.Lerp(NormalizedFrom, NormalizedTo, m_Delta);
        }

        protected virtual void OnDrawGizmosSelected()
        {
            var from = NormalizedFrom;
            var to = NormalizedTo;

            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(from, 0.25f);
            Gizmos.DrawSphere(to, 0.25f);
            Gizmos.DrawLine(from, to);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(Vector2.Lerp(from, to, offset), 0.5f);
        }
    }
}
