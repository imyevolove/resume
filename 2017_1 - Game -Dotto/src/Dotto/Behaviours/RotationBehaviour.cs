﻿using Dotto;
using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class RotationBehaviour : BaseBehaviour
    {
        [SerializeField] public float speed;

        public override Bounds Bounds
        {
            get { return new Bounds(target == null ? Vector3.zero : target.position, Vector3.one); }
        }

        protected virtual void Update()
        {
            if (target == null) return;
            target.Rotate(0, 0, speed * Time.deltaTime);
        }
    }
}
