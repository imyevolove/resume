﻿using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public abstract class BaseBehaviour : MonoBehaviour
    {
        [SerializeField] public Transform target;

        private Transform m_Transform;
        public Transform Transform
        {
            get
            {
                if (m_Transform == null)
                    m_Transform = transform;

                return m_Transform;
            }
        }

        public abstract Bounds Bounds { get; }
    }
}
