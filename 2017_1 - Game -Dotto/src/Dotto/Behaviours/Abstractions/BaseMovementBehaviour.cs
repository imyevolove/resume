﻿using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [DisallowMultipleComponent]
    public abstract class BaseMovementBehaviour : BaseBehaviour
    {
        [SerializeField] public float speed;
    }
}
