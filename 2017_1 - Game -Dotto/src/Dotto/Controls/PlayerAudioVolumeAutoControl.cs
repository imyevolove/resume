﻿using System;

namespace Dotto
{
    [Serializable]
    public class PlayerAudioVolumeAutoControl : BaseVolumeAutoControl
    {
        protected override void Start()
        {
            base.Start();
            SetControl(Player.Local.Configurations.AudioVolume);
            UpdateVolumeImmediate();
        }
    }
}
