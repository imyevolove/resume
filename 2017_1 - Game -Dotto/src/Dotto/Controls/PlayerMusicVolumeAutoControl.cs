﻿using System;

namespace Dotto
{
    [Serializable]
    public class PlayerMusicVolumeAutoControl : BaseVolumeAutoControl
    {
        protected override void Start()
        {
            base.Start();
            SetControl(Player.Local.Configurations.MusicVolume);
            UpdateVolumeImmediate();
        }
    }
}
