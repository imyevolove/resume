﻿using System;
using System.Collections;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class BaseVolumeAutoControl : MonoBehaviour
    {
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] public bool fade = false;
        [SerializeField] public float fadeSpeed;

        private PlayerConfigurations.Volume m_Volume;
        public PlayerConfigurations.Volume Volume
        {
            get { return m_Volume; }
            private set
            {
                RemoveVolumeEventListeners(m_Volume);
                AddVolumeEventListeners(value);
                m_Volume = value;
            }
        }

        private Coroutine m_VolumeCoroutine;

        protected virtual void Start()
        {
        }

        protected virtual void OnEnable()
        {
            UpdateVolumeImmediate();
        }

        public void SetControl(PlayerConfigurations.Volume volume)
        {
            Volume = volume;
            UpdateVolume();
        }

        protected void UpdateVolume()
        {
            if (Volume == null || audioSource == null) return;
            if (fade && gameObject.activeSelf) StartChangeVolume();
            else UpdateVolumeImmediate();
        }

        protected void UpdateVolumeImmediate()
        {
            if (audioSource == null || Volume == null) return;
            audioSource.volume = Volume.Value;
        }

        private void OnVolumeValueChanged(float value)
        {
            UpdateVolume();
        }

        private void AddVolumeEventListeners(PlayerConfigurations.Volume volume)
        {
            if (volume == null) return;
            volume.Event_ValueChanged.AddListener(OnVolumeValueChanged);
        }

        private void RemoveVolumeEventListeners(PlayerConfigurations.Volume volume)
        {
            if (volume == null) return;
            volume.Event_ValueChanged.RemoveListener(OnVolumeValueChanged);
        }

        private void StartChangeVolume()
        {
            try
            {
                StopChangeVolume();
                m_VolumeCoroutine = StartCoroutine(LerpVolumeChange(Volume.Value));
            }
            catch (Exception) { }
        }

        private void StopChangeVolume()
        {
            if (m_VolumeCoroutine != null)
            {
                StopCoroutine(m_VolumeCoroutine);
                m_VolumeCoroutine = null;
            }
        }
        
        private IEnumerator LerpVolumeChange(float volume)
        {
            var time = 0f;
            while (audioSource.volume != volume)
            {
                time += Time.deltaTime * fadeSpeed;
                audioSource.volume = Mathf.SmoothStep(audioSource.volume, volume, time);

                yield return null;
            }
        }
    }
}