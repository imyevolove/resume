﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class ScreenAspectController : MonoBehaviour
    {
        [SerializeField] public string calculateFromTag;
        [SerializeField] public float minProjectionSize = 20;
        [SerializeField] public Vector2 screenTicknessOffset;
        [SerializeField] public bool centerCamera;

        protected virtual void Awake()
        {
            GameManager.Instance.Event_OnLevelChanged.AddListener(OnLevelChanged);
        }

        private void OnLevelChanged(StageInfo stage)
        {
            var camera = Camera.main;
            var objects = GameObject.FindGameObjectsWithTag(calculateFromTag).Select(go => go.GetComponent<Renderer>()).Where(r => r != null);
            var behaviours = FindObjectsOfType<BaseBehaviour>();
            var combinedBounds = new Bounds();

            foreach (var obj in objects)
                combinedBounds.Encapsulate(obj.bounds);

            ///Debug.Log(combinedBounds);

            foreach (var behaviour in behaviours)
            {
                ///Debug.LogFormat("Behaviour: {0}, Bounds: {1}", behaviour.GetType(), behaviour.Bounds);
                combinedBounds.Encapsulate(behaviour.Bounds);
            }

            ///Debug.Log(combinedBounds);

            var extents = combinedBounds.extents;
            extents += (Vector3)screenTicknessOffset;
            combinedBounds.extents = extents;

            SetCameraOrthographicSize(camera, combinedBounds, minProjectionSize, centerCamera);
        }

        public static void SetCameraOrthographicSize(Camera camera, Bounds bounds, float minSize = 0, bool applyPosition = false)
        {
            float screenRatio = Screen.width / (float)Screen.height;
            float targetRatio = bounds.size.x / bounds.size.y;
            float orthoSize = minSize;

            if (screenRatio >= targetRatio)
            {
                orthoSize = bounds.size.y / 2;
            }
            else
            {
                float differenceInSize = targetRatio / screenRatio;
                orthoSize = bounds.size.y / 2 * differenceInSize;
            }

            camera.orthographicSize = Mathf.Max(orthoSize, minSize);

            if (applyPosition)
                camera.transform.position = new Vector3(bounds.center.x, bounds.center.y, camera.transform.position.z);
        }
    }
}
