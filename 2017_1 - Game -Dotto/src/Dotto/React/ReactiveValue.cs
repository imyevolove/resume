﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto
{
    [Serializable]
    public class ReactiveValue<T>
    {
        public readonly ValueChangedEvent Event_ValueChanged = new ValueChangedEvent();

        public class ValueChangedEvent : UnityEvent<T>
        {
        }

        [SerializeField] private T m_Value;
        public virtual T Value
        {
            get { return m_Value; }
            set
            {
                if (m_Value.Equals(value)) return;

                m_Value = value;
                Event_ValueChanged.Invoke(value);
            }
        }

        public ReactiveValue()
        {
        }

        public ReactiveValue(T defaultValue)
        {
            m_Value = defaultValue;
        }
    }
}