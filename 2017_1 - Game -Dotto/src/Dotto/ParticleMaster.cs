﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    using Pooling;

    public sealed class ParticleMaster : MonoBehaviour
    {
        private static ParticleMaster m_Instance;
        public static ParticleMaster Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = FindObjectOfType<ParticleMaster>();

                if (m_Instance == null)
                    m_Instance = new GameObject("autocreated_ParticleMaster", typeof(ParticleMaster)).GetComponent<ParticleMaster>();

                return m_Instance;
            }
        }

        private List<ParticleConnection> m_Particles = new List<ParticleConnection>();
        private int m_ParticlesIndex = 0;

        private struct ParticleConnection
        {
            public ParticleSystem particle;
            public ParticleSystem original;
        }

        private void Update()
        {
            for (m_ParticlesIndex = 0; m_ParticlesIndex < m_Particles.Count; m_ParticlesIndex++)
            {
                var conn = m_Particles[m_ParticlesIndex];
                
                if (!conn.particle.isPlaying)
                {
                    UnityPoolMaster.Instance.Release(conn.original, conn.particle);
                    m_Particles.RemoveAt(m_ParticlesIndex);
                }
            }
        }

        public void Play(ParticleSystem particleSystem, Vector3 position, Quaternion rotation)
        {
            var particles = UnityPoolMaster.Instance.GetFree(particleSystem);
            particles.transform.SetPositionAndRotation(position, rotation);
            particles.Play();

            m_Particles.Add(new ParticleConnection
            {
                original = particleSystem,
                particle = particles
            });
        }
    }
}
