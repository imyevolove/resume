﻿using UnityEngine.Events;

namespace Dotto
{
    using Serialization;

    public class PlayerConfigurations : ISerializable
    {
        private Volume m_MusicVolume = new Volume(1f);
        public Volume MusicVolume { get { return m_MusicVolume; } }

        private Volume m_AudioVolume = new Volume(1f);
        public Volume AudioVolume { get { return m_AudioVolume; } }

        public ReactiveValue<bool> m_InvertPointer = new ReactiveValue<bool>(false);
        public ReactiveValue<bool> InvertPointer { get { return m_InvertPointer; } }

        public class Volume : ReactiveValue<float>
        {
            public Volume()
            {
            }

            public Volume(float defaultValue) : base(defaultValue)
            {
            }
        }

        public void Deserialization(SerializationContainer container)
        {
            container.TryDeserialize(MusicVolume, "music_volume");
            container.TryDeserialize(AudioVolume, "audio_volume");
            container.TryDeserialize(InvertPointer, "invert_pointer");
        }

        public void Serialization(SerializationContainer container)
        {
            container["music_volume"] = MusicVolume.Value;
            container["audio_volume"] = AudioVolume.Value;
            container["invert_pointer"] = InvertPointer.Value;
        }
    }
}
