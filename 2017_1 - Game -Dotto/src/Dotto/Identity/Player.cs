﻿using UnityEngine;

namespace Dotto
{
    using Dotto.Ads;
    using Dotto.Database;
    using Dotto.Economy;
    using Dotto.Popups;
    using Dotto.Powerups;
    using Serialization;
    using System;
    using System.Collections.Generic;
    using UnityEngine.Events;

    public class Player : ISerializable
    {
        public readonly CharacterThemeChangedEvent Event_CharacterThemeChanged = new CharacterThemeChangedEvent();

        private const string SaveFilename = "player.sv";
        public readonly static Player Local = new Player();

        public readonly CharacterThemeManager CharacterThemeManager;
        private string m_SelectedCharacterThemeGuid = "";

        public CharacterTheme CurrentCharacterTheme
        {
            get { return ApplicationDatabase.Instance.Characters.Find(m_SelectedCharacterThemeGuid, true); }
            set
            {
                m_SelectedCharacterThemeGuid = value == null ? "" : value.Guid;
                DispatchCharacterThemeChangedEvents();
            }
        }

        private int m_MaxCompletedStageIndex = 0;
        public int MaxCompletedLevel
        {
            get { return m_MaxCompletedStageIndex; }
            set
            {
                m_MaxCompletedStageIndex = Mathf.Max(m_MaxCompletedStageIndex, value);
            }
        }

        private PlayerPowerupManager m_PowerupManager = new PlayerPowerupManager();
        public PlayerPowerupManager PowerupManager { get { return m_PowerupManager; } }

        public readonly PlayerConfigurations Configurations = new PlayerConfigurations();

        public bool PurchaseCharacterTheme(CharacterTheme theme)
        {
            if (CharacterThemeManager.IsUnlocked(theme)) return false;

            if (theme.price.kind == CurrencyKind.Coins)
            {
                if (theme.price.value > Money.Value) return false;

                Money.Value -= theme.price.value;
                CharacterThemeManager.Unlock(theme);
            }
            else
            {
                Popup popup = PopupManager.Instance.watchAdsPopup;

                var operation = popup.Show();
                operation.Event_Completed += (o) => 
                {
                    if (o.IsCanceled)
                    {
                        Debug.LogWarning("Canceled");
                    }
                    else
                    {
                        AdvertisementManager.Instance.ShowRewardedVideo((bool finished) =>
                        {
                            if (finished)
                            {
                                Local.CharacterThemeManager.InvestToProgress(theme, 1);
                                Local.Save();
                            }
                            else
                            {
                                Debug.LogWarning("Video ads not finished");
                            }
                        });
                    }
                };

                return false;
            }
            
            return true;
        }

        private readonly ReactiveValue<bool> m_AdvertisementDisabled = new ReactiveValue<bool>(false);
        public ReactiveValue<bool> AdvertisementDisabled { get { return m_AdvertisementDisabled; } }

        public readonly Money Money = new Money();

        public class CharacterThemeChangedEvent : UnityEvent<CharacterTheme>
        {
        }

        protected bool IsNewPlayer { get; private set; }

        private bool m_StartKitLock = false;

        public Player()
        {
            CharacterThemeManager = new CharacterThemeManager();
            CharacterThemeManager.Event_OnUnlock.AddListener((theme) => { Save(); });

            PowerupManager.Add(CreatePowerup(PowerupId.SkipLevel));
        }

        public bool StageIsAvailable(StageInfo stage)
        {
            if (stage == null) return false;
            return stage.Index <= MaxCompletedLevel;
        }

        public void Deserialization(SerializationContainer container)
        {
            /// Try deserialize complete stages index
            try
            {
                container.Deserialize("max_completed_stage", ref m_MaxCompletedStageIndex);
            }
            catch (Exception)
            {
            }
            
            /// Try deserialize selected character theme guid
            try
            {
                container.Deserialize("selected_character_theme", ref m_SelectedCharacterThemeGuid);
                DispatchCharacterThemeChangedEvents();
            }
            catch (Exception)
            {
            }

            /// Try deserialize unlocked characters list
            try
            {
                if (container.KeyValueIs<SerializationContainer>("characters_manager"))
                    CharacterThemeManager.Deserialization((SerializationContainer)container["characters_manager"]);
            }
            catch (Exception)
            {
            }

            /// Try deserialize player money
            try
            {
                DeserializeReactiveValue(container, "player_money", Money);
            }
            catch (Exception)
            {
            }

            /// Try deserialize adblock state
            try
            {
                DeserializeReactiveValue(container, "ads_lock", m_AdvertisementDisabled);
            }
            catch (Exception)
            {
            }

            /// Try deserialize player configurations
            try
            {
                if (container.KeyValueIs<SerializationContainer>("player_configs"))
                    Configurations.Deserialization((SerializationContainer)container["player_configs"]);
            }
            catch (Exception)
            {
            }

            /// Try deserialize powerups
            try
            {
                if (container.KeyValueIs<SerializationContainer>("powerups"))
                    PowerupManager.Deserialization((SerializationContainer)container["powerups"]);
            }
            catch (Exception)
            {
            }
        }

        public void Serialization(SerializationContainer container)
        {
            container["selected_character_theme"] = m_SelectedCharacterThemeGuid;
            container["max_completed_stage"] = m_MaxCompletedStageIndex;
            container["ads_lock"] = m_AdvertisementDisabled.Value;
            container["player_money"] = Money.Value;
            container["player_configs"] = SerializationContainer.Serialize(Configurations);
            container["characters_manager"] = SerializationContainer.Serialize(CharacterThemeManager);
            container["powerups"] = SerializationContainer.Serialize(PowerupManager);
        }

        public void Save()
        {
            Storage.Save(this, SaveFilename);
            OnSave();
        }

        public void Load()
        {
            IsNewPlayer = !Storage.Load(this, SaveFilename);
            OnLoad();
        }

        protected virtual void OnLoad()
        {
            if (IsNewPlayer && !m_StartKitLock)
            {
                m_StartKitLock = true;
                GiveStartKit();
            }
        }

        protected virtual void OnSave()
        {

        }
        
        private void DispatchCharacterThemeChangedEvents()
        {
            if (Event_CharacterThemeChanged != null)
                Event_CharacterThemeChanged.Invoke(CurrentCharacterTheme);
        }

        private void DeserializeReactiveValue<T>(SerializationContainer container, string key, ReactiveValue<T> react)
        {
            var value = react.Value;
            container.Deserialize(key, ref value);
            react.Value = value;
        }

        private void GiveStartKit()
        {
            foreach (var powerup in PowerupManager.Powerups)
                powerup.Powerup.Shots++;
        }

        private static MarkedAppPowerup CreatePowerup(PowerupId id)
        {
            var price = new Price();
            var cfg = ApplicationDatabase.Instance.Powerups.Find(id);
            if(cfg != null)
                price = cfg.price;

            var powerup = AppPowerupFactory.Create(id, price);
            powerup.icon = cfg == null ? null : cfg.icon;
            powerup.Name = cfg == null ? "" : cfg.name;

            return new MarkedAppPowerup(id, powerup);
        }
    }
}