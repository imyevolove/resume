﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class AntiSticking
    {
        [SerializeField] public int stickingMaxCalls = 2;
        public GameObject Trigger { get; private set; }

        private int m_Counter;

        public bool IsEnded { get; private set; }

        public void NextCall(GameObject trigger)
        {
            if (Trigger != trigger)
            {
                Reset();
                Trigger = trigger;
                return;
            }
            
            m_Counter++;

            if (m_Counter >= stickingMaxCalls)
            {
                Reset();
                IsEnded = true;
                return;
            }
        }

        public void Reset()
        {
            m_Counter = 0;
            IsEnded = false;
            Trigger = null;
        }
    }
}
