﻿using System;
using System.IO;
using UnityEngine;
using System.Xml.Serialization;
using System.Xml;

namespace Dotto
{
    using Serialization;

    public static class Storage
    {

        /// <summary>
        /// Сохраняет объект в файл
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static void Save<T>(T target, string filename)
            where T : ISerializable
        {
            var filepath = MakeFilePath(filename);

            SerializationContainer container = new SerializationContainer();
            target.Serialization(container);
            SaveText(filepath, container.ToXml());
        }

        /// <summary>
        /// Загружает объект динамического типа поимени файла из persistentDataPath
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool Load<T>(T target, string filename)
            where T : ISerializable
        {
            var filepath = MakeFilePath(filename);

            string data = LoadText(filepath);
            target.Deserialization(SerializationContainer.Deserialize(data));
            
            return !string.IsNullOrEmpty(data);
        }

        /// <summary>
        /// Состовляет из имени файла конечный путь до файла
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static string MakeFilePath(string filename)
        {
            return Path.Combine(Application.persistentDataPath, filename);
        }

        ////////////////////////////////////////////////////////////
        public static string LoadText(string filename)
        {
            try
            {
                var text = File.ReadAllText(filename);
                text = EncryptionHelper.Decrypt(text);

                Debug.LogFormat("File was loaded from : {0}", filename);
                return text;
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return "";
            }
        }

        ////////////////////////////////////////////////////////////
        public static void SaveText(string filename, string text)
        {
            try
            {
                CreateDirectories(filename);
                
                text = EncryptionHelper.Encrypt(text);
                File.WriteAllText(filename, text);

                Debug.LogFormat("File was saved to: {0}", filename);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }
        }

        ////////////////////////////////////////////////////////////
        public static bool SaveObject(object obj, string filename)
        {
            var filepath = MakeFilePath(filename);

            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());

                XmlWriterSettings settings = new XmlWriterSettings
                {
                    NewLineHandling = NewLineHandling.None,
                    Indent = false
                };

                StringWriter stringWriter = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings);
                xmlSerializer.Serialize(xmlWriter, obj);

                SaveText(filepath, xmlSerializer.ToString());

                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return false;
            }
        }

        ////////////////////////////////////////////////////////////
        public static bool LoadObject<T>(string filename, out T output)
        {
            var filepath = MakeFilePath(filename);

            try
            {
                var text = LoadText(filepath);

                if (string.IsNullOrEmpty(text))
                {
                    output = default(T);
                    return false;
                }

                var serializer = new XmlSerializer(typeof(T));

                using (TextReader reader = new StringReader(text))
                    output = (T)serializer.Deserialize(reader);

                return true;
            }
            catch (Exception e)
            {
                output = default(T);

                Debug.LogError(e);

                return false;
            }
        }

        ////////////////////////////////////////////////////////////
        private static void CreateDirectories(string path)
        {
            path = Path.GetDirectoryName(path);
            Directory.CreateDirectory(path);
        }
    }
}
