﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto
{
    public interface ICharacterThemeManager
    {
        CharacterThemeEvent Event_OnUnlock { get; }

        CharacterTheme[] Unlocked { get; }
        bool IsUnlocked(CharacterTheme theme);
        void Unlock(CharacterTheme theme);
    }
}
