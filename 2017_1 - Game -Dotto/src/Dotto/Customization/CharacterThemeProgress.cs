﻿using Dotto.Database;
using Dotto.Economy;
using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.Customization
{
    [Serializable]
    public class CharacterThemeProgress : ISerializable
    {
        public readonly UnityEvent Event_Unlocked = new UnityEvent();
        public readonly UnityEvent Event_AnyChange = new UnityEvent();

        public CharacterTheme Theme { get; private set; }

        private bool m_IsUnlocked = false;
        public bool IsUnlocked
        {
            get { return Theme != null && Theme.price.value <= 0 ? true : m_IsUnlocked; }
            set { m_IsUnlocked = value; }
        }

        public ProductInvestment Investment { get; private set; }

        public CharacterThemeProgress(CharacterTheme theme)
        {
            IsUnlocked = false;
            Theme = theme;

            Investment = new ProductInvestment();
            Investment.Event_InvestmentCompleted.AddListener(OnInvestmentCompleted);
            Investment.Event_InvestmentValueChanged.AddListener(OnInvestmentValueChanged);

            UpdateInvestmentPriceFromTheme();
        }

        public void Unlock()
        {
            if (IsUnlocked) return;
            IsUnlocked = true;

            Event_AnyChange.Invoke();
            Event_Unlocked.Invoke();
        }

        public void Serialization(SerializationContainer container)
        {
            container["theme_id"] = Theme.Guid;
            container["unlocked"] = IsUnlocked;
            container["investment"] = Investment.Investment;
        }

        public void Deserialization(SerializationContainer container)
        {
            /// Try deserialize theme guid
            try
            {
                var guid = "";
                container.Deserialize("theme_id", ref guid);
                Theme = ApplicationDatabase.Instance.Characters.Find(guid);
                UpdateInvestmentPriceFromTheme();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            /// Try deserialize investment
            try
            {
                var investmentValue = 0;
                container.Deserialize("investment", ref investmentValue);

                if (investmentValue != 0)
                {
                    Investment.Investment = investmentValue;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            /// Try deserialize unlock status
            try
            {
                var status = false;
                container.Deserialize("unlocked", ref status);
                IsUnlocked = status;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        protected virtual void OnInvestmentCompleted(int weight)
        {
            if (weight <= 0) return;
            Unlock();

            Event_AnyChange.Invoke();
        }

        protected virtual void OnInvestmentValueChanged(int value)
        {
            Event_AnyChange.Invoke();
        }

        private void UpdateInvestmentPriceFromTheme()
        {
            Investment.Price = Theme != null ? Theme.price : default(Price);
        }
    }
}
