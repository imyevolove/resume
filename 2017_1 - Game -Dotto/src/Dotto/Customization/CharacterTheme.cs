﻿using Dotto.Economy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class CharacterTheme : ISerializationCallbackReceiver
    {
        [SerializeField] private string m_Guid;
        public string Guid { get { return m_Guid; } }

        [SerializeField] private Sprite m_CharacterImage;
        public Sprite CharacterImage
        {
            get { return m_CharacterImage; }
            set { m_CharacterImage = value; }
        }

        [SerializeField] private Color m_CharacterColor;
        public Color CharacterColor
        {
            get { return m_CharacterColor; }
            set { m_CharacterColor = value; }
        }
        
        [SerializeField] public Price price;

        public CharacterTheme(Sprite image, Color color) : this(image, color, default(Price))
        {
        }

        public CharacterTheme(Sprite image, Color color, Price price)
        {
            m_Guid = System.Guid.NewGuid().ToString();
            this.price = price;
            m_CharacterImage = image;
            m_CharacterColor = color;
        }

        public static CharacterTheme Create()
        {
            return new CharacterTheme(null, Color.white);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            if (!IsValidGuid())
                ChangeGuid();
        }

        private bool IsValidGuid()
        {
            return !string.IsNullOrEmpty(m_Guid);
        }

        private void ChangeGuid()
        {
            m_Guid = System.Guid.NewGuid().ToString();
        }
    }
}
