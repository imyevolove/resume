﻿using Dotto.Customization;
using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public class CharacterThemeManager : ICharacterThemeManager, ISerializable
    {
        private readonly CharacterThemeEvent m_Event_OnUnlock = new CharacterThemeEvent();
        public CharacterThemeEvent Event_OnUnlock { get { return m_Event_OnUnlock; } }

        private List<CharacterThemeProgress> m_UnlockedThemeProgresses = new List<CharacterThemeProgress>();

        public CharacterTheme[] Unlocked { get { return m_UnlockedThemeProgresses.Where(p => p.IsUnlocked).Select(p => p.Theme).ToArray(); } }

        public bool IsUnlocked(CharacterTheme theme)
        {
            return m_UnlockedThemeProgresses.Exists(progress => progress.Theme == theme && progress.IsUnlocked);
        }

        public void Unlock(CharacterTheme theme)
        {
            var progress = GetProgress(theme);
            if (progress == null) return;
            progress.Unlock();
        }
        
        public CharacterThemeProgress GetProgress(CharacterTheme theme)
        {
            if (theme == null) return null;
            var progress = m_UnlockedThemeProgresses.FirstOrDefault(p => p.Theme == theme);

            if (progress == null)
            {
                progress = new CharacterThemeProgress(theme);
                RegisterProgress(progress);
            }

            return progress;
        }

        public void InvestToProgress(CharacterTheme theme, int value)
        {
            var progress = GetProgress(theme);
            if (progress == null)
            {
                Debug.LogWarning("Can't invest to progress");
                return;
            }
            progress.Investment.Investment += Mathf.Abs(value);
        }

        public void Serialization(SerializationContainer container)
        {
            List<SerializationContainer> data = new List<SerializationContainer>();

            foreach (var progress in m_UnlockedThemeProgresses)
            {
                if (progress.Theme == null) continue;

                var itemData = new SerializationContainer();
                progress.Serialization(itemData);

                data.Add(itemData);
            }

            container["unlocked_themes"] = data;
        }

        public void Deserialization(SerializationContainer container)
        {
            try
            {
                if (container.KeyValueIs<List<SerializationContainer>>("unlocked_themes"))
                {
                    var data = container["unlocked_themes"] as List<SerializationContainer>;

                    foreach (var item in data)
                    {
                        var themeProgress = new CharacterThemeProgress(null);
                        themeProgress.Deserialization(item);
                        
                        if (themeProgress.Theme == null || m_UnlockedThemeProgresses.Exists(up => up.Theme == themeProgress.Theme)) continue;
                        RegisterProgress(themeProgress);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        
        private void RegisterProgress(CharacterThemeProgress progress)
        {
            m_UnlockedThemeProgresses.Add(progress);
            progress.Event_Unlocked.AddListener(() => 
            {
                OnThemeUnlocked(progress);
            });
        }

        protected virtual void OnThemeUnlocked(CharacterThemeProgress progress)
        {
            m_Event_OnUnlock.Invoke(progress.Theme);
        }
    }
}
