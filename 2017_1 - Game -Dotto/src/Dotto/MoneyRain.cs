﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(AudioPlayer))]
    public class MoneyRain : MonoBehaviour
    {
        [SerializeField] public float afterDelay;
        [SerializeField] public float delayTime;
        [SerializeField] public int moneyDivider;
        [SerializeField] public ParticleSystem particles;
        [SerializeField] public AudioBundle sfxBundle;

        private int m_Money;
        private Transform[] m_Positions;
        private Action m_EndCallback;
        private Coroutine m_AnimCoroutine;

        private AudioPlayer m_AudioPlayer;

        protected virtual void Awake()
        {
            m_AudioPlayer = GetComponent<AudioPlayer>();
            m_AudioPlayer.Bundle = sfxBundle;
        }

        public void Play(int money, Transform[] positions, Action endCallback)
        {
            m_Money = money;
            m_Positions = positions;

            DispatchEndCallback();
            m_EndCallback = endCallback;

            StartAnimation();
        }

        private void Emit(Vector3 position, int count)
        {
            particles.transform.position = position;
            particles.Emit(count);

            m_AudioPlayer.PlayNext();
        }

        private void DispatchEndCallback()
        {
            if (m_EndCallback == null) return;
            m_EndCallback.Invoke();
            m_EndCallback = null;
        }

        private IEnumerator MoneyAnim()
        {
            var counter = Mathf.CeilToInt(m_Money / moneyDivider);
            var timer = 0f;
            var positionIndex = 0;
            Transform position = transform;

            while (counter > 0)
            {
                if (timer >= delayTime)
                {
                    timer = 0f;
                    counter--;

                    if (m_Positions.Length > 0)
                    {
                        positionIndex = counter % m_Positions.Length;
                        position = m_Positions[positionIndex];
                    }

                    Emit(position.position, 1);
                }
                else
                {
                    timer += Time.deltaTime;
                }

                yield return null;
            }

            yield return new WaitForSeconds(afterDelay);
            DispatchEndCallback();
        }

        private void StartAnimation()
        {
            StopAnimation();
            m_AnimCoroutine = StartCoroutine(MoneyAnim());
        }

        private void StopAnimation()
        {
            if (m_AnimCoroutine == null) return;
            StopCoroutine(m_AnimCoroutine);
            m_AnimCoroutine = null;
        }
    }
}
