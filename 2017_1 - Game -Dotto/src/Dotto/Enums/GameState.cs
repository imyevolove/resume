﻿namespace Dotto
{
    public enum GameState
    {
        Ready,
        Running,
        CutScene
    }
}