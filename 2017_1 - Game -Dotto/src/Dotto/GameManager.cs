﻿using Dotto.UI;
using System;
using UnityEngine;

namespace Dotto
{
    using Dotto.Areas;
    using Dotto.Economy;
    using System.Linq;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;
    using Utils;

    [Serializable]
    public sealed class GameManager : MonoSingleton<GameManager>
    {
        public readonly StageEvent Event_OnLevelChanged = new StageEvent();
        public readonly StageEvent Event_OnLevelLoaded = new StageEvent();
        public readonly UnityEvent Event_OnGameOver = new UnityEvent();
        public readonly StageEvent Event_OnLevelCompleted = new StageEvent();
        public readonly StageEvent Event_OnLevelRestart = new StageEvent();

        [NonSerialized] private GameState m_GameState;
        public GameState GameState
        {
            get { return m_GameState; }
            private set
            {
                m_GameState = value;
                DispatchGameStateChangedEvent();
            }
        }

        public bool IsLocked { get { return GameState == GameState.CutScene; } }
        public bool IsPlaying { get { return GameState == GameState.Running; } }

        [SerializeField] public MoneyRain MoneyRain;
        [SerializeField] public BaseRewardGenerator RewardGenerator;
        [SerializeField] public StageDatabase stageDatabase;
        [SerializeField] public Ball ball;

        [SerializeField] private BallLauncher m_BallLauncher;
        public BallLauncher BallLauncher { get { return m_BallLauncher; } }

        [SerializeField] private ScreenRestriction m_ScreenRestriction;
        public ScreenRestriction ScreenRestriction { get { return m_ScreenRestriction; } }

        [SerializeField] private StageTransitionUIHandler m_StageTransitionUIHandler;
        public StageTransitionUIHandler StageTransitionUIHandler { get { return m_StageTransitionUIHandler; } }

        public StageInfo CurrentStage { get { return m_StageHandler == null ? null : m_StageHandler.StageInfo; } }
        public Point[] StageWalls { get { return m_StageHandler == null ? new Point[0] { } : m_StageHandler.Walls; } }

        public readonly AreaCollector AreaCollector = new AreaCollector();

        private StageHandler m_StageHandler;
        private bool CanRequestStageChange { get { return m_StageHandler == null || !m_StageHandler.IsLocked; } }
        private bool m_AutoResetStage = true;

        ///private IStageAttemptsHistory m_AttemptsHistory = new StageAttemptsHistory();
        private int m_LevelAttempts = 0;

        #region Data structures and events

        public class StageEvent : UnityEvent<StageInfo>
        {
        }

        #endregion

        protected override void Awake()
        {
            base.Awake();

            BallLauncher.ball = ball;
            m_ScreenRestriction.Event_OnAnyObjectExit.AddListener(OnScreenAnyObjectExit);

            Player.Local.Load();
        }

        private void Start()
        {
            LoadStage(stageDatabase.FindByIndex(Player.Local.MaxCompletedLevel));
        }

        public void SkipLevel()
        {
            CompleteLevel();
        }

        public void LoadStage(StageInfo stage)
        {
            if (!CanRequestStageChange)
            {
                Debug.LogWarning("Can't load new stage. Stage handler is locked");
                return;
            }

            if (m_StageHandler != null)
            {
                var operation = m_StageHandler.UnloadScene();
                operation.completed += (ao) =>
                {
                    m_StageHandler.Dispose();
                    InitializeStage(stage);
                };
            }
            else
            {
                InitializeStage(stage);
            }
        }

        public void LaunchBall()
        {
            if (GameState != GameState.Ready) return;
            if (!BallLauncher.Launch()) return;

            GameState = GameState.Running;
        }
        
        public void Restart()
        {
            m_LevelAttempts++;
            PrepareLevel();
            DispatchLevelRestartEvents();
        }
        
        private void OnGameOver()
        {
            Restart();
        }

        private void OnLevelRestart()
        {
        }

        private void OnLevelChanged(StageInfo stage)
        {
        }

        private void OnLevelLoaded(StageInfo stage)
        {
            AreaCollector.UpdateReferences();
        }

        private void OnLevelCompleted()
        {
        }

        private void OpenStageTransitionPanel(int level)
        {
            StageTransitionUIHandler.Setup(level);
            StageTransitionUIHandler.Show(() =>
            {
                LoadNextStage();
                m_AutoResetStage = true;
            });
        }

        private void InitializeStage(StageInfo stage)
        {
            m_StageHandler = new StageHandler(stage);
            m_StageHandler.Event_Completed.AddListener(CompleteLevel);
            m_StageHandler.LoadScene((ao) =>
            {
                PrepareLevel();
                DispatchLevelLoadedEvents(stage);
                DispatchLevelChangedEvents(stage);
            });
        }

        private void LoadNextStage()
        {
            if (m_StageHandler == null)
            {
                Debug.LogError("Can't load next stage. Stage handler must be defined");
                return;
            }

            var next = stageDatabase.FindNext(m_StageHandler.StageInfo, stageDatabase.Stages[0]);
            LoadStage(next);
        }

        private void OnBallOutOfScreenBounds(Ball ball)
        {
            if (m_StageHandler.IsLocked || !m_AutoResetStage) return;
            DispatchGameOverEvents();
        }

        private void OnGameStateChanged(GameState state)
        {
        }

        private void OnScreenAnyObjectExit(GameObject go)
        {
            var ball = go.GetComponent<Ball>();
            if (ball != this.ball) return;
            if (m_GameState != GameState.Running) return;

            OnBallOutOfScreenBounds(ball);
        }
        
        private void DisablePlayer()
        {
            ball.DisableObject();
        }

        private void PrepareLevel()
        {
            DisablePlayer();

            if (m_StageHandler != null)
                m_StageHandler.Reset();

            GameState = GameState.Ready;
        }

        private void CompleteLevel()
        {
            DispatchLevelCompletedEvents();

            var stageIndex = CurrentStage.Index;
            var level = stageIndex + 1;
            var player = Player.Local;

            var isRepeateTime = stageIndex < player.MaxCompletedLevel;
            var reward = RewardGenerator.Generate(CurrentStage.Index, StageWalls.Length, m_LevelAttempts, isRepeateTime);

            player.Money.Value += reward;
            player.MaxCompletedLevel = level;
            player.Save();

            m_LevelAttempts = 0;
            m_AutoResetStage = false;

            GameState = GameState.CutScene;

            MoneyRain.Play(reward, new Transform[] { ball.Transform }, () =>
            {
                OpenStageTransitionPanel(level);
            });
        }

        #region Event dispatchers

        private void DispatchGameStateChangedEvent()
        {
            OnGameStateChanged(GameState);
        }

        private void DispatchLevelLoadedEvents(StageInfo stage)
        {
            OnLevelLoaded(stage);
            Event_OnLevelLoaded.Invoke(stage);
        }

        private void DispatchLevelChangedEvents(StageInfo stage)
        {
            OnLevelChanged(stage);
            Event_OnLevelChanged.Invoke(stage);
        }

        private void DispatchLevelCompletedEvents()
        {
            OnLevelCompleted();
            Event_OnLevelCompleted.Invoke(CurrentStage);
        }

        private void DispatchLevelRestartEvents()
        {
            OnLevelRestart();
            Event_OnLevelRestart.Invoke(CurrentStage);
        }

        private void DispatchGameOverEvents()
        {
            OnGameOver();
            Event_OnGameOver.Invoke();
        }

        #endregion
    }
}
