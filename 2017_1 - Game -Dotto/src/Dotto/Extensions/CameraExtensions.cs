﻿using UnityEngine;

namespace Dotto
{
    public static class CameraExtensions
    {
        public static Bounds OrthographicBounds(this Camera camera)
        {
            
            float screenAspect = (float)camera.scaledPixelWidth / (float)camera.scaledPixelHeight;
            float cameraHeight = camera.orthographicSize * 2;

            Bounds bounds = new Bounds(
                camera.transform.position,
                new Vector3(cameraHeight * screenAspect, cameraHeight, 0));

            return bounds;
        }
    }
}
