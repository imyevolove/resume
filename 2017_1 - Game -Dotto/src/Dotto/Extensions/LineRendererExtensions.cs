﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public static class LineRendererExtensions
    {
        /// <summary>
        /// Добавляет дополнительную позицию к линии и возвращает ее индекс
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static int NextPosition(this LineRenderer line)
        {
            return line.positionCount++;
        }
    }
}
