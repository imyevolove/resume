﻿using UnityEngine;

namespace Dotto
{
    public static class ComponentExtensions
    {
        public static void DisableObject(this Component component)
        {
            component.gameObject.SetActive(false);
        }

        public static void EnableObject(this Component component)
        {
            component.gameObject.SetActive(true);
        }
    }
}
