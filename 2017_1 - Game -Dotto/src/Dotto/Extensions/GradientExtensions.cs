﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public static class GradientExtensions
    {
        public static Gradient ChangeColor(this Gradient source, Color color)
        {
            Gradient gradient = new Gradient();

            var colorKeys = source.colorKeys.Select(k => new GradientColorKey(color, k.time)).ToArray();
            gradient.SetKeys(colorKeys, source.alphaKeys);

            return gradient;
        }
    }
}
