﻿using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public static class SerializationContainerExtensions
    {
        public static void TryDeserialize<T>(this SerializationContainer container, ReactiveValue<T> option, string containerKey)
        {
            try
            {
                Deserialize(container, option, containerKey);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public static void Deserialize<T>(this SerializationContainer container, ReactiveValue<T> option, string containerKey)
        {
            if (!container.HasKey(containerKey)) return;

            T value = default(T);
            container.Deserialize(containerKey, ref value);
            option.Value = value;
        }
    }
}
