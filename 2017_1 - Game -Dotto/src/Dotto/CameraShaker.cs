﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(Camera))]
    public class CameraShaker : MonoBehaviour
    {
        [SerializeField] public CameraShakeOptions options;
        [SerializeField] private bool m_ShakeDebug = false;

        public bool Shaking { get { return m_ShakeCoroutine != null; } }

        private Camera m_Camera;
        public Camera Camera
        {
            get
            {
                if (m_Camera == null)
                    m_Camera = GetComponent<Camera>();

                return m_Camera;
            }
        }

        private Coroutine m_ShakeCoroutine;

        [Serializable]
        public struct CameraShakeOptions
        {
            [SerializeField] public float power;
            [SerializeField] public float duration;
            [SerializeField] public float delay;

            public void SetPower(float power)
            {
                this.power = power;
            }

            public void SetDuration(float duration)
            {
                this.duration = duration;
            }
        }

        protected virtual void OnEnable()
        {
            StopShake();
        }

        protected virtual void OnDisable()
        {
            StopShake();
        }

        protected virtual void Update()
        {
            if (m_ShakeDebug)
            {
                m_ShakeDebug = false;
                Shake();
            }
        }

        public void Shake()
        {
            Shake(options);
        }

        public void Shake(CameraShakeOptions options)
        {
            StopShake();
            StartCoroutine(ShakeEnumerator(options));
        }

        public void StopShake()
        {
            if (m_ShakeCoroutine == null) return;
            StopCoroutine(m_ShakeCoroutine);
            m_ShakeCoroutine = null;
        }

        private IEnumerator ShakeEnumerator(CameraShakeOptions options)
        {
            var counter = 0f;
            var delayCounter = 0f;
            var cTransform = transform;
            var originalPosition = cTransform.position;
            var direct = Vector3.zero;
            var movementDirect = Vector3.zero;
            var movedTo = Vector3.zero;

            while(counter < options.duration)
            {
                counter += Time.deltaTime;
                delayCounter += Time.deltaTime;

                if (delayCounter >= options.delay)
                {
                    delayCounter = 0f;

                    originalPosition = cTransform.position - movedTo;
                    direct = Random.insideUnitCircle * options.power;
                    movementDirect = direct / options.delay;

                    movedTo = Vector3.zero;
                    cTransform.position = originalPosition;
                }
                else
                {
                    movedTo += movementDirect * Time.deltaTime;
                    cTransform.position += movementDirect * Time.deltaTime;
                }
                
                yield return null;
            }

            originalPosition = cTransform.position - movedTo;
            cTransform.position = originalPosition;

            StopShake();
        }
    }
}
