﻿using UnityEngine;
using UnityEngine.Events;

namespace Dotto
{
    [SerializeField]
    public class BallLauncher : MonoBehaviour
    {
        [SerializeField] public Ball ball;
        [SerializeField] public Vector2 launchPosition;
        [SerializeField] public Vector2 launchDirection;
        [SerializeField] public float launchPower;

        public readonly StateChangedEvent Event_OnStateChanged = new StateChangedEvent();

        private LauncherState m_State;
        public LauncherState State
        {
            get { return m_State; }
            set
            {
                m_State = value;
                DispatchStateChangedEvent();
            }
        }
        
        public enum LauncherState
        {
            Ready,
            Prepare
        }

        public class StateChangedEvent : UnityEvent<LauncherState>
        {
        }

        public void PrepareToLaunch()
        {
            launchDirection = Vector2.zero;
            launchPower = 0;

            State = LauncherState.Prepare;
            ball.Freeze(launchPosition);
        }

        public bool Launch()
        {
            if (State != LauncherState.Prepare)
            {
                Debug.LogWarning("Can't launch ball. Launcher is not ready");
                return false;
            }

            if (launchDirection.magnitude < 0.05f) return false;

            ball.Launch(launchPosition, launchDirection, launchPower);
            State = LauncherState.Ready;

            return true;
        }

        private void DispatchStateChangedEvent()
        {
            if (Event_OnStateChanged == null) return;
            Event_OnStateChanged.Invoke(State);
        }
    }

}