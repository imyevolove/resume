﻿using System;
using System.Linq;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(AudioSource))]
    public class AudioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioBundle bundle;
        public AudioBundle Bundle
        {
            get { return bundle; }
            set
            {
                bundle = value;
                UpdateClipPipeline();
            }
        }

        protected AudioSource AudioSource { get; private set; }
        protected AudioClip[] m_ClipPipeline;
        protected int m_ClipIndex = 0;

        [SerializeField] private bool m_PlayOnAwake = false;
        [SerializeField] private bool m_AutoNext = true;
        
        protected virtual void Awake()
        {
            AudioSource = GetComponent<AudioSource>();
            UpdateClipPipeline();

            if (m_PlayOnAwake)
                Play();
        }

        protected virtual void Update()
        {
            if (m_AutoNext)
            {
                if (AudioSource.clip == null)
                {
                    PlayNext();
                }
                else if(!AudioSource.isPlaying)
                {
                    PlayNext();
                }
            }
        }

        public virtual void Play()
        {
            Stop();

            var index = m_ClipIndex % m_ClipPipeline.Length;
            var clip = m_ClipPipeline[index];

            AudioSource.clip = clip;
            AudioSource.Play();
        }

        public virtual void PlayNext()
        {
            m_ClipIndex++;
            m_ClipIndex = m_ClipIndex % m_ClipPipeline.Length;

            Play();
        }

        public virtual void Stop()
        {
            AudioSource.Stop();
        }

        private void UpdateClipPipeline()
        {
            m_ClipPipeline = Bundle == null 
                ? new AudioClip[0] 
                : Bundle.Randomized.ToArray();
        }
    }
}
