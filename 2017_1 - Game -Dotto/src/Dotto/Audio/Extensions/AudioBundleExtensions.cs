﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    public static class AudioBundleExtensions
    {
        public static void PlayAtPoint(this AudioBundle bundle, Vector3 position, float volume = 1)
        {
            AudioSource.PlayClipAtPoint(bundle.RandomClip, position, volume);
        }

        public static void Play(this AudioBundle bundle, float volume = 1)
        {
            AudioSource.PlayClipAtPoint(bundle.RandomClip, Camera.main.transform.position, volume);
        }
    }
}
