﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dotto
{
    [CreateAssetMenu(menuName = "Audio/Bundle")]
    public class AudioBundle : ScriptableObject
    {
        [SerializeField] protected List<AudioClip> m_Clips = new List<AudioClip>();
        public List<AudioClip> Clips { get { return m_Clips; } }

        public IEnumerable<AudioClip> Randomized { get { return Clips.OrderBy(c => Random.value); } }
        public AudioClip RandomClip { get { return Clips[Random.Range(0, Clips.Count)]; } }
    }
}
