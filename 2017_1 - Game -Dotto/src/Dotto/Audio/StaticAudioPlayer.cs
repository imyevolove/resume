﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(AudioSource))]
    public class StaticAudioPlayer : MonoBehaviour
    {
        private static List<StaticAudioPlayer> m_Players = new List<StaticAudioPlayer>();

        [SerializeField] public AudioBundle audioBundle;

        private AudioSource m_AudioSource;
        public AudioSource AudioSource
        {
            get
            {
                if (m_AudioSource == null)
                    m_AudioSource = GetComponent<AudioSource>();

                return m_AudioSource;
            }
        }

        protected virtual void Awake()
        {
            m_Players.Add(this);
        }

        protected virtual void OnDestroy()
        {
            m_Players.Remove(this);
        }

        public static StaticAudioPlayer GetAudioPlayer(AudioBundle bundle)
        {
            if (bundle == null)
                throw new ArgumentNullException("Bundle must be defined");

            var player = m_Players.First(p => p.audioBundle == bundle);

            if (player == null)
            {
                player = new GameObject("autocreated_StaticAudioPlayer", typeof(StaticAudioPlayer)).GetComponent<StaticAudioPlayer>();
                player.audioBundle = bundle;
            }

            return player;
        }
    }
}
