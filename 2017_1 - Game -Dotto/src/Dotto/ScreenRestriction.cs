﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto
{
    [Serializable]
    [ExecuteInEditMode]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Camera))]
    public class ScreenRestriction : MonoBehaviour
    {
        public readonly ObjectTriggerEvent Event_OnAnyObjectExit = new ObjectTriggerEvent();

        private BoxCollider2D m_Collider;
        private Camera m_Camera;

        private float m_PreviousCameraSize;

        public class ObjectTriggerEvent : UnityEvent<GameObject>
        {
        }

        public void RecalculateRectangle()
        {
            m_Collider.size = m_Camera.OrthographicBounds().size;
        }

        protected virtual void Awake()
        {
            m_Collider = GetComponent<BoxCollider2D>();
            m_Collider.isTrigger = true;

            m_Camera = GetComponent<Camera>();

            TryUpdateBounds();
        }

        protected virtual void Update()
        {
            TryUpdateBounds();
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            DispatchObjectTriggerExitEvents(collision.gameObject);
        }

        private void TryUpdateBounds()
        {
            if (m_PreviousCameraSize != m_Camera.orthographicSize)
            {
                m_PreviousCameraSize = m_Camera.orthographicSize;
                RecalculateRectangle();
            }
        }

        private void DispatchObjectTriggerExitEvents(GameObject gameObject)
        {
            if (Event_OnAnyObjectExit == null) return;
            Event_OnAnyObjectExit.Invoke(gameObject);
        }
    }
}
