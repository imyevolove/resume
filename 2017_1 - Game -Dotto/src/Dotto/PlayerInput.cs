﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto
{
    [Serializable]
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Image))]
    public class PlayerInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] public LayerMask blockingLayerMask;

        public GameManager GameManager { get { return GameManager.Instance; } }
        public BallLauncher BallLauncher { get { return GameManager.BallLauncher; } }

        [SerializeField] private bool m_LocalInvertPointer;
        [SerializeField] public bool inheritInvertPointerFromLocalPlayerConfiguration = false;
        public bool InvertPointer
        {
            get
            {
                return inheritInvertPointerFromLocalPlayerConfiguration
                    ? Player.Local.Configurations.InvertPointer.Value
                    : m_LocalInvertPointer;
            }
            set
            {
                if (inheritInvertPointerFromLocalPlayerConfiguration)
                    Player.Local.Configurations.InvertPointer.Value = value;

                m_LocalInvertPointer = value;
            }
        }

        private Vector2 m_MouseDownPosition;
        private Camera m_Camera;

        public void OnDrag(PointerEventData eventData)
        {
            if (GameManager.IsLocked || GameManager.IsPlaying) return;

            UpdateLauncher(BallLauncher, eventData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (GameManager.IsLocked) return;
            
            if (GameManager.IsPlaying)
                GameManager.Restart();

            m_Camera = Camera.main;

            m_MouseDownPosition = eventData.position;

            var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 100, blockingLayerMask.value);
            if (hit.collider != null) return;

            var worldMousePosition = PointerToWorldPosition(eventData);

            if (!GameManager.AreaCollector.AllowedIn(worldMousePosition))
            {
                Debug.LogFormat("Not allowed in point {0}", worldMousePosition);
                return;
            }

            BallLauncher.launchPosition = worldMousePosition;
            BallLauncher.PrepareToLaunch();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (GameManager.IsLocked || GameManager.IsPlaying) return;

            UpdateLauncher(BallLauncher, eventData);
            GameManager.LaunchBall();
        }

        private Vector2 PointerToWorldPosition(PointerEventData eventData)
        {
            return m_Camera.ScreenToWorldPoint(eventData.position);
        }

        private void UpdateLauncher(BallLauncher launcher, PointerEventData eventData)
        {
            var tg = PointerToWorldPosition(eventData) - launcher.launchPosition;
            launcher.launchDirection =  tg.normalized * (InvertPointer ? -1 : 1);
            launcher.launchPower = tg.magnitude;
        }
    }

}