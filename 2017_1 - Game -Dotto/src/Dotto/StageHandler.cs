﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Dotto
{
    public class StageHandler : IDisposable
    {
        public readonly UnityEvent Event_Completed = new UnityEvent();
        public bool IsCompleted { get; private set; }

        public StageInfo StageInfo { get; private set; }
        public Scene Scene { get; private set; }

        public Point[] Walls { get; private set; }

        public bool IsLocked { get { return LockAsyncOperation != null && !LockAsyncOperation.isDone; } }
        public AsyncOperation LockAsyncOperation { get; private set; }

        public StageHandler(StageInfo info)
        {
            StageInfo = info;
            Scene = SceneManager.GetSceneByName(StageInfo.SceneName);
        }

        public void LoadScene(Action<AsyncOperation> callback)
        {
            var operation = LoadScene();
            operation.completed += callback;
        }

        public AsyncOperation LoadScene()
        {
            var operation = SceneManager.LoadSceneAsync(StageInfo.SceneName, LoadSceneMode.Additive);
            operation.completed += (ao) =>
            {
                Scene = SceneManager.GetSceneByName(StageInfo.SceneName);
                LockAsyncOperation = null;
                RegisterWalls();
            };

            LockAsyncOperation = operation;
            return operation;
        }

        public void UnloadScene(Action<AsyncOperation> callback)
        {
            var operation = UnloadScene();
            operation.completed += callback;
        }

        public AsyncOperation UnloadScene()
        {
            return LockAsyncOperation = SceneManager.UnloadSceneAsync(StageInfo.SceneName);
        }

        private int m_ActiveWallsCounter;
        private bool m_IsDisposed;

        internal void RegisterWalls()
        {
            if (m_IsDisposed) return;

            RemoveWallEventListeners(Walls);
            Walls = UnityEngine.Object.FindObjectsOfType<Point>().ToArray();
            AddWallEventListeners(Walls);

            Reset();
        }

        public void Reset()
        {
            foreach (var wall in Walls)
                wall.Restore();
            m_ActiveWallsCounter = Walls.Count(w => w.Active);
        }

        private void AddWallEventListeners(IEnumerable<Point> walls)
        {
            if (walls == null) return;

            foreach (var wall in walls)
                wall.Event_OnActiveChanged.AddListener(OnAnyWallActiveStateChanged);
        }

        private void RemoveWallEventListeners(IEnumerable<Point> walls)
        {
            if (walls == null) return;

            foreach (var wall in walls)
                wall.Event_OnActiveChanged.RemoveListener(OnAnyWallActiveStateChanged);
        }

        private void OnAnyWallActiveStateChanged(bool state)
        {
            if (IsCompleted) return;

            m_ActiveWallsCounter += state ? 1 : -1;
            if (m_ActiveWallsCounter <= 0)
            {
                IsCompleted = true;
                DispatchStageCompletedEvent();
            }
        }

        private void DispatchStageCompletedEvent()
        {
            if (Event_Completed == null) return;
            Event_Completed.Invoke();
        }

        public void Dispose()
        {
            if (m_IsDisposed) return;

            Event_Completed.RemoveAllListeners();

            m_IsDisposed = true;
            RemoveWallEventListeners(Walls);

            if (Scene.isLoaded)
                UnloadScene();
        }
    }
}
