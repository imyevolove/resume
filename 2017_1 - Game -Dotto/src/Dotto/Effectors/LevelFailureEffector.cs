﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Effectors
{
    [Serializable]
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Animator))]
    public class LevelFailureEffector : MonoBehaviour
    {
        [SerializeField] public CameraShaker cameraShaker;
        [SerializeField] public CameraShaker.CameraShakeOptions cameraShakerOptions;
        
        [SerializeField] public AudioBundle soundEffect;

        private Animator m_Animator;
        public Animator Animator
        {
            get
            {
                if (m_Animator == null)
                    m_Animator = GetComponent<Animator>();

                return m_Animator;
            }
        }

        protected virtual void Start()
        {
            GameManager.Instance.Event_OnGameOver.AddListener(OnGameOver);
        }

        public virtual void Play()
        {
            if (cameraShaker != null)
                cameraShaker.Shake(cameraShakerOptions);

            if (soundEffect != null)
                soundEffect.Play(Player.Local.Configurations.AudioVolume.Value);

            Animator.SetTrigger("Play");
        }

        protected virtual void OnGameOver()
        {
            Debug.Log("Game over");
            Play();
        }
    }
}
