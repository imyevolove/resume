﻿using System;
using System.Linq;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class ReflectLauncherVisualizator : LauncherVisualizator
    {
        [SerializeField] public LayerMask raycastLayerMask;
        [SerializeField] public uint reflectionCount = 0;
        [SerializeField] public float endReflectedTraceLineLength;

        protected override void Draw(BallLauncher launcher)
        {
            lineRenderer.positionCount = 1;
            
            /// Позиция запуска / игрока
            lineRenderer.SetPosition(0, launcher.launchPosition);

            if (reflectionCount == 0)
            {
                DrawNextLinePosition(launcher.launchPosition + launcher.launchDirection * launcher.launchPower);
            }
            else
            {
                var isLastIteration = false;
                var previousOrigin = launcher.launchPosition;
                var previousDirection = launcher.launchDirection;
                var previousRaycastDistance = 20f;
                Collider2D previousCollider = null;

                for (var i = 0; i < reflectionCount; i++)
                {
                    isLastIteration = i + 1 >= reflectionCount;

                    /// Отладочная информация о направлении
                    Debug.DrawRay(previousOrigin, previousDirection * 1f, Color.green);

                    /// Поиск точки рикошета
                    var raycastHit = FindReflection(previousOrigin, previousDirection, previousRaycastDistance, previousCollider);
                    var isCollide = raycastHit.collider != null;
                    
                    /// Нет первого столкновения, рисуем линию
                    if (i == 0 && !isCollide)
                    {
                        DrawNextLinePosition(previousOrigin + previousDirection * previousRaycastDistance);
                        break;
                    }
                    
                    /// Есть столкновение, отражаем
                    if (isCollide)
                    {
                        var reflectedDirection = Vector2.Reflect(previousDirection, raycastHit.normal);
                        DrawNextLinePosition(raycastHit.point);

                        previousOrigin = raycastHit.point;
                        previousDirection = reflectedDirection;
                        previousRaycastDistance = isLastIteration ? endReflectedTraceLineLength : 10f;
                        previousCollider = raycastHit.collider;
                    }

                    if (!isCollide || isLastIteration)
                    {
                        DrawNextLinePosition(previousOrigin + previousDirection * endReflectedTraceLineLength);
                        break;
                    }
                }
            }
        }

        private RaycastHit2D FindReflection(Vector2 origin, Vector2 direction, float distance)
        {
            return Physics2D.Raycast(origin, direction, distance, raycastLayerMask.value);
        }

        private RaycastHit2D FindReflection(Vector2 origin, Vector2 direction, float distance, Collider2D ignoreCollider)
        {
            var hits = Physics2D.RaycastAll(origin, direction, distance, raycastLayerMask.value);
            return hits.FirstOrDefault(h => h.collider != ignoreCollider && !h.collider.isTrigger);
        }

        private void DrawNextLinePosition(Vector2 position)
        {
            lineRenderer.SetPosition(lineRenderer.NextPosition(), position);
        }
    }
}