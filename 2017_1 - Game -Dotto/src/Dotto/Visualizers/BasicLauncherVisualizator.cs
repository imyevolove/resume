﻿using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public class BasicLauncherVisualizator : LauncherVisualizator
    {
        protected override void Draw(BallLauncher launcher)
        {
            if (lineRenderer.positionCount != 2)
                lineRenderer.positionCount = 2;

            lineRenderer.SetPosition(0, launcher.launchPosition);
            lineRenderer.SetPosition(1, launcher.launchPosition + launcher.launchDirection * launcher.launchPower);
        }
    }
}