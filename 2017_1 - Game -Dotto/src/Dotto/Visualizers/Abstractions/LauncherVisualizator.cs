﻿using System;
using UnityEngine;

namespace Dotto
{
    [Serializable]
    public abstract class LauncherVisualizator : MonoBehaviour
    {
        [SerializeField] public bool inheritColor = true;
        [SerializeField] public LineRenderer lineRenderer;
        [SerializeField] private BallLauncher m_BallLauncher;
        public BallLauncher BallLauncher
        {
            get { return m_BallLauncher; }
            set
            {
                var old = m_BallLauncher;
                m_BallLauncher = value;

                OnLauncherChanged(old, value);
            }
        }

        private bool m_Update = false;

        protected virtual void Awake()
        {
            BallLauncher = m_BallLauncher;
        }

        protected virtual void Update()
        {
            if (BallLauncher == null || !m_Update) return;
            Draw(BallLauncher);
        }

        protected virtual void EndDraw()
        {
            lineRenderer.enabled = false;
        }

        protected virtual void StartDraw()
        {
            lineRenderer.enabled = true;
            lineRenderer.positionCount = 0;
        }

        protected abstract void Draw(BallLauncher launcher);

        protected virtual void OnLauncherChanged(BallLauncher previous, BallLauncher current)
        {
            RemoveLauncherEventListeners(previous);
            AddLauncherEventListeners(current);
        }

        protected virtual void OnLauncherStateChanged(BallLauncher.LauncherState state)
        {
            var previousUpdateState = m_Update;
            m_Update = state == BallLauncher.LauncherState.Prepare;

            if (previousUpdateState != m_Update)
            {
                if (m_Update)
                {
                    if (inheritColor)
                    {
                        ChangeLineColor();
                    }

                    StartDraw();
                }
                else
                {
                    EndDraw();
                }
            }
        }

        private void AddLauncherEventListeners(BallLauncher target)
        {
            if (target == null) return;
            target.Event_OnStateChanged.AddListener(OnLauncherStateChanged);
        }

        private void RemoveLauncherEventListeners(BallLauncher target)
        {
            if (target == null) return;
            target.Event_OnStateChanged.RemoveListener(OnLauncherStateChanged);
        }

        private void ChangeLineColor()
        {
            var canChangeColor = lineRenderer != null && BallLauncher != null && BallLauncher.ball != null && BallLauncher.ball.Theme != null;
            if (!canChangeColor) return;
            lineRenderer.colorGradient = lineRenderer.colorGradient.ChangeColor(BallLauncher.ball.Theme.CharacterColor);
        }
    }
}
