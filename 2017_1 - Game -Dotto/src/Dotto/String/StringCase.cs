﻿namespace Dotto
{
    public enum StringCase
    {
        None,
        Upper,
        Lower,
        Capitalize,
        CapitalizeAll
    }
}
