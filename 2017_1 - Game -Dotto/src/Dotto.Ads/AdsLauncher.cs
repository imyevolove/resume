﻿using Dotto.UI;
using System;
using System.Collections;
using UnityEngine;

namespace Dotto.Ads
{
    [Serializable]
    public class AdsLauncher : MonoBehaviour
    {
        [SerializeField] protected AdsNoticeDrawer noticeDrawer;

        [SerializeField] public float pauseForFirstAds;
        [SerializeField] public float pauseBetweenAds;
        [SerializeField] public float noticeDelay;

        private int m_RequestIndex = 0;
        private float m_NoticeDelayCounter = 0;

        private Coroutine m_NoticeDelayCoroutine;
        private Coroutine m_RequestAdDelayCoroutine;

        protected virtual void OnEnable()
        {
            StartDelayedAdRequest();
        }

        private void OnAdPauseEnd()
        {
            if (Player.Local.AdvertisementDisabled.Value)
            {
                Debug.Log("Ads is locked");
                return;
            }

            var adsManager = AdvertisementManager.Instance;
            if (adsManager == null || !adsManager.VideoIsReady(adsManager.options.basicAdsVideoPlacementId))
            {
                StartDelayedAdRequest();
                return;
            }
            else
            {
                noticeDrawer.Show();
                StartNoticeAwait();
            }
        }

        private void OnNoticeDelayEnd()
        {
            StartDelayedAdRequest();

            noticeDrawer.Hide();

            var adsManager = AdvertisementManager.Instance;
            adsManager.ShowVideo(adsManager.options.basicAdsVideoPlacementId);
        }

        private void OnNoticeDelayTick(float timeleft)
        {
            if (timeleft < 0)
                timeleft = 0;

            noticeDrawer.SetTimeleft(timeleft, noticeDelay);
        }

        private void StartNoticeAwait()
        {
            SafeStopCoroutine(m_NoticeDelayCoroutine);
            m_NoticeDelayCoroutine = StartCoroutine(NoticeDelayHandler());
        }

        private void StartDelayedAdRequest()
        {
            SafeStopCoroutine(m_RequestAdDelayCoroutine);
            m_RequestAdDelayCoroutine = StartCoroutine(DelayedAdRequest());
        }

        private IEnumerator NoticeDelayHandler()
        {
            m_NoticeDelayCounter = noticeDelay;
            while (m_NoticeDelayCounter > 0)
            {
                m_NoticeDelayCounter -= Time.deltaTime;
                OnNoticeDelayTick(m_NoticeDelayCounter);
                yield return null;
            }

            OnNoticeDelayEnd();
        }

        private IEnumerator DelayedAdRequest()
        {
            m_RequestIndex++;
            yield return new WaitForSeconds(m_RequestIndex > 1 ? pauseBetweenAds : pauseForFirstAds);
            OnAdPauseEnd();
        }

        private void SafeStopCoroutine(Coroutine coroutine)
        {
            if (coroutine == null) return;
            StopCoroutine(coroutine);
        }
    }
}
