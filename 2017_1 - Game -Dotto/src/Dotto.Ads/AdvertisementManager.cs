﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Dotto.Ads
{
    [Serializable]
    public class AdvertisementManager : MonoBehaviour
    {
        public static AdvertisementManager Instance { get; private set; }

        [SerializeField] public AdvertisementOptions options;

        [Serializable]
        public struct AdvertisementOptions
        {
            public string androidGameId;
            public string iosGameId;

            public string basicAdsVideoPlacementId;
            public string rewardedAdsVideoPlacementId;
        }

        protected virtual void Awake()
        {
            Instance = this;

            DontDestroyOnLoad(gameObject);
            InitializeAdvertisement();
        }

        public bool VideoIsReady()
        {
            return Advertisement.IsReady();
        }

        public bool VideoIsReady(string placementId)
        {
            return Advertisement.IsReady(placementId);
        }

        public void ShowVideo(string placementId)
        {
            Advertisement.Show(placementId);
        }

        public void ShowVideo(string placementId, Action<ShowResult> callback)
        {
            Advertisement.Show(placementId, new ShowOptions
            {
                resultCallback = callback
            });
        }

        public void ShowRewardedVideo(Action<ShowResult> callback)
        {
            ShowVideo(options.rewardedAdsVideoPlacementId, callback);
        }

        public void ShowRewardedVideo(Action<bool> callback)
        {
            ShowRewardedVideo((status) =>
            {
                if (callback == null) return;
                callback.Invoke(status == ShowResult.Finished);
            });
        }

        private void InitializeAdvertisement()
        {
            if (Advertisement.isSupported && !Advertisement.isInitialized)
            {
                var gameId = "0";

#if UNITY_IOS
                gameId = options.androidGameId;
#elif UNITY_ANDROID
                gameId = options.iosGameId;
#endif

                Advertisement.Initialize(gameId);
            }
            else
            {
                Debug.LogWarning("Advertisement not available");
            }
        }
    }
}
