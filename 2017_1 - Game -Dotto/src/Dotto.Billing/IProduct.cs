﻿namespace Dotto.Billing
{
    using UnityEngine.Purchasing;

    public interface IProduct
    {
        string      ProductID   { get; }
        ProductType ProductType { get; }

        string title { get; set; }
        string description { get; set; }
        string price { get; set; }

        event ProductChangeEventHandler onChange;

        void Purchase();
        void Purchase(ProductEventHandler eventHandler);
    }
}
