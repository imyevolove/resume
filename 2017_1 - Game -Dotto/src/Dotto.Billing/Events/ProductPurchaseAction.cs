﻿namespace Dotto.Billing
{
    public delegate void ProductPurchaseAction(IProduct product);
}
