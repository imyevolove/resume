﻿namespace Dotto.Billing
{
    using System.Collections.Generic;
    using UnityEngine;

    public class IAPManager
    {
        public IAPManager() {}

        public static int ProductsCount { get { return m_Products.Count; } }

        private static BaseIAPController m_IAPController;
        private static List<IProduct> m_Products = new List<IProduct>();

        public static void Initialize()
        {
            RepairController();

            if (m_IAPController.IsInitialized())
            {
                return;
            }

            m_IAPController.onInitializedSuccess -= RequestProductPrices;
            m_IAPController.onInitializedSuccess += RequestProductPrices;
            m_IAPController.Initialize();
        }

        ////////////////////////////////////////////////////////////
        public static void AddProduct(IProduct product)
        {
            RepairController();
            if (m_IAPController.AddProduct(product.ProductID, product.ProductType))
            {
                m_Products.Add(product);
            }
        }

        ////////////////////////////////////////////////////////////
        public static void AddProducts(IProduct[] products)
        {
            foreach (var product in products)
            {
                AddProduct(product);
            }
        }

        ////////////////////////////////////////////////////////////
        public static void BuyProduct(IProduct product, ProductEventHandler eventHandler)
        {
            Initialize();

            if (!m_Products.Contains(product))
            {
                Debug.LogFormat("Can't buy broduct. Product {0} is not registered", product.ProductID);

                if (eventHandler != null)
                {
                    eventHandler(new ProductEventData() { product = product, status = PurchaseStatus.Fail });
                }

                return;
            }

            m_IAPController.BuyProduct(product, eventHandler);
        }

        ////////////////////////////////////////////////////////////
        public static void RestorePurchases()
        {
            Initialize();
            m_IAPController.RestorePurchases();
        }

        ////////////////////////////////////////////////////////////
        public static IProduct GetProduct(int index)
        {
            return m_Products[index];
        }

        ////////////////////////////////////////////////////////////
        public static IProduct GetProduct(string productId)
        {
            return m_Products.Find(p => { return p.ProductID == productId; });
        }

        ////////////////////////////////////////////////////////////
        public static IProduct[] GetProducts()
        {
            return m_Products.ToArray();
        }

        ////////////////////////////////////////////////////////////
        private static void RepairController()
        {
            if (m_IAPController == null)
            {
                m_IAPController = IAPController.Instantiate();
            }
        }

        ////////////////////////////////////////////////////////////
        private static void RequestProductPrices()
        {
            foreach (var product in m_Products)
            {
                product.price = m_IAPController.GetProductPrice(product.ProductID);
            }
        }
    }
}
