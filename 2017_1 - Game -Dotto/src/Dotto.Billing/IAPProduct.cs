﻿namespace Dotto.Billing
{
    using UnityEngine;
    using UnityEngine.Purchasing;

    public class IAPProduct : IProduct
    {
        public string       ProductID   { get; protected set; }
        public ProductType  ProductType { get; protected set; }

        public string description
        {
            get { return mDescription; }
            set { mDescription = value; CallOnChangeEvent(); }
        }
        public string title
        {
            get { return mTitle; }
            set { mTitle = value; CallOnChangeEvent(); }
        }
        public string price
        {
            get { return mPrice; }
            set { mPrice = value; CallOnChangeEvent(); }
        }

        public event ProductChangeEventHandler onChange;

        protected string mDescription;
        protected string mTitle;
        protected string mPrice;

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public IAPProduct(string productId, ProductType type)
        {
            ProductID   = productId;
            ProductType = type;
        }

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public IAPProduct(string productId, ProductType type, string title, string description)
        {
            ProductID   = productId;
            ProductType = type;

            this.title          = title;
            this.description    = description;
        }

        ////////////////////////////////////////////////////////////
        public virtual void Purchase()
        {
            IAPManager.BuyProduct(this, eventData => {
                switch (eventData.status)
                {
                    case PurchaseStatus.Success:
                    case PurchaseStatus.Purchased:
                        Debug.Log("Say Thx here + remove ads + add vip status");
                        break;
                }
            });
        }

        ////////////////////////////////////////////////////////////
        public virtual void Purchase(ProductEventHandler eventHandler)
        {
            IAPManager.BuyProduct(this, eventHandler);
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnChangeEvent()
        {
            if (onChange == null) return;
            onChange();
        }
    }
}
