﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Economy
{
    [CreateAssetMenu(menuName = "Rewards/Basic generator")]
    public class RewardGenerator : BaseRewardGenerator
    {
        [SerializeField] public int rewardPerTile;
        [SerializeField] public float repeateTimeMultiplier;
        [SerializeField] public AnimationCurve tileRewardMultiplier = new AnimationCurve();
        [SerializeField] public AnimationCurve levelRewardMultiplier = new AnimationCurve();
        [SerializeField] public AnimationCurve attemptsRewardMultiplier = new AnimationCurve();

        public override int Generate(int level, int tilesCount, int attemptsCount, bool isRepeateTime = false)
        {
            var multiplier = levelRewardMultiplier.Evaluate(level);
            multiplier *= tileRewardMultiplier.Evaluate(tilesCount);
            multiplier *= attemptsRewardMultiplier.Evaluate(attemptsCount);

            var reward = rewardPerTile * multiplier;

            if (isRepeateTime)
                reward *= repeateTimeMultiplier;

            return Mathf.CeilToInt(reward);
        }
    }
}
