﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Economy
{
    public interface IProduct
    {
        Price Price { get; }
        int Investment { get; set; }
    }
}
