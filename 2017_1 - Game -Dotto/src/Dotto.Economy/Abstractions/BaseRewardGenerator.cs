﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Economy
{
    public abstract class BaseRewardGenerator : ScriptableObject
    {
        public abstract int Generate(int level, int tilesCount, int attemptsCount, bool isRepeateTime = false);
    }
}
