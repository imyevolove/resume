﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Economy
{
    public enum CurrencyKind
    {
        Coins = 0, /// Just coins
        VideoView = 1 /// Video ads viewing
    }
}
