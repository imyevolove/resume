﻿using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Economy
{
    [Serializable]
    public struct Price
    {
        [SerializeField] public CurrencyKind kind;
        [SerializeField] public int value;

        public void Set(CurrencyKind kind, int value)
        {
            SetKind(kind);
            SetValue(value);
        }

        public void SetKind(CurrencyKind kind)
        {
            this.kind = kind;
        }

        public void SetValue(int value)
        {
            this.value = value;
        }

        public void Past(Price from)
        {
            kind = from.kind;
            value = from.value;
        }
    }
}
