﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.Economy
{
    public class ProductInvestment
    {
        public readonly InvestmentCompleteEvent Event_InvestmentCompleted = new InvestmentCompleteEvent();
        public readonly InvestmentCompleteEvent Event_InvestmentValueChanged = new InvestmentCompleteEvent();
        public virtual Price Price { get; set; }

        public int m_Investment;
        public virtual int Investment
        {
            get { return m_Investment; }
            set
            {
                m_Investment = value < 0 ? 0 : value;

                if (m_Investment >= Price.value)
                {
                    var iters = Price.value == 0 ? 0 : Mathf.FloorToInt(m_Investment / Price.value);
                    m_Investment = 0;
                    Event_InvestmentCompleted.Invoke(iters);
                }

                Event_InvestmentValueChanged.Invoke(Investment);
            }
        }

        public class InvestmentCompleteEvent : UnityEvent<int>
        {
        }

        public ProductInvestment()
        {
        }

        public ProductInvestment(Price price)
        {
            Price = price;
        }
    }
}
