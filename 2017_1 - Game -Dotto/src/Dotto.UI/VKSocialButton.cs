﻿using Dotto.Localization;
using Dotto.Popups;
using Dotto.Socials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VK.Unity;

namespace Dotto.UI
{
    [Serializable]
    public class VKSocialButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private SocialMenu m_SocialMenu;
        [SerializeField] private Image m_Icon;

        [Header("Localization")]
        [SerializeField] private TextLocalizer m_LocalizedText;
        [SerializeField] private string m_LocalizedConnectedKey;
        [SerializeField] private string m_LocalizedDisconnectedKey;

        protected virtual void Start()
        {
            UpdateConnectedMessage();

            GameVars.Social.OnLogin.AddListener(UpdateConnectedMessage);
            GameVars.Social.OnLogout.AddListener(UpdateConnectedMessage);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!GameVars.Social.IsInitialized)
            {
                GameVars.Social.Initialize(() =>
                {
                    if (!GameVars.Social.IsInitialized)
                    {
                        Debug.LogWarning("Social initializing failed");
                        return;
                    }

                    LoginOrOpenMenu();
                });

                return;
            }

            LoginOrOpenMenu();
        }

        private void LoginOrOpenMenu()
        {
            m_SocialMenu.OpenWithLogin();
        }

        private void UpdateConnectedMessage()
        {
            m_LocalizedText.localizationKey = GameVars.Social.IsLoggedIn ? m_LocalizedConnectedKey : m_LocalizedDisconnectedKey;
            m_LocalizedText.UpdateText();
        }
    }
}
