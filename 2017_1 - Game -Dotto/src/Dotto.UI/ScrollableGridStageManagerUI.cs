﻿using System;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    public class ScrollableGridStageManagerUI : BaseGridStageManagerUI
    {
        [SerializeField] protected RectTransform Content;
        
        public override void Redraw()
        {
            RemoveCells();

            foreach (var stage in Target)
                CreateCell(stage);
        }

        private void CreateCell(StageInfo stage)
        {
            var cell = GetFreeCell();
            cell.transform.SetParent(Content, false);
            cell.SetTarget(stage);
        }
    }
}
