﻿using Dotto.Socials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    public class SocialFriendsUI : MonoBehaviour
    {
        [SerializeField] private RectTransform m_Container;
        [SerializeField] private SocialFriendBox m_FriendBoxPrefab;

        private List<SocialFriendBox> m_UserBoxes = new List<SocialFriendBox>();

        public void Show(UserInfo[] users)
        {
            gameObject.SetActive(true);

            DisableAllBoxes();

            if (users.Length == 0) return;

            var boxes = GetBoxes(users.Length);

            var userIndex = 0;
            foreach (var box in boxes)
            {
                box.EnableObject();
                box.SetTarget(users[userIndex]);

                userIndex++;
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private IEnumerable<SocialFriendBox> GetBoxes(int count)
        {
            if (m_UserBoxes.Count < count)
            {
                RegisterBoxes(count - m_UserBoxes.Count);
            }

            return m_UserBoxes.Take(count);
        }

        private void RegisterBoxes(int count)
        {
            while (count > 0)
            {
                count--;

                var box = Instantiate(m_FriendBoxPrefab, m_Container, false);
                box.DisableObject();

                m_UserBoxes.Add(box);
            }
        }

        private void DisableAllBoxes()
        {
            foreach (var box in m_UserBoxes)
            {
                box.DisableObject();
            }
        }
    }
}
