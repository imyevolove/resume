﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class StageLevelDrawer :MonoBehaviour
    {
        [SerializeField] protected Text TextField;

        protected virtual void Start()
        {
            GameManager.Instance.Event_OnLevelChanged.AddListener(OnStageChanged);
            Redraw();
        }

        public void Redraw()
        {
            Redraw(GameManager.Instance.CurrentStage);
        }

        protected void Redraw(StageInfo stage)
        {
            if (stage == null) return;
            TextField.text = (stage.Index + 1).ToString();
        }

        private void OnStageChanged(StageInfo stage)
        {
            Redraw(stage);
        }
    }
}
