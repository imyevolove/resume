﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    public class AsyncImage : MonoBehaviour
    {
        public Image Image { get; private set; }

        private Coroutine m_LoaderCoroutine;

        protected virtual void Awake()
        {
            Image = GetComponent<Image>();
        }

        public void LoadAsync(string url)
        {
            StopLoading();

            var request = new WWW(url);
            StartCoroutine(LoadTextureRoutine(request));
        }

        protected virtual void OnTextureLoaded(Texture2D texture)
        {
            Image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
        }

        private IEnumerator LoadTextureRoutine(WWW request)
        {
            yield return request;

            if (request.texture != null)
            {
                OnTextureLoaded(request.texture);
                StopLoading();
            }
        }

        private void StopLoading()
        {
            if (m_LoaderCoroutine == null) return;

            StopCoroutine(m_LoaderCoroutine);
            m_LoaderCoroutine = null;
        }
    }
}
