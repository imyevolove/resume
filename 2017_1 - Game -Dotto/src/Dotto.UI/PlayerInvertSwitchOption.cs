﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.UI
{
    [Serializable]
    public class PlayerInvertSwitchOption : SwitchOption
    {
        protected virtual void Start()
        {
            var option = Player.Local.Configurations.InvertPointer;
            option.Event_ValueChanged.AddListener(OnOptionValueChanged);
            Value = option.Value;
        }

        protected override void OnValueChanged(bool value)
        {
            base.OnValueChanged(value);
            Player.Local.Configurations.InvertPointer.Value = value;
        }

        private void OnOptionValueChanged(bool value)
        {
            if (value == Value) return;
            Value = value;
        }
    }
}
