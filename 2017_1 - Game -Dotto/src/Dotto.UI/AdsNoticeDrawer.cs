﻿using Dotto.Localization;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class AdsNoticeDrawer : ScreenLayout
    {
        [SerializeField] protected Image FillImage;
        [SerializeField] protected Text TimeleftTextField;
        [SerializeField] protected string localizationKey;

        public void SetTimeleft(float timeleft, float delay)
        {
            var time = Mathf.Floor(timeleft);
            if (time < 0) time = 0;

            var localizaedText = LocalizationManager.LocalizationProvider.GetValue(localizationKey, time);
            TimeleftTextField.text = localizaedText.ToUpper();

            FillImage.fillAmount = timeleft / delay;
        }
    }
}
