﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [ExecuteInEditMode]
    public class CircleSlider : MonoBehaviour, IDragHandler, IInitializePotentialDragHandler, IPointerDownHandler
    {
        public readonly ValueChangeEvent Event_ValueChanged = new ValueChangeEvent();

        [SerializeField] public float sliderRadius;
        [SerializeField] public RectTransform handleRect;

        [NonSerialized] private RectTransform m_Rect;
        private RectTransform RectTransform
        {
            get
            {
                if (m_Rect == null)
                    m_Rect = GetComponent<RectTransform>();

                return m_Rect;
            }
        }

        [SerializeField] private float m_SliderAngle = 0;
        public float SliderAngle
        {
            get { return m_SliderAngle; }
            set
            {
                var v = value % 360;

                m_AngleDelta = m_SliderAngle - v;
                m_SliderAngle = v;

                SetDirty();
            }
        }

        private DrivenRectTransformTracker m_Tracker;

        protected bool IsActive { get { return isActiveAndEnabled; } }
        private bool m_MayDrag = false;
        private float m_AngleDelta = 0;

        public class ValueChangeEvent : UnityEvent<SliderEventData>
        {
        }

        public class SliderEventData
        {
            public float Delta { get; set; }
            public int Direction { get { return Delta < 0 ? -1 : +1; } }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            m_MayDrag = IsActive && handleRect != null;

            if (m_MayDrag)
                SetAngle(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!m_MayDrag) return;
            
            SetAngle(eventData);
            DispatchSliderEventData();
        }

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            eventData.useDragThreshold = false;
        }

        protected virtual void OnEnable()
        {
            SetDirty();
        }

        protected virtual void OnDisable()
        {
            m_Tracker.Clear();
            LayoutRebuilder.MarkLayoutForRebuild(RectTransform);
        }

        protected virtual void OnRectTransformDimensionsChange()
        {
            SetDirty();
        }

        private void SetDirty()
        {
            if (!IsActive) return;
            UpdateHandle();
        }

        private void SetAngle(PointerEventData eventData)
        {
            var dir = (eventData.position - (Vector2)RectTransform.position).normalized;
            SliderAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        }

        private void UpdateHandle()
        {
            if (!IsActive) return;
            m_Tracker.Clear();

            if (handleRect == null) return;
            m_Tracker.Add(this, handleRect,
                        DrivenTransformProperties.Anchors |
                        DrivenTransformProperties.AnchoredPosition);

            handleRect.anchorMin = new Vector2(0.5f, 0.5f);
            handleRect.anchorMax = new Vector2(0.5f, 0.5f);

            var angleRad = SliderAngle * Mathf.Deg2Rad;
            handleRect.anchoredPosition = new Vector2(
                Mathf.Cos(angleRad) * sliderRadius,
                Mathf.Sin(angleRad) * sliderRadius);
        }
        
        private void DispatchSliderEventData()
        {
            Event_ValueChanged.Invoke(new SliderEventData
            {
                Delta = m_AngleDelta
            });
        }

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            UpdateHandle();
        }
#endif
    }
}
