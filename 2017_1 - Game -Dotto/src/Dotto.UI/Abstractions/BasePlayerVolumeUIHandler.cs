﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public abstract class BasePlayerVolumeUIHandler : Drawer<PlayerConfigurations.Volume>
    {
        [SerializeField] protected Slider VolumeSlider;

        protected override void Awake()
        {
            base.Awake();
            VolumeSlider.onValueChanged.AddListener(OnSliderValueChanged);
        }

        protected override void OnAttach(PlayerConfigurations.Volume target)
        {
            AddVolumeEventListeners(target);
            TriggerFakeValueChangedEvent();
        }

        protected override void OnDeattach(PlayerConfigurations.Volume target)
        {
            RemoveVolumeEventListeners(target);
        }

        protected virtual void OnVolumeValueChanged(float value)
        {
            if (value == VolumeSlider.value) return;
            VolumeSlider.value = value;
        }

        protected virtual void OnSliderValueChanged(float value)
        {
            if (value == Target.Value) return;
            Target.Value = value;
        }

        private void TriggerFakeValueChangedEvent()
        {
            if (Target == null) return;
            OnVolumeValueChanged(Target.Value);
        }

        private void AddVolumeEventListeners(PlayerConfigurations.Volume volume)
        {
            if (volume == null) return;
            volume.Event_ValueChanged.AddListener(OnVolumeValueChanged);
        }

        private void RemoveVolumeEventListeners(PlayerConfigurations.Volume volume)
        {
            if (volume == null) return;
            volume.Event_ValueChanged.RemoveListener(OnVolumeValueChanged);
        }
    }
}
