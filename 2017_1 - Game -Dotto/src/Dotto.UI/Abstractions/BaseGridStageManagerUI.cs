﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.UI
{
    using Pooling;

    [Serializable]
    public abstract class BaseGridStageManagerUI : BaseStageManagerUI
    {
        [SerializeField] protected BaseStageCellDrawer CellDrawerPrefab;

        private MonoPool<BaseStageCellDrawer> m_CellPool;

        private StageInfo m_SelectedStage;
        public override StageInfo SelectedStage { get { return m_SelectedStage; } }
 
        protected override void Awake()
        {
            base.Awake();
            m_CellPool = new MonoPool<BaseStageCellDrawer>(CellDrawerPrefab);
        }

        protected override void OnAttach(StageInfo[] target)
        {
            Redraw();
        }

        protected override void OnDeattach(StageInfo[] target)
        {
        }

        protected BaseStageCellDrawer GetFreeCell()
        {
            var cell = m_CellPool.GetFree();
            cell.Event_OnClick.RemoveListener(OnAnyCellClickEvent);
            cell.Event_OnClick.AddListener(OnAnyCellClickEvent);
            return cell;
        }

        protected void RemoveCells()
        {
            m_CellPool.ReleaseAll();
        }

        private void OnAnyCellClickEvent(BaseStageCellDrawer cell)
        {
            m_SelectedStage = cell.Target;
            DispatchStageSelectedEvent(m_SelectedStage);
        }
    }
}
