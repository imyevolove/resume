﻿using Dotto.Customization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    public abstract class BaseCharacterThemeDrawer : Drawer<CharacterTheme>, IPointerClickHandler
    {
        public readonly UnityEvent Event_OnClick = new UnityEvent();
        public CharacterThemeProgress ThemeProgress { get; private set; }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            Event_OnClick.Invoke();
        }

        public void SetThemeProgress(CharacterThemeProgress progress)
        {
            var old = ThemeProgress;
            ThemeProgress = progress;

            OnThemeProgressDeattach(old);
            OnThemeProgressAttach(ThemeProgress);
        }

        public void Redraw()
        {
            if (Target == null)
            {
                Debug.LogWarning("Can't redraw character theme. Target must be defined");
                return;
            }

            Draw();
        }

        protected abstract void Draw();

        protected override void OnAttach(CharacterTheme target)
        {
            Redraw();
        }

        protected override void OnDeattach(CharacterTheme target)
        {
        }

        protected virtual void OnThemeProgressAttach(CharacterThemeProgress progress)
        {
            if (progress == null) return;
            progress.Event_AnyChange.AddListener(OnThemeProgressAnyChange);

            Redraw();
        }

        protected virtual void OnThemeProgressDeattach(CharacterThemeProgress progress)
        {
            if (progress == null) return;
            progress.Event_AnyChange.RemoveListener(OnThemeProgressAnyChange);
        }

        protected virtual void OnThemeProgressAnyChange()
        {
            Redraw();
        }
    }
}
