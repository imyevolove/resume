﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    public abstract class BasePlayerMasterVolumeUIHandler : Drawer<PlayerConfigurations.Volume>
    {
        [SerializeField] private PlayerVolume volumeType;

        public enum PlayerVolume
        {
            Audio,
            Music
        }

        protected virtual void Start()
        {
            PlayerConfigurations.Volume trg = null;

            switch (volumeType)
            {
                case PlayerVolume.Audio:
                    trg = Player.Local.Configurations.AudioVolume;
                    break;

                case PlayerVolume.Music:
                    trg = Player.Local.Configurations.MusicVolume;
                    break;
            }

            SetTarget(trg);
        }

        protected override void OnAttach(PlayerConfigurations.Volume target)
        {
            AddVolumeEventListeners(target);

            if (target != null)
                OnVolumeValueChanged(target.Value);
        }

        protected override void OnDeattach(PlayerConfigurations.Volume target)
        {
            RemoveVolumeEventListeners(target);
        }

        protected abstract void OnVolumeValueChanged(float value);
        
        private void AddVolumeEventListeners(PlayerConfigurations.Volume volume)
        {
            if (volume == null) return;
            volume.Event_ValueChanged.AddListener(OnVolumeValueChanged);
        }

        private void RemoveVolumeEventListeners(PlayerConfigurations.Volume volume)
        {
            if (volume == null) return;
            volume.Event_ValueChanged.RemoveListener(OnVolumeValueChanged);
        }
    }
}
