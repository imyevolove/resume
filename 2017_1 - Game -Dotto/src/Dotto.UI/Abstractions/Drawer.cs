﻿using UnityEngine;

namespace Dotto.UI
{
    /// <summary>
    /// Abstract drawer class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [SerializeField]
    public abstract class Drawer<T> : MonoBehaviour
    {
        public T Target { get; private set; }
        public bool HasTarget { get { return Target != null; } }
        
        protected virtual void Awake()
        {
        }

        public void SetTarget(T target)
        {
            if (target == null) return;

            OnDeattach(Target);
            Target = target;
            OnAttach(Target);
        }

        protected abstract void OnAttach(T target);
        protected abstract void OnDeattach(T target);
    }
}
