﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(AudioSource))]
    public abstract class BaseCharacterThemeManagerUI : Drawer<Player>
    {
        [SerializeField] protected AudioClip themePurchaseSuccessSound;
        [SerializeField] protected AudioClip themePurchaseFailureSound;
        [SerializeField] protected AudioClip themeApplySound;

        private bool m_IsDirty = false;

        private AudioSource m_AudioSource;
        protected AudioSource AudioSource
        {
            get
            {
                if (m_AudioSource == null)
                    m_AudioSource = GetComponent<AudioSource>();

                return m_AudioSource;
            }
        }

        public void Redraw()
        {
            m_IsDirty = false;
            Draw();
        }

        protected abstract void Draw();

        protected virtual void OnEnable()
        {
            if (m_IsDirty)
                Redraw();
        }

        protected override void OnAttach(Player target)
        {
            m_IsDirty = true;
            Redraw();

            AddPlayerEventListeners(target);
        }

        protected override void OnDeattach(Player target)
        {
            RemovePlayerEventListeners(target);
        }

        protected virtual void OnCharacterThemeChanged(CharacterTheme theme)
        {
            RedrawOrMarkAsDirty();
        }

        protected virtual void OnCharacterThemeUnlocked(CharacterTheme theme)
        {
            RedrawOrMarkAsDirty();
        }

        private void RedrawOrMarkAsDirty()
        {
            if (gameObject.activeSelf)
            {
                Redraw();
            }
            else
            {
                m_IsDirty = true;
            }
        }

        private void AddPlayerEventListeners(Player target)
        {
            if (target == null) return;
            target.Event_CharacterThemeChanged.AddListener(OnCharacterThemeChanged);
            target.CharacterThemeManager.Event_OnUnlock.AddListener(OnCharacterThemeUnlocked);
        }

        private void RemovePlayerEventListeners(Player target)
        {
            if (target == null) return;
            target.Event_CharacterThemeChanged.RemoveListener(OnCharacterThemeChanged);
            target.CharacterThemeManager.Event_OnUnlock.RemoveListener(OnCharacterThemeUnlocked);
        }

        protected void PlayThemePurchaseSuccessSound()
        {
            AudioSource.PlayOneShot(themePurchaseSuccessSound);
        }
        
        protected void PlayThemePurchaseFailureSound()
        {
            AudioSource.PlayOneShot(themePurchaseFailureSound);
        }
        
        protected void PlayThemeApplySound()
        {
            AudioSource.PlayOneShot(themeApplySound);
        }
    }
}
