﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.UI
{
    using Pooling;

    [Serializable]
    public abstract class BaseStageManagerUI : Drawer<StageInfo[]>
    {
        public readonly StageSelectEvent Event_OnStageSelected = new StageSelectEvent();
        public abstract StageInfo SelectedStage { get; }
        
        public class StageSelectEvent : UnityEvent<StageInfo>
        {
        }

        public abstract void Redraw();
        
        protected override void OnAttach(StageInfo[] target)
        {
            Redraw();
        }

        protected override void OnDeattach(StageInfo[] target)
        {
        }

        protected void DispatchStageSelectedEvent(StageInfo stage)
        {
            Event_OnStageSelected.Invoke(stage);
        }
    }
}
