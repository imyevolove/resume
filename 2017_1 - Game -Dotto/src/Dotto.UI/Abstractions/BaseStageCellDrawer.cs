﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Dotto.UI
{
    [Serializable]
    public abstract class BaseStageCellDrawer : Drawer<StageInfo>, IPointerClickHandler
    {
        public readonly ClickEvent Event_OnClick = new ClickEvent();

        [SerializeField] protected Animator Animator;

        private bool m_ActiveState;
        public bool ActiveState
        {
            get { return m_ActiveState; }
            set
            {
                m_ActiveState = value;
                OnActiveStateChanged(value);
            }
        }

        public class ClickEvent : UnityEvent<BaseStageCellDrawer>
        {
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Event_OnClick.Invoke(this);
        }

        public abstract void Redraw();

        protected virtual void OnEnable()
        {
            UpdateActiveState();
        }

        protected virtual void OnActiveStateChanged(bool state)
        {
            if (Animator == null) return;
            Animator.SetBool("active", state);
        }

        protected override void OnAttach(StageInfo target)
        {
            UpdateActiveState();
            Redraw();
        }

        protected override void OnDeattach(StageInfo target)
        {
        }

        private void UpdateActiveState()
        {
            if (Target == null) return;
            ActiveState = Player.Local.StageIsAvailable(Target);
        }
    }
}
