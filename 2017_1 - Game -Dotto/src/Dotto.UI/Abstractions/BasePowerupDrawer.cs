﻿using Dotto.Powerups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    public abstract class BasePowerupDrawer : Drawer<AppPowerup>
    {
        public void Redraw()
        {
            if (Target == null)
            {
                Debug.LogWarning("Can't redraw powerup. Target must be defined");
                return;
            }
            RedrawCore();
        }

        protected abstract void RedrawCore();

        protected override void OnAttach(AppPowerup target)
        {
            AddEventListeners(target);
            Redraw();
        }

        protected override void OnDeattach(AppPowerup target)
        {
            RemoveEventListeners(target);
        }

        protected virtual void OnPowerupShotsCountChanged(int shots)
        {
            Redraw();
        }

        protected virtual void OnPowerupChanged(AppPowerup powerup)
        {
            Redraw();
        }

        private void AddEventListeners(AppPowerup target)
        {
            if (target == null) return;
            //target.Event_ShotsChanged.AddListener(OnPowerupShotsCountChanged);
            target.Event_Changed.AddListener(OnPowerupChanged);
        }

        private void RemoveEventListeners(AppPowerup target)
        {
            if (target == null) return;
            //target.Event_ShotsChanged.RemoveListener(OnPowerupShotsCountChanged);
            target.Event_Changed.RemoveListener(OnPowerupChanged);
        }
    }
}
