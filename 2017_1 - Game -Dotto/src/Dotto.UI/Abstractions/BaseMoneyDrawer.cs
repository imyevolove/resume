﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.UI
{
    using Economy;

    [Serializable]
    public abstract class BaseMoneyDrawer : Drawer<Money>
    {
        public void Redraw()
        {
            if (Target == null)
            {
                Debug.Log("Target must be defined");
                return;
            }

            Draw();
        }

        protected abstract void Draw();

        protected virtual void OnMoneyValueChanged(int value)
        {
            Redraw();
        }

        protected override void OnAttach(Money target)
        {
            Redraw();

            if (target != null)
                target.Event_ValueChanged.AddListener(OnMoneyValueChanged);
        }

        protected override void OnDeattach(Money target)
        {
            if (target != null)
                target.Event_ValueChanged.RemoveListener(OnMoneyValueChanged);
        }
    }
}
