﻿using Dotto.Database;
using Dotto.Economy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class PriceMessage : MonoBehaviour
    {
        [SerializeField] protected Image currencyIconImage;
        [SerializeField] protected Text  priceValueText;

        public void Draw(CurrencyKind kind, int value)
        {
            currencyIconImage.sprite = ApplicationDatabase.Instance.Icons.CurrencyIcons.FindByKey(kind);
            priceValueText.text = value.ToString();
        }
    }
}
