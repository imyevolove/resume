﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Animator))]
    public class GameMenuUIHandler : ScreenLayout, IPointerClickHandler
    {
        [SerializeField] protected SliderStageManagerUI stagesDrawer;
        [SerializeField] protected Button removeAdsButton;

        [NonSerialized] private Animator m_Animator;
        public Animator Animator
        {
            get
            {
                if (m_Animator == null)
                    m_Animator = GetComponent<Animator>();

                return m_Animator;
            }
        }

        protected virtual void Start()
        {
            var stages = GameManager.Instance.stageDatabase.Stages.ToArray();
            stagesDrawer.SetTarget(stages);
            stagesDrawer.Event_OnStageSelected.AddListener(OnStageSelected);

            removeAdsButton.onClick.AddListener(RequestPurchaseAdsLocker);

            OnPLayerAdvertisementStateChanged(Player.Local.AdvertisementDisabled.Value);
            Player.Local.AdvertisementDisabled.Event_ValueChanged.AddListener(OnPLayerAdvertisementStateChanged);
        }

        public override void Show()
        {
            if (GameManager.Instance.IsLocked) return;

            if (GameManager.Instance.CurrentStage != null)
                stagesDrawer.CurrentStageIndex = GameManager.Instance.CurrentStage.Index;

            base.Show();
        }

        public override void Hide()
        {
            Animator.SetTrigger("Disable");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            SelectLevelAndExit();
        }

        private void OnStageSelected(StageInfo stage)
        {
            SelectLevelAndExit();
        }

        private void RequestPurchaseAdsLocker()
        {
            BillingResources.AdvertisementBlocker.Purchase();
        }

        private void OnPLayerAdvertisementStateChanged(bool disabled)
        {
            removeAdsButton.gameObject.SetActive(!disabled);
        }

        private void RequestHide()
        {
            base.Hide();
        }

        private void SelectLevelAndExit()
        {
            var selectedStage = stagesDrawer.SelectedStage;

            if (selectedStage == null || !Player.Local.StageIsAvailable(selectedStage))
                return;

            if (selectedStage != GameManager.Instance.CurrentStage)
                GameManager.Instance.LoadStage(selectedStage);

            Player.Local.Save();
            Hide();
        }
    }
}
