﻿using Dotto.Customization;
using Dotto.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    public class CharacterThemeManagerUI : BaseCharacterThemeManagerUI
    {
        [SerializeField] protected DetailedCharacterThemeDrawer itemPrefab;
        [SerializeField] protected RectTransform container;
        [SerializeField] protected BaseCharacterThemeDrawer selectedTheme;

        private List<DetailedCharacterThemeDrawer> m_Items = new List<DetailedCharacterThemeDrawer>();

        protected override void Awake()
        {
            base.Awake();
            BuildItems();
        }

        protected virtual void Start()
        {
            SetTarget(Player.Local);
        }

        protected override void Draw()
        {
            selectedTheme.SetTarget(Target.CurrentCharacterTheme);
            UpdateItems();
        }
        
        protected virtual void OnItemClick(DetailedCharacterThemeDrawer item)
        {
            var player = Player.Local;
            var theme = item.Target;

            if (player.CharacterThemeManager.IsUnlocked(theme))
            {
                if (player.CurrentCharacterTheme != theme)
                {
                    PlayThemeApplySound();
                    player.CurrentCharacterTheme = theme;
                }
            }
            else
            {
                if (player.PurchaseCharacterTheme(theme))
                {
                    PlayThemePurchaseSuccessSound();
                    player.CurrentCharacterTheme = theme;
                }
                else
                {
                    PlayThemePurchaseFailureSound();
                    item.TriggerFailureAnimation();
                }
            }
        }

        private void UpdateItems()
        {
            foreach (var item in m_Items)
                ApplyThemeToItem(item, Target.CharacterThemeManager);
        }

        private void ApplyThemeToItem(DetailedCharacterThemeDrawer item, ICharacterThemeManager manager)
        {
            DetailedCharacterThemeDrawer.DetalizationLevel detalization;
            
            if (Target.CurrentCharacterTheme == item.Target)
            {
                detalization = DetailedCharacterThemeDrawer.DetalizationLevel.Equipped;
            }
            else if (manager.IsUnlocked(item.Target))
            {
                detalization = DetailedCharacterThemeDrawer.DetalizationLevel.Unlocked;
            }
            else
            {
                detalization = DetailedCharacterThemeDrawer.DetalizationLevel.Locked;
            }

            item.Detalization = detalization;
        }

        private void BuildItem(CharacterTheme theme, CharacterThemeProgress progress)
        {
            var drawer = Instantiate(itemPrefab, container, false);
            drawer.SetTarget(theme);
            drawer.SetThemeProgress(progress);

            drawer.Event_OnClick.AddListener(() => 
            {
                OnItemClick(drawer);
            });

            m_Items.Add(drawer);
        }

        private void BuildItems()
        {
            var player = Player.Local;
            m_Items.Clear();

            foreach (var theme in ApplicationDatabase.Instance.Characters.Themes.OrderBy(theme => theme.price.kind).ThenBy(theme => theme.price.value))
            {
                var progress = player.CharacterThemeManager.GetProgress(theme);
                BuildItem(theme, progress);
            }
        }
    }
}
