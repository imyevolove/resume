﻿using Dotto.Customization;
using Dotto.Economy;
using Dotto.Powerups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.UI
{
    public static class PriceMessageExtensions
    {
        public static void Draw(this PriceMessage message, AppPowerup powerup)
        {
            message.Draw(powerup.price.kind, powerup.price.value - powerup.Investment);
        }

        public static void Draw(this PriceMessage message, ProductInvestment investment)
        {
            message.Draw(investment.Price.kind, investment.Price.value - investment.Investment);
        }
    }
}
