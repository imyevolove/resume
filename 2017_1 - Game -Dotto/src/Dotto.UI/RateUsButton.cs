﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(RectTransform))]
    public class RateUsButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] public string bundleName = "";

        public void OnPointerClick(PointerEventData eventData)
        {
            OpenMarketPage();
        }

        public void OpenMarketPage()
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=" + bundleName);
        }
    }
}
