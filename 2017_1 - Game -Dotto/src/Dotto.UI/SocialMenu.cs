﻿using Dotto.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class SocialMenu : MonoBehaviour
    {
        [SerializeField] public string socialGroupUrl;
        [SerializeField] public string socialDirectGamesGroupUrl;

        [SerializeField] private AsyncImage m_UserPhoto;
        [SerializeField] private Text m_UserName;
        [SerializeField] private SocialFriendsUI m_FriendsUI;
        
        public void Open()
        {
            gameObject.SetActive(true);

            UpdateUserUI();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OpenWithLogin()
        {

            if (GameVars.Social.IsLoggedIn)
            {
                Open();
                return;
            }

            var popup = PopupManager.Instance.awaitPopup;
            popup.Show();

            GameVars.Social.Login(info =>
            {
                popup.Cancel();

                if (!info.Status)
                {
                    Debug.LogError(info.Message);
                    return;
                }

                Open();
            });
        }

        public void ShowFriendsUI()
        {
            var popup = PopupManager.Instance.awaitPopup;

            var awaiter = GameVars.Social.GetFriends().GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                popup.Cancel();
                m_FriendsUI.Show(awaiter.GetResult());
            });
        }

        public void GoToSocialGroup()
        {
            Application.OpenURL(socialGroupUrl);
        }

        public void GoToSocialDirectGamesGroup()
        {
            Application.OpenURL(socialDirectGamesGroupUrl);
        }

        public void Logout()
        {
            GameVars.Social.Logout();
            Hide();
        }

        private void UpdateUserUI()
        {
            var userAwaiter = GameVars.Social.GetUser().GetAwaiter();
            userAwaiter.OnCompleted(() => 
            {
                var result = userAwaiter.GetResult();

                m_UserPhoto.LoadAsync(result.PhotoUrl);
                m_UserName.text = result.FullName;
            });
        }
    }
}
