﻿using System;
using UnityEngine;

namespace Dotto.UI
{
    [Serializable]
    public class MasterPlayerVolumeUIHandler : BasePlayerVolumeUIHandler
    {
        [SerializeField] private PlayerVolume volumeType;

        public enum PlayerVolume
        {
            Audio,
            Music
        }

        protected virtual void Start()
        {
            PlayerConfigurations.Volume trg = null;

            switch (volumeType)
            {
                case PlayerVolume.Audio:
                    trg = Player.Local.Configurations.AudioVolume;
                    break;

                case PlayerVolume.Music:
                    trg = Player.Local.Configurations.MusicVolume;
                    break;
            }

            SetTarget(trg);
        }
    }
}
