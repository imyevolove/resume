﻿using Dotto.Localization;
using Dotto.Popups;
using Dotto.Socials;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class SocialFriendBox : Drawer<UserInfo>
    {
        [SerializeField] private Image m_Icon;
        [SerializeField] private Text m_UserName;
        [SerializeField] private Button m_InviteButton;

        [Header("Localization")]
        [SerializeField] private string m_InviteSuccessLocalizationKey;

        private Coroutine m_PhotoLoaderCoroutine;

        protected override void Awake()
        {
            base.Awake();
            m_InviteButton.onClick.AddListener(InviteFriend);
        }

        protected override void OnAttach(UserInfo target)
        {
            m_UserName.text = target.FullName;

            DismissPhotoLoading();
            LoadUserPhoto();
        }

        protected override void OnDeattach(UserInfo target)
        {
            DismissPhotoLoading();
        }

        protected virtual void OnUserPhotoLoaded(Texture2D texture)
        {
            m_Icon.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
        }

        private void LoadUserPhoto()
        {
            if (!HasTarget || string.IsNullOrEmpty(Target.PhotoUrl)) return;
            
            var request = new WWW(Target.PhotoUrl);
            StartCoroutine(LoadUserPhotoRoutine(request));
        }

        private void InviteFriend()
        {
            if (!HasTarget) return;

            var popup = PopupManager.Instance.awaitPopup;
            popup.Show();

            var awaiter = Target.Social.InviteFriend(Target).GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                popup.Cancel();

                var result = awaiter.GetResult();

                var messagePopup = PopupManager.Instance.messageBox;
                messagePopup.Message = (result.Status ? LocalizationManager.LocalizationProvider.GetValue(m_InviteSuccessLocalizationKey).ToUpper() : result.Message);
                messagePopup.Show();
            });
        }

        private IEnumerator LoadUserPhotoRoutine(WWW request)
        {
            yield return request;

            if (request.texture != null)
            {
                OnUserPhotoLoaded(request.texture);
                DismissPhotoLoading();
            }
        }

        private void DismissPhotoLoading()
        {
            if (m_PhotoLoaderCoroutine == null) return;

            StopCoroutine(m_PhotoLoaderCoroutine);
            m_PhotoLoaderCoroutine = null;

        }
    }
}
