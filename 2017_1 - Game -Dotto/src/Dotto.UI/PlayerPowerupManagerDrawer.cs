﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dotto.Powerups;
using UnityEngine;
using Dotto.Pooling;
using Dotto.Economy;
using Dotto.Popups;
using Dotto.Ads;
using Dotto.Localization;

namespace Dotto.UI
{
    [Serializable]
    public class PlayerPowerupManagerDrawer : Drawer<PlayerPowerupManager>
    {
        [SerializeField] protected RectTransform powerupContainer;
        [SerializeField] protected PowerupDrawer powerupOriginal;

        [SerializeField] private string localizationWatchAdsPopupAsk;
        [SerializeField] private string localizationUsePowerupPopupAsk;

        private Dictionary<MarkedAppPowerup, PowerupDrawer> m_PowerupDrawers = new Dictionary<MarkedAppPowerup, PowerupDrawer>();
        private MonoPool<PowerupDrawer> m_PowerupPool;

        private Dictionary<CurrencyKind, Popup> m_ReactionPopup = new Dictionary<CurrencyKind, Popup>();

        protected override void Awake()
        {
            base.Awake();
            m_PowerupPool = new MonoPool<PowerupDrawer>(powerupOriginal);
            SetTarget(Player.Local.PowerupManager);
        }

        protected virtual void Start()
        {
            InitializeReactionPopups();
        }

        public void Rebuild()
        {
            foreach (var kvp in m_PowerupDrawers)
                TryRebuildPowerupDrawer(kvp.Key);

            m_PowerupPool.ReleaseAll();
            m_PowerupDrawers.Clear();

            if (Target == null)
            {
                Debug.LogWarning("Can't rebuild powerup manager drawer. Target must be defined");
                return;
            }

            foreach (var powerup in Target.Powerups)
                TryRebuildPowerupDrawer(powerup);
        }

        protected override void OnAttach(PlayerPowerupManager target)
        {
            Rebuild();
            AddEventListeners(target);
        }

        protected override void OnDeattach(PlayerPowerupManager target)
        {
            RemoveEventListeners(target);
        }

        protected virtual void OnPowerupAdded(MarkedAppPowerup powerup)
        {
            TryRebuildPowerupDrawer(powerup);
        }

        protected virtual void OnPowerupRemoved(MarkedAppPowerup powerup)
        {
            TryRemovePowerupDrawer(powerup);
        }

        private void TryRebuildPowerupDrawer(MarkedAppPowerup powerup)
        {
            if (m_PowerupDrawers.ContainsKey(powerup)) return;

            var powerupDrawer = m_PowerupPool.GetFree();
            powerupDrawer.transform.SetParent(powerupContainer, false);
            powerupDrawer.SetTarget(powerup);

            RemovePowerupDrawerEventListeners(powerupDrawer);
            AddPowerupDrawerEventListeners(powerupDrawer);
        }

        private void TryRemovePowerupDrawer(MarkedAppPowerup powerup)
        {
            PowerupDrawer drawer = null;
            if (m_PowerupDrawers.TryGetValue(powerup, out drawer))
            {
                m_PowerupPool.Release(drawer);
                RemovePowerupDrawerEventListeners(drawer);
            }
        }

        private void AddEventListeners(PlayerPowerupManager manager)
        {
            if (manager == null) return;
            manager.Event_PowerupAdded.AddListener(OnPowerupAdded);
            manager.Event_PowerupRemoved.AddListener(OnPowerupRemoved);
        }

        private void RemoveEventListeners(PlayerPowerupManager manager)
        {
            if (manager == null) return;
            manager.Event_PowerupAdded.RemoveListener(OnPowerupAdded);
            manager.Event_PowerupRemoved.RemoveListener(OnPowerupRemoved);
        }

        private void AddPowerupDrawerEventListeners(PowerupDrawer drawer)
        {
            if (drawer == null) return;
            drawer.Event_OnClick.AddListener(AnyPowerupClicked);
        }

        private void RemovePowerupDrawerEventListeners(PowerupDrawer drawer)
        {
            if (drawer == null) return;
            drawer.Event_OnClick.RemoveListener(AnyPowerupClicked);
        }

        private void AnyPowerupClicked(PowerupDrawer darwer)
        {
            var powerup = darwer.Target;
            var kind = powerup.price.kind;

            Popup popup = null;
            Action confirmAction = null;

            if (powerup.HasShots)
            {
                popup = PopupManager.Instance.usePowerupConfirmationPopup;

                var powerupLocalizedName = LocalizationManager.LocalizationProvider.GetValue("powerup_" + powerup.Name);
                popup.Message = LocalizationManager.LocalizationProvider.GetValue(localizationUsePowerupPopupAsk, powerupLocalizedName).ToUpper();

                confirmAction = () => 
                {
                    powerup.Use();
                    Player.Local.Save();
                };
            }
            else
            {
                m_ReactionPopup.TryGetValue(kind, out popup);
                confirmAction = () =>
                {
                    AdvertisementManager.Instance.ShowRewardedVideo((bool finished) =>
                    {
                        if (finished)
                        {
                            powerup.AddInvestement(1);
                            Debug.Log("Investement added");
                            Player.Local.Save();
                        }
                        else
                        {
                            Debug.LogWarning("Video ads not finished");
                        }
                    });
                };
            }

            if (popup != null)
            {
                var operation = popup.Show();
                operation.Event_Completed += (o) =>
                {
                    if (o.IsCanceled)
                    {
                        Debug.LogWarning("Canceled");
                    }
                    else
                    {
                        confirmAction.Invoke();
                    }
                };
            }
            else
            {
                Debug.LogWarningFormat("Can't show popup. Popup for '{0}' is not defined", kind);
            }
        }

        private void InitializeReactionPopups()
        {
            RegisterReactionPopup(CurrencyKind.VideoView, PopupManager.Instance.watchAdsPopup);
        }

        private void RegisterReactionPopup(CurrencyKind kind, Popup popup)
        {
            if (m_ReactionPopup.ContainsKey(kind))
            {
                m_ReactionPopup[kind] = popup;
            }
            else
            {
                m_ReactionPopup.Add(kind, popup);
            }
        }
    }
}
