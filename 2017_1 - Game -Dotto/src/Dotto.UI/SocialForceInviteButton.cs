﻿using Dotto.Socials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Dotto.UI
{
    [Serializable]
    public class SocialForceInviteButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] public string userId;

        public void OnPointerClick(PointerEventData eventData)
        {
            GameVars.Social.InviteFriend(new UserInfo(GameVars.Social, userId, "name", null));
        }
    }
}
