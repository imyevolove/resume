﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    public class PowerupDrawer : BasePowerupDrawer, IPointerClickHandler
    {
        public readonly PowerupEvent Event_OnClick = new PowerupEvent();

        public class PowerupEvent : UnityEvent<PowerupDrawer>
        {
        }

        [SerializeField] protected Image iconImage;
        [SerializeField] protected PriceMessage message;

        public void OnPointerClick(PointerEventData eventData)
        {
            Event_OnClick.Invoke(this);
        }

        protected override void RedrawCore()
        {
            message.gameObject.SetActive(!Target.HasShots);
            message.Draw(Target);
            iconImage.sprite = Target.icon;
        }
    }
}
