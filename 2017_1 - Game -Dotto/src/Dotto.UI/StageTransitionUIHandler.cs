﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Animator))]
    public class StageTransitionUIHandler : ScreenLayout, IPointerClickHandler
    {
        [SerializeField] protected Text StageIndexTextField;

        public Animator Animator { get; private set; }

        protected virtual void Awake()
        {
            Animator = GetComponent<Animator>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Hide();
        }

        public void Setup(int stageIndex)
        {
            StageIndexTextField.text = stageIndex.ToString();
        }

        public override void Show()
        {
            base.Show();
            Animator.SetBool("enabled", true);
        }

        public override void Hide()
        {
            Animator.SetBool("enabled", false);
        }

        private void KeyEvent_Hide()
        {
            base.Hide();
        }
    }
}
