﻿using Dotto.Pooling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(AudioSource))]
    public class SliderStageManagerUI : BaseStageManagerUI
    {
        [SerializeField] protected FallText levelText;
        [SerializeField] protected Button loadLevelButton;

        [SerializeField] private CircleSlider m_Slider;
        public CircleSlider Slider { get { return m_Slider; } }
        
        [NonSerialized] private AudioSource m_AudioSource;
        public AudioSource AudioSource
        {
            get
            {
                if (m_AudioSource == null)
                    m_AudioSource = GetComponent<AudioSource>();

                return m_AudioSource;
            }
        }

        [SerializeField] public float sliderThreshold = 10f;
        [SerializeField] public AudioClip changeLevelSound;
        
        public override StageInfo SelectedStage { get { return Target == null ? null : Target[CurrentStageIndex]; } }

        private float m_DirectionChangeThreshold = 0f;
        private float m_SliderThresholdCounter = 0f;
        private bool m_SliderForwardDirection = true;
        private int m_CurrentStageIndex;
        public int CurrentStageIndex
        {
            get { return m_CurrentStageIndex; }
            set
            {
                var old = m_CurrentStageIndex;
                m_CurrentStageIndex = Target != null 
                        ? Mathf.Clamp(value, 0, Mathf.Min(Target.Length - 1, Player.Local.MaxCompletedLevel))
                        : value;

                if(old != m_CurrentStageIndex)
                    OnStageIndexChanged(m_CurrentStageIndex);
            }
        }

        private MonoPool<FallText> m_TextPool;
        private FallText m_PreviousText;
        private int m_LastDrawedIndex = -1;

        protected override void Awake()
        {
            base.Awake();

            levelText.DisableObject();
            m_TextPool = new MonoPool<FallText>(levelText);

            Slider.Event_ValueChanged.AddListener(OnSliderValueChanged);
            loadLevelButton.onClick.AddListener(DispatchStageSelectedEvent);
        }

        protected virtual void OnEnable()
        {
            Redraw();
        }

        public override void Redraw()
        {
            if (Target == null) return;
            if (m_LastDrawedIndex == CurrentStageIndex) return;
            
            // Not first call
            if (m_LastDrawedIndex >= 0)
                PlayChangeLevelSound();

            m_LastDrawedIndex = CurrentStageIndex;
            PushText((CurrentStageIndex + 1).ToString());
        }

        protected override void OnAttach(StageInfo[] target)
        {
            CurrentStageIndex = Player.Local.MaxCompletedLevel;
            base.OnAttach(target);
        }

        protected virtual void OnSliderValueChanged(CircleSlider.SliderEventData eventData)
        {
            UpdateThreshold(eventData.Delta);
            CheckLevelChange();
        }

        protected virtual void OnStageIndexChanged(int index)
        {
        }

        private void PlayChangeLevelSound()
        {
            if (changeLevelSound == null) return;
            AudioSource.PlayOneShot(changeLevelSound);
        }

        private void PushText(string text)
        {
            if (m_PreviousText != null)
                m_PreviousText.Disable();

            var obj = m_TextPool.GetFree();
            obj.transform.SetParent(levelText.transform.parent, false);
            obj.text = text;
            m_PreviousText = obj;
        }

        private void CheckLevelChange()
        {
            if (m_SliderThresholdCounter >= sliderThreshold)
            {
                m_SliderThresholdCounter = 0;
                CurrentStageIndex += m_SliderForwardDirection ? 1 : -1;

                Redraw();
            }
        }

        private void UpdateThreshold(float delta)
        {
            var sameSign = m_SliderForwardDirection == IsForwardDirection(delta);

            if (!sameSign)
            {
                if (m_DirectionChangeThreshold > 0)
                {
                    m_DirectionChangeThreshold -= Math.Abs(delta);
                }
                else
                {
                    m_DirectionChangeThreshold = 0.2f;
                    m_SliderForwardDirection = delta >= 0;
                    m_SliderThresholdCounter = 0;
                }
            }
            else
            {
                m_SliderThresholdCounter += Math.Abs(delta);
            }
            
        }

        private int GetDirection(float value)
        {
            return value < 0 ? -1 : +1;
        }

        private bool IsForwardDirection(float value)
        {
            return value >= 0;
        }

        private void DispatchStageSelectedEvent()
        {
            var stage = Target[CurrentStageIndex];
            DispatchStageSelectedEvent(stage);
        }
    }
}
