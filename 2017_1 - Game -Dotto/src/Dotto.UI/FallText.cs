﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Animator))]
    public class FallText : Text
    {
        private Animator m_Animator;
        public Animator Animator
        {
            get
            {
                if (m_Animator == null)
                    m_Animator = GetComponent<Animator>();

                return m_Animator;
            }
        }
        
        public void Disable()
        {
            Animator.SetTrigger("Disable");
        }

        private void RequestDisable()
        {
            gameObject.SetActive(false);
        }
    }
}
