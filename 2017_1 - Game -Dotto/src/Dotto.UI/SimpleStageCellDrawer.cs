﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class SimpleStageCellDrawer : BaseStageCellDrawer
    {
        [SerializeField] protected Text LevelTextField;

        public override void Redraw()
        {
            LevelTextField.text = (Target.Index + 1).ToString();
        }
    }
}
