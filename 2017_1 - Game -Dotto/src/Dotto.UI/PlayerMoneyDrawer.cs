﻿using Dotto.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class PlayerMoneyDrawer : BaseMoneyDrawer
    {
        [SerializeField] protected Text valueText;
        [SerializeField] public float fadeTime;

        private int m_DrawValue;
        private Coroutine m_AnimCoroutine;

        protected override void Awake()
        {
            base.Awake();

            m_DrawValue = Player.Local.Money.Value;
            SetTarget(Player.Local.Money);
            RedrawValue();
        }

        protected override void Draw()
        {
            if (m_DrawValue == Target.Value) return;
            StartAnimation();
        }

        private IEnumerator MoneyAnim(int destinationValue)
        {
            var fadeLimit = fadeTime;
            var counter = 0f;
            var time = 0f;
            var tmpPreviousValue = m_DrawValue;

            while (counter < fadeLimit)
            {
                counter += Time.deltaTime;
                time = counter / fadeLimit;

                m_DrawValue = (int)Mathf.SmoothStep(tmpPreviousValue, destinationValue, time);
                RedrawValue();

                yield return null;
            }

            m_DrawValue = destinationValue;
            RedrawValue();
        }

        private void RedrawValue()
        {
            valueText.text = m_DrawValue.ToString();
        }

        private void StartAnimation()
        {
            StopAnimation();
            m_AnimCoroutine = StartCoroutine(MoneyAnim(Player.Local.Money.Value));
        }

        private void StopAnimation()
        {
            if (m_AnimCoroutine == null) return;
            StopCoroutine(m_AnimCoroutine);
            m_AnimCoroutine = null;
        }
    }
}
