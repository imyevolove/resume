﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class OverlayableCharacterThemeDrawer : CharacterThemeDrawer
    {
        [SerializeField] protected Image overlayImage;
        [SerializeField] protected float overlayTime;
        
        protected readonly Color OverlayHiddenColor = new Color(1, 1, 1, 0);
        protected readonly Color OverlayShowenColor = new Color(1, 1, 1, 1);

        private Coroutine m_OverlayCoroutine;
        private bool m_IsFirstDraw = true;

        protected virtual void OnEnable()
        {
            if (Target == null) return;

            StopAnimation();
            ChangeImageAndHideOverlayImmediate();
        }

        protected override void Draw()
        {
            if (Target == null) return;
            base.Draw();

            if (m_IsFirstDraw)
            {
                m_IsFirstDraw = false;
                ChangeImageAndHideOverlayImmediate();
            }
            else
            {
                var previousSprite = characterImage.sprite;
                characterImage.sprite = previousSprite;
                
                if (m_OverlayCoroutine == null)
                    StartAnimation();
            }
        }

        private void HideOverlay()
        {
            overlayImage.color = OverlayHiddenColor;
        }

        private void ChangeImageImmediate()
        {
            characterImage.sprite = Target.CharacterImage;
        }

        private void ChangeImageAndHideOverlayImmediate()
        {
            ChangeImageImmediate();
            HideOverlay();
        }

        private IEnumerator OverlayAnim()
        {
            var timeLimit = overlayTime / 2f;
            var forwardCounter = timeLimit;
            var backwardCounter = timeLimit;

            var workColor = OverlayShowenColor;

            while (forwardCounter > 0)
            {
                forwardCounter -= Time.deltaTime;

                workColor.a = 1 - forwardCounter / timeLimit;
                overlayImage.color = workColor;

                yield return null;
            }

            ChangeImageImmediate();

            while (backwardCounter > 0)
            {
                backwardCounter -= Time.deltaTime;

                workColor.a = backwardCounter / timeLimit;
                overlayImage.color = workColor;

                yield return null;
            }

            HideOverlay();
            StopAnimation();
        }

        private void StartAnimation()
        {
            StopAnimation();
            m_OverlayCoroutine = StartCoroutine(OverlayAnim());
        }

        private void StopAnimation()
        {
            if (m_OverlayCoroutine == null) return;
            StopCoroutine(m_OverlayCoroutine);
            m_OverlayCoroutine = null;
        }
    }
}
