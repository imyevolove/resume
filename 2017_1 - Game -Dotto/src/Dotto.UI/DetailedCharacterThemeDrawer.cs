﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Animator))]
    public class DetailedCharacterThemeDrawer : CharacterThemeDrawer
    {
        [SerializeField] protected PriceMessage priceMessage;

        public enum DetalizationLevel
        {
            Unlocked = 0,
            Locked = 1,
            Equipped = 2
        }

        private DetalizationLevel m_Detalization;
        public DetalizationLevel Detalization
        {
            get { return m_Detalization; }
            set
            {
                m_Detalization = value;
                OnDetalizationChanged();
            }
        }

        private Animator m_Animator;

        public void TriggerFailureAnimation()
        {
            m_Animator.SetTrigger("Failure");
        }

        protected virtual void OnEnable()
        {
            ApplyAnimatorDetalizationLevel();
        }

        protected override void Awake()
        {
            base.Awake();
            m_Animator = GetComponent<Animator>();
        }

        protected override void Draw()
        {
            base.Draw();

            if (ThemeProgress == null)
            {
                priceMessage.Draw(Target.price.kind, Target.price.value);
            }
            else
            {
                priceMessage.Draw(ThemeProgress.Investment);
            }
            
            ApplyAnimatorDetalizationLevel();
        }

        protected virtual void OnDetalizationChanged()
        {
            Redraw();
        }

        private void ApplyAnimatorDetalizationLevel()
        {
            m_Animator.SetInteger("Level", (int)Detalization);
        }
    }
}
