﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    public class PlayerVolumeUIHandler : BasePlayerMasterVolumeUIHandler, IPointerClickHandler
    {
        [SerializeField] protected Sprite enabledIcon;
        [SerializeField] protected Sprite disabledIcon;
        [SerializeField] protected Image targetImage;
        [SerializeField] protected Image sliceImage;
        [SerializeField] protected float sliceAnimationSpeed;

        private Coroutine m_SliceCoroutine;

        public void OnPointerClick(PointerEventData eventData)
        {
            Target.Value = Target.Value <= 0 ? 1 : 0;
        }

        protected virtual void OnEnable()
        {
            StartSliceAnimation();
        }

        protected override void OnVolumeValueChanged(float value)
        {
            StartSliceAnimation();
            UpdateIcon();
        }

        private void UpdateIcon()
        {
            if (Target == null) return;
            targetImage.sprite = Target.Value <= 0 ? disabledIcon : enabledIcon;
        }

        private void StartSliceAnimation()
        {
            if (Target == null) return;
            
            var to = Mathf.Lerp(0, 1, Target.Value);

            StopSliceAnimation();
            m_SliceCoroutine = StartCoroutine(SliceAnimation(to));
        }

        private void StopSliceAnimation()
        {
            if (m_SliceCoroutine != null)
            {
                StopCoroutine(m_SliceCoroutine);
                m_SliceCoroutine = null;
            }
        }

        private IEnumerator SliceAnimation(float to)
        {
            var time = 0f;
            while (sliceImage.fillAmount != to)
            {
                time += Time.deltaTime * sliceAnimationSpeed;
                sliceImage.fillAmount = Mathf.SmoothStep(sliceImage.fillAmount, to, time);
                
                yield return null;
            }
        }
    }
}
