﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Animator))]
    public class SwitchOption : MonoBehaviour, IPointerClickHandler
    {
        private Animator m_Animator;
        public Animator Animator
        {
            get
            {
                if (m_Animator == null)
                    m_Animator = GetComponent<Animator>();
                return m_Animator;
            }
        }

        [SerializeField] private bool m_Value;
        public virtual bool Value
        {
            get { return m_Value; }
            set
            {
                m_Value = value;

                OnValueChanged(value);
                UpdateAnimator();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Value = !Value;
        }

        protected virtual void OnEnable()
        {
            UpdateAnimator();
        }

        protected virtual void OnValueChanged(bool value)
        {
        }

        protected virtual void UpdateAnimator()
        {
            Animator.SetBool("Enabled", Value);
        }
    }
}
