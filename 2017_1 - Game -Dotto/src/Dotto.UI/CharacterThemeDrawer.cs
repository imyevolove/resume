﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.UI
{
    [Serializable]
    public class CharacterThemeDrawer : BaseCharacterThemeDrawer
    {
        [SerializeField] protected Image outlineImage;
        [SerializeField] protected Image characterImage;

        protected override void Draw()
        {
            characterImage.sprite = Target.CharacterImage;
            outlineImage.color = Target.CharacterColor;
        }
    }
}
