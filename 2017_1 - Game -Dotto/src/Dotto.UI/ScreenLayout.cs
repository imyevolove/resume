﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.UI
{
    [Serializable]
    [RequireComponent(typeof(RectTransform))]
    [DisallowMultipleComponent]
    public class ScreenLayout : MonoBehaviour
    {
        public readonly ActiveChangedEvent Event_ActiveChanged = new ActiveChangedEvent();

        public class ActiveChangedEvent : UnityEvent<bool>
        {
        }

        public void Show(Action onHide)
        {
            if (onHide != null)
            {
                UnityAction<bool> act = null;
                act = (state) =>
                {
                    if (state) return; /// Если объект был включен, то пропускаем действие
                    Event_ActiveChanged.RemoveListener(act);
                    onHide.Invoke();
                };

                Event_ActiveChanged.AddListener(act);
            }

            Show();
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
            DispatchActiveChangedEvent();
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
            DispatchActiveChangedEvent();
        }

        private void DispatchActiveChangedEvent()
        {
            if (Event_ActiveChanged == null) return;
            Event_ActiveChanged.Invoke(gameObject.activeSelf);
        }
    }
}
