﻿using Dotto.Economy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public sealed class AppPowerupFactory
    {
        private AppPowerupFactory()
        {
        }

        private delegate AppPowerup PowerupCreatorFunction();

        private static Dictionary<PowerupId, PowerupAction> m_CreatorFunctions = new Dictionary<PowerupId, PowerupAction>
        {
            /// SKIP LEVEL POWERUP
            {
                PowerupId.SkipLevel,
                () => { GameManager.Instance.SkipLevel(); }
            }
        };

        public static AppPowerup Create(PowerupId id)
        {
            return Create(id, 0);
        }

        public static AppPowerup Create(PowerupId id, int shots)
        {
            return Create(id, default(Price), shots);
        }

        public static AppPowerup Create(PowerupId id, Price price)
        {
            return Create(id, price, 0);
        }

        public static AppPowerup Create(PowerupId id, Price price, int shots)
        {
            AppPowerup powerup = null;
            PowerupAction action = null;

            if (m_CreatorFunctions.TryGetValue(id, out action))
            {
                powerup = new AppPowerupBuilder(action)
                    .SetPrice(price)
                    .SetShots(shots)
                    .Build();
            }

            return powerup;
        }
    }
}
