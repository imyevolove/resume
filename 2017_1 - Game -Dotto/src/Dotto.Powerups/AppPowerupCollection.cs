﻿using Dotto.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    [Serializable]
    public class AppPowerupCollection : PowerupCollection<AppPowerup>
    {
        public readonly PowerupCollectionEvent<AppPowerup> Event_PowerupChanged = new PowerupCollectionEvent<AppPowerup>();

        public override void Add(AppPowerup powerup)
        {
            if (powerup == null) return;

            var founded = Powerups.FirstOrDefault(p => p.Equals(powerup));
            if (founded != null)
            {
                founded.TargetPowerup.Shots += powerup.TargetPowerup.Shots;
                Event_PowerupChanged.Invoke(founded);
                return;
            }

            base.Add(powerup);
            Event_PowerupChanged.Invoke(powerup);
        }
    }
}
