﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Powerups
{
    [Serializable]
    public class LimitedPowerup : Powerup, ILimitedPowerup
    {
        private readonly LimitedPowerupShotsChangedEvent m_Event_ShotsChanged = new LimitedPowerupShotsChangedEvent();
        public LimitedPowerupShotsChangedEvent Event_ShotsChanged { get { return m_Event_ShotsChanged; } }

        [SerializeField]
        [Range(0, 100, order = 1)]
        private int m_Shots;
        public int Shots
        {
            get { return m_Shots; }
            set
            {
                m_Shots = value < 0 ? 0 : value;
                DispatchShotsChangedEvents();
            }
        }

        public bool HasShots { get { return Shots > 0; } }

        public LimitedPowerup(PowerupAction action) : this(action, 0)
        {
        }

        public LimitedPowerup(PowerupAction action, int shots) : base(action)
        {
            Shots = shots;
        }
        
        public override bool Use()
        {
            if (!HasShots) return false;
            Shots--;

            return base.Use();
        }

        private void DispatchShotsChangedEvents()
        {
            Event_ShotsChanged.Invoke(Shots);
        }
    }
}
