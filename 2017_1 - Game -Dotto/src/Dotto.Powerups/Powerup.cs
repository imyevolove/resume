﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public class Powerup : IPowerup
    {
        protected PowerupAction action;

        public string Name { get; set; }

        public Powerup(PowerupAction action)
        {
            if (action == null)
                throw new ArgumentNullException("Use action is null");

            this.action = action;
        }
        public virtual bool Use()
        {
            if (action == null)
                return false;

            action.Invoke();
            return true;
        }
    }
}
