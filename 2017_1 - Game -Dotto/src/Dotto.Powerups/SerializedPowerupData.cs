﻿using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    [Serializable]
    public class SerializedPowerupData : ISerializable
    {
        public PowerupId id;
        public int shots;
        public int investment;

        public void Serialization(SerializationContainer container)
        {
            container["id"] = id;
            container["shots"] = shots;
            container["investment"] = investment;
        }

        public void Deserialization(SerializationContainer container)
        {
            container.Deserialize("id", ref id);
            container.Deserialize("shots", ref shots);
            container.Deserialize("investment", ref investment);
        }
    }
}
