﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public class PowerupBuilder : IPowerupBuilder<Powerup>
    {
        private PowerupAction m_PowerupAction;

        public void SetAction(PowerupAction action)
        {
            m_PowerupAction = action;
        }

        public Powerup Build()
        {
            return new Powerup(m_PowerupAction);
        }
    }
}
