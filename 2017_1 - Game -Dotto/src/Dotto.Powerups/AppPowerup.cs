﻿using Dotto.Economy;
using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.Powerups
{
    [Serializable]
    public class AppPowerup : ILimitedPowerup
    {
        public LimitedPowerup TargetPowerup { get; private set; }

        public LimitedPowerupShotsChangedEvent Event_ShotsChanged { get { return TargetPowerup.Event_ShotsChanged; } }
        public readonly AppPowerupEvent Event_Changed = new AppPowerupEvent();

        public class AppPowerupEvent : UnityEvent<AppPowerup>
        {
        }

        public Sprite icon;
        public Price price;
        public int Shots {
            get { return TargetPowerup.Shots; }
            set { TargetPowerup.Shots = value; }
        }

        public int m_Investment;
        public int Investment
        {
            get { return m_Investment; }
            set
            {
                m_Investment = value < 0 ? 0 : value;

                if (m_Investment >= price.value)
                {
                    var addShots = Mathf.FloorToInt(m_Investment / price.value);
                    m_Investment = 0;
                    Shots += addShots;
                }

                DispatchChangedEvent();
            }
        }
        
        public bool HasShots { get { return TargetPowerup.HasShots; } }

        public string Name
        {
            get { return TargetPowerup.Name; }
            set { TargetPowerup.Name = value; }
        }

        public AppPowerup(LimitedPowerup powerup) : this(powerup, default(Price))
        {
        }

        public AppPowerup(LimitedPowerup powerup, Price price)
        {
            TargetPowerup = powerup;
            this.price.Past(price);

            Event_ShotsChanged.AddListener((shots) => { DispatchChangedEvent(); });
        }

        public void AddInvestement(int count)
        {
            Investment += count;
        }

        public bool Use()
        {
            return TargetPowerup.Use();
        }

        private void DispatchChangedEvent()
        {
            Event_Changed.Invoke(this);
        }
    }
}
