﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public class PowerupCollection<T> : IPowerupCollection<T>
        where T : IPowerup
    {
        private List<T> m_Powerups = new List<T>();
        protected List<T> Powerups { get { return m_Powerups; } }

        public T this[int index] { get { return Powerups[index]; } }
        public int Count { get { return Powerups.Count; } }

        private PowerupCollectionEvent<T> m_Event_PowerupAdded = new PowerupCollectionEvent<T>();
        public PowerupCollectionEvent<T> Event_PowerupAdded { get { return m_Event_PowerupAdded; } }

        private PowerupCollectionEvent<T> m_Event_PowerupRemoved = new PowerupCollectionEvent<T>();
        public PowerupCollectionEvent<T> Event_PowerupRemoved { get { return Event_PowerupRemoved; } }

        public bool Contains(T powerup)
        {
            return Powerups.Contains(powerup);
        }

        public virtual void Add(T powerup)
        {
            if (powerup == null)
                throw new ArgumentNullException("Powerup must be defined");

            Powerups.Add(powerup);
            Event_PowerupAdded.Invoke(powerup);
        }

        public virtual void RemoveAll()
        {
            foreach (var powerup in m_Powerups)
                Remove(powerup);
        }

        public virtual bool Remove(T powerup)
        {
            var result = Powerups.Remove(powerup);

            if (result)
                Event_PowerupRemoved.Invoke(powerup);

            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Powerups.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
