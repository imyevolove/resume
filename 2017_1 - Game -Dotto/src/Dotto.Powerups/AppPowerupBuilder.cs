﻿using Dotto.Economy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Powerups
{
    public class AppPowerupBuilder : IPowerupBuilder<AppPowerup>
    {
        private AppPowerup m_Powerup;

        public AppPowerupBuilder(PowerupAction action)
        {
            var subPowerup = new LimitedPowerup(action);
            m_Powerup = new AppPowerup(subPowerup);
        }

        public AppPowerupBuilder SetShots(int shots)
        {
            m_Powerup.TargetPowerup.Shots = shots;
            return this;
        }

        public AppPowerupBuilder SetPrice(Price price)
        {
            m_Powerup.price = price;
            return this;
        }

        public AppPowerupBuilder SetIcon(Sprite sprite)
        {
            m_Powerup.icon = sprite;
            return this;
        }

        public AppPowerup Build()
        {
            return m_Powerup;
        }
    }
}
