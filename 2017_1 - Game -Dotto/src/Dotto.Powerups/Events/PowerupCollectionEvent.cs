﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Dotto.Powerups
{
    public class PowerupCollectionEvent<T> : UnityEvent<T> where T : IPowerup
    {
    }
}
