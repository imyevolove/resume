﻿using Dotto.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.Powerups
{
    public class PlayerPowerupManager : ISerializable
    {
        public readonly PowerupManagerEvent Event_PowerupAdded = new PowerupManagerEvent();
        public readonly PowerupManagerEvent Event_PowerupRemoved = new PowerupManagerEvent();
        public readonly PowerupManagerEvent Event_PowerupChanged = new PowerupManagerEvent();
        
        public class PowerupManagerEvent : UnityEvent<MarkedAppPowerup>
        {
        }

        private Dictionary<PowerupId, MarkedAppPowerup> m_PowerupDictionary = new Dictionary<PowerupId, MarkedAppPowerup>();
        public MarkedAppPowerup[] Powerups { get { return m_PowerupDictionary.Values.ToArray(); } }

        public void Add(MarkedAppPowerup powerupContainer)
        {
            if (powerupContainer == null)
                throw new ArgumentNullException("Powerup container must be defined");

            MarkedAppPowerup foundedContainer = null;
            if (m_PowerupDictionary.TryGetValue(powerupContainer.Id, out foundedContainer))
            {
                foundedContainer.Powerup.Shots += powerupContainer.Powerup.Shots;
                Event_PowerupChanged.Invoke(foundedContainer);
            }
            else
            {
                m_PowerupDictionary.Add(powerupContainer.Id, powerupContainer);
                Event_PowerupChanged.Invoke(powerupContainer);
            }
        }
        
        public void Serialization(SerializationContainer container)
        {
            var data = new List<SerializationContainer>();

            foreach (var powerupContainer in m_PowerupDictionary.Values)
            {
                var dataItem = new SerializedPowerupData
                {
                    id = powerupContainer.Id,
                    shots = powerupContainer.Powerup.Shots,
                    investment = powerupContainer.Powerup.Investment
                };
                data.Add(SerializationContainer.Serialize(dataItem));
            }

            container["powerups"] = data;
        }

        public void Deserialization(SerializationContainer container)
        {
            /// Try deserialize player configurations
            try
            {
                var data = new List<SerializationContainer>();
                container.Deserialize("powerups", ref data);

                foreach (var dataItem in data)
                {
                    if (!dataItem.KeyValueIs<PowerupId>("id")) continue;

                    var powerupContainer = m_PowerupDictionary.Values.FirstOrDefault(p => p.Id == (PowerupId)dataItem["id"]);
                    if (powerupContainer == null) continue;

                    var powerupData = new SerializedPowerupData();
                    powerupData.Deserialization(dataItem);

                    powerupContainer.Powerup.Shots = powerupData.shots;
                    powerupContainer.Powerup.Investment = powerupData.investment;
                }

            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
            }
        }
    }
}
