﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public interface ILimitedPowerup : IPowerup
    {
        LimitedPowerupShotsChangedEvent Event_ShotsChanged { get; }

        int Shots { get; set; }
        bool HasShots { get; }
    }
}
