﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public interface IPowerupBuilder<T> where T : IPowerup
    {
        T Build();
    }
}
