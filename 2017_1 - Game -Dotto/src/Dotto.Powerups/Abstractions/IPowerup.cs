﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public interface IPowerup
    {
        string Name { get; set; }
        bool Use();
    }
}
