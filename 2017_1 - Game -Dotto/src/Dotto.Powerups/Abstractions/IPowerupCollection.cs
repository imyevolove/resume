﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Powerups
{
    public interface IPowerupCollection<T> : IEnumerable<T> where T : IPowerup
    {
        PowerupCollectionEvent<T> Event_PowerupAdded { get; }
        PowerupCollectionEvent<T> Event_PowerupRemoved { get; }

        int Count { get; }
        T this[int index] { get; }

        bool Contains(T powerup);
        void Add(T powerup);
        bool Remove(T powerup);
        void RemoveAll();
    }
}
