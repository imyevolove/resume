﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dotto.Economy;
using Dotto.Serialization;

namespace Dotto.Powerups
{
    [Serializable]
    public class MarkedAppPowerup
    {
        public PowerupId Id { get; private set; }
        public AppPowerup Powerup { get; private set; }

        public MarkedAppPowerup(PowerupId id, AppPowerup powerup)
        {
            Id = id;
            Powerup = powerup;
        }

        public static implicit operator AppPowerup(MarkedAppPowerup powerup)
        {
            return powerup.Powerup;
        }
    }
}
