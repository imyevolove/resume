﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Areas
{
    public class AreaCollector
    {
        private List<LaunchArea> m_Areas = new List<LaunchArea>();

        public void UpdateReferences()
        {
            m_Areas.Clear();

            var areas = UnityEngine.Object.FindObjectsOfType<LaunchArea>();
            m_Areas.AddRange(areas);
        }

        public bool AllowedIn(Vector2 point)
        {
            if (m_Areas.Count == 0) return true;

            var fromAny = !m_Areas.Any(a => a.Mode == LaunchArea.DetectionMode.Allowed);
            var area = m_Areas.FirstOrDefault(a => a.InArea(point));

            return area == null ? fromAny : area.Mode == LaunchArea.DetectionMode.Allowed;
        }
    }
}
