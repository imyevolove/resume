﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Areas
{
    [Serializable]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class LaunchArea : MonoBehaviour, IArea
    {
        [SerializeField] private float m_Width;
        public float Width
        {
            get { return m_Width; }
            set
            {
                m_Width = value;
                UpdateArea();
            }
        }

        [SerializeField] private float m_Height;
        public float Height
        {
            get { return m_Height; }
            set
            {
                m_Height = value;
                UpdateArea();
            }
        }

        [SerializeField] public AreaTheme theme;
        [SerializeField] private DetectionMode m_Mode;
        public DetectionMode Mode
        {
            get { return m_Mode; }
            set
            {
                m_Mode = value;
                UpdateAreaColor();
            }
        }

        private SpriteRenderer m_Renderer;
        public SpriteRenderer Renderer
        {
            get
            {
                if (m_Renderer == null)
                    m_Renderer = GetComponent<SpriteRenderer>();

                return m_Renderer;
            }
        }

        private BoxCollider2D m_Collider;
        public BoxCollider2D Collider
        {
            get
            {
                if (m_Collider == null)
                    m_Collider = GetComponent<BoxCollider2D>();

                return m_Collider;
            }
        }

        public enum DetectionMode
        {
            Allowed,
            NotAllowed
        }

        public Bounds GetBounds()
        {
            return new Bounds(transform.position, new Vector3(Width, Height, 0));
        }

        public bool InArea(Vector2 point)
        {
            return GetBounds().Contains(point);
        }

        protected virtual void OnValidate()
        {
            if (Application.isPlaying) return;
            UpdateArea();
            UpdateAreaColor();
        }

        private void UpdateArea()
        {
            Renderer.size = new Vector2(Width, Height);
            Collider.size = new Vector2(Width, Height);
        }

        private void UpdateAreaColor()
        {
            Renderer.color = Mode == DetectionMode.Allowed ? theme.AllowedColor : theme.NotAllowedColor;
        }
    }
}
