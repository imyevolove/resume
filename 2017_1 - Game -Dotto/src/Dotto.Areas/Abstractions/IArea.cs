﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Areas
{
    public interface IArea
    {
        Bounds GetBounds();
        bool InArea(Vector2 point);
    }
}
