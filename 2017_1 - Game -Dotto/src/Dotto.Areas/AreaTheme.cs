﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Areas
{
    [Serializable]
    public struct AreaTheme
    {
        [SerializeField] private Color m_AllowedColor;
        public Color AllowedColor
        {
            get { return m_AllowedColor; }
            set { m_AllowedColor = value; }
        }

        [SerializeField] private Color m_NotAllowedColor;
        public Color NotAllowedColor
        {
            get { return m_NotAllowedColor; }
            set { m_NotAllowedColor = value; }
        }
    }
}
