﻿using Dotto.Socials.VK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using VK.Unity;

namespace Dotto.Socials
{
    public class VKSocial : ISocial
    {
        public bool IsLoggedIn => VKSDK.IsLoggedIn;
        public bool IsInitialized => VKSDK.IsInitialized;

        public UnityEvent OnLogin { get; private set; } = new UnityEvent();
        public UnityEvent OnLogout { get; private set; } = new UnityEvent();
        public UnityEvent OnInitialized { get; private set; } = new UnityEvent();

        public Scope[] Scopes { get; set; }

        private UserInfo m_CachedUserInfo;

        public VKSocial(params Scope[] scopes)
        {
            Scopes = scopes;
        }

        public void Initialize(Action callback)
        {
            VKSDK.Init(() => 
            {
                if (IsInitialized)
                {
                    OnInitialized.Invoke();
                }

                callback();
            });
        }


        public void Login(Action<SocialOperationInfo> callback)
        {
            VKSDK.Login(Scopes, (response) => 
            {
                if (IsLoggedIn)
                {
                    OnLogin.Invoke();
                }

                var isOk = !string.IsNullOrEmpty(response.accessToken);
                callback(new SocialOperationInfo(isOk, isOk ? "ok" : response.error.errorMessage));

                if (isOk)
                {
                    GetUser().GetAwaiter();
                }
            });
        }

        public void Logout()
        {
            VKSDK.Logout();
            OnLogout.Invoke();
        }

        public async Task<UserInfo[]> GetFriends()
        {
            List<UserInfo> users = new List<UserInfo>();
            
            var task = new TaskCompletionSource<UserInfo[]>();
            
            VKSDK.API(
                "apps.getFriendsList", 
                new Dictionary<string, string> { { "extended", "1" }, { "count", "100" }, { "type", "invite" }, { "fields", "photo_200" } },
                response =>
                {
                    var isOk = !string.IsNullOrEmpty(response.responseJsonString);
                    if (!isOk)
                    {
                        Debug.LogError($"Error: {response.error.errorMessage}");
                    }
                    else
                    {
                        try
                        {
                            var responseModel = JsonUtility.FromJson<VKUsersResponseModel>(response.responseJsonString);

                            foreach (var item in responseModel.response.items)
                            {
                                users.Add(new UserInfo(this, item.id, item.FullName, item.photo_200));
                            }
                        }
                        catch (Exception e) { Debug.LogError(e); }
                    }
                    
                    task.SetResult(users.ToArray());
                });

            return await task.Task;
        }

        public async Task<SocialOperationInfo> InviteFriend(UserInfo userInfo)
        {
            var task = new TaskCompletionSource<SocialOperationInfo>();

            VKSDK.API(
                "apps.sendRequest",
                new Dictionary<string, string> { { "user_id", userInfo.Id }, { "type", "invite " }, { "name", "invitation" }, { "key", "invitation_key" } },
                response =>
                {
                    var isOk = !string.IsNullOrEmpty(response.responseJsonString);
                    task.SetResult(new SocialOperationInfo(isOk, isOk ? "ok" : $"Error: {response.error.errorMessage}. Error code: {response.error.errorCode}."));
                });

            return await task.Task;
        }

        public async Task<UserInfo> GetUser()   
        {
            if (!string.IsNullOrEmpty(m_CachedUserInfo.Id) && m_CachedUserInfo.Id == VKSDK.UserId.ToString())
                return await Task.FromResult(m_CachedUserInfo);

            var task = new TaskCompletionSource<UserInfo>();
            UserInfo userInfo = default(UserInfo);

            VKSDK.API(
                "users.get",
                new Dictionary<string, string> { { "fields", "photo_200" } },
                response =>
                {
                    var isOk = !string.IsNullOrEmpty(response.responseJsonString);
                    if (!isOk)
                    {
                        Debug.LogError($"Error: {response.error.errorMessage}");
                    }
                    else
                    {
                        try
                        {
                            var responseModel = JsonUtility.FromJson<VKUsersResponseSingleModel>(response.responseJsonString);
                            var userModel = responseModel.response.FirstOrDefault();
                            userInfo = new UserInfo(this, userModel.id, userModel.FullName, userModel.photo_200);
                        }
                        catch (Exception e) { Debug.LogError(e); }
                    }

                    if (userInfo.Id == VKSDK.UserId.ToString())
                    {
                        m_CachedUserInfo = userInfo;
                    }

                    task.SetResult(userInfo);
                });

            return await task.Task;
        }
    }
}
