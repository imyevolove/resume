﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotto.Socials.VK.Models
{
    [Serializable]
    public struct VKUserModel
    {
        public string id;
        public string first_name;
        public string last_name;
        public string photo_200;

        public string FullName => $"{first_name} {last_name}";
    }
}
