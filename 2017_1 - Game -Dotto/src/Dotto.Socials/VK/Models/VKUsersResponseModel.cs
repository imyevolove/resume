﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotto.Socials.VK.Models
{
    [Serializable]
    public struct VKUsersResponseModel
    {
        public VKUserItemsResponseModel response;

        [Serializable]
        public struct VKUserItemsResponseModel
        {
            public int count;
            public VKUserModel[] items;
        }
    }
}
