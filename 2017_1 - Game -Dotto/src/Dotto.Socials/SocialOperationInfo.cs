﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Socials
{
    public struct SocialOperationInfo
    {
        public bool Status { get; private set; }
        public string Message { get; private set; }

        public SocialOperationInfo(bool status, string message)
        {
            Status = status;
            Message = message;
        }
    }
}
