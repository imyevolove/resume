﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Dotto.Socials
{
    [Serializable]
    public struct UserInfo
    {
        public string Id { get; private set; }
        public string FullName { get; private set; }
        public string PhotoUrl { get; private set; }

        public readonly ISocial Social;

        public UserInfo(ISocial social, string id, string name, string photoUrl)
        {
            Social = social;
            Id = id;
            FullName = name;
            PhotoUrl = photoUrl;
        }
    }
}
