﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;

namespace Dotto.Socials
{
    public interface ISocial
    {
        UnityEvent OnLogin { get; }
        UnityEvent OnLogout { get; }
        UnityEvent OnInitialized { get; }

        bool IsLoggedIn { get; }
        bool IsInitialized { get; }

        void Initialize(Action callback);

        void Login(Action<SocialOperationInfo> callback);
        void Logout();
        
        Task<UserInfo> GetUser();
        Task<UserInfo[]> GetFriends();
        Task<SocialOperationInfo> InviteFriend(UserInfo userInfo);
    }
}
