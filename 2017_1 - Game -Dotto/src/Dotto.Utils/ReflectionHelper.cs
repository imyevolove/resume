﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections;

namespace Dotto.Utils
{
    public class ReflectionHelper
    {
        public static bool IsAssignableFrom(Type first, Type second)
        {
#if NETFX_CORE
            return first.GetTypeInfo().IsAssignableFrom(second.GetTypeInfo());
#else
            return first.IsAssignableFrom(second);
#endif
        }

        public static bool IsEnum(Type type)
        {
#if NETFX_CORE
            return type.GetTypeInfo().IsEnum;
#else
            return type.IsEnum;
#endif
        }

        private static IEnumerable GetBaseTypes(Type type)
        {
            yield return type;
            Type baseType;

#if NETFX_CORE
            baseType = type.GetTypeInfo().BaseType;
#else
            baseType = type.BaseType;
#endif

            if (baseType != null)
            {
                foreach (var t in GetBaseTypes(baseType))
                {
                    yield return t;
                }
            }
        }

        public static PropertyInfo GetProperty(Type type, string name)
        {
#if NETFX_CORE
            return  GetBaseTypes(type).OfType<Type>()
                    .Select(baseType => baseType.GetTypeInfo().GetDeclaredProperty(name))
                    .FirstOrDefault(property => property != null);
#else
            return GetBaseTypes(type).OfType<Type>()
                    .Select(baseType => baseType.GetProperty(name))
                    .FirstOrDefault(property => property != null);
#endif
        }

        public static MethodInfo GetMethod(Type type, string name)
        {
#if NETFX_CORE
            return  GetBaseTypes(type).OfType<Type>()
                    .Select(baseType => baseType.GetTypeInfo().GetDeclaredMethod(name))
                    .FirstOrDefault(method => method != null);
#else
            return GetBaseTypes(type).OfType<Type>()
                    .Select(baseType => baseType.GetMethod(name))
                    .FirstOrDefault(method => method != null);
#endif

        }

        public static FieldInfo GetField(Type type, string name)
        {
#if NETFX_CORE
            return  GetBaseTypes(type).OfType<Type>()
                    .Select(baseType => baseType.GetTypeInfo().GetDeclaredField(name))
                    .FirstOrDefault(field => field != null);
#else
            return GetBaseTypes(type).OfType<Type>()
                    .Select(baseType => baseType.GetField(name))
                    .FirstOrDefault(field => field != null);
#endif
        }

        public static bool IsValueType(Type type)
        {
#if NETFX_CORE
            return type.GetTypeInfo().IsValueType;
#else
            return type.IsValueType;
#endif
        }

#if NETFX_CORE
        
        private static System.Collections.Generic.List<Assembly> loadedAssemblies;

        public static IEnumerable GetLoadedAssemblies()
        {
            // Check if cached.
            if (loadedAssemblies != null)
            {
                return loadedAssemblies;
            }

            // Find assemblies.
            //Windows.StorageFolder
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;

            loadedAssemblies = new System.Collections.Generic.List<Assembly>();

            //IAsyncOperation
            var folderFilesAsync = folder.GetFilesAsync();
            folderFilesAsync.AsTask().Wait();

            foreach (/*StorageFile*/ var file in folderFilesAsync.GetResults())
            {
                if (file.FileType == ".dll" || file.FileType == ".exe")
                {
                    try
                    {
                        var filename = file.Name.Substring(0, file.Name.Length - file.FileType.Length);
                        AssemblyName name = new AssemblyName { Name = filename };
                        Assembly asm = Assembly.Load(name);
                        loadedAssemblies.Add(asm);
                    }
                    catch (BadImageFormatException)
                    {
                        // Thrown reflecting on C++ executable files for which the C++ compiler stripped the relocation addresses (such as Unity dlls): http://msdn.microsoft.com/en-us/library/x4cw969y(v=vs.110).aspx
                    }
                }
            }

            return loadedAssemblies;
        }

        public static Type FindType(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return null;
            }

            // Split type name from .dll version:
            // Get start of "Version=…, Culture=…, PublicKeyToken=…" string.
            int versionIndex = fullName.IndexOf("Version", StringComparison.Ordinal);

            if (versionIndex >= 0)
            {
                // Get end of "Version=…, Culture=…, PublicKeyToken=…" string for generics.
                int endIndex = fullName.IndexOf(']', versionIndex);

                // Get end of "Version=…, Culture=…, PublicKeyToken=…" string for non-generics.
                endIndex = endIndex >= 0 ? endIndex : fullName.Length;

                // Remove version info.
                fullName = fullName.Remove(versionIndex - 2, endIndex - versionIndex + 2);
            }

            Type t = Type.GetType(fullName);

            if (t != null)
            {
                return t;
            }

            foreach (Assembly asm in GetLoadedAssemblies())
            {
                t = asm.GetType(fullName);
                if (t != null)
                {
                    return t;
                }
            }

            throw new TypeLoadException(string.Format("Unable to find type {0}.", fullName));
        }

#endif

    }

}