﻿using System;
using UnityEngine;

namespace Dotto.Utils
{
    /// <summary>
    /// Базовый контроллер
    /// </summary>
    [Serializable]
    public abstract class MonoSingleton<TController> : MonoBehaviour
        where TController : MonoSingleton<TController>
{
    private static TController m_Instance;
    public static TController Instance
    {
        get
        {
            if (m_Instance == null)
                m_Instance = FindObjectOfType<TController>();

            if (m_Instance == null)
                throw new NullReferenceException(string.Format("Mono singleton '{0}' not founded in scene. Please fix it", typeof(TController).FullName));

            return m_Instance;
        }
    }

    protected virtual void Awake()
    {
        if (m_Instance == null)
            m_Instance = this as TController;
    }
}
}
