﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Tutorials
{
    using Serialization;

    [Serializable]
    public class TutorialManager : MonoBehaviour, ISerializable
    {
        [SerializeField] private string m_DatabaseFileName;

        [SerializeField] private List<BaseTutorialBehaviour> m_TutorialBehaviours = new List<BaseTutorialBehaviour>();
        private List<ITutorial> m_Tutorials = new List<ITutorial>();

        protected List<ITutorial> Tutorials { get { return m_Tutorials; } }

        private List<string> m_CompletedTutorials = new List<string>();

        public int Count { get { return Tutorials.Count; } }

        protected virtual void Awake()
        {
            Load();

            foreach (var tutorial in m_TutorialBehaviours)
                Register(tutorial);
        }

        public bool Register(ITutorial tutorial)
        {
            if (tutorial == null)
                return false;

            if (Tutorials.Contains(tutorial))
                return true;

            RegisterTutorialEventListeners(tutorial);
            CompleteTutorialIfNeeded(tutorial);
            Tutorials.Add(tutorial);
            return true;
        }

        public bool Unregister(ITutorial tutorial)
        {
            var removeResult = Tutorials.Remove(tutorial);

            if(removeResult)
                RegisterTutorialEventListeners(tutorial);

            return removeResult;
        }

        public void Clear()
        {
            Tutorials.Clear();
        }

        public void Serialization(SerializationContainer container)
        {
            container["completed_tutorials"] = m_CompletedTutorials;
        }

        public void Deserialization(SerializationContainer container)
        {
            container.Deserialize("completed_tutorials", ref m_CompletedTutorials);
        }

        public void Save()
        {
            Storage.Save(this, m_DatabaseFileName);
        }

        public void Load()
        {
            Storage.Load(this, m_DatabaseFileName);
        }

        private void CompleteTutorialIfNeeded(ITutorial tutorial)
        {
            if (tutorial == null || tutorial.IsCompleted || string.IsNullOrEmpty(tutorial.TutorialName)) return;
            if (!m_CompletedTutorials.Contains(tutorial.TutorialName)) return;
            tutorial.Complete();
        }

        private void RegisterTutorialEventListeners(ITutorial tutorial)
        {
            if (tutorial == null) return;
            tutorial.Event_OnComplete.AddListener(OnAnyTutorialCompleted);
        }

        private void UnregisterTutorialEventListeners(ITutorial tutorial)
        {
            if (tutorial == null) return;
            tutorial.Event_OnComplete.RemoveListener(OnAnyTutorialCompleted);
        }

        private void OnAnyTutorialCompleted(ITutorial tutorial)
        {
            if (m_CompletedTutorials.Contains(tutorial.TutorialName)) return;
            m_CompletedTutorials.Add(tutorial.TutorialName);
            Save();
        }
    }
}
