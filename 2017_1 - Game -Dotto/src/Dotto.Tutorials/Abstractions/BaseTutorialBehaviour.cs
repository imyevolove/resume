﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Dotto.Tutorials
{
    [Serializable]
    public abstract class BaseTutorialBehaviour : MonoBehaviour, ITutorial
    {
        private readonly TutorialEvent m_Event_OnComplete = new TutorialEvent();
        public TutorialEvent Event_OnComplete { get { return m_Event_OnComplete; } }

        [SerializeField] private string m_TutorialName;
        public string TutorialName
        {
            get { return m_TutorialName; }
            set { m_TutorialName = value; }
        }

        public bool IsCompleted { get; private set; }

        public void Complete()
        {
            if (IsCompleted) return;
            IsCompleted = true;

            DispatchCompleteEvents();
        }

        public void SilentComplete()
        {
            IsCompleted = true;
        }

        public void Play()
        {
            if (IsCompleted) return;
            OnPlay();
        }

        protected abstract void OnPlay();
        protected abstract void OnCompleted();

        private void DispatchCompleteEvents()
        {
            OnCompleted();
            Event_OnComplete.Invoke(this);
        }
    }
}
