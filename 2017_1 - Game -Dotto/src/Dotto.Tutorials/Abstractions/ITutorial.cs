﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Dotto.Tutorials
{
    public interface ITutorial
    {
        TutorialEvent Event_OnComplete { get; }

        bool IsCompleted { get; }
        string TutorialName { get; set; }

        void Play();
        void Complete();
        void SilentComplete();
    }
}
