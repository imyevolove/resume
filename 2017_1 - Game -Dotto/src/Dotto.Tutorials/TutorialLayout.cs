﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dotto.Tutorials
{
    [Serializable]
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(RectTransform))]
    public class TutorialLayout : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public readonly PointerEvent Event_OnPointerClick = new PointerEvent();
        public readonly PointerEvent Event_OnPointerDown = new PointerEvent();
        public readonly PointerEvent Event_OnPointerUp = new PointerEvent();

        public class PointerEvent : UnityEvent<PointerEventData>
        {
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Event_OnPointerClick.Invoke(eventData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Event_OnPointerDown.Invoke(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Event_OnPointerUp.Invoke(eventData);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
