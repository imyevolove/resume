﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Dotto.Tutorials
{
    public class TutorialEvent : UnityEvent<ITutorial>
    {
    }
}
