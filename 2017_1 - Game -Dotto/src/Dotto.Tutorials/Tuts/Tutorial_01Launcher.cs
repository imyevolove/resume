﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Dotto.Tutorials
{
    [Serializable]
    public class Tutorial_01Launcher : BaseTutorialBehaviour
    {
        [SerializeField] protected TutorialLayout Container;

        protected virtual void Awake()
        {
            var manager = GameManager.Instance;

            UnityAction<StageInfo> levelCallback = null;
            levelCallback = (stage) => 
            {
                manager.Event_OnLevelChanged.RemoveListener(levelCallback);

                if (!IsCompleted)
                    Play();
            };
            manager.Event_OnLevelChanged.AddListener(levelCallback);
        }

        protected virtual void Update()
        {
            if (!IsCompleted)
            {
                if (Container.gameObject.activeSelf && Input.GetMouseButtonDown(0))
                {
                    Complete();
                }
            }
        }

        protected override void OnPlay()
        {
            Container.Show();
        }

        protected override void OnCompleted()
        {
            Container.Hide();
        }
    }
}
