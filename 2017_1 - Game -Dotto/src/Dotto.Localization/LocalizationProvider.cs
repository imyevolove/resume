﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Localization
{
    public class LocalizationProvider : ILocalizationProvider
    {
        public ILocalizationPackage LocalizationPackage { get; set; }

        public string GetValue(string key)
        {
            if(LocalizationPackage == null)
            {
                Debug.LogError("Can't get value from package. Package must be defined");
                return "";
            }
            
            return LocalizationPackage.GetValue(key);
        }

        public string GetValue(string key, params object[] additionalValues)
        {
            return string.Format(LocalizationPackage.GetValue(key), additionalValues);
        }
    }
}
