﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dotto.Localization
{
    [CreateAssetMenu(menuName = "Localization/Create Database")]
    public class LocalizationDatabase : ScriptableObject, ILocalizationDatabase
    {
        [SerializeField] private List<LocalizationPackage> m_Packages = new List<LocalizationPackage>();
        [SerializeField] private LocalizationScheme m_Scheme = new LocalizationScheme();
        public LocalizationScheme Scheme { get { return m_Scheme; } }

        /// <summary>
        /// Пакеты локализации
        /// </summary>
        public ILocalizationPackage[] Packages { get { return m_Packages.ToArray(); } }

        public SystemLanguage[] AvailableLanguages { get { return m_Packages.Select(p => p.LanguageCode).ToArray(); } }

        protected virtual void OnEnable()
        {
            foreach (var package in m_Packages)
                package.Scheme = m_Scheme;
        }

        public LocalizationPackage CreateNewPackage(SystemLanguage languageCode)
        {
            var package = new LocalizationPackage(m_Scheme, languageCode);
            m_Packages.Add(package);

            return package;
        }

        public bool AddPackage(ILocalizationPackage package)
        {
            var typedPackage = package as LocalizationPackage;
            var canAdd = typedPackage != null && !m_Packages.Contains(typedPackage);
            if (!canAdd) return false;

            m_Packages.Add(typedPackage);
            return true;
        }

        public void RemovePackage(int index)
        {
            m_Packages.RemoveAt(index);
        }

        public bool RemovePackage(ILocalizationPackage package)
        {
            var typedPackage = package as LocalizationPackage;
            return m_Packages.Remove(typedPackage);
        }

        public bool RemoveLocalizationKey(string key)
        {
            var element = Scheme.Elements.FirstOrDefault(e => e.key == key);
            if (element == null) return false;

            if (Scheme.Elements.Remove(element))
            {
                foreach (var package in Packages)
                    package.Elements.RemoveAll(e => e.Key == element.guid);
            }

            return true;
        }
    }
}
