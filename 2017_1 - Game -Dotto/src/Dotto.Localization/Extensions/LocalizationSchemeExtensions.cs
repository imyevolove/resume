﻿namespace Dotto.Localization
{
    public static class LocalizationSchemeExtensions
    {
        public static void RemoveInvalidElements(this LocalizationScheme scheme)
        {
            scheme.Elements.RemoveAll(e => string.IsNullOrEmpty(e.guid) || string.IsNullOrEmpty(e.key));
        }
    }
}
