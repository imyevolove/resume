﻿using System.Linq;

namespace Dotto.Localization
{
    public static class LocalizationPackageExtensions
    {
        public static LocalizationElement GetOrCreate(this ILocalizationPackage package, string key)
        {
            var element = package.Elements.FirstOrDefault(e => e.Key == key);

            if (element == null)
            {
                element = new LocalizationElement(key);
                package.Elements.Add(element);
            }

            return element;
        }

        public static void RemoveInvalidElements(this ILocalizationPackage package)
        {
            package.Elements.RemoveAll(e => string.IsNullOrEmpty(e.Key));
        }

        public static void RemoveInvalidElements(this LocalizationPackage package)
        {
            if (package.Scheme == null) return;
            package.Elements.RemoveAll(e => !package.Scheme.GuidExists(e.Key));
        }
    }
}
