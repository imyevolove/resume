﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Dotto.Localization
{
    [Serializable]
    [RequireComponent(typeof(Text))]
    public class TextLocalizer : MonoBehaviour
    {
        [SerializeField]
        public string localizationKey;

        [SerializeField]
        public StringCase stringCase = StringCase.None;

        ////////////////////////////////////////////////////////////
        public void UpdateText(params string[] formatValues)
        {
            if (string.IsNullOrEmpty(localizationKey))
            {
                Debug.Log("LOCALIZATION KEY IS EMPTY");
                return;
            }
            GetComponent<Text>().text = GetLocalization(localizationKey, formatValues);
        }

        ////////////////////////////////////////////////////////////
        public void UpdateTextCustom(string localizationKey, params string[] formatValues)
        {
            if (string.IsNullOrEmpty(localizationKey))
            {
                Debug.Log("LOCALIZATION KEY IS EMPTY");
                return;
            }
            GetComponent<Text>().text = GetLocalization(localizationKey, formatValues);
        }

        ////////////////////////////////////////////////////////////
        public void SetCustomText(string str)
        {
            GetComponent<Text>().text = str;
        }

        ////////////////////////////////////////////////////////////
        protected string GetLocalization(string localizationKey, params string[] formatValues)
        {
            string localizationString = LocalizationManager.LocalizationProvider.GetValue(localizationKey, formatValues);
            localizationString = StringFormat.Format(localizationString, stringCase);
            return localizationString;
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateText()
        {
            if (string.IsNullOrEmpty(localizationKey))
            {
                Debug.Log("LOCALIZATION KEY IS EMPTY");
                return;
            }
            GetComponent<Text>().text = GetLocalization(localizationKey);
        }

        ////////////////////////////////////////////////////////////
        protected virtual void Start()
        {
            UpdateText();
            LocalizationManager.Event_LocalizationChanged.AddListener(OnLocalizationChanged);
        }

        ////////////////////////////////////////////////////////////
        protected virtual void OnDestroy()
        {
            LocalizationManager.Event_LocalizationChanged.AddListener(OnLocalizationChanged);
        }

        protected virtual void OnLocalizationChanged(ILocalizationPackage package)
        {
            UpdateText();
        }
    }
}
