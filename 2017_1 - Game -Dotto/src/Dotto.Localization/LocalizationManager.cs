﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Dotto.Localization
{
    public class LocalizationManager
    {
        private LocalizationManager()
        {
        }

        private const string LocalizationDatabasePath = "DB/Localization";

        static LocalizationManager()
        {
            LoadLocalizationDatabase();
            SetDefaultLocalization();
        }

        public static ILocalizationDatabase LocalizationDatabase { get; private set; }

        private readonly static LocalizationProvider LocalizationProviderInstance = new LocalizationProvider();
        public static ILocalizationProvider LocalizationProvider { get { return LocalizationProviderInstance; } }

        public readonly static LocalizationChangedEvent Event_LocalizationChanged = new LocalizationChangedEvent();

        public static void SetDefaultLocalization()
        {
            SetLocalization(0);
        }

        public static void SetLocalization(SystemLanguage languageCode)
        {
            if (LocalizationDatabase == null)
            {
                Debug.LogWarning("Can't set localization. Localization database must be defined");
                return;
            }

            var packageIndex = Array.FindIndex(LocalizationDatabase.Packages, (pkg) => { return pkg.LanguageCode == languageCode; });

            if (packageIndex < 0)
            {
                Debug.LogWarningFormat("Language code '{0} not found'", languageCode);
                return;
            }

            SetLocalization(packageIndex);
        }

        public static void SetLocalization(int index)
        {
            if (LocalizationDatabase == null)
            {
                Debug.LogWarning("Can't set localization. Localization database must be defined");
                return;
            }

            if (LocalizationDatabase.Packages.Length <= 0)
            {
                Debug.LogWarning("Can't set localization. Localization database have no localization packages");
                return;
            }

            index = Mathf.Clamp(index, 0, LocalizationDatabase.Packages.Length - 1);
            var package = LocalizationDatabase.Packages[index];
            LocalizationProviderInstance.LocalizationPackage = package;

            Event_LocalizationChanged.Invoke(package);
        }

        private static void LoadLocalizationDatabase()
        {
            var db = Resources.Load(LocalizationDatabasePath, typeof(ILocalizationDatabase)) as ILocalizationDatabase;

            if (db == null)
            {
                Debug.LogErrorFormat("Localization database not found in Resources folder '{0}'", LocalizationDatabasePath);
                return;
            }

            LocalizationDatabase = db;
        }
    }
}
