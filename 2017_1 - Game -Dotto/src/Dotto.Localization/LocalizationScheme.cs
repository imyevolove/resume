﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dotto.Localization
{
    [Serializable]
    public class LocalizationScheme
    {
        [SerializeField] private List<SchemeElement> m_Elements = new List<SchemeElement>();
        public List<SchemeElement> Elements { get { return m_Elements; } }
        
        [Serializable]
        public class SchemeElement
        {
            [SerializeField] public string guid;
            [SerializeField] public string key;

            public SchemeElement(string key)
            {
                this.guid = System.Guid.NewGuid().ToString();
                this.key = key;
            }
        }

        public bool AddElement(SchemeElement element)
        {
            if (m_Elements.Contains(element)) return false;

            m_Elements.Add(element);
            return true;
        }

        public bool KeyExists(string key)
        {
            return Elements.Exists(e => e.key == key);
        }

        public bool GuidExists(string guid)
        {
            return Elements.Exists(e => e.guid == guid);
        }
    }
}
