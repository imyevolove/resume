﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace Dotto.Localization
{
    public class LocalizationChangedEvent : UnityEvent<ILocalizationPackage>
    {
    }
}
