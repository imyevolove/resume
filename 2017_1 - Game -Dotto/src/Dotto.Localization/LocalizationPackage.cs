﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dotto.Localization
{
    [Serializable]
    public class LocalizationPackage : ILocalizationPackage
    {
        [SerializeField] private SystemLanguage m_LanguageCode;
        public SystemLanguage LanguageCode
        {
            get { return m_LanguageCode; }
            set { m_LanguageCode = value; }
        }

        [SerializeField] private List<LocalizationElement> m_Elements = new List<LocalizationElement>();
        public List<LocalizationElement> Elements { get { return m_Elements; } }
        
        public LocalizationScheme Scheme { get; set; }

        public LocalizationPackage(LocalizationScheme scheme) : this(scheme, SystemLanguage.English)
        {
        }

        public LocalizationPackage(LocalizationScheme scheme, SystemLanguage languageCode)
        {
            LanguageCode = languageCode;
            Scheme = scheme;
        }

        public string GetValue(string key)
        {
            if (Scheme != null)
            {
                var schemeElement = Scheme.Elements.FirstOrDefault(v => v.guid == key || v.key == key);
                key = schemeElement == null ? "" : schemeElement.guid;
            }

            var localizationElement = m_Elements.FirstOrDefault(v => v.Key == key);
            return localizationElement == null ? "" : localizationElement.Value;
        }
    }
}
