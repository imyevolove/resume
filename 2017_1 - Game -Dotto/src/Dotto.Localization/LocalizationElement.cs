﻿using System;
using UnityEngine;

namespace Dotto.Localization
{
    [Serializable]
    public class LocalizationElement
    {
        [SerializeField] private string m_Key;
        public string Key
        {
            get { return m_Key; }
            set { m_Key = value; }
        }

        [SerializeField] private string m_Value;
        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        public LocalizationElement()
        {

        }

        public LocalizationElement(string key) : this(key, "")
        {
        }

        public LocalizationElement(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
