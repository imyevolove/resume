﻿using System.Collections.Generic;
using UnityEngine;

namespace Dotto.Localization
{
    public interface ILocalizationPackage
    {
        SystemLanguage LanguageCode { get; set; }
        List<LocalizationElement> Elements { get; }
        
        string GetValue(string key);
    }
}
