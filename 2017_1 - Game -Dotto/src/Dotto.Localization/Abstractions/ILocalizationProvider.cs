﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dotto.Localization
{
    public interface ILocalizationProvider
    {
        string GetValue(string key);
        string GetValue(string key, params object[] additionalValues);
    }
}
