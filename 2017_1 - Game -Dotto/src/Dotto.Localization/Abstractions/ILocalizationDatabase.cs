﻿using UnityEngine;

namespace Dotto.Localization
{
    public interface ILocalizationDatabase
    {
        LocalizationScheme Scheme { get; }
        ILocalizationPackage[] Packages { get; }
        SystemLanguage[] AvailableLanguages { get; }
        
        bool AddPackage(ILocalizationPackage package);
        void RemovePackage(int index);
        bool RemovePackage(ILocalizationPackage package);

        bool RemoveLocalizationKey(string key);
    }
}
