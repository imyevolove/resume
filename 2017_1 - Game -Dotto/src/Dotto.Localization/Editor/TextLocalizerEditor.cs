﻿using System;
using System.Globalization;
using System.Linq;
using Dotto.Localization;
using UnityEditor;
using UnityEngine;
using Dotto.Editor;

namespace Dotto.Localization.Editor
{
    [CustomEditor(typeof(TextLocalizer))]
    [CanEditMultipleObjects]
    public class TextLocalizerEditor : InspectorEditor<TextLocalizer>
    {
        private string[] m_LocalizationKeys;
        private string[] m_LocalizationGuids;

        protected override void OnDrawUI()
        {
            if (m_LocalizationKeys.Length > 0)
            {
                var localizationIndex = Array.IndexOf(m_LocalizationGuids, Target.localizationKey);
                localizationIndex = localizationIndex < 0 ? 0 : localizationIndex;

                localizationIndex = EditorGUILayout.Popup("Localization key", localizationIndex, m_LocalizationKeys);
                Target.localizationKey = m_LocalizationGuids[localizationIndex];

                GUI.enabled = false;
                EditorGUILayout.LabelField("Localization value", LocalizationManager.LocalizationProvider.GetValue(Target.localizationKey));
                GUI.enabled = true;
            }
            else
            {
                EditorGUILayout.HelpBox("Please add at least one localization key", MessageType.Error);
            }

            Target.stringCase = (StringCase)EditorGUILayout.EnumPopup("String case", Target.stringCase);
        }
        
        protected override void OnInitialize()
        {
            UpdateLocalizationKeys();
        }

        private void UpdateLocalizationKeys()
        {
            m_LocalizationKeys = LocalizationManager.LocalizationDatabase.Scheme.Elements.Select(e => e.key).ToArray();
            m_LocalizationGuids = LocalizationManager.LocalizationDatabase.Scheme.Elements.Select(e => e.guid).ToArray();
        }
    }
}
