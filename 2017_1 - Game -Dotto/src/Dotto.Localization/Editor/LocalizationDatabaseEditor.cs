﻿using System;
using System.Globalization;
using System.Linq;
using Dotto.Localization;
using UnityEditor;
using UnityEngine;
using Dotto.Editor;

namespace Dotto.Localization.Editor
{
    [CustomEditor(typeof(LocalizationDatabase))]
    public class LocalizationDatabaseEditor : InspectorEditor<LocalizationDatabase>
    {
        private int m_SelectedPackageIndex = 0;
        private int m_PreviousPackageIndex = -1;
        private string[] m_Languages;
        private string m_LocalizationCreatorKey;
        private Vector2 m_PackageEditorScrollView = new Vector2();
        
        protected LocalizationPackage SelectedPackage { get; private set; }

        protected override void OnDrawUI()
        {
            DrawToolbar();
            SelectedPackage = Target.Packages.ElementAtOrDefault(m_SelectedPackageIndex) as LocalizationPackage;
            InvokePackageChangedEventIfNeeded();
            DrawSelectedPackageEditor();
        }

        /// <summary>
        /// Вызывается один раз при инициализации эдитора
        /// </summary>
        protected override void OnInitialize()
        {
            BuildLanguages();
            InitializeScheme();
        }

        /// <summary>
        /// Вызывается после выбора нового пакета локализации
        /// </summary>
        /// <param name="package"></param>
        protected virtual void OnSelectedPackageChanged(LocalizationPackage package)
        {
            if (package == null) return;
            NormalizePackage(package);
        }

        protected virtual void DrawToolbar()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Localization database manager", EditorStyles.boldLabel);
            if (GUILayout.Button("Create package"))
                Target.CreateNewPackage(default(SystemLanguage));
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Available packages", EditorStyles.boldLabel);
            m_SelectedPackageIndex = GUILayout.Toolbar(m_SelectedPackageIndex, Target.AvailableLanguages.Select(l => l.ToString()).ToArray());
            EditorGUILayout.EndVertical();
        }

        protected virtual void DrawPackageEditor()
        {
            var package = SelectedPackage;

            DrawSchemeManager();

            #region Package manager
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            #region Header
            EditorGUILayout.BeginHorizontal();
            
            EditorGUILayout.LabelField(string.Format("Selected package: {0}", package.LanguageCode.ToString()));
            
            if (GUILayout.Button("Remove package"))
            {
                Target.RemovePackage(package);
                return;
            }

            EditorGUILayout.EndHorizontal();
            #endregion
            
            package.LanguageCode = (SystemLanguage)EditorGUILayout.EnumPopup("Language", package.LanguageCode);

            EditorGUILayout.LabelField(string.Format("Items '{0}'", package.Elements.Count), EditorStyles.boldLabel);
            
            m_PackageEditorScrollView = EditorGUILayout.BeginScrollView(m_PackageEditorScrollView, false, false);

            foreach (var schemeElement in Target.Scheme.Elements)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                var element = package.GetOrCreate(schemeElement.guid);

                schemeElement.key = EditorGUILayout.TextField(schemeElement.key);
                element.Value = EditorGUILayout.TextField(element.Value);

                if (GUILayout.Button("Remove"))
                {
                    Target.RemoveLocalizationKey(schemeElement.key);
                    return;
                }
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            #endregion
        }

        private void DrawSchemeManager()
        {
            #region Scheme keys manager
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            /////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////// HEADER
            EditorGUILayout.LabelField("Localization keys manager", EditorStyles.boldLabel);

            /////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////// BODY
            EditorGUILayout.BeginHorizontal();

            m_LocalizationCreatorKey = EditorGUILayout.TextField(m_LocalizationCreatorKey);

            if (GUILayout.Button("Create"))
            {
                if (string.IsNullOrEmpty(m_LocalizationCreatorKey))
                {
                    EditorUtility.DisplayDialog("Error", "Localization key must be defined", "Continue");
                    return;
                }

                if (Target.Scheme.KeyExists(m_LocalizationCreatorKey))
                {
                    if (EditorUtility.DisplayDialog("Error", string.Format("Localization key '{0}' already defined", m_LocalizationCreatorKey), "Continue"))
                    {
                        m_LocalizationCreatorKey = string.Empty;
                    }
                    return;
                }

                Target.Scheme.AddElement(new LocalizationScheme.SchemeElement(m_LocalizationCreatorKey));
                m_LocalizationCreatorKey = string.Empty;
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
            #endregion

        }

        private void DrawSelectedPackageEditor()
        {
            if (SelectedPackage == null)
            {
                EditorGUILayout.HelpBox("Package not selected", MessageType.Info);
                return;
            }

            DrawPackageEditor();
        }

        private void InvokePackageChangedEventIfNeeded()
        {
            if (m_PreviousPackageIndex == m_SelectedPackageIndex) return;
            m_PreviousPackageIndex = m_SelectedPackageIndex;

            OnSelectedPackageChanged(SelectedPackage);
        }

        private void InitializeScheme()
        {
            Target.Scheme.RemoveInvalidElements();
        }

        private void NormalizePackage(LocalizationPackage package)
        {
            package.RemoveInvalidElements();
        }

        private void BuildLanguages()
        {
            m_Languages = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.SpecificCultures).Select(c => c.TwoLetterISOLanguageName).ToArray();
            Array.Sort(m_Languages, String.Compare);
        }
    }
}
