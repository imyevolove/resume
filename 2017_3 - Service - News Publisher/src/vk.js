require("hero-include");

include
    .using("./")
    .using("./lib");
include("Extensions/InheritExtension");

const VK = require('vk-io');
var Config = include("Config");
var vkApi = include("VK/Api");

global.ROOT_DIRNAME = include.convertDirectoryPath("./");
var VKConfig = new Config("cfg/vk.json");

var vk = new VK({
    app: VKConfig.data.app,
    key: "85nmqeHLs1s4ZXxf67fP",
    login: VKConfig.data.login,
    pass: VKConfig.data.password
})
const auth = vk.standaloneAuth();
auth.run()
    .then((token) => {
        vkApi.post('pages.save', { title: "title test 2", group_id: VKConfig.data.group, text: "title test", access_token: token })
            .then(function (id) {
                console.log({ id: id, wid: "page-" + VKConfig.data.group + "_" + id });
            })
            .catch(function (err) {
                console.error('Unable to complete API request', err);
            })
        console.log('Token:', token);
    })
    .catch((error) => {
        console.error(error);
    });