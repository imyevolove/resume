﻿require("hero-include");

include
    .using("./")
    .using("./lib");
include("Extensions/InheritExtension");


global.ROOT_DIRNAME = include.convertDirectoryPath("./");
var Config = include("Config");
var botConfig = new Config("cfg/bot.json");
var VKConfig = new Config("cfg/vk.json");

var QueueExecutor = include("QueueExecutor");
var StopgameWebHandler = include("WebHandlers/StopgameWebHandler");
var VKSocialHandler = include("Social/VKSocialHandler");
var Bot = include("Bots/Bot");
var schedule = include('node-schedule');

var opt = {
    app: VKConfig.data.app,
    key: VKConfig.data.key,
    login: VKConfig.data.login,
    password: VKConfig.data.password,
    group: VKConfig.data.group,
    videoAlbum: VKConfig.data.videoAlbum,
    photoAlbum: VKConfig.data.photoAlbum
};

var vkSocialHandler = new VKSocialHandler(opt);
var stopgameWebHandler = new StopgameWebHandler();

var bot = new Bot("forbiten_bot");
bot.addWebHandler("stopgame", stopgameWebHandler);
bot.addSocialHandler(vkSocialHandler);

bot.startIntervalGrabbind(botConfig.data.extract_period, true);
bot.startIntervalPosting(botConfig.data.post_period);