﻿var PublishPipeline = include("Publishers/PublishPipeline");
var AbstractWebHandler = include("WebHandlers/AbstractWebHandler");
var AbstractSocialHandler = include("Social/AbstractSocialHandler");
var QueueExecutor = include("QueueExecutor");
var schedule = include('node-schedule');

module.exports = function Bot(botname)
{
    var that = this;

    var m_WebHandlers = {};
    var m_SocialHandlers = [];
    var m_Pipeline = new PublishPipeline(botname + "_pipeline");

    this.startIntervalGrabbind = function (period, execNow, callback) {
        if (execNow) that.extractArticles(callback);

        var rule = new schedule.RecurrenceRule();
        if (period.hour) rule.hour = period.hour;
        if (period.minute) rule.minute = period.minute;

        var job = schedule.scheduleJob(rule, function () {
            console.log("Extracting cron");
            that.extractArticles();
        });
    }

    this.startIntervalPosting = function (period, execNow, callback) {
        if (execNow) that.postNextArticleToSocials(callback);

        var rule = new schedule.RecurrenceRule();
        if (period.hour) rule.hour = period.hour;
        rule.minute = period.minute || 0;

        var job = schedule.scheduleJob(rule, function () {
            console.log("Posting cron");
            that.postNextArticleToSocials();
        });
    }

    this.addSocialHandler = function (handler) {

        if (!AbstractSocialHandler.prototype.isPrototypeOf(handler))
            throw new Error("Handler must be a AbstractSocialHandler");

        if (m_SocialHandlers.indexOf(handler) > 0)
            throw new Error("Handler already exists");

        m_SocialHandlers.push(handler);
    };

    this.addWebHandler = function (id, handler) {

        if (typeof id != "string")
            throw new Error("id must be a string");

        if (!AbstractWebHandler.prototype.isPrototypeOf(handler))
            throw new Error("Handler must be a AbstractWebHandler");

        if (!!m_WebHandlers[id])
            throw new Error("Handler with id '" + id + "' already exists");

        id = id.toLowerCase();
        m_WebHandlers[id] = handler;
    };

    this.extractArticles = function (callback) {
        callback = normalizeCallback(callback);
        console.log("extracting");

        var results = {};
        var qExecutor = new QueueExecutor();

        for (var key in m_WebHandlers)
            addHandlerActionToQExecutor(key, m_WebHandlers[key]);

        qExecutor.addAction(function (callback) {
            for (var key in results)
                m_Pipeline.addArray(results[key].map(function (article) {
                    return article.url;
                }), key, true);
            callback();
        });

        qExecutor.addAction(function (callback) {

            for (var key in results) {
                if (!m_WebHandlers[key] || !results[key].length) continue;
                m_WebHandlers[key].setLastArticleUrl(results[key][0].url);
            }
            callback();
        });

        qExecutor.start(function (error) {

            console.log("Extract ended" + (error ? " with error " + error : "" ))

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: results });

        });

        function addHandlerActionToQExecutor(id, handler) {
            qExecutor.addAction(function (callback) {
                console.log("start extract");
                handler.extractArticles(function (articles) {
                    results[id] = articles;
                    callback();
                });
            });
        }
    };
    
    this.postNextArticleToSocials = function (callback) {
        
        callback = normalizeCallback(callback);
        
        var data = m_Pipeline.popSafe();
        if (!data) {
            console.log("Pipeline is empty");
            return callback(false, "Pipeline is empty");
        }

        var articleDocument = null;
        var qExecutor = new QueueExecutor();
        
        /// Download and parse article
        qExecutor.addAction(function (callback) {
            try {
                var handler = m_WebHandlers[data.handler];
                parseArticle(handler, data.url, function (document) {
                    articleDocument = document;
                    callback(!document, document);
                })
            } catch (err) { callback(err); }
        });

        for (var i in m_SocialHandlers)
            addHandlerActionToQExecutor(m_SocialHandlers[i]);
        
        qExecutor.start(function (error) {
            if (error) console.log(error);
            else {
                console.log("pop");
                m_Pipeline.pop();
            }

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback();

        });

        function addHandlerActionToQExecutor(handler) {
            qExecutor.addAction(function (callback) {
                authorizeHandler(handler, function () {
                    postToSocial(handler, articleDocument, function (status) {
                        callback(!status, status);
                    });
                });
            });
        }
    };

    function parseArticle(parser, url, callback) {
        parser.parseArticle(url, function (document) {
            if (!document) setTimeout(parseArticle.bind(null, parser, url, callback), 1000);
            else callback(document);
        });
    }

    function postToSocial(handler, articleDocument, callback) {
        handler.postFullArticle(articleDocument, function (status) {
            console.log("Post status: " + status);
            if (!status) setTimeout(postToSocial.bind(null, handler, articleDocument, callback), 1000);
            else callback(status);
        });
    }

    function authorizeHandler(handler, callback) {
        try {
            handler.authorize(function (status) {
                if (!status) {
                    console.log("VK authorization failure");
                    setTimeout(authorizeHandler.bind(null, handler, callback), 1000);
                    return;
                }
                callback();
            });
        } catch(err) {
            console.log(err);
            setTimeout(authorizeHandler.bind(null, handler, callback), 1000);
        }
    }

    function normalizeCallback(callback) {
        return Function.prototype.isPrototypeOf(callback) ? callback : function () { };
    }
}