﻿module.exports = function AbstractWebHandler()
{
    this.CACHE_FOLDER_NAME = "_web_handlers_cache";

    this.setLastArticleUrl = function (url) { };
    this.extractArticles = function (url, callback) { };
    this.parseArticle = function (url, callback) { };
}