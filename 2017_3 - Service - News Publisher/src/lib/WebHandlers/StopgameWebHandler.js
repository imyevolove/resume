﻿var AbstractWebHandler = include("./AbstractWebHandler");
var StopgameArticlesGrabberCreator = include("Grabbers/StopgameArticlesGrabberCreator");
var Config = include("Config");
var Cache = include("Cache");
var path = include("path");
var normalizeUrl = require('normalize-url');

module.exports = function StopgameWebHandler()
{
    var that = this;

    this.inherit(AbstractWebHandler, arguments);

    var m_Config = new Config("cfg/stopgame.json");
    var m_Cache = new Cache(path.normalize(ROOT_DIRNAME + "/_collector/stopgame.json"));
    
    var m_ArticlesGrabber = StopgameArticlesGrabberCreator.create();

    this.config = m_Config.data;

    this.setLastArticleUrl = function (url) {
        m_Cache.data.last_article_url = url;
        m_Cache.save();
    };

    /// Extract all unpublished articles
    this.extractArticles = function (callback) {
        callback = normalizeCallback(callback);

        var date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setDate(date.getDate() - (that.config.grabber.lifetime || 5));
        extractAllArticles(
            100,
            date.getTime(),
            m_Cache.data.last_article_url,
            that.config.grabber.news_url,
            that.config.grabber.news_page_url_part,
            callback
        );
    }

    this.parseArticle = function (url, callback) {
        callback = normalizeCallback(callback);
        m_ArticlesGrabber.parse(url, callback);
    }

    function normalizeCallback(callback) {
        return Function.prototype.isPrototypeOf(callback) ? callback : function () { };
    }

    function extractAllArticles(callSizeLimit, lifetime, limitByUrl, url, pageUrlPart, callback)
    {
        var index = -1;
        var i;
        var collection = [];
        next();
        
        function next()
        {
            index++;
            if (index >= callSizeLimit) { return end(); }
            
            var r_url = normalizeUrl(url + "/" + pageUrlPart.replace(/\$/ig, index + 1));
            console.log("Requesting page [" + r_url + "]");
            
            m_ArticlesGrabber.extract(r_url, function (result) {
                if (!result) return next();
                
                for (i = 0; i < result.length; i++) {
                    
                    /// Filter by relevate
                    if (lifetime >= result[i].time) return end();
                    
                    /// Filter by url
                    if (result[i].url == limitByUrl) return end();

                    collection.push(result[i]);
                }

                next();
            });
        }

        function end()
        {
            callback(collection);
        }
    }
}