﻿const request = require('request-promise');
const BufferList = require('bl');
const FormData = require('form-data');
const Network = include('Net/Network');
var requestBase = require("request");
const VK = require('vk-io');
var path = include('path');
var fs = include('fs');
var crypto = include('crypto');
var vkApi = include("VK/Api");
var AbstractSocialHandler = include("./AbstractSocialHandler");
var ArticleDocument = include("Documents/ArticleDocument");
var DocumentUtils = include("Documents/DocumentUtils");
var TextUtils = include("Documents/TextUtils");
var BaseAttachment = include("Documents/Attachments/BaseAttachment");
var QueueExecutor = include("QueueExecutor");

module.exports = function VKSocialHandler(options)
{
    var that = this;

    var m_Token;

    var m_VK = new VK({
        app: options.app,
        key: options.key,
        login: options.login,
        pass: options.password
    });
    var m_Auth = m_VK.standaloneAuth();

    this.inherit(AbstractSocialHandler, arguments);
    
    this.isAuthorized = function () {
        return !!m_Token;
    };

    this.authorize = function (callback) {
        callback = Function.prototype.isPrototypeOf(callback) ? callback : function () {};

        console.log("Try vk auth");
        
        m_Auth.run()
            .then((token) => {
                m_Token = token;
                console.log('Token:', token);
                callback(true);
            })
            .catch((error) => {
                console.error(error);
                callback(false);
            });
    };

    var wrapToH1 = wrapToSingle.bind(null, "==");
    var wrapToH2 = wrapToSingle.bind(null, "===");
    var wrapToH3 = wrapToSingle.bind(null, "====");
    var wrapToItalic = wrapToSingle.bind(null, "''");
    var wrapToBold = wrapToSingle.bind(null, "'''");
    var wrapToQuote = wrapTo.bind(null, "<blockquote>", "</blockquote>");
    var wrapToAlignLeft = wrapTo.bind(null, "<left>", "</left>");
    var wrapToAlignCenter = wrapTo.bind(null, "<center>", "</center>");
    var wrapToAlignRight = wrapTo.bind(null, "<right>", "</right>");
    var wrapToMarqList = wrapTo.bind(null, "*", "");
    var wrapToLink = wrapTo.bind(null, "[[", "]]");
    var wrapToExternalLink = wrapTo.bind(null, "[", "]");
    
    this.uploadVideo = function (url, callback) {
        vkApi('video.save', { link: url, group_id: options.group, album_id: options.videoAlbum, access_token: m_Token })
            .then(function (info) {
                if (!info && !info.upload_url) return callback(false);
                request.get(info.upload_url, function (err, res) {
                    try {
                        if (err) throw new Error(err);

                        var response = JSON.parse(res.body);
                        if (response.hasOwnProperty("error_code"))
                            throw new Error(response["error_message"]);

                        callback("video" + info.owner_id + "_" + info.vid);
                    } catch (err) {
                        callback(false, err ? (err["message"] || "unknown") : "unknown");
                    }
                });
            })
            .catch(function (err) {
                console.error('Unable to complete API request', err);
                callback(false);
            })
    }

    this.uploadImage = function (url, callback) {
        function endError() { callback(false); }

        var qe = new QueueExecutor();

        /// Загрузка изображения
        qe.addAction(function (callback) {
            var filename = crypto.createHash('md5').update(url).digest('hex') + path.extname(url);
            var dest = path.normalize(ROOT_DIRNAME + "/_cahce_images/" + filename);
            Network.downloadFile(url, dest, function (result) {
                if (!result.status) callback("Image download failure");
                else callback(null, result.destination);
            });
        });

        /// Получение сервера
        qe.addAction(function (callback, image) {
            vkApi('photos.getUploadServer', { group_id: options.group, album_id: options.photoAlbum, access_token: m_Token })
                .then(function (info) {
                    callback(null, image, info);
                })
                .catch(function (err) {
                    console.error('Unable to complete API request', err);
                    callback(err);
                })
        });

        /// Загрузка на сервер
        qe.addAction(function (callback, image, info) {
            var r = requestBase.post(info.upload_url, function (err, res, body) {
                removeLocalFile(image);
                if (err) callback(err);
                else callback(null, body);
            });
            form = r.form();
            form.append("file1", fs.createReadStream(image));
        });

        /// Обработка результата
        qe.addAction(function (callback, result) {
            try {
                var response = JSON.parse(result);
                callback(null, response);
            } catch (err) { callback("Unknown error"); }
        });

        /// Сохранение фотографии
        qe.addAction(function (callback, result) {
            result.access_token = m_Token;
            vkApi('photos.save', result)
                .then(function (info) {
                    callback(null, info);
                })
                .catch(function (err) {
                    console.error('Unable to complete API request', err);
                    callback(err);
                })
        });

        /// Обработка результата
        qe.addAction(function (callback, result) {
            try {
                callback(null, "photo" + result[0].owner_id + "_" + result[0].pid);
            } catch (err) { callback(err); }
        });

        qe.start(function (err, image_url) {
            if (err) console.log(err);
            callback((err || !image_url) ? false : image_url);
        });
    }

    this.prepareArticleDocumentAndUploadAttachments = function (articleDocument, callback) {

        if (!ArticleDocument.prototype.isPrototypeOf(articleDocument))
            throw new Error("articleDocument document must be a ArticleDocument");

        var qe = new QueueExecutor();

        var attachments = articleDocument.getAttachments();
        attachments = attachments.filter(function (attachment) {
            if (DocumentUtils.isImageAttachment(attachment)) return true;
            if (DocumentUtils.isVideoAttachment(attachment)) return true;
            return false;
        });

        attachments.forEach(function (attachment) {
            qe.addAction(function (callback) {
                if (DocumentUtils.isImageAttachment(attachment)) {
                    recursoveRepeateUploadImage(attachment.getValue(), function (res) {
                        if (res) attachment.setValue(res);
                        else articleDocument.removeAttachment(attachment);
                        callback();
                    });
                } else if (DocumentUtils.isVideoAttachment(attachment)) {
                    recursoveRepeateUploadVideo(attachment.getValue(), function (res) {
                        if (res) attachment.setValue(res);
                        else articleDocument.removeAttachment(attachment);
                        callback();
                    });
                } else callback();
            });
        });

        qe.start(function () {
            callback(articleDocument);
        }); 
    };

    function recursoveRepeateUploadImage(url, callback) {
        console.log("uploading image: " + url);
        that.uploadImage(url, function (res) {
            if (res) callback(res);
            else setTimeout(recursoveRepeateUploadImage.bind(null, url, callback), 1000);
        });
    }

    function recursoveRepeateUploadVideo(url, callback) {
        console.log("uploading video: " + url);
        that.uploadVideo(url, function (res, code) {
            if (res) callback(res);
            else if (!code) setTimeout(recursoveRepeateUploadVideo.bind(null, url, callback), 1000);
            else callback(null);
        });
    }

    this.writeWikiArticle = function (title, text, callback) 
    {
        text = TextUtils.formatText(text);
        vkApi.post('pages.save', { title: title, group_id: options.group, text: text, access_token: m_Token })
            .then(function (id) {
                callback({ id: id, wid: "page-" + options.group + "_" + id });
            })
            .catch(function (err) {
                console.error('Unable to complete API request', err);
                callback(false);
            })
    }

    this.postOnWall = function (message, attachments, callback)
    {
        message = TextUtils.formatText(message);
        attachments = attachments.join(",");
        vkApi.post("wall.post", { owner_id: -(options.group), from_group: 1, message: message, attachments: attachments, access_token: m_Token })
            .then(function (id) {
                callback(true);
            })
            .catch(function (err) {
                console.log(err);
                callback(false);
            })
    }

    this.postFullArticle = function (article, callback) {
        var qe = new QueueExecutor();

        /// Подготовка документа
        qe.addAction(function (callback) {
            console.log("Document preparing");
            that.prepareArticleDocumentAndUploadAttachments(article, function (article) {
                callback(null, article);
            });
        });

        /// Преобразование в текст
        qe.addAction(function (callback, article) {
            console.log("Document converting");
            var title = article.getTitle();
            var text = that.convertArticleDocumentToText(article);
            var clearText = article.getShortClearText(200, 800);
            
            callback(null, { article: article, title: title, text: text, clearText: clearText});
        });

        /// Создание вики страницы
        qe.addAction(function (callback, content) {
            console.log("Wiki creating");
            that.writeWikiArticle(content.title, content.text, function (result) {
                content.wiki = result.wid;
                callback(result ? null : "Wiki error", content)
            });
        });

        /// Формирование параметров для поста
        qe.addAction(function (callback, content) {
            /// Хештеги
            content.text = "#FORBITEN_NEWS" + enterText();

            /// Короткий текст статьи
            content.text += content.title + enter2Text() + content.clearText;

            /// Добавление ссылки на статью для ленивых)
            content.text += enter2Text() + "Читайте всю wiki-статью по ссылке:"
                + enterText() + "https://vk.com/club" + options.group + "?w=" + content.wiki;
            content.attachments = [content.wiki];

            var iteractiveAttachments = DocumentUtils.getFirstImageOrVideoAttachment(content.article.getAttachments(), true);
            if (iteractiveAttachments) content.attachments.push(iteractiveAttachments.getValue());
            
            callback(null, content);
        });

        /// Создание поста
        qe.addAction(function (callback, content) {
            console.log("Posting");
            that.postOnWall(content.text, content.attachments, function (result) {
                callback(result ? null : "Post error", content)
            });
        });

        qe.start(function (err) {
            if (err) console.log(err);
            callback(!err);
        });
    }

    this.convertArticleDocumentToText = function (articleDocument) {

        if (!ArticleDocument.prototype.isPrototypeOf(articleDocument))
            throw new Error("articleDocument document must be a ArticleDocument");

        var text = "";

        //////////////////////////
        ///// EDIT TEXT HERE /////
        //////////////////////////

        appendToText(wrapToH1(articleDocument.getTitle()));
        
        appendToText(enterText());
        appendToText(enterText());

        var attachments = articleDocument.getAttachments();
        for (var i = 0; i < attachments.length; i++)
            appendToText(convertAttachmentToText(attachments[i]));
        
        /////////////////////////
        //////////////////////////
        //////////////////////////

        if (articleDocument.getSourceUrl()) {
            appendToText(enterText());
            appendToText(createExternalLinkText("Источник", articleDocument.getSourceUrl()));
        }

        text = TextUtils.removeDuplicateSpaces(text);

        return text;

        function appendToText(str) {
            text += str;
        }
    }

    function removeLocalFile(path) {
        try { fs.unlinkSync(path); return true; }
        catch (err) { return false; }
    }

    function convertAttachmentToText(attachment)
    {
        if (!BaseAttachment.prototype.isPrototypeOf(attachment))
            return "";

        if (DocumentUtils.isImageAttachment(attachment))
            return wrapToLink(attachment.getValue() + "|" + (attachment.width || 5000) + "px| ");

        if (DocumentUtils.isVideoAttachment(attachment))
            return wrapToLink(attachment.getValue() + "|5000x300px;player| ");

        if (DocumentUtils.isLinkAttachment(attachment))
            return createExternalLinkText(attachment.text, attachment.link);

        if (DocumentUtils.isTextAttachment(attachment))
        {
            if (attachment.fontWeight == "bold")
                return wrapToBold(attachment.getValue());
            return attachment.getValue();
        }

        return attachment.getValue();
    }

    function enter2Text() {
        return "\n\n";
    }

    function enterText()
    {
        return "\n";
    }

    function createExternalLinkText(name, url)
    {
        return wrapToExternalLink(url + "|" + name);
    }

    function wrapToSingle(wrapStr, text) {
        return TextUtils.wrapTextToSpaces(wrapStr + text + wrapStr);
    }

    function wrapTo(start, end, text) {
        return TextUtils.wrapTextToSpaces(start + text + end);
    }
}