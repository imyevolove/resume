﻿var fs = include("fs")

module.exports = function Config(path) {

    var that = this;

    this.data = {};

    this.load = function (path) {
        try {
            var content = include(path);
            return that.data = content;
        } catch (err) { console.log(err); return null; }
    }

    constructor();
    function constructor()
    {
        if (path) that.load(path);
    }
};