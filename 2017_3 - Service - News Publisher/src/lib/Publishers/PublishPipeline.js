﻿var AbstractWebHandler = include("WebHandlers/AbstractWebHandler");
var Cache = include("Cache");
var path = include("path");

module.exports = function PublishPipeline(cachefilename)
{
    var that = this;
    var m_Cache = new Cache(path.normalize(ROOT_DIRNAME + "_pipelines/" + cachefilename + ".json"));
    m_Cache.data.list = m_Cache.data.list || [];
    
    this.cacheSize = 1000;

    this.popSafe = function ()
    {
        try {
            var item = m_Cache.data.list[m_Cache.data.list.length - 1];
            return item;
        } catch (err) { return undefined; }
    }

    this.pop = function () {
        var item = m_Cache.data.list.pop();
        m_Cache.save();
        return item;
    };

    this.add = function (url, handlerID) {

        if (typeof url != "string")
            throw new Error("Url must be a string");

        if (typeof handlerID != "string")
            throw new Error("HandlerID must be a string");

        if (m_Cache.data.list.length >= cacheSize)
            m_Cache.data.list.splice(m_Cache.data.list.length - 1, 1);

        m_Cache.data.list.unshift({ url: url, handler: handlerID });
        m_Cache.save();
    }

    this.addArray = function (urls, handlerID, reverse) {
        
        if (!Array.isArray(urls))
            throw new Error("Urls must be a Array");

        if (typeof handlerID != "string")
            throw new Error("HandlerID must be a string");

        var delete_delta = that.cacheSize - m_Cache.data.list.length + urls.length;
        if (delete_delta < 0)
            m_Cache.data.list.splice(m_Cache.data.list.length - delete_delta, delete_delta);

        if (reverse === true)
            urls = urls.reverse();

        for (var i = 0; i < urls.length; i++) {
            m_Cache.data.list.unshift({ url: urls[i], handler: handlerID });
        }
        
        m_Cache.save();
    }
}