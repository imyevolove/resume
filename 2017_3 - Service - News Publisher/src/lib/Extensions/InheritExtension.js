﻿Object.defineProperty(Object.prototype, "inherit", {
    writable: false,
    enumerable: false,
    configurable: false,
    value: function (target, args) {
        target.apply(this, args);

        if (!(target.prototype == this.constructor.prototype)) {
            this.__proto__ = this.constructor.prototype = Object.create(target.prototype);
            this.constructor.prototype.constructor = target;
        }
    }
});