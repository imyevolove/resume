﻿function TextUtils()
{
    var that = this;

    this.removeDuplicateSpaces = function (text) {
        return text.replace(/ +(?= )/g, '');
    };

    this.wrapTextToSpaces = function (text) {
        return " " + text + " ";
    };

    this.normalizePunctuation = function (text) {
        return text.replace(/(\s\,)/ig, ",").replace(/,(?=[^\s])/g, ", ").replace(/\s+\./g, '.');
    };

    /**
     * Normalize text. Remove duplicate spaces and normalize punctuation
     * @param {string} text
     * @return {string}
     */
    this.formatText = function (text) {
        text = that.removeDuplicateSpaces(text);
        text = that.normalizePunctuation(text);

        return text;
    };
}

module.exports = new TextUtils();