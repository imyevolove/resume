﻿var BaseAttachment = include("Attachments/BaseAttachment");
var AttachmentSystem = include("./Attachments/AttachmentSystem");
var DocumentUtils = include("./DocumentUtils");

module.exports = function ArticleDocument()
{
    var that = this;
    this.inherit(AttachmentSystem);

    var m_Title = "";
    var m_SourceUrl = "";

    this.setTitle = function (title) {

        if (typeof title != "string")
            throw new Error("Title must be a string");

        m_Title = title;
    };

    this.getTitle = function () {
        return m_Title;
    };

    this.getClearText = function () {
        return DocumentUtils.covertAttachmentsToClearText(that.getAttachments());
    };

    this.getShortClearText = function (min, max) {

        min = Math.abs(min);
        max = Math.abs(max);

        if (min > max) min = max;

        var clearText = that.getClearText();
        var textParts = clearText.replace(/\n\s\n/ig, "\n\n").split("\n\n");
        
        var shortText = textParts[0];
        if (shortText.length < min) {
            if (!!textParts[1]) shortText += "\n\n" + (textParts[1] || "");
            if (shortText.length < min) shortText = clearText.substr(0, min);
        }
        if (shortText.length > max) shortText = clearText.substr(0, max);

        return shortText;
    }

    this.setSourceUrl = function (url) {

        if (typeof url != "string")
            throw new Error("Url must be a string");

        m_SourceUrl = url;
    };

    this.getSourceUrl = function () {
        return m_SourceUrl;
    };

}