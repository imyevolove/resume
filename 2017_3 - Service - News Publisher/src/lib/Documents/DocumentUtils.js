﻿var jsdom = include("jsdom");
var window = jsdom.jsdom().defaultView;

var TextAttachment = include("./Attachments/TextAttachment");
var ImageAttachment = include("./Attachments/ImageAttachment");
var VideoAttachment = include("./Attachments/VideoAttachment");
var LinkAttachment = include("./Attachments/LinkAttachment");
var TextUtils = include("./TextUtils");

function DocumentUtils()
{
    var that = this;

    this.isImageAttachment = function (att) {
        return ImageAttachment.prototype.isPrototypeOf(att);
    }

    this.isVideoAttachment = function (att) {
        return VideoAttachment.prototype.isPrototypeOf(att);
    }

    this.isTextAttachment = function (att) {
        return TextAttachment.prototype.isPrototypeOf(att);
    }

    this.isLinkAttachment = function (att) {
        return LinkAttachment.prototype.isPrototypeOf(att);
    }

    this.covertAttachmentsToClearText = function (attachments)
    {
        var text = "";

        for (var i = 0; i < attachments.length; i++)
            text += TextUtils.wrapTextToSpaces(convertAttachmentToCelarText(attachments[i]));

        text = TextUtils.formatText(text);
        return text;
    }

    this.getFirstImageOrVideoAttachment = function (attachments, videoImportant) {
        var result = undefined;
        for (var i in attachments) {
            if (that.isImageAttachment(attachments[i]) || that.isVideoAttachment(attachments[i])) {
                if (!result) result = attachments[i];
                if (that.isImageAttachment(attachments[i]) && videoImportant === true) continue;
                else {
                    result = attachments[i];
                    break;
                }
            }
        }
        return result;
    };

    this.objectToAttachment = function (obj) {

        try {
            if (window.Text.prototype.isPrototypeOf(obj)) {
                return new TextAttachment(obj.nodeValue);
            }

            try {
                if (obj.nodeName.toLowerCase() == "b") {
                    var text = new TextAttachment(obj.textContent);
                    text.fontWeight = "bold";
                    return text;
                }
            } catch (err) { }

            try {
                if (obj.nodeName.toLowerCase() == "a") {
                    if (obj.textContent.length > 0) {
                        var text = new LinkAttachment(obj.textContent);
                        text.text = obj.textContent || "link";
                        text.link = obj.href;
                    }
                    return text;
                }
            } catch (err) { }

            if (window.HTMLImageElement.prototype.isPrototypeOf(obj)) {
                var image = new ImageAttachment(obj.src);
                image.width = obj.width || 0;
                image.height = obj.height || 0;
                return image;
            }
        } catch (err) { return null; }

        return null;
    }

    function convertAttachmentToCelarText(attachment) {

        if (that.isTextAttachment(attachment) || that.isLinkAttachment(attachment))
            return attachment.getValue();

        return "";
    }
}

module.exports = new DocumentUtils();