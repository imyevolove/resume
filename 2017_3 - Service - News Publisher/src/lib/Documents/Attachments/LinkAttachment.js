﻿var BaseAttachment = include("./BaseAttachment");

module.exports = function LinkAttachment() {
    this.inherit(BaseAttachment, arguments);

    this.text = "";
    this.link = "";
}