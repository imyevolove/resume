﻿var BaseAttachment = include("./BaseAttachment");

module.exports = function ImageAttachment() {
    this.inherit(BaseAttachment, arguments);

    this.width = 0;
    this.height = 0;
}