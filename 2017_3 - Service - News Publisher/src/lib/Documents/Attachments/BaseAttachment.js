﻿module.exports = function BaseAttachment(value)
{
    var that = this;

    var m_Value = "";
    
    this.setValue = function (value) {

        if (typeof value != "string")
            throw new Error("Value must be a string");

        if (value[0] == " ")
            value = value.substring(1);

        if (value[value.length - 1] == " " && value.length - 1 >= 0)
            value = value.substring(0, value.length - 1);

        m_Value = value;
    };

    this.getValue = function () {
        return m_Value || "";
    };
    /*
    this.getValueWithAttachments = function () {
        return m_Value + attachmentsToText();
    };

    function attachmentsToText()
    {
        var text = "";
        var attachments = that.getAttachments();

        for (var i in attachments)
            text += attachments[i].getValue();

        return text;
    }
    */

    constructor();
    function constructor()
    {
        if (value) that.setValue(value);
    }
}