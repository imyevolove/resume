﻿var BaseAttachment = include("./BaseAttachment");

module.exports = function TextAttachment() {
    this.inherit(BaseAttachment, arguments);

    this.fontStyle = "normal";
    this.fontWeight = "normal";
}