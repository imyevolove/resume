﻿var BaseAttachment = include("./BaseAttachment");

module.exports = function AttachmentSystem()
{
    var that = this;

    var m_Attachments = [];

    this.removeAttachment = function (attachment) {
        m_Attachments.splice(m_Attachments.indexOf(attachment), 1);
    };

    this.addAttachment = function (attachment) {
        
        if (!BaseAttachment.prototype.isPrototypeOf(attachment))
            throw new Error("Attachment argument must be a BaseAttachment");

        m_Attachments.push(attachment);
    };

    this.addAttachments = function (attachments) {

        if (!Array.isArray(attachments))
            throw new Error("Attachments argument must be a Array");

        attachments.forEach(that.addAttachment);
    };

    this.getAttachments = function () {
        return [].concat(m_Attachments);
    };
}