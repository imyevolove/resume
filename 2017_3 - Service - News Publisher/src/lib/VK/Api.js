﻿const request = require('request-promise');
const querystring = require('querystring');
const requestBase = require('request');
const extend = require('xtend');
const isPlainObject = require('is-plain-object');
const joi = require('joi');

module.exports = function (method, params) {

    return request.get('https://api.vk.com/method/' + method, {
        qs: extend(params),
        json: true,
        resolveWithFullResponse: true
    }).then(function (r) {

        if (r.body.hasOwnProperty('error')) {
            throw new Error("VK Api Error [Code: " + r.body.error.error_code + " Message: " + r.body.error.error_msg + "]");
        }

        if (!r.body.hasOwnProperty('response')) {
            throw new Error('No `response` field in API response');
        }

        return r.body.response;
        });
}
module.exports.post = function (method, params) {
    /**
    var options = {
        url: 'https://api.vk.com/method/' + method,
        body: querystring.stringify(params)
    };

    return requestBase.post(options, function (err, res, body) {
        if (err) return callback(err);

        try {
            var r = JSON.parse(body);
            if (r.hasOwnProperty('error')) {
                return callback(new Error("VK Api Error [Code: " + r.error.error_code + " Message: " + r.error.error_msg + "]"));
            }

            if (!r.hasOwnProperty('response')) {
                return callback(new Error('No `response` field in API response'));
            }

            callback(null, r);
        } catch (err) { callback(err); }
    });
     */

    var options = {
        method: 'POST',
        uri: 'https://api.vk.com/method/' + method,
        body: querystring.stringify(params)
    };

    return request(options).then(function (r) {
        var r = JSON.parse(r);

        if (r.hasOwnProperty('error')) {
            throw new Error("VK Api Error [Code: " + r.error.error_code + " Message: " + r.error.error_msg + "]");
        }

        if (!r.hasOwnProperty('response')) {
            throw new Error('No `response` field in API response');
        }

        return r.response;
    });
}