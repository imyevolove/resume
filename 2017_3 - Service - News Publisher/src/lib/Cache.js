﻿var fs = include("fs"),
    path = include("path"),
    mkdirp = include("mkdirp");

function fsExistsSync(path) {
    try {
        fs.accessSync(path);
        return true;
    } catch (e) {
        return false;
    }
}

module.exports = function Cache(filepath)
{
    var that = this;
    this.data = {};

    this.load = function () {
        try {
            var content = readFile(filepath);
            var dt = JSON.parse(content);
            if (dt) this.data = dt;
            return this.data;
        } catch (err) {}
    }

    this.save = function () {
        return writeFile(JSON.stringify(that.data), filepath);
    }

    constructor();
    function constructor() {
        if (filepath) that.load(filepath);
    }

    function readFile(filepath)
    {
        var fileExists = fsExistsSync(filepath);
        if (!fileExists) return null;

        try {
            var content = fs.readFileSync(filepath, "utf8");
            return content;
        } catch (err) { return null; }
    }

    function writeFile(text, filepath)
    {
        try {
            mkdirp.sync(path.dirname(filepath));
            fs.writeFileSync(filepath, text, "utf8");

            return true;
        } catch (err) {
            return false;
        }
    }
}