﻿module.exports = function DownloadFileResult() {
    this.error;
    this.status;
    this.url;
    this.destination;
}