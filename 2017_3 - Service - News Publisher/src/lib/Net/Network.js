﻿var needle = include("needle"),
    jsdom = require("jsdom"),
    request = include("request"),
    mkdirp = include('mkdirp'),
    path = include('path'),
    fs = include('fs');
var DownloadFileResult = include("./DownloadFileResult");

/**
 * 
 * @param {string} path
 * @return {bool}
 */
function fsExistsSync(path) {
    try {
        fs.accessSync(path);
        return true;
    } catch (e) {
        return false;
    }
}

function Network()
{
    this.requestHtml = function (url, callback) {
        jsdom.env(
            {
                url: url,
                done: function (err, win) {
                    if (!Function.prototype.isPrototypeOf(callback)) return;
                    callback({ error: err, response: win });
                    if (!err) win.close();
                }
            }
        );
    };

    /// Скачивает файл
    this.downloadFile = function (url, destination, callback) {

        /// 
        if (!Function.prototype.isPrototypeOf(callback))
            callback = function () { };

        ///
        mkdirp.sync(path.dirname(path.normalize(destination)));

        ///
        var result = new DownloadFileResult();
        result.url = url;
        result.destination = destination;


        ///
        request.head(url, function (err, res, body) {
            if (err) {
                result.status = false;
                result.error = err;

                callback(result);
                return;
            }

            request(url).pipe(fs.createWriteStream(destination)).on('close', function () {
                result.status = true;
                callback(result);
            });
        });
    };
}

module.exports = new Network();