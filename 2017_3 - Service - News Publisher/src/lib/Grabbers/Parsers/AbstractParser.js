﻿var Network = include("Net/Network");

module.exports = function AbstractParser() {
    this.parse = function (url, callback) {
        Network.requestHtml(url, callback);
    };
}