﻿var AbstractParser = include("./AbstractParser");
var ArticleDocument = include("Documents/ArticleDocument");
var DocumentUtils = include("Documents/DocumentUtils");
var TextAttachment  = include("Documents/Attachments/TextAttachment");
var ImageAttachment = include("Documents/Attachments/ImageAttachment");
var VideoAttachment = include("Documents/Attachments/VideoAttachment");

module.exports = function StopgameArticlesParser() {
    this.inherit(AbstractParser, arguments);

    var m_ParsHandlers = [];

    var m_FuncParse = this.parse;
    this.parse = function (url, callback) {

        if (!url.match(/^[a-zA-Z]+:\/\//)) {
            url = 'http://' + url;
        }

        m_FuncParse(url, function (result) {
            callback(result.error
                ? null
                : parseArticleDocument(result.response, result.response.document)); /// Too can be null
        });
    };

    function parseArticleDocument(window, document) {
        try {
            var articleDocument = new ArticleDocument();
            var titleContainerElement = document.querySelectorAll(".section-general-title")[1];
            var titleElement = titleContainerElement.getElementsByTagName("h1")[0];
            var mainTextElement = document.querySelector(".main_text");
            
            articleDocument.setTitle(titleElement.textContent);
            articleDocument.setSourceUrl(window.location.href);
            articleDocument.addAttachments(parseNodes(window, mainTextElement));

            return articleDocument;
        } catch (err) { console.log(err); return null; }
    }

    function parseNodes(window, element)
    {
        var attachments = [];
        var node = undefined;
        var _att = undefined;
        
        if (!element || !window.HTMLElement.prototype.isPrototypeOf(element)) return attachments;
        
        for (var i = 0; i < element.childNodes.length; i++)
        {
            node = element.childNodes[i];
            if (window.HTMLScriptElement.prototype.isPrototypeOf(node))
            {
                var matches = (node.textContent).match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/);
                if (matches && /youtube.com/.test(matches[0])) {
                    attachments.push(new VideoAttachment(matches[0]));
                }
                continue;
            }

            if (window.HTMLElement.prototype.isPrototypeOf(node) && node.className == "review_image") {
                var imageElement = node.getElementsByTagName("img")[0];
                if (imageElement) {
                    attachments.push(new ImageAttachment(imageElement.src));
                    continue;
                }
            }
            
            _att = DocumentUtils.objectToAttachment(element.childNodes[i]);
            if (!_att) {
                var r = parseNodes(window, element.childNodes[i]);
                if (r.length > 0)
                    attachments = attachments.concat(r);
            }
            else attachments.push(_att); 
        }

        return attachments;
    }
}