﻿var Network = include("Net/Network");

module.exports = function AbstractExtractor() {
    this.extract = function (url, callback) {
        Network.requestHtml(url, callback);
    };
}