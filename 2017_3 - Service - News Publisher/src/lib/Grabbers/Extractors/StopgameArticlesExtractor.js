﻿var AbstractExtractor = include("./AbstractExtractor");

module.exports = function StopgameArticlesExtractor() {
    this.inherit(AbstractExtractor, arguments);

    var m_FuncExtract = this.extract;
    this.extract = function (url, callback) {
        m_FuncExtract(url, function (result) {
            callback(result.error
                ? null
                : extractArticles(result.response.document));
        });
    };

    function extractArticles(document)
    {
        var newsContainerElement = document.querySelector(".section-news-lent");
        var newsArticleElements = newsContainerElement.querySelectorAll(".lent-block");
        newsArticleElements = [].slice.call(newsArticleElements);

        var articles = [];
        newsArticleElements.forEach(function (element) {
            var articleInfo = extractArticleInfo(element);
            if (articleInfo) articles.push(articleInfo);
        });

        return articles;
    }

    function extractArticleInfo(articleElement)
    {
        try {
            var briefContainerElement = articleElement.querySelector(".lent-brief");
            var titleElement = briefContainerElement.querySelector(".title");
                titleElement = titleElement.getElementsByTagName("a")[0];
            var briefElement = briefContainerElement.querySelector(".brief");
            var actionsElement = briefContainerElement.querySelector(".lent-actions");
            var actionPublishDateElement = actionsElement.querySelector(".lent-date");
            var actionViewsElement = actionsElement.querySelector(".lent-views");
            var linkElement = articleElement.getElementsByTagName("a")[0];

            var title = titleElement.textContent;
            var brief = briefElement.textContent.trim(" ")
            var publishData = actionPublishDateElement.textContent;
            var views = parseInt(actionViewsElement.textContent) || 0;
            var url = linkElement.href;
            url = url.replace(/(^\w+:|^)\/\//, '');

            var dateParts = publishData.split(".");
            var year = dateParts[2];
            var month = dateParts[1] - 1;
            month = month < 0 ? 0 : month;
            var day = dateParts[0];
            var time = new Date(year, month, day).getTime();

            return {
                title: titleElement.textContent,
                brief: brief,
                date: publishData,
                time: time,
                views: views,
                url: url
            }

        } catch (err) { return undefined; }
    }
}