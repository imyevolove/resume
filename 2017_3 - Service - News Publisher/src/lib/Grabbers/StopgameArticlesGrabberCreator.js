﻿var GrabberBuilder = include("./GrabberBuilder");
var StopgameArticlesExtractor = include("./Extractors/StopgameArticlesExtractor");
var StopgameArticlesParser = include("./Parsers/StopgameArticlesParser");

module.exports = new (function StopgameArticlesGrabberCreator() {
    this.create = function () {
        return new GrabberBuilder()
            .setExtractor(new StopgameArticlesExtractor())
            .setParser(new StopgameArticlesParser())
            .build();
    }
})();