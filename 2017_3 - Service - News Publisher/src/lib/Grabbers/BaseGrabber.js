﻿var AbstractExtractor = include("./Extractors/AbstractExtractor");
var AbstractParser = include("./Parsers/AbstractParser");

module.exports = function BaseGrabber(extractor, parser) {

    /// Validation input arguments
    if (!AbstractExtractor.prototype.isPrototypeOf(extractor))
        throw new TypeError("Argument 'extractor' must be a AbstractExtractor");
    if (!AbstractParser.prototype.isPrototypeOf(parser))
        throw new TypeError("Argument 'parser' must be a AbstractParser");

    this.extract = function (url, callback) {
        extractor.extract(url, callback);
    };

    this.parse = function (url, callback) {
        parser.parse(url, callback);
    };
}