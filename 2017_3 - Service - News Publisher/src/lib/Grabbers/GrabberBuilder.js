﻿var BaseGrabber = include("./BaseGrabber");
var AbstractExtractor = include("./Extractors/AbstractExtractor");
var AbstractParser = include("./Parsers/AbstractParser");

module.exports = function GrabberBuilder()
{
    var that = this;
    var m_Extractor,
        m_Parser;
    
    this.setExtractor = function (extractor) {

        if (!AbstractExtractor.prototype.isPrototypeOf(extractor))
            throw new TypeError("Argument 'extractor' must be a AbstractExtractor");

        m_Extractor = extractor;

        return this;
    };

    this.setParser = function (parser) {

        if (!AbstractParser.prototype.isPrototypeOf(parser))
            throw new TypeError("Argument 'parser' must be a AbstractParser");

        m_Parser = parser;

        return this;
    };

    this.build = function ()
    {
        if (!m_Extractor) throw new Error("Extractor not defined");
        if (!m_Parser) throw new Error("Parser not defined");

        return new BaseGrabber(m_Extractor, m_Parser);
    }
}