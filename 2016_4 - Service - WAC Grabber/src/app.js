var path = require("path");
require("hero-include");

/// Add library directories
include
    .using("./")
    .using("./src/lib")
    .using("./src/config");

global.env = global['env'] || {};
global.env.csgostash = {
    urls: {
        base: "https://csgostash.com/",
        cases: "https://csgostash.com/containers/skin-cases",
        stickers: "https://csgostash.com/stickers/regular",
        gloves: "https://csgostash.com/gloves",
        collections: "https://csgostash.com/",
        maps: "http://counterstrike.wikia.com/wiki/Counter-Strike:_Global_Offensive"
    }
};

global.env.gameAssetNames = {
    cases: "cases",
    gloves: "gloves",
    guns: "guns",
    knives: "knives",
    collections: "collections",
    stickers: "stickers",
    maps: "maps"
};

/// Extensions include
include("Extensions/InheritExtension");
include("Extensions/MathExtension");

var readline = include('readline');
var Command = include("Commands/Command");

/// Require AssetsDatabase / global scope
include("Database/AssetDatabase");
AssetDatabase.path = path.join(__dirname, "database");

global.relativeBuildsDirectory = "/builds/";
global.relativeBuildDirectory = "/builds/" + (new Date()).toISOString().replace(/[^a-zA-Z0-9]/g, '');
global.relativeBuildDocumentsDirectory = path.join(relativeBuildDirectory, "documents");
global.currencyRemoveDot = true;
global.requestCookieString = ""; /// Cookie for request

// Commands dictionary
var commands = [];
commands.push(include("Commands/UpdateAssetDatabaseCollectionsAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseCasesAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseStickersAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseGlovesAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseKnivesAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseGunsAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseMapsAssetCommand"));
commands.push(include("Commands/UpdateAssetDatabaseAllAssetsCommand"));
commands.push(include("Commands/DownloadAssetDatabaseMapsImagesCommand"));
commands.push(include("Commands/DownloadAllAssetDatabaseImagesCommand"));
commands.push(include("Commands/TransformMapImagesCommand"));
commands.push(include("Commands/BuildGameBundlesCommand"));
commands.push(include("Commands/BuildStatsTemplateCommand"));
commands.push(include("Commands/ExperimentsCommand"));

var cmdQuit = new Command(function () {
    console.log("quit");
    process.exit(1);
}, "Quit");
commands.push(cmdQuit);

program();
function program() {
    AssetDatabase.load();
    process.stdout.write('\x1Bc');

    var nextIndex = 0;

    console.log("--------------------------------------------------");
    for(var key in commands) {
        nextIndex = (Number(key) + 1);

        if(nextIndex >= commands.length)
            console.log("*******------------------------------------*******");
        console.log(nextIndex + ". " + commands[key].description);
    }
    console.log("--------------------------------------------------");

    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.question("input command: ", function(answer) {
        process.stdout.write('\x1Bc');
        rl.close();
        var index = Number(answer) - 1;

        if(!commands[index]) {
            console.log("unknown command");
            program();
        } else {
            commands[Number(answer) - 1].execute(function (data) {
                var rl2 = readline.createInterface({
                    input: process.stdin,
                    output: process.stdout
                });

                rl2.question("If you want try again write y: ", function(answer) {
                    rl2.close();
                    if (answer.toUpperCase() == "Y")
                        program();
                    else
                        cmdQuit.execute();
                });
            });
        }
    });
}