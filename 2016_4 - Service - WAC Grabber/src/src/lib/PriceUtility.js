﻿var priceUtility = {};

priceUtility.getEntityPrice = function (basePrice, level) {
    return Math.floor(basePrice * Math.pow(Math.log10(level + 10), level + 10));
}

priceUtility.getEntityPriceLog2 = function (basePrice, level) {
    return Math.floor(basePrice * Math.pow(Math.log2(level + 2), level + 2));
}

priceUtility.generatePrice = function (keys) {
    return keys + 1 + priceUtility.generateRandomKeys01();
};

priceUtility.getSkinPriceAverage = function (skin) {
    if (!skin || !skin.hasOwnProperty("collections") || !Array.isArray(skin.collections)) return 0;

    var counter = 0;
    var result = 0;

    for (var i in skin.collections) {
        if (!skin.collections[i].hasOwnProperty("price")) continue;
        result += skin.collections[i].price;
        counter++;
    }

    if (counter == 0) return 0;

    return result / counter;
};

priceUtility.getSkinPriceMin = function (skin) {
    if (!skin || !skin.hasOwnProperty("collections") || !Array.isArray(skin.collections)) return 0;

    var counter = 0;
    var result = skin.collections[0].price;

    for (var i in skin.collections) {
        if (!skin.collections[i].hasOwnProperty("price")) continue;
        result = Math.min(result, skin.collections[i].price);
    }

    return result;
};

priceUtility.getSkinPriceMax = function (skin) {
    if (!skin || !skin.hasOwnProperty("collections") || !Array.isArray(skin.collections)) return 0;

    var counter = 0;
    var result = skin.collections[0].price;

    for (var i in skin.collections) {
        if (!skin.collections[i].hasOwnProperty("price")) continue;
        result = Math.max(result, skin.collections[i].price);
    }

    return result;
};

priceUtility.generateRandomKeys01 = function ()
{
    return parseFloat(Math.random().toFixed(2));
}

module.exports = priceUtility;