﻿var path = include("path");
var Command = include("Commands/Command");
var GameDataBuilder = include("Services/Bundles/GameDataBuilder");

module.exports = new Command(function (callback) {
    var gmdBuilder = new GameDataBuilder();
    gmdBuilder.build(function (bundles) {
        
        var directory = path.normalize(path.join(AssetDatabase.path, "/_bundles"));
        gmdBuilder.writeBundlesToFiles(bundles, directory);
        
        callback();
    });

}, "Build game bundle");