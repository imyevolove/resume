﻿var Asset = include("Database/Asset");
var KnivesSearchEngine = include("Services/SearchEngines/KnivesSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.base)
    .setSearchEngine(new KnivesSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.knives))
    .setDescription("Update AssetDatabase Asset 'Knives'")
    .build();