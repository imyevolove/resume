﻿var Asset = include("Database/Asset");
var CasesSearchEngine = include("Services/SearchEngines/CasesSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.cases)
    .setSearchEngine(new CasesSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.cases))
    .setDescription("Update AssetDatabase Asset 'Cases'")
    .build();