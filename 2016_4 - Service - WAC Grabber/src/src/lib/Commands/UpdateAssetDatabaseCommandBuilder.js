﻿var Command = include("./Command");
var Asset = include("Database/Asset");
var BaseSearchEngine = include("Services/SearchEngines/BaseSearchEngine");

function UpdateAssetDatabaseCommandBuilder()
{
    var m_SearchEngine,
        m_Asset,
        m_Url,
        m_Description;

    ///
    this.setSearchEngine = function (engine) {
        if (!BaseSearchEngine.prototype.isPrototypeOf(engine))
            throw new TypeError();

        m_SearchEngine = engine;
        return this;
    };

    ///
    this.setAsset = function (asset) {
        if (!Asset.prototype.isPrototypeOf(asset))
            throw new TypeError();

        m_Asset = asset;
        return this;
    };

    ///
    this.setUrl = function (url) {
        if (typeof url != "string")
            throw new TypeError();

        m_Url = url;
        return this;
    };

    ///
    this.setDescription = function (description) {
        if (typeof description != "string")
            throw new TypeError();

        m_Description = description;
        return this;
    };

    /// Build command
    this.build = function ()
    {
        var canBuild = !(!m_SearchEngine || !m_Asset || !m_Url);
        if (!canBuild) throw new Error("Can't build. Missing property");

        var command = new Command(function (callback) {
            m_SearchEngine.request(m_Url, function (result) {
                if (!result.error) {
                    m_Asset.data = result.result;
                    m_Asset.mtime = Date.now();

                    AssetDatabase.setAsset(m_Asset);
                    AssetDatabase.save();
                }

                if (Function.prototype.isPrototypeOf(callback)) callback(result);
            });
        }, m_Description);
        
        return command;
    }
}

/// Export
module.exports = UpdateAssetDatabaseCommandBuilder;