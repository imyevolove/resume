function Command(action, description) {
    
    if (!Function.prototype.isPrototypeOf(action))
        throw new TypeError("Argument 'action' is not function");

    this.description = typeof description == "string" ? description : "";

    this.execute = function (callback) {
        action(callback);
    };
}

module.exports = Command;