﻿var Command = include("Commands/Command");
var QueueExecutor = include('QueueExecutor');

function QueueExecutorCommandBinder(command)
{
    return function (callback)
    {
        command.execute(callback.bind(null, null));
    }
}

/// Export
module.exports = new Command(function (callback) {

    var qExecutor = new QueueExecutor();
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseCasesAssetCommand")));
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseCollectionsAssetCommand")));
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseStickersAssetCommand")));
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseGlovesAssetCommand")));
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseKnivesAssetCommand")));
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseGunsAssetCommand")));
    qExecutor.addAction(QueueExecutorCommandBinder(include("Commands/UpdateAssetDatabaseMapsAssetCommand")));
    qExecutor.start(callback);

}, "Update all assets");