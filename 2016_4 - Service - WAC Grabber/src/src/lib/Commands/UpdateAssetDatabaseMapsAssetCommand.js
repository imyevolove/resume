﻿var Asset = include("Database/Asset");
var MapsSearchEngine = include("Services/SearchEngines/MapsSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.maps)
    .setSearchEngine(new MapsSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.maps))
    .setDescription("Update AssetDatabase Asset 'Maps'")
    .build();