﻿var Asset = include("Database/Asset");
var GunsSearchEngine = include("Services/SearchEngines/GunsSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.base)
    .setSearchEngine(new GunsSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.guns))
    .setDescription("Update AssetDatabase Asset 'Guns'")
    .build();