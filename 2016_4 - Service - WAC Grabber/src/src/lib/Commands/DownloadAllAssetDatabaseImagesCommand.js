﻿var Command = include("Commands/Command");
var Asset = include("Database/Asset");
var AssetDatabase = include("Database/AssetDatabase");
var Network = include("Network");
var QueueExecutor = include("QueueExecutor");
var ProgressIndicator = include("ProgressIndicator");
var path = include("path");

module.exports = new Command(function (callback) {

    var assets = AssetDatabase.getAllAssets();

    var mainQExecutor = new QueueExecutor();

    var genericProgressIndicator = new ProgressIndicator();
    genericProgressIndicator.width = assets.reduce(function (a, b) {
        return (typeof a == "object" ? a.data.length : a) + b.data.length;
    }, 0);

    /// Биндинг поосередной обработки ассетов
    assets.forEach(function (asset) {
        mainQExecutor.addAction(function (callback) {
            
            /// Обработка файлов ассета
            var assetQExecutor = new QueueExecutor();
            
            /// Биндинг поосередной обработки файлов ассета
            asset.data.forEach(function (item, index) {
                if (!item || !item.image)
                {
                    genericProgressIndicator.tick();
                    console.log(genericProgressIndicator.toString() + " Item broken [Index - " + index + "] [Asset - " + asset.getName() + "]");
                    return;
                }

                var image_url = item.image;
                var isValidExtension = Asset.extnameTest(image_url);
                var extensionOpt = isValidExtension ? null : ".png";
                var filepath = asset.getPathToFileByUrl(image_url, extensionOpt);

                assetQExecutor.addAction(function (callback) {

                    Network.compareRemoteAndLocalFiles(image_url, filepath, function (status) {
                        
                        if (!status) {
                            asset.downloadFile(image_url, ".png", function (status) {
                                genericProgressIndicator.tick();

                                if (!status) console.log(genericProgressIndicator.toString() + " File download error");
                                else console.log(genericProgressIndicator.toString() + " File downloaded: " + path.basename(image_url));

                                callback(!status);
                            });
                        } else {
                            genericProgressIndicator.tick();
                            console.log(genericProgressIndicator.toString() + " File already exists: " + path.basename(image_url));
                            callback();
                        }
                    });
                    
                });

            });

            /// Обработка результата обработки ассета
            assetQExecutor.start(function (error) {

                ///
                if (error) console.log("Fatal error");
                else console.log("Finished");

                /// 
                if (!Function.prototype.isPrototypeOf(callback)) return;
                callback(!!error);

            });
        });
    });

    /// Обработка результата
    mainQExecutor.start(function (error) {

        ///
        if (error) console.log("Fatal error");
        else console.log("Finished");

        /// 
        if (!Function.prototype.isPrototypeOf(callback)) return;
        callback(!error); /// Отправка статуса выполнения

    });

}, "Download AssetDatabase all images");