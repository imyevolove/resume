﻿var Command = include("Commands/Command");
var Asset = include("Database/Asset");
var AssetDatabase = include("Database/AssetDatabase");
var Network = include("Network");
var QueueExecutor = include("QueueExecutor");
var ProgressIndicator = include("ProgressIndicator");
var path = include("path");
var sharp = include("sharp");
var mkdirp = include("mkdirp");

module.exports = new Command(function (callback) {

    var asset = AssetDatabase.getAsset(env.gameAssetNames.maps);

    var mainQExecutor = new QueueExecutor();

    var genericProgressIndicator = new ProgressIndicator();
    genericProgressIndicator.width = asset.data.length;

    mainQExecutor.addAction(function (callback) {

        /// Обработка файлов ассета
        var assetQExecutor = new QueueExecutor();

        /// Биндинг поочередной обработки файлов ассета
        asset.data.forEach(function (item, index) {
            if (!item || !item.mapImageUrl || !item.mapIconImageUrl) {
                genericProgressIndicator.tick();
                console.log(genericProgressIndicator.toString() + " Item broken [Index - " + index + "] [Asset - " + asset.getName() + "]");
                return;
            }

            assetQExecutor.addAction(function (callback) {
                var itemsQExecutor = new QueueExecutor();

                addTransformImageActionToQueueExecutor(itemsQExecutor, asset, item, "mapImageUrl");

                itemsQExecutor.start(function () {
                    callback();
                });
            });

        });

        /// Обработка результата обработки ассета
        assetQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback(!!error);

        });
    });

    /// Обработка результата
    mainQExecutor.start(function (error) {

        ///
        if (error) console.log("Fatal error");
        else console.log("Finished");

        /// 
        if (!Function.prototype.isPrototypeOf(callback)) return;
        callback(!error); /// Отправка статуса выполнения

    });

}, "Transform map images");

function addTransformImageActionToQueueExecutor(qExecutor, asset, item, propertyName)
{
    qExecutor.addAction(function (callback) {
        var isValidExtension = Asset.extnameTest(item[propertyName]);
        var extensionOpt = isValidExtension ? null : ".png";

        var sourceImageFilepath = asset.getPathToFileByUrl(item[propertyName], extensionOpt);
        var transformedImageFilepath = path.normalize(path.dirname(sourceImageFilepath) + "/transformed" + "/" + path.basename(sourceImageFilepath));

        mkdirp.sync(path.dirname(transformedImageFilepath));
        console.log(transformedImageFilepath);

        sharp(sourceImageFilepath)
            .resize(512, 256)
            .withoutEnlargement()
            .greyscale()
            .toFile(transformedImageFilepath)
            .then(function () {
                console.log("OK");
                callback();
            })
            .catch(function (err) {
                console.log(err);
                callback();
            });
    });
}