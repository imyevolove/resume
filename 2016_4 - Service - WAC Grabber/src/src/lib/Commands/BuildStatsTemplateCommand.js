﻿var path = include("path");
var Command = include("Commands/Command");
var GameDataBuilder = include("Services/Bundles/GameDataBuilder");
var ConverterUtils = include("ConverterUtils");

module.exports = new Command(function (callback) {

    var assets = AssetDatabase.getAllAssetsAsDictionary()

    var names = [];
    names = names.concat(names, fetchAllEntityNamesFromAsset(assets[global.env.gameAssetNames.guns]));
    names = names.concat(names, fetchAllEntityNamesFromAsset(assets[global.env.gameAssetNames.knives]));
    names = names.concat(names, fetchAllEntityNamesFromAsset(assets[global.env.gameAssetNames.gloves]));
    
    var filteredNames = [];

    names.forEach(function (value) {
        if (filteredNames.indexOf(value) >= 0) return;
        filteredNames.push(value);
    });

    console.log(filteredNames);

    callback();

}, "Build stats template");

function fetchAllEntityNamesFromAsset(bundle)
{
    var names = [];

    for (var i in bundle.data)
    {
        var name = ConverterUtils.retriveSkinOriginalName(bundle.data[i].name).toUpperCase();

        if (names.indexOf(name) < 0 && !/default/i.test(name))
        {
            names.push(name);
        }
    }

    return names;
}