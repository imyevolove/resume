﻿var Asset = include("Database/Asset");
var StickersSearchEngine = include("Services/SearchEngines/StickersSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.stickers)
    .setSearchEngine(new StickersSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.stickers))
    .setDescription("Update AssetDatabase Asset 'Stickers'")
    .build();