﻿var Command = include("Commands/Command");
var Asset = include("Database/Asset");
var AssetDatabase = include("Database/AssetDatabase");
var Network = include("Network");
var QueueExecutor = include("QueueExecutor");
var ProgressIndicator = include("ProgressIndicator");
var path = include("path");

module.exports = new Command(function (callback) {

    var asset = AssetDatabase.getAsset(env.gameAssetNames.maps);

    var mainQExecutor = new QueueExecutor();

    var genericProgressIndicator = new ProgressIndicator();
    genericProgressIndicator.width = asset.data.length;

    mainQExecutor.addAction(function (callback) {

        /// Обработка файлов ассета
        var assetQExecutor = new QueueExecutor();
        
        /// Биндинг поочередной обработки файлов ассета
        asset.data.forEach(function (item, index) {
            if (!item || !item.mapImageUrl || !item.mapIconImageUrl) {
                genericProgressIndicator.tick();
                console.log(genericProgressIndicator.toString() + " Item broken [Index - " + index + "] [Asset - " + asset.getName() + "]");
                return;
            }

            addDownloadImageTaskToQExecutor(assetQExecutor, asset, item.mapImageUrl, {
                progress: genericProgressIndicator
            });
            addDownloadImageTaskToQExecutor(assetQExecutor, asset, item.mapIconImageUrl, {
                progress: genericProgressIndicator
            });

        });

        /// Обработка результата обработки ассета
        assetQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback(!!error);

        });
    });

    /// Обработка результата
    mainQExecutor.start(function (error) {

        ///
        if (error) console.log("Fatal error");
        else console.log("Finished");

        /// 
        if (!Function.prototype.isPrototypeOf(callback)) return;
        callback(!error); /// Отправка статуса выполнения

    });

}, "Download AssetDatabase Maps images");

function addDownloadImageTaskToQExecutor(qExecutor, asset, imageUrl, options)
{
    var isValidExtension = Asset.extnameTest(imageUrl);
    var extensionOpt = isValidExtension ? null : ".png";
    var filepath = asset.getPathToFileByUrl(imageUrl, extensionOpt);

    var progressTick    = options && options.progress ? options.progress.tick : function () { };
    
    qExecutor.addAction(function (callback) {

        Network.compareRemoteAndLocalFiles(imageUrl, filepath, function (status) {
            if (!status) {
                asset.downloadFile(imageUrl, extensionOpt, function (status) {
                    progressTick();

                    if (!status) console.log(" File download error");
                    else console.log("File downloaded: " + path.basename(imageUrl));

                    callback(!status);
                });
            } else {
                progressTick();
                console.log("File already exists: " + path.basename(imageUrl));
                callback();
            }
        });

    });
}