﻿var Asset = include("Database/Asset");
var CollectionsSearchEngine = include("Services/SearchEngines/CollectionsSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.cases)
    .setSearchEngine(new CollectionsSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.collections))
    .setDescription("Update AssetDatabase Asset 'Collections'")
    .build();