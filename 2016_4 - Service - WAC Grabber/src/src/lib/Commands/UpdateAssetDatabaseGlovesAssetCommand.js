﻿var Asset = include("Database/Asset");
var GlovesSearchEngine = include("Services/SearchEngines/GlovesSearchEngine");
var UpdateAssetDatabaseCommandBuilder = include("Commands/UpdateAssetDatabaseCommandBuilder");

/// Export
module.exports = new UpdateAssetDatabaseCommandBuilder()
    .setUrl(env.csgostash.urls.gloves)
    .setSearchEngine(new GlovesSearchEngine())
    .setAsset(new Asset(env.gameAssetNames.gloves))
    .setDescription("Update AssetDatabase Asset 'Gloves'")
    .build();