﻿function QueueExecutor()
{
    var m_Actions = [];

    /// Добавляет событие в список
    this.addAction = function (action) {
        if (!Function.prototype.isPrototypeOf(action))
            throw new TypeError("Argument 'action' is not a function");

        m_Actions.push(action);
    }

    /// Удаляет определенное значение
    this.removeAction = function (action) {
        m_Actions.splice(m_Actions.indexOf(action), 1);
    }

    /// Удаляет все события
    this.removeAllActions = function (action) {
        m_Actions = [];
    }

    /// Стартует исполнение
    this.start = function (callback) {
        /// Дублирование списка для избежания модификаций массива во время исполнения
        invoke([].concat(m_Actions), callback)
    }

    function invoke(actions, callback) {
        callback = Function.prototype.isPrototypeOf(callback) ? callback : function () { };

        var index = -1;
        next();

        function next(err)
        {
            index++;

            var args = [].slice.call(arguments);
            args.splice(0, 1); /// Remove error arg

            /// Действия закончились
            if (index >= actions.length) {
                args.splice();
                callback.apply(null, args);
                return;
            }

            if (!!err)
            {
                callback(Error.prototype.isPrototypeOf(err) ? err : new Error());
                return;
            }

            args = [next].concat([].slice.call(args));
            actions[index].apply(null, args);
        }
    }
}

QueueExecutor.create = function () {
    return new QueueExecutor();
}

module.exports = QueueExecutor;