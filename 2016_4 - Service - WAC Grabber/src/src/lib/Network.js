﻿var fs = include('fs'),
    request = include('request'),
    syncRequest = include('sync-request'),
    mkdirp = include('mkdirp'),
    path = include('path');

/**
 * 
 * @param {string} path
 * @return {bool}
 */
function fsExistsSync(path) {
    try {
        fs.accessSync(path);
        return true;
    } catch (e) {
        return false;
    }
}

function Network()
{
    /// Скачивает файл
    this.downloadFile = function (url, destination, callback) {

        /// 
        if (!Function.prototype.isPrototypeOf(callback))
            callback = function () { };

        ///
        mkdirp.sync(path.dirname(path.normalize(destination)));
        
        ///
        var result = new DownloadFileResult();
        result.url = url;
        result.destination = destination;


        ///
        request.head(url, function (err, res, body) {
            if (err) {
                result.status = false;
                result.error = err;

                callback(result);
                return;
            }

            request(url).pipe(fs.createWriteStream(destination)).on('close', function () {
                result.status = true;
                callback(result);
            });
        });
    }

    ///
    this.compareRemoteAndLocalFiles = function (url, path, callback) {
        try {
            if (!fsExistsSync(path)) throw new Error("File not exists");
            request.head(url, function (err, res, body) {
                var stats = fs.statSync(path);
                callback(stats.size == res.headers['content-length']);
            });
        } catch (err) { callback(false); }
    }
}

function DownloadFileResult()
{
    this.error;
    this.status;
    this.url;
    this.destination;
}

module.exports = new Network();