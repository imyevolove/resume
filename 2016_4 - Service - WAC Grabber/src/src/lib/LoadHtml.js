var jsdom = require("jsdom");

module.exports = function LoadHtml(url, callback) {
    jsdom.env(
        {
            url: url,
            done: function (errors, window) {
                callback(errors, window);
                window.close();
            }
        }
    );
};