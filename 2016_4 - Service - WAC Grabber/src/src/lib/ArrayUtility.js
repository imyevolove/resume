﻿module.exports.sortKeyByKey = function (arr, seq, ignoreCase) {
    if (ignoreCase)
    {
        arr = arr.map(function (item) { return item.toUpperCase(); });
        seq = seq.map(function (item) { return item.toUpperCase(); });
    }

    var n_seq = seq.concat().reverse();
    return _sortKeyByKey([]);


    function _sortKeyByKey(a) {
        if (n_seq.length <= 0) {
            return a.concat(arr.filter(function (e) {
                for (var key in seq) {
                    if (seq[key] == e) return false;
                }
                return true;
            }));
        }

        var key = n_seq.pop();
        a = a.concat(arr.filter(function (a, b) { return a == key || b == key; }));
        return _sortKeyByKey(a);
    }
}