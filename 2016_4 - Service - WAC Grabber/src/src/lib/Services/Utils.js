﻿function Utils()
{
    this.parseNumber = function (str, removeDot)
    {
        var number = 0;
        try {
            /// Первичная обработка данных
            number = str.replace(/,/g, ".").replace(/(\.)+/g, ".").match(/[0-9\.]+/)[0];

            /// Удаление лишних чисел перед разделителем
            number = parseFloat(number).toString();

            /// Выборочное удаление разделителя числа
            if (removeDot === true && number[0] != "0")
                number = number.replace(/\./g, "");

            /// Формирование конечного результата
            number = parseFloat(number);
        } catch (err) {
            number = 0;
        } finally {
            if (typeof number != "number") number = 0;
            return number;
        }
    }
}

module.exports = new Utils();