﻿var Utils = include("Services/Utils");
var LoadHtml = include("LoadHtml");

function SearchUtils()
{
    this.searchResultBoxes = function (document)
    {
        var container = document.querySelector(".main-content");
        var rowElements = container.querySelectorAll(".row");
        var rowStartIndex = 3;

        var itemElements = [];

        if (rowStartIndex >= 0 && rowStartIndex < rowElements.length) {

            /// Поиск ячеек во преполагаемых всех строках
            for (var i = rowStartIndex; i < rowElements.length; i++) {
                itemElements = itemElements.concat(retriveResultBoxesFromElement(rowElements[i]));
            }

        } else {

            /// Поиск ячеек по всему документу
            itemElements = itemElements.concat(retriveResultBoxesFromElement(document));

        }
        
        var items = itemElements.map(function (element) {
            var resultBox = new ResultBox();
            
            /// Title
            try {
                resultBox.title = element.getElementsByTagName("h3")[0].textContent;
            } catch (err) { }

            /// Image
            try {
                resultBox.image = element.getElementsByTagName("img")[0].src;
            } catch (err) { }

            /// Link
            try {
                resultBox.link = element.getElementsByTagName("img")[0].parentElement.href;
            } catch (err) { }

            /// Quality
            try {
                resultBox.quality = element.querySelector(".quality").children[0].textContent;
            } catch (err) { }

            /// Stattrak
            try {
                resultBox.stattrak = !!element.querySelector(".stattrak");
            } catch (err) { }

            /// Souvenir
            try {
                resultBox.stattrak = !!element.querySelector(".souvenir");
            } catch (err) { }

            /// Collection
            try {
                resultBox.collection = element.querySelector(".collection").getElementsByTagName("a")[0].textContent;
            } catch (err) { }

            /// Price money
            try {
                var priceElement = element.querySelector(".price").getElementsByTagName("a")[0];
                resultBox.prices.money = Utils.parseNumber(priceElement.innerHTML, global.currencyRemoveDot);
            } catch (err) { }

            /// Price keys
            try {
                var priceElement = element.querySelector(".price").getElementsByTagName("a")[0];
                resultBox.prices.keys = Utils.parseNumber(priceElement.attributes[3].value);
            } catch (err) { }

            return resultBox;
        });

        return items;
    }

    this.requestAndRetriveInformation = function (url, callback)
    {
        LoadHtml(url, function (err, win) {
            callback(err ? null : retriveFullResultBoxFromDocument(win.document));
        });
    }

    this.searchItemsInDropdownElements = function (document, indexOrArray)
    {
        try {
            var allDropdownElements = document.querySelector(".navbar-nav").querySelectorAll(".dropdown");
            var activeDropdownList = [];

            if (typeof indexOrArray == "number") {
                activeDropdownList.push(allDropdownElements[indexOrArray]);
            } else if (Array.isArray(indexOrArray)) {
                indexOrArray.forEach(function (index) {
                    activeDropdownList.push(allDropdownElements[index]);
                });
            } else {
                throw new TypeError("Invalid argгьуте ензу");
            }

            var links = [];

            for (var i = 0; i < activeDropdownList.length; i++)
                links = links.concat(retriveLinksFromDropdownElement(activeDropdownList[i]));

            return links;
        }
        catch (err) {
            return null
        }
    }

    function retriveFullResultBoxFromDocument(document)
    {
        var resultBox = new ResultBoxFullDetails();

        var resultBoxElement = document.querySelector(".result-box");
        var collectionContainerElement = document.querySelector(".skin-details-collection-container");
        var pricesContainerElement = document.querySelector(".price-details-table");

        /// Title
        try {
            resultBox.title = resultBoxElement.getElementsByTagName("h2")[0].textContent;
        } catch (err) { }

        /// Image
        try {
            resultBox.image = resultBoxElement.getElementsByTagName("img")[0].src;
        } catch (err) { }

        /// Link
        try {
            resultBox.link = document.location.href;
        } catch (err) { }

        /// Quality
        try {
            resultBox.quality = resultBoxElement.querySelector(".quality").children[0].textContent;
        } catch (err) { }

        /// Stattrak
        try {
            resultBox.stattrak = !!resultBoxElement.querySelector(".stattrak");
        } catch (err) { }

        /// Souvenir
        try {
            resultBox.souvenir = !!resultBoxElement.querySelector(".souvenir");
        } catch (err) { }

        /// Collections
        try {
            var collections = [].slice.call(collectionContainerElement.querySelectorAll(".collection-text-label"));
            resultBox.collections = collections.map(function (element) {
                return element.innerHTML;
            });
        } catch (err) { }

        /// Prices
        try {
            var tableBodyElement = pricesContainerElement.getElementsByTagName("tbody")[0];
            var tableRowElements = [].slice.call(tableBodyElement.getElementsByTagName("tr"));

            tableRowElements.forEach(function (rowElement) {
                var cellElements = rowElement.getElementsByTagName("td");
                var title = cellElements[0].textContent || "";
                title = title.replace(/[^\x20-\x7E]/gmi, "");

                /// Получение и парсинг цены с steam
                var steamPrice = cellElements[1].textContent || "";
                steamPrice = Utils.parseNumber(steamPrice, global.currencyRemoveDot);

                /// Получение и парсинг цены с bitskins
                var bitskinsPrice = cellElements[cellElements.length - 2].textContent || "";
                bitskinsPrice = Utils.parseNumber(bitskinsPrice, global.currencyRemoveDot);

                /// Получение и парсинг цены в ключах
                var keysPrice = cellElements[cellElements.length - 1].textContent || "";
                keysPrice = Utils.parseNumber(keysPrice);

                /// Проверка на доступ к записи
                var titleExists = !(typeof title != "string" || !title.length);
                var priceExists = !(!steamPrice && !bitskinsPrice && !keysPrice);

                if (!titleExists || !priceExists) return;

                /// Генерация результата
                resultBox.prices[title] = {
                    steam: steamPrice,
                    bitskins: bitskinsPrice,
                    keys: keysPrice
                }
            });

        } catch (err) { }

        return resultBox;
    }
    
    function retriveResultBoxesFromElement(element)
    {
        var itemElements = [];

        try {
            itemElements = element.querySelectorAll(".result-box")
            itemElements = [].slice.call(itemElements);
            itemElements = itemElements.filter(function (element) {
                try {
                    return (element.parentElement.className.indexOf("adv-result-box") < 0);
                } catch (err) { return false; }
            });
        } catch (err) {
            itemElements = [];
        } finally {
            return Array.isArray(itemElements) ? itemElements : [];
        }
    }

    function retriveLinksFromDropdownElement(element)
    {
        var dropdownMenu = element.querySelector(".dropdown-menu");

        var listElements = dropdownMenu.getElementsByTagName("a");
        listElements = [].slice.call(listElements);

        var links = listElements.map(function (element) {
            return element.href;
        });

        return links;
    }
}

function ResultBoxFullDetails() {
    this.link;
    this.title;
    this.image;
    this.quality;
    this.collections;
    this.stattrak;
    this.souvenir;
    this.prices = {};
}

function ResultBox()
{
    this.link;
    this.title;
    this.image;
    this.quality;
    this.collection;
    this.stattrak;
    this.souvenir;
    this.prices = {
        money: 0,
        keys: 0
    };
}

module.exports = new SearchUtils();