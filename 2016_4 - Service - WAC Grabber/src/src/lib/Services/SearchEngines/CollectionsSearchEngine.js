﻿var SearchUtils = include("./SearchUtils");
var CollectionData = include("./Schemes/CollectionData");
var CollectionItemData = include("./Schemes/CollectionItemData");

var ProgressBar = require('progress');
var waterfall = include('waterfall');
var QueueExecutor = include("QueueExecutor");
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function CollectionsSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (data, callback)
    {
        var document = data.document,
            link = data.link;
        
        var headerElement = document.querySelector(".content-header");
        var titleElement = headerElement.getElementsByTagName("h4")[0];
        var imageElement = headerElement.getElementsByTagName("img")[0];
        
        var collectionItem = new CollectionData();
        collectionItem.url = link;
        collectionItem.name = titleElement.innerHTML;
        collectionItem.image = "";

        var boxes = SearchUtils.searchResultBoxes(data.document);
        var targetBox = boxes[0];
        LoadHtml(targetBox.link, function (err, win) {
            if (err) throw new Error("Invalide link: " + targetBox.link);

            var document = win.document;
            var collectionContainerElement = document.querySelector(".skin-details-collection-container");
            var collections = [].slice.call(collectionContainerElement.getElementsByTagName("img"));
            var collection = collections[collections.length - 1];
            collectionItem.image = collection.src;
            
            callback(collectionItem);
        });
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR COLLECTIONS");

        var collectedData = [];
        var requestWaterfall = waterfall();

        /// Загрзка станицы с кейсами
        requestWaterfall.push(function (callback) {
            console.log("Page loading");
            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Загрзка списка кейсов
        requestWaterfall.push(function (document, callback) {

            var links = [];

            /// Кейсы
            var boxes = SearchUtils.searchResultBoxes(document);
            boxes.forEach(function (box) {
                links.push(box.link);
            });

            callback(null, links);
        });

        /// Загрузка каждой из страницы с кейсами
        requestWaterfall.push(function (links, callback) {

            var progressAK = new ProgressBar('[:bar] :rate/bps :percent :etas', { total: links.length });

            var collections = [];
            var casesQExecutor = new QueueExecutor();

            links.forEach(function (link) {

                /// Загрузка страницы с кейсом
                casesQExecutor.addAction(function (callback) {
                    LoadHtml(link, function (err, win) {
                        progressAK.tick();

                        if (err) {
                            callback(true);
                            return;
                        }

                        that.handler(
                            {
                                link: link,
                                document: win.document
                            }, function (item)
                            {
                                collections.push(item);
                                callback(false);
                            }
                        );
                    });
                });
            });
            
            /// Запуск и обработка результата выполнения
            casesQExecutor.start(function (error) {
                callback(!!error, collections);
            });
        });

        // Запуск и обработка ошибки
        requestWaterfall.callback(function (error, collections) {

            ///
            if (error) console.log("Fatal error");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: collections });

        });

    };
}