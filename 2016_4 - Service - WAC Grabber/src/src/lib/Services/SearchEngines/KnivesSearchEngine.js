﻿var Utils = include("Services/Utils");
var SearchUtils = include("./SearchUtils");
var KniveData = include("./Schemes/KniveData");
var QueueExecutor = include('QueueExecutor');

var ProgressBar = require('progress');
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function KnivesSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (box)
    {
        var item = new KniveData();
        item.name = box.title;
        item.quality = box.quality;
        item.stattrak = box.stattrak;
        item.souvenir = box.souvenir;
        item.url = box.link;
        item.image = box.image;
        item.collections = box.collections;
        item.prices = box.prices;

        return item;
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR KNIVES");

        var items = [];
        var requestQExecutor = new QueueExecutor();

        /// Загрзка основной страницы сайта
        requestQExecutor.addAction(function (callback) {
            console.log("page requesting");

            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Обработка загруженой страницы
        /// Извлечение списка 
        requestQExecutor.addAction(function (callback, document) {
            console.log("links retrieving");

            var links = SearchUtils.searchItemsInDropdownElements(document, 4);
            callback(!links, links);
        });

        /// Загрзка данных по ссылкам
        requestQExecutor.addAction(function (callback, links) {
            console.log("processing of primary information");

            requestAllKniveResultBoxes(links, function (result) {
                callback(!!result.error, result.result);
            });

        });

        /// Обработка данных
        requestQExecutor.addAction(function (callback, boxes) {
            console.log("requesting for all information");

            var links = boxes.map(function (box) {
                return box.link;
            });

            requestResultBoxesFullInformations(links, function (result) {
                callback(!!result.error, result.result);
            });

        });

        // Обработка Каждого элемента по отдельности
        requestQExecutor.addAction(function (callback, boxes) {

            boxes.forEach(function (box) {
                items.push(that.handler(box));
            });
            
            callback();

        });

        /// Обработка результата
        requestQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: items });

        });

    };

    function requestAllKniveResultBoxes(pageLinks, callback) {
        var progressAK = new ProgressBar('Retrieving result boxes [:bar] :rate/bps :percent :etas', { total: pageLinks.length });

        var resultBoxes = [];
        var altQExecutor = new QueueExecutor();

        ///
        /// Обработчик
        var handler = function (link, next, index) {
            LoadHtml(link, function (err, win) {

                progressAK.tick();

                if (err) {
                    next(true, err);
                    return;
                }

                resultBoxes = resultBoxes.concat(SearchUtils.searchResultBoxes(win.document));

                next(null, index + 1);
            });
        }

        ///
        /// Добавление обработчиков
        for (var i = 0; i < pageLinks.length; i++)
            altQExecutor.addAction(handler.bind(null, pageLinks[i]));

        ///
        altQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: resultBoxes });

        });
    }

    function requestResultBoxesFullInformations(links, callback) {
        var progressFI = new ProgressBar('Retrieving result boxes full information [:bar] :rate/bps :percent :etas', { total: links.length });

        var resultBoxes = [];
        var altQExecutor = new QueueExecutor();

        ///
        /// Обработчик
        var handler = function (link, next, index) {
            SearchUtils.requestAndRetriveInformation(link, function (resultBox) {
                progressFI.tick();

                if (!resultBox) {
                    next(true);
                    return;
                }
                
                resultBoxes.push(resultBox);

                next(null, index + 1);
            });
        }

        ///
        /// Добавление обработчиков
        for (var i = 0; i < links.length; i++)
            altQExecutor.addAction(handler.bind(null, links[i]));

        altQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: resultBoxes });

        });
    }
}
