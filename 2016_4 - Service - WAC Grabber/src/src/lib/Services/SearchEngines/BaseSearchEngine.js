﻿module.exports = function BaseSearchEngine()
{
    /// Per item data handler
    this.handler = function (data) { };

    /// Request
    this.request = function (url, callback) { };
}