﻿var Utils = include("Services/Utils");
var SearchUtils = include("./SearchUtils");
var GloveData = include("./Schemes/GloveData");

var ProgressBar = require('progress');
var waterfall = include('waterfall')
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function GlovesSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (data)
    {
        var item = new GloveData();
        var box = data.box,
            link = data.link;

        item.url = box.link;
        item.image = box.image;
        item.name = box.title;
        item.quality = box.quality;
        item.collections = box.collections;
        item.stattrak = !!box.stattrak;
        item.souvenir = !!box.souvenir;
        item.prices = box.prices;

        return item;
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR GLOVES");

        var collectedData = [];
        var requestWaterfall = waterfall();

        /// Загрзка основной страницы сайта
        requestWaterfall.push(function (callback) {
            console.log("Page loading");
            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Загрзка списка перчаток
        requestWaterfall.push(function (document, callback) {

            var links = [];

            var boxes = SearchUtils.searchResultBoxes(document);
            boxes.forEach(function (box) {
                links.push(box.link);
            });
            
            callback(null, links);

        });

        /// Загрзка кадого элемента по отдельности
        requestWaterfall.push(function (links, callback) {

            var collections = [];
            var altWaterfall = waterfall();

            var progress = new ProgressBar('Data loading and processing [:bar] :rate/bps :percent :etas', { total: links.length });

            ///
            /// Обработчик
            var handler = function (link, index, next) {
                SearchUtils.requestAndRetriveInformation(link, function (resultBox) {
                    progress.tick();

                    if (!resultBox) {
                        next(true);
                        return;
                    }

                    collections.push(that.handler({
                        link: link,
                        box: resultBox
                    }));

                    next(null, index + 1);
                });
            }

            ///
            /// Запуск перебора
            altWaterfall.push(function (next) {
                next(null, 0);
            });

            ///
            /// Добавление обработчиков
            for (var i = 0; i < links.length; i++)
                altWaterfall.push(handler.bind(null, links[i]));

            ///
            /// Завершение обработки
            altWaterfall.push(function () {
                arguments[arguments.length - 1](null); /// Исключение аргументов с данными
            });

            ///
            /// Ошибка обработки одного из элементов
            altWaterfall.callback(function (error) {
                if (error) console.log("error");
                callback(error, collections);
            });

        });
        
        // Запуск и обработка ошибки
        requestWaterfall.callback(function (error, items) {

            ///
            if (error) console.log("Fatal error");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: items });

        });

    };
}