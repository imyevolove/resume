﻿var Utils = include("Services/Utils");
var SearchUtils = include("./SearchUtils");
var StickerData = include("./Schemes/StickerData");

var ProgressBar = require('progress');
var waterfall = include('waterfall')
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function StickersSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (data)
    {
        var stickerItem = new StickerData();
        var box = data.box,
            link = data.link;

        stickerItem.name = box.title;
        stickerItem.url = box.link;
        stickerItem.image = box.image;
        stickerItem.prices.money = box.prices.money;
        stickerItem.prices.keys = box.prices.keys;
        stickerItem.quality = box.quality;

        return stickerItem;
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR STICKERS");

        var collectedData = [];
        var requestWaterfall = waterfall();

        /// Загрзка основной страницы сайта
        requestWaterfall.push(function (callback) {
            console.log("Page loading");
            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Получение ссылок на стикеры
        requestWaterfall.push(function (document, callback) {

            var links = [document.location.href];
            [].slice.call(document.querySelector(".pagination").getElementsByTagName("li")).filter(function (li) {
                return !(li.className == "disabled" || li.className == "active" || !!li.children[0].getAttribute("rel"));
            }).forEach(function (li) {
                links.push(li.children[0].href);
            });
            
            callback(null, links);

        });

        /// Загрзка данных
        requestWaterfall.push(function (links, callback) {

            var items = [];
            var altWaterfall = waterfall();

            var progress = new ProgressBar('Data loading and processing [:bar] :rate/bps :percent :etas', { total: links.length });

            ///
            /// Обработчик
            var handler = function (link, index, next) {
                LoadHtml(link, function (err, win) {

                    progress.tick();
                    
                    if (err) {
                        next(true, err);
                        return;
                    }

                    var boxes = SearchUtils.searchResultBoxes(win.document);
                    
                    boxes.forEach(function (box) {
                        items.push(that.handler({
                            box: box,
                            link: link
                        }));
                    });
                    
                    next(null, index + 1);
                });
            }

            ///
            /// Запуск перебора
            altWaterfall.push(function (next) {
                next(null, 0);
            });

            ///
            /// Добавление обработчиков
            for (var i = 0; i < links.length; i++)
                altWaterfall.push(handler.bind(null, links[i]));

            ///
            /// Завершение обработки
            altWaterfall.push(function () {
                arguments[arguments.length - 1](null); /// Исключение аргументов с данными
            });

            ///
            /// Ошибка обработки одного из элементов
            altWaterfall.callback(function (error) {
                if (error) console.log("error");
                callback(error, items);
            });

        });

        // Запуск и обработка ошибки
        requestWaterfall.callback(function (error, items) {

            ///
            if (error) console.log("Fatal error");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: items });

        });

    };
}