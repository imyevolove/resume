﻿var BaseScheme = include("./BaseScheme");

module.exports = function StickerData()
{
    this.inherit(BaseScheme, arguments);

    this.quality;
    this.url;
    this.prices = {
        money: 0,
        keys: 0
    };
}