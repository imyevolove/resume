﻿var BaseScheme = include("./BaseScheme");

module.exports = function GloveData()
{
    this.inherit(BaseScheme, arguments);
    
    this.quality = "";
    this.stattrak = false;
    this.souvenir = false;
    this.url = "";
    this.collections = [];
    this.prices = {};
}