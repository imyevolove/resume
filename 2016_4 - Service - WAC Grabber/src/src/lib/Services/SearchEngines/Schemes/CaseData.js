﻿var BaseScheme = include("./BaseScheme");

module.exports = function CaseData()
{
    this.inherit(BaseScheme, arguments);

    this.url;
    this.collection;
    this.prices = {
        money: 0,
        keys: 0
    };
    this.items = [];
}