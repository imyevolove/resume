﻿var BaseScheme = include("./BaseScheme");

module.exports = function MapData()
{
    this.inherit(BaseScheme, arguments);
    
    this.name = "";
    this.mapImageUrl = "";
    this.mapIconImageUrl = "";
}