﻿var Utils = include("Services/Utils");
var SearchUtils = include("./SearchUtils");
var CaseData = include("./Schemes/CaseData");
var CollectionItemData = include("./Schemes/CollectionItemData");

var ProgressBar = require('progress');
var waterfall = include('waterfall')
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function CasesSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (data)
    {
        var document = data.document,
            link = data.link;

        var headerElement = document.querySelector(".content-header");
        var titleElement = headerElement.getElementsByTagName("h1")[0];
        var collectionTitleElement = headerElement.getElementsByTagName("h4")[0];
        var imageElement = headerElement.getElementsByTagName("img")[0];

        var caseItem = new CaseData();
        caseItem.url = link;
        caseItem.name = titleElement.innerHTML;
        caseItem.image = imageElement.src;
        caseItem.collection = collectionTitleElement.innerHTML;

        /// Find prices
        var columns = [].slice.call(document.querySelectorAll(".col-md-6"));
        var priceColumns = columns.filter(function (element) {
            return element.children[0].innerHTML.toUpperCase() == "PRICES";
        });
        var keysColumns = columns.filter(function (element) {
            return element.children[0].innerHTML.toUpperCase() == "TRADING STATS";
        });
        var priceColumn = priceColumns[0];
        var keysColumn = keysColumns[0];
        var priceButtons = [];
        var statsButtons = [];
        var prices = [];
        var keysPrice = 0;

        if (priceColumn) {
            priceButtons = [].slice.call(priceColumn.querySelectorAll(".btn-group"));
            priceButtons.forEach(function (button) {
                var price = Utils.parseNumber(button.querySelector(".pull-right").innerHTML);
                if (!!price) prices.push(price);
            });
        }

        if (keysColumn) {
            statsButtons = [].slice.call(keysColumn.getElementsByTagName("td"));
            statsButtons.forEach(function (button) {
                if (button.textContent.toUpperCase().indexOf("KEYS") < 0) return;
                var price = Utils.parseNumber(button.querySelector(".pull-right").innerHTML);
                if (!!price) keysPrice = price;
            });
        }

        /// Set prices
        if (prices.length > 0)
            caseItem.prices.money = prices.reduce(function (a, b) { return a + b; }, 0) / prices.length;
        caseItem.prices.keys = keysPrice;

        var boxes = SearchUtils.searchResultBoxes(data.document);
        boxes.forEach(function (box) {
            var collectionDeepItem = new CollectionItemData();
            collectionDeepItem.fullname = box.title;

            caseItem.items.push(collectionDeepItem);
        });

        return caseItem;
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR CASES");

        var collectedData = [];
        var requestWaterfall = waterfall();

        /// Загрзка основной страницы сайта
        requestWaterfall.push(function (callback) {
            console.log("Page loading");
            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Загрзка списка кейсов
        requestWaterfall.push(function (document, callback) {

            var links = [];

            var boxes = SearchUtils.searchResultBoxes(document);
            boxes.forEach(function (box) {
                links.push(box.link);
            });
            
            callback(null, links);
        });

        /// Загрзка данных
        requestWaterfall.push(function (links, callback) {

            var items = [];
            var altWaterfall = waterfall();

            var progress = new ProgressBar('Data loading and processing [:bar] :rate/bps :percent :etas', { total: links.length });

            ///
            /// Обработчик
            var handler = function (link, index, next) {
                LoadHtml(link, function (err, win) {

                    progress.tick();

                    if (err) {
                        next(true, err);
                        return;
                    }

                    items.push(that.handler({
                        link: link,
                        document: win.document
                    }));

                    next(null, index + 1);
                });
            }

            ///
            /// Запуск перебора
            altWaterfall.push(function (next) {
                next(null, 0);
            });

            ///
            /// Добавление обработчиков
            for (var i = 0; i < links.length; i++)
                altWaterfall.push(handler.bind(null, links[i]));

            ///
            /// Завершение обработки
            altWaterfall.push(function () {
                arguments[arguments.length - 1](null); /// Исключение аргументов с данными
            });

            ///
            /// Ошибка обработки одного из элементов
            altWaterfall.callback(function (error) {
                if (error) console.log("error");
                callback(error, items);
            });

        });

        // Запуск и обработка ошибки
        requestWaterfall.callback(function (error, items) {

            ///
            if (error) console.log("Fatal error");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: items });

        });

    };
}