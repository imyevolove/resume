﻿var Utils = include("Services/Utils");
var SearchUtils = include("./SearchUtils");
var GunData = include("./Schemes/GunData");
var GunDefaultData = include("./Schemes/GunDefaultData");
var QueueExecutor = include('QueueExecutor');

var ProgressBar = require('progress');
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function GunsSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (box)
    {
        var item = new GunData();
        item.name = box.title;
        item.quality = box.quality;
        item.stattrak = box.stattrak;
        item.souvenir = box.souvenir;
        item.url = box.link;
        item.image = box.image;
        item.collections = box.collections;
        item.prices = box.prices;

        return item;
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR GUNS");

        var items = [];
        var requestQExecutor = new QueueExecutor();

        /// Загрзка основной страницы сайта
        requestQExecutor.addAction(function (callback) {
            console.log("page requesting");

            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Обработка загруженой страницы
        /// Извлечение списка 
        requestQExecutor.addAction(function (callback, document) {
            console.log("links retrieving");

            var links = SearchUtils.searchItemsInDropdownElements(document, [0, 1, 2, 3]);
            callback(!links, links);
        });

        /// Загрзка данных по ссылкам
        requestQExecutor.addAction(function (callback, links) {
            console.log("processing of primary information");

            requestAllKniveResultBoxes(links, function (result) {

                result.result.default.forEach(function (box) {
                    var item = new GunDefaultData();
                    item.name = box.title;
                    item.image = box.image;
                    items.push(item);
                });

                callback(!!result.error, result.result.skins);
            });

        });

        /// Обработка данных
        requestQExecutor.addAction(function (callback, boxes) {
            console.log("requesting for all information");

            var links = boxes.map(function (box) {
                if (!box.link) console.log(box);
                return box.link;
            });

            requestResultBoxesFullInformations(links, function (result) {
                callback(!!result.error, result.result);
            });

        });

        // Обработка Каждого элемента по отдельности
        requestQExecutor.addAction(function (callback, boxes) {

            boxes.forEach(function (box) {
                items.push(that.handler(box));
            });
            
            callback();

        });

        /// Обработка результата
        requestQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: items });

        });

    };

    function requestAllKniveResultBoxes(pageLinks, callback) {
        var progressAK = new ProgressBar('Retrieving result boxes [:bar] :rate/bps :percent :etas', { total: pageLinks.length });

        var resultBoxes = [];
        var defaultBoxes = [];
        var altQExecutor = new QueueExecutor();

        ///
        /// Обработчик
        var handler = function (link, next, index) {
            LoadHtml(link, function (err, win) {

                progressAK.tick();

                if (err) {
                    next(true, err);
                    return;
                }

                var boxesArray;
                var boxes = SearchUtils.searchResultBoxes(win.document);

                boxes.forEach(function (box) {
                    boxesArray = (/\b(default)\b/i.test(box.title)) ? defaultBoxes : resultBoxes;
                    boxesArray.push(box);
                });

                next(null, index + 1);
            });
        }

        ///
        /// Добавление обработчиков
        for (var i = 0; i < pageLinks.length; i++)
            altQExecutor.addAction(handler.bind(null, pageLinks[i]));

        ///
        altQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: { default: defaultBoxes, skins: resultBoxes} });

        });
    }

    function requestResultBoxesFullInformations(links, callback) {
        var progressFI = new ProgressBar('Retrieving result boxes full information [:bar] :rate/bps :percent :etas', { total: links.length });

        var resultBoxes = [];
        var altQExecutor = new QueueExecutor();

        ///
        /// Обработчик
        var handler = function (link, next, index) {
            SearchUtils.requestAndRetriveInformation(link, function (resultBox) {
                progressFI.tick();

                if (!resultBox) {
                    next(true);
                    return;
                }
                
                resultBoxes.push(resultBox);

                next(null, index + 1);
            });
        }

        ///
        /// Добавление обработчиков
        for (var i = 0; i < links.length; i++)
            altQExecutor.addAction(handler.bind(null, links[i]));

        altQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");
            else console.log("Finished");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: resultBoxes });

        });
    }
}
