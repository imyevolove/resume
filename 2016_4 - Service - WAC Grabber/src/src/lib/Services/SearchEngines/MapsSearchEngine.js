﻿var SearchUtils = include("./SearchUtils");
var MapData = include("./Schemes/MapData");

var ProgressBar = require('progress');
var waterfall = include('waterfall');
var QueueExecutor = include("QueueExecutor");
var LoadHtml = include("LoadHtml");
var BaseSearchEngine = include("./BaseSearchEngine");

module.exports = function MapsSearchEngine()
{
    this.inherit(BaseSearchEngine, arguments);
    var that = this;

    /// Обработчик одной единицы информации
    this.handler = function (data, callback)
    {
        try {
            if (!data || !data.name || !data.mapImageUrl || !data.mapIconImageUrl)
            {
                throw new Error("Invalid data");
            }

            var mapItem = new MapData();
            mapItem.name = data.name;
            mapItem.mapImageUrl = data.mapImageUrl;
            mapItem.mapIconImageUrl = data.mapIconImageUrl;

            callback(mapItem);
        } catch (err) { callback(null); }
    }

    /// Запрос информации
    this.request = function (url, callback) {

        console.log("START OF DATA REQUEST FOR MAPS");

        var collectedData = [];
        var requestWaterfall = waterfall();

        /// Загрзка станицы
        requestWaterfall.push(function (callback) {
            console.log("LOADING MAIN PAGE");
            LoadHtml(url, function (err, win) {
                callback(null, win.document);
            });
        });

        /// Загрзка списка
        requestWaterfall.push(function (document, callback) {

            var links = [];

            var mapsNavItem = [].slice.call(document.querySelectorAll(".nav-item")).find(function (navItem) {
                try {
                    return (/maps/i).test(navItem.getElementsByTagName("a")[0].href);
                } catch (err) { return false; }
            });

            if (!mapsNavItem)
            {
                callback(new Error("Maps navigation item not found"));
                return;
            }

            var mapsLinks = [];
            [].slice.call(mapsNavItem.getElementsByTagName("ul"))
                .slice(3)
                .forEach(function (ul) {
                    
                    [].slice.call(ul.getElementsByTagName("li")).forEach(function (li) {
                        try {
                            mapsLinks.push(li.getElementsByTagName("a")[0].href);
                        }
                        catch (err) { }
                    });
                });
            
            callback(null, mapsLinks);
        });

        /// Загрузка каждой из страниц
        requestWaterfall.push(function (links, callback) {
            var progressAK = new ProgressBar('[:bar] :rate/bps :percent :etas', { total: links.length });

            var collections = [];
            var dataCollections = [];
            var itemsQExecutor = new QueueExecutor();
            
            links.forEach(function (link) {

                /// Загрузка страницы с кейсом
                itemsQExecutor.addAction(function (callback) {
                    
                    var pagesQExecutor = new QueueExecutor();
                    var itemData = {};
                    
                    pagesQExecutor.addAction(function (pageCB) {
                        LoadHtml(link, function (err, win) {
                            
                            if (err) {
                                pageCB(true);
                                return;
                            }

                            try {

                                var headerElement = win.document.querySelector(".header-title");
                                var infoboxElement = win.document.querySelector(".infoboximage");

                                /// Invalid page. Skip
                                if (!headerElement || !infoboxElement)
                                {
                                    console.log("SKIP");
                                    pageCB(null, null);
                                    return;
                                }

                                itemData.name = headerElement.getElementsByTagName("h1")[0].innerHTML;
                                itemData.mapImageUrl = clearImageUrl(infoboxElement.getElementsByTagName("a")[0].href || "");

                                var galleryLink = win.document.getElementById("tab-2").getElementsByTagName("a")[0].href;
                                
                                pageCB(null, galleryLink);
                            } catch (err) { pageCB(err); }
                        });
                    });

                    pagesQExecutor.addAction(function (pageCB, link) {

                        /// Skip
                        if (!link)
                        {
                            pageCB(null);
                            return;
                        }

                        LoadHtml(link, function (err, win) {
                            progressAK.tick();

                            if (err) {
                                pageCB(true);
                                return;
                            }

                            try {
                                var targetFoundStatus = false;
                                var lightboxElements = [].slice.call(win.document.querySelectorAll(".lightbox-caption"))
                                var collectionElement = lightboxElements.find(function (col) {
                                    return (/collection/i).test(col.innerHTML) && (/set/i).test(col.parentElement.querySelector(".thumbimage").getAttribute("data-src"));
                                });
                                
                                itemData.mapIconImageUrl = collectionElement.parentElement.querySelector(".thumbimage").getAttribute("data-src");
                                itemData.mapIconImageUrl = clearImageUrl(itemData.mapIconImageUrl || "");
                                
                                pageCB(null);

                            } catch (err) { pageCB(new Error("Error when load gallery page")); }
                        });
                    });

                    pagesQExecutor.start(function (error) {
                        if (error)
                        {
                            console.log("Error: " + error.message);
                            console.log(error.stack);
                        }

                        dataCollections.push(itemData);
                        callback(!!error, itemData);
                    });
                });
            });
            
            // Обработка данных
            itemsQExecutor.addAction(function (callback) {

                dataCollections.forEach(function (data) {
                    that.handler(data, function (result) {
                        collections.push(result);
                    });
                });
                
                callback(null);
            });
            
            /// Запуск и обработка результата выполнения
            itemsQExecutor.start(function (error) {
                console.log("Loading items ended with " + (!error ? "no errors" : "error"));
                callback(!!error, collections);
            });
        });

        // Запуск и обработка ошибки
        requestWaterfall.callback(function (error, items) {

            ///
            if (error) console.log("Fatal error");
            else console.log("No errors");

            items = items.filter(function (item) { return !!item; });
            
            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback({ error: error, result: items });

        });

    };
}

function clearImageUrl(url)
{
    return url.replace(/(\/revision).*$/i, "")
}