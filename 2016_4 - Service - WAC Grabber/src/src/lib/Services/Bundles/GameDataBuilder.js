﻿var path = include("path");
var fs = include("fs");
var mkdirp = include("mkdirp");
var ConverterUtils = include("ConverterUtils");
var MultiBundleBuilder = include("./Builders/MultiBundleBuilder");
var KnivesBundleBuilder = include("./Builders/KnivesBundleBuilder");
var GunsBundleBuilder = include("./Builders/GunsBundleBuilder");
var GlovesBundleBuilder = include("./Builders/GlovesBundleBuilder");
var CasesBundleBuilder = include("./Builders/CasesBundleBuilder");
var CollectionsBundleBuilder = include("./Builders/CollectionsBundleBuilder");
var StickersBundleBuilder = include("./Builders/StickersBundleBuilder");
var MapsBundleBuilder = include("./Builders/MapsBundleBuilder");
var ArrayUtility = include("ArrayUtility");
var PriceUtility = include("PriceUtility");
var Bundle = include("./Bundle");

module.exports = function GameDataBuilder() {

    var that = this;
    var m_MultiBundleBuilder = new MultiBundleBuilder();

    var configs = {
        weaponsStats: null
    };

    refreshConfigs();

    addAssetToBuild(env.gameAssetNames.maps, MapsBundleBuilder);
    addAssetToBuild(env.gameAssetNames.knives, KnivesBundleBuilder);
    addAssetToBuild(env.gameAssetNames.guns, GunsBundleBuilder);
    addAssetToBuild(env.gameAssetNames.gloves, GlovesBundleBuilder);
    addAssetToBuild(env.gameAssetNames.cases, CasesBundleBuilder);
    addAssetToBuild(env.gameAssetNames.collections, CollectionsBundleBuilder);
    addAssetToBuild(env.gameAssetNames.stickers, StickersBundleBuilder);

    this.refreshConfigs = refreshConfigs;

    function refreshConfigs()
    {
        configs.weaponsStats = include("properties/weapons.stats.json");
    }

    this.build = function (callback) {
        m_MultiBundleBuilder.build(function (bundles) {

            var oBundles = transformBundlesArrayToDictionary(bundles);
            var _statsSkinsStart = oBundles[env.gameAssetNames.guns].data.length;
            
            console.log("Starting normalize bundles");
            normalizeBundles(bundles);

            console.log("Starting generate additional bundles");
            generateAdditionalBundles(bundles);

            console.log("Starting generate prices for default entities");
            generatePricesForDefaultEntities(bundles);

            console.log("Starting generate case prices");
            generateCasePrices(oBundles[env.gameAssetNames.cases].data, oBundles);
            normalizeCasePrices(oBundles[env.gameAssetNames.cases].data, oBundles);
            
            console.log("Starting normalize skin modifiers");
            normalizeSkinModifiers(bundles);

            console.log("Starting normalize skin prices");
            normalizePricesForSkinsInAllBundles(bundles);

            normalizePricesStep2(bundles);

            console.log("Starting normalize prices for all bundles");
            normalizePricesInAllBundlesAndAllProperties(oBundles);

            console.log("Skins count before normalizing process:", _statsSkinsStart);
            console.log("Skins count after normalizing process:", oBundles[env.gameAssetNames.guns].data.length);
            
            callback(bundles);
        });
    }

    this.writeBundlesToFiles = writeBundlesToFiles;
    
    function generateAdditionalBundles(bundles) {
        var oBundles = transformBundlesArrayToDictionary(bundles);
        bundles.push(generateGunsSequenceBundle(oBundles.guns));
        bundles.push(generateKnivesSequenceBundle(oBundles.knives));
        bundles.push(generateGlovesSequenceBundle(oBundles.gloves));
    }

    function generateGunsSequenceBundle(bundle) {
        var list = [];

        var gunsSequence = [];
        try {
            gunsSequence = include("properties/guns.sequence.json");
        } catch (err) { gunsSequence = []; console.log(err); }

        var defaultItems = bundle.data.filter(function (item) { return item.default; });
        var skinItems = bundle.data.filter(function (item) { return !item.default; });

        var defaultItemNames = defaultItems.map(function (item) { return item.name; });
        var sortedDefaultItemNames = ArrayUtility.sortKeyByKey(defaultItemNames, gunsSequence, true);

        sortedDefaultItemNames.forEach(function (name) {
            var item = defaultItems.find(function (item) { return (item && item.name.toLowerCase() == name.toLowerCase()); })
            if (item) list.push(item);
        });

        bundle.data = skinItems;
        
        /// Remove default items
        skinItems.forEach(function (item) { delete item.default; });

        defaultItems.forEach(function (item) {
            delete item.default;
            delete item.skins;
            delete item.rarity;
        });

        var bundle = new Bundle();
        bundle.name = "guns.default";
        bundle.data = list;

        return bundle;
    }

    function generatePricesForDefaultEntities(bundles)
    {
        var oBundles = transformBundlesArrayToDictionary(bundles);
        generatePricesForEntities(PriceUtility.getEntityPrice,      oBundles[env.gameAssetNames.guns + ".default"].data,    configs.weaponsStats.generic.base_price);
        generatePricesForEntities(PriceUtility.getEntityPriceLog2,  oBundles[env.gameAssetNames.knives + ".default"].data,  configs.weaponsStats.generic.knives.base_price);
        generatePricesForEntities(PriceUtility.getEntityPriceLog2,  oBundles[env.gameAssetNames.gloves + ".default"].data,  configs.weaponsStats.generic.gloves.base_price);
    }

    function generatePricesForEntities(calcFunc, entities, basePrice, indexOffset)
    {
        indexOffset = typeof indexOffset == "number" ? Math.abs(indexOffset) : 0;

        for (var i = 0; i < entities.length; i++)
        {
            entities[i]["stats"] = entities[i]["stats"] || {};
            entities[i].stats["base_price"] = calcFunc(basePrice, i + indexOffset);
        }
    }

    function normalizePricesStep2(bundles)
    {
        var oBundles = transformBundlesArrayToDictionary(bundles);
        var Knives = oBundles["knives.default"].data;

        for (var i in Knives)
        {
            Knives[i].stats.base_price /= 5;
            Knives[i].stats.base_price = Math.max(Knives[i].stats.base_price,
                configs.weaponsStats.generic.knives.base_price);
            Knives[i].stats.base_price = Math.floor(Knives[i].stats.base_price);
        }
    }

    function normalizePricesForSkinsInAllBundles(bundles)
    {
        var oBundles = transformBundlesArrayToDictionary(bundles);

        var defGuns = oBundles["guns.default"].data;
        var Guns = oBundles["guns"].data;

        var defKnives = oBundles["knives.default"].data;
        var Knives = oBundles["knives"].data;

        var defGloves = oBundles["gloves.default"].data;
        var Gloves = oBundles["gloves"].data;

        var cases = oBundles["cases"].data;

        normalizePricesForSkins(defGuns,    Guns);
        normalizePricesForSkins(defKnives,  Knives);
        normalizePricesForSkins(defGloves,  Gloves);
        normalizePricesForSkinsAtCasePrice(cases, oBundles);

        normalizePriceWithLimit(Knives, configs.weaponsStats.generic.knives.max_price);
    }

    function normalizePriceWithLimit(skins, limit)
    {
        var minPrice = getMinForSkins(skins);
        var maxPrice = getMaxForSkins(skins);
        var maxPriceNorm = maxPrice - minPrice;

        var tmpProgress = 0;

        for (var i in skins)
        {
            for (var o in skins[i].collections)
            {
                tmpProgress = (skins[i].collections[o].price - minPrice) / maxPriceNorm

                if (tmpProgress < 0 || tmpProgress > 1)
                {
                    console.log("CALCULATION ERROR&&&&&&&&&&&&&&&&&&&&&&&&", tmpProgress);
                }

                skins[i].collections[o].price = Math.ceil(Math.lerp(minPrice, limit, tmpProgress));
            }
        }
    }

    function getMinForSkins(skins)
    {
        var min = skins[0].collections[0].price;

        for (var i in skins) {
            min = Math.min(min, PriceUtility.getSkinPriceMin(skins[i]));
        }

        return min;
    }

    function getMaxForSkins(skins)
    {
        var max = 0;

        for (var i in skins) {
            max = Math.max(max, PriceUtility.getSkinPriceMax(skins[i]));
        }

        return max;
    }

    function normalizePricesForSkinsAtCasePrice(cases, bundlesDictionary)
    {
        var filteredSkinItems = [];
        var cs, skin, skinIndex, progress, price;
        
        for (var i in cases)
        {
            cs = cases[i];
            filteredSkinItems = cs.items
                .map(function (id) { return findSkinInBundles(id, { guns: bundlesDictionary.guns }) || null; })
                .filter(function (o) { return o != null; })
                .sort(function (a, b) { return getSkinMinStatsWeight(a) - getSkinMinStatsWeight(b); });
            
            skinIndex = 0;
            
            for (var o in filteredSkinItems)
            {
                progress = Math.lerp(0.1, 2, skinIndex / (filteredSkinItems.length - 1));
                price = progress * cs.price;

                skinIndex++;

                skin = filteredSkinItems[o];
                if (skin == null) continue;
                
                setCollectionsPrice(skin.collections, price * 0.5, price * 1);
            }
        }
    }

    function setCollectionsPrice(collections, minPrice, maxPrice)
    {
        var newPrice = 0;

        collections = collections.sort(function (a, b) { return a.price - b.price; });
        
        for (var i = 0; i < collections.length; i++)
        {
            newPrice = Math.lerp(minPrice, maxPrice, i / (collections.length - 1));

            if (newPrice < collections[i].price)
            {
                collections[i].price = Math.ceil(newPrice);
            }
        }
    }

    function getSkinMinStatsWeight(skin)
    {
        return getMinValueFromArrayAttributes(skin.collections, function (o) { return getStatsWeight(o.stats); });
    }

    function getStatsWeight(stats)
    {
        return stats.damage + stats.income * 0.5
    }

    function normalizePricesForSkins(entities, skins)
    {
        for (var i in entities)
        {
            var entitySkins = fetchSkins(skins, entities[i].id).reverse();
            generatePricesForSkins(entities[i], entitySkins);
        }
    }

    function generatePricesForSkins(entity, skins)
    {
        for (var i = 0; i < skins.length; i++) {
            var skinIndex = (i / (skins.length - 1));
            skinIndex = isNaN(skinIndex) ? 1 : skinIndex;

            var skinAveragePrice = Math.lerp(0.4, 1.6, skinIndex) * entity.stats.base_price;
            generatePricesForSkinCollections(skins[i], skinAveragePrice, skins[i].collections);
        }
    }

    function generatePricesForSkinCollections(skin, basePrice, collections)
    {
        var getCollectionPriceAttributeFunction = function (o) { return o.price; };

        var minPrice = getMinValueFromArrayAttributes(collections, getCollectionPriceAttributeFunction);
        var maxPrice = getMaxValueFromArrayAttributes(collections, getCollectionPriceAttributeFunction);

        for (var o = 0; o < collections.length; o++)
        {
            //var priceProgress = (collections[o].price - minPrice) / maxPrice;
            //var price = Math.lerp(0.5, 1.5, priceProgress) * basePrice;
            
            var priceProgress = Math.lerp(0.2, 1, o / collections.length);
            var price = priceProgress * ((collections[o].stats.damage + collections[o].stats.income * 0.5) * configs.weaponsStats.generic.skin_price);

            collections[o].price = Math.ceil(price);
        }

    }

    function normalizePricesInAllBundlesAndAllProperties(bundles)
    {
        normalizePriceInObject(bundles, 2);
    }

    function normalizePriceInObject(obj, count)
    {
        if (Object.prototype.isPrototypeOf(obj))
        {
            if (obj.hasOwnProperty("price") && typeof obj.price == "number")
            {
                obj.price = numberControlFloat(obj.price, count);
            }

            for (var i in obj)
            {
                normalizePriceInObject(obj[i], count);
            }
        }
    }

    function generateGlovesSequenceBundle(bundle) {
        var dict = {};
        var list = [];

        bundle.data.forEach(function (item) {
            if (!!dict[item.originalID]) return;
            dict[item.originalID] = {
                id: item.originalID,
                name: item.originalName,
            };
        });

        for (var n in dict)
            list.push(dict[n]);

        var bundle = new Bundle();
        bundle.name = "gloves.default";
        bundle.data = list;

        return bundle;
    }

    function generateKnivesSequenceBundle(bundle) {
        var dict = {};
        var list = [];

        bundle.data.forEach(function (item) {
            if (!!dict[item.originalID]) return;
            dict[item.originalID] = {
                id: item.originalID,
                name: item.originalName,
            };
        });

        for (var n in dict)
            list.push(dict[n]);

        var bundle = new Bundle();
        bundle.name = "knives.default";
        bundle.data = list;

        return bundle;
    }

    function normalizeSkinModifiers(bundles) {
        var oBundles = transformBundlesArrayToDictionary(bundles);

        var defGuns = oBundles["guns.default"].data;
        var Guns = oBundles["guns"].data;

        var defKnives = oBundles["knives.default"].data;
        var Knives = oBundles["knives"].data;

        var defGloves = oBundles["gloves.default"].data;
        var Gloves = oBundles["gloves"].data;

        generateStatsForEntities(defGuns, Guns, oBundles, true, configs.weaponsStats.generic);
        generateStatsForEntities(defKnives, Knives, oBundles, false, configs.weaponsStats.generic.knives);
        generateStatsForEntities(defGloves, Gloves, oBundles, false, configs.weaponsStats.generic.gloves);
    }
    
    function generateCasePrices(cases, bundlesDictionary) {
        var counter = 0;
        var sumPrice = 0;

        for (var i = 0; i < cases.length; i++)
        {
            counter = 0;
            sumPrice = 0;

            for (var o in cases[i].items)
            {
                var skin = findSkinInBundles(cases[i].items[o], bundlesDictionary);
                if (skin == null) continue;

                counter++;
                sumPrice += PriceUtility.getSkinPriceAverage(skin);
            }

            cases[i].price = numberControlFloat(sumPrice / counter, 2);
        }
    }

    function getSkinsMinPricesForCase(cs, bundlesDictionary)
    {
        var sumPrice = 1000000000;

        for (var o in cs.items) {
            var skin = findSkinInBundles(cs.items[o], bundlesDictionary);
            if (skin == null) continue;

            sumPrice = Math.min(sumPrice, PriceUtility.getSkinPriceAverage(skin));
        }
    }

    function getSkinsMaxPricesForCase(cs, bundlesDictionary)
    {
        var sumPrice = 0;

        for (var o in cs.items) {
            var skin = findSkinInBundles(cs.items[o], bundlesDictionary);
            if (skin == null) continue;

            sumPrice = Math.max(sumPrice, PriceUtility.getSkinPriceMax(skin));
        }
    }

    function normalizeCasePrices(cases, bundlesDictionary)
    {
        var previousPrice = 0;

        //generateTempPriceForCases(cases);
        var cases = cases.sort((a, b) => a.price - b.price);
        var maxCasePrice = getMaxValueFromArrayAttributes(cases, function (cs) { return cs.price; });

        for (var i = 0; i < cases.length; i++)
        {
            var index = Math.easing.easeInQuint(i / (cases.length - 1));
            var skinsMinPrice = getSkinsMinPricesForCase(cases[i], bundlesDictionary) * 10;

            cases[i].price = Math.lerp(
                configs.weaponsStats.generic.first_case_price,
                configs.weaponsStats.generic.last_case_price /*maxCasePrice*/, index);
            
            if (i > 0)
            {
                var diff = (previousPrice + configs.weaponsStats.generic.case_min_difference_price);
                
                if (diff > cases[i].price)
                {
                    cases[i].price = diff;
                }
            }

            if (cases[i].price < skinsMinPrice)
            {
                cases[i].price = skinsMinPrice;
            }

            cases[i].price = Math.ceil(cases[i].price);
            previousPrice = cases[i].price;

            console.log(cases[i].name + " --- ", cases[i].price);
        }
    }

    function generateTempPriceForCases(cases, bundlesDictionary)
    {
        for (var i = 0; i < cases.length; i++)
        {
            var skinsMinPrice = getSkinsMinPricesForCase(cases[i], bundlesDictionary) * 3;
            cases[i].price = skinsMinPrice;
        }
    }

    function findSkinInBundles(skinId, bundlesDictionary) {
        for (var i in bundlesDictionary) {
            for (var o in bundlesDictionary[i].data) {
                if (!bundlesDictionary[i].data[o].hasOwnProperty("id")) continue;
                if (bundlesDictionary[i].data[o].id != skinId) continue;
                return bundlesDictionary[i].data[o];
            }
        }

        return null;
    }

    function generateStatsForEntities(entitiesBundle, skinsBundle, bundlesDictionary, shiftForDefault, statsOptions) {
        var options = configs.weaponsStats;
        var entities = entitiesBundle;
        var skins = [];

        var shift = shiftForDefault ? 1 : 0;
        var skinOptions = {};

        var mainSkinCollectionIndex = 0;
        var summarySkinCollectionsLength = getCollectionSummaryFromSkins(skinsBundle);

        var stats = null;
        var previousStats = null;

        for (var i in entities) {
            skinOptions = options.weapons.find(function (e) {
                return e.id.toUpperCase() == entities[i].name.toUpperCase();
            });

            if (!skinOptions) {
                skinOptions = options.default;

                throw new Error("Stats not found for " + entities[i].name);
            }

            console.log("Generation stats for entity [" + entities[i].name + "]");
            
            stats = smartGenerateStats(skinOptions, statsOptions, entities[i].stats.base_price, 0, stats);
            normalizeStatsValues(stats, entities[i].stats.base_price);

            console.log("--------------------------------------------");
            
            normalizeStatsToULong(stats);
            applyStatsTo(entities[i], stats);

            skins = fetchSkins(skinsBundle, entities[i].id).reverse();
            generateStatsForSkins(bundlesDictionary, entities[i], skins, mainSkinCollectionIndex, summarySkinCollectionsLength, skinOptions, statsOptions);
            mainSkinCollectionIndex += getCollectionSummaryFromSkins(skins);
            
            previousStats = stats;
        }

        function getCollectionSummaryFromSkins(skins)
        {
            return skins.reduce((pv, cv) => (typeof pv == "number" ? pv : pv.collections.length) + cv.collections.length, 0);
        }
    }

    function comapreAndNormalizeStatsAtPreviousStats(stats, previousStats)
    {
        if (!previousStats || !stats) return;

        stats.dmg = normalizeValues(stats.dmg, previousStats.dmg);
        stats.inc = normalizeValues(stats.inc, previousStats.inc);

        function normalizeValues(valueA, valueB) {
            if (valueA <= valueB)
            {
                var dt = (valueB - valueA) || 1;
                dt += 1;
            }

            return Math.ceil(valueA + dt) || valueA;
        }
    }

    function smartGenerateStats(options, statsOptions, price, progress, previousStats)
    {
        progress = Math.clamp(progress, 0, 1);

        var dmg_prog_start = options["dmg.min"] / (options["dmg.min"] + options["inc.min"]);
        var inc_prog_start = 1 - dmg_prog_start;
        var dmg_prog_end   = options["dmg.max"] / (options["dmg.max"] + options["inc.max"]);
        var inc_prog_end   = 1 - dmg_prog_end;
        
        var dmg = price / statsOptions.damage_price;
        dmg *= Math.lerp(dmg_prog_start, dmg_prog_end, progress);
        dmg = Math.ceil(dmg);

        var inc = price / statsOptions.income_price;
        inc *= Math.lerp(inc_prog_start, inc_prog_end, progress);
        inc = Math.ceil(inc);
        return {
            dmg: dmg,
            inc: inc
        };
    }

    function helper_AddLimitStatsValue(value, mul, price)
    {
        if (value * mul > price) return price / mul;
        return value;
    }

    function helper_NormalizeStatsValue(value)
    {
        if (value < 1 && value !== 0) return 1;
        return value;
    }

    function generateStatsForSkins(bundlesDictionary, entity, skins, shift, sumCollections, options, statsOptions)
    {
        var stats = {};
        var previousStats = null;
        var lastStats = null;

        var getCollectionPriceAttributeFunction = function (o) { return o.price; };

        for (var i in skins)
        {
            var allSkins = findSkinsInBumdlesBySkinOriginalId(skins[i].originalID, bundlesDictionary);
            var prices = getStatsPricesFromSkins(allSkins);

            var minCollectionPrice = getMinValueFromArrayAttributes(skins[i].collections, getCollectionPriceAttributeFunction);
            var maxCollectionPrice = getMaxValueFromArrayAttributes(skins[i].collections, getCollectionPriceAttributeFunction);

            previousStats == null;

            for (var o in skins[i].collections)
            {
                var collectionPriceProgress = (skins[i].collections[o].price - minCollectionPrice) / maxCollectionPrice;
                
                stats = smartGenerateStats(options, statsOptions, entity.stats.base_price, collectionPriceProgress);
                
                ///stats = generateStatsForSkinsNormalHandler(options, skins[i], skins[i].collections[o], prices, counter, max);
                //stats = generateStatsForSkinsControlPriceHandler(allOptions["control.price"], skins[i].collections[o].price);

                normalizeStatsValues(stats, entity.stats.base_price);
                
                var shiftIndex = Math.cos(shift / sumCollections);

                //console.log(shift, shiftIndex, stats.dmg, stats.dmg * shiftIndex);

                stats.dmg = stats.dmg * shiftIndex;
                stats.inc = stats.inc * shiftIndex;
                
                normalizeStatsToULong(stats);
                
                comapreAndNormalizeStatsAtPreviousStats(stats, previousStats);
                applyStatsTo(skins[i].collections[o], stats);

                lastStats = stats;
                shift++;
            }
            
            previousStats = lastStats;
        }
    }

    function generateStatsForSkinsControlPriceHandler(options, price) {
        return getStatsForOptionByPriceControlled(options, price);
    }

    function generateStatsForSkinsNormalHandler(options, skin, collection, prices, counter, max) {
        try {
            var stats = getStatsForOptionByPrice(options, collection.price, prices.min, prices.max);
            return stats;
        }
        catch (e) {
            console.warn("Calculation stats by price error", e);
            return getStatsForOption(options, counter, max);
        }
    }

    function normalizeStatsValues(stats, price)
    {
        stats.dmg = helper_AddLimitStatsValue(stats.dmg, 100, price);
        stats.dmg = helper_NormalizeStatsValue(stats.dmg);

        stats.inc = helper_AddLimitStatsValue(stats.inc, 300, price);
        stats.inc = helper_NormalizeStatsValue(stats.inc);
    }

    function normalizeStatsToULong(stats)
    {
        stats.dmg = numberToULong(stats.dmg);
        stats.inc = numberToULong(stats.inc);

        return stats;
    }

    function numberToULong(value)
    {
        return Math.ceil(Math.abs(value));
    }

    function numberControlFloat(value, count)
    {
        return parseFloat(value.toFixed(count || 2))
    }

    function applyStatsTo(obj, stats) {
        obj["stats"] = obj["stats"] || {};
        obj.stats.damage = stats.dmg;
        obj.stats.income = stats.inc;
    }

    function findSkinsInBumdlesBySkinOriginalId(id, dictionaryBundles)
    {
        var result = [];
        var skins = [];

        for (var i in dictionaryBundles)
        {
            skins = findSkinsBySkinOriginalId(id, dictionaryBundles[i].data);
            result = result.concat(result, skins);
        }

        return result;
    }

    function findSkinsBySkinOriginalId(id, skins)
    {
        return skins.filter(function (s) {
            if (!s.hasOwnProperty("originalID")) return false;
            return s.originalID.toUpperCase() == id.toUpperCase();
        });
    }

    function getStatsPricesFromSkins(skins)
    {
        var counter = 0,
            min = 999999999,
            avg = 0,
            max = 0;

        var _price = 0;
        var _sMax = 0;
        var _sMin = 0;
        var _sAvg = 0;

        for (var i in skins)
        {
            _sMax = Math.max.apply(Math, skins[i].collections.map(function (o) { return o.price; }));
            _sMin = Math.min.apply(Math, skins[i].collections.map(function (o) { return o.price; }));
            _sAvg = Object.keys(skins[i].collections).reduce((acc, value) => acc + skins[i].collections[value].price, 0)
            _sAvg /= skins[i].collections.length;
            
            avg += _sAvg;
            min = Math.min(min, _sMin);
            max = Math.max(max, _sMax);
        }

        avg = avg / skins.length;

        return {
            min: min,
            max: max,
            average: avg
        };
    }

    function getStatsForOption(options, index, max)
    {
        var progress = index / max;
        
        return {
            dmg: options["dmg.min"] + (options["dmg.max"] - options["dmg.min"]) * progress,
            inc: options["inc.min"] + (options["inc.max"] - options["inc.min"]) * progress
        };
    }

    function getStatsForOptionByPrice(options, price, minPrice, maxPrice) {
        var progress = Math.clamp(price, minPrice, maxPrice) / maxPrice;

        return {
            dmg: options["dmg.min"] + (options["dmg.max"] - options["dmg.min"]) * progress,
            inc: options["inc.min"] + (options["inc.max"] - options["inc.min"]) * progress
        };
    }

    function getStatsForOptionByPriceControlled(options, price) {
        
        var ofsPrice = price - options.min.offset;
        var ofsMax = options.max.offset - options.min.offset;
        var progress = Math.clamp(ofsPrice, options.min.offset, ofsMax) / ofsMax;
        
        return {
            dmg: options.min.dmg + ofsMax * progress,
            inc: options.min.inc + ofsMax * progress
        };
    }

    function debugPrintSkinsInfo(skins)
    {
        for (var i in skins)
        {
            debugPrintSkinInfo(skins[i])
        }
    }

    function debugPrintSkinInfo(skin)
    {
        console.log(skin.name, skin.rarity);
    }

    function getMaxValueFromArrayAttributes(arr, getAttributeFunction)
    {
        return Math.max.apply(Math, arr.map(function (o) { return getAttributeFunction(o); }))
    }

    function getMinValueFromArrayAttributes(arr, getAttributeFunction)
    {
        return Math.min.apply(Math, arr.map(function (o) { return getAttributeFunction(o); }))
    }

    function fetchSkins(skinsData, id)
    {
        var skins = [];

        for (var i in skinsData)
        {
            if (skinsData[i].originalID == id)
            {
                skins.push(skinsData[i]);
            }
        }

        return skins;
    }

    function normalizeBundles(bundles)
    {
        var oBundles = transformBundlesArrayToDictionary(bundles);
        
        normalizeCasesBundle(oBundles.cases);
        normalizeCaseLinksBundle(oBundles.guns, oBundles);
        normalizeCaseLinksBundle(oBundles.knives, oBundles);
        normalizeCaseLinksBundle(oBundles.gloves, oBundles);
        normalizeSkins(oBundles.guns, oBundles);
        normalizeSkins(oBundles.knives, oBundles);
        normalizeSkins(oBundles.gloves, oBundles);
        normalizeGuns(oBundles.guns, oBundles);
    }

    function transformBundlesArrayToDictionary(bundles)
    {
        var oBundles = {};
        bundles.forEach(function (bundle) {
            oBundles[bundle.name] = bundle;
        });

        return oBundles;
    }

    /// Cases normalizer
    function normalizeCasesBundle(bundle) {
        bundle.data.forEach(function (caseItem) {
            caseItem.items = [];
        });
    }

    function normalizeSkins(bundle, bundles)
    {
        bundle.data.forEach(function (item) {

            if (item.default) {
                item.name = item.name.replace(/default/ig, "").trim(" ");
                item.id = ConverterUtils.convertStringToCode(item.name);
                item.skins = [];
                return;
            }

            item.skinName = ConverterUtils.retriveSkinName(item.name);
            item.originalName = ConverterUtils.retriveSkinOriginalName(item.name);
            item.originalID = ConverterUtils.convertStringToCode(item.originalName);
        });
    }

    /// normalizer
    function normalizeCaseLinksBundle(bundle, bundles) {
        var removeItems = [];

        bundle.data.forEach(function (item) {
            if (item.default) return;
            var items = [];
            
            item.availableIn.forEach(function (availableItem) {
                var _case = findCaseByIdInBundle(availableItem.id, bundles.cases);
                if (!_case) return;
                
                _case.items.push(item.id);
                items.push(_case.id);
            });

            item.availableIn = undefined;
            delete item.availableIn;

            if (!items.length) removeItems.push(item);
        });

        removeItems.forEach(function (item) {
            bundle.data.splice(bundle.data.indexOf(item), 1);
        });
    }

    function normalizeGuns(bundle, bundles) {
        var defaultItems = [];

        /// Default first
        bundle.data.forEach(function (item) {
            defaultItems.push(item);
        });

        ///Skins
        bundle.data.forEach(function (item) {
            if (item.default) return;

            var originalItem = findDefaultItemByOriginalID(item.originalID);
            if (originalItem) originalItem.skins.push(item.id);

        });

        function findDefaultItemByOriginalID(id)
        {
            for (var i in defaultItems)
                if (defaultItems[i].id == id) return defaultItems[i];
            return undefined;
        }
    }

    function findCaseBySkinIdInCasesArray(skinId, cases)
    {
        for (var key in cases)
        {
            if (cases[key].items.indexOf(skinId) >= 0)
            {
                return cases[key];
            }
        }

        return null;
    }

    function findCaseBySkinIdInBundle(skinId, bundle)
    {
        for (var key in bundle.data)
        {
            if (bundle.data[key].items.indexOf(skinId) >= 0)
            {
                return bundle.data[key];
            }
        }

        return null;
    }

    function findCaseByIdInBundle(id, bundle)
    {
        for (var key in bundle.data) {
            if (bundle.data[key].id == id) {
                return bundle.data[key];
            }
        }

        return undefined;
    }

    function findItemByIdInBundle(id, bundle)
    {
        for (var key in bundle.data)
        {
            if (bundle.data[key].id == id)
            {
                console.log(1);
                return bundle.data[key];
            }
        }
        return undefined;
    }

    function writeBundlesToFiles(bundles, directory) {

        bundles.forEach(function (bundle) {
            var fpath = path.normalize(path.join(directory, bundle.name.toLowerCase() + ".json"));
            writeBundleToFile(bundle, fpath);
        });
    }

    function writeBundleToFile(bundle, filepath) {
        var text = JSON.stringify(bundle.data);

        mkdirp.sync(path.dirname(filepath));
        fs.writeFileSync(filepath, text);
    }

    function addAssetToBuild(assetName, builder) {
        var asset = AssetDatabase.getAsset(assetName);
        if (!asset) {
            console.warn("Asset '" + assetName + "' not found");
            return false;
        }

        var bundleBuilder = new builder();
        m_MultiBundleBuilder.addBuilder(asset, bundleBuilder);

        console.warn("Asset '" + assetName + "' successfully added to build list");
        return true;
    }
}