﻿var BaseBundleBuilder = include("./BaseBundleBuilder");
var ConverterUtils = include("ConverterUtils");
var SkinRarity = include("Entities/SkinRarity");
var SkinSignature = include("Entities/SkinSignature");
var SkinCondition = include("Entities/SkinCondition");
var PriceUtility = include("PriceUtility");

module.exports = function GunsBundleBuilder()
{
    var that = this;
    this.inherit(BaseBundleBuilder, arguments);

    /* Add handlers */
    this.addBuildHandler(function (item, source, asset, callback) {

        /// ID
        item.id = ConverterUtils.convertStringToCode(source.name);

        /// Example: "Chroma 2 Case"
        item.name = source.name;

        /// Rarity
        item.rarity = source.default
            ? SkinRarity.TYPES.DEFAULT
            : SkinRarity.parse(source.quality) || SkinRarity.TYPES.CONSUMER;
        item.default = !!source.default;
        
        if (!source.default)
        {
            /// 
            //item.stattrak = source.stattrak;
            //item.souvenir = source.souvenir;
            item.signature = SkinSignature.generate(source.stattrak, source.souvenir);

            /// Image
            item.image = "";

            item.availableIn = [];

            if (source.collections)
            {
                item.availableIn = source.collections.map(function (name) {
                    return {
                        id: ConverterUtils.convertStringToCode(name)
                    };
                });
            }

            /// Collections list
            item.collections = [];

            for (var key in source.prices) {
                item.collections.push({
                    condition: SkinCondition.parse(key) || SkinCondition.TYPES.FACTORY_NEW,
                    signature: SkinSignature.parse(key) || SkinSignature.TYPES.BASE,
                    price: PriceUtility.generatePrice(source.prices[key].keys)
                });
            }

        }

        /// Normalize image path
        asset.getLocalFileOrDownloadByUrl(source.image, ".png", function (filepath) {
            if (!filepath) {
                callback(new Error("File not found and can't be downloaded"));
                return;
            }

            /// Image
            item.image = asset.getRelativePathToFileByUrl(source.image, ".png");
            callback();
        });

    });
}