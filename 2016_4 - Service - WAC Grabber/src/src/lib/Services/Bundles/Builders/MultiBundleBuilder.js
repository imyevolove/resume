﻿var QueueExecutor = include('QueueExecutor');
var BaseBundleBuilder = include("./BaseBundleBuilder");
var ProgressIndicator = include("ProgressIndicator");

module.exports = function MultiBundleBuilder()
{
    var that = this;
    var m_BuilderDetails = [];
    
    /// Добаляею билдер бандла
    this.addBuilder = function (asset, builder)
    {
        if (!BaseBundleBuilder.prototype.isPrototypeOf(builder))
            throw new TypeError("Argument 'builder' have a wrong type");

        m_BuilderDetails.push({ builder: builder, asset: asset });
    }

    /// Удаляет все билдеры
    this.removeAllBuilders = function () {
        m_BuilderDetails = [];
    }

    /// Построение
    this.build = function (callback) {

        var mainQExecutor = new QueueExecutor();
        var buildResults = [];

        var genericProgressIndicator = new ProgressIndicator();
        genericProgressIndicator.width = m_BuilderDetails.length;

        m_BuilderDetails.forEach(function (details) {
            /// Добавление обработчика на каждого из строителей
            mainQExecutor.addAction(function (callback) {
                /// Запуск обработки
                /// Слушаем окончание и продолжаем
                details.builder.build(details.asset, function (bundle) {

                    genericProgressIndicator.tick();
                    console.log(genericProgressIndicator.toString() + " Bundle builded [Asset - " + details.asset.getName() + "]");


                    buildResults.push(bundle);
                    callback();
                });
            });
        });

        /// Запуск и обработка результата выполнения
        mainQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback(buildResults);

        });
    };
}