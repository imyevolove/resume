﻿var QueueExecutor = include('QueueExecutor');
var Asset = include("Database/Asset");
var Bundle = include("Services/Bundles/Bundle");
var Invoker = include("./Invoker");
var ProgressIndicator = include("ProgressIndicator");

module.exports = function BaseBundleBuilder()
{
    var that = this;
    var m_BuildHandlers = [];

    /// Добавляет обработчик в список шагов
    this.addBuildHandler = function (handler) {
        if (!Function.prototype.isPrototypeOf(handler))
            throw new TypeError("Argument 'handler' have a wrong type");

        m_BuildHandlers.push(handler);
    };

    /// Удаляет обработчик
    this.removeBuildHandler = function (handler) {
        if (!Function.prototype.isPrototypeOf(handler))
            throw new TypeError("Argument 'handler' have a wrong type");

        return that.removeBuildHandlerByIndex(m_BuildHandlers.indexOf(handler));
    };

    /// Удаляет обработчик по индексу
    this.removeBuildHandlerByIndex = function (index) {
        if (!Function.prototype.isPrototypeOf(index))
            throw new TypeError("Argument 'index' have a wrong type");

        return !!m_BuildHandlers.splice(index, 1).length;
    };

    /// Удаляет все обработчики
    this.removeAllBuildHandler = function () {
        m_BuildStepHandlers = [];
    };

    /// Преобразует ассет в конкретный тип данных
    /// Возвращает функцию на следующий обработчик
    this.build = function (asset, callback) {
        if (!Asset.prototype.isPrototypeOf(asset))
            throw new TypeError("Argument 'asset' have a wrong type");

        var bundle = new Bundle();
        bundle.name = asset.getName().toLowerCase();

        return invokeBuilder(bundle, asset, [].concat(m_BuildHandlers), callback);
    };

    function invokeBuilder(bundle, asset, handlers, callback)
    {
        var genericProgressIndicator = new ProgressIndicator();
        genericProgressIndicator.width = asset.data.length;

        var wrappedHandlers = [];
        
        handlers.forEach(function (handler) {
            /// Обертка обработчиков для обработки каждого из элементов ассета
            wrappedHandlers.push(function (callback) {

                var itemsQExecutor = new QueueExecutor();

                asset.data.forEach(function (source) {

                    /// Добавление обработчика итема
                    itemsQExecutor.addAction(function (callback) {
                        var result = {};

                        handler(result, source, asset, function (err) {
                            bundle.data.push(result);

                            genericProgressIndicator.tick();
                            console.log(genericProgressIndicator.toString() + " Item processed [Asset - " + asset.getName() + "]");

                            callback(err);
                        });
                    });

                });

                /// Запуск и обработка результата выполнения
                itemsQExecutor.start(function (error) {

                    /// Все итемы обработаны
                    if (!Function.prototype.isPrototypeOf(callback)) return;
                    callback();

                });
            });
        });

        var mainQExecutor = new QueueExecutor();

        wrappedHandlers.forEach(function (handler) {
            /// Добавление обработчика
            mainQExecutor.addAction(function (callback) {
                /// Обработка
                handler(callback);
            });
        });

        /// Запуск и обработка результата выполнения
        mainQExecutor.start(function (error) {

            ///
            if (error) console.log("Fatal error");

            /// 
            if (!Function.prototype.isPrototypeOf(callback)) return;
            callback(bundle);

        });
    }
}

/*
    manager.addHandler(function(item, data, bundles) {})
*/