﻿var BaseBundleBuilder = include("./BaseBundleBuilder");
var StickerRarity = include("Entities/StickerRarity");
var ConverterUtils = include("ConverterUtils");
var PriceUtility = include("PriceUtility");

module.exports = function StickersBundleBuilder()
{
    var that = this;
    this.inherit(BaseBundleBuilder, arguments);

    /* Add handlers */
    this.addBuildHandler(function (item, source, asset, callback) {

        /// ID
        item.id = ConverterUtils.convertStringToCode(source.name);

        /// Example: "Chroma 2 Case"
        item.name = source.name;

        /// Image
        item.image = "";

        /// Price
        item.price = PriceUtility.generatePrice(source.prices.keys);
        
        /// Rarity Example: EXOTIC
        item.rarity = StickerRarity.parse(source.quality);
        
        /// Normalize image path
        asset.getLocalFileOrDownloadByUrl(source.image, ".png", function (filepath) {
            if (!filepath) {
                callback(new Error("File not found and can't be downloaded"));
                return;
            }

            /// Image
            item.image = asset.getRelativePathToFileByUrl(source.image, ".png");
            callback();
        });

    });
}