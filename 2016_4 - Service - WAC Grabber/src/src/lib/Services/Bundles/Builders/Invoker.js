﻿module.exports = function Invoker(handlers, callback)
{
    if (!Array.isArray(handlers))
        throw new Error("Argument 'handlers' must have a Array type");

    callback = Function.prototype.isPrototypeOf(callback) ? callback : function () { };

    var index = 0;
    var last_handler = handlers[handlers.length - 1];
    return next;
    
    function next()
    {
        if (index + 1 >= handlers.length) return null;

        var args = [next].concat([].slice.call(arguments));
        handlers[index].apply(null, args);

        if (last_handler == handlers[index])
            callback();

        index++;
        return next;
    }
}