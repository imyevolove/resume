﻿var BaseBundleBuilder = include("./BaseBundleBuilder");
var ConverterUtils = include("ConverterUtils");

module.exports = function CasesBundleBuilder()
{
    var that = this;
    this.inherit(BaseBundleBuilder, arguments);

    /* Add handlers */
    this.addBuildHandler(function (item, source, asset, callback) {

        /// ID
        item.id = ConverterUtils.convertStringToCode(source.name);

        /// Example: "Chroma 2 Case"
        item.name = source.name;

        /// Image
        item.image = "";
        
        /// Collection name Example: "The Chroma 2 Collection"
        item.collectionId = ConverterUtils.convertStringToCode(source.collection); 

        /// Collections list
        item.items = [];
        source.items.forEach(function (sitem) {
            if (!sitem || !sitem.fullname) return;
            item.items.push(ConverterUtils.convertStringToCode(sitem.fullname));
        });

        /// Normalize image path
        asset.getLocalFileOrDownloadByUrl(source.image, ".png", function (filepath) {
            if (!filepath) {
                callback(new Error("File not found and can't be downloaded"));
                return;
            }

            /// Image
            item.image = asset.getRelativePathToFileByUrl(source.image, ".png");
            callback();
        });
        
    });
}