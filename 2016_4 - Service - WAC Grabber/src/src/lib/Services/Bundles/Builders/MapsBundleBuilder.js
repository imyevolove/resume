﻿var BaseBundleBuilder = include("./BaseBundleBuilder");
var ConverterUtils = include("ConverterUtils");
var QueueExecutor = include("QueueExecutor");
var Asset = include("Database/Asset");

module.exports = function MapsBundleBuilder()
{
    var that = this;
    this.inherit(BaseBundleBuilder, arguments);

    /* Add handlers */
    this.addBuildHandler(function (item, source, asset, callback) {

        /// ID
        item.id = ConverterUtils.convertStringToCode(source.name);

        /// Example: "Chroma 2 Case"
        item.name = source.name;

        var itemsQExecutor = new QueueExecutor();

        addDownloadImageActionToQueueExecutor(itemsQExecutor, asset, item, source, "mapImageUrl", "cover");
        addDownloadImageActionToQueueExecutor(itemsQExecutor, asset, item, source, "mapIconImageUrl", "icon");

        itemsQExecutor.start(function () {
            callback();
        });

    });
}

function addDownloadImageActionToQueueExecutor(qExecutor, asset, item, source, propertyName, extPropertyName)
{

    var isValidExtension = Asset.extnameTest(source[propertyName]);
    var extensionOpt = isValidExtension ? null : ".png";

    qExecutor.addAction(function (callback) {
        asset.getLocalFileOrDownloadByUrl(source[propertyName], extensionOpt, function (filepath) {
            if (!filepath) {
                callback(new Error("File not found and can't be downloaded"));
                return;
            }

            /// Image
            item[extPropertyName] = asset.getRelativePathToFileByUrl(source[propertyName], extensionOpt);
            callback();
        });
    });
}