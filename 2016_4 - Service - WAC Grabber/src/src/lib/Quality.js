﻿function Quality()
{
    this.type = Quality.TYPES
}

/// Quality types
Quality.STATES = {};
Quality.STATES.BATTLE_SCARRED = 0;
Quality.STATES.WELL_WORN      = 1;
Quality.STATES.FIELD_TESTED   = 2;
Quality.STATES.MINIMAL_WEAR   = 3;
Quality.STATES.FACTORY_NEW = 4;

Quality.TYPES.UNTYPED = "UNTYPED";
Quality.TYPES.CONSUMER = "CONSUMER";
Quality.TYPES.INDUSTRIAL = "INDUSTRIAL";
Quality.TYPES.MILSPEC = "MILSPEC";
Quality.TYPES.RESTRICTED = "RESTRICTED";
Quality.TYPES.COVERT = "COVERT";
Quality.TYPES.GOLD = "GOLD";

module.eqports = Quality;