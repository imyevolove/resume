﻿var crypto = include("crypto");

function ConverterUtils()
{
    this.convertStringToCode = function (str) {
        str = str.toUpperCase().replace(/[^\w\|\(\)\-]/ig, "");
        str = crypto.createHmac('sha1', "code")
            .update(str)
            .digest('hex');
        return str;
    }

    this.retriveSkinName = function (str) {
        return str.replace(/(.+)(\|)/, "").trim(" ");
    }

    this.retriveSkinOriginalName = function (str) {
        return str.replace(/(\|)(.+)/, "").trim(" ");
    }
}

module.exports = new ConverterUtils();