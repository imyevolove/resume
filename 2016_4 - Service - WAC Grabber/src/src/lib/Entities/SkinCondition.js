﻿function SkinCondition()
{
    
}

SkinCondition.TYPES = {
    BATTLE_SCARRED: "BATTLE_SCARRED",
    WELL_WORN: "WELL_WORN",
    FIELD_TESTED: "FIELD_TESTED",
    MINIMAL_WEAR: "MINIMAL_WEAR",
    FACTORY_NEW: "FACTORY_NEW"
};

SkinCondition.rgxs = {
    battleScared:   /(?=.*battle)(?=.*scarred)/i,
    wellWorn:       /(?=.*well)(?=.*worn)/i,
    fieldTested:    /(?=.*field)(?=.*tested)/i,
    minimalWear:    /(?=.*minimal)(?=.*wear)/i,
    factoryNew:     /(?=.*factory)(?=.*new)/i,
};

SkinCondition.parse = function (str) {
    if (SkinCondition.rgxs.battleScared.test(str)) return SkinCondition.TYPES.BATTLE_SCARRED;
    if (SkinCondition.rgxs.wellWorn.test(str)) return SkinCondition.TYPES.WELL_WORN;
    if (SkinCondition.rgxs.fieldTested.test(str)) return SkinCondition.TYPES.FIELD_TESTED;
    if (SkinCondition.rgxs.minimalWear.test(str)) return SkinCondition.TYPES.MINIMAL_WEAR;
    if (SkinCondition.rgxs.factoryNew.test(str)) return SkinCondition.TYPES.FACTORY_NEW;
    return undefined;
};

module.exports = SkinCondition;