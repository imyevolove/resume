﻿function GloveRarity()
{
    
}

GloveRarity.TYPES = {
    EXTRAORDINARY: "EXTRAORDINARY"
};

GloveRarity.rgxs = {
    extraordinary:  /(?=.*extraordinary)/i
};

GloveRarity.parse = function (str) {
    if (GloveRarity.rgxs.extraordinary.test(str)) return GloveRarity.TYPES.EXTRAORDINARY;
    return undefined;
};

module.exports = GloveRarity;