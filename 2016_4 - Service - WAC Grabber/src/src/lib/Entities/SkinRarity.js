﻿function SkinRarity()
{
    
}

SkinRarity.TYPES = {
    DEFAULT: "DEFAULT",
    CONSUMER: "CONSUMER",
    INDUSTRIAL: "INDUSTRIAL",
    MILSPEC: "MILSPEC",
    RESTRICTED: "RESTRICTED",
    CLASSIFIED: "CLASSIFIED",
    COVERT: "COVERT",
    GOLD: "GOLD",
    CONTRABAND: "CONTRABAND"
};

SkinRarity.rgxs = {
    consumer:       /consumer/i,
    industrial:     /industrial/i,
    milspec:        /milspec|mil-spec/i,
    restricted:     /restricted/i,
    classified:     /classified/i,
    covert:         /covert/i,
    gold:           /gold/i,
    contraband:     /contraband/i
};

SkinRarity.parse = function (str) {
    if (SkinRarity.rgxs.consumer.test(str)) return SkinRarity.TYPES.CONSUMER;
    if (SkinRarity.rgxs.industrial.test(str)) return SkinRarity.TYPES.INDUSTRIAL;
    if (SkinRarity.rgxs.milspec.test(str)) return SkinRarity.TYPES.MILSPEC;
    if (SkinRarity.rgxs.restricted.test(str)) return SkinRarity.TYPES.RESTRICTED;
    if (SkinRarity.rgxs.classified.test(str)) return SkinRarity.TYPES.CLASSIFIED;
    if (SkinRarity.rgxs.covert.test(str)) return SkinRarity.TYPES.COVERT;
    if (SkinRarity.rgxs.gold.test(str)) return SkinRarity.TYPES.GOLD;
    if (SkinRarity.rgxs.contraband.test(str)) return SkinRarity.TYPES.CONTRABAND;
    return undefined;
};

module.exports = SkinRarity;