﻿function SkinSignature()
{
    
}

SkinSignature.TYPES = {
    BASE: "BASE",
    STATTRAK: "STATTRAK",
    SOUVENIR: "SOUVENIR"
};

SkinSignature.rgxs = {
    stattrak: /stattrak/i,
    souvenir: /souvenir/i
};

SkinSignature.generate = function (stattrak, souvenir) {
    if (stattrak) return SkinSignature.TYPES.STATTRAK;
    if (souvenir) return SkinSignature.TYPES.SOUVENIR;
    return SkinSignature.TYPES.BASE;
}

SkinSignature.parse = function (str) {
    if (SkinSignature.rgxs.stattrak.test(str)) return SkinSignature.TYPES.STATTRAK;
    if (SkinSignature.rgxs.souvenir.test(str)) return SkinSignature.TYPES.SOUVENIR;
    return undefined;
};

module.exports = SkinSignature;