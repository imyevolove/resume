﻿function StickerRarity()
{
    
}

StickerRarity.TYPES = {
    HIGH_GRADE: "HIGH_GRADE",
    REMARKABLE: "REMARKABLE",
    EXOTIC: "EXOTIC",
    CONTRABAND: "CONTRABAND"
};

StickerRarity.rgxs = {
    highgrade:  /(?=.*high)(?=.*grade)/i,
    remarkable: /(?=.*remarkable)/i,
    exotic:     /(?=.*exotic)/i,
    contraband: /(?=.*contraband)/i,
};

StickerRarity.parse = function (str) {
    if (StickerRarity.rgxs.highgrade.test(str))     return StickerRarity.TYPES.HIGH_GRADE;
    if (StickerRarity.rgxs.remarkable.test(str))    return StickerRarity.TYPES.REMARKABLE;
    if (StickerRarity.rgxs.exotic.test(str))        return StickerRarity.TYPES.EXOTIC;
    if (StickerRarity.rgxs.contraband.test(str))    return StickerRarity.TYPES.CONTRABAND;
    return undefined;
};

module.exports = StickerRarity;