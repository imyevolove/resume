﻿module.exports = function ProgressIndicator()
{
    var that = this;

    this.width = 0;
    this.current = 0;

    this.tick = function () { that.current++; }

    this.getPercentage = function () {
        if (that.current >= that.width) return 100;
        return Math.floor(100 / that.width * that.current);
    }

    this.toString = function ()
    {
        return "[" + that.current + "/" + that.width + "]";
    }
}