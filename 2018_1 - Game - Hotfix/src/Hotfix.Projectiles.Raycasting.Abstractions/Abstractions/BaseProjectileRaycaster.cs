﻿using System;
using UnityEngine;

namespace Hotfix.Projectiles.Raycasting
{
    [Serializable]
    public abstract class BaseProjectileRaycaster : IProjectileRaycaster
    {
        [SerializeField] private ProjectileRaycastSettings _settings;
        public ProjectileRaycastSettings Settings { get => _settings; set => _settings = value; }

        public abstract ProjectileOperation Raycast(ProjectileRequest request);
    }
}
