﻿using System.Linq;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace Hotfix.Projectiles.Raycasting
{
    public static class ProjectileRaycastUtils
    {
        public static bool BatchedRaycastBackface(RaycastHit initialHit, out RaycastHit hit, Vector3 direction)
        {
            var distance = initialHit.collider.bounds.size.magnitude + .5f;
            var origin = initialHit.point + direction * distance;

            BatchedColliderRaycast(new Ray(origin, -direction), out hit, initialHit.collider, distance);

            return hit.collider != null;
        }
        public static bool NativeRaycastBackface(RaycastHit initialHit, out RaycastHit hit, Vector3 direction)
        {
            var distance = initialHit.collider.bounds.size.magnitude + .5f;
            var origin = initialHit.point + direction * distance;
            distance += .05f;

            initialHit.collider.Raycast(new Ray(origin, -direction), out hit, distance);

            return hit.collider != null;
        }
        public static bool BatchedColliderRaycast(Ray ray, out RaycastHit hit, Collider collider, float maxDistance, int bufferSize = 10, JobHandle dependsOn = default(JobHandle))
        {
            var results = new NativeArray<RaycastHit>(bufferSize, Allocator.TempJob);
            var commands = new NativeArray<RaycastCommand>(1, Allocator.TempJob)
            {
                [0] = new RaycastCommand(ray.origin, ray.direction, maxDistance, 1 << collider.gameObject.layer, bufferSize)
            };

            var handle = RaycastCommand.ScheduleBatch(commands, results, 1, dependsOn);
            handle.Complete();

            hit = results.FirstOrDefault(r => r.collider != null && ReferenceEquals(collider, r.collider));

            results.Dispose();
            commands.Dispose();

            return hit.collider != null;
        }
        public static bool BatchedRaycast(Ray ray, out RaycastHit hit, LayerMask layerMask, float maxDistance, JobHandle dependsOn = default(JobHandle))
        {
            var results = new NativeArray<RaycastHit>(1, Allocator.TempJob);
            var commands = new NativeArray<RaycastCommand>(1, Allocator.TempJob)
            {
                [0] = new RaycastCommand(ray.origin, ray.direction, maxDistance, layerMask)
            };

            var handle = RaycastCommand.ScheduleBatch(commands, results, 1, dependsOn);
            handle.Complete();

            hit = results[0];

            results.Dispose();
            commands.Dispose();

            return hit.collider != null;
        }
    }
}
