﻿namespace Hotfix.Projectiles.Raycasting
{
    public interface IProjectileRaycaster
    {
        ProjectileRaycastSettings Settings { get; }

        ProjectileOperation Raycast(ProjectileRequest request);
    }
}
