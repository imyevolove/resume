﻿using System;
using UnityEngine;

namespace Hotfix.Projectiles.Raycasting
{
    [Serializable]
    public struct ProjectileRaycastSettings
    {
        [SerializeField] public LayerMask mask;
        [SerializeField] public float maxDistance;
        [SerializeField] public int iterations;
    }
}
