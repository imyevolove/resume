﻿using Hotfix.Systems;
using System;
using UnityEngine;

namespace Hotfix.Physics.Destructions
{
    [Serializable]
    [DisallowMultipleComponent]
    public sealed class DestructibleObject : DestructibleBehaviour
    {
        #pragma warning disable 0649
        [SerializeField] private GameObject _alive;
        [SerializeField] private GameObject _destroyed;
        [SerializeField] private int _health = 1;
        #pragma warning restore 0649

        #region UNITY METHODS
        protected override void Awake()
        {
            base.Awake();
            ApplyActivation();
        }
        #endregion

        #region OVERRIDES
        protected override bool DestructByDamage(ref DamageEventData data)
        {
            _health -= data.damage;
            return _health <= 0;
        }

        protected override void InternalDestruct()
        {
            ApplyActivation();
            PrepareDestroyedObject();
        }
        #endregion

        private void ApplyActivation()
        {
            _alive.SetActive(!Destructed);
            _destroyed.SetActive(Destructed);
        }

        private void PrepareDestroyedObject()
        {
            _destroyed.transform.SetPositionAndRotation(_alive.transform.position, _alive.transform.rotation);
        }
    }
}
