﻿using Hotfix.Systems;
using System;
using UnityEngine;

namespace Hotfix.Physics.Destructions
{
    [Serializable]
    [DisallowMultipleComponent]
    public sealed class ExplosionDestructionAffector : DestructionAffectorBehaviour
    {
        [SerializeField] public Rigidbody[] parts;
        [SerializeField] public Transform center;
        [SerializeField] public float power = 1;

        public override void Affect(ref DamageEventData data)
        {
            for (var i = 0; i < parts.Length; i++)
            {
                var part = parts[i];
                var partCenter = part.GetComponent<Renderer>().bounds.center;
                part?.AddForceAtPosition((center.position - partCenter).normalized * data.impulse * power, center.position, ForceMode.Impulse);
                ///Debug.DrawLine(center.position, center.position + (center.position - partCenter).normalized * 1f, Color.green, 2f);
            }
        }

        private void OnDrawGizmos()
        {
            if (center == null) return;

            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(center.position, 0.1f);
        }
    }
}
