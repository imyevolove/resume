﻿using Hotfix.Systems;
using System;
using UnityEngine;

namespace Hotfix.Physics.Destructions
{
    [Serializable]
    [DisallowMultipleComponent]
    public sealed class PhysicsDestructionAffector : DestructionAffectorBehaviour
    {
        [SerializeField] public Rigidbody[] parts;
        [SerializeField] public float power = 1;

        public override void Affect(ref DamageEventData data)
        {
            for (var i = 0; i < parts.Length; i++)
                parts[i]?.AddForceAtPosition(data.direction * data.impulse * power, data.point, ForceMode.Impulse);
        }
    }
}
