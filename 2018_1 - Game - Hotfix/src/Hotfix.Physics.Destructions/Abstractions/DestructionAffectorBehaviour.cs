﻿using Hotfix.Systems;
using System;
using UnityEngine;

namespace Hotfix.Physics.Destructions
{
    [Serializable]
    public abstract class DestructionAffectorBehaviour : MonoBehaviour, IDestructionAffector
    {
        public abstract void Affect(ref DamageEventData data);
    }
}
