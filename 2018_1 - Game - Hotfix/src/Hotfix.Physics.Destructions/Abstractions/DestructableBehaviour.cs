﻿using Hotfix.Systems;
using System;

namespace Hotfix.Physics.Destructions
{
    [Serializable]
    public abstract class DestructibleBehaviour : DamageableBehaviour, IDistructible
    {
        [NonSerialized] private IDestructionAffector[] _affectors;

        public bool Destructed { get; private set; } = false;

        protected virtual void Awake()
        {
            _affectors = GetComponents<IDestructionAffector>();
        }

        public void Destruct()
        {
            if (Destructed) return;

            Destructed = true;

            InternalDestruct();
        }

        public sealed override void InflictDamage(DamageEventData data)
        {
            if (Destructed) return;

            if (DestructByDamage(ref data))
            {
                Destructed = true;

                InternalDestruct();
                _affectors?.AffectAll(ref data);
            }
        }

        protected abstract bool DestructByDamage(ref DamageEventData data);

        protected abstract void InternalDestruct();
    }
}
