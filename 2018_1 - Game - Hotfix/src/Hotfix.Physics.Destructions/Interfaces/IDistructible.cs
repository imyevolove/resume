﻿namespace Hotfix.Physics.Destructions
{
    public interface IDistructible
    {
        void Destruct();
    }
}
