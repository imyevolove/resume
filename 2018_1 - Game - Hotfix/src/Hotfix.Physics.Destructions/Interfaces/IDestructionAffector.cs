﻿using Hotfix.Systems;

namespace Hotfix.Physics.Destructions
{
    public interface IDestructionAffector
    {
        void Affect(ref DamageEventData data);
    }
}
