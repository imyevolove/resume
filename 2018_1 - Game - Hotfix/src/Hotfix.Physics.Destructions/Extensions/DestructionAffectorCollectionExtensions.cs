﻿using Hotfix.Systems;
using System.Collections.Generic;

namespace Hotfix.Physics.Destructions
{
    public static class DestructionAffectorCollectionExtensions
    {
        public static void AffectAll(this IEnumerable<IDestructionAffector> affectors, ref DamageEventData data)
        {
            if (affectors == null) return;

            foreach (var affector in affectors)
                affector?.Affect(ref data);
        }
    }
}
