﻿using Hotfix.Systems;
using System;
using System.Linq;
using UnityEngine;

namespace Hotfix.Physics.Destructions
{
    [Serializable]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public sealed class DestructiblePhysicsDamage : MonoBehaviour
    {
        public const float ImpulseScale = 0.01f;

        [SerializeField] public DestructibleBehaviour destructible;
        [SerializeField] public LayerMask ignoreLayer;
        [SerializeField] public float minImpulse = 10f;
        [SerializeField] public float maxImpulse = 100f;
        [SerializeField] public float damageMultiplier = 1f;

        public Rigidbody Rigidbody { get; private set; }
        public Collider Collider { get; private set; }
        
        private void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
            Collider = GetComponent<Collider>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (destructible == null) return;

            var layerbyte = 1 << collision.gameObject.layer;
            if ((layerbyte & ignoreLayer.value) == layerbyte) return;

            var impulse = collision.impulse.magnitude;
            if (impulse < minImpulse) return;

            var contact = collision.contacts.FirstOrDefault();
            var damage = (int)(impulse * damageMultiplier);
            destructible.InflictDamage(new DamageEventData(damage, contact.point, contact.normal, Mathf.Min(impulse, maxImpulse) * ImpulseScale));
        }
    }
}
