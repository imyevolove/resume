﻿using Hotfix.Projectiles.Raycasting;
using UnityEngine;

namespace Hotfix.Projectiles
{
    [CreateAssetMenu(menuName = "Application/Projectiles/RaycastProjectileSystem", fileName = "ProjectileRaycastSystem")]
    public class RaycastProjectileSystem : BaseProjectileSystem
    {
        #pragma warning disable CS0649
        [SerializeField] private ProjectileDefferedRaycaster _raycaster;
        #pragma warning restore CS0649
        
        protected override ProjectileOperation FireHandle(ProjectileRequest request)
        {
            return _raycaster.Raycast(request);
        }
    }
}
