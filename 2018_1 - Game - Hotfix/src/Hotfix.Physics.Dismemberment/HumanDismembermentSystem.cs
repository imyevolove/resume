﻿using Hotfix.Graphics;
using Hotfix.Pooling;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    [DisallowMultipleComponent]
    public class HumanDismembermentSystem : DismembermentSystem<HumanDismembermentChain>
    {
        [SerializeField] public Renderer humanRenderer;
        [SerializeField] public TextureSize maskSize;

        private Texture2D _mask;
        private TextureCombiner _textureCombiner;

        protected override void Awake()
        {
            base.Awake();
            _textureCombiner = new TextureCombiner();
            _mask = GraphicsUtils.CreateTextureForTextureCombiner(maskSize);
        }

        protected override void OnAnyChainChanged(DismembermentChain chain)
        {
            base.OnAnyChainChanged(chain);

            var dmbrt = chain as HumanDismembermentChain;
            if (dmbrt != null && dmbrt.flesh != null)
            {
                var flesh = PoolManager.Active.GetObject(dmbrt.flesh);

                if (chain.ActiveSelf) flesh?.Deactivate();
                else flesh?.Activate(dmbrt.transform, humanRenderer.material);
            }

            chain.gameObject.SetActive(chain.ActiveSelf);

            RepaintAll();
        }

        private void RepaintAll()
        {
            var masks = CollectMasks(Root);
            _textureCombiner.Combine(ref _mask, masks);
            ApplyMask();
        }

        private void ApplyMask() => ApplyMaskFor(humanRenderer, _mask);

        /*
        private void ApplyMask()
        {
            foreach (var renderer in renderers)
            {
                if (renderer == null) continue;
                ApplyMaskFor(renderer, _mask);
            }
        }
        */

        private void ApplyMaskFor(Renderer renderer, Texture2D texture)
        {
            renderer.material.SetTexture("_Stencil", texture);
        }

        private List<Texture2D> CollectMasks(DismembermentChain chain) => CollectMasks(chain, new List<Texture2D>(), false);
        private List<Texture2D> CollectMasks(DismembermentChain chain, List<Texture2D> collection, bool ignoreActiveStatus)
        {
            if (!chain.ActiveSelf || ignoreActiveStatus)
            {
                var maskable = chain as IMaskableDismemberment;
                if (maskable != null && maskable.Mask != null) collection.Add(maskable.Mask);

                ignoreActiveStatus = true;
            }

            foreach (var child in chain.Childrens)
            {
                CollectMasks(child, collection, ignoreActiveStatus);
            }

            return collection;
        }
    }
}
