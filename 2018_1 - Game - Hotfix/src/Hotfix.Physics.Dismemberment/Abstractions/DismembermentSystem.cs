﻿using System;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    public abstract class DismembermentSystem<T> : DismembermentSystem
        where T : DismembermentChain
    {
        [SerializeField] [LockInPlayMode] private T _root;
        public T Root => _root;

        #region Root chain getters/setters
        public override DismembermentChain  GetRoot() => _root;
        public          T                   GetRootT() => _root;

        public override void SetRoot(DismembermentChain chain) => SetRoot(chain as T);
        public  void SetRoot(T chain)
        {
            if (_root == chain) return;

            var previous = _root;

            _root = chain;

            OnRootChanged(previous, _root);
        }
        #endregion
        
        #region UNITY API
        protected virtual void Awake()
        {
            OnRootChanged(null, _root);
        }
        protected virtual void OnDestroy()
        {
            SetRoot(null);
        }
        #endregion

        #region Protected event methods
        protected virtual void OnRootChanged(T previous, T current)
        {
            UnregisterEventListeners(previous);
            RegisterEventListeners(current);
        }
        protected virtual void OnAnyChainChanged(DismembermentChain chain)
        {
        }
        #endregion

        #region Internal root chain event methods
        private void OnRootActiveChanged(DismembermentChainEventData eventData)
        {
            OnAnyChainChanged(eventData.chain);
        }
        private void OnRootAnyDeepChildActiveChanged(DismembermentChainChildEventData eventData)
        {
            OnAnyChainChanged(eventData.chain);
        }
        #endregion

        #region Event listening manager
        private void RegisterEventListeners(T target)
        {
            if (target == null) return;
            target.ActiveChanged += OnRootActiveChanged;
            target.DeepChildActiveChanged += OnRootAnyDeepChildActiveChanged;
        }
        private void UnregisterEventListeners(T target)
        {
            if (target == null) return;
            target.ActiveChanged -= OnRootActiveChanged;
            target.DeepChildActiveChanged -= OnRootAnyDeepChildActiveChanged;
        }
        #endregion
    }

    [Serializable]
    public abstract class DismembermentSystem : MonoBehaviour
    {
        public abstract DismembermentChain GetRoot();
        public abstract void SetRoot(DismembermentChain chain);

        public void SetChainActive(DismembermentChain chain, bool active)
        {
            if (chain == null) throw new ArgumentNullException(nameof(chain));

            var root = GetRoot();
            if (root == null) return;

            if (!root.ContainsChainRecursive(chain))
            {
                Debug.LogWarning($"Root chain {root} do not contains chain {chain}");
                return;
            }

            chain.ActiveSelf = active;
        }
    }
}
