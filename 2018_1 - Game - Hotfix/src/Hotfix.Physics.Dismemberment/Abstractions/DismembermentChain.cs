﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    [ExecuteInEditMode]
    public abstract class DismembermentChain : MonoBehaviour
    {
        [SerializeField] private DismembermentChain _parent;

        public event DismembermentChainActiveChanged ActiveChanged;
        public event DismembermentChainChildActiveChanged ChildActiveChanged;
        public event DismembermentChainChildActiveChanged DeepChildActiveChanged;

        public IEnumerable<DismembermentChain> Childrens => _childrens;
        public int ChildCount => _childrens.Count;
        
        public DismembermentChain Parent => _parent;
        public bool ActiveSelf
        {
            get => _activeSelf;
            set
            {
                if (_activeSelf == value) return;

                _activeSelf = value;
                
                DispatchActiveChangedEvents(value);
                SendActiveChangedEventsForParent(value);
                SendDeepChildActiveChangedEventsForParent(value);
            }
        }

        [NonSerialized] private List<DismembermentChain> _childrens = new List<DismembermentChain>();
        [NonSerialized] private bool _activeSelf = true;
        
        #region UNITY API
        protected virtual void Awake()
        {
            Refresh();
        }
        protected virtual void OnValidate()
        {
            Refresh();
        }
        protected virtual void OnDestroy()
        {
            DeattachFromParent();
        }
        #endregion
        #region Methods for working with childrens
        /// <summary>
        /// Вернет true, если указанный объект является дочерним.
        /// </summary>
        /// <param name="chain"></param>
        /// <returns></returns>
        public bool HasChild(DismembermentChain chain) => _childrens.Contains(chain);
        /// <summary>
        /// Вернет дочерний объект по индексу.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public DismembermentChain GetChild(int index) => _childrens[index];
        #endregion
        /// <summary>
        /// Устанавливает цепочку в качестве цепочки-родителя.
        /// </summary>
        /// <param name="parent"></param>
        public void SetParent(DismembermentChain parent)
        {
            if (parent == this) throw new DismembermentChainException("Can't set self as parent.");

            /// Отсоединение от родителя.
            if (parent == null)
            {
                DeattachFromParent();
                return;
            }

            /// Уже присоединен к этому родителю.
            if (parent == _parent) return;

            if (!ValidatePotentialParentByChildrenRequirements(parent))
            {
                Debug.LogWarning("Can't attach child as parent.");
                return;
            }

            DeattachFromParent();
            
            _parent = parent;

            /// Произошла ошибка присоединения к родителю.
            if (!parent.InternalTryAddChild(this)) _parent = null;
        }
        #region Protected event methods
        protected virtual void OnActiveChanged(bool status)
        {
        }
        protected virtual void OnChildActiveChanged(DismembermentChain child, bool status)
        {
        }
        protected virtual void OnDeepChildActiveChanged(DismembermentChainChildEventData eventData)
        {
        }
        #endregion
        #region Internal methods
        /// <summary>
        /// Добавляет цепочку в список дочерних.
        /// </summary>
        /// <param name="chain"></param>
        /// <returns></returns>
        private bool InternalTryAddChild(DismembermentChain chain)
        {
            try
            {
                if (_childrens.Contains(chain)) return true;

                _childrens.Add(chain);

                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return false;
            }
        }
        /// <summary>
        /// Удаляет цепочку из списка дочерних.
        /// </summary>
        /// <param name="chain"></param>
        /// <returns></returns>
        private bool InternalTryRemoveChild(DismembermentChain chain) => _childrens.Remove(chain);
        #endregion
        #region Event senders
        private void SendActiveChangedEventsForParent(bool status) => _parent?.DispatchChildActiveChangedEvents(this, status);
        private void SendDeepChildActiveChangedEventsForParent(bool status) => SendDispatchDeepChildActiveChangedEventsForParent(new DismembermentChainChildEventData(_parent, this));
        private void SendDispatchDeepChildActiveChangedEventsForParent(DismembermentChainChildEventData eventData) => _parent?.DispatchDeepChildActiveChangedEvents(eventData);
        #endregion
        #region Event dispatchers
        private void DispatchActiveChangedEvents(bool status)
        {
            OnActiveChanged(status);
            ActiveChanged?.Invoke(new DismembermentChainEventData(this));
        }
        private void DispatchChildActiveChangedEvents(DismembermentChain child, bool status)
        {
            OnChildActiveChanged(child, status);
            ChildActiveChanged?.Invoke(new DismembermentChainChildEventData(this, child));
        }
        private void DispatchDeepChildActiveChangedEvents(DismembermentChainChildEventData eventData)
        {
            OnDeepChildActiveChanged(eventData);
            DeepChildActiveChanged?.Invoke(eventData);
            SendDispatchDeepChildActiveChangedEventsForParent(eventData);
        }
        #endregion
        #region Refresh methods
        /// <summary>
        /// Обновляет состояние.
        /// </summary>
        private void Refresh()
        {
            RefreshParent();
            RefreshChildrens();
        }
        /// <summary>
        /// Обновляет взаимосвязи c дочерними объектами.
        /// </summary>
        private void RefreshChildrens() => _childrens.RemoveAll(child => child == null || child._parent != this);
        /// <summary>
        /// Обновляет взаимосвязь с цепочкой-родителем.
        /// </summary>
        private void RefreshParent()
        {
            if (_parent == null) return;

            if (_parent == this || !ValidatePotentialParentByChildrenRequirements(_parent))
            {
                _parent.InternalTryRemoveChild(this);
                _parent = null;

                return;
            }

            _parent.InternalTryAddChild(this);
        }
        #endregion
        #region Other
        private void DeattachFromParent()
        {
            if (_parent == null) return;

            if (_parent.InternalTryRemoveChild(this))
            {
                _parent = null;
                return;
            }

            var cParent = _parent;

            _parent = null;

            cParent.Refresh();
        }
        private bool ValidatePotentialParentByChildrenRequirements(DismembermentChain chain) => !_childrens.Contains(chain);
        #endregion
    }
}
