﻿namespace Hotfix.Physics
{
    public delegate void DismembermentChainActiveChanged(DismembermentChainEventData eventData);
    public delegate void DismembermentChainChildActiveChanged(DismembermentChainChildEventData eventData);
}
