﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotfix.Physics
{
    public static class DismembermentChainExtensions
    {
        public static bool ContainsChainRecursive(this DismembermentChain root, DismembermentChain chain)
        {
            if (root == null) throw new ArgumentNullException(nameof(root));
            if (chain == null) throw new ArgumentNullException(nameof(chain));

            if (root.Childrens.Contains(chain)) return true;

            foreach (var child in root.Childrens)
            {
                if (child.ContainsChainRecursive(chain))
                {
                    return true;
                }
            }

            return false;
        }
        public static DismembermentChain FindChain(this DismembermentChain root, DismembermentChain chain, Func<DismembermentChain, bool> predicate, bool recursiveScan = false)
        {
            if (root == null) throw new ArgumentNullException(nameof(root));
            if (chain == null) throw new ArgumentNullException(nameof(chain));

            var result = root.Childrens.FirstOrDefault(predicate);
            if (result != null) return result;
            if (!recursiveScan) return null;
            
            foreach (var child in root.Childrens)
            {
                var scanResult = child.FindChain(chain, predicate);
                if (scanResult != null) return scanResult;
            }

            return null;
        }
        public static DismembermentChain[] GetAllHierarchyChildrens(this DismembermentChain root)
        {
            var collection = new List<DismembermentChain>();

            GetChildrensRecursive(root, collection);

            return collection.ToArray();
        }

        private static void GetChildrensRecursive(DismembermentChain root, List<DismembermentChain> collection)
        {
            if (root == null) return;

            foreach (var child in root.Childrens)
            {
                if (child == null) return;

                collection.Add(child);

                GetChildrensRecursive(child, collection);
            }
        }
    }
}
