﻿using UnityEngine;

namespace Hotfix.Physics
{
    public static class DismemebrmentFleshExtensions
    {
        public static void Activate(this DismembrmentFlesh flesh, Transform place)
        {
            flesh.CopyTransform(place);
            flesh.Activate();
        }
        public static void AddImpulse(this DismembrmentFlesh flesh, Vector3 force) => flesh.Rigidbody.AddForce(force, ForceMode.Impulse);
        public static void AddImpulse(this DismembrmentFlesh flesh, Vector3 direction, float force) => AddImpulse(flesh, direction * force);
    }
}
