﻿using System;
using System.Runtime.Serialization;

namespace Hotfix.Physics
{
    public class DismembermentChainException : Exception
    {
        public DismembermentChainException()
        {
        }

        public DismembermentChainException(string message) : base(message)
        {
        }

        public DismembermentChainException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DismembermentChainException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
