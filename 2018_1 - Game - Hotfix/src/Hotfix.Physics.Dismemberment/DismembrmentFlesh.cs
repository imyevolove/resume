﻿using System;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Renderer))]
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public class DismembrmentFlesh : MonoBehaviour
    {
        public Renderer     Renderer    => GetComponent(ref _renderer);
        public Collider     Collider    => GetComponent(ref _collider);
        public Rigidbody    Rigidbody   => GetComponent(ref _rigidbody);

        private Renderer    _renderer;
        private Collider    _collider;
        private Rigidbody   _rigidbody;

        /// <summary>
        /// Активирует объект.
        /// </summary>
        public virtual void Activate()
        {
            Refresh();
            SetGameObjectActive(true);
        }
        /// <summary>
        /// Деактивирует объект.
        /// </summary>
        public virtual void Deactivate() => SetGameObjectActive(false);
        /// <summary>
        /// Устанавливает активность игрового объекта.
        /// </summary>
        /// <param name="active"></param>
        public void SetGameObjectActive(bool active) => gameObject.SetActive(active);
        /// <summary>
        /// Устанавливает положение объекта в пространстве из указанного объекта <see cref="Transform"/>
        /// </summary>
        /// <param name="reference"></param>
        public void CopyTransform(Transform reference) => transform.SetPositionAndRotation(reference.position, reference.rotation);
        /// <summary>
        /// Обновляет объект до состояния переиспользования.
        /// </summary>
        public virtual void Refresh() => Rigidbody.velocity = Vector3.zero;
        /// <summary>
        /// Вернет компонент по референсу. Если компонент не определен, то будет совершена попытка получить его из <see cref="GameObject"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        private T GetComponent<T>(ref T field)
        {
            if (field == null) field = GetComponent<T>();
            return field;
        }
    }
}
