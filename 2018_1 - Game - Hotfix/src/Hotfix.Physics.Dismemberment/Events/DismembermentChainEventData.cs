﻿namespace Hotfix.Physics
{
    public struct DismembermentChainEventData
    {
        public DismembermentChain chain;

        public DismembermentChainEventData(DismembermentChain chain)
        {
            this.chain = chain;
        }
    }
}
