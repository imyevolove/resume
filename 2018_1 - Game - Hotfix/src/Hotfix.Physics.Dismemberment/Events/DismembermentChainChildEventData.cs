﻿namespace Hotfix.Physics
{
    public struct DismembermentChainChildEventData
    {
        public DismembermentChain parent;
        public DismembermentChain chain;

        public DismembermentChainChildEventData(DismembermentChain parent, DismembermentChain child)
        {
            this.parent = parent;
            this.chain = child;
        }
    }
}
