﻿using UnityEngine;

namespace Hotfix.Physics
{
    public interface IMaskableDismemberment
    {
        Texture2D Mask { get; }
    }
}
