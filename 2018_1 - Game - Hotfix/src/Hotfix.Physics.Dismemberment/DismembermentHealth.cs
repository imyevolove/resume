﻿using Hotfix.Systems;
using System;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    [RequireComponent(typeof(DismembermentChain))]
    public class DismembermentHealth : HealthComponent
    {
        [SerializeField] public bool restoreOnActive;
        public DismembermentChain Dismemberment { get; private set; }

        private void Awake()
        {
            Dismemberment = GetComponent<DismembermentChain>();
            RegisterEventListeners(Dismemberment);
        }

        private void OnDestroy()
        {
            UnregisterEventListeners(Dismemberment);
            Dismemberment = null;
        }

        protected override void OnHealthChanged(int health)
        {
            if (Dismemberment == null) return;
            Dismemberment.ActiveSelf = !this.IsEmpty();
        }

        private void OnDismembermentActiveChanged(DismembermentChainEventData eventData)
        {
            if (eventData.chain.ActiveSelf)
            {
                this.Restore();
            }
        }

        private void RegisterEventListeners(DismembermentChain target)
        {
            if (target == null) return;
            target.ActiveChanged += OnDismembermentActiveChanged;
        }
        private void UnregisterEventListeners(DismembermentChain target)
        {
            if (target == null) return;
            target.ActiveChanged -= OnDismembermentActiveChanged;
        }
    }
}
