﻿using System;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    public class HumanDismembermentChain : DismembermentChain, IMaskableDismemberment
    {
        [SerializeField] public Texture2D mask;
        [SerializeField] public HumanDismembermentFlesh flesh;

        public Texture2D Mask => mask;
    }
}
