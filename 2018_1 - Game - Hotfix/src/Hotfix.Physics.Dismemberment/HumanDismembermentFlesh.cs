﻿using System;
using UnityEngine;

namespace Hotfix.Physics
{
    [Serializable]
    public sealed class HumanDismembermentFlesh : DismembrmentFlesh
    {
        public void CopySkinProperties(Material source)
        {
            MaterialPropertyBlock properties = new MaterialPropertyBlock();
            properties.SetColor("_Color", source.GetColor("_Color"));
            properties.SetColor("_SkinColor", source.GetColor("_SkinColor"));
            Renderer.SetPropertyBlock(properties);

            Renderer.material.mainTexture = source.mainTexture;
            Renderer.material.mainTextureOffset = source.mainTextureOffset;
            Renderer.material.mainTextureScale = source.mainTextureScale;
        }
        public void Activate(Transform transform, Material material)
        {
            DismemebrmentFleshExtensions.Activate(this, transform);
            CopySkinProperties(material);
        }
    }
}
