﻿namespace Hotfix.Physics.Materials
{
    public interface IPhysicalMaterialProvider
    {
        PhysicalMaterial Material { get; }
    }
}
