﻿namespace Hotfix.Physics.Materials
{
    public interface IPhysicalMaterial
    {
        PhysicalMaterialFlags Flags { get; set; }
        float DensityDamageSuppression { get; set; }
        float PenetrationAngleDistortion { get; set; }
        float RicochetAngle { get; set; }
        float RicochetChance { get; set; }
        float RicochetAngleDistortion { get; set; }
        float RicochetDamageSuppressionFactor { get; set; }

        bool IsValid { get; }
    }
}
