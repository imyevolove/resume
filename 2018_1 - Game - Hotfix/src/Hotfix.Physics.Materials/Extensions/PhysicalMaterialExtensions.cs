﻿using UnityEngine;
using URandom = UnityEngine.Random;

namespace Hotfix.Physics.Materials
{
    public static class PhysicalMaterialExtensions
    {
        public static void ApplyPenetrationDistortion(this IPhysicalMaterial material, ref Vector3 direction, float power = 1f)
        {
            direction.y += URandom.Range(-power, power) * material.PenetrationAngleDistortion * Mathf.Deg2Rad;
            direction.z += URandom.Range(-power, power) * material.PenetrationAngleDistortion * Mathf.Deg2Rad;
            direction.Normalize();
        }

        public static Vector3 CalculatePenetrationDistortion(this IPhysicalMaterial material, Vector3 sourceDirection, float power = 1f)
        {
            sourceDirection.y += URandom.Range(-power, power) * material.PenetrationAngleDistortion * Mathf.Deg2Rad;
            sourceDirection.z += URandom.Range(-power, power) * material.PenetrationAngleDistortion * Mathf.Deg2Rad;
            return sourceDirection.normalized;
        }

        public static float GetRicochetDamageSuppressionValue(this IPhysicalMaterial material) => material.DensityDamageSuppression * material.RicochetDamageSuppressionFactor;
    }
}
