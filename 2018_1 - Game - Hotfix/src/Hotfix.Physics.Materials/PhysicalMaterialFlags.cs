﻿using System;

namespace Hotfix.Physics.Materials
{
    /// <summary>
    /// Флаги активации свойств физического материала.
    /// </summary>
    [Flags]
    public enum PhysicalMaterialFlags
    {
        /// <summary>
        /// Может отрекошетить
        /// </summary>
        AllowRicochet = 1 << 0,
        /// <summary>
        /// Может быть пробита
        /// </summary>
        AllowPenetration = 1 << 1,
        /// <summary>
        /// Искажение угла по пробитию
        /// </summary>
        AllowPenetrationDistortion = 1 << 2
    }
}
