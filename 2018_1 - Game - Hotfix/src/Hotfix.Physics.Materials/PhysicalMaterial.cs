﻿using System;
using UnityEngine;

namespace Hotfix.Physics.Materials
{
    [Serializable]
    [DisallowMultipleComponent]
    public sealed class PhysicalMaterial : MonoBehaviour, IPhysicalMaterial, IPhysicalMaterialProvider
    {
        #pragma warning disable CS0649
        [SerializeField] private PhysicalMaterialDescriptor _descriptor;
#pragma warning restore CS0649

        public PhysicalMaterialDescriptor Descriptor => _descriptor;
        public PhysicalMaterialFlags Flags              { get => _descriptor.Material.Flags;                             set => LogPropertySetAccessDenied(); }
        public float DensityDamageSuppression           { get => _descriptor.Material.DensityDamageSuppression;          set => LogPropertySetAccessDenied(); }
        public float PenetrationAngleDistortion         { get => _descriptor.Material.PenetrationAngleDistortion;        set => LogPropertySetAccessDenied(); }
        public float RicochetAngle                      { get => _descriptor.Material.RicochetAngle;                     set => LogPropertySetAccessDenied(); }
        public float RicochetAngleDistortion            { get => _descriptor.Material.RicochetAngleDistortion;           set => LogPropertySetAccessDenied(); }
        public float RicochetDamageSuppressionFactor    { get => _descriptor.Material.RicochetDamageSuppressionFactor;   set => LogPropertySetAccessDenied(); }
        public float RicochetChance                     { get => _descriptor.Material.RicochetChance;                    set => LogPropertySetAccessDenied(); }

        public bool IsValid => _descriptor != null && _descriptor.Material != null;
        public PhysicalMaterial Material => this;

        private void Awake()
        {
            if (!IsValid)
            {
                throw new NullReferenceException($"{typeof(PhysicalMaterial).FullName} EXCEPTION. Please, check objects on your scene and add materials manually. Object name: {name}");
            }
        }

        private static void LogPropertySetAccessDenied() => Debug.LogWarning($"Can't change material properties by {typeof(PhysicalMaterial).Name} Component");
    }
}
