﻿using System;
using UnityEngine;

namespace Hotfix.Physics.Materials
{
    [Serializable]
    [DisallowMultipleComponent]
    public class PhysicalMaterialProvider : MonoBehaviour, IPhysicalMaterialProvider
    {
        [SerializeField] public PhysicalMaterial material;
        public PhysicalMaterial Material => material;
    }
}
