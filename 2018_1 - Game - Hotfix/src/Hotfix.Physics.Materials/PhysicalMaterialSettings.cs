﻿using UnityEngine;

namespace Hotfix.Physics.Materials
{
    [CreateAssetMenu(menuName = "Application/Materials/Material", fileName = "PhysicalMaterialSettings")]
    public sealed class PhysicalMaterialSettings : ScriptableObject, IPhysicalMaterial
    {
        [SerializeField, Bitmask] private PhysicalMaterialFlags _flags;
        [SerializeField] private float _densityDamageSuppression;
        [SerializeField] private float _penetrationAngleDistortion = 10f;
        [SerializeField] private float _ricochetAngle = 10f;
        [SerializeField] private float _ricochetAngleDistortion = 5f;
        [SerializeField] private float _ricochetDamageSuppressionFactor = .5f;
        [SerializeField] private float _ricochetChance = 1f;

        public bool IsValid => true;
        /// <summary>
        /// Флаги свойств.
        /// </summary>
        public PhysicalMaterialFlags Flags { get => _flags; set => _flags = value; }
        /// <summary>
        /// Гашение урона по плотности на 1 метр.
        /// </summary>
        public float DensityDamageSuppression { get => _densityDamageSuppression; set => _densityDamageSuppression = value; }
        /// <summary>
        /// Максимальный угол искажения от пробиваемости.
        /// </summary>
        public float PenetrationAngleDistortion { get => _penetrationAngleDistortion; set => _penetrationAngleDistortion = value; }
        /// <summary>
        /// Угол вхождения для рикошета.
        /// </summary>
        public float RicochetAngle { get => _ricochetAngle; set => _ricochetAngle = value; }
        /// <summary>
        /// Угол искажения для рикошета.
        /// </summary>
        public float RicochetAngleDistortion { get => _ricochetAngleDistortion; set => _ricochetAngleDistortion = value; }
        /// <summary>
        /// Гашение урона от рекошета.
        /// </summary>
        public float RicochetDamageSuppressionFactor { get => _ricochetDamageSuppressionFactor; set => _ricochetDamageSuppressionFactor = value; }
        /// <summary>
        /// Шанс рикошета.
        /// </summary>
        public float RicochetChance { get => _ricochetChance; set => _ricochetChance = value; }
    }
}
