﻿using Hotfix.Effects;
using UnityEngine;

namespace Hotfix.Physics.Materials
{
    [CreateAssetMenu(menuName = "Application/Materials/Descriptor", fileName = "MaterialDescriptor")]
    public class PhysicalMaterialDescriptor : ScriptableObject
    {
        #pragma warning disable 0649
        [SerializeField] private PhysicalMaterialSettings _material;
        [SerializeField] private ScriptableEffect _projectileImpactEffect;
        #pragma warning restore 0649

        public PhysicalMaterialSettings Material => _material;
        public ScriptableEffect ProjectileImpactEffect => _projectileImpactEffect;
    }
}
