﻿namespace Hotfix.Physics.Materials
{
    public static class PhysicalMaterialFlagsUtils
    {
        /// <summary>
        /// Добавляет флаг в маску
        /// </summary>
        /// <param name="flag"></param>
        public static void AddFlag(ref PhysicalMaterialFlags bitmask, PhysicalMaterialFlags flag) => bitmask |= flag;
        /// <summary>
        /// Добавляет флаг в маску
        /// </summary>
        /// <param name="flag"></param>
        public static PhysicalMaterialFlags AddFlag(PhysicalMaterialFlags bitmask, PhysicalMaterialFlags flag) => bitmask | flag;
        /// <summary>
        /// Удаляет флаг из маски
        /// </summary>
        /// <param name="flag"></param>
        public static void RemoveFlag(ref PhysicalMaterialFlags bitmask, PhysicalMaterialFlags flag) => bitmask &= ~flag;
        /// <summary>
        /// Удаляет флаг из маски
        /// </summary>
        /// <param name="flag"></param>
        public static PhysicalMaterialFlags RemoveFlag(PhysicalMaterialFlags bitmask, PhysicalMaterialFlags flag) => bitmask & ~flag;
        /// <summary>
        /// Вернет true если в маске присутствует указанный флаг.
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public static bool HasFlag(PhysicalMaterialFlags bitmask, PhysicalMaterialFlags flag) => (bitmask & flag) == flag;
        /// <summary>
        /// Вернет true если в маске присутствует хотябы один флаг из указанной маски.
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public static bool HasAnyFlag(PhysicalMaterialFlags bitmask, PhysicalMaterialFlags falgs) => (bitmask & falgs) != 0;
    }
}
