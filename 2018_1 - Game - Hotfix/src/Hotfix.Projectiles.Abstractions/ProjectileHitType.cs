﻿namespace Hotfix.Projectiles
{
    public enum ProjectileHitType
    {
        Normal,
        Penetration
    }
}
