﻿using Hotfix.Weapons;
using UnityEngine;

namespace Hotfix.Projectiles
{
    /// <summary>
    /// Структура для создания запроса projectile на рейкаст.
    /// </summary>
    public struct ProjectileRequest
    {
        public Ray ray;
        public IWeaponDescriptor weapon;
        public float spread;

        public ProjectileRequest(Vector3 origin, Vector3 direction, float spread, IWeaponDescriptor weapon) 
            : this(new Ray(origin, direction), spread, weapon)
        {
        }
        public ProjectileRequest(Ray ray, float spread, IWeaponDescriptor weapon)
        {
            this.ray = ray;
            this.weapon = weapon;
            this.spread = spread;
        }
    }
}
