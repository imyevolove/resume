﻿using System;
using UnityEngine;

namespace Hotfix.Projectiles
{
    public sealed class ProjectileOperation : IDisposable
    {
        public event ProjectileHitEventDelegate OnHit = delegate { };

        private event Action<ProjectileOperation> _completed;
        public event Action<ProjectileOperation> Completed
        {
            add
            {
                if (IsCompleted)
                {
                    value?.Invoke(this);
                    return;
                }

                _completed += value;
            }
            remove
            {
                _completed -= value;
            }
        }

        public bool IsCompleted { get; private set; }

        internal void Complete()
        {
            if (IsCompleted)
            {
                Debug.LogWarning("Operation already completed");
                return;
            }

            IsCompleted = true;

            _completed?.Invoke(this);
        }
        internal void DispatchHitEvent(ProjectileHit hit)
        {
            if (IsCompleted)
            {
                Debug.LogWarning("Can't dispatch hit event because operation marked as completed");
                return;
            }

            OnHit.Invoke(hit);
        }

        public void Dispose()
        {
            _completed = null;
        }
    }

    public sealed class ProjectileOperationSource : IDisposable
    {
        public readonly ProjectileOperation Operation;

        public ProjectileOperationSource() : this(new ProjectileOperation())
        {
        }
        public ProjectileOperationSource(ProjectileOperation operation)
        {
            Operation = operation;
        }

        public void DispatchHitEvent(ProjectileHit hit) => Operation.DispatchHitEvent(hit);
        public void Complete() => Operation.Complete();
        public void Dispose () => Operation.Dispose();
    }
}
