﻿using System;
using UnityEngine;

namespace Hotfix.Projectiles
{
    [Serializable]
    public abstract class BaseProjectileSystem : ScriptableObject, IProjectileSystem
    {
        protected abstract ProjectileOperation FireHandle(ProjectileRequest request);

        public ProjectileOperation Fire(ProjectileRequest request)
        {
            var operation = FireHandle(request);
            operation.OnHit += (hit) => { OnProjectileOperationHitEvent(ref request, ref hit); };

            return operation;
        }

        private static void OnProjectileOperationHitEvent(ref ProjectileRequest request, ref ProjectileHit hit)
        {
            hit.HittableMediator?.Impact(hit);
        }
    }
}
