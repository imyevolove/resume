﻿using System;
using UnityEngine;

namespace Hotfix.Projectiles
{
    [Serializable]
    [RequireComponent(typeof(ImpactMediator))]
    public abstract class Hittable : MonoBehaviour, IHittable
    {
        protected ImpactMediator Mediator { get; private set; }

        public void Hit(ProjectileHit hit)
        {
            if (!isActiveAndEnabled) return;
            InternalHit(ref hit);
        }

        protected abstract void InternalHit(ref ProjectileHit hit);

        protected virtual void Awake() => Mediator = GetComponent<ImpactMediator>();
        protected virtual void OnEnable() => Mediator?.Register(this);
        protected virtual void OnDestroy() => Mediator?.Unregister(this);
    }
}
