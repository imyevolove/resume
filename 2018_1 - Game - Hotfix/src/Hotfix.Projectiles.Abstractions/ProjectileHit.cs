﻿using Hotfix.Weapons;
using UnityEngine;

namespace Hotfix.Projectiles
{
    public struct ProjectileHit
    {
        public static float MaxImpulse { get; set; } = 200f;
        public static float ImpulseScale { get; set; } = 0.01f;

        public readonly ContactPoint Contact;
        public readonly Vector3 Direction;
        public readonly IWeaponDescriptor Weapon;
        public int Damage { get; private set; }
        public readonly int RawDamage;
        public readonly ImpactMediator HittableMediator;
        public readonly ProjectileHitType Type;
        public readonly float Impulse;

        public ProjectileHit(ContactPoint contact, Vector3 direction, IWeaponDescriptor weapon, int rawDamage, ProjectileHitType type)
        {
            Type = type;
            Direction = direction;
            Weapon  = weapon;
            Contact = contact;
            HittableMediator = contact.collider?.GetComponent<ImpactMediator>();
            RawDamage = rawDamage < 0 ? 0 : rawDamage;
            Damage  = Mathf.Max(RawDamage, weapon.ProjectileData.DamageMinimum);
            Impulse = Mathf.Min(RawDamage, MaxImpulse) * ImpulseScale;
        }

        public void ModifyDamage(float multiplier) => Damage = Mathf.CeilToInt(Damage * multiplier);
    }
}
