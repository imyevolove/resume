﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Hotfix.Projectiles
{
    /// <summary>
    /// Выступает в роли посредника между событием попадания и слушателями этих событий.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class ImpactMediator : MonoBehaviour
    {
        [NonSerialized] private List<Hittable> _hittables = new List<Hittable>();

        internal void Register(Hittable hittable) => _hittables.Add(hittable);
        internal void Unregister(Hittable hittable) => _hittables.Remove(hittable);

        #region UNITY METHODS
        private void Start()
        {
            RemoveComponentIfHasNoHittables();
        }
        #endregion

        /// <summary>
        /// Вызывает событие попадания для всех слушателей.
        /// </summary>
        /// <param name="hit"></param>
        public void Impact(ProjectileHit hit)
        {
            foreach (var hittable in _hittables)
                hittable?.Hit(hit);
        }
       /// <summary>
       /// Удаление компонента, если нет слушателей.
       /// </summary>
        private void RemoveComponentIfHasNoHittables()
        {
            if (_hittables.Count != 0) return;
            DestroyImmediate(this, false);
        }
    }
}
