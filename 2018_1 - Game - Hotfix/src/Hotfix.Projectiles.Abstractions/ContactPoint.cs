﻿using UnityEngine;

namespace Hotfix.Projectiles
{
    public struct ContactPoint
    {
        public Vector3 normal;
        public Vector3 point;
        public Collider collider;

        public ContactPoint(RaycastHit raycastHit)
        {
            normal = raycastHit.normal;
            point = raycastHit.point;
            collider = raycastHit.collider;
        }
    }
}
