﻿using Hotfix.Physics.Materials;
using Hotfix.Weapons;
using UnityEngine;

namespace Hotfix.Projectiles
{
    public static class ProjectileUtils
    {
        public static void ApplyDistancedDamageSuppression(float suppressionValue, float distance, ref float damage) => damage -= distance * suppressionValue;
        public static void ApplyRicocheteDamageSuppression(IPhysicalMaterial material, ref float damage) => damage -= material.DensityDamageSuppression * material.RicochetDamageSuppressionFactor;
        public static void ApplyPenetrationDamageSuppression(IPhysicalMaterial material, float distance, ref float damage) => ApplyDistancedDamageSuppression(material.DensityDamageSuppression, distance, ref damage);
        public static void ApplySpaceDamageSuppression(IProjectileDescriptor projectile, float totalDistance, float distance, ref float damage)
        {
            if (totalDistance <= projectile.Range) return;
            ApplyDistancedDamageSuppression(projectile.DamageDrop, distance, ref damage);
        }

        public static bool CanRicochetFromAngle(Vector3 normal, Vector3 direction, IPhysicalMaterial material) => CanRicochetFromAngle(normal, direction, material.RicochetAngle);
        public static bool CanRicochetFromAngle(Vector3 normal, Vector3 direction, float angleThreshold) => (90f - (180f - Vector3.Angle(normal, direction))) < angleThreshold;
        public static bool CanRicochetByChance(float weight01) => ARandom.RandomState(weight01);
        public static bool CanRicochetByChance(IPhysicalMaterial material) => ARandom.RandomState(material.RicochetChance);
        public static bool CanRicochet(Vector3 normal, Vector3 direction, IPhysicalMaterial material) => CanRicochetByChance(material) && CanRicochetFromAngle(normal, direction, material);
    }
}
