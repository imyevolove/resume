﻿namespace Hotfix.Projectiles
{
    public interface IProjectileSystem
    {
        ProjectileOperation Fire(ProjectileRequest info);
    }
}
