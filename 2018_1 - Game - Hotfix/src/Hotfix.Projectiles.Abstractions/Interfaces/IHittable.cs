﻿namespace Hotfix.Projectiles
{
    internal interface IHittable
    {
        void Hit(ProjectileHit hit);
    }
}
