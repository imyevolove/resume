﻿namespace Hotfix.Projectiles
{
    public delegate void ProjectileHitEventDelegate(ProjectileHit hit);
}
