﻿using Hotfix.Coroutines;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hotfix.Projectiles.Raycasting
{
    [Serializable]
    public class ProjectileDefferedRaycaster : BaseProjectileRaycaster
    {
        [NonSerialized] private List<ProjectileRaycastWorker> _workers = new List<ProjectileRaycastWorker>();
        [NonSerialized] private List<ProjectileRaycastWorker> _delayedRemove = new List<ProjectileRaycastWorker>();
        [NonSerialized] private bool _requestsLaunched;

        public ProjectileDefferedRaycaster()
        {
        }
        public ProjectileDefferedRaycaster(ProjectileRaycastSettings settings)
        {
            Settings = settings;
        }

        public override ProjectileOperation Raycast(ProjectileRequest request)
        {
            var operation = new ProjectileOperationSource();

            CoroutineExecutionMaster.StartRoutine(DelayedWorkerRegistration(request, operation));
            
            return operation.Operation;
        }

        private void LaunchRequestsIfNecessary()
        {
            if (_requestsLaunched) return;

            _requestsLaunched = true;
            CoroutineExecutionMaster.StartRoutine(DelayedRequests());
        }

        private IEnumerator DelayedWorkerRegistration(ProjectileRequest request, ProjectileOperationSource operation)
        {
            yield return null;

            for (var i = 0; i < request.weapon.ProjectilesQuantity; i++)
            {
                _workers.Add(new ProjectileRaycastWorker(request, operation, Settings));
            }

            LaunchRequestsIfNecessary();
        }

        private IEnumerator DelayedRequests()
        {
            while (_workers.Count != 0)
            {
                foreach (var worker in _workers)
                {
                    worker.Process();

                    if (worker.IsCompleted)
                    {
                        _delayedRemove.Add(worker);
                    }
                }
                
                /// Плохая реализация
                if (_delayedRemove.Count != 0)
                {
                    foreach (var worker in _delayedRemove)
                    {
                        _workers.Remove(worker);
                    }
                    _delayedRemove.Clear();
                }

                yield return null;
            }

            _requestsLaunched = false;
        }
    }
}
