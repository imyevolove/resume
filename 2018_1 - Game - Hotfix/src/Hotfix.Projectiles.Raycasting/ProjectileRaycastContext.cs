﻿using Hotfix.Weapons;
using UnityEngine;

namespace Hotfix.Projectiles.Raycasting
{
    public struct ProjectileRaycastContext
    {
        public ProjectileRaycastSettings raycastSettings;
        public ProjectileDescriptorStruct projectile;
        public Ray ray;
        public float damage;
        public float totalDistance;
    }
}
