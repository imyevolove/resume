﻿namespace Hotfix.Projectiles.Raycasting
{
    /*
    [Serializable]
    public class ProjectileRaycaster : BaseProjectileRaycaster
    {
        public ProjectileRaycaster()
        {
        }
        public ProjectileRaycaster(ProjectileRaycastSettings settings)
        {
            Settings = settings;
        }

        public override ProjectileOperation Raycast(ProjectileRequest request)
        {
            var operation = new ProjectileOperationSource();

            Coroutines.CoroutineExecutionMaster.StartRoutine(DelayedRequests(request, operation));
            
            return operation.Operation;
        }

        private IEnumerator DelayedRequests(ProjectileRequest request, ProjectileOperationSource operation)
        {
            yield return null;

            for (var i = 0; i < request.weapon.ProjectilesQuantity; i++)
            {
                var process = new RaycastRequest(ref request, this);
                process.Perform();

                foreach (var hit in process.Hits)
                {
                    operation.DispatchHitEvent(hit);
                }
            }

            operation.Complete();
        }

        public struct RaycastRequest
        {
            public LayerMask hitMask;
            public float maxDistance;

            public Vector3 direction;
            public RaycastHit previousHit;
            
            public int IterationIndex { get; private set; }
            public int IterationsCount { get; private set; }
            public readonly List<ProjectileHit> Hits;
            
            public float projectileDamage;
            public float totalRaycastLength;
            public ProjectileDescriptorStruct projectileData;
            public float NormalizedDamage => Mathf.Max(projectileDamage, projectileData.DamageMinimum);

            public bool IsCompleted => IterationIndex >= IterationsCount;
            private IWeaponDescriptor weapon;

            public RaycastRequest(ref ProjectileRequest requestFrom, ProjectileRaycaster raycaster)
            {
                Hits = new List<ProjectileHit>();
                weapon = requestFrom.weapon;
                totalRaycastLength = 0f;
                projectileDamage = requestFrom.weapon.ProjectileData.Damage;
                projectileData = requestFrom.weapon.ProjectileData;
                previousHit = new RaycastHit { point = requestFrom.ray.origin };
                IterationIndex = 0;
                IterationsCount = raycaster.Settings.iterations;
                hitMask = raycaster.Settings.mask;
                maxDistance = raycaster.Settings.maxDistance;

                direction = requestFrom.ray.direction;
                /// Модифицируем первый раз как симуляция разброса
                ApplySpread(requestFrom.spread);
            }

            public void RegisterHit(ref RaycastHit raycastHit, ProjectileHitType hitType = ProjectileHitType.Normal)
            {
                var hit = new ProjectileHit(new ContactPoint(raycastHit), weapon, projectileDamage, hitType);

                Hits.Add(hit);

                totalRaycastLength += (raycastHit.point - previousHit.point).magnitude;
            }

            public void Perform()
            {
                IterationIndex = 0;

                while (!IsCompleted)
                {
                    NextRaycast();
                }

                RemoveLastPenetrationHit();
            }

            public void Next() => IterationIndex++;
            public void Complete() => IterationIndex = IterationsCount;

            private void NextRaycast()
            {
                /// Проверяем, не истратила ли пуля вес потенциал
                if (projectileDamage <= 0)
                {
                    Complete();
                    return;
                }

                /// Первый запрос на столкновение
                if (!ProjectileRaycastUtils.BatchedRaycast(new Ray(previousHit.point, direction), out RaycastHit initialHit, hitMask, maxDistance))
                {
                    /// Нет попадания, выходим из цикла
                    Complete();
                    return;
                }

                /// Добавляем сегмент в коллекцию
                ProjectileUtils.ApplySpaceDamageSuppression(weapon.ProjectileData, totalRaycastLength, (initialHit.point - previousHit.point).magnitude, ref projectileDamage);
                RegisterHit(ref initialHit);

                previousHit = initialHit;

                /// Проверяем, не истратила ли пуля вес потенциал
                if (projectileDamage <= 0)
                {
                    Complete();
                    return;
                }

                var penetrationDirection = direction;

                /// Поиск материала.
                var physicalMaterial = initialHit.collider.GetComponent<IPhysicalMaterial>();
                if (physicalMaterial != null && physicalMaterial.IsValid)
                {
                    /// Смотрим на вероятность рикошета.
                    if (ProjectileUtils.CanRicochet(initialHit.normal, penetrationDirection, physicalMaterial))
                    {
                        var reflectedDirection = Vector3.Reflect(penetrationDirection, initialHit.normal);
                        var reflectedCross = Vector3.Cross(penetrationDirection, reflectedDirection).normalized;

                        /// Изменить на вес, чтобы рикошет в любом сучае срабатывал (но вроде так оно и есть, хз, чекать надо)
                        reflectedDirection.z += -reflectedCross.y * ARandom.Value * physicalMaterial.RicochetAngleDistortion * Mathf.Deg2Rad;
                        reflectedDirection.y += reflectedCross.z * ARandom.Value * physicalMaterial.RicochetAngleDistortion * Mathf.Deg2Rad;

                        /// Перезаполняем запрос и продолжаем выполнение
                        previousHit = initialHit;
                        direction = reflectedDirection;

                        ProjectileUtils.ApplyRicocheteDamageSuppression(physicalMaterial, ref projectileDamage);

                        Next();
                        return;
                    }

                    if (!PhysicalMaterialFlagsUtils.HasFlag(physicalMaterial.Flags, PhysicalMaterialFlags.AllowPenetration))
                    {
                        /// Пробиваемость запрещена, выходим.
                        Complete();
                        return;
                    }

                    if (PhysicalMaterialFlagsUtils.HasFlag(physicalMaterial.Flags, PhysicalMaterialFlags.AllowPenetrationDistortion))
                    {
                        physicalMaterial.ApplyPenetrationDistortion(ref penetrationDirection);
                    }
                }

                /// **** ПРЯМОЙ РЕЙКАСТ БЕЗ МОДИФИКАЦИЙ
                /// Поиск выходного отверстия.
                if (ProjectileRaycastUtils.NativeRaycastBackface(initialHit, out RaycastHit finalHit, penetrationDirection))
                {
                    /// Выходное отверстие найдено, Добавляем сегмент в коллекцию
                    ProjectileUtils.ApplyPenetrationDamageSuppression(physicalMaterial, (finalHit.point - initialHit.point).magnitude, ref projectileDamage);
                    RegisterHit(ref finalHit, ProjectileHitType.Penetration);

                    previousHit = finalHit;
                }

                /// Перезаполняем запрос и продолжаем выполнение
                direction = penetrationDirection;

                Next();
            }

            private void RemoveLastPenetrationHit()
            {
                if (Hits.Count < 2) return;
                if (Hits.Last().Type == ProjectileHitType.Penetration)
                {
                    Hits.RemoveAt(Hits.Count - 1);
                }
            }

            public void ApplySpread(float spread)
            {
                var spreadVector = ARandom.InsideUnitSphere * spread * Mathf.Deg2Rad;
                direction.x += spreadVector.x;
                direction.y += spreadVector.y;
                direction.z += spreadVector.z;
                direction.Normalize();
            }
        }
    }
    */
}
