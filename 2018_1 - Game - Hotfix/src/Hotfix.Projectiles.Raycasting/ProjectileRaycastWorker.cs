﻿using Hotfix.Physics.Materials;
using UnityEngine;
using UPhysics = UnityEngine.Physics;

namespace Hotfix.Projectiles.Raycasting
{
    public class ProjectileRaycastWorker
    {
        public static float BackfaceRaycastOffsetMultiplier = 0.01f;

        private ProjectileOperationSource _operation;
        private ProjectileRequest _request;
        private ProjectileRaycastSettings _raycastSettings;

        public int IterationIndex { get; private set; } = 0;
        public bool IsCompleted { get; private set; }

        private ProjectileRaycastContext _context;
        private float _lastHitDistance;

        public ProjectileRaycastWorker(ProjectileRequest request, ProjectileOperationSource operation, ProjectileRaycastSettings raycastSettings)
        {
            _request = request;
            _raycastSettings = raycastSettings;
            _operation = operation;

            BuildInitialContext();
        }

        public void Process()
        {
            if (IterationIndex >= _raycastSettings.iterations)
            {
                Complete();
                return;
            }

            Pass();
            IterationIndex++;
        }

        private void Pass()
        {
            UPhysics.queriesHitBackfaces = false;

            /// Первый запрос на столкновение
            if (!ProjectileRaycastUtils.BatchedRaycast(_context.ray, out RaycastHit initialHit, _context.raycastSettings.mask, _context.raycastSettings.maxDistance))
            {
                /// Нет попадания, выходим из цикла
                Complete();
                return;
            }

            RecalculateDistances(initialHit.point, _context.ray.origin);
            ProjectileUtils.ApplySpaceDamageSuppression(_context.projectile, _context.totalDistance, _lastHitDistance, ref _context.damage);

            _operation.DispatchHitEvent(new ProjectileHit(new ContactPoint(initialHit), _context.ray.direction, _request.weapon, (int)_context.damage, ProjectileHitType.Normal));
            DrawDebugLine(initialHit.point);

            _context.ray.origin = initialHit.point;

            /// Проверяем, не истратила ли пуля вес потенциал
            if (TryCompleteByDamage()) return;

            var penetrationDirection = _context.ray.direction;

            /// Поиск материала.
            var physicalMaterial = initialHit.collider.GetComponent<IPhysicalMaterialProvider>()?.Material;
            var physicalMaterialIsValid = physicalMaterial != null && physicalMaterial.IsValid;

            if (physicalMaterial == null || !physicalMaterial.IsValid)
            {
                Debug.LogWarning($"PHYSICAL MATERIAL NOT FOUND OR INVALID. Object: {initialHit.transform.name}");

                Complete();
                return;
            }

            /// Смотрим на вероятность рикошета.
            if (PhysicalMaterialFlagsUtils.HasFlag(physicalMaterial.Flags, PhysicalMaterialFlags.AllowRicochet) && ProjectileUtils.CanRicochet(initialHit.normal, penetrationDirection, physicalMaterial))
            {
                var reflectedDirection = Vector3.Reflect(penetrationDirection, initialHit.normal);
                var reflectedCross = Vector3.Cross(penetrationDirection, reflectedDirection).normalized;

                /// Изменить на вес, чтобы рикошет в любом сучае срабатывал (но вроде так оно и есть, хз, чекать надо)
                reflectedDirection.z += -reflectedCross.y * ARandom.Value * physicalMaterial.RicochetAngleDistortion * Mathf.Deg2Rad;
                reflectedDirection.y += reflectedCross.z * ARandom.Value * physicalMaterial.RicochetAngleDistortion * Mathf.Deg2Rad;

                /// Перезаполняем запрос и продолжаем выполнение
                _context.ray.direction = reflectedDirection;
                ApplyRaycastOriginOffset();

                ProjectileUtils.ApplyRicocheteDamageSuppression(physicalMaterial, ref _context.damage);

                return;
            }

            if (!PhysicalMaterialFlagsUtils.HasFlag(physicalMaterial.Flags, PhysicalMaterialFlags.AllowPenetration))
            {
                /// Пробиваемость запрещена, выходим.
                Complete();
                return;
            }

            if (PhysicalMaterialFlagsUtils.HasFlag(physicalMaterial.Flags, PhysicalMaterialFlags.AllowPenetrationDistortion))
            {
                physicalMaterial.ApplyPenetrationDistortion(ref penetrationDirection);
            }

            ApplyRaycastOriginOffset();

            /// **** ПРЯМОЙ РЕЙКАСТ БЕЗ МОДИФИКАЦИЙ
            /// Поиск выходного отверстия.
            if (initialHit.collider is MeshCollider)
            {
                UPhysics.queriesHitBackfaces = true;
                if (ProjectileRaycastUtils.BatchedRaycast(_context.ray, out RaycastHit finalHit, _context.raycastSettings.mask, _context.raycastSettings.maxDistance))
                {
                    RecalculateDistances(finalHit.point, initialHit.point);
                    ProjectileUtils.ApplyPenetrationDamageSuppression(physicalMaterial, _lastHitDistance, ref _context.damage);

                    /// Проверяем, не истратила ли пуля вес потенциал
                    if (TryCompleteByDamage()) return;

                    _operation.DispatchHitEvent(new ProjectileHit(new ContactPoint(finalHit), (finalHit.point - initialHit.point).normalized, _request.weapon, (int)_context.damage, ProjectileHitType.Penetration));

                    DrawPenetrationDebugLine(finalHit.point);

                    _context.ray.origin = finalHit.point;
                }
                UPhysics.queriesHitBackfaces = false;
            }
            else
            {
                if (ProjectileRaycastUtils.NativeRaycastBackface(initialHit, out RaycastHit finalHit, penetrationDirection))
                {
                    RecalculateDistances(finalHit.point, initialHit.point);
                    ProjectileUtils.ApplyPenetrationDamageSuppression(physicalMaterial, _lastHitDistance, ref _context.damage);

                    /// Проверяем, не истратила ли пуля вес потенциал
                    if (TryCompleteByDamage()) return;

                    _operation.DispatchHitEvent(new ProjectileHit(new ContactPoint(finalHit), (finalHit.point - initialHit.point).normalized, _request.weapon, (int)_context.damage, ProjectileHitType.Penetration));

                    DrawPenetrationDebugLine(finalHit.point);

                    _context.ray.origin = finalHit.point;
                }
            }
            

            /// Перезаполняем запрос и продолжаем выполнение
            _context.ray.direction = penetrationDirection;
        }

        private void ApplyRaycastOriginOffset()
        {
            _context.ray.origin += _context.ray.direction * BackfaceRaycastOffsetMultiplier;
        }

        private void Complete()
        {
            if (IsCompleted) return;
            IsCompleted = true;
        }
        private bool TryCompleteByDamage()
        {
            if (_context.damage <= 0)
            {
                Complete();
                return true;
            }

            return false;
        }

        private void RecalculateDistances(Vector3 start, Vector3 end)
        {
            _lastHitDistance = (start - end).magnitude;
            _context.totalDistance += _lastHitDistance;
        }
        private void BuildInitialContext()
        {
            _context.damage = _request.weapon.ProjectileData.Damage;
            _context.projectile = _request.weapon.ProjectileData;
            _context.ray = _request.ray;
            _context.raycastSettings = _raycastSettings;

            ApplySpread(_request.spread);
        }
        private void ApplySpread(float spread)
        {
            _context.ray.direction += ARandom.InsideUnitSphere * spread * Mathf.Deg2Rad;
            _context.ray.direction.Normalize();
        }
        private void DrawDebugLine(Vector3 end) => DrawDebugLine(end, Color.red);
        private void DrawPenetrationDebugLine(Vector3 end) => DrawDebugLine(end, Color.yellow);
        private void DrawDebugLine(Vector3 end, Color color) => Debug.DrawLine(_context.ray.origin, end, color, 3f);
    }
}
