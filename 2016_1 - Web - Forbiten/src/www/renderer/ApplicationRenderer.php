<?php namespace renderer;

use core\renderer\IRenderer;
use core\entities\Application;

class ApplicationRenderer implements IRenderer
{
    public function render(Application $application, ?\stdClass $params) : void
    {
        $params = self::repairParameter($params, new \stdClass());
        self::repairParameter($params->format, "normal");

        $template = '<div class="product" format="{FORMAT}"  style="background: url({APP_IMAGE})">
            <a class="link" href="{APP_STORE_LINK}" target="_blank"></a>
            <div class="details">
                <span class="_category">{APP_CATEGORY}</span>
                <span class="_app_name">{APP_NAME}</span>
            </div>
        </div>';

        self::replaceInTemplate($template, "{APP_CATEGORY}",      $application->category);
        self::replaceInTemplate($template, "{APP_NAME}",          $application->name);
        self::replaceInTemplate($template, "{APP_IMAGE}",         $application->image_src);
        self::replaceInTemplate($template, "{APP_STORE_LINK}",    $application->store_link);

        foreach($params as $key => $value)
        {
            self::replaceInTemplate($template, "{" . strtoupper($key) . "}", $value);
        }

        echo $template;
    }

    private static function defaultIfEmpty(string $value, string $defaultValue) : string
    {
        return is_null($value) ? $defaultValue : $value;
    }

    private static function replaceInTemplate(string &$template, string $keyword, string $value) : void
    {
        $template = str_replace($keyword, $value, $template);
    }

    private static function repairParameter(&$value, $default) : void
    {
        $value = is_null($value) ? $default : $value;
    }
}
?>
