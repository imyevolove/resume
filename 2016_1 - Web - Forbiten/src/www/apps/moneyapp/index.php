<?php
    define("APP_ROOT_DIR", __DIR__);

    $writable_data = [];
    $filepath = normalize_path(APP_ROOT_DIR . "/data/l_request.log");

    /// Collect POST|GET data
    $writable_data["post"]  = $_POST;
    $writable_data["get"]   = $_GET;

    /// Convert array to string with readable format
    $writable_content_string = json_encode($writable_data, JSON_PRETTY_PRINT);
    /// Put data to file
    file_put_contents($filepath, $writable_content_string . "\n\r", FILE_APPEND);
?>
