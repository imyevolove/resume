<?php
function __autoload($className)
{
    __autoload_base($className);
}
function __autoload_vendor($className)
{
    __autoload_base("vendor/" . $className);
}

function __autoload_base($className)
{
    $filename = DIR_ROOT . "//" . $className . ".php";
    $filename = str_replace('\\', '/', $filename);
    $filename = preg_replace('#/+#', '/', $filename);

    if (file_exists($filename))
    {
        require_once $filename;
    }
}
spl_autoload_register("__autoload");
spl_autoload_register("__autoload_vendor");
?>
