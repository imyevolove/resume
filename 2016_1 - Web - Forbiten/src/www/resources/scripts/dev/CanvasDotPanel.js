function CanvasDotPanel(targetCanvasElement)
{
    if(!HTMLCanvasElement.prototype.isPrototypeOf(targetCanvasElement))
        throw new Error("Arg targetCanvasElement is not HTMLCanvasElement");

    var canvas  = targetCanvasElement;
    var context = targetCanvasElement.getContext("2d");

    var update = new Update();

    var entitesIteratorIndex = 0;
    var entitesIteratorIndex2 = 0;
    var canvasBoundOffset = new Vector2(50, 50);
    var pointSizeMinMax = new Vector2(2, 4);
    var pointMoveVectorMinMax = new Vector2(-5, 5);
    var pointMoveVectorForceSpeed = 1;
    var lineLengthMax = 100;

    var animEnterTime = 1;

    var maxPointsCount = 0;
    var entities = [];

    var drawLineConnectinos = {},
        drawLinePointA,
        drawLinePointB,
        drawLineLength;

    this.getMaxPointsCount = function() {
        return maxPointsCount;
    };
    this.setMaxPointsCount = function(value) {
        if(typeof value != "number")
            throw new Error("Arg is not number");

        maxPointsCount = Math.positive(value);
        createPointsIfNeed();
    };

    init();
    function init()
    {
        resizeCanvasAtParent();
        targetCanvasElement.addEventListener("resize", resizeCanvasAtParent);
        createPointsIfNeed();
        update.subscribe(renderFunc);
    }

    function spawnPointEntity()
    {
        var point = new PointEntity(context);
        point.radius = getRandomPointRadius();
        point.alpha = 1;

        point.moveVector    = getRandomPointMoveVector();
        point.moveVector.x += pointMoveVectorForceSpeed * Math.getRandomPositiveOrNegative();
        point.moveVector.y += pointMoveVectorForceSpeed * Math.getRandomPositiveOrNegative();

        point.position = getRandomPointPosition();
    }

    function positionInCanvasRect(position, canvas, offset)
    {
        if(position.x < -offset.x || position.x > offset.x + canvas.width) return false;
        if(position.y < -offset.y || position.y > offset.y + canvas.height) return false;
        return true;
    }

    function createPointsIfNeed()
    {
        var count = maxPointsCount - entities.length;
            count = Math.clamp(count, 0, Number.MAX_SAFE_INTEGER);

        for(var i = 0; i < count; i++)
            spawnPointEntity();
    }

    function renderFunc()
    {
        clearCanvas();
        DrawLinesBetweenPoints();
        drawEntities();
    }

    function drawEntities()
    {
        for(entitesIteratorIndex = 0; entitesIteratorIndex < entities.length; entitesIteratorIndex++)
        {
            entities[entitesIteratorIndex].update();
            entities[entitesIteratorIndex].render();
        }
    }

    function clearCanvas()
    {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    function DrawLinesBetweenPoints()
    {
        drawLineConnectinos = {};

        for(entitesIteratorIndex = 0; entitesIteratorIndex < entities.length; entitesIteratorIndex++)
        {
            drawLinePointA = entities[entitesIteratorIndex];
            for(entitesIteratorIndex2 = 0; entitesIteratorIndex2 < entities.length; entitesIteratorIndex2++)
            {
                drawLinePointB = entities[entitesIteratorIndex2];

                // Can't connect it self
                if(drawLinePointA == drawLinePointB) continue;

                // Already connected
                if(drawLineConnectinos[drawLinePointA] == drawLinePointB ||
                    drawLineConnectinos[drawLinePointB] == drawLinePointA) continue;

                // Line length between points
                drawLineLength = getLengthBetweenPoints(drawLinePointB.position, drawLinePointA.position);

                if(drawLineLength < lineLengthMax)
                {
                    // Add connection history
                    drawLineConnectinos[drawLinePointA] = drawLinePointB;

                    drawConnectionLine(
                        drawLinePointB.position,
                        drawLinePointA.position,
                        "rgba(170, 169, 173, " + (1 - drawLineLength / lineLengthMax) *
                        Math.min(drawLinePointA.alpha, drawLinePointB.alpha) + ")");
                }
            }
        }
    }

    function drawConnectionLine(startPosition, endPosition, color)
    {
        context.beginPath();
        context.strokeStyle = color;
        context.moveTo(startPosition.x, startPosition.y);
        context.lineTo(endPosition.x, endPosition.y);
        context.stroke();
    }

    function getLengthBetweenPoints(pointA, pointB)
    {
        return Math.sqrt(Math.pow(pointB.x - pointA.x, 2) + Math.pow(pointB.y - pointA.y, 2));
    }

    // PointEntity class
    function PointEntity(ctx)
    {
        var that = this;

        var anim = new Animation(function(time) {
            that.alpha = time;
        }, animEnterTime);
        anim.play();

        entities.push(this);

        this.position = new Vector2(0, 0);
        this.moveVector = new Vector2(0, 0);

        this.color = new Color(170, 169, 173, 1);
        this.radius = 1;
        this.alpha = 0;

        this.context = ctx;
        this.update = function() {
             move();
             checkPointInRect();
        };
        this.render = function()
        {
            context.beginPath();
            context.arc(that.position.x, that.position.y, that.radius, 0, 2 * Math.PI, false);
            context.fillStyle = createColorStringFromColor(that.color.r, that.color.g, that.color.b, that.color.a * that.alpha);
            context.fill();
        };
        this.destroy = function()
        {
            entities.splice(entities.indexOf(this), 1);
        };

        function move()
        {
            that.position.x += that.moveVector.x * update.getDeltaTime();
            that.position.y += that.moveVector.y * update.getDeltaTime();
        }

        function checkPointInRect()
        {
            if(!positionInCanvasRect(that.position, canvas, canvasBoundOffset))
            {
                that.destroy();
                createPointsIfNeed();
            }
        }
    }

    function Color(r, g, b, a)
    {
        var that = this;

        this.r = r || 255;
        this.g = g || 255;
        this.b = b || 255;
        this.a = a || 1;

        this.getColorString = function()
        {
            return "rgba(" + that.r + "," + that.g + "," + that.b + "," + that.a + ")";
        };
    }

    function createColorStringFromColor(r, g, b, a)
    {
        return "rgba(" + r + "," + g + "," + b + "," + a + ")";
    }

    function getRandomPointRadius()
    {
        return Math.getRandom(pointSizeMinMax.x, pointSizeMinMax.y);
    }

    function getRandomPointMoveVector()
    {
        return new Vector2(
            Math.getRandom(pointMoveVectorMinMax.x, pointMoveVectorMinMax.y),
            Math.getRandom(pointMoveVectorMinMax.x, pointMoveVectorMinMax.y));
    }

    function getRandomPointPosition()
    {
        return new Vector2(
            Math.getRandom(0, canvas.width),
            Math.getRandom(0, canvas.height));
    }

    function Vector2(x, y)
    {
        this.x = x || 0;
        this.y = y || 0;
    }

    function resizeCanvasAtParent()
    {
        var parent = targetCanvasElement.parentElement;

        if(!parent)
            throw new Error("Canvas element is not attached to document");

        setCanvasSize(parent.offsetWidth, parent.offsetHeight);
    }

    function setCanvasSize(width, height)
    {
        // Set html element size
        canvas.style.width  = width  + "px";
        canvas.style.height = height + "px";

        // Set viewport size
        canvas.width  = width;
        canvas.height = height;
    }
}
