function Animation(action, time)
{
    // Exceptions here
    if(!Function.prototype.isPrototypeOf(action))
        throw new Error("Argument 'action' is not function");

    var update = new Update();
    var timeIndex = 0;

    this.play  = function() {
        timeIndex = 0;
        update.subscribe(Animate);
    };

    this.stop  = function() {
        timeIndex = 0;
        update.unsubscribe(Animate);
    };

    this.pause = function() {
        update.unsubscribe(Animate);
    };

    this.resume = function() {
        if(timeIndex >= time) return;
        update.subscribe(Animate);
    };

    function Animate(deltaTime)
    {
        // Breaker
        if(timeIndex >= time)
        {
            update.unsubscribe(Animate);

            action(1);
            return;
        }

        action(timeIndex / time);
        timeIndex += deltaTime;
    }
}
