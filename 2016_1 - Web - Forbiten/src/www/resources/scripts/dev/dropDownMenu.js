function toggleDropDownMenu(target, options)
{
    var attrValue = target.getAttribute(options.attr);
    target.setAttribute(options.attr, attrValue == options.opened ? options.closed : options.opened);
}
