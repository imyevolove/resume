var support = (function() {

    var supportFormElement;
    var supportForm;

    this.initialize = function() {
        supportFormElement = document.getElementById("support_form");
        initializeSupportForm();
    };

    function initializeSupportForm()
    {
        var isSending = false;
        var submitButton = supportFormElement.getElementsByTagName("button")[0] || supportFormElement.getElementsByTagName("submit")[0];

        submitButton.addEventListener("click", function() {
            if(isSending) return;

            var inputs = [].slice.call(supportFormElement.getElementsByTagName("input"));
            inputs = inputs.concat([].slice.call(supportFormElement.getElementsByTagName("textarea")));

            var data = {};
            var _chdName, _chdValue;

            for(var key in inputs)
            {
                _chdName = inputs[key].getAttribute("name");
                _chdValue = inputs[key].innerHTML || inputs[key].value;

                // Required field is empty
                if(inputs[key].getAttribute("required") && !_chdValue)
                {
                    console.log("Required field is empty");
                    return;
                }

                data[_chdName] = _chdValue;
            }

            changeButtonText("Sending ...");
            report.send(data, function(response) {
                if(!response)
                {
                    isSending = false;
                    changeButtonText("Send");
                    console.log("error");
                    return;
                }
                else
                {
                    if(response.status == "success")
                    {
                        changeButtonText("Success");
                        lockReportForm();
                    }
                    else
                    {
                        isSending = false;
                        console.log(response);
                        changeButtonText("Error [ " + response.message + " ] . Resend");
                    }
                }

            });
        });

        function changeButtonText(value)
        {
            submitButton.innerHTML = value;
        }
    }

    function lockReportForm()
    {
        document.getElementById("report_lock").style.display = "block";
        supportFormElement.parentElement.removeChild(supportFormElement);
    }

    return this;

}).call({});

support.initialize();
