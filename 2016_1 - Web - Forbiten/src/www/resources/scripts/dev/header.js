var header = (function() {

    var that = this;
    var scrollOffset = 100;
    var scrollLength = 100;
    var scrollIntensity = 0.8;

    var _cachedCurrentScrool = 0;
    var _cachedScrollIntensity = 0;

    this.element = document.querySelector(".header_section");

    initScrollListener();

    function initScrollListener()
    {
        document.addEventListener("scroll", headerAnimationListener);
    }

    function headerAnimationListener()
    {
        _cachedCurrentScrool = document.body.scrollTop - scrollOffset;
        _cachedCurrentScrool = Math.positive(_cachedCurrentScrool);

        if(scrollLength > _cachedCurrentScrool) {
            _cachedScrollIntensity = Math.lerp(0, scrollIntensity, _cachedCurrentScrool / scrollLength);
        } else {
            _cachedScrollIntensity = scrollIntensity;
        }

        that.element.style.boxShadow = "0 50px 50px -50px rgba(0, 0, 0, " + _cachedScrollIntensity + ")";
    }

    return this;

}).call({});
