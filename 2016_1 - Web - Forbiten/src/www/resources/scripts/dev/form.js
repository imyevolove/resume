function Form(element)
{
    if(!HTMLFormElement.prototype.isPrototypeOf(element))
        throw new Error("Element is not form element");

    var that = this;
    var elements = [];

    this.addInput = function(type, name, screenValue)
    {
        var fe = new FormElement("input");
        element.appendChild(fe.node);

        elements.push(fe);
    };

    this.addTextArea = function(name, screenValue, maxLength)
    {
        var fe = new FormElement("textarea");
        element.appendChild(fe.node);

        elements.push(fe);
    };

    initializeFromFormElement();
    function initializeFromFormElement()
    {
        var elements = element.getElementsByTagName('*');
            elements = [].slice.call(elements);

        elements.forEach(function(e) {
            if(!HTMLInputElement.prototype.isPrototypeOf(e) &&
                !HTMLTextAreaElement.prototype.isPrototypeOf(e)) return;

            var fe = new FormElement(e.tagName);
            fe.setScreenValue(e.getAttribute("screenValue"));
            fe.setInputMaxLength(e.getAttribute("maxlength"));

            copyAttributes(e, fe.node);

            element.insertBefore(fe.node, e);
            e.parentElement.removeChild(e);

            elements.push(fe);
        });
    }

    function createElement(tag)
    {
        return document.createElement(tag);
    }

    function addElementToForm(child)
    {
        element.appendChild(child);
    }

    function copyAttributes(fromElement, toElement)
    {
        var fromElementAttributes = fromElement.attributes;

        for(var i = 0; i < fromElementAttributes.length; i++)
        {
            toElement.setAttribute(fromElementAttributes[i].name, fromElementAttributes[i].value);
        }
    }

    function bindAttributes(element, attrs)
    {
        for(var key in attrs)
            element.setAttribute(key, attrs[key]);
    }

    function FormElement(tag)
    {
        var maxlength = -1, // -1 - infinity
            screenValue = "";

        var isActiveScreenValue = true;
        var _cachedValue = "";

        this.node = document.createElement(tag);

        controlScreenValue(this.node);

        this.setInputMaxLength = function(value) {
            if(typeof value != "number") return;
            maxlength = Math.clamp(value, -1, Number.MAX_SAFE_INTEGER);
        };
        this.getInputMaxLength = function() { return maxlength; };

        this.setScreenValue = function(value) {
            if(typeof value != "string") return;
            screenValue = value;
            controlScreenValue(this.node);
        };
        this.getScreenValue = function() { return screenValue; };

        this.node.addEventListener("input", inputEventListener);
        this.node.addEventListener("focus", focusEventListener);
        this.node.addEventListener("blur", blurEventListener);

        function focusEventListener(e)
        {
            controlScreenValue(e.target, true);
        }

        function blurEventListener(e)
        {
            controlScreenValue(e.target);
        }

        function inputEventListener(e)
        {
            controlInputValue(e.target);
        }

        function controlInputValue(e)
        {
            _cachedValue = e.innerHTML || e.value;
        }

        function controlScreenValue(e, forceHide)
        {
            isActiveScreenValue = _cachedValue === undefined || _cachedValue.length === 0;

            if(forceHide === true)
            {
                e.innerHTML = e.value = _cachedValue;
            }
            else if(isActiveScreenValue)
            {
                e.innerHTML = e.value = screenValue;
            }
        }
    }
}
