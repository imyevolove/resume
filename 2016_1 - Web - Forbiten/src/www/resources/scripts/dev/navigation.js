var navigation = (function() {
    var currentNavigateAnimation = null;

    // Navigate to element with attribute "tag"
    this.navigateTo = function(tag, offset) {
        var element = findElementWithTag(tag);

        if(!HTMLElement.prototype.isPrototypeOf(element))
            throw new Error("Element not found");

        if(typeof offset != "number")
            offset = 0;

        if(currentNavigateAnimation !== null)
        {
            currentNavigateAnimation.stop();
            currentNavigateAnimation = null;
        }

        currentNavigateAnimation = new Animation(createAnimScrollAction(scrollToVerticalPosition, element, -146 + offset), 0.4);
        currentNavigateAnimation.play();
    };

    function findElementWithTag(tag)
    {
        var allElements = document.getElementsByTagName('*');
            allElements = [].slice.call(allElements);

        return allElements.filter(new AttributeFilter("tag", tag))[0];
    }

    // Scroll to DOM element function
    function scrollToVerticalPosition(value)
    {
        document.body.scrollTop = value;
    }

    function createAnimScrollAction(scrollFunction, element, offset)
    {
        var from = document.body.scrollTop;
        var to = getOffsetTop(element) + offset;

        return function(time)
        {
            scrollFunction(Math.lerp(from, to, time));
        };
    }

    function AttributeFilter(attr, value)
    {
        return function(e) {

            if(!HTMLElement.prototype.isPrototypeOf(e)) {
                return false;
            }

            return value
                ? (e.getAttribute(attr) == value)
                : (e.getAttribute(attr) != null);
        };
    }

    function getOffsetTop(el) {
        var _y = 0;
        while( el && !isNaN( el.offsetTop ) ) {
            _y += el.offsetTop;
            el = el.offsetParent;
        }
        return _y;
    }

    return this;
}).call({});
