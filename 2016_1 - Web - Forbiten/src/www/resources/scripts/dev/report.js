var report = (function() {

    this.send = function(data, callback)
    {
        var formData = new FormData();

        for(var key in data)
        {
            formData.append(key, data[key]);
        }

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function()
        {
            if(xhr.readyState == 4)
            {
                if(xhr.status == 200)
                {
                    callback(JSON.parse(xhr.responseText));
                }
                else
                {
                    callback(null);
                }
            }
        };
        xhr.open("POST", "/report");
        xhr.send(formData);
    };

    return this;

}).call({});
