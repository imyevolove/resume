Math.lerp = function(a, b, t) {
    return a*(1-t) + b*t;
};

Math.clamp = function(value, min_value, max_value) {
    return Math.min(Math.max(value, min_value), max_value);
};

Math.positive = function(v) {
    return v < 0 ? 0 : v;
};

Math.getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

Math.getRandom = function(min, max) {
    return Math.random() * (max - min) + min;
};

Math.getRandomPositiveOrNegative = function()
{
    return Math.random() < 0.5 ? -1 : 1;
};
