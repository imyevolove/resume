var requestAnimationFrame = window.requestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.msRequestAnimationFrame;
var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.mozCancelAnimationFrame;

function Update()
{
    var listeners = [];
    var requestID;
    var i = 0;
    var lastExecutionTime = 0;
    var deltaTime = 0;

    this.getDeltaTime = function() {
        return deltaTime;
    };

    this.subscribe = function(callback) {
        if(listeners.indexOf(callback) != -1) return;
        listeners.push(callback);

        if(listeners.length > 0 && requestID === undefined)
        {
            lastExecutionTime = Date.now();
            requestID = requestAnimationFrame(update);
        }
    };

    this.unsubscribe = function(callback) {
        listeners.splice(listeners.indexOf(callback), 1);

        if(listeners.length === 0 && requestID !== undefined)
        {
            cancelAnimationFrame(requestID);
            requestID = undefined;
        }
    };

    function update()
    {
        deltaTime = (Date.now() - lastExecutionTime) / 1000;

        for(i = 0; i < listeners.length; i++)
            listeners[i](deltaTime);

        lastExecutionTime = Date.now();
        requestID = requestAnimationFrame(update);
    }
}
