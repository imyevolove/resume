<?php session_start();
// Root index file

define("DIR_ROOT", __DIR__);

require_once "path.php";
require_once "autoload.php";
require_once "vendor/Mobile_Detect.php";

use core\WebApplication;
use core\ApplicationsDataLoaderFromJson;
use Phroute\Phroute\RouteCollector;

$deviceDetector = new Mobile_Detect();
define("IS_MOBILE_DEVIDE", $deviceDetector->isMobile());

$webApplication = new WebApplication();

$appsLoader = new ApplicationsDataLoaderFromJson();
$appsLoader->filename = normalize_path(DIR_ROOT . "/data/apps.json");
$webApplication->getApplicationsManager()->loadAppsData($appsLoader);

// ROUTER
$router = new RouteCollector();

// routes here
$router->any('/', function() {
    require_once "pages/index.php";
});

/*
$router->any('/new', function() {
    require_once "pages/n_index.php";
});
*/

$router->any('/elite', function() {
    require_once "pages/elite.php";
});

$router->post('/report', function() {
    // SUPPORT MESSAGE HERE
    // REMOVE THIS SHIT. MAKE MORE RESPONSIVE

    $report_interval = 2 * 60; // 2 minutes
    $current_time = time();

    // INTERVAL
    if(isset($_SESSION["last_report"]) && is_int($_SESSION["last_report"]) && $_SESSION["last_report"] + $report_interval > $current_time)
    {
        response_json(array(
            "status" => "error",
            "message" => "Exceeded the number of reports. Try again later."
        ));
    }

    // DATA IS NOT FOUND
    if(
        !isset($_POST["game"]) || !isset($_POST["device"]) || !isset($_POST["email"]) || !isset($_POST["message"]) ||
        empty($_POST["game"]) || empty($_POST["device"]) || empty($_POST["email"])|| empty($_POST["message"])
        )
    {
        response_json(array(
            "status" => "error",
            "message" => "One of the arguments is empty"
        ));
    }

    $game       = format_string($_POST["game"]);
    $device     = format_string($_POST["device"]);
    $email      = format_string($_POST["email"]);
    $message    = format_string($_POST["message"]);

    if(
        !filter_var($email, FILTER_VALIDATE_EMAIL) ||
        empty($game) || empty($device) || empty($message) ||
        !is_string($game) || !is_string($device) || !is_string($message)
    )
    {
        response_json(array(
            "status" => "error",
            "message" => "One of the arguments is wrong"
        ));
    }

    $to      = 'forbitenstudio@gmail.com';
    $subject = 'Forbiten website report';
    $message = 'Sender: ' . $email.
    "\r\n Game: " . $game .
    "\r\n Device: " . $device .
    "\r\n Message: " . $message;
    $headers = 'From: support@forbiten.com' . "\r\n" .
        'Reply-To: ' . $email . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    try
    {
        mail($to, $subject, $message, $headers);
        $_SESSION["last_report"] = time();

        response_json(array(
            "status" => "success",
            "message" => "Message sended"
        ));
    } catch (Exception $e)
    {
        response_json(array(
            "status" => "error",
            "message" => "Send message error. Catch block"
        ));
    }

});

function format_string($value)
{
    $value = htmlentities($value, ENT_QUOTES | ENT_IGNORE, "UTF-8");
    return $value;
}

function response_json(array $data)
{
    echo json_encode($data);
    exit();
}

// DISPATCHER
try
{
    $dispatcher = new Phroute\Phroute\Dispatcher($router->getData());
    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
}

// 404 HERE
catch (Exception $e)
{
    require_once "pages/errors/404.html";
}
