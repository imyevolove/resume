<?php namespace core;
interface IApplicationsDataLoader
{
    public function load() : array;
}
?>
