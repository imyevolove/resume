<?php namespace core\renderer;

use core\entities\Application;

interface IRenderer
{
    public function render(Application $application, ?\stdClass $params) : void;
}
?>
