<?php namespace core;

use core\IApplicationsDataLoader;
use core\entities\Application;

class ApplicationsDataLoaderFromJson implements IApplicationsDataLoader
{
    public $filename;

    public function load() : array
    {
        $data   = json_decode(file_get_contents($this->filename));
        $apps   = [];
        $app    = null;
        
        foreach ($data as $value)
        {
            $app = new Application();

            foreach ($value as $key => $value)
            {
                $app->$key = $value;
            }

            array_push($apps, $app);
        }

        return $apps;
    }
}
?>
