<?php
namespace core
{
    use core\entities\Application;
    use core\IApplicationsDataLoader;

    class ApplicationsManager
    {
        private $applications = [];

        public function loadAppsData(IApplicationsDataLoader $loader)
        {
            $this->applications = $loader->load();
        }

        public function getApps() : array
        {
            return $this->applications;
        }

        public function registerApp(Application $application) : void
        {
            array_push($this->applications, $application);
        }

        public function unregisterApp(Application $application) : void
        {
            array_splice($this->applications, array_search($application, $this->applications), 1);
        }
    }
}
?>
