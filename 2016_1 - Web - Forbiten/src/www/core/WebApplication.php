<?php
namespace core
{
    use core\ApplicationsManager;
    use core\entities\Application;

    class WebApplication
    {
        private $applicationsManager;

        function __construct()
        {
            $this->applicationsManager = new ApplicationsManager();
        }

        public function getApplicationsManager() : ApplicationsManager
        {
            return $this->applicationsManager;
        }
    }
}
?>
