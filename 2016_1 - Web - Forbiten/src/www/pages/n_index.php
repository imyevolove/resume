<!DOCTYPE HTML>
<html>
    <head>
        <title>FORBITEN</title>
        <link rel="icon" type="image/png" href="/resources/media/favicon.png" />

        <!-- META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Forbiten Studio. Games developers">
        <meta name="keywords" content="Forbiten, Forbiten Studio, Games, Indie Games, Android games, Clicker, Mobile games, игры, мобильные игры, инди игры, андройд игры, кликеры, кликер">
        <meta name="author" content="Alexey Medvedev">

        <meta name='robots' content='index,follow'>
        <meta name='target' content='all'>
        <meta name='distribution' content='Global'>
        <meta name='rating' content='General'>

        <!-- INCLUDE STYLES -->
        <link type="text/css" rel="stylesheet" href="/resources/css/base.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/css/new.main.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/fonts/agency/font.css" />
        <link type="text/css" rel="stylesheet" href="/resources/fonts/fontello/fontello.css" />

        <!-- INCLUDE GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=cyrillic" rel="stylesheet">

        <!-- INCLUDE SCRIPTS -->
        <!--
        <script type="text/javascript" src="resources/scripts/math.extension.js" defer></script>
        <script type="text/javascript" src="resources/scripts/update.js" defer></script>
        <script type="text/javascript" src="resources/scripts/animation.js" defer></script>
        <script type="text/javascript" src="resources/scripts/CanvasDotPanel.js" defer></script>
        <script type="text/javascript" src="resources/scripts/navigation.js" defer></script>
        <script type="text/javascript" src="resources/scripts/dropDownMenu.js" defer></script>
        <script type="text/javascript" src="resources/scripts/report.js" defer></script>

        <script type="text/javascript" src="resources/scripts/header.js"  defer></script>
        <script type="text/javascript" src="resources/scripts/support.js"  defer></script>
        -->
        <!--
        <script type="text/javascript" src="/resources/scripts/min.scripts.js"  defer></script>

        <script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>
        -->

    </head>
    <body>

        <script type="text/javascript">
            window.addEventListener("load", function () {
               document.getElementById("loader").style.display = "none";
            });
        </script>

        <!-- LOADER SCREEN -->
        <div class="loader" id="loader">
            <div class="logo" style="background-image: url(/resources/media/loader_preview.jpg)">
                <div class="loading">loading</div>
            </div>
        </div>

        <!-- FIRST SLIDE -->
        <div class="display-head" style="background-image: url(/resources/media/bg.jpg)">
            <div class="_content">
                <div class="_inner_content">
                    <nav>
                        <!-- <div class="navbar-brand">forbiten</div> -->
                        <ul class="navbar-list">
                            <li class="nav-item">
                                <a class="nav-link">О нас</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Игры</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Поддержка</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Связаться с нами</a>
                            </li>
                        </ul>
                        <div class="navbar-float-cleaner"></div>
                    </nav>
                    <div class="hi_title">
                        <div class="title">#forbiten</div>
                        <div class="sub_title">this world needs new games</div>
                    </div>
                </div>
            </div>
            <div class="_icon" style="background-image: url(/resources/media/arrow-down.png)"></div>
<!--            <div class="overlay_dotted_grid" style="background-image: url(/resources/media/patterns/black-plus-signs.png)"></div>-->
            <div class="_logo" style="background-image: url(/resources/media/logo_white_high.png)"></div>
        </div>
        <div class="body">
                <div class="about_section base_section">
                    <div class="container">
                        <div class="section-title">Этому  миру нужны новые игры</div>
                        <p class="section_paragraph">
                            Слоган нашей студии "Этому  миру нужны новые игры".
                        </p>
                        <div class="applications-list">
                            <div class="application-item">
                                <div class="image"></div>
                                <div class="details"></div>
                            </div>
                            <div class="application-item"></div>
                            <div class="application-item"></div>
                        </div>
                    </div>
                </div>
        </div>
    </body>
</html>
