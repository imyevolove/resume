<!DOCTYPE HTML>
<html>
    <head>
        <title>FORBITEN</title>
        <link rel="icon" type="image/png" href="resources/media/img/favicon.png" />

        <!-- META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Forbiten Studio. Games developers. Tactical Battlefield Simulator">
        <meta name="keywords" content="Tactical Battlefield Simulator, Forbiten, Forbiten Studio, Games, Indie Games, Android games, Clicker, Mobile games, игры, мобильные игры, инди игры, андройд игры, кликеры, кликер">
        <meta name="author" content="Alexey Medvedev">

        <meta name='robots' content='index,follow'>
        <meta name='target' content='all'>
        <meta name='distribution' content='Global'>
        <meta name='rating' content='General'>

        <!-- INCLUDE STYLES -->
        <link type="text/css" rel="stylesheet" href="/resources/css/main.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/css/project.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/fonts/DIN/din.css" />
        <link type="text/css" rel="stylesheet" href="/resources/fonts/fontello/fontello.css" />

        <!-- INCLUDE GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=cyrillic" rel="stylesheet">

    </head>
    <body>
        <header class="project_header">

        </header>
        <div class="page project-page">
            <div class="project_section">
                <div class="pj-title">Project name</div>
                <div class="pj-content">Project text</div>
            </div>
        </div>
    </body>
</html>
