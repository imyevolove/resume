<?php
    // INCLUDES
    use renderer\ApplicationRenderer;

    global $webApplication;

    //
    $apps = $webApplication->getApplicationsManager()->getApps();

    // RENDERERS
    $applicationRenderer = new ApplicationRenderer();
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>FORBITEN</title>
        <link rel="icon" type="image/png" href="resources/media/img/favicon.png" />

        <!-- META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Forbiten Studio. Games developers">
        <meta name="keywords" content="Forbiten, Forbiten Studio, Games, Indie Games, Android games, Clicker, Mobile games, игры, мобильные игры, инди игры, андройд игры, кликеры, кликер">
        <meta name="author" content="Alexey Medvedev">

        <meta name='robots' content='index,follow'>
        <meta name='target' content='all'>
        <meta name='distribution' content='Global'>
        <meta name='rating' content='General'>

        <!-- INCLUDE STYLES -->
        <link type="text/css" rel="stylesheet" href="/resources/css/main.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/fonts/DIN/din.css" />
        <link type="text/css" rel="stylesheet" href="/resources/fonts/fontello/fontello.css" />

        <!-- INCLUDE GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=cyrillic" rel="stylesheet">

        <!-- INCLUDE SCRIPTS -->
        <!-- <script type="text/javascript" src="resources/scripts/math.extension.js" defer></script>
        <script type="text/javascript" src="resources/scripts/update.js" defer></script>
        <script type="text/javascript" src="resources/scripts/animation.js" defer></script>
        <script type="text/javascript" src="resources/scripts/CanvasDotPanel.js" defer></script>
        <script type="text/javascript" src="resources/scripts/navigation.js" defer></script>
        <script type="text/javascript" src="resources/scripts/dropDownMenu.js" defer></script>
        <script type="text/javascript" src="resources/scripts/report.js" defer></script>

        <script type="text/javascript" src="resources/scripts/header.js"  defer></script>
        <script type="text/javascript" src="resources/scripts/support.js"  defer></script> -->
        <script type="text/javascript" src="/resources/scripts/min.scripts.js"  defer></script>

        <script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>

    </head>
    <body>
        <!-- HEADER NAVIGATION -->
        <header>
            <div class="header_section">
                <div class="logotype">
                    <div class="image" style="background: url(resources/media/img/logo.png)"></div>
                </div>
                <div class="media_menu_button" onclick="toggleDropDownMenu(document.getElementById('header_navigation'), {attr: 'smart_visible', opened: 'visible', closed: 'hidden' })">menu</div>
                <div class="navigation" id="header_navigation">
                    <nav>
                        <div class="button" color="red" onclick="navigation.navigateTo('about')">О нас</div>
                        <div class="button" onclick="navigation.navigateTo('games', -35)">Игры</div>
                        <div class="button" onclick="navigation.navigateTo('support', -35)">Поддержка</div>
                        <div class="button" onclick="navigation.navigateTo('support', -35)">Связаться с нами</div>
                        <div class="fakebutton">
                            <a href="https://play.google.com/store/apps/dev?id=6978304903959327663" target="_blank" class="link">
                                <div class="icon" style="background: url(resources/media/img/google_play_icon.png)"></div>
                                <span>Google Play</span>
                            </a>
                        </div>
                    </nav>
                </div>
            </div>
        </header>

        <!-- PAGE -->
        <div class="page">

            <!-- DOT PANEL -->
            <div class="dot_decoration_section">
                <div class="dot_panel">
                    <div class="gradient"></div>
                    <canvas id="canvas_dot_panel_element"></canvas>
                    <script>
                    if(!window["CanvasDotPanel"])
                    {
                        window.addEventListener("load", function load(event){
                            initCanvasDotPanel();
                        },false);
                    }
                    else
                    {
                        initCanvasDotPanel();
                    }

                    function initCanvasDotPanel()
                    {
                        var canvasDotPanel = new CanvasDotPanel(document.getElementById("canvas_dot_panel_element"));
                        canvasDotPanel.setMaxPointsCount(20);
                    }
                    </script>
                </div>
            </div>

            <!-- PAGE HEADER -->
            <section class="page_header_section image_renderer" style="background: url(resources/media/img/header_bg.png)" tag="about">
                <div class="page_header_content">
                    <div class="studio_title">FORBITEN</div>
                    <div class="line_divider"></div>
                    <div class="studio_about">
                        Привет, путник! Добро пожаловать на сайт студии Forbiten!
                        <br />
                        Мы создаем инди игры разнообразных жанров для ПК и других платформ.
                        <br />
                        Разработка игр — наша главная страсть.
                    </div>
                    <!--
                    <div class="downloads_counter">
                        <div class="_container">
                            <div class="_value">70.000</div>
                            <div class="_title">games downloaded</div>
                        </div>
                    </div>
                    -->
                </div>
            </section>

            <!-- PAGE BODY GAMES -->
            <section class="page_body_section" tag="games">
                <!-- PRODUCTS -->
                <div class="products_sub_section">
                    <div class="section_title">Игры</div>
                    <div class="products">
                        <?php
                            foreach ($apps as $app)
                                $applicationRenderer->render($app, null);
                        ?>
                    </div>
                </div>

                <!-- SUPPORT -->
                <div class="support_sub_section_wrapper" style="background: url(resources/media/img/support_bg.png)">
                    <div class="support_sub_section" tag="support">
                        <div class="section_title">Поддержка | Связаться с нами</div>
                        <div class="report_sended" id="report_lock">Сообщение отправлено</div>
                        <form id="support_form">
                            <input type="text" name="game" placeholder="Название игры" />
                            <input type="text" name="device" placeholder="Устройство (Android|IOS|WP) и его версия" />
                            <input type="email" name="email" placeholder="Email обратной связи" />
                            <textarea name="message" maxlength="500" placeholder="Текст сообщения"></textarea>
                            <button class="send_button" onclick="event.preventDefault()">Отправить сообщение</button>
                        </form>
                    </div>
                </div>

                <!-- SOCIAL -->
                <!--
                <div class="social_sub_section">
                    <div class="section_title">Социальные сети</div>
                    <div>

                    </div>
                </div>
                -->

            </section>


            <!-- FOOTER -->
            <footer>
                <div class="footer_section">
                    <div class="footer_cell navigation">
                        <list class="nav_list">
                            <ul>
                                <li><a onclick="navigation.navigateTo('about')">О Нас</a></li>
                                <li><a onclick="navigation.navigateTo('games', -35)">Игры</a></li>
                                <li><a onclick="navigation.navigateTo('support')">Поддержка</a></li>
                                <li><a onclick="navigation.navigateTo('support')">Связаться с нами</a></li>
                            </ul>
                        </list>
                    </div>
                    <div class="footer_cell copyright">Forbiten <?php echo date("Y"); ?></div>
                    <div class="footer_cell social">
                        <a href="https://twitter.com/ForbitenStudio" target="_blank" class="social_link twitter icon-twitter"></a>
                        <a href="https://vk.com/club104689708" target="_blank" class="social_link vk icon-vkontakte"></a>
                    </div>
                </div>
            </footer>

        </div>

        <?php if (!IS_MOBILE_DEVIDE): ?>

        <!-- VK Widget Messages -->
        <div id="vk_community_messages"></div>
        <script type="text/javascript">
        VK.Widgets.CommunityMessages("vk_community_messages", 104689708, {tooltipButtonText: "Есть вопрос?"});
        </script>

        <?php endif; ?>

    </body>
</html>
