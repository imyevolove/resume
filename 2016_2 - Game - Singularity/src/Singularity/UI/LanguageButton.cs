﻿using Singularity.LocalizationManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Singularity.UI
{
    [Serializable]
    public class LanguageButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        protected LanguageContextMenu languageContextMenu;

        [SerializeField]
        protected Text text;

        public void OnPointerClick(PointerEventData eventData)
        {
            languageContextMenu.NextVisibility();
        }

        protected void Start()
        {
            LocalizationManager.onChange += UpdateText;
            UpdateText();
        }

        protected void UpdateText()
        {
            LocalizationBundle bundle = LocalizationManager.GetLocalizationBundle(LocalizationManager.CurrentLanguage);
            if (bundle == null) return;

            text.text = bundle.Signature.LocalName.ToUpper();
        }

        private void OnDestroy()
        {
            LocalizationManager.onChange -= UpdateText;
        }
    }
}
