﻿using Singularity.AnimatorState;
using Singularity.IAPManagement;
using Singularity.LocalizationManagement;
using Singularity.LocalizationManagement.Mono;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Singularity.UI
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class PurchaseProductCard : MonoBehaviour
    {
        public event UnityAction onChange;

        [SerializeField]
        protected Image image;

        [SerializeField]
        protected Button buyButton;

        [SerializeField]
        protected TextLocalizer localizer;

        protected AnimatorStateManager animatorStateManager;
        protected Dictionary<string, BaseAnimatorState> states
            = new Dictionary<string, BaseAnimatorState>()
            {
                { "locked",     new BoolAnimatorState("locked") },
                { "unlocked",   new BoolAnimatorState("unlocked") },
                { "selected",   new BoolAnimatorState("selected") }
            };

        protected IProduct targetProduct;

        ////////////////////////////////////////////////////////////
        public void SetTarget(IProduct product)
        {
            if (product == null) return;

            if (targetProduct != null)
            {
                targetProduct.onStateChange -= Redraw;
                targetProduct.onInfoChange  -= Redraw;
            }

            targetProduct = product;
            targetProduct.onStateChange += Redraw;
            targetProduct.onInfoChange  += Redraw;

            Redraw();
        }

        ////////////////////////////////////////////////////////////
        public void Redraw()
        {
            if (targetProduct == null) return;
            image.sprite = targetProduct.image;

            switch (targetProduct.state)
            {
                case ProductState.Locked:
                    animatorStateManager.Dismiss();
                    animatorStateManager.Activate(states["locked"]);
                    localizer.UpdateTextCustom("shop_card_locked", string.IsNullOrEmpty(targetProduct.price) 
                        ? LocalizationManager.GetLocalization("shop_card_buy")
                        : targetProduct.price);
                    break;
                case ProductState.Unlocked:
                    animatorStateManager.Dismiss();
                    animatorStateManager.Activate(states["unlocked"]);
                    localizer.UpdateTextCustom("shop_card_unlocked");
                    break;
            }

            if (targetProduct is IUsable)
            {
                IUsable usable = targetProduct as IUsable;
                /// Already used
                if (!usable.CanUse())
                {
                    animatorStateManager.Dismiss();
                    animatorStateManager.Activate(states["selected"]);
                    localizer.UpdateTextCustom("shop_card_selected");
                }
            }
        }

        ////////////////////////////////////////////////////////////
        protected void OnClickAction()
        {
            if (targetProduct == null) return;
            switch (targetProduct.state)
            {
                case ProductState.Locked:
                    targetProduct.Purchase();
                    break;
                case ProductState.Unlocked:
                    if (targetProduct is IUsable)
                    {
                        IUsable usable = targetProduct as IUsable;
                        if (!usable.CanUse()) break;

                        usable.Use();
                        Redraw();
                    }
                    break;
            }


            CallOnChangeEvent();
        }

        ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////// UNITY ACTIONS
        protected void Awake()
        {
            animatorStateManager = new AnimatorStateManager(GetComponent<Animator>());
            buyButton.onClick.RemoveAllListeners();
            buyButton.onClick.AddListener(OnClickAction);
        }

        ////////////////////////////////////////////////////////////
        protected void OnEnable()
        {
            Redraw();
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnChangeEvent()
        {
            if (onChange != null)
            {
                onChange();
            }
        }

        ////////////////////////////////////////////////////////////
        protected virtual void OnDestroy()
        {
            targetProduct.onStateChange -= Redraw;
            targetProduct.onInfoChange  -= Redraw;
        }
    }
}
