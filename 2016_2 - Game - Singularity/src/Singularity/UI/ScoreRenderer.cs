using System.Collections.Generic;
using Singularity.Controllers;
using Singularity.LocalizationManagement.Mono;
using Singularity.Personal;
using UnityEngine.UI;
using Singularity.Models.Animation;
using UnityEngine;

namespace Singularity.UI
{
    public class ScoreRenderer : TextLocalizer
    {
        int currentValue;
        int endValue;

        IndependentAnimation animText = new IndependentAnimation() { time = 0.25f };

        protected override void Start()
        {
            base.Start();

            animText.action = (time) => {
                UpdateText(((int)Mathf.Lerp(currentValue, endValue, time)).ToString());
            };
            animText.onEnd += delegate {
                currentValue = endValue;
                UpdateText(currentValue.ToString());
            };

            var score = GameController.Instance.gameHandler.score;
            score.onChange += UpdateText;
            UpdateText(score);
        }

        protected void UpdateText(Score score)
        {
            endValue = score.Value;
            animText.Dismiss();
            animText.Execute();
        }
        /*
        protected IEnumerator UpdateTextAnimation()
        {
            IndependentAnimation anim = new IndependentAnimation() { time = 1, action = (time) => {} };
            anim.Execute();
        }
        */
    }
}