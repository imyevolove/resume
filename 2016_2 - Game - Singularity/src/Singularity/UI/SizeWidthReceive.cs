﻿using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable, ExecuteInEditMode, RequireComponent(typeof(RectTransform))]
public class SizeWidthReceive : MonoBehaviour
{
	[SerializeField]
	public RectTransform targetTransform;
	
	[SerializeField]
	public bool  receiveWidth = false;
	[SerializeField]
	public bool  limitWidthMin = false;
	[SerializeField]
	public bool  limitWidthMax = false;
	[SerializeField]
	public float widthMin;
	[SerializeField]
	public float widthMax;

	void Start()
	{
		CalculateSize();
	}

	void Update()
	{
		CalculateSize();
	}

	void CalculateSize()
	{
		if(targetTransform == null)
			return;

		if(!receiveWidth)
			return;

		RectTransform selfTransform = (RectTransform)transform;

		Vector2 size = new Vector2(
			receiveWidth  ? targetTransform.sizeDelta.x : selfTransform.sizeDelta.x,
			selfTransform.sizeDelta.y
		);

		if(limitWidthMin || limitWidthMax)
			size.x = Mathf.Clamp(size.x, 
				limitWidthMin ? widthMin : float.NegativeInfinity, 
				limitWidthMax ? widthMax : float.PositiveInfinity);

		selfTransform.sizeDelta = size;
	}
}