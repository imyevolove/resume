using System;
using System.Collections.Generic;
using Singularity.Controllers;
using Singularity.Models;
using Singularity.Models.UI;
using UnityEngine;

namespace Singularity.UI
{
    [Serializable]
    public class PipelinePoolVisualizer : MonoBehaviour
    {
        [SerializeField]
        public int poolCount;

        [SerializeField]
        protected Transform container;

        [SerializeField]
        protected TilePipelineViewer pipelineViewerPrefab;

        [SerializeField]
        protected TilePipeline pipeline;

        protected LinkedQueue<TilePipelineViewer> viewersQueue = new LinkedQueue<TilePipelineViewer>();

        private int index = 0;

        public void Start()
        {
            RenderAllTiles();
            pipeline.onChange += RenderNext;
            pipeline.onReset  += RenderAllTiles;
        }
        
        public TilePipelineViewer GetItem(int index)
        {
            return viewersQueue[index];
        }

        private void RenderNext()
        {
            TilePipelineViewer viewer = viewersQueue.Dequeue();
            if(viewer != null) viewer.Destroy();

            int shift = Mathf.Clamp(poolCount, 0, pipeline.queueLength) - 1;
            
            AddRenderPoolTile(shift, true, false);
            MakeNextViewerDraggable();
        }

        private void MakeNextViewerDraggable()
        {
            if(viewersQueue.Count <= 0)
                return;
            
            TilePipelineViewer viewer = viewersQueue.Peek();
            
            if(viewer == null)
            {
                 viewersQueue.Dequeue();
                 MakeNextViewerDraggable();
                 return;
            }

            ActivateDragAndDropSystem(viewer, true);
        }

        private void RenderAllTiles()
        {
            ClearPool();

            int count = Mathf.Clamp(poolCount, 0, pipeline.queueLength);
            for(var i = 0; i < count; i++)
                AddRenderPoolTile(i, true, i == 0);
        }

        private void AddRenderPoolTile(int index, bool reverse, bool dragADrop)
        {
            TilePipelineViewer viewer = GameObject.Instantiate<TilePipelineViewer>(pipelineViewerPrefab, container, false);
            viewer.name = "Viewer_" + this.index;
            viewer.tileData = pipeline.GetTileFromPoolSilent(index);
            viewer.index = index;
            viewer.pipeline = pipeline;

            RectTransform rt = (RectTransform)viewer.transform;
            rt.anchoredPosition = new Vector2(
                0,
                ((RectTransform)container.transform).rect.height
            );

            viewersQueue.Enqueue(viewer);

            //if(reverse) viewer.transform.SetAsFirstSibling();
            
            ActivateDragAndDropSystem(viewer, dragADrop);

            this.index++;
        }

        private void ActivateDragAndDropSystem(TilePipelineViewer viewer, bool status)
        {
            TilePipelineViewerPickupComponent dd = viewer.GetComponent<TilePipelineViewerPickupComponent>();
            if(!dd) dd = viewer.gameObject.AddComponent<TilePipelineViewerPickupComponent>();

            dd.enabled = status; 
        }
        
        private void ClearPool()
        {
            viewersQueue.Clear();
            foreach(TilePipelineViewer child in container.transform.GetComponentsInChildren<TilePipelineViewer>())
                child.Destroy();
        }
    }
}