﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode, RequireComponent(typeof(GridLayoutGroup))]
public class GridLayoutGroupDynamicCellSizeModifier : MonoBehaviour
{
	// For Fast row settings
	[SerializeField]
	protected int cellCount;

	// Force Redraw or redraw all time flag
	public bool forceUpdate = false;

	void Start()
	{
		CalculateCellSize ();
	}

	void Update()
	{
		if (transform.hasChanged || forceUpdate)
		{
			CalculateCellSize ();
		}
		transform.hasChanged = false;
	}

	void CalculateCellSize()
	{
		var layout = GetComponent<GridLayoutGroup> ();
		var layoutTransform = (RectTransform)layout.transform;

		// Cell Sizes
		float 	width 	= layoutTransform.rect.width,
				height 	= layoutTransform.rect.height;

		// Decrease padding
		width 	-= layout.padding.horizontal;
		height 	-= layout.padding.vertical;

		// Decrease spaces
		width 	-= layout.spacing.x * Mathf.Abs(cellCount - 1);
		height 	-= layout.spacing.y * Mathf.Abs(cellCount - 1);

		// Per cell
		width 	/= cellCount;
		height 	/= cellCount;

		layout.cellSize = new Vector2 (width, height);
	}
}
