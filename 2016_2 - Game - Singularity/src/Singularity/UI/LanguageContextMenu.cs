﻿using Singularity.LocalizationManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.UI
{
    [Serializable]
    public class LanguageContextMenu : MonoBehaviour
    {
        [SerializeField]
        protected RectTransform contentTransform;

        [SerializeField]
        protected Button languageButtonPrefab;

        public bool Visibility
        {
            get { return gameObject.activeSelf; }
        }

        ////////////////////////////////////////////////////////////
        public void SetVisibility(bool visibility)
        {
            gameObject.SetActive(visibility);
        }

        ////////////////////////////////////////////////////////////
        public void NextVisibility()
        {
            SetVisibility(!Visibility);
        }

        ////////////////////////////////////////////////////////////
        public void Show()
        {
            SetVisibility(true);
        }

        ////////////////////////////////////////////////////////////
        public void Hide()
        {
            SetVisibility(false);
        }

        ////////////////////////////////////////////////////////////
        protected void Start()
        {
            foreach (var language in LocalizationManager.Languages)
            {
                InstantiateButton(language);
            }
        }

        ////////////////////////////////////////////////////////////
        protected void InstantiateButton(string lang)
        {
            var button = GameObject.Instantiate<Button>(languageButtonPrefab, contentTransform, false);
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate {
                SelectLanguage(lang);
            });
            button.onClick.AddListener(Hide);

            Text text = button.GetComponentInChildren<Text>();
            if (text != null)
            {
                LocalizationBundle bundle = LocalizationManager.GetLocalizationBundle(lang);
                if (bundle != null)
                {
                    text.text = bundle.Signature.LocalName;
                }
            }
        }

        protected void SelectLanguage(string lang)
        {
            if (LocalizationManager.ApplyLocalizationLanguage(lang))
            {
                Game.current.SetSelectedLanguageCode(lang);
            }
        }
    }
}
