﻿using Singularity.IAPManagement;
using Singularity.LocalizationManagement;
using Singularity.LocalizationManagement.Mono;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.UI
{
    public class PurchaseSpecialProduct : MonoBehaviour
    {
        [SerializeField]
        protected Button buyButton;

        [SerializeField]
        protected TextLocalizer descriptionLocalizator;

        [SerializeField]
        protected TextLocalizer priceLocalizator;

        protected IProduct targetProduct;

        ////////////////////////////////////////////////////////////
        public void SetTarget(IProduct product)
        {
            if (product == null) return;

            if (targetProduct != null)
            {
                targetProduct.onStateChange -= Redraw;
                targetProduct.onInfoChange  -= Redraw;
            }

            targetProduct = product;
            targetProduct.onStateChange += Redraw;
            targetProduct.onInfoChange  += Redraw;

            Redraw();
        }

        ////////////////////////////////////////////////////////////
        public void Redraw()
        {
            if (targetProduct == null) return;
            switch (targetProduct.state)
            {
                case ProductState.Locked:

                    priceLocalizator.UpdateTextCustom("shop_card_locked", string.IsNullOrEmpty(targetProduct.price)
                        ? LocalizationManager.GetLocalization("shop_card_buy")
                        : targetProduct.price);

                    descriptionLocalizator.SetCustomText(targetProduct.description);

                    gameObject.SetActive(true);
                    break;
                case ProductState.Unlocked:
                    gameObject.SetActive(false);
                    break;
            }
        }

        ////////////////////////////////////////////////////////////
        protected void OnClickAction()
        {
            if (targetProduct == null) return;
            switch (targetProduct.state)
            {
                case ProductState.Locked:
                    targetProduct.Purchase();
                    break;
                case ProductState.Unlocked:
                    Redraw();
                    break;
            }
        }

        ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////// UNITY ACTIONS
        protected void Awake()
        {
            buyButton.onClick.RemoveAllListeners();
            buyButton.onClick.AddListener(OnClickAction);
        }
    }
}
