﻿using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable, ExecuteInEditMode, RequireComponent(typeof(RectTransform))]
public class CopySize : MonoBehaviour
{
	[SerializeField]
	public RectTransform targetTransform;

    [SerializeField]
    public Vector2 padding;

    void Start()
	{
		UpdateSize();
	}

	void Update()
	{
		UpdateSize();
	}

	void UpdateSize()
	{
		((RectTransform)transform).sizeDelta = targetTransform.sizeDelta + padding;
	}
}