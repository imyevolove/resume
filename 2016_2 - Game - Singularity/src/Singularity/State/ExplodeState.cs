using UnityEngine;

namespace Singularity.State
{
    public class ExplodeState : IState
    {
        public void Use(Animator animator)
        {
            animator.SetBool("explode", true);
        }

        public void Dismiss(Animator animator)
        {
            animator.SetBool("explode", false);
        }
    }
}