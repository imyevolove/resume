using System;
using UnityEngine;

namespace Singularity.State
{
    public static class States
    {
        public static IState DragState              = new DragState();
        public static IState IdleState              = new IdleState();
        public static IState ReduceState            = new ReduceState();
        public static IState ShakeState            = new ShakeState();
        public static IState ExplodeState            = new ExplodeState();
    }
}