using UnityEngine;

namespace Singularity.State
{
    public class IdleState : IState
    {
        public void Use(Animator animator)
        {
            animator.SetBool("idle", true);
        }

        public void Dismiss(Animator animator)
        {
            animator.SetBool("idle", false);
        }
    }
}