using UnityEngine;

namespace Singularity.State
{
    public class ShakeState : IState
    {
        public void Use(Animator animator)
        {
            animator.SetBool("shake", true);
        }

        public void Dismiss(Animator animator)
        {
            animator.SetBool("shake", false);
        }
    }
}