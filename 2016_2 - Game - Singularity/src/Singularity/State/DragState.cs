using UnityEngine;

namespace Singularity.State
{
    public class DragState : IState
    {
        public void Use(Animator animator)
        {
            animator.SetBool("drag", true);
        }

        public void Dismiss(Animator animator)
        {
            animator.SetBool("drag", false);
        }
    }
}