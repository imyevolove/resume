using UnityEngine;

namespace Singularity.State
{
    public interface IState
    {
        void Use(Animator animator);
        void Dismiss(Animator animator);
    }
}