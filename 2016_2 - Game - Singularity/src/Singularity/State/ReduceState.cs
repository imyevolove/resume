using UnityEngine;

namespace Singularity.State
{
    public class ReduceState : IState
    {
        public void Use(Animator animator)
        {
            animator.SetBool("reduce", true);
        }

        public void Dismiss(Animator animator)
        {
            animator.SetBool("reduce", false);
        }
    }
}