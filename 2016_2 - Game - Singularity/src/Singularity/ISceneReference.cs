using UnityEngine.SceneManagement;

namespace Singularity
{
    public interface ISceneReference
    {
        string SceneID { get; }
        void Load(LoadSceneMode mode);
        void LoadAsync(LoadSceneMode mode);
    }
}