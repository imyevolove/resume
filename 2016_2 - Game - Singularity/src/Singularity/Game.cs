using System.Xml.Serialization;
using Singularity.IO;
using Singularity.Models;
using Singularity.Personal;
using Singularity.Personal.Data;
using UnityEngine;
using Singularity.IAPManagement;

namespace Singularity
{
    [System.Serializable]
    public class Game
    {
        private static Game m_CurrentGameInstance;
        public  static Game current
        {
            get 
            {
                if(m_CurrentGameInstance == null)
                    m_CurrentGameInstance = new Game();
                return m_CurrentGameInstance;
            }
            protected set
            {
                m_CurrentGameInstance = value;
            }
        }

        public GameDataManager gameDataManager = new GameDataManager();
        public Player player = new Player();
        public GameHandler gameHandler;

        ////////////////////////////////////////////////////////////
        public Game()
        {
            gameDataManager.Load();
            player = new Player(gameDataManager.gameData.playerData);
        }

        ////////////////////////////////////////////////////////////
        public void RegisterPurchase(IProduct product)
        {
            gameDataManager.gameData.playerPurchasesData.RegisterPurchase(product);
            Save();
        }

        ////////////////////////////////////////////////////////////
        public void UnregisterPurchase(IProduct product)
        {
            gameDataManager.gameData.playerPurchasesData.UnregisterPurchase(product);
            Save();
        }

        ////////////////////////////////////////////////////////////
        public void SetSelectedTheme(string themeId)
        {
            gameDataManager.gameData.playerSettingsData.selectedThemeId = themeId;
            Save();
        }

        ////////////////////////////////////////////////////////////
        public string GetSelectedTheme()
        {
            return gameDataManager.gameData.playerSettingsData.selectedThemeId;
        }

        ////////////////////////////////////////////////////////////
        public void SetSelectedLanguageCode(string languageCode)
        {
            gameDataManager.gameData.playerSettingsData.selectedLanguageCode = languageCode;
            Save();
        }

        ////////////////////////////////////////////////////////////
        public string GetSelectedLanguageCode()
        {
            return gameDataManager.gameData.playerSettingsData.selectedLanguageCode;
        }

        ////////////////////////////////////////////////////////////
        public string[] GetPurchases()
        {
            return gameDataManager.gameData.playerPurchasesData.purchases.ToArray();
        }

        ////////////////////////////////////////////////////////////
        public void Save()
        {
            gameDataManager.gameData.playerData = player.ToPlayerData();
            gameDataManager.Save();
        }
    }
}