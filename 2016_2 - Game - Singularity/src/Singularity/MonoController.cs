﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity
{
    public abstract class MonoController : MonoBehaviour
    {
        protected virtual void Awake()
        {
            EnableDontDestroyOnLoad();
        }

        protected virtual void EnableDontDestroyOnLoad()
        {
            DontDestroyOnLoad(gameObject);
        }

        protected static TController LoadAndInstantiateController<TController>() where TController : MonoController
        {
            string controllerName = typeof(TController).Name;
            string path = "Controllers/" + "pref_ctrl_" + controllerName;

            TController controllerOriginal = Resources.Load<TController>(path);
            
            if(controllerOriginal == null)
            {
                Debug.LogWarningFormat("Controller {0} was not fonded by path {1}", controllerName, path);
                return default(TController);
            }
            
            return GameObject.Instantiate<TController>(controllerOriginal);
        }

        public static void Load<TController>() where TController : MonoController
        {
            TController controller = GameObject.FindObjectOfType<TController>();
            if (controller != null) return;

            LoadAndInstantiateController<TController>();
        }
    }
}
