using UnityEngine;
using UnityEngine.SceneManagement;

namespace Singularity
{
    public class SceneReference : ISceneReference
    {
        private string m_SceneID;
        public  string SceneID { get { return m_SceneID; } }

        public SceneReference(string sceneID)
        {
            m_SceneID = sceneID;
        }


        /// <summary>
        /// Load scene 
        /// </summary>
        /// <param name="mode"></param>
        public void Load(LoadSceneMode mode = LoadSceneMode.Single)
        {
            SceneManager.LoadScene(SceneID, mode);
        }

        /// <summary>
        /// Load scene async
        /// </summary>
        /// <param name="mode"></param>
        public void LoadAsync(LoadSceneMode mode = LoadSceneMode.Single)
        {
            SceneManager.LoadSceneAsync(SceneID, mode);
        }
    }
}