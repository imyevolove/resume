namespace Singularity.Models.IO.Events
{
    public interface IDraggable
    {
        void OnDragStart();
        void OnDragEnd();
        void OnDrag();
    }
}