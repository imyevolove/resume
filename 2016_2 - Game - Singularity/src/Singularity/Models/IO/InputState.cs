namespace Singularity.Models.IO
{
    public enum InputState
    {
        None,
        DragTile,
        DragSkill
    }
}