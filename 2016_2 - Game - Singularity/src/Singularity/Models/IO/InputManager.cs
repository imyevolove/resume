using UnityEngine;

namespace Singularity.Models.IO
{
    public class InputManager
    {
        private static InputManager m_Instance;
        public static InputManager Instance
        { 
            get
            {
                if(m_Instance == null)
                    m_Instance = new InputManager();
                return m_Instance;
            }
        }

        public InputState state { get; protected set; }
        public object target { get; protected set; }
        public bool tileSelection;

        public bool isCurrentTileTarget
        {
            get { return target is Tile; }
        }

        public CellPtr currentCell;

        public void SetState(InputState state, object target)
        {
            this.state  = state;
            this.target = target;
        }

        public void ResetData()
        {
            currentCell = null;
            tileSelection = false;
            SetState(InputState.None, null);
        }
    }
}