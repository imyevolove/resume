using UnityEngine;
using UnityEngine.EventSystems;

namespace Singularity.Models.IO.Events
{
    public static class InputUtility
    {
        public static TileContainer GetTileContainerFromEventData(PointerEventData eventData)
        {
            if(eventData == null || eventData.hovered == null)
                return null;

            GameObject go = eventData.hovered.Find(g => { return g.GetComponent<TileContainer>() != null; });
            if(go == null) return null;
            
            TileContainer container = go.GetComponent<TileContainer>();
            return container;
        }
    }
}