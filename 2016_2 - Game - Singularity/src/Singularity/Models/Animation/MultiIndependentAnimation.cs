﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Singularity.Models.Animation
{
	public class MultiIndependentAnimation
	{
		public bool performed { get; protected set; }

		public event AnimationEventHandler onStart;
		public event AnimationEventHandler onEnd;

		protected Dictionary<IndependentAnimation, AnimationState> animations
			= new Dictionary<IndependentAnimation, AnimationState>();

		public void AddAnimation(IndependentAnimation animation)
		{
			if(animations.ContainsKey(animation))
			{
				Debug.LogWarning("Animation already added");
				return;
			}

			animations.Add(animation, AnimationState.Idle);
			animation.onStart 	+= delegate { animations[animation] = AnimationState.Runned; };
			animation.onEnd 	+= delegate { animations[animation] = AnimationState.Idle; TryCallEnd(); };
		}

		public void Perform()
		{
			if(performed)
			{
				Debug.Log("Animation is runned, wait until the end of the operation can cause before it again");
				return;
			}

			performed = true;

			if(onStart != null)
				onStart();

			List<IndependentAnimation> keys = new List<IndependentAnimation> (animations.Keys);
			foreach(var anim in keys)
				anim.Execute();
		}

		protected void TryCallEnd()
		{
			if(!performed)
				return;

			if(!HasAllStateAnimations(AnimationState.Idle))
				return;

			performed = true;

			if(onEnd != null)
				onEnd();
		}

		protected bool HasAllStateAnimations(AnimationState state)
		{
			foreach(var animState in animations.Values)
				if(animState != state)
					return false;
			return true;
		}

	}
}