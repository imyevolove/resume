﻿using System.Collections;
using UnityEngine;

namespace Singularity.Models.Animation
{
	public class IndependentAnimation
	{
		protected float _time;
		public float time
		{
			get { return _time; }
			set { _time = Mathf.Abs(value); }
		}

		protected float _pause;
		public float pause
		{
			get { return _pause; }
			set { _pause = Mathf.Abs(value); }
		}

		public bool performed { get; protected set; }

		public event AnimationEventHandler onStart;
		public event AnimationEventHandler onEnd;
		public event AnimationEventHandler onPauseEnd;

		public AnimationIterator action;

		private AnimationExecutor executor;

		public IndependentAnimation()
		{
			performed = false;
			onEnd += DeactivateExecutor;
		}

		public void Dismiss()
		{
			DeactivateExecutor();
			performed = false;
		}

		public void Execute()
		{
			if(performed)
			{
				Debug.Log("Animation is runned, wait until the end of the operation can cause before it again");
				return;
			}

			if(action == null)
			{
				Debug.Log("Animation action is not defined");
				return;
			}

			performed = true;

			executor = AnimationExecutorPool.GetInstance().GetExecutorInstance();
			executor.ExecuteAnimation(Iterator());
		}

		private void DeactivateExecutor()
		{
			if(executor == null) return;

			executor.DismissAnimation();
			AnimationExecutorPool.GetInstance().DeactivateExecutor(executor);
			executor = null;
		}

		protected IEnumerator Iterator()
        {
			float elapsedTime = 0;
			float pause = -this.pause;

			while(pause < 0)
			{
				pause += Time.deltaTime;
				yield return true;
			}

			if(onPauseEnd != null)
				onPauseEnd();

            if (onStart != null)
                onStart();

            while (elapsedTime < time)
            {
                elapsedTime += Time.deltaTime;
				action(elapsedTime / time);
                yield return true;
            }
			action(1);

			if(onEnd != null)
				onEnd();
        }

	}
}