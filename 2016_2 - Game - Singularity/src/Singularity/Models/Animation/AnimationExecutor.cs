﻿using System.Collections;
using UnityEngine;

namespace Singularity.Models.Animation
{
	public class AnimationExecutor : MonoBehaviour
	{
		public void OnDisable()
		{
			enabled = true;
		}

		public void ExecuteAnimation(IEnumerator action)
		{
			StartCoroutine(action);
		}

		public void DismissAnimation()
		{
			StopAllCoroutines();
		}
	}
}