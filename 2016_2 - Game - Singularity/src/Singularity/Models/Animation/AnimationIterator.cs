﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Singularity.Models.Animation
{
	public delegate void AnimationIterator(float time);
}