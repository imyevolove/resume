﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Singularity.Models.Animation
{
	public class AnimationExecutorPool
	{
		private static AnimationExecutorPool Instance;
		public static AnimationExecutorPool GetInstance()
		{
			if(Instance == null)
				Instance = new AnimationExecutorPool();
			return Instance;
		}

		protected List<AnimationExecutor> executors = new List<AnimationExecutor>();
		public AnimationExecutor GetExecutorInstance()
		{
            foreach (var ex in executors)
			{
                if (ex == null)
                {
                    /// Remove null items
                    RemoveNullItems();
                    return GetExecutorInstance();
                }
				if(!ex.gameObject.activeSelf)
				{
					ex.gameObject.SetActive(true);
					return ex;
				}
			}

			AnimationExecutor executor = new GameObject("AnimationExecutor", typeof(AnimationExecutor))
							.GetComponent<AnimationExecutor>();
			executors.Add(executor);
			return executor;
		}

        public void DeactivateExecutor(AnimationExecutor executor)
        {
            executor.gameObject.SetActive(false);
        }

        protected void RemoveNullItems()
        {
            executors.RemoveAll(e => { return e == null; });
        }
    }
}