﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Singularity.Models
{
    [Serializable]
    public class BoardSize
    {
        [SerializeField]
        protected int x;

        [SerializeField]
        protected int y;

        public int Y { get { return y; } }
        public int X { get { return x; } }

        public int MaxIndex { get { return x * y - 1; } }
        public int Length   { get { return x * y; } }

        public BoardSize(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    
    [Serializable]
    public abstract class GameBoard : MonoBehaviour
    {
        [SerializeField]
        public   BoardSize Size;

        public event UnityAction<CellPtr, object> onCellClick;

        protected Tile[] tiles;
        
        protected virtual void Start()
        {
            tiles = new Tile[Size.X * Size.Y];
        }

        public bool hasEmptyCell
        {
            get { return Array.Exists<Tile>(tiles, tile => { return tile == null; }); }
        }

        public Tile[] GetTiles()
        {
            return tiles;
        }

        public Tile GetTile(int index)
        {
            return tiles[index];
        }

        public Tile GetTile(Tile tile)
        {
            return Array.Find<Tile>(tiles, (t) => t == tile);
        }

        public Tile GetTile(int x, int y)
        {
            return tiles[XYToIndex(x, y)];
        }

        public virtual bool RemoveTile(Tile tile)
        {
            int index = Array.FindIndex(tiles, (tl) => tl == tile);

            if(index < 0)
                return false;

            tiles[index] = null;
            return true;
        }

        public virtual bool RemoveTile(int x, int y)
        {
            int index = XYToIndex(x, y);
            
            tiles[index] = null;
            return true;
        }

        public virtual bool CanInsert(int x, int y)
        {
            return CanInsert(XYToIndex(x, y));
        }

        public virtual bool CanInsert(int index)
        {
            if(index < 0 || index > Size.MaxIndex)
                return false;

            if(tiles[index] != null)
                return false;

            return true;
        }

        public virtual bool InsertTile(Tile tile, int x, int y)
        {
            int index = XYToIndex(x, y);
            return InsertTile(tile, index);
        }

        public virtual bool InsertTile(Tile tile, CellPtr cellPtr)
        {
            int index = XYToIndex(cellPtr.x, cellPtr.y);
            return InsertTile(tile, index);
        }

        public virtual bool InsertTile(Tile tile, int index)
        {
            if(index < 0 || index > Size.MaxIndex)
                return false;

            if(tiles[index] != null)
                return false;

            if(tiles[index] != null)
                return false;

            tiles[index] = tile;
            return true;
        }

        public virtual bool HasTile(Tile tile)
        {
            Tile s_tile = Array.Find<Tile>(tiles, (t) => { return t == tile; });
            return s_tile != null;
        }

        public virtual void Clear()
        {
            if (tiles == null) return;

            foreach(var tile in tiles)
            {
                if(tile != null)
                    tile.Destroy();
            }

            Array.Clear(tiles, 0, tiles.Length);
        }

        protected void DispatchOnClickEvent(CellPtr cellPtr, object data)
        {
            if(onCellClick != null)
                onCellClick(cellPtr, data);
        }

        public int XYToIndex(int x, int y)
        {
            return y * Size.X + x;
        }

        public CellPtr IndexToXY(int index)
        {
            CellPtr cellPtr = new CellPtr(
                index % Size.X,
                Mathf.FloorToInt(index / Size.X)
            );
            return cellPtr;
        }

        public abstract Tile[] GetExpectedReduceTiles(int x, int y);
        public abstract Tile[] GetExpectedReduceTiles(int index);
        public abstract Tile[] GetExpectedReduceTiles(CellPtr cellPtr);

        public abstract void HighlightTilesAround(int x, int y);
        public abstract void HighlightTilesAround(int index);
        public abstract void HighlightTilesAround(CellPtr cellPtr);

        public abstract void DisableHighlight();
    }
}