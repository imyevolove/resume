﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.Audio
{
    [Serializable, RequireComponent(typeof(AudioSource))]
    public class AudioQueuePlayer : MonoBehaviour
    {
        [SerializeField]
        public List<AudioClip> audioClips = new List<AudioClip> ();
        public int currentAudioClipIndex { get; protected set; }

        [HideInInspector]
        protected AudioSource audioSource;

        ////////////////////////////////////////////////////////////
        public void Reset()
        {
            currentAudioClipIndex = 0;
        }

        ////////////////////////////////////////////////////////////
        public void PlayNext()
        {
            bool canPlay = true;

            canPlay = audioClips.Count != 0;
            if (!canPlay) return;

            audioSource.PlayOneShot(audioClips[currentAudioClipIndex]);

            currentAudioClipIndex++;
            if (currentAudioClipIndex >= audioClips.Count)
                currentAudioClipIndex = audioClips.Count - 1;
        }

        ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////// UNITY METHODS
        protected void Awake()
        {
            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }
        }
    }
}
