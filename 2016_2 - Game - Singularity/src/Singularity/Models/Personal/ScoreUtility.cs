using System;
using System.Linq;

namespace Singularity.Personal
{
    public static class ScoreUtility
    {
        public static Score GetBestScore(params Score[] scores)
        {
            return scores.Aggregate((agg, next) => next.Value > agg.Value ? next : agg);
        }
    }
}