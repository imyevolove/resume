﻿namespace Singularity.Personal
{
    public abstract class BaseScoreCorrector
    {
        protected int result;

        ////////////////////////////////////////////////////////////
        public virtual int GetResult()
        {
            return result;
        }

        ////////////////////////////////////////////////////////////
        public virtual void Add(int weight, int count)
        {
            result += weight * count;
        }

        ////////////////////////////////////////////////////////////
        public virtual void Reset()
        {
            result = 0;
        }
    }
}
