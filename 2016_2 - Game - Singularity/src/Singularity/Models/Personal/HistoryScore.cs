using System;
using UnityEngine;

namespace Singularity.Personal
{
    [Serializable]
    public class HistoryScore : Score
    {
        public override int Add(int value)
        {
            return m_Value;
        }

        public override int Reduce(int value)
        {
            return m_Value;
        }

        public override void Clear()
        {
            // Do nothing
        }
    }
}