using System;
using Singularity.Personal.Data;
using UnityEngine;

namespace Singularity.Personal
{
    [Serializable]
    public class Player
    {
        public Score bestScore = new HistoryScore();

        public Player() {}
        public Player(PlayerData playerData)
        {
            bestScore.Set(playerData.playerScoreData.bestScore);
        }

        public bool SetBestScoreIfNeeded(Score score)
        {
            if(!bestScore.IsBetter(score)) 
                return false;
            
            bestScore.Change(score);
            return true;
        }


        public PlayerData ToPlayerData()
        {
            PlayerData playerData = new PlayerData();
            playerData.playerScoreData.bestScore = bestScore.Value;
            return playerData;
        }
    }
}