using System;
using Singularity.IO;
using UnityEngine;

namespace Singularity.Personal.Data
{
    [System.Serializable]
    public class PlayerSettingsData
    {
        public string selectedThemeId;
        public string selectedLanguageCode;
    }
}