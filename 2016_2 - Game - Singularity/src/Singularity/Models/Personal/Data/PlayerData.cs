using System;
using Singularity.IO;
using UnityEngine;

namespace Singularity.Personal.Data
{
    [System.Serializable]
    public class PlayerData
    {
        public PlayerScoreData      playerScoreData     = new PlayerScoreData();
    }
}