using System;
using System.Collections.Generic;
using Singularity.IO;
using UnityEngine;

namespace Singularity.Personal.Data
{
    [System.Serializable]
    public class PlayerScoreData
    {
        protected   int m_BestScore;
        public      int bestScore
        {
            get { return m_BestScore; }
            set
            {
                m_BestScore = Mathf.Abs(value);
            }
        }
    }
}