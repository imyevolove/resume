using System;
using System.IO;
using Singularity.IO;
using UnityEngine;

namespace Singularity.Personal.Data
{
    [System.Serializable]
    public class GameDataManager
    {
        /// <summary>
        /// Save file name
        /// </summary>
        public string saveFilename = "game";
        
        /// <summary>
        /// Save file extension
        /// </summary>
        public static readonly string saveFileExtension = "xo";

        /// <summary>
        /// Save file directory
        /// </summary>
        public static string saveFileDirectory 
        {
            get { return Storage.persistentDataPath + Path.DirectorySeparatorChar + "Saves"; }
        }
        
        /// <summary>
        /// Save file absolute path
        /// </summary>
        /// <returns></returns>
        public string filepath
        {
            get
            {
                return Path.Combine(saveFileDirectory, saveFilename + "." + saveFileExtension);
            }
        }

        // Hide game data from public scope
        protected   GameData m_gameData;
        
        /// <summary>
        /// Game data attached to this GameDataManager
        /// </summary>
        /// <returns></returns>
        public      GameData gameData
        {
            get
            {
                return m_gameData;
            }
            protected set
            {
                m_gameData = value;
            }
        }

        public GameDataManager()
        {
            Load();
        }

        /////////////////////////////////////////////////////////
        public void Save()
        {
            Storage.Save<GameData>(gameData, filepath);
        }

        /////////////////////////////////////////////////////////
        public void Save(string filepath)
        {
            Storage.Save<GameData>(gameData, filepath);
        }

        /////////////////////////////////////////////////////////
        public void Save(GameData gameData)
        {
            Storage.Save<GameData>(gameData, filepath);
        }

        /////////////////////////////////////////////////////////
        public void Save(GameData gameData, string filepath)
        {
            Storage.Save<GameData>(gameData, filepath);
        }

        /////////////////////////////////////////////////////////
        /// <summary>
        /// Load game data in current game data object
        /// </summary>
        /// <returns></returns>
        public void Load()
        {
            var data = Storage.Load<GameData>(filepath);

            if (data != null)
                m_gameData = data;

            if (m_gameData == null)
                m_gameData = new GameData();
        }

        /////////////////////////////////////////////////////////
        public void Load(string filepath)
        {
            var data = Storage.Load<GameData>(filepath);

            if (data != null)
                m_gameData = data;

            if (m_gameData == null)
                m_gameData = new GameData();
        }
    }
}