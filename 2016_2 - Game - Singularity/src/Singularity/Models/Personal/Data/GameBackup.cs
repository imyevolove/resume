﻿using Singularity.IO;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Singularity.Models
{
    [Serializable]
    public class GameBackup : BaseBackup
    {
        public List<int> gameBoardTileWeights = new List<int>();
        public List<int> pipelineTileWeights = new List<int>();
        
        public int minWeightLimitter;
        public int maxWeightLimitter;
        
        public int score;

        public List<float> skillRecoilCooldownTimeLeft = new List<float>();
    }
}