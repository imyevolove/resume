using System;
using System.Collections.Generic;
using Singularity.IO;
using UnityEngine;
using Singularity.IAPManagement;

namespace Singularity.Personal.Data
{
    [System.Serializable]
    public class PlayerPurchasesData
    {
        public List<string> purchases = new List<string>();

        ////////////////////////////////////////////////////////////
        public void RegisterPurchase(IProduct product)
        {
            if (purchases.Contains(product.productId))
                return;
            
            purchases.Add(product.productId);
        }

        ////////////////////////////////////////////////////////////
        public void UnregisterPurchase(IProduct product)
        {
            if (!purchases.Contains(product.productId))
                return;

            purchases.Remove(product.productId);
        }
    }
}