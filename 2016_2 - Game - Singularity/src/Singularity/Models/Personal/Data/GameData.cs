using System;
using Singularity.IO;
using UnityEngine;

namespace Singularity.Personal.Data
{
    [System.Serializable]
    public class GameData
    {
        //////////////////////////////////////////////
        public PlayerData playerData = new PlayerData();
        
        //////////////////////////////////////////////
        public PlayerSettingsData playerSettingsData = new PlayerSettingsData();
        
        //////////////////////////////////////////////
        public PlayerPurchasesData playerPurchasesData = new PlayerPurchasesData();
    }
}