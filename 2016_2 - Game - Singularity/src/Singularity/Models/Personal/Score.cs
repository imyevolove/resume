using System;
using UnityEngine;

namespace Singularity.Personal
{
    [System.Serializable]
    public class Score
    {
        protected int m_Value;

        public event ScoreEventHandler onChange;

        public int Value 
        {
            get
            { 
                return m_Value;
            }
        }

        public virtual void Set(int value)
        {
            m_Value = value;
            DispatchOnChangeEvent();
        }

        public virtual void Change(Score score)
        {
            m_Value = score.Value;
            DispatchOnChangeEvent();
        }

        public virtual int Add(int value)
        {
            this.m_Value += Mathf.Abs(value);
            DispatchOnChangeEvent();

            return m_Value;
        }

        public virtual int Reduce(int value)
        {
            this.m_Value -= Mathf.Abs(value);
            
            if(this.m_Value < 0)
                this.m_Value = 0;

            DispatchOnChangeEvent();

            return m_Value;
        }

        public virtual void Clear()
        {
            this.m_Value = 0;
            DispatchOnChangeEvent();
        }

        public virtual bool IsBetter(Score score)
        {
            return score.Value > m_Value;
        }

        protected void DispatchOnChangeEvent()
        {
            if(onChange != null)
                onChange(this);
        }
    }
}