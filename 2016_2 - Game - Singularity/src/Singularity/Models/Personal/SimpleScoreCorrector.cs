﻿using UnityEngine;

namespace Singularity.Personal
{
    public class SimpleScoreCorrector : BaseScoreCorrector
    {
        protected const float mult = 1.4f;
        protected const float bonus = 1.05f;
        protected int bonusCounter = 0;

        ////////////////////////////////////////////////////////////
        public override void Add(int weight, int count)
        {
            result += (int)(weight * count + Mathf.Pow(mult, weight + count));
        }

        ////////////////////////////////////////////////////////////
        public override int GetResult()
        {
            /// Add bonus. We love players
            return (int)(result * Mathf.Pow(bonus, bonusCounter));
        }

        ////////////////////////////////////////////////////////////
        public override void Reset()
        {
            base.Reset();
            bonusCounter = 0;
        }
    }
}
