﻿using UnityEngine;
using System;
using Singularity.Models.Entities;
using System.Collections.Generic;
using System.Collections;

namespace Singularity.Models
{
    public delegate void PipelineAction();

    [Serializable]
    public class TilePipeline : MonoBehaviour
    {
        [SerializeField]
        protected   int m_QueueLength; 
        public      int queueLength { get { return m_QueueLength; } }

        public event PipelineAction onChange;
        public event PipelineAction onReset;
        public TileGenratorWeightLimiter limiter = new TileGenratorWeightLimiter();

        protected Queue<TileData> queue = new Queue<TileData> ();

        protected void DispatchEvent(PipelineAction action)
        {
            if(action != null)
                action();
        }

        public void Generate()
        {
            queue.Clear();
            FillPool();
            DispatchEvent(onReset);
        }

        public void Reset()
        {
            limiter.minWeight = limiter.maxWeight = 0;
            Generate();
        }

        public void PushFirst(TileData data)
        {
            TileData[] arr = queue.ToArray();
            
            queue.Clear();
            queue.Enqueue(data);
            
            foreach(var dt in arr)
                queue.Enqueue(dt);

            DispatchEvent(onReset);
        }

        public virtual TileData Next()
        {
            // Get last item
            TileData resultTile = queue.Dequeue();
            
            // Add new items
            FillPool();
            DispatchEvent(onChange);

            return resultTile;
        }

        protected virtual void Awake()
        {
            FillPool();
        }

        public TileData GetTileFromPoolSilent(int index)
        {
            TileData[] tiles = queue.ToArray();
            index = Mathf.Clamp(index, 0, tiles.Length - 1);
            
            return tiles[index];
        }

        public TileData[] GetTilesFromPoolSilent(int count)
        {
            TileData[] tiles = queue.ToArray();
            count = Mathf.Clamp(count, 0, tiles.Length);
            
            TileData[] result = new TileData[count];
            Array.Copy(tiles, 0, result, 0, count);

            return result;
        }

        public void SetQueue(TileData[] data)
        {
            queue = new Queue<TileData>(data);
            DispatchEvent(onReset);
        }

        public TileData[] GetQueueArray()
        {
            return queue.ToArray();
        }

        private void FillPool()
        {
            int c = queueLength - queue.Count;
            if(c <= 0) return;

            while(c > 0)
            {
                queue.Enqueue(GetRandomTile());
                c--;
            }
        }

        private TileData GetRandomTile()
        {
            TileData tile = TileManager.Instance.GetRandomTileDataInRange(
                limiter.minWeight,
                limiter.maxWeight
            );

            if(tile == null)
                tile = TileManager.Instance.GetTileDataByIndex(0);

            return tile;
        }
    }
}