﻿using UnityEngine;
using System;
using Singularity.State;
using UnityEngine.UI;
using Singularity.Models.Entities;
using System.Collections.Generic;
using System.Collections;

namespace Singularity.Models
{
    [Serializable, RequireComponent(typeof(Animator))]
    public class Tile : MonoBehaviour
    {
        [SerializeField]
        public int Weight { get; protected set; }

        [HideInInspector]
        public Color Color { get; protected set; }

        [HideInInspector]
        public bool lockState = false;

        [SerializeField]
        protected Image image;

        [SerializeField]
        protected Text TileWeightText;

        [SerializeField]
        protected float EvaluateColorTime;

        protected void Update()
        {
            TileWeightText.text = Weight.ToString();
        }

        private IState currentState = States.IdleState;

        public int SetWeight(int value)
        {
            Weight = Mathf.Abs(value);
            return Weight;
        }

        public void SetColor(Color color)
        {
            StopCoroutine("EvaluateEnumerator");
            
            Color = color;
            image.color = color;
        }

         public void SetColorFade(Color color)
         {
            Color = color;
            StopCoroutine("EvaluateEnumerator");
            StartCoroutine(EvaluateEnumerator(image.color,color, EvaluateColorTime)); 
         }

        public virtual void Evaluate(TileData data)
        {
            SetWeight       (data.weight);
            SetColorFade    (data.color);
        }

        public void ApplyState(IState state)
        {
            if (lockState) return;

            Animator animator = GetComponent<Animator>();
            currentState.Dismiss(animator);
            currentState = state;
            currentState.Use(animator);
        }

        public virtual void Destroy()
        {
            GameObject.Destroy(gameObject);
        }

        public Tile InstantiateCopy()
        {
            Tile tile = UnityEngine.Object.Instantiate(this);
            tile.gameObject.SetActive(true);
            return tile;
        }

        public override bool Equals (object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            
            return ((Tile)obj).Weight == Weight;
        }
        
        // override object.GetHashCode
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        protected IEnumerator EvaluateEnumerator(Color from, Color to, float time)
        {
            float evaluateTime = 0;
            while(evaluateTime < time)
            {
                image.color = Color.Lerp(from, to, evaluateTime / time);
                evaluateTime += Time.deltaTime;
                yield return true;
            }
            image.color = to;
        }
        
    }
}