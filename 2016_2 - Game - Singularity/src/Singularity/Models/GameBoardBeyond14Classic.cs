﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Singularity.Models
{
    [Serializable]
    public sealed class GameBoardBeyond14Classic : GameBoard
    {
        [SerializeField]
        private GameObject m_GameBoardObject;

        [SerializeField]
        public  TileContainer m_PrefabGameTileContainer;

        private TileContainer[] m_TileContainers;

        protected override void Start()
        {
            base.Start();
            PrepareGameBoard();
        }

        public override bool RemoveTile(Tile tile)
        {
            bool removeStatus = base.RemoveTile(tile);

            if(removeStatus)
                tile.Destroy();

            return removeStatus;
        }

        public override bool RemoveTile(int x, int y)
        {
            Tile tile = GetTile(x, y);
            bool removeStatus = base.RemoveTile(x, y);

            if(removeStatus)
                tile.Destroy();

            return removeStatus;
        }

        public override bool InsertTile(Tile tile, int index)
        {
            bool status = base.InsertTile(tile, index);

            if(status)
                GetTileContainer(index).InsertTile(tile);

            return status;
        }

        public override bool InsertTile(Tile tile, int x, int y)
        {
            bool status = base.InsertTile(tile, x, y);

            if(status)
                GetTileContainer(x, y).InsertTile(tile);

            return status;
        }

        public TileContainer GetTileContainer(int x, int y)
        {
            return m_TileContainers[XYToIndex(x, y)];
        }

        public TileContainer GetTileContainer(int index)
        {
            return m_TileContainers[index];
        }

        public override void Clear()
        {
            base.Clear();
        }

        public override Tile[] GetExpectedReduceTiles(int x, int y)
        {
            int[] indexes = GetBoxIndexes(x, y, 1);
            List<Tile> expectedTiles = new List<Tile>();

            foreach(var index in indexes)
                if(tiles[index] != null)
                    expectedTiles.Add(tiles[index]);

            return expectedTiles.ToArray();
        }

        public override Tile[] GetExpectedReduceTiles(int index)
        {
            return GetExpectedReduceTiles(IndexToXY(index));
        }

        public override Tile[] GetExpectedReduceTiles(CellPtr cellPtr)
        {
            return GetExpectedReduceTiles(cellPtr.x, cellPtr.y);
        }

        private void PrepareGameBoard()
        {
            m_TileContainers = new TileContainer[Size.Length];

            // Delete all children
            foreach(Transform child in m_GameBoardObject.transform)
                GameObject.Destroy(child.gameObject);

            // Fill game board
            for(var i = 0; i < Size.Length; i++)
                InstantiateTileContainer(i);
        }

        private void InstantiateTileContainer(int index)
        {
            CellPtr         cellPtr         = IndexToXY(index);
            TileContainer   tileContainer   = GameObject.Instantiate<TileContainer>(
                    m_PrefabGameTileContainer, m_GameBoardObject.transform, false);
                    
            tileContainer.gameBoard = this;
            tileContainer.position  = cellPtr;
            
            m_TileContainers[index] = tileContainer;
        }

        private bool IndexInBox(Vector2 center, Vector2 point, int rayLength)
        {
            if(point.x < center.x - rayLength || point.x > center.x + rayLength)
                return false;

            if(point.y < center.y - rayLength || point.y > center.y + rayLength)
                return false;

            return true;
        }

        /// <summary>
        ///
        /// Formula incorrected and have errors
        /// </summary>
        /// <param name="center"></param>
        /// <param name="rayLength"></param>
        /// <returns></returns>
        private int[] GetBoxIndexes(Vector2 center, int rayLength)
        {
            return GetBoxIndexes((int)center.x, (int)center.y, rayLength);
        }

        /// <summary>
        /// ///
        /// Formula incorrected and have errors
        /// </summary>
        /// <param name="center"></param>
        /// <param name="rayLength"></param>
        /// <returns></returns>
        private int[] GetBoxIndexes(int centerX, int centerY, int rayLength)
        {
            int dia = rayLength * 2 + 1;
            List<int> indexes = new List<int> ();
            
            int currentIndex = 0,
                cursorX = 0,
                cursorY = 0;

            for(var y = 0; y < dia; y++)
            {
                cursorY = (centerY - 1) + y;

                // Out of vertical rect
                if(cursorY < 0 || cursorY >= Size.Y)
                    continue;
                
                for(var x = 0; x < dia; x++)
                {
                    cursorX = (centerX - 1) + x;

                    // Out of vertical rect
                    if(cursorX < 0 || cursorX >= Size.X)
                        continue;
                    
                    currentIndex = cursorY * Size.X + cursorX;
                    
                    indexes.Add(currentIndex);
                }
            }

            return indexes.ToArray();
        }

        public override void HighlightTilesAround(int x, int y)
        {
            int[] indexes = GetBoxIndexes(x, y, 1);
            foreach(var index in indexes)
                m_TileContainers[index].SetHighlight(true);
        }

        public override void HighlightTilesAround(int index)
        {
            CellPtr cellPtr = IndexToXY(index);
            HighlightTilesAround(cellPtr);
        }

        public override void HighlightTilesAround(CellPtr cellPtr)
        {
            HighlightTilesAround(cellPtr.x, cellPtr.y);
        }

        public override void DisableHighlight()
        {
            foreach(var container in m_TileContainers)
                container.SetHighlight(false);
        }
    }
}