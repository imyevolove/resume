using System;
using System.Linq;
using Singularity.Controllers.Entities;
using UnityEngine;

namespace Singularity.Models.Entities
{
    public class TileManager
    {
        private static TileManager m_Instance;
        public static TileManager Instance
        { 
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = new TileManager();
                }

                return m_Instance;
            }
        }

        public TileData[] availableTiles { get; protected set; }

        public TileManager()
        {
            availableTiles = TileController.Instance.GetAvailableTiles().ToArray();
            Array.Sort<TileData>(availableTiles, (tileA, tileB) => { return tileA.weight - tileB.weight; });
        }

        public Tile InstantiateTile(int weight)
        {
            TileData data = GetTileData(weight);

            Tile tile = TileController.Instance.GetTilePrefab().InstantiateCopy();
            tile.SetWeight(data.weight);
            tile.SetColor(data.color);
            
            return tile;
        }

        public Tile InstantiateTile(TileData tileData)
        {
            return InstantiateTile(tileData.weight);
        }

        public TileData GetTileDataByIndex(int index)
        {
            return availableTiles[index];
        }

        public TileData GetNextTileData(TileData tileData)
        {
            TileData sTileData = Array.Find<TileData>(availableTiles, td => { return td.weight == tileData.weight + 1; });
            if(sTileData == null)
                return new TileData(
                    sTileData.weight + 1,
                    TileController.Instance.GetUnregisteredTileData().color
                );
            return sTileData;
        }

        public TileData GetNextTileData(int weight)
        {
            TileData tileData = Array.Find<TileData>(availableTiles, td => { return td.weight == weight + 1; });
            if(tileData == null)
                return new TileData(
                    weight + 1,
                    TileController.Instance.GetUnregisteredTileData().color
                );
            return tileData;
        }

        public TileData GetTileData(int weight)
        {
            TileData tileData = Array.Find<TileData>(availableTiles, td => { return td.weight == weight; });
            if(tileData == null)
                return new TileData(
                    weight,
                    TileController.Instance.GetUnregisteredTileData().color
                );
            return tileData;
        }

        /// <summary>
        /// Return random tile data in range
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public TileData GetRandomTileDataInRange(int minWeight, int maxWeight)
        {
            TileData[] tiles = Array.FindAll<TileData>(availableTiles, 
                item => { return item.weight > minWeight && item.weight < maxWeight; });
            
            if(tiles.Length == 0)
                return null;
            
            return tiles[UnityEngine.Random.Range(0, tiles.Length)];
        }
    }
}

/*
    TileManager.Instance.GetAvailableTiles();
    TileManager.Instance.GetMaxTile();
    TileManager.Instance.GetMaxTile();
    TileManager.Instance.GetNextTile(Tile tile);
*/