using UnityEngine;

namespace Singularity.Models.Entities
{
    [ExecuteInEditMode]
    public class TileSizeFilter : MonoBehaviour
    {
        protected void OnTransformParentChanged()
        {
            UpdateProperties();
        }

        protected void Update()
        {
            /*
            if(transform.hasChanged)
            {
                UpdateProperties();
                transform.hasChanged = false;
            }
            */
        }

        protected void UpdateProperties()
        {
            if(transform.parent == null || transform.parent.GetType() != typeof(RectTransform))
                return;

            var rt = (RectTransform)transform;
            var rtp = (RectTransform)transform.parent;

            rt.anchorMin    = new Vector3(0.5f, 0.5f);
            rt.anchorMax    = new Vector3(0.5f, 0.5f);
            rt.pivot        = new Vector3(0.5f, 0.5f);
            rt.sizeDelta    = new Vector2(
                rtp.rect.width,
                rtp.rect.height
            );
        }
    }
}