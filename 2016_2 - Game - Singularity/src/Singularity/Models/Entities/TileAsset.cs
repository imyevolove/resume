﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.Entities
{
    [Serializable]
    public class TileAsset : MonoBehaviour
    {
        [SerializeField]
        public Color unregisteredTileColor;

        [SerializeField]
        public List<Color> tileColors;

        public Color GetTileColor(int weight)
        {
            var index = weight - 1;

            if (index < 0 || index > tileColors.Count - 1)
                return unregisteredTileColor;

            return tileColors[index];
        }
    }
}
