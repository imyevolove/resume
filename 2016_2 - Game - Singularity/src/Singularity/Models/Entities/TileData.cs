using System;
using UnityEngine;

namespace Singularity.Models.Entities
{
    [Serializable]
    public class TileData
    {
        [SerializeField]
        public int weight;

        [SerializeField]
        public Color color;

        public TileData(int weight, Color color)
        {
            this.weight = weight;
            this.color  = color;
        }

        public static TileData Create(Tile tile)
        {
            return new TileData(tile.Weight, tile.Color);
        }
    }
}