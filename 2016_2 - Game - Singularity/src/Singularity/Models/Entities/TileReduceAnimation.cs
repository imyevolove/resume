using System;
using Singularity.Models.Animation;
using Singularity.State;
using UnityEngine;

namespace Singularity.Models.Entities
{
    [Serializable]
    public class TileReduceAnimation : IndependentAnimation
    {
        public TileReduceAnimation(Tile tile, RectTransform destinationTransform)
		{
            RectTransform tileTransform = (RectTransform)tile.transform;
            Vector3 startPosition = tileTransform.position;

            onPauseEnd += () => {
                tile.ApplyState(States.ReduceState);
            };

			action = (time) => {
                tileTransform.position = Vector3.Lerp(startPosition, destinationTransform.position, time);
            };
		}
    }
}