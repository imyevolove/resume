using UnityEngine;

namespace Singularity.Iap
{
    public class PurchaseData
    {
        public string id { get { return m_Id; } }

        [SerializeField]
        private string m_Id;

        public PurchaseData() {}
        public PurchaseData(string id) 
        {
            m_Id = id;
        }
    }
}