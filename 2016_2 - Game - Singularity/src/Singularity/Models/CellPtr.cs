﻿namespace Singularity.Models
{
    public class CellPtr
    {
        public readonly int x;
        public readonly int y;

        public CellPtr(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}