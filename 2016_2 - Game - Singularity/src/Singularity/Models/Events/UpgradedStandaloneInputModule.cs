using UnityEngine;
using UnityEngine.EventSystems;

namespace Singularity.Models.Events
{
    public class UpgradedStandaloneInputModule : StandaloneInputModule
    {
        public PointerEventData GetPointerData()
        {
            return m_PointerData[kMouseLeftId];
        }
    }
}