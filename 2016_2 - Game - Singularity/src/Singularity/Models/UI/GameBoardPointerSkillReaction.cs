﻿using Singularity.Skills;
using Singularity.State;
using System;
using UnityEngine;

namespace Singularity.Models.UI
{
    [Serializable]
    public class GameBoardPointerSkillReaction : MonoBehaviour
    {
        [SerializeField]
        protected SkillManager skillManager;

        protected void Awake()
        {
            skillManager.onSkillAdd -= AddEventListeners;
            skillManager.onSkillRemove -= RemoveEventListeners;
            skillManager.onSkillAdd += AddEventListeners;
            skillManager.onSkillRemove += RemoveEventListeners;

            foreach (var skill in skillManager.skills)
                AddEventListeners(skill);
        }

        protected void AddEventListeners(BaseSkill skill)
        {
            skill.onPreUse += ActivateReaction;
            skill.onAbort += DeactivateReaction;
            skill.onUse += DeactivateReaction;
        }

        protected void RemoveEventListeners(BaseSkill skill)
        {
            skill.onPreUse -= ActivateReaction;
            skill.onAbort -= DeactivateReaction;
            skill.onUse -= DeactivateReaction;
        }

        protected void ActivateReaction()
        {
            Game.current.gameHandler.SetStateForAllTiles(States.ShakeState);
        }

        protected void DeactivateReaction()
        {
            Game.current.gameHandler.SetStateForAllTiles(States.IdleState);
        }

    }
}
