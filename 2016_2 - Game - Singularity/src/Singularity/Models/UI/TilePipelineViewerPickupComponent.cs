﻿using System;
using Singularity.Controllers;
using Singularity.Models.IO;
using Singularity.Models.IO.Events;
using Singularity.State;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Singularity.Models.UI
{
    [Serializable, RequireComponent(typeof(TilePipelineViewer), typeof(CanvasGroup))]
    public class TilePipelineViewerPickupComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        private CanvasGroup canvasGroup;
        private TilePipelineViewer tilePipelineViewer;

        private bool IsDragged = false;

        protected void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            tilePipelineViewer = GetComponent<TilePipelineViewer>();
        }

        protected void Update()
        {
            if(IsDragged)
                UpdateTilePosition();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if(IsDragged)
                return;

            DisableRaycastDetection();

            //transform.SetParent(GetParentCanvas().transform, true);
            
            IsDragged = true;
            InputManager.Instance.tileSelection = true;

            GetComponent<CanvasGroup>();
            
            if(tilePipelineViewer != null && tilePipelineViewer.tileInside != null)
                tilePipelineViewer.tileInside.ApplyState(States.DragState);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            InputManager.Instance.tileSelection = false;
            EnableRaycastDetection();

            TileContainer tileContainer = InputUtility.GetTileContainerFromEventData(eventData);
            if(tileContainer != null) 
            {
                InputManager.Instance.currentCell = tileContainer.position;
                InputManager.Instance.SetState(InputState.DragTile, tileContainer.GetTile());
                
                GameController.Instance.gameHandler.ExecuteCellAction();
            }
            
            IsDragged = false;

            GameController.Instance.gameHandler.DisableHighlight();
            tilePipelineViewer.MoveLocal(Vector2.zero);

            if (tilePipelineViewer != null && tilePipelineViewer.tileInside != null)
                tilePipelineViewer.tileInside.ApplyState(States.IdleState);
        }

        protected void UpdateTilePosition()
        {
            Vector3 position = Vector3.zero;

            switch(GetParentCanvas().renderMode)
            {
                case RenderMode.ScreenSpaceOverlay:
                RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)GetParentCanvas().transform, 
                    (Vector2)Input.mousePosition, null, out position);
                break;
                case RenderMode.ScreenSpaceCamera:
                position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                break;
            }
            
            //transform.position = position;
            tilePipelineViewer.MoveWorld(position);
        }

        protected Canvas GetParentCanvas()
        {
            Canvas[] c = GetComponentsInParent<Canvas>();
            Canvas topmost = c[c.Length-1];

            return topmost;
        }

        protected void EnableRaycastDetection()
        {
            canvasGroup.blocksRaycasts = true;
        }

        protected void DisableRaycastDetection()
        {
            canvasGroup.blocksRaycasts = false;
        }
    }
}