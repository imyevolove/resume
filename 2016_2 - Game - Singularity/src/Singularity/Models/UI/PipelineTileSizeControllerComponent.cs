﻿using System;
using UnityEngine;

namespace Singularity.Models.UI
{
    [Serializable, ExecuteInEditMode]
    public class PipelineTileSizeControllerComponent : MonoBehaviour
    {
        [SerializeField]
        public float firstElementSize;
        [SerializeField]
        public float otherElementSize;

        protected void Update()
        {
            CalculateSizes();
        }

        protected void CalculateSizes()
        {
            TilePipelineViewer[] tiles = transform.GetComponentsInChildren<TilePipelineViewer>();
            Array.Reverse(tiles);
            
            for(var i = 0; i < tiles.Length; i++) 
            {
                if(i == 0)
                    ModifyTileSize(tiles[i], firstElementSize);
                else
                    ModifyTileSize(tiles[i], otherElementSize);
            }
        }

        protected void ModifyTileSize(TilePipelineViewer tile, float size)
        {
            RectTransform rectTransform = (RectTransform)tile.transform;
            if(rectTransform == null) return;

            rectTransform.sizeDelta = new Vector2(size, size);
        }
    }
}