﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Singularity.Skills;
using System.Collections.Generic;

namespace Singularity.UI
{
    [Serializable]
    public class ManagedSkillsDrawer : MonoBehaviour
    {
        [SerializeField]
        protected SkillManager skillManager;

        [SerializeField]
        protected RectTransform container;

        [SerializeField]
        protected SkillButton skillButtonPrefab;

        protected List<SkillButton> buttons;

        protected void Awake()
        {
            skillManager.onSkillAdd     -= DrawSkill;
            skillManager.onSkillRemove  -= RemoveSkill;
            skillManager.onSkillAdd     += DrawSkill;
            skillManager.onSkillRemove  += RemoveSkill;

            buttons = new List<SkillButton>();

             // Remove all children
            foreach(Transform child in container.transform)
                GameObject.Destroy(child.gameObject);

            // Draw existing skills
            foreach(var skill in skillManager.skills)
                DrawSkill(skill);
        }

        protected void DrawSkill(BaseSkill skill)
        {
            SkillButton button = GameObject.Instantiate<SkillButton>(skillButtonPrefab, container.transform, false);
            button.SetTarget(skill);
            buttons.Add(button);
        }

        protected void RemoveSkill(BaseSkill skill)
        {
            SkillButton button = buttons.Find(b => { return b.target == skill; });
            if(button == null) return;
            
            button.Destroy();
            buttons.Remove(button);
        }
    }
}