using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.Models.UI
{
    [Serializable, ExecuteInEditMode, RequireComponent(typeof(RectTransform))]
    public class PipelineVerticalGroupLayout : MonoBehaviour
    {
        [SerializeField]
        public float transitionSpeed;

        [SerializeField]
        public Vector2 elementSize;

        [SerializeField]
        public float elementScale;

        [SerializeField]
        public float spacing;

        [SerializeField]
        public HAlignment alignmentHorizontal;

        public enum HAlignment
        {
            Left,
            Right,
            Center
        }

        private List<Transform> CorountineSchedule = new List<Transform>();

        public void ForceRefresh()
        {
            StopAllCoroutines();
            CorountineSchedule.Clear();
            UpdateAll();
        }

        protected void Start()
        {
            UpdateAll();
        }

        protected void OnTransformChildrenChanged()
        {
            ForceRefresh();
        }

        protected void Update()
        {
            #if UNITY_EDITOR
            //UpdateAllFaster();
            #endif
        }

        protected void UpdateAllFaster()
        {
            var index = -1;
            var count = transform.childCount;

            foreach(Transform child in transform)
            {
                index++;

                ((RectTransform)child).anchoredPosition = GetPosition(index);
                ((RectTransform)child).sizeDelta  = elementSize * GetScale(index);
            }
        }

        protected void UpdateAll()
        {
            var index = -1;
            var count = transform.childCount;
            var pattern = new Vector2(1f, 0);
            
            Vector2 newPosition;
            float newScale;
            RectTransform rt;

            foreach(Transform child in transform)
            {
                index++;
                
                if(CorountineSchedule.Find(c => c == child) != null)
                    continue;

                rt = (RectTransform)child;
                rt.anchorMin = pattern;
                rt.anchorMax = pattern;
                rt.pivot     = pattern;

                rt.sizeDelta = elementSize;

                newPosition = GetPosition(index);
                newScale = GetScale(index);

                //rt.anchoredPosition = new Vector2(
                //    newPosition.x,
                //    newPosition.y
                //);

                CorountineSchedule.Add(child);
                StartCoroutine(UpdateChieldParameters( rt, newPosition, 
                    new Vector3(newScale, newScale, newScale), transitionSpeed * Time.deltaTime));
            }
        }

        protected float GetScale(int index)
        {
            return (index == 0 ? 1 : elementScale);
        }

        protected Vector2 GetPosition(int index)
        {
            bool one = index - 1 < 0;
            float position = 0;

            if(!one)
            {
                position = elementSize.y
                 + (index - 1) * (elementSize.y * elementScale) // another size
                 + index * spacing; // all spaces
            }

            return new Vector2(0, position);
        }

        protected IEnumerator UpdateChieldParameters(RectTransform target, Vector2 position, Vector3 scale, float time)
        {
            float elapsedTime = 0;
            Vector3 startingPos = target.anchoredPosition;
            Vector3 startingSize = target.sizeDelta;

            while (elapsedTime < time)
            {
                target.anchoredPosition = Vector2.Lerp(startingPos, position, (elapsedTime / time));
                target.localScale = Vector3.Lerp(target.localScale, scale, (elapsedTime / time));
                elapsedTime += Time.deltaTime;
                yield return true;
            }

            target.anchoredPosition = position;
            target.localScale = scale;
            
            CorountineSchedule.Remove((Transform)target);
        }
    }
}