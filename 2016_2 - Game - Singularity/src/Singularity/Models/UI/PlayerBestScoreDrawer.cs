﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Singularity.Skills;
using System.Collections.Generic;
using Singularity.Personal;

namespace Singularity.UI
{
    [Serializable]
    public class PlayerBestScoreDrawer : MonoBehaviour
    {
        [SerializeField]
        protected Text scoreText;

        protected int lastBestScore;

        public void Start()
        { 
            UpdateText(Game.current.player.bestScore);
            UpdateText(Game.current.gameHandler.score);
            
            Game.current.gameHandler.score.onChange += UpdateText;
            Game.current.player.bestScore.onChange += UpdateText;
        }

        protected void UpdateText(Score score)
        {
            if(scoreText == null) return;
            if(lastBestScore > score.Value) return;
            
            lastBestScore = score.Value;
            scoreText.text = score.Value.ToString();
        }
    }
}