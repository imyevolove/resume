﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Singularity.Skills;
using System.Collections.Generic;
using System.Collections;
using Singularity.Models.IO;

namespace Singularity.UI
{
    public delegate void SkillButtonEventHandler();

    public enum SkillButtonState
    {
        Available,
        Recoil,
        Selected
    }

    [Serializable]
    public class SkillButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        protected Image buttonImage;

        [SerializeField]
        protected Image iconImage;

        [SerializeField]
        protected Image recoilImage;

        [SerializeField]
        protected Animator animator;

        [SerializeField]
        protected float fadeSpeed;

        [HideInInspector]
        public BaseSkill target { get; protected set; }

        public SkillButtonState state { get; protected set; }

        ////////////////////////////////////////////////////////////
        public void SetTarget(BaseSkill skill)
        {
            target = skill;
            iconImage.sprite = skill.icon;

            SetState(SkillButtonState.Available);

            skill.onPreUse += ApplySelectedStyle;
            skill.onAbort += ApplyAvailableStyle;
            skill.recoilHandler.onRecoilFinish += ApplyAvailableStyle;
            skill.recoilHandler.onRecoilStart += ApplyRecoilStyle;
            skill.recoilHandler.onRecoilTick += UpdateRecoilProgress;
        }

        ////////////////////////////////////////////////////////////
        public bool IsActive
        {
            get { return animator.GetBool("active"); }
        }

        ////////////////////////////////////////////////////////////
        public void SetState(SkillButtonState state)
        {
            this.state = state;
            Redraw();
        }

        ////////////////////////////////////////////////////////////
        protected void ApplyAvailableStyle()
        {
            SetState(SkillButtonState.Available);
        }

        ////////////////////////////////////////////////////////////
        protected void ApplyRecoilStyle()
        {
             SetState(SkillButtonState.Recoil);
        }

        ////////////////////////////////////////////////////////////
        protected void ApplySelectedStyle()
        {
            SetState(SkillButtonState.Selected);
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateRecoilProgress()
        {
            SetRecoilProgress(target.recoilHandler.cooldownDeltaTime);
        }

        ////////////////////////////////////////////////////////////
        protected void Redraw()
        {
            switch(state)
            {
                case SkillButtonState.Available:
                    recoilImage.fillAmount = 360;
                    animator.SetBool("active", true);
                    animator.SetBool("selected", false);
                break;
                case SkillButtonState.Recoil:
                    recoilImage.fillAmount = 0;
                    animator.SetBool("active", false);
                    animator.SetBool("selected", false);
                break;
                case SkillButtonState.Selected:
                    animator.SetBool("selected", true);
                break;
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Update recoil ui progress
        /// min 0 - max 1
        /// </summary>
        /// <param name="progress"></param>
        public void SetRecoilProgress(float progress)
        {
            progress = Mathf.Clamp01(progress);
            StopCoroutine("AnimRecoilProgress");
            StartCoroutine(AnimRecoilProgress(progress));
        }

        ////////////////////////////////////////////////////////////
        public IEnumerator AnimRecoilProgress(float value)
        {
            while(recoilImage.fillAmount < value)
            {
                recoilImage.fillAmount += Time.deltaTime * fadeSpeed;
                
                if(recoilImage.fillAmount > value)
                    recoilImage.fillAmount = value;
                    
                yield return true;
            }
            recoilImage.fillAmount = value;
        }

        ////////////////////////////////////////////////////////////
        public void OnPointerClick(PointerEventData eventData)
        {
            if (target == null) return;
            if (!target.isReady) return;
            if (InputManager.Instance.target is BaseSkill 
                && (BaseSkill)InputManager.Instance.target != target) return;
            target.Use(null);
        }

        ////////////////////////////////////////////////////////////
        public void Destroy()
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}