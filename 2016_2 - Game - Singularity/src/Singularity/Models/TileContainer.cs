﻿using System;
using Singularity.Models.IO.Events;
using Singularity.Controllers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Singularity.Models.IO;
using System.Collections;
using UnityEngine.UI;
using Singularity.ThemeManagement;

namespace Singularity.Models
{
    
    [Serializable]
    public class    TileContainer 
                :   MonoBehaviour, 
                    IPointerClickHandler, 
                    IPointerDownHandler, 
                    IPointerUpHandler,
                    IPointerEnterHandler,
                    IPointerExitHandler
    {
        [SerializeField]
        protected float offsetHightlight;

        [SerializeField]
        protected float offsetNormal;

        [SerializeField]
        protected RectTransform container;

        [HideInInspector]
        protected Image image;

        [HideInInspector]
        public GameBoard    gameBoard;
        public CellPtr      position;

        protected void Start()
        {
            image = GetComponent<Image>();
            SetHighlight(false);
        }

        public void Clear()
        {
            // Delete all children
            foreach(Transform child in transform)
                GameObject.Destroy(child.gameObject);
        }

        public bool CanInsertTile()
        {
            return transform.childCount == 0;
        }

        public void InsertTile(Tile tile)
        {
            tile.transform.SetParent(container, false);

            RectTransform rt = (RectTransform)tile.transform;
            rt.anchorMin = Vector2.zero;
            rt.anchorMax = Vector2.one;
            rt.sizeDelta = Vector2.zero;
        }

        public Tile GetTile()
        {
            return transform.GetComponentInChildren<Tile>();
        }

        public void SetHighlight(bool status)
        {
            StopHightlightEnumerators();
            StartCoroutine(HightlightEnumerator(
                status ? offsetHightlight : offsetNormal, 
                status  ? ThemeManager.current.gmEmptyCellHLColor  
                        : ThemeManager.current.gmEmptyCellColor, 
                0.1f
                ));
                
        }

        public void OnPointerClick(PointerEventData data)
        {
            if(InputManager.Instance.state == InputState.None)
            {
                InputManager.Instance.currentCell = position;
                InputManager.Instance.SetState(InputState.DragTile, GetTile());
            }

            GameController.Instance.gameHandler.ExecuteCellAction();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            InputManager.Instance.tileSelection = false;
            StopCoroutine("DelayTryHightlight");
            GameController.Instance.gameHandler.DisableHighlight();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            InputManager.Instance.tileSelection = true;
            StartCoroutine(DelayTryHightlight(0.1f));
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            InputManager.Instance.currentCell = position;
            if(!InputManager.Instance.tileSelection && !InputManager.Instance.isCurrentTileTarget) return;
            TryHightlight();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            InputManager.Instance.currentCell = null;
            //gameBoard.DisableHighlight();
        }

        protected void TryHightlight()
        {
            var inputManager = InputManager.Instance;

            if(inputManager.state != InputState.DragTile && inputManager.state != InputState.None)
                return;

            if(GetTile() != null)
            {
                gameBoard.DisableHighlight();
                return;
            }

            gameBoard.DisableHighlight();
            gameBoard.HighlightTilesAround(position);
        }

        protected IEnumerator DeferredHightlightEnumerator(float deferTime)
        {
            yield return new WaitForSeconds(deferTime);
            StartCoroutine(HightlightEnumerator(offsetHightlight, ThemeManager.current.gmEmptyCellHLColor, 0.1f));
        }

        protected IEnumerator DelayTryHightlight(float deferTime)
        {
            yield return new WaitForSeconds(deferTime);
            TryHightlight();
        }

        protected IEnumerator HightlightEnumerator(float offset, Color color, float time)
        {
            /// It's amazing breaking) I love my code -_- 
            /// Do you like this shit?
            /// Catch container exists? I don't think so...
            if (image == null)
            {
                SetContainerOffset(offset);
                yield break;
            }

            float elapsedTime = 0;
            float startOffset = Mathf.Abs(container.offsetMin.x);
            Color startColor = image.color;

            while(elapsedTime < time)
            {
                var t = elapsedTime / time;
                SetContainerOffset(Mathf.Lerp(startOffset, offset, t));
                image.color = Color.Lerp(startColor, color, t);
                elapsedTime += Time.deltaTime;
                yield return true;
            }

            SetContainerOffset(offset);
            image.color = color;
        }

        protected void DismissHightlight()
        {
            StartCoroutine(HightlightEnumerator(offsetNormal, ThemeManager.current.gmEmptyCellColor, 0.1f));
        }

        protected void StopHightlightEnumerators()
        {
            StopCoroutine("DeferredHightlightEnumerator");
            StopCoroutine("HightlightEnumerator");
        }

        protected void SetContainerOffset(float offset)
        {
            container.offsetMin = new Vector2(offset, offset);
            container.offsetMax = new Vector2(-offset, -offset);
        }
    }
}