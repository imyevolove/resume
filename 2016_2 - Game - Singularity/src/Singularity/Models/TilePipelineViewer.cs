﻿using UnityEngine;
using System;
using UnityEngine.Events;
using Singularity.Models.Entities;

namespace Singularity.Models
{
    [Serializable]
    public class TilePipelineViewer : MonoBehaviour
    {
        public event UnityAction onDestroy;

        [SerializeField]
        public Transform insideContainer;

        [HideInInspector]
        public TileData tileData;

        [HideInInspector]
        public TilePipeline pipeline;

        [HideInInspector]
        public int index;

        [HideInInspector]
        public Tile tileInside;

        ////////////////////////////////////////////////////////////
        public void Start()
        {
            tileInside = TileManager.Instance.InstantiateTile(tileData);
            tileInside.transform.SetParent(insideContainer, false);
        }

        ////////////////////////////////////////////////////////////
        public void Destroy()
        {
            Destroy(this.gameObject);
            Destroy(this);

            if(onDestroy != null)
                onDestroy();
        }

        ////////////////////////////////////////////////////////////
        public void MoveWorld(Vector2 position)
        {
            ((RectTransform)insideContainer.transform).position = position;
        }

        ////////////////////////////////////////////////////////////
        public void MoveLocal(Vector2 position)
        {
            ((RectTransform)insideContainer.transform).anchoredPosition = position;
        }

        ////////////////////////////////////////////////////////////
        public void UnregisterAndDestroy()
        {
            pipeline.Next();
            Destroy();
        }
    }
}