using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Singularity.Controllers.Entities;
using Singularity.IO;
using Singularity.Models.Animation;
using Singularity.Models.Entities;
using Singularity.Models.IO;
using Singularity.Personal;
using Singularity.Skills;
using Singularity.State;
using Singularity.UI;
using UnityEngine;
using Singularity.Audio;

namespace Singularity.Models
{
    public delegate void GameHandlerEventHandler();

    [Serializable]
    public class GameHandler
    {
        [SerializeField]
        protected SkillManager skillManager;

        [SerializeField]
        protected AudioQueuePlayer audioQueuePlayer;

        [SerializeField]
        protected AudioSource audioTilePlace;

        [SerializeField]
        protected GameBoard gameBoard;
        //public GameBoard GameBoard { get { return gameBoard; } }

        [SerializeField]
        protected TilePipeline m_TilePipeline;
        public TilePipeline pipeline { get { return m_TilePipeline; }}
        //public TileConveyorBase TileConveyorBase { get { return tileConveyor; } }

        [SerializeField]
        protected PipelinePoolVisualizer m_PipelinePoolVisualizer;
        public PipelinePoolVisualizer pipelinePoolVisualizer { get { return m_PipelinePoolVisualizer; } }

        /// <summary>
        /// Triggered when can be make next move
        /// </summary>
        public event GameHandlerEventHandler onMoveNext;

        [HideInInspector]
        public bool isFrozen { get; protected set; }

        public const string backupName = "game";

        /// <summary>
        /// Current game score
        /// </summary>
        public Score score { get; protected set; }

        protected BaseScoreCorrector scoreCorrector = new SimpleScoreCorrector();

        public GameHandler()
        {
            score = new Score();
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Freeze game operations
        /// </summary>
        public void Freeze()
        {
            isFrozen = true;
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Unfreeze game operations
        /// </summary>
        public void Unfreeze()
        {
            isFrozen = false;
        }

        ////////////////////////////////////////////////////////////
        public void DisableHighlight()
        {
            gameBoard.DisableHighlight();
        }


        ////////////////////////////////////////////////////////////
        public void LoadBackup()
        {
            LoadBackup(BackupManager.LoadBackup<GameBackup>(backupName));
        }

        ////////////////////////////////////////////////////////////
        public void LoadBackup(GameBackup backup)
        {
            if (backup == null)
            {
                Restart();
                return;
            }

            Clear();

            // Apply score
            score.Set(backup.score);

            // Load game board
            for(var tileIndex = 0; tileIndex < backup.gameBoardTileWeights.Count; tileIndex++)
            {
                var weight = backup.gameBoardTileWeights[tileIndex];
                if(weight <= 0) continue;
                InsertAndInstantiateTile(TileManager.Instance.GetTileData(weight), tileIndex, true);
            }

            // Load pipeline
            List<TileData> queueTiles = new List<TileData>();
            for(var tileIndex = 0; tileIndex < backup.pipelineTileWeights.Count; tileIndex++)
            {
                var weight = backup.pipelineTileWeights[tileIndex];
                if(weight <= 0) continue;
                queueTiles.Add(TileManager.Instance.GetTileData(weight));
            }

            if(queueTiles.Count < pipeline.queueLength)
            {
                pipeline.Generate();
            }
            else
            {
                pipeline.SetQueue(queueTiles.ToArray());
            }

            // Load limitter
            pipeline.limiter.minWeight = backup.minWeightLimitter;
            pipeline.limiter.maxWeight = backup.maxWeightLimitter;

            // Load skill recoils
            IEnumerator<BaseSkill> skillEnumerator  = skillManager.skills.GetEnumerator();
            IEnumerator<float> recoilEnumerator = backup.skillRecoilCooldownTimeLeft.GetEnumerator();

            while(skillEnumerator.MoveNext())
            {
                recoilEnumerator.MoveNext();
                if(recoilEnumerator == null) break;

                var skill = (BaseSkill)skillEnumerator.Current;
                if(skill == null) continue;

                skill.recoilHandler.SetCooldownTimeLeft(recoilEnumerator.Current);
            }

        }

        ////////////////////////////////////////////////////////////
        public GameBackup CreateBackup()
        {
            GameBackup backup = new GameBackup();
            
            // Write Score
            backup.score = score.Value;
            
            // Write gameBoard
            foreach(var tile in gameBoard.GetTiles())
                backup.gameBoardTileWeights.Add(tile == null ? -1 : tile.Weight);

            // Write pipeline
            foreach(var tileData in pipeline.GetQueueArray())
                backup.pipelineTileWeights.Add(tileData == null ? -1 : tileData.weight);

            // Write limitter
            backup.minWeightLimitter = pipeline.limiter.minWeight;
            backup.maxWeightLimitter = pipeline.limiter.maxWeight;

            // Write skill cooldowns
            foreach(var skill in skillManager.skills)
                backup.skillRecoilCooldownTimeLeft.Add(skill.recoilHandler.cooldownTimeLeft);

            return backup;
        }

        ////////////////////////////////////////////////////////////
        public void RemoveBackup()
        {
            BackupManager.RemoveBackup(backupName);
        }

        ////////////////////////////////////////////////////////////
        public void SaveBackup()
        {
            BackupManager.SaveBackup<GameBackup>(CreateBackup(), backupName);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Main game steps logic
        /// </summary>
        public void NextLogicIteration(int x, int y)
        {
            bool        canMakeStep = false;
            Tile        tileTrigger = gameBoard.GetTile(x, y);
            Tile[]      tiles = gameBoard.GetExpectedReduceTiles(x, y);
            List<Tile>  activeTiles = new List<Tile>();
            
            MultiIndependentAnimation multiIndependentAnimation = new MultiIndependentAnimation();

            if(tileTrigger == null)
            {
                Debug.LogWarning("Tile trigger not found");
                return;
            }

            foreach (var tile in tiles)
            {
                // avoid itself
                if(tileTrigger == tile)
                    continue;

                // apply
                if(tileTrigger.Equals(tile))
                {
                    activeTiles.Add(tile);
                    canMakeStep = true;
                    Freeze();
                }
            }

            if(canMakeStep)
            {
                var tIndex = 0;
                foreach(var tile in activeTiles)
                {
                    var anim = GenerateTileAnimation(tile, tileTrigger, tIndex);
                    multiIndependentAnimation.AddAnimation(anim);
                    tIndex++;
                }

                multiIndependentAnimation.onEnd += delegate {
                    //score.Add(tileTrigger.Weight);
                    scoreCorrector.Add(tileTrigger.Weight, activeTiles.Count + 1);

                    TileData nextTile = TileManager.Instance.GetNextTileData(tileTrigger.Weight);
                    tileTrigger.Evaluate(nextTile);

                    if(nextTile.weight > m_TilePipeline.limiter.maxWeight)
                        m_TilePipeline.limiter.maxWeight = nextTile.weight;

                    Unfreeze();
                    NextLogicIteration(x, y);
                };

                multiIndependentAnimation.onStart += delegate {
                    //audioQueuePlayer.PlayNext();
                };

                multiIndependentAnimation.Perform();
            }  
            else 
            {
                score.Add(scoreCorrector.GetResult());
                scoreCorrector.Reset();

                audioQueuePlayer.Reset();

                if (onMoveNext != null) 
                    onMoveNext();

                SaveBackup();

                if(!gameBoard.hasEmptyCell)
                {
                    Freeze();
                    GameEnd();
                }
            } 
        }

        ////////////////////////////////////////////////////////////
        private void GameEnd()
        {
            // Write best score
            bool hasNewBestScore = Game.current.player.SetBestScoreIfNeeded(score);

            // Save data
            Game.current.Save();

            //
            LeaderboardController.ReportBestScore(score.Value);

            // Remove backup for prevent loading latest game
            RemoveBackup();

            //
            GameEndController gameEndController = GameEndController.Instance;
            gameEndController.SetScore(score.Value);
            gameEndController.SetNewBestScorePanleVisibility(hasNewBestScore);
            gameEndController.SetVisibility(true);
        }

        ////////////////////////////////////////////////////////////
        private IndependentAnimation GenerateTileAnimation(Tile tile, Tile tileTrigger, int index)
        {
            var anim = new TileReduceAnimation(tile, (RectTransform)tileTrigger.transform);
            anim.time = 0.2f;
            anim.pause = (index + 1) * 0.1f;

            anim.onEnd += delegate 
            {
                gameBoard.RemoveTile(tile);
            };

            anim.onStart += delegate 
            {
                audioTilePlace.Stop();
                audioQueuePlayer.PlayNext();
            };

            return anim;
        }

        ////////////////////////////////////////////////////////////
        public void NextLogicIteration(int index)
        {
            NextLogicIteration(gameBoard.IndexToXY(index));
        }

        ////////////////////////////////////////////////////////////
        public void NextLogicIteration(CellPtr cellPtr)
        {
            NextLogicIteration(cellPtr.x, cellPtr.y);
        }

        ////////////////////////////////////////////////////////////
        public Tile GetTile(int x, int y)
        {
            return gameBoard.GetTile(x, y);
        }

        ////////////////////////////////////////////////////////////
        public Tile GetTile(int index)
        {
            return gameBoard.GetTile(index);
        }

        ////////////////////////////////////////////////////////////
        public Tile GetTile(CellPtr cellPtr)
        {
            return gameBoard.GetTile(cellPtr.x, cellPtr.y);
        }

        ////////////////////////////////////////////////////////////
        public bool RemoveTile(Tile tile)
        {
            return gameBoard.RemoveTile(tile);
        }

        ////////////////////////////////////////////////////////////
        public void SetStateForAllTiles(IState state)
        {
            foreach(var tile in gameBoard.GetTiles())
            {
                if(tile == null)
                    continue;

                tile.ApplyState(state);
            }
        }

        ////////////////////////////////////////////////////////////
        public bool InsertTile(Tile tile, int x, int y, bool preventNextLogicIteration = false)
        {
            bool status = gameBoard.InsertTile(tile, x, y);
            if(status && preventNextLogicIteration) NextLogicIteration(x, y);

            if (status)
            {
                audioTilePlace.Play();
            }

            return status;
        }

        ////////////////////////////////////////////////////////////
        public bool InsertTile(Tile tile, int index, bool preventNextLogicIteration = false)
        {
            return InsertTile(tile, gameBoard.IndexToXY(index), preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        public bool InsertTile(Tile tile, CellPtr cellPtr, bool preventNextLogicIteration = false)
        {
            return InsertTile(tile, cellPtr.x, cellPtr.y, preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return status insert and instantiate operation for given x and y position
        /// </summary>
        /// <param name="tilePrefab"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool InsertAndInstantiateTile(TileData tileData, int x, int y, bool preventNextLogicIteration = false)
        {
            if(!gameBoard.CanInsert(x, y))
                return false;
            
            Tile tile = TileManager.Instance.InstantiateTile(tileData);
            bool insertStatus = gameBoard.InsertTile(tile, x, y);
            
            // 
            if(!insertStatus)
                GameObject.Destroy(tile.gameObject);
            else if(!preventNextLogicIteration)
                NextLogicIteration(x, y);

            if (insertStatus)
            {
                audioTilePlace.Play();
            }

            return insertStatus; 
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return status insert and instantiate operation for given index position
        /// </summary>
        /// <param name="tilePrefab"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool InsertAndInstantiateTile(TileData tileData, int index, bool preventNextLogicIteration = false)
        {
            return InsertAndInstantiateTile(tileData, gameBoard.IndexToXY(index), preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return status insert and instantiate operation for given cell pointer
        /// </summary>
        /// <param name="tilePrefab"></param>
        /// <param name="cellPointer"></param>
        /// <returns></returns>
        public bool InsertAndInstantiateTile(TileData tileData, CellPtr cellPointer, bool preventNextLogicIteration = false)
        {
            return InsertAndInstantiateTile(tileData, cellPointer.x, cellPointer.y, preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return status auto insert and instantiate 
        /// taken from pipeline operation for given x and y position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool InsertAndInstantiateAutoTile(int x, int y, bool preventNextLogicIteration = false)
        {
            return InsertAndInstantiateTile(m_TilePipeline.Next(), x, y, preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return status auto insert and instantiate 
        /// taken from pipeline operation for given index position
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool InsertAndInstantiateAutoTile(int index, bool preventNextLogicIteration = false)
        {
            return InsertAndInstantiateTile(m_TilePipeline.Next(), gameBoard.IndexToXY(index), preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return status auto insert and instantiate 
        /// taken from pipeline operation for given cell pointer
        /// </summary>
        /// <param name="cellPointer"></param>
        /// <returns></returns>
        public bool InsertAndInstantiateAutoTile(CellPtr cellPointer, bool preventNextLogicIteration = false)
        {
            return InsertAndInstantiateTile(m_TilePipeline.Next(), cellPointer.x, cellPointer.y, preventNextLogicIteration);
        }

        ////////////////////////////////////////////////////////////
        public void ExecuteCellAction()
        {
            CellPtr cellPtr = InputManager.Instance.currentCell;
            if(InputManager.Instance.state == InputState.DragSkill)
            {
                BaseSkill skill = (BaseSkill)InputManager.Instance.target;
                
                if(skill != null)
                    skill.Use(cellPtr);

                InputManager.Instance.SetState(InputState.None, null);
                
            }
            else if(!isFrozen && gameBoard.CanInsert(cellPtr.x, cellPtr.y))
            {
                InsertAndInstantiateAutoTile(cellPtr);
            }

            InputManager.Instance.ResetData();
        }

        ////////////////////////////////////////////////////////////
        public void Init()
        {
            gameBoard.onCellClick += (cellPtr, obj) => {
                ExecuteCellAction();
            };
        }

        ////////////////////////////////////////////////////////////
        public void Clear()
        {
            score.Clear();
            DisableHighlight();
            gameBoard.Clear();
            skillManager.Reset();
            pipeline.Reset();
            Unfreeze();
        }

        ////////////////////////////////////////////////////////////
        public void Restart()
        {
            RemoveBackup();
            Clear();
        }
    }
}