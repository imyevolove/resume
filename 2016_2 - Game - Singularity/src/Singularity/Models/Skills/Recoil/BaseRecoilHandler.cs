﻿using System;
using UnityEngine;

namespace Singularity.Skills.Recoil
{
    [Serializable]
    public abstract class BaseRecoilHandler : MonoBehaviour
    {
        [SerializeField]
        private float m_CooldownTime;
        public  float cooldownTime
        {
            get { return m_CooldownTime; }
            protected set { m_CooldownTime = value; }
        }

        public float cooldownDeltaTime  { get { return 1 - cooldownTimeLeft / cooldownTime; } }
        public float cooldownTimeLeft  { get; protected set; }
        
        public event RecoilHandlerEventHandler onRecoilStart;
        public event RecoilHandlerEventHandler onRecoilTick;
        public event RecoilHandlerEventHandler onRecoilFinish;

        public abstract void RunRollback();
        public abstract void Rollback();

        public void SetCooldownTimeLeft(float time)
        {
            if(time == 0) return;

            RunRollback();
            cooldownTimeLeft = Math.Abs(time);
            DispatchOnRecoilTickEvent();
        }

        protected void DispatchOnRecoilStartEvent()
        {
            DispatchEvent(onRecoilStart);
        }

        protected void DispatchOnRecoilTickEvent()
        {
            DispatchEvent(onRecoilTick);
        }

        protected void DispatchOnRecoilFinishEvent()
        {
            DispatchEvent(onRecoilFinish);
        }

        private void DispatchEvent(RecoilHandlerEventHandler eventHandler)
        {
            if(eventHandler == null) return;
            eventHandler();
        }
    }
}