﻿using System;
using Singularity.Controllers;
using UnityEngine;

namespace Singularity.Skills.Recoil
{
    [Serializable]
    public class RecoilHandlerGameMoveEnd : BaseRecoilHandler
    {

        public override void RunRollback()
        {
            // Un / Subscribe on gameHandler event
            GameController.Instance.gameHandler.onMoveNext -= NextIteration;
            GameController.Instance.gameHandler.onMoveNext += NextIteration;

            // Reset timer
            cooldownTimeLeft = cooldownTime;

            //
            DispatchOnRecoilStartEvent();
        }

        public override void Rollback()
        {
            GameController.Instance.gameHandler.onMoveNext -= NextIteration;
            DispatchOnRecoilFinishEvent();
        }

        protected void NextIteration()
        {
            if(cooldownTimeLeft <= 0)
            {
                Rollback();
                return;
            }

            cooldownTimeLeft -= 1;
            DispatchOnRecoilTickEvent();
        }
    }
}