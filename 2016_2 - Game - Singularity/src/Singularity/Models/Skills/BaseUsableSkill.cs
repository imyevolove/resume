﻿using System;
using Singularity.Controllers;
using Singularity.Models;
using Singularity.Models.IO;
using Singularity.State;
using UnityEngine;

namespace Singularity.Skills
{
    [Serializable]
    public abstract class BaseUsableSkill : BaseSkill
    {
        public override void Use(object target)
        {
            DispatchPreUseEvent();
            base.Use(target);
        }
    }
}