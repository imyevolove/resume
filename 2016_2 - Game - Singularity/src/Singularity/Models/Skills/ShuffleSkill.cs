﻿using System.Collections;
using System.Collections.Generic;
using Singularity.Controllers;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.Models.IO;
using Singularity.State;
using UnityEngine;

namespace Singularity.Skills
{
    public class ShuffleSkill : BaseUsableSkill
    {
        [SerializeField]
        protected float waitDuration;

        // Do nothing
        protected override void AbortAction() {}

        protected override bool UseAction(object target)
        {
            var gameHandler = GameController.Instance.gameHandler;
            gameHandler.pipeline.Generate();
            
            return true;
        }
    }
}