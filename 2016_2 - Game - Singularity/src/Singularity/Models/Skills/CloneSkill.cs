﻿using System.Collections;
using System.Collections.Generic;
using Singularity.Controllers;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.Models.IO;
using Singularity.State;
using UnityEngine;

namespace Singularity.Skills
{
    public class CloneSkill : BasePointerSkill
    {
        [SerializeField]
        protected float waitDuration;

        // Do nothing
        protected override void AbortAction() {}

        protected override bool UseAction(object target)
        {
            // Cast
            CellPtr cellPtr = (CellPtr)target;
            if(cellPtr == null) return false;

            //
            var gameHandler = GameController.Instance.gameHandler;

            // Current tile
            Tile tile = gameHandler.GetTile(cellPtr);
            if(tile == null)
                return false;

            //gameHandler.freeze = true;
            gameHandler.pipeline.PushFirst(TileData.Create(tile));
            
            return true;
        }
    }
}