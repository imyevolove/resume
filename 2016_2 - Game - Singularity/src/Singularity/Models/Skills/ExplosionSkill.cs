﻿using System.Collections;
using System.Collections.Generic;
using Singularity.Controllers;
using Singularity.Models;
using Singularity.Models.IO;
using Singularity.State;
using UnityEngine;

namespace Singularity.Skills
{
    public class ExplosionSkill : BasePointerSkill
    {
        [SerializeField]
        protected float waitDuration;

        // Do nothing
        protected override void AbortAction() {}

        protected override bool UseAction(object target)
        {
            CellPtr cellPtr = (CellPtr)target;
            if(cellPtr == null) return false;

            var gameHandler = GameController.Instance.gameHandler;
            
            Tile tile = gameHandler.GetTile(cellPtr);
            if(tile == null)
                return false;

            gameHandler.Freeze();

            StartCoroutine(DeferredExplosion(tile));
            return true;
        }

        protected IEnumerator DeferredExplosion(Tile target)
        {
            var gameHandler = GameController.Instance.gameHandler;
            target.ApplyState(States.ExplodeState);
            target.lockState = true;

            yield return new WaitForSeconds(waitDuration);
            
            gameHandler.RemoveTile(target);
            gameHandler.Unfreeze();
        }
    }
}