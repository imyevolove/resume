﻿using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.IO;
using Singularity.UI;
using UnityEngine;

namespace Singularity.Skills
{
    public delegate void SkillManagerEventHandler(BaseSkill skill);

    [Serializable]
    public class SkillManager : MonoBehaviour
    {
        [SerializeField]
        private List<BaseSkill> m_Skills;
        public List<BaseSkill> skills
        {
            get { return m_Skills; }
            protected set { m_Skills = value; }
        }

        public event SkillManagerEventHandler onSkillAdd;
        public event SkillManagerEventHandler onSkillRemove;

        public void Reset()
        {
            foreach(var skill in skills)
                skill.Rollback();
        }

        public void AddSkill(BaseSkill skill)
        {
            skills.Add(skill);
            DispatchEvent(onSkillAdd, skill);
        }

        public bool RemoveSkill(BaseSkill skill)
        {
            bool removeStatus = skills.Remove(skill);
            
            if(removeStatus)
                DispatchEvent(onSkillRemove, skill);

            return removeStatus;
        }

        protected void DispatchEvent(SkillManagerEventHandler eventHandler, BaseSkill skill)
        {
            if(eventHandler == null) return;
            eventHandler(skill);
        }
    }
}