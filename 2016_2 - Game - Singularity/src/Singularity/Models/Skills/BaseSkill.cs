﻿using System;
using System.Collections;
using System.Collections.Generic;
using Singularity.Models;
using UnityEngine;
using Singularity.Skills.Recoil;

namespace Singularity.Skills
{
    [Serializable]
    public abstract class BaseSkill : MonoBehaviour
    {
        [SerializeField]
        public Sprite icon;
        
        [SerializeField]
        public BaseRecoilHandler        recoilHandler;
        public bool  isReady            { get; protected set; }

        public event SkillEventHandler onPreUse;
        public event SkillEventHandler onUse;
        public event SkillEventHandler onAbort;

        protected virtual void Start()
        {
            isReady = true;
            recoilHandler.onRecoilFinish += Rollback;
        }

        public virtual void Use(object target)
        {
            if (!isReady) return;
            if (!UseAction(target)) 
            {
                DispatchEvent(onAbort);
                return;
            }
            
            isReady = false;
            DispatchEvent(onUse);
            recoilHandler.RunRollback();
        }

        public virtual void Abort()
        {
            AbortAction();
            DispatchEvent(onAbort);
        }

        protected abstract bool UseAction(object target);
        protected abstract void AbortAction();

        public void Rollback()
        {
            isReady = true;
            // Hack for avoiding stack overflow )))
            recoilHandler.onRecoilFinish -= Rollback;
            recoilHandler.Rollback();
            recoilHandler.onRecoilFinish += Rollback;
        }

        private void DispatchEvent(SkillEventHandler eventHandler)
        {
            if(eventHandler == null) return;
            eventHandler();
        }

        protected void DispatchPreUseEvent()
        {
            DispatchEvent(onPreUse);
        }
    }
}