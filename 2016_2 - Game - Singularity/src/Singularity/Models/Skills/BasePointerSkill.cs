﻿using System;
using Singularity.Controllers;
using Singularity.Models;
using Singularity.Models.IO;
using Singularity.State;
using UnityEngine;

namespace Singularity.Skills
{
    public enum BasePointerSkillState
    {
        Prepare,
        Perform
    }

    [Serializable]
    public abstract class BasePointerSkill : BaseSkill
    {
        protected BasePointerSkillState state = BasePointerSkillState.Prepare;
        public override void Use(object target)
        {
            switch(state)
            {
                case BasePointerSkillState.Prepare:
                    state = BasePointerSkillState.Perform;
                    DispatchPreUseEvent();
                    RegisterInInputState();
                break;
                case BasePointerSkillState.Perform:
                    state = BasePointerSkillState.Prepare;
                    base.Use(target);
                    UnregisterInInputState();
                break;
            }
        }

        public override void Abort()
        {
            UnregisterInInputState();
            base.Abort();
        }

        protected void RegisterInInputState()
        {
            InputManager.Instance.SetState(InputState.DragSkill, this);
        }
        
        protected void UnregisterInInputState()
        {
            InputManager inputManager = InputManager.Instance;
            
            if(inputManager.target != (object)this) return;
            InputManager.Instance.SetState(InputState.None, null);
        }
    }
}