using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Singularity.IO
{
    public static class Storage
    {
        public static string persistentDataPath { get { return Application.persistentDataPath; } }

        public static string MakePath(params string[] parts)
        {
            return parts.Aggregate((x, y) => Path.Combine(x, y));
        }

        public static T Load<T>(string filename) where T: class
        {
            try
            {
                if(File.Exists(filename)) {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream file = File.Open(filename, FileMode.Open);
                    T obj = (T)bf.Deserialize(file);
                    
                    file.Close();
                    return obj;
                }
            }
            catch(Exception e)
            {
                Debug.LogException(e);
            }
            return null;
        }
        
        public static void Save<T>(T obj, string filename) where T: class
        {
            try
            {
                CreateDirectories(filename);

                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create (filename);
                bf.Serialize(file, obj);
                file.Close();
            }
            catch(IOException e)
            {
                Debug.LogException(e);
            }
        }

        public static string UTF8ByteArrayToString(byte[] characters) 
        {      
            UTF8Encoding encoding = new UTF8Encoding(); 
            string constructedString = encoding.GetString(characters); 
            return (constructedString); 
        } 

        public static byte[] StringToUTF8ByteArray(string pXmlString) 
        { 
            UTF8Encoding encoding = new UTF8Encoding(); 
            byte[] byteArray = encoding.GetBytes(pXmlString); 
            return byteArray; 
        }

        private static void CreateDirectories(string path)
        {
            path = Path.GetDirectoryName(path);
            Directory.CreateDirectory(path);
        }
    }
}