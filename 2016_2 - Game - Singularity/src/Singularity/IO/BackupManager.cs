﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Singularity.IO
{
    public class BackupManager
    {
        public const string backupFileExtension = "backup";

        ////////////////////////////////////////////////////////////
        public static string backupDirectoryPath
        {
            get
            {
                return Storage.persistentDataPath + Path.DirectorySeparatorChar + "Saves";
            }
        }

        ////////////////////////////////////////////////////////////
        public static bool BackupFileExists(string backupName)
        {
            return File.Exists(MakePath(backupName));
        }

        ////////////////////////////////////////////////////////////
        public static void RemoveBackup(string backupName)
        {
            string filepath = MakePath(backupName);

            if (!File.Exists(filepath)) return;
            File.Delete(filepath);
        }

        ////////////////////////////////////////////////////////////
        public static T LoadBackup<T>(string backupName) where T : BaseBackup
        {
            return Storage.Load<T>(MakePath(backupName));
        }

        ////////////////////////////////////////////////////////////
        public static void SaveBackup<T>(T backup, string backupName) where T : BaseBackup
        {
            Storage.Save<T>(backup, MakePath(backupName));
        }

        ////////////////////////////////////////////////////////////
        protected static string MakePath(string backupName)
        {
            return backupDirectoryPath + Path.DirectorySeparatorChar + backupName + "." + backupFileExtension;
        }
    }
}
