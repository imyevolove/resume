﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Singularity.AdsManagement
{
    public interface IAdsProvider
    {
        void Initialize();
        bool IsInitialized();

        void ShowBanner();
        void HideBanner();

        void LoadInterstitial();
        void ShowInterstitial();
        bool CanShowInterstitial();

        void LoadVideo();
        void ShowVideo();
        bool CanShowVideo();
    }
}
