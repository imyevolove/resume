﻿using AppodealAds.Unity.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.AdsManagement
{
    public class AppodealProvider : IAdsProvider
    {
        protected string appKey;
        private bool isInitialized = false;

        public AppodealProvider(string appKey)
        {
            this.appKey = appKey;
        }

        public bool CanShowInterstitial()
        {
            return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
        }

        public bool CanShowVideo()
        {
            return Appodeal.isLoaded(Appodeal.NON_SKIPPABLE_VIDEO);
        }

        public void HideBanner()
        {
            Debug.Log("HIDE BANNER");
            Appodeal.hide(Appodeal.BANNER);
        }

        public void Initialize()
        {
            if (IsInitialized()) return;
            Appodeal.disableLocationPermissionCheck();
            Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.NON_SKIPPABLE_VIDEO | Appodeal.BANNER);
            isInitialized = true;
        }

        public bool IsInitialized()
        {
            return isInitialized;
        }

        public void LoadInterstitial()
        {
            /// do nothing
        }

        public void LoadVideo()
        {
            /// do nothing
        }

        public void ShowBanner()
        {
            Debug.Log("SHOW BANNER");
            Appodeal.show(Appodeal.BANNER_TOP);
        }

        public void ShowInterstitial()
        {
            Appodeal.show(Appodeal.INTERSTITIAL);
        }

        public void ShowVideo()
        {
            Appodeal.show(Appodeal.NON_SKIPPABLE_VIDEO);
        }
    }
}
