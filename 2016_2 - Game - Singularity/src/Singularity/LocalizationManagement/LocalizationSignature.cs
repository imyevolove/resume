namespace Singularity.LocalizationManagement
{
    public sealed class LocalizationSignature
    {
        public string LocalName { get; private set; }
        public string Code      { get; private set; }
        
        public static LocalizationSignature Unknown
        {
            get
            {
                return new LocalizationSignature("unknown", "unknown");
            }
        }

        public LocalizationSignature(string langCode, string langLocalName)
        {
            Code        = langCode;
            LocalName   = langLocalName;
        }
    }
}