using System.Collections.Generic;
using UnityEngine;

namespace Singularity.LocalizationManagement
{
    public class LocalizationConfig
    {
        public readonly string localizationAssetsPath;

        //
        public Dictionary<string, LocalizationSignature> localizationSignatures { get; protected set; }

        public LocalizationConfig(string localizationAssetsPath)
        {
            this.localizationAssetsPath = localizationAssetsPath;
            localizationSignatures = new Dictionary<string, LocalizationSignature>();
        }
    }
}