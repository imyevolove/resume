using Generic.String;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.LocalizationManagement.Mono
{
    [Serializable, RequireComponent(typeof(Text))]
    public class TextLocalizer : MonoBehaviour
    {
        [SerializeField]
        public string localizationKey;

        [SerializeField]
        public StringCase stringCase = StringCase.None;
        
        ////////////////////////////////////////////////////////////
        public void UpdateText(params string[] formatValues)
        {
            if (string.IsNullOrEmpty(localizationKey))
            {
                Debug.Log("LOCALIZATION KEY IS EMPTY");
                return;
            }
            GetComponent<Text>().text = GetLocalization(localizationKey, formatValues);
        }

        ////////////////////////////////////////////////////////////
        public void UpdateTextCustom(string localizationKey, params string[] formatValues)
        {
            if (string.IsNullOrEmpty(localizationKey))
            {
                Debug.Log("LOCALIZATION KEY IS EMPTY");
                return;
            }
            GetComponent<Text>().text = GetLocalization(localizationKey, formatValues);
        }

        ////////////////////////////////////////////////////////////
        public void SetCustomText(string str)
        {
            GetComponent<Text>().text = str;
        }

        ////////////////////////////////////////////////////////////
        protected string GetLocalization(string localizationKey, params string[] formatValues)
        {
            string localizationString = LocalizationManager.GetLocalization(localizationKey, formatValues);
            localizationString = StringFormat.Format(localizationString, stringCase);
            return localizationString;
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateText()
        {
            if (string.IsNullOrEmpty(localizationKey))
            {
                Debug.Log("LOCALIZATION KEY IS EMPTY");
                return;
            }
            GetComponent<Text>().text = GetLocalization(localizationKey);
        }

        ////////////////////////////////////////////////////////////
        protected virtual void Start()
        {
            UpdateText();
            LocalizationManager.onChange += UpdateText;
        }

        ////////////////////////////////////////////////////////////
        protected virtual void OnDestroy()
        {
            LocalizationManager.onChange -= UpdateText;
        }
    }
}