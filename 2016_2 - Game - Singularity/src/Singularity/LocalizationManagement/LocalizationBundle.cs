namespace Singularity.LocalizationManagement
{
    public class LocalizationBundle : LocalizationDictionary
    {
        public LocalizationSignature Signature { get; protected set; }

        public LocalizationBundle(LocalizationSignature signature)
        {
            Signature = signature;
        }

        public LocalizationBundle()
        {
            Signature = LocalizationSignature.Unknown;
        }
    }
}