﻿//using GooglePlayGames;
//using google.service.game;
using GooglePlayGames;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Singularity.SocialManagement
{
    public class SocialManager
    {
        private static Dictionary<string, ILeaderboard> leaderboards 
            = new Dictionary<string, ILeaderboard>();

        ////////////////////////////////////////////////////////////
        public static void LogIn()
        {
            PlayGamesPlatform.Activate();
            Social.localUser.Authenticate((bool success) => {
                Debug.LogFormat("SOCIAL AUTH: status({0})", success);
            });

            //GoogleGame.Instance().login(true, false);
            //GoogleGame.Instance().gameEventHandler += SocialManager_gameEventHandler;
            //PlayGamesPlatform.Activate();
            /*
            Social.localUser.Authenticate((status) => {
                Debug.Log(status
                    ? "Auth OK!"
                    : "Auth Fail");
            });
            */
        }
        /*
        private static void SocialManager_gameEventHandler(int result_code, string eventName, string msg)
        {
            Debug.LogFormat("SOCIAL EVENT: CODE({0}) EVENT({1}) MESSAGE({2})", result_code, eventName, msg);
            if (eventName == GameEvent.onConnectSuccess)
            {
                Debug.Log("Login Success");
            }
        }
        */

        ////////////////////////////////////////////////////////////
        public static void RegisterLeaderboard(string id)
        {
            if (leaderboards.ContainsKey(id)) return;

            ILeaderboard leaderboard = Social.CreateLeaderboard();
            leaderboard.id = id;

            leaderboards.Add(id, leaderboard);
            //LoadScore(id);

            Debug.Log("Register leaderboard OK!");
        }

        ////////////////////////////////////////////////////////////
        public static void ShowLeaderboardUI()
        {
            Social.ShowLeaderboardUI();
            //GoogleGame.Instance().showLeaderboards();
        }

        ////////////////////////////////////////////////////////////
        public static void ShowLeaderboardUI(string board)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(board);
        }

        ////////////////////////////////////////////////////////////
        public static void ReportScore(string board, long score)
        {
            //GoogleGame.Instance().submitLeaderboardScore(board, score);
            Social.ReportScore(score, board, (status) => {
                Debug.Log(status
                    ? "Score reported successfully"
                    : "Add Score Fail");
            });
        }

        ////////////////////////////////////////////////////////////
        public static void LoadScore(string board)
        {
            ILeaderboard leaderboard;
            if (!leaderboards.TryGetValue(board, out leaderboard)) return;

            PlayGamesPlatform.Instance.LoadScores(leaderboard, (status) => { });
        }

        ////////////////////////////////////////////////////////////
        public static void LoadScore(string board, Action<bool> callback)
        {
            ILeaderboard leaderboard;
            if (!leaderboards.TryGetValue(board, out leaderboard)) return;

            PlayGamesPlatform.Instance.LoadScores(leaderboard, callback);
        }
    }
}
