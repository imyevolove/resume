﻿//using google.service.game;
using Singularity.LocalizationManagement;
using Singularity.SocialManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.Controllers.Entities
{
    public class LeaderboardController : MonoController
    {
        private static LeaderboardController m_Instance;
        public static LeaderboardController Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    // Find on scene
                    m_Instance = GameObject.FindObjectOfType<LeaderboardController>();

                    if (m_Instance == null)
                    {
                        // Load from file
                        m_Instance = LoadAndInstantiateController<LeaderboardController>();

                        if (m_Instance == null)
                        {
                            // Create new object
                            m_Instance = (new GameObject("LeaderboardController", typeof(LeaderboardController)))
                                .GetComponent<LeaderboardController>();
                        }
                    }
                }

                return m_Instance;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            SocialManager.LogIn();

            // Best score
            SocialManager.RegisterLeaderboard(GPGSIds.leaderboard_score);
            //GoogleGame.Instance().loadLeaderboardsMetadata(false);
        }

        public static void ReportBestScore(long score)
        {
            SocialManager.ReportScore(GPGSIds.leaderboard_score, score);
        }

        public static void ShowMainLeaderboard()
        {
            Debug.Log("PRE SHOW LEADERBOARD");
            //GoogleGame.Instance().showLeaderboard(boards["score"]);
            SocialManager.ShowLeaderboardUI(GPGSIds.leaderboard_score);
            Debug.Log("POST SHOW LEADERBOARD");
        }
    }
}
