using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.Personal;
using UnityEngine;
using UnityEngine.UI;
using Singularity.SocialManagement;
using Singularity.IO;
using Singularity.AdsManagement;
using Singularity.IAPManagement;

namespace Singularity.Controllers.Entities
{
    [Serializable]
    public class MenuPlayDialogController : MonoController
    {
        private static MenuPlayDialogController m_Instance;
        public static MenuPlayDialogController Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    // Find on scene
                    m_Instance = GameObject.FindObjectOfType<MenuPlayDialogController>();

                    if (m_Instance == null)
                    {
                        // Load from file
                        m_Instance = LoadAndInstantiateController<MenuPlayDialogController>();

                        if (m_Instance == null)
                        {
                            // Create new object
                            m_Instance = (new GameObject("GameEndController", typeof(MenuPlayDialogController)))
                                .GetComponent<MenuPlayDialogController>();
                        }
                    }
                }

                return m_Instance;
            }
        }

        [SerializeField]
        protected RectTransform rootContainer;

        [SerializeField]
        protected Text continueButtonSubtitle;

        ////////////////////////////////////////////////////////////
        public void SetVisibility(bool visibility)
        {
            rootContainer.gameObject.SetActive(visibility);
        }

        ////////////////////////////////////////////////////////////
        public void StartNewGame()
        {
            SetVisibility(false);
            BackupManager.RemoveBackup(GameHandler.backupName);
            MenuController.Instance.StartGame();
        }

        ////////////////////////////////////////////////////////////
        public void ContinueGame()
        {
            SetVisibility(false);
            AdsManager.ShowAnyFullAd();
            MenuController.Instance.StartGame();
        }

        ////////////////////////////////////////////////////////////
        protected void UpdateContinueButton()
        {
            continueButtonSubtitle.gameObject.SetActive(AdsManager.canShowAds);
        }

        ////////////////////////////////////////////////////////////
        protected override void EnableDontDestroyOnLoad()
        {
            // Do nothing
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            SetVisibility(false);

            UpdateContinueButton();
            AdsManager.onChange -= UpdateContinueButton;
            AdsManager.onChange += UpdateContinueButton;
        }
    }
}