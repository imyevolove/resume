using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.ThemeManagement;
using Singularity.ThemeManagement.Mono;
using UnityEngine;

namespace Singularity.Controllers.Entities
{
    [Serializable]
    public class ThemeController : MonoController
    {
        private static ThemeController m_Instance;
        public static ThemeController Instance
        { 
            get
            {
                if(m_Instance == null)
                {
                    // Find on scene
                    m_Instance = GameObject.FindObjectOfType<ThemeController>();
                    
                    if(m_Instance == null)
                    {
                        // Load from file
                        m_Instance = LoadAndInstantiateController<ThemeController>();

                        if(m_Instance == null)
                        {
                            // Create new object
                            m_Instance = (new GameObject("ThemeController", typeof(ThemeController)))
                                .GetComponent<ThemeController>();
                        }
                    }
                }

                return m_Instance;
            }
        }

        [SerializeField]
        private string observedThemeName;

        [SerializeField]
        protected List<Theme> themes;

        protected void OnValidate()
        {
            Debug.Log("Theme Controller: Validate");
            foreach(var theme in themes)
            {
                if(theme.name == observedThemeName)
                {
                    ThemeManager.AddTheme(theme, true);
                    ThemeManager.SelectTheme(theme);
                    ThemeStyleReceiver.RedrawAll();
                    break;
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            Init();
        }

        protected void Init()
        {
            foreach(var theme in themes)
                ThemeManager.AddTheme(theme);
            
            // Set default theme
            ThemeManager.SelectTheme(0);

            // Set selected theme
            ThemeManager.SelectTheme(Game.current.GetSelectedTheme());
            ThemeStyleReceiver.RedrawAll();
            
        }
    }
}