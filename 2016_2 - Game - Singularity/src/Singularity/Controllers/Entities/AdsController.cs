﻿using Singularity.AdsManagement;
using Singularity.LocalizationManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.Controllers.Entities
{
    public class AdsController : MonoController
    {
        protected override void Awake()
        {
            base.Awake();

            /*
             IAdsProvider provider = new AdmobProvider(
                "ca-app-pub-1844390866925362/8516231933", 
                "ca-app-pub-1844390866925362/9992965133", 
                "ca-app-pub-1844390866925362/6760297136");
            */

            IAdsProvider provider = new AppodealProvider("0a2a9dde5c230e2521091f646913776e93937d3227d51035");
            AdsManager.Setup(provider);
        }
    }
}
