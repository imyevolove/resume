using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.Entities;
using UnityEngine;
using Singularity.ThemeManagement;

namespace Singularity.Controllers.Entities
{
    [Serializable]
    public class TileController : MonoBehaviour
    {
        private static TileController m_Instance;
        public static TileController Instance
        { 
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = GameObject.FindObjectOfType<TileController>();
                    
                    if(m_Instance == null)
                        m_Instance = (new GameObject("TileController", typeof(TileController)))
                            .GetComponent<TileController>();
                }

                return m_Instance;
            }
        }

        [SerializeField]
        protected Tile tilePrefab;

        public List<TileData> GetAvailableTiles()
        {
            List<TileData> data = new List<TileData>();
            int weight = 1;

            foreach (var color in ThemeManager.current.gmTileAsset.tileColors)
            {
                data.Add(new TileData(weight, color));
                weight++;
            }
            return data;
        }

        public TileData GetUnregisteredTileData()
        {
            return new TileData(0, ThemeManager.current.gmTileAsset.unregisteredTileColor);
        }

        public Tile GetTilePrefab()
        {
            return tilePrefab;
        }
    }
}