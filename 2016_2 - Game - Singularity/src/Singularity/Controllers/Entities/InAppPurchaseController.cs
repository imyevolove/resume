﻿using Singularity.AdsManagement;
using Singularity.IAPManagement;
using Singularity.ThemeManagement;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Singularity.Controllers.Entities
{
    public class InAppPurchaseController : MonoController
    {
        protected override void Awake()
        {
            base.Awake();

            List<IProduct> products = new List<IProduct>();

            var game = Game.current;
            var bundle = new ProductsBundleBuilder();

            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_0", "theme0"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_1", "theme1"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_2", "theme2"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_3", "theme3"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_4", "theme4"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_5", "theme5"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_6", "theme6"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_7", "theme7"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(CreateThemeProduct("product_theme_8", "theme8"));
            ////////////////////////////////////////////////////////////
            bundle.AddProduct(new UIAPProduct("product_special_0", ProductType.Consumable, ProductMarker.Special)
            {
                // Unlock all products
                unlockAction = (product) =>
                {
                    Game.current.RegisterPurchase(product);
                    foreach (var pdct in products)
                    {
                        if (pdct != product)
                        {
                            pdct.Unlock();
                        }
                    }
                    AdsManager.DisableAds();
                },
                image = Resources.Load<Sprite>("IAP/Products/Images/not-found"),
                description = "Remove Ads \n Unlock all themes"
            });

            products.AddRange(bundle.Build());
            products[0].Unlock(); /// Default theme

            IAPManager.AddProducts(products.ToArray());
            IAPManager.Initialize();

            /// Unlock defined purchases
            foreach (var purchasId in game.GetPurchases())
            {
                var product = products.Find(p => { return p.productId == purchasId; });
                if (product != null)
                    product.Unlock();
            }
        }

        protected IProduct CreateThemeProduct(string productId, string themeId)
        {
            return new UsableProduct(productId, ProductType.Consumable)
            {
                unlockAction = (product) => { Game.current.RegisterPurchase(product); },
                useAction = delegate {
                    if (ThemeManager.SelectTheme(themeId))
                        Game.current.SetSelectedTheme(themeId);
                },
                useActionValidator = delegate { return ThemeManager.current.name != themeId; },
                image = Resources.Load<Sprite>(string.Format("IAP/Products/Images/{0}", themeId))
            };
        }
    }
}
