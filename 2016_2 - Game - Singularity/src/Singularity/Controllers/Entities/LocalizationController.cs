﻿using Singularity.LocalizationManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.Controllers.Entities
{
    public class LocalizationController : MonoController
    {
        protected override void Awake()
        {
            base.Awake();

            string[] path = new string[] { "Localization", "config" };
            string langConfigPath = path.Aggregate((x, y) => Path.Combine(x, y));

            LocalizationManager.LoadConfigFile(langConfigPath);
            LocalizationManager.LoadLocalizations();

            LocalizationManager.ApplyLocalizationLanguage(Game.current.GetSelectedLanguageCode());
        }
    }
}
