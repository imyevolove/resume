using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.Personal;
using UnityEngine;
using UnityEngine.UI;
using Singularity.SocialManagement;

namespace Singularity.Controllers.Entities
{
    [Serializable]
    public class GameEndController : MonoController
    {
        private static GameEndController m_Instance;
        public static GameEndController Instance
        { 
            get
            {
                if(m_Instance == null)
                {
                    // Find on scene
                    m_Instance = GameObject.FindObjectOfType<GameEndController>();
                    
                    if(m_Instance == null)
                    {
                        // Load from file
                        m_Instance = LoadAndInstantiateController<GameEndController>();

                        if(m_Instance == null)
                        {
                            // Create new object
                            m_Instance = (new GameObject("GameEndController", typeof(GameEndController)))
                                .GetComponent<GameEndController>();
                        }
                    }
                }

                return m_Instance;
            }
        }

        [SerializeField]
        protected RectTransform rootContainer;

        [SerializeField]
        protected RectTransform rootDialog;

         [SerializeField]
        protected Text scoreText;

        [SerializeField]
        protected Transform newBestScorePanel;

        public void SetScore(int scoreValue)
        {
            scoreText.text = scoreValue.ToString();
        }

        public void SetNewBestScorePanleVisibility(bool visibility)
        {
            newBestScorePanel.gameObject.SetActive(visibility);
        }

        public void SetVisibility(bool visibility)
        {
            rootContainer.gameObject.SetActive(visibility);
        }

        public void Action_GoToMenu()
        {
            MainController.Instance.LoadScene(SceneReferences.SCENE_MENU);
        }

        public void Action_Retry()
        {
            Game.current.gameHandler.Restart();
            SetVisibility(false);
        }

        public void Action_ShowLeaderboard()
        {
            LeaderboardController.ShowMainLeaderboard();
        }

        protected override void EnableDontDestroyOnLoad()
        {
            // Do nothing
        }
    }
}