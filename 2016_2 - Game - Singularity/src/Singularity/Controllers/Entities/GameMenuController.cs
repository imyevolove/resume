using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.Personal;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.Controllers.Entities
{
    [Serializable]
    public class GameMenuController : MonoController
    {
        private static GameMenuController m_Instance;
        public static GameMenuController Instance
        { 
            get
            {
                if(m_Instance == null)
                {
                    // Find on scene
                    m_Instance = GameObject.FindObjectOfType<GameMenuController>();
                    
                    if(m_Instance == null)
                    {
                        // Load from file
                        m_Instance = LoadAndInstantiateController<GameMenuController>();

                        if(m_Instance == null)
                        {
                            // Create new object
                            m_Instance = (new GameObject("GameMenuController", typeof(GameMenuController)))
                                .GetComponent<GameMenuController>();
                        }
                    }
                }

                return m_Instance;
            }
        }

        [SerializeField]
        protected RectTransform rootContainer;

        [SerializeField]
        protected RectTransform rootDialog;

        public void SetVisibility(bool visibility)
        {
            rootContainer.gameObject.SetActive(visibility);
        }

        public void Action_GoToMenu()
        {
            MainController.Instance.LoadScene(SceneReferences.SCENE_MENU);
        }

        public void Action_Restart()
        {
            Game.current.gameHandler.Restart();
            SetVisibility(false);
        }

        public void Action_Close()
        {
            SetVisibility(false);
        }

        protected override void EnableDontDestroyOnLoad()
        {
            // Do nothing
        }
    }
}