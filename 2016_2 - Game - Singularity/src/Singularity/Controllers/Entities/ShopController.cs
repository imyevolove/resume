using System;
using System.Collections.Generic;
using Singularity.Models;
using Singularity.Models.Entities;
using Singularity.Personal;
using UnityEngine;
using UnityEngine.UI;
using Singularity.IAPManagement;
using Singularity.UI;

namespace Singularity.Controllers.Entities
{
    [Serializable]
    public class ShopController : MonoController
    {
        private static ShopController m_Instance;
        public static ShopController Instance
        { 
            get
            {
                if (m_Instance == null)
                {
                    // Find on scene
                    m_Instance = GameObject.FindObjectOfType<ShopController>();
                    
                    if(m_Instance == null)
                    {
                        // Load from file
                        m_Instance = LoadAndInstantiateController<ShopController>();

                        if(m_Instance == null)
                        {
                            // Create new object
                            m_Instance = (new GameObject("ShopController", typeof(ShopController)))
                                .GetComponent<ShopController>();
                        }
                    }
                }

                return m_Instance;
            }
        }

        [SerializeField]
        protected PurchaseProductCard purchaseProductCardPrefab;

        [SerializeField]
        protected PurchaseSpecialProduct purchaseSpecialProduct;

        [SerializeField]
        protected RectTransform rootContainer;

        [SerializeField]
        protected RectTransform specialOfferContainer;

        [SerializeField]
        protected RectTransform itemsContainer;

        protected List<PurchaseProductCard> cards = new List<PurchaseProductCard>();

        public void Show()
        {
            rootContainer.gameObject.SetActive(true);
        }

        public void Hide()
        {
            rootContainer.gameObject.SetActive(false);
        }

        protected void Start()
        {
            Show();

            foreach (var product in IAPManager.GetProducts(ProductMarker.Default))
            {
                AddProduct(product);
            }

            IProduct[] specialProducts = IAPManager.GetProducts(ProductMarker.Special);
            if (specialProducts.Length > 0)
            {
                SetSpecialProduct(specialProducts[0]);
            }

            Hide();
        }

        public void Action_RestorePurchases()
        {
            IAPManager.RestorePurchases();
        }

        protected void SetSpecialProduct(IProduct product)
        {
            purchaseSpecialProduct.SetTarget(product);
        }

        protected void AddProduct(IProduct product)
        {
            PurchaseProductCard card = GameObject.Instantiate<PurchaseProductCard>(
                purchaseProductCardPrefab);

            card.gameObject.SetActive(true);
            card.transform.SetParent(itemsContainer, false);
            card.SetTarget(product);

            card.onChange += delegate {
                foreach (var cd in cards)
                {
                    if (cd == card) continue;
                    cd.Redraw();
                }
            };

            cards.Add(card);
        }

        protected override void EnableDontDestroyOnLoad()
        {
            // Do nothing
        }
    }
}