﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Singularity.Controllers.Entities;
using Singularity.SocialManagement;
using Singularity.IO;
using Singularity.Models;
using Singularity.AdsManagement;

namespace Singularity.Controllers
{
    [Serializable]
    public sealed class MenuController : MonoController
    {
        public static MenuController Instance { get; private set; }

        [HeaderAttribute("Continue Button")]
        [SerializeField]
        private Animator MenuAnimator;

        [SerializeField]
        private RectTransform menuContainer;

        [SerializeField]
        private RectTransform lockPanel;

        [SerializeField]
        private Text ContinueButtonText;
        
        [SerializeField]
        private Text ContinueButtonScoreText;

        private void Start()
        {
            /// Show menu first
            Action_ShowMenu();
            AdsManager.ShowBanner();
        }

        private void RedrawContinueButton()
        {
            /*
            if(!ContinueButtonText.IsNull())
                ContinueButtonText.text = LocalizationController
                .GetInstance().GetPhrase("MENU_BUTTON_CONTINUE_TITLE")
                .ToUpper();

            if(!ContinueButtonScoreText.IsNull())
                ContinueButtonScoreText.text = (LocalizationController
                .GetInstance().GetPhrase("MENU_BUTTON_CONTINUE_SCORE_TITLE")
                + " - " + "UNDEFINED")
                .ToUpper();
                */
        }

        protected override void EnableDontDestroyOnLoad()
        {
            if(Instance == null)
            {
                Instance = this;
                //base.EnableDontDestroyOnLoad();
            }
            else if(Instance != this)
            {
                Destroy(this.gameObject);
                Destroy(this);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            lockPanel.gameObject.SetActive(false);
        }

        public void Show()
        {
            menuContainer.gameObject.SetActive(true);
        }

        public void Hide()
        {
            menuContainer.gameObject.SetActive(false);
        }

        public void Action_ShowMenu()
        {
            Show();
            ShopController.Instance.Hide();
        }

        public void Action_ShowShop()
        {
            Hide();
            ShopController.Instance.Show();
        }

        public void Action_PlayButton()
        {
            if (BackupManager.BackupFileExists(GameHandler.backupName))
            {
                MenuPlayDialogController.Instance.SetVisibility(true);
            }
            else
            {
                StartGame();
            }
        }

        public void Action_OpenSocialPage()
        {
            Application.OpenURL("https://vk.com/club104689708");
        }

        public void Action_ShowLeaderboard()
        {
            LeaderboardController.ShowMainLeaderboard();
        }

        public void StartGame()
        {
            MenuAnimator.SetBool("GoGame", true);
            lockPanel.gameObject.SetActive(true);
        }

        public void LoadGameScene()
        {
            SceneReferences.SCENE_GAME.LoadAsync(
                UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
}
