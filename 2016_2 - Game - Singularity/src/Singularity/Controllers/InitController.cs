﻿using Singularity.Controllers.Entities;
using Singularity.SocialManagement;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Singularity.Controllers
{
    [SerializeField]
    public class InitController : MonoController
    {
        ////////////////////////////////////////////////////////////
        protected override void EnableDontDestroyOnLoad()
        {
            /// Do nothing
        }

        ////////////////////////////////////////////////////////////
        protected void Start()
        {
            MonoController.Load<LeaderboardController>();
            MonoController.Load<InAppPurchaseController>();
            MonoController.Load<LocalizationController>();
            MonoController.Load<ThemeController>();
            MonoController.Load<AdsController>();
            LoadNextScene();
        }

        protected void LoadNextScene()
        {
            /// Check editor additive scene
            for (var i = 0; i < SceneManager.sceneCount; i++)
            {
                /// For dev game
                if (
                    SceneManager.GetSceneAt(i).name == SceneReferences.SCENE_MENU.SceneID
                    || SceneManager.GetSceneAt(i).name == SceneReferences.SCENE_GAME.SceneID)
                {
                    return;
                }
            }

            /// Load menu scene
            SceneManager.LoadScene(SceneReferences.SCENE_MENU.SceneID);
        }
    }
}