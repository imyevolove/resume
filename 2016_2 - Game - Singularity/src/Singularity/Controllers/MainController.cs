﻿using Singularity.Controllers.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Singularity.Controllers
{

    public static class SceneReferences
    {
        public static readonly ISceneReference SCENE_MENU    = new SceneReference("scn_menu");
        public static readonly ISceneReference SCENE_GAME    = new SceneReference("scn_game");
        public static readonly ISceneReference SCENE_LOADING = new SceneReference("scn_loading");
    }

    [SerializeField]
    public class MainController : MonoController
    {
        public static MainController Instance { get; private set; }

        ////////////////////////////////////////////////////////////
        protected override void EnableDontDestroyOnLoad()
        {
            if(Instance == null)
            {
                Instance = this;
                base.EnableDontDestroyOnLoad();
            }
            else if(Instance != this)
            {
                Destroy(this.gameObject);
                Destroy(this);
            }
        }

        ////////////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();

        }

        ////////////////////////////////////////////////////////////
        public void LoadScene(ISceneReference scene, LoadSceneMode mode = LoadSceneMode.Single)
        {
            SceneManager.LoadScene(scene.SceneID, mode);
        }
    }
}