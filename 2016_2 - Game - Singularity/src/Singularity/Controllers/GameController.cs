﻿using UnityEngine;
using System;
using Singularity.Models;
using Singularity.Personal;
using Singularity.LocalizationManagement;
using Singularity.LocalizationManagement.IO;
using System.IO;
using Singularity.Controllers.Entities;
using Singularity.ThemeManagement;
using System.Linq;
using Singularity.IO;
using Singularity.AdsManagement;

namespace Singularity.Controllers
{
    [Serializable]
    public sealed class GameController : MonoController
    {
        public static GameController Instance { get; private set; }

        [SerializeField]
        public GameHandler gameHandler = new GameHandler();
        
        private void Start()
        {
            base.Awake();

            // Init Theme controller
            ThemeController themeController = ThemeController.Instance;

            Game.current.gameHandler = gameHandler;
            gameHandler.Init();
            gameHandler.LoadBackup();

            AdsManager.ShowBanner();
        }

        protected override void EnableDontDestroyOnLoad()
        {
            if (Instance == null)
            {
                Instance = this;
                //base.EnableDontDestroyOnLoad();
            }
            else if (Instance != this)
            {
                Destroy(this.gameObject);
                Destroy(this);
            }
        }
    }
}
