using System;
using UnityEngine;

namespace Singularity.ThemeManagement.Mono
{
    [Serializable]
    public abstract class ThemeStyleReceiver : MonoBehaviour
    {
        [SerializeField] 
        protected string fieldName;
        
        protected virtual void Start()
        {
            ThemeManager.onChange += Redraw;
            Redraw();
        }

        /*
        protected virtual void Update()
        {
            #if UNITY_EDITOR
            if(!Application.isPlaying)
            {
                Debug.Log(123);
                Redraw(ThemeManager.current);
            }
            #endif
        }
        */

        public abstract void Redraw();

        protected TFiled GetThemeField<TFiled>(Theme theme)
        {
            try
            {
                return (TFiled)theme.GetType().GetField(fieldName).GetValue(theme);
            }
            catch(Exception)
            {
                Debug.LogFormat("Theme Stile receiver catch error");
                return default(TFiled);
            }
        }

        public static void RedrawAll()
        {
            foreach(var receiver in GameObject.FindObjectsOfType<ThemeStyleReceiver>())
                receiver.Redraw();
        }
    }
}