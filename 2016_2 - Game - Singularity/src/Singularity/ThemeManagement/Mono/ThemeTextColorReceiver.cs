using System;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.ThemeManagement.Mono
{
    [Serializable, RequireComponent(typeof(Text))]
    public class ThemeTextColorReceiver : ThemeStyleReceiver
    {
        public override void Redraw()
        {
            if(this == null || GetComponent<Text>() == null)
                return;

            Color color = GetThemeField<Color>(ThemeManager.current);
            GetComponent<Text>().color = color;
        }
    }
}