using System;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.ThemeManagement.Mono
{
    [Serializable, RequireComponent(typeof(Image))]
    public class ThemeImageColorReceiver : ThemeStyleReceiver
    {
        public override void Redraw()
        {
            if(this == null || GetComponent<Image>() == null)
                return;

            Color color = GetThemeField<Color>(ThemeManager.current);
            GetComponent<Image>().color = color;
        }
    }
}