using System;
using UnityEngine;
using UnityEngine.UI;

namespace Singularity.ThemeManagement.Mono
{
    [Serializable, RequireComponent(typeof(Image))]
    public class ThemeImageSpriteReceiver : ThemeStyleReceiver
    {
        public override void Redraw()
        {
            if(this == null || GetComponent<Image>() == null)
                return;

            Sprite sprite = GetThemeField<Sprite>(ThemeManager.current);
            GetComponent<Image>().sprite = sprite;
        }
    }
}