using Singularity.Entities;
using System;
using UnityEngine;

namespace Singularity.ThemeManagement
{
    [Serializable]
    public class Theme
    {
        [SerializeField]
        protected string m_Name;
        public string name { get { return m_Name; } }

        [SerializeField]
        public TileAsset gmTileAsset;

        [SerializeField]
        public Sprite gmTileShapeSprite;

        [SerializeField]
        public Sprite gmBgShapeSprite;

        [SerializeField]
        public Color gmBgShapeColor;

        [SerializeField] 
        public Color gmBgColor;

        [SerializeField] 
        public Color gmEmptyCellColor;

        [SerializeField] 
        public Color gmEmptyCellHLColor;
        
        [SerializeField] 
        public Color gmScoreTxtColor;

        [SerializeField]
        public Color gmBstScoreTxtColor;

        public Theme(string name)
        {
            this.m_Name = name;
        }

        public bool Equals(Theme theme)
        {
            if(theme == null) return false;
            return theme.name.ToUpper() == name.ToUpper();
        }
    }
}