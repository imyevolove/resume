using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Singularity.ThemeManagement
{
    [Serializable]
    public static class ThemeManager
    {
        //////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////// EVENTS
        public static event ThemeEventHandler onChange;

        // Current selected Theme
        public static Theme current { get; private set; }

        // Themes dictionary [name - theme]
        private static Dictionary<string, Theme> themes;

        //////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////// CONSTRUCTOR
        static ThemeManager()
        {
            current = new Theme("unnamed"); // Theme by default
            themes  = new Dictionary<string, Theme> ();
        }

        //////////////////////////////////////////////////////////////
        public static bool SelectTheme(Theme theme)
        {
            if(theme == null) 
            {
                Debug.Log("Param theme could not be null");
                return false;
            }

            if(!themes.ContainsKey(theme.name))
            {
                Debug.LogFormat("Theme [{0}] not found", theme.name);
                return false;
            }

            if(current.Equals(theme))
            {
                Debug.LogFormat("This theme [{0}] already selected", theme.name);
                return false;
            }

            current = themes[theme.name];
            ExecuteEvent(onChange);
            return true;
        }

        //////////////////////////////////////////////////////////////
        public static bool SelectTheme(string themeName)
        {
            if (string.IsNullOrEmpty(themeName)) return false;
            if(!themes.ContainsKey(themeName))
            {
                Debug.LogFormat("Theme [{0}] not found", themeName);
                return false;
            }

            //
            if(current.name.ToUpper() == themeName.ToUpper())
            {
                Debug.LogFormat("This theme [{0}] already selected", themeName);
                return false;
            }

            current = themes[themeName];
            ExecuteEvent(onChange);
            return true;
        }

        //////////////////////////////////////////////////////////////
        public static bool SelectTheme(int index)
        {
            if(index >= themes.Count)
            {
                Debug.LogFormat("Index [{0}] out of range", index);
                return false;
            }

            Theme theme = themes.Values.ElementAt(index);

            //
            if(current.Equals(theme))
            {
                Debug.LogFormat("This theme [{0}] already selected", theme.name);
                return false;
            }

            current = theme;
            ExecuteEvent(onChange);
            return true;
        }

        //////////////////////////////////////////////////////////////
        public static bool AddTheme(Theme theme, bool rewrite = false)
        {
            if(theme == null) 
            {
                Debug.Log("Param theme could not be null");
                return false;
            }

            if(themes.ContainsKey(theme.name))
            {
                if(!rewrite) 
                {
                    Debug.LogFormat("Theme [{0}] contains in themes list. Yuo can use rewrite flag", theme.name);
                    return false;
                }
                else 
                {
                    themes[theme.name] = theme;

                    // If current theme equal new theme
                    // then redefine current theme
                    // and call event
                    if(current.Equals(theme))
                        current = theme;
                    
                    //
                    ExecuteEvent(onChange);
                    return true;
                }
            }

            themes.Add(theme.name, theme);
            return true;
        }

        //////////////////////////////////////////////////////////////
        public static bool RemoveTheme(Theme theme)
        {
            if(theme == null) 
            {
                Debug.Log("Param theme could not be null");
                return false;
            }

            if(!themes.ContainsKey(theme.name))
            {
                Debug.LogFormat("Theme [{0}] not found", theme.name);
                return false;
            }

            return themes.Remove(theme.name);
        }

        //////////////////////////////////////////////////////////////
        public static bool RemoveTheme(string themeName) 
        {
            if(!themes.ContainsKey(themeName))
            {
                Debug.LogFormat("Theme [{0}] not found", themeName);
                return false;
            }

            return themes.Remove(themeName);
        }

        //////////////////////////////////////////////////////////////
        private static void ExecuteEvent(ThemeEventHandler eventHandler)
        {
            if(eventHandler == null) return;
            eventHandler();
        }

    }
}