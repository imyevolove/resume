﻿using Singularity.IAPManagement.Events;
using UnityEngine;

namespace Singularity.IAPManagement
{
    public interface IProduct
    {
        string productId { get; }

        string title { get; set; }
        string description { get; set; }
        string price { get; set; }

        Sprite image { get; set; }

        ProductState    state   { get; }
        ProductType     type    { get; }
        ProductMarker   marker  { get; }

        bool isUnlocked { get; }

        event ProductChangeEventHandler onStateChange;
        event ProductChangeEventHandler onInfoChange;

        void Purchase();
        void Unlock();
        void Lock();
    }
}
