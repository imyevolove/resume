﻿using Singularity.IAPManagement.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Singularity.IAPManagement
{
    public abstract class BaseIAPController : MonoBehaviour
    {
        public event UnityAction onInitializedSuccess;

        public BaseIAPController() {}

        ////////////////////////////////////////////////////////////
        public abstract void Initialize();

        ////////////////////////////////////////////////////////////
        public abstract bool IsInitialized();

        ////////////////////////////////////////////////////////////
        public abstract bool AddProduct(string productId, ProductType type);

        ////////////////////////////////////////////////////////////
        public abstract string GetProductPrice(string productId);

        ////////////////////////////////////////////////////////////
        public abstract void BuyProduct(IProduct product, ProductEventHandler eventHandler);

        ////////////////////////////////////////////////////////////
        public abstract void RestorePurchases();

        ////////////////////////////////////////////////////////////
        protected void CallInitializedSuccessEvent()
        {
            if (onInitializedSuccess != null)
            {
                onInitializedSuccess();
            }
        }

        ////////////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}
