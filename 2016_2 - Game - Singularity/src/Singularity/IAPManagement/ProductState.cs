﻿namespace Singularity.IAPManagement
{
    public enum ProductState
    {
        Locked,
        Unlocked
    }
}
