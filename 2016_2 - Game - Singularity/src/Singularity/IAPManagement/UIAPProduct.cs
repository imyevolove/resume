﻿using System;
using Singularity.IAPManagement.Events;
using UnityEngine;

namespace Singularity.IAPManagement
{
    public class UIAPProduct : IProduct
    {
        public string productId     { get; protected set; }

        public string description   {
            get { return mDescription; }
            set { mDescription = value; CallOnInfoChange(); }
        }
        public string title
        {
            get { return mTitle; }
            set { mTitle = value; CallOnInfoChange(); }
        }
        public string price
        {
            get { return mPrice; }
            set { mPrice = value; CallOnInfoChange(); }
        }

        public ProductState     state   { get; protected set; }
        public ProductType      type    { get; protected set; }
        public ProductMarker    marker  { get; protected set; }

        public bool isUnlocked { get { return state == ProductState.Unlocked; } }

        public Sprite image         { get; set; }
       
        public ProductPurchaseAction unlockAction;

        public event ProductChangeEventHandler onStateChange;
        public event ProductChangeEventHandler onInfoChange;

        protected string mDescription;
        protected string mTitle;
        protected string mPrice;

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public UIAPProduct(string productId, ProductType type, ProductMarker marker = ProductMarker.Default)
        {
            this.productId  = productId;
            this.type       = type;
            this.marker     = marker;
            state           = ProductState.Locked;
        }

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public UIAPProduct(string productId, ProductType type, string title, string description, ProductMarker marker = ProductMarker.Default)
        {
            this.productId      = productId;
            this.title          = title;
            this.description    = description;
            this.type           = type;
            this.marker         = marker;
            state               = ProductState.Locked;
        }

        ////////////////////////////////////////////////////////////
        public void Purchase()
        {
            IAPManager.BuyProduct(this, eventData => {
                switch (eventData.status)
                {
                    case PurchaseStatus.Success:
                    case PurchaseStatus.Purchased:
                        Unlock();
                        break;
                }
            });
        }

        ////////////////////////////////////////////////////////////
        public void Unlock()
        {
            if (state == ProductState.Unlocked) return;

            state = ProductState.Unlocked;
            CallOnStateChange();

            if (unlockAction != null)
            {
                unlockAction(this);
            }
        }

        ////////////////////////////////////////////////////////////
        public void Lock()
        {
            if (state == ProductState.Locked) return;
            state = ProductState.Locked;
            CallOnStateChange();
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnStateChange()
        {
            if (onStateChange != null)
            {
                onStateChange();
            }
        }

        ////////////////////////////////////////////////////////////
        protected void CallOnInfoChange()
        {
            if (onInfoChange != null)
            {
                onInfoChange();
            }
        }
    }
}