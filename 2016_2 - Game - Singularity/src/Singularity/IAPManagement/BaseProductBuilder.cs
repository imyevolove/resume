﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Singularity.IAPManagement.Events;

namespace Singularity.IAPManagement
{
    public abstract class BaseProductsBundleBuilder
    {
        public abstract BaseProductsBundleBuilder AddProduct(IProduct product);
        public abstract IProduct[] Build();
    }
}
