﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Singularity.IAPManagement.Events
{
    public enum PurchaseStatus
    {
        Purchased,
        Success,
        Fail
    }
}
