﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Singularity.IAPManagement.Events
{
    /// <summary>
    /// Product event data
    /// </summary>
    public class ProductEventData
    {
        public IProduct         product { get; protected set; }
        public PurchaseStatus   status  { get; protected set; }

        public ProductEventData(IProduct product, PurchaseStatus status)
        {
            this.product = product;
            this.status  = status;
        }
    }
}
