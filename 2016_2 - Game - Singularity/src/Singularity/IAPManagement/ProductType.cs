﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Singularity.IAPManagement
{
    public enum ProductType
    {
        /// <summary>
        /// Can be purchased repeatedly. Suitable for consumable products such as virtual 
        /// currencies.
        /// </summary>
        Consumable = 0,
        /// <summary>
        /// Can only be purchased once. Suitable for one-off purchases such as extra levels.
        /// </summary>
        NonConsumable = 1,
        /// <summary>
        /// Can be purchased repeatedly and restored. Durable but with a finite duration
        /// of validity.
        /// </summary>
        Subscription = 2
    }
}
