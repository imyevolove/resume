﻿using Singularity.IAPManagement.Events;
using System;
using UnityEngine;

namespace Singularity.IAPManagement
{
    public class UsableProduct : UIAPProduct, IUsable
    {
        public UsableProduct(string productId, ProductType type, 
            ProductMarker marker = ProductMarker.Default)
            : base(productId, type, marker) {}

        public UsableProduct(string productId, ProductType type, string title, string description, 
            ProductMarker marker = ProductMarker.Default)
            : base(productId, type, title, description, marker) {}

        public ProductUseAction useAction;
        public ProductUseActionValidator useActionValidator;

        public bool CanUse()
        {
            if(useActionValidator == null)
                return false;
            return useActionValidator();
        }

        public void Use()
        {
            if (!isUnlocked)
            {
                Debug.Log("Can't use, product is locked");
                return;
            }

            if (!CanUse())
            {
                Debug.Log("Can't use. Validator False");
                return;
            }

            if (useAction != null)
            {
                useAction();
            }
        }
    }
}
