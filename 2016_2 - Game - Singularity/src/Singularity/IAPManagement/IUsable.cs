﻿namespace Singularity.IAPManagement
{
    public interface IUsable
    {
        bool CanUse();
        void Use();
    }
}
