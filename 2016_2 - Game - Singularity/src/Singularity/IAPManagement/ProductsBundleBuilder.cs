﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Singularity.IAPManagement.Events;

namespace Singularity.IAPManagement
{
    public class ProductsBundleBuilder : BaseProductsBundleBuilder
    {
        private HashSet<IProduct> products = new HashSet<IProduct>();

        ////////////////////////////////////////////////////////////
        public override BaseProductsBundleBuilder AddProduct(IProduct product)
        {
            products.Add(product);
            return this;
        }

        ////////////////////////////////////////////////////////////
        public override IProduct[] Build()
        {
            return products.ToArray();
        }
    }
}
