﻿using Singularity.IAPManagement.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.IAPManagement
{
    public class IAPManager
    {
        public IAPManager() {}

        public static int productsCount { get { return products.Count; } }

        private static BaseIAPController mIAPController;
        private static List<IProduct> products = new List<IProduct>();

        public static void Initialize()
        {
            RepairController();

            if (mIAPController.IsInitialized())
            {
                return;
            }

            mIAPController.onInitializedSuccess -= RequestProductPrices;
            mIAPController.onInitializedSuccess += RequestProductPrices;
            mIAPController.Initialize();
        }

        ////////////////////////////////////////////////////////////
        public static void AddProduct(IProduct product)
        {
            RepairController();
            if (mIAPController.AddProduct(product.productId, product.type))
            {
                products.Add(product);
            }
        }

        ////////////////////////////////////////////////////////////
        public static void AddProducts(IProduct[] products)
        {
            foreach (var product in products)
            {
                AddProduct(product);
            }
        }

        ////////////////////////////////////////////////////////////
        public static void BuyProduct(IProduct product, ProductEventHandler eventHandler)
        {
            Initialize();
            if (!products.Contains(product))
            {
                Debug.LogFormat("Can't buy broduct. Product {0} is not registered", product.productId);
                if (eventHandler != null)
                {
                    eventHandler(new ProductEventData(product, PurchaseStatus.Fail));
                }
                return;
            }

            mIAPController.BuyProduct(product, eventHandler);
        }

        ////////////////////////////////////////////////////////////
        public static void RestorePurchases()
        {
            Initialize();
            mIAPController.RestorePurchases();
        }

        ////////////////////////////////////////////////////////////
        public static IProduct GetProduct(int index)
        {
            return products[index];
        }

        ////////////////////////////////////////////////////////////
        public static IProduct GetProduct(string productId)
        {
            return products.Find(p => { return p.productId == productId; });
        }

        ////////////////////////////////////////////////////////////
        public static IProduct[] GetProducts()
        {
            return products.ToArray();
        }

        ////////////////////////////////////////////////////////////
        public static IProduct[] GetProducts(ProductMarker markerFilter)
        {
            return products.FindAll(p => { return p.marker == markerFilter; }).ToArray();
        }

        ////////////////////////////////////////////////////////////
        private static void RepairController()
        {
            if (mIAPController == null)
            {
                mIAPController = IAPController.Instantiate();
            }
        }

        ////////////////////////////////////////////////////////////
        private static void RequestProductPrices()
        {
            foreach (var product in products)
            {
                product.price = mIAPController.GetProductPrice(product.productId);
            }
        }
    }
}
