﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.AnimatorState
{
    public abstract class BaseAnimatorState
    {
        public abstract void Activate(Animator animator);
        public abstract void Dismiss (Animator animator);
    }
}

/*
    new BoolAnimatorState("action");
 */
