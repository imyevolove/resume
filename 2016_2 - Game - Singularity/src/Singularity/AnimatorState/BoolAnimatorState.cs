﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.AnimatorState
{
    public class BoolAnimatorState : BaseAnimatorState
    {
        public string parameterId { get; protected set; }

        ////////////////////////////////////////////////////////////
        //////////////////////////////////////////////// CONSTRUCTOR
        public BoolAnimatorState(string parameterId)
        {
            this.parameterId = parameterId;
        }

        ////////////////////////////////////////////////////////////
        public override void Activate(Animator animator)
        {
            if (animator == null) return;
            animator.SetBool(parameterId, true);
        }

        ////////////////////////////////////////////////////////////
        public override void Dismiss(Animator animator)
        {
            if (animator == null) return;
            animator.SetBool(parameterId, false);
        }
    }
}

/*
    new BoolAnimatorState("action");
 */
