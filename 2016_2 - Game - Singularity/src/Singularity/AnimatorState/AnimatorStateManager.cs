﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Singularity.AnimatorState
{
    public class AnimatorStateManager
    {
        public BaseAnimatorState lastState
        {
            get
            {
                try
                {
                    return historyStack.Peek();
                }
                catch (Exception) { }
                return null;
            }
        }

        protected Animator animator;
        protected Stack<BaseAnimatorState> historyStack = new Stack<BaseAnimatorState>();

        ////////////////////////////////////////////////////////////
        public AnimatorStateManager(Animator animator)
        {
            this.animator = animator;
        }

        ////////////////////////////////////////////////////////////
        public void Activate(BaseAnimatorState state)
        {
            if (state == null) return;
            historyStack.Push(state);
            state.Activate(animator);
        }

        ////////////////////////////////////////////////////////////
        public void Dismiss()
        {
            try
            {
                /// Dismiss current state
                BaseAnimatorState state = historyStack.Pop();
                if (state != null) state.Dismiss(animator);

                /// Try activate previous state
                BaseAnimatorState previousState = historyStack.Pop();
                if (previousState != null) Activate(previousState);
            }
            catch (Exception) { }
        }
    }
}
