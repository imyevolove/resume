using System;
using System.Collections.Generic;
using System.Linq;

public class LinkedQueue<T>
{
    public int Count
    {
        get { return _items.Count; }
    }

    public void Enqueue(T item)
    {
        _items.AddLast(item);
    }

    public void Clear()
    {
        _items.Clear();
    }

    public T Dequeue()
    {
        if (_items.First == null)
           return default(T);

        var item = _items.First.Value;
        _items.RemoveFirst();

        return item;
    }

    public T Peek()
    {
        if (_items.First == null)
           return default(T);

        return _items.First.Value;
    }

    public void Remove(T item)
    {
        _items.Remove(item);
    }

    public void RemoveAt(int index)
    {
        Remove(_items.Skip(index).First());
    }

    public T this[int index]
    {
        get
        { 
            return _items.ElementAt(index);
        }
    }

    private LinkedList<T> _items = new LinkedList<T>();
}