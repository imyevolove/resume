﻿namespace Generic.String
{
    public enum StringCase
    {
        None,
        Upper,
        Lower,
        Capitalize,
        CapitalizeAll
    }
}
